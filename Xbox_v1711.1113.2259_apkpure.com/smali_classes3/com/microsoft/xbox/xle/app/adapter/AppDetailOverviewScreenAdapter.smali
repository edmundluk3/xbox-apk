.class public Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "AppDetailOverviewScreenAdapter.java"


# instance fields
.field private final buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final buySubscriptionText:Landroid/widget/TextView;

.field private final descriptionText:Landroid/widget/TextView;

.field private final genreLayout:Landroid/widget/RelativeLayout;

.field private final genreText:Landroid/widget/TextView;

.field private final helpButton:Landroid/widget/RelativeLayout;

.field private final imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final languagesLayout:Landroid/widget/RelativeLayout;

.field private final languagesText:Landroid/widget/TextView;

.field private final playButton:Landroid/widget/RelativeLayout;

.field private final playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final playSubscriptionText:Landroid/widget/TextView;

.field private final publisherLayout:Landroid/widget/RelativeLayout;

.field private final publisherText:Landroid/widget/TextView;

.field private final purchaseButton:Landroid/widget/RelativeLayout;

.field private final purchaseButtonIconView:Landroid/widget/TextView;

.field private final purchaseButtonLabel:Landroid/widget/TextView;

.field private final purchaseButtonStrikethroughText:Landroid/widget/TextView;

.field private final purchaseButtonText:Landroid/widget/TextView;

.field private final purchasedDateTextView:Landroid/widget/TextView;

.field private final purchasedTextLayout:Landroid/widget/LinearLayout;

.field private final ratingLayout:Landroid/widget/RelativeLayout;

.field private final ratingText:Landroid/widget/TextView;

.field private final releaseDateText:Landroid/widget/TextView;

.field private final releaseLayout:Landroid/widget/RelativeLayout;

.field private final starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 57
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    .line 59
    const v0, 0x7f0e01b7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->screenBody:Landroid/view/View;

    .line 60
    const v0, 0x7f0e01b8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 61
    const v0, 0x7f0e01c4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->descriptionText:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0e01c7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->publisherText:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0e01ca

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->genreText:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0e01cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->ratingText:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0e01d0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->releaseDateText:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0e01d3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->languagesText:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0e01b9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    .line 68
    const v0, 0x7f0e01bc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    .line 69
    const v0, 0x7f0e01c5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->publisherLayout:Landroid/widget/RelativeLayout;

    .line 70
    const v0, 0x7f0e01c8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->genreLayout:Landroid/widget/RelativeLayout;

    .line 71
    const v0, 0x7f0e01cb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->ratingLayout:Landroid/widget/RelativeLayout;

    .line 72
    const v0, 0x7f0e01ce

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->releaseLayout:Landroid/widget/RelativeLayout;

    .line 73
    const v0, 0x7f0e01d1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->languagesLayout:Landroid/widget/RelativeLayout;

    .line 74
    const v0, 0x7f0e01bf

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    .line 75
    const v0, 0x7f0e01c1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchasedDateTextView:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f0e0967

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonIconView:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0e01ba

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    .line 78
    const v0, 0x7f0e096b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonText:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0e096a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0e096c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 81
    const v0, 0x7f0e096d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0e0977

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 83
    const v0, 0x7f0e0978

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0e0968

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonLabel:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0e01b3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 86
    const v0, 0x7f0e01b5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    sget-boolean v0, Lcom/microsoft/xbox/toolkit/Build;->IncludePurchaseFlow:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 95
    :cond_1
    return-void

    .line 89
    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private getDisplayPriceText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getIsFree()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070afd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->showStarRatingDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->launchApp()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->purchaseTitle()V

    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->launchHelp()V

    return-void
.end method

.method private updateSubscriptionDetails()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 234
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getSubscriptionText()Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "subscriptionName":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getSubscriptionImageResourceId()I

    move-result v0

    .line 237
    .local v0, "subscriptionImageResourceId":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 238
    if-lez v0, :cond_1

    .line 239
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    .line 240
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageResource(I)V

    .line 241
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 242
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 243
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 244
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 246
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 249
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 250
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 251
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0

    .line 253
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 254
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 255
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 256
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 257
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonIconView:Landroid/widget/TextView;

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    :cond_3
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->helpButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    :cond_3
    return-void
.end method

.method public updateViewOverride()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->updateLoadingIndicator(Z)V

    .line 137
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->descriptionText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    move v1, v2

    :goto_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v1, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getIsPlayable()Z

    move-result v1

    if-eqz v1, :cond_c

    move v0, v3

    .line 141
    .local v0, "playButtonVisibility":I
    :goto_1
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->IncludePurchaseFlow:Z

    if-eqz v1, :cond_14

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v1

    if-nez v1, :cond_14

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonLabel:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getPurchaseButtonLabelText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 145
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->isBusy()Z

    move-result v1

    if-nez v1, :cond_d

    move v1, v2

    :goto_2
    invoke-virtual {v5, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 148
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->isBusy()Z

    move-result v5

    if-nez v5, :cond_e

    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getIsPlayable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getIsPurchasable()Z

    move-result v1

    if-nez v1, :cond_f

    .line 157
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 158
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 159
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 160
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playSubscriptionText:Landroid/widget/TextView;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 168
    :cond_2
    :goto_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getIsPurchasable()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 170
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 171
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 172
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonLabel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getIsFree()Z

    move-result v1

    if-eqz v1, :cond_10

    move v1, v4

    :goto_5
    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 173
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonText:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->getDisplayPriceText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 175
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 177
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getDisplayListPrice()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 183
    :goto_6
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->updateSubscriptionDetails()V

    .line 192
    :cond_3
    :goto_7
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getPurchaseDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 193
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchasedDateTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getPurchaseDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 202
    :goto_8
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getPublisher()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 203
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->publisherText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getPublisher()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 204
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->publisherLayout:Landroid/widget/RelativeLayout;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 206
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 207
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->genreText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getGenres()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 208
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->genreLayout:Landroid/widget/RelativeLayout;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 210
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getRating()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->ratingText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getRating()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 212
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->ratingLayout:Landroid/widget/RelativeLayout;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 214
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 215
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->releaseDateText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getReleaseDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 216
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->releaseLayout:Landroid/widget/RelativeLayout;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 218
    :cond_7
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getLanguages()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 219
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->languagesText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getLanguages()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->languagesLayout:Landroid/widget/RelativeLayout;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 222
    :cond_8
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v1, :cond_9

    .line 223
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 224
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->imageTitle:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getBoxArtBackgroundColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundColor(I)V

    .line 226
    :cond_9
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    if-eqz v1, :cond_a

    .line 227
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->starRatings:Lcom/microsoft/xbox/xle/ui/StarRatingView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getAverageUserRating()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/StarRatingView;->setAverageUserRating(F)V

    .line 230
    :cond_a
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 231
    return-void

    .end local v0    # "playButtonVisibility":I
    :cond_b
    move v1, v3

    .line 137
    goto/16 :goto_0

    :cond_c
    move v0, v4

    .line 139
    goto/16 :goto_1

    .restart local v0    # "playButtonVisibility":I
    :cond_d
    move v1, v3

    .line 145
    goto/16 :goto_2

    :cond_e
    move v2, v3

    .line 149
    goto/16 :goto_3

    .line 162
    :cond_f
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->getIsAdult()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 163
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->updateSubscriptionDetails()V

    goto/16 :goto_4

    :cond_10
    move v1, v3

    .line 172
    goto/16 :goto_5

    .line 180
    :cond_11
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButtonStrikethroughText:Landroid/widget/TextView;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_6

    .line 185
    :cond_12
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchaseButton:Landroid/widget/RelativeLayout;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 187
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->buySubscriptionText:Landroid/widget/TextView;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_7

    .line 196
    :cond_13
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->purchasedTextLayout:Landroid/widget/LinearLayout;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_8

    .line 199
    :cond_14
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;->playButton:Landroid/widget/RelativeLayout;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_8
.end method
