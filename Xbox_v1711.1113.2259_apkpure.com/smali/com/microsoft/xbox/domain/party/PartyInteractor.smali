.class public Lcom/microsoft/xbox/domain/party/PartyInteractor;
.super Ljava/lang/Object;
.source "PartyInteractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyMemberChangeResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;,
        Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final initialLoadTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;",
            ">;"
        }
    .end annotation
.end field

.field private final inviteTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteResult;",
            ">;"
        }
    .end annotation
.end field

.field private final kickMemberTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;",
            ">;"
        }
    .end annotation
.end field

.field private final leavePartyTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyResult;",
            ">;"
        }
    .end annotation
.end field

.field private final mutePartyTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;",
            ">;"
        }
    .end annotation
.end field

.field private final partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

.field private final sendTextTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextResult;",
            ">;"
        }
    .end annotation
.end field

.field private final setAllowBroadcastTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastResult;",
            ">;"
        }
    .end annotation
.end field

.field private telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

.field private final toggleJoinabilityTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityResult;",
            ">;"
        }
    .end annotation
.end field

.field private final toggleMemberMuteTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;)V
    .locals 1
    .param p1, "partyChatRepository"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .param p2, "userSummaryRepository"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .param p3, "telemetryService"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    .line 57
    iput-object p3, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    .line 59
    invoke-static {p0, p2}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    .line 103
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->inviteTransformer:Lio/reactivex/ObservableTransformer;

    .line 126
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->mutePartyTransformer:Lio/reactivex/ObservableTransformer;

    .line 141
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->toggleJoinabilityTransformer:Lio/reactivex/ObservableTransformer;

    .line 152
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->setAllowBroadcastTransformer:Lio/reactivex/ObservableTransformer;

    .line 162
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->leavePartyTransformer:Lio/reactivex/ObservableTransformer;

    .line 172
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->sendTextTransformer:Lio/reactivex/ObservableTransformer;

    .line 181
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->toggleMemberMuteTransformer:Lio/reactivex/ObservableTransformer;

    .line 194
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->kickMemberTransformer:Lio/reactivex/ObservableTransformer;

    .line 204
    return-void
.end method

.method static synthetic lambda$new$11(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 104
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$37;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 105
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 104
    return-object v0
.end method

.method static synthetic lambda$new$16(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 127
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$32;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 128
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 127
    return-object v0
.end method

.method static synthetic lambda$new$20(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 142
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$28;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 143
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 142
    return-object v0
.end method

.method static synthetic lambda$new$24(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 153
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$24;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 154
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 153
    return-object v0
.end method

.method static synthetic lambda$new$28(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 163
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$20;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 164
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 163
    return-object v0
.end method

.method static synthetic lambda$new$31(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 173
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$18;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 174
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 173
    return-object v0
.end method

.method static synthetic lambda$new$35(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 182
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 183
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 182
    return-object v0
.end method

.method static synthetic lambda$new$39(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 195
    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 196
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 195
    return-object v0
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "userSummaryRepository"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 60
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$44;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 61
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/microsoft/xbox/xbservices/domain/party/PartySession;)Ljava/util/Collection;
    .locals 6
    .param p0, "partySession"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .param p1, "partySession1"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getRoster()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 77
    .local v0, "memberXuids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getRoster()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 78
    .local v1, "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 80
    .end local v1    # "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    :cond_0
    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;Lcom/microsoft/xbox/xbservices/domain/party/PartySession;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "userSummaryRepository"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .param p1, "partySession"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$48;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$49;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lio/reactivex/Observable;->toList()Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$50;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    .line 72
    return-object v0
.end method

.method static synthetic lambda$null$10(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;)Lio/reactivex/ObservableSource;
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->inviteToParty()V

    .line 109
    new-instance v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x103000a

    const v3, 0x7f070208

    sget-object v4, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;->MULTIPLE:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;-><init>(Landroid/content/Context;IILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;)V

    .line 112
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->activate()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$38;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 113
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$OkIntent;

    .line 118
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$39;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 119
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapIterable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$40;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$41;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 121
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$42;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 122
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$43;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 123
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 109
    return-object v0
.end method

.method static synthetic lambda$null$12(Lcom/microsoft/xbox/domain/party/PartyInteractor;Ljava/lang/Object;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "ignored"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->toggleSelfMute()V

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->toggleMuteRemoteAudio()V

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isRemoteAudioEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->muteParty(Z)V

    .line 134
    return-void
.end method

.method static synthetic lambda$null$13(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;
    .locals 1
    .param p0, "result"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 135
    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;->completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$14(Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 137
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$15(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$33;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$34;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 135
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$35;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 136
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$36;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 137
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 129
    return-object v0
.end method

.method static synthetic lambda$null$17(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityResult;
    .locals 1
    .param p0, "result"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 147
    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityResult;->completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$18(Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 149
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$19(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getJoinRestriction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->inviteOnly(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->toggleJoinability()Lio/reactivex/Single;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$29;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 147
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$30;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 148
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$31;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 149
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 145
    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;
    .locals 1
    .param p0, "partySession"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .param p1, "userSummaryList"    # Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;->with(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$21(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastResult;
    .locals 1
    .param p0, "result"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 157
    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastResult;->completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$22(Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 159
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$23(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;->allow()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->setAllowedInBroadcast(Z)Lio/reactivex/Single;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$25;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 157
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$26;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 158
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$27;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 159
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 155
    return-object v0
.end method

.method static synthetic lambda$null$25(Lretrofit2/Response;)Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyResult;
    .locals 1
    .param p0, "result"    # Lretrofit2/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 167
    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyResult;->completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$26(Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 169
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$27(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->leaveParty()V

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->leaveParty()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$21;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 167
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$22;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 168
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$23;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 169
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 166
    return-object v0
.end method

.method static synthetic lambda$null$29(Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 178
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 99
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$30(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->sendChatMessage()V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->sendTextChatMessage(Ljava/lang/String;)V

    .line 177
    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextResult;->completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextResult;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$19;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 178
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 177
    return-object v0
.end method

.method static synthetic lambda$null$32(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteResult;
    .locals 1
    .param p0, "result"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 189
    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteResult;->completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$33(Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 191
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$34(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;->xuid()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    .line 185
    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getCachedRoster()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    .line 186
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->isAudioEnabled()Z

    move-result v3

    .line 184
    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->muteMember(Ljava/lang/String;Ljava/util/Map;Z)V

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;->xuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->toggleMemberMute(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$15;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 189
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$16;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 190
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 191
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 187
    return-object v0
.end method

.method static synthetic lambda$null$36(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;
    .locals 1
    .param p0, "result"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 200
    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;->completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$37(Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 202
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$38(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;->xuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->removeMember(Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;->xuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->kickUser(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 200
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 201
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 202
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 198
    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "userSummaryRepository"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v2, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getPartySubject()Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v2}, Lio/reactivex/Observable;->cache()Lio/reactivex/Observable;

    move-result-object v0

    .line 66
    .local v0, "partyObservable":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartySession;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$45;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 72
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 97
    .local v1, "userObservable":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;>;"
    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$46;->lambdaFactory$()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->zip(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    .line 98
    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getPartyMessages()Lio/reactivex/Observable;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getPartyMemberChanges()Lio/reactivex/Observable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;->with(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lcom/microsoft/xbox/domain/party/PartyInteractor$InitialLoadResult;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p2}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$47;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 99
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 97
    return-object v2
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 114
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->cancelInvite()V

    .line 117
    :cond_0
    return-void
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/domain/party/PartyInteractor;Ljava/lang/Long;)Lio/reactivex/SingleSource;
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/domain/party/PartyInteractor;
    .param p1, "xuid"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->sendPartyInvite(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;)Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteResult;
    .locals 1
    .param p0, "result"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 121
    invoke-static {}, Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteResult;->completedInstance()Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method


# virtual methods
.method public getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getInviteTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$InviteResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->inviteTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getKickMemberTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$KickMemberResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->kickMemberTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getLeavePartyTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$LeavePartyResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->leavePartyTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getMutePartyTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$MutePartyResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->mutePartyTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getSendTextTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$SendTextResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->sendTextTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getToggleAllowBroadcastTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$SetAllowBroadcastResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->setAllowBroadcastTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getToggleJoinabilityTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$TogglePartyJoinabilityResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->toggleJoinabilityTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getToggleMemberMuteTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteAction;",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor$ToggleMemberMuteResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor;->toggleMemberMuteTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
