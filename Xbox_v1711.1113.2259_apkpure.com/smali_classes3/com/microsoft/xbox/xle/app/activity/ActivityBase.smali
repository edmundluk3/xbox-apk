.class public abstract Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.super Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
.source "ActivityBase.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/ActionBarCallbacks;


# static fields
.field protected static final NO_CONTEXTUAL_APPBAR_ID:I = -0x1

.field private static final TAG:Ljava/lang/String;

.field public static final aboutChannel:Ljava/lang/String; = "About"

.field public static final avatarChannel:Ljava/lang/String; = "Avatar"

.field public static final detailsChannel:Ljava/lang/String; = "Details"

.field public static final discoverChannel:Ljava/lang/String; = "Discover"

.field public static final gamesChannel:Ljava/lang/String; = "Games"

.field public static final homeChannel:Ljava/lang/String; = "Home"

.field public static final launchChannel:Ljava/lang/String; = "Launch"

.field public static final messageChannel:Ljava/lang/String; = "Message"

.field public static final nowPlayingChannel:Ljava/lang/String; = "NowPlaying"

.field public static final profileChannel:Ljava/lang/String; = "Profile"

.field public static final searchChannel:Ljava/lang/String; = "Search"

.field public static final searchFilterChannel:Ljava/lang/String; = "SearchFilter"

.field public static final searchResultsChannel:Ljava/lang/String; = "SearchResults"

.field public static final settingChannel:Ljava/lang/String; = "Settings"

.field public static final socialChannel:Ljava/lang/String; = "Social"

.field public static final whatsnewChannel:Ljava/lang/String; = "What\'s new"


# instance fields
.field private collapsedAppBarButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private enableMediaTransportControls:Z

.field private isPivotPane:Z

.field private isSubPane:Z

.field private name:Ljava/lang/String;

.field protected refresh:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private showRightPane:Z

.field private showUtilityBar:Z

.field protected viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(I)V

    .line 78
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(IZ)V

    .line 82
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 3
    .param p1, "orientation"    # I
    .param p2, "usesAccelerometer"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 89
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;-><init>(Landroid/content/Context;IZ)V

    .line 74
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 98
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isPivotPane:Z

    .line 99
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isSubPane:Z

    .line 102
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showUtilityBar:Z

    .line 103
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showRightPane:Z

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 85
    const/4 v0, 0x7

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;-><init>(Landroid/content/Context;IZ)V

    .line 74
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 98
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isPivotPane:Z

    .line 99
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isSubPane:Z

    .line 102
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showUtilityBar:Z

    .line 103
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showRightPane:Z

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 93
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 98
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isPivotPane:Z

    .line 99
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isSubPane:Z

    .line 102
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showUtilityBar:Z

    .line 103
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showRightPane:Z

    .line 94
    return-void
.end method

.method private extractAppBarButtons(Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;)[Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p1, "appBarView"    # Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    .prologue
    .line 413
    const/4 v0, 0x0

    .line 414
    .local v0, "buttons":[Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    if-eqz p1, :cond_0

    .line 415
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getAppBarButtons()[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    .line 417
    :cond_0
    return-object v0
.end method

.method private getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 521
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/xle/ui/XLERootView;

    if-eqz v0, :cond_0

    .line 522
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/XLERootView;

    .line 524
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onAnimateInCompleted$0(Ljava/lang/ref/WeakReference;)V
    .locals 1
    .param p0, "viewModelWeakPtr"    # Ljava/lang/ref/WeakReference;

    .prologue
    .line 210
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 211
    .local v0, "viewModelPtr":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceUpdateViewImmediately()V

    .line 214
    :cond_0
    return-void
.end method


# virtual methods
.method public adjustBottomMargin(I)V
    .locals 1
    .param p1, "bottomMargin"    # I

    .prologue
    .line 530
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 531
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/XLERootView;->setBottomMargin(I)V

    .line 533
    :cond_0
    return-void
.end method

.method protected computeBottomMargin()I
    .locals 3

    .prologue
    .line 236
    const/4 v0, 0x0

    .line 237
    .local v0, "marginBottom":I
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 238
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getIsApplicationBarShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f09004d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 243
    :cond_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isPivotPane:Z

    if-eqz v1, :cond_1

    .line 244
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f090054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    :cond_1
    return v0
.end method

.method protected delayAppbarAnimation()Z
    .locals 1

    .prologue
    .line 556
    const/4 v0, 0x0

    return v0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 381
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/XLERootView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 382
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 383
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/XLERootView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    const/4 v0, 0x1

    .line 386
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected forceAdjustBottomMargin(I)V
    .locals 1
    .param p1, "bottomMargin"    # I

    .prologue
    .line 536
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 537
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/XLERootView;->setBottomMargin(I)V

    .line 539
    :cond_0
    return-void
.end method

.method public forceRefresh()V
    .locals 3

    .prologue
    .line 164
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Refresh"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceRefresh()V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->refresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 171
    return-void
.end method

.method public forceUpdateViewImmediately()V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceUpdateViewImmediately()V

    .line 229
    :cond_0
    return-void
.end method

.method protected abstract getActivityName()Ljava/lang/String;
.end method

.method public getActivityNameForFeedback()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    .line 125
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method public getAnimateIn(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 6
    .param p1, "goingBack"    # Z

    .prologue
    .line 272
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 273
    .local v2, "root":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 274
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v4

    const-string v5, "Screen"

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v0

    .line 275
    .local v0, "animation":Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;
    if-eqz v0, :cond_0

    .line 276
    check-cast v0, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;

    .end local v0    # "animation":Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;
    sget-object v4, Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;->ANIMATE_IN:Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;

    invoke-virtual {v0, v4, p1, v2}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;->compile(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;ZLandroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v3

    .line 277
    .local v3, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    if-eqz v3, :cond_0

    .line 278
    new-instance v1, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;-><init>()V

    .line 279
    .local v1, "animationPackage":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;->add(Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;)V

    .line 286
    .end local v1    # "animationPackage":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .end local v3    # "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAnimateOut(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 6
    .param p1, "goingBack"    # Z

    .prologue
    .line 253
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 254
    .local v2, "root":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 255
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v4

    const-string v5, "Screen"

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v0

    .line 256
    .local v0, "animation":Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;
    if-eqz v0, :cond_0

    .line 257
    check-cast v0, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;

    .end local v0    # "animation":Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;
    sget-object v4, Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;->ANIMATE_OUT:Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;

    invoke-virtual {v0, v4, p1, v2}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;->compile(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;ZLandroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v3

    .line 258
    .local v3, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    if-eqz v3, :cond_0

    .line 259
    new-instance v1, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;-><init>()V

    .line 260
    .local v1, "animationPackage":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;->add(Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;)V

    .line 267
    .end local v1    # "animationPackage":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .end local v3    # "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getHeaderName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 589
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    .line 590
    .local v0, "root":Lcom/microsoft/xbox/xle/ui/XLERootView;
    if-eqz v0, :cond_0

    .line 591
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/XLERootView;->getHeaderName()Ljava/lang/String;

    move-result-object v1

    .line 593
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 607
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getActivityName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRelativeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 611
    const/4 v0, 0x0

    return-object v0
.end method

.method public getShouldShowAppbar()Z
    .locals 2

    .prologue
    .line 375
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getShouldShowAppbar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    return-object v0
.end method

.method protected isManagingOwnAppBar()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method protected isShowRightPane()Z
    .locals 1

    .prologue
    .line 623
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showRightPane:Z

    return v0
.end method

.method protected isShowUtilityBar()Z
    .locals 1

    .prologue
    .line 615
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showUtilityBar:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onActivityResult(IILandroid/content/Intent;)V

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onActivityResult(IILandroid/content/Intent;)V

    .line 142
    :cond_0
    return-void
.end method

.method public onAnimateInCompleted()V
    .locals 3

    .prologue
    .line 207
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v1, :cond_0

    .line 208
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 209
    .local v0, "viewModelWeakPtr":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v1

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase$$Lambda$1;->lambdaFactory$(Ljava/lang/ref/WeakReference;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->postRunnableAfterReady(Ljava/lang/Runnable;)V

    .line 219
    .end local v0    # "viewModelWeakPtr":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;>;"
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v1, :cond_1

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onAnimateInCompleted()V

    .line 222
    :cond_1
    return-void
.end method

.method public onAnimateInStarted()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceUpdateViewImmediately()V

    .line 203
    :cond_0
    return-void
.end method

.method public onApplicationPause()V
    .locals 1

    .prologue
    .line 431
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onApplicationPause()V

    .line 433
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onApplicationPause()V

    .line 436
    :cond_0
    return-void
.end method

.method public onApplicationResume()V
    .locals 1

    .prologue
    .line 440
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onApplicationResume()V

    .line 442
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onApplicationResume()V

    .line 445
    :cond_0
    return-void
.end method

.method public onBackButtonPressed()V
    .locals 4

    .prologue
    .line 307
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-eq v2, v3, :cond_0

    .line 308
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->dismiss(Z)V

    .line 324
    :goto_0
    return-void

    .line 310
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 312
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v2, :cond_1

    .line 313
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onBackButtonPressed()V

    goto :goto_0

    .line 314
    :cond_1
    if-eqz v0, :cond_2

    .line 315
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->goBack()V

    goto :goto_0

    .line 318
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GoBack()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 319
    :catch_0
    move-exception v1

    .line 320
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->TAG:Ljava/lang/String;

    const-string v3, "onBackButtonPressed, navigation manager go back failed"

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 500
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 503
    const-string v0, "ActivityBase"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 507
    :cond_0
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 130
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreate()V

    .line 133
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;->Created:Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;

    invoke-interface {v0, p0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->OnActivityStateChange(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;)V

    .line 134
    return-void
.end method

.method public abstract onCreateContentView()V
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 561
    const/4 v0, 0x0

    .line 562
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v1, :cond_0

    .line 563
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 565
    :cond_0
    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onDestroy()V

    .line 464
    :cond_0
    const-string v0, "ActivityBase"

    const-string v1, "onDestroy called"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 467
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onDestroy()V

    .line 468
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 639
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onDetachedFromWindow()V

    .line 640
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->clearDisappearingChildren()V

    .line 641
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 580
    const/4 v0, 0x0

    .line 581
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v1, :cond_0

    .line 582
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 584
    :cond_0
    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 422
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    .line 424
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onPause()V

    .line 427
    :cond_0
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 570
    const/4 v0, 0x0

    .line 572
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v1, :cond_0

    .line 573
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 575
    :cond_0
    return v0
.end method

.method public onRehydrate()V
    .locals 2

    .prologue
    .line 484
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onRehydrate()V

    .line 485
    const-string v0, "ActivityBase"

    const-string v1, "onRehydrate"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onRehydrate()V

    .line 489
    :cond_0
    return-void
.end method

.method public onRehydrateOverride()V
    .locals 0

    .prologue
    .line 493
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreateContentView()V

    .line 494
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 299
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 300
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 303
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 449
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    .line 451
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onResume()V

    .line 454
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 291
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 295
    :cond_0
    return-void
.end method

.method public onSetActive()V
    .locals 4

    .prologue
    .line 328
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    .line 329
    const-string v1, "ActivityBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OnSetActivity is called "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v1, :cond_0

    .line 332
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetActive()V

    .line 335
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 337
    .local v0, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    instance-of v1, p0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;

    if-nez v1, :cond_6

    .line 338
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isManagingOwnAppBar()Z

    move-result v1

    if-nez v1, :cond_1

    .line 339
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getShouldShowAppbar()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setShouldShowNowPlaying(Z)V

    .line 340
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getShouldShowAppbar()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 341
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->show()V

    .line 347
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 348
    if-eqz v0, :cond_2

    .line 349
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/XLERootView;->getShowTitleBar()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->setShowTitleBar(Z)V

    .line 353
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->sendAccessibilityEvent(I)V

    .line 360
    :cond_3
    if-eqz v0, :cond_4

    .line 361
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isShowUtilityBar()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 362
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->show()V

    .line 367
    :goto_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 368
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isShowRightPane()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->setVisibilityRightPane(Z)V

    .line 371
    :cond_4
    :goto_2
    return-void

    .line 343
    :cond_5
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->hide()V

    goto :goto_0

    .line 356
    :cond_6
    const-string v1, "ActivityBase"

    const-string v2, "This is a pivot activity. Skipping screen specific operations."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 364
    :cond_7
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBar;->hide()V

    goto :goto_1
.end method

.method public onSetInactive()V
    .locals 1

    .prologue
    .line 392
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    .line 394
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetInactive()V

    .line 397
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getIsStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 176
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStart()V

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->load()V

    .line 190
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->delayAppbarAnimation()Z

    move-result v0

    if-nez v0, :cond_2

    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->computeBottomMargin()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->adjustBottomMargin(I)V

    .line 194
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;->Started:Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;

    invoke-interface {v0, p0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->OnActivityStateChange(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;)V

    .line 195
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-interface {v0, p0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->setViewModelForActivity(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 196
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getIsStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStop()V

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetInactive()V

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStop()V

    .line 159
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;->Stopped:Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;

    invoke-interface {v0, p0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->OnActivityStateChange(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/test/automator/IAutomator$ActivityStateChange;)V

    .line 160
    return-void
.end method

.method public onTombstone()V
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onTombstone()V

    .line 477
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onTombstone()V

    .line 479
    return-void
.end method

.method public removeBottomMargin()V
    .locals 2

    .prologue
    .line 543
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 544
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/XLERootView;->setBottomMargin(I)V

    .line 546
    :cond_0
    return-void
.end method

.method public resetBottomMargin()V
    .locals 1

    .prologue
    .line 550
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getXLERootView()Lcom/microsoft/xbox/xle/ui/XLERootView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 551
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->computeBottomMargin()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->adjustBottomMargin(I)V

    .line 553
    :cond_0
    return-void
.end method

.method protected setAppBarLayout(IZZ)V
    .locals 5
    .param p1, "appBarLayoutId"    # I
    .param p2, "isEditable"    # Z
    .param p3, "enableMediaTransportControls"    # Z

    .prologue
    const/4 v2, 0x0

    .line 400
    if-lez p1, :cond_1

    .line 401
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 402
    .local v1, "vi":Landroid/view/LayoutInflater;
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    .line 403
    .local v0, "collapsedAppBar":Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->extractAppBarButtons(Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;)[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->collapsedAppBarButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 408
    .end local v0    # "collapsedAppBar":Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;
    .end local v1    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->setIsEditable(Z)V

    .line 409
    if-eqz p2, :cond_0

    move p3, v2

    .end local p3    # "enableMediaTransportControls":Z
    :cond_0
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->enableMediaTransportControls:Z

    .line 410
    return-void

    .line 405
    .restart local p3    # "enableMediaTransportControls":Z
    :cond_1
    new-array v3, v2, [Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->collapsedAppBarButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    goto :goto_0
.end method

.method public setHeaderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "headerName"    # Ljava/lang/String;

    .prologue
    .line 600
    return-void
.end method

.method public setIsPivotPane(Z)V
    .locals 0
    .param p1, "isPivotPane"    # Z

    .prologue
    .line 513
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isPivotPane:Z

    .line 514
    return-void
.end method

.method public setIsSubPane(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 517
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->isSubPane:Z

    .line 518
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 603
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->name:Ljava/lang/String;

    .line 604
    return-void
.end method

.method public setScreenState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 632
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->setScreenState(I)V

    .line 635
    :cond_0
    return-void
.end method

.method protected setShowRightPane(Z)V
    .locals 0
    .param p1, "showRightPane"    # Z

    .prologue
    .line 627
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showRightPane:Z

    .line 628
    return-void
.end method

.method protected setShowUtilityBar(Z)V
    .locals 0
    .param p1, "showUtilityBar"    # Z

    .prologue
    .line 619
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->showUtilityBar:Z

    .line 620
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    return v0
.end method
