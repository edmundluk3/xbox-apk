.class public final enum Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;
.super Ljava/lang/Enum;
.source "ChatHistoryDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MessageStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

.field public static final enum Deleted:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

.field public static final enum Enforced:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

.field public static final enum Moderated:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

.field public static final enum Ok:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 78
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    const-string v1, "Ok"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Ok:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    .line 79
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    const-string v1, "Deleted"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Deleted:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    .line 80
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    const-string v1, "Enforced"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Enforced:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    .line 81
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    const-string v1, "Moderated"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Moderated:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    .line 77
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Ok:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Deleted:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Enforced:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->Moderated:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    const-class v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    return-object v0
.end method
