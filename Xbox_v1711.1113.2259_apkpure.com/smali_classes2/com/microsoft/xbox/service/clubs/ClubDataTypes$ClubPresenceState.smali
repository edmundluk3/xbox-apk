.class public final enum Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
.super Ljava/lang/Enum;
.source "ClubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClubPresenceState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum Chat:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum Feed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum InGame:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum InLfg:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum InParty:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum NotInClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum Play:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum Roster:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 94
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "NotInClub"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->NotInClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 95
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "InClub"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 96
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "Chat"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Chat:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 97
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "Feed"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Feed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 98
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "Roster"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Roster:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 99
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "Play"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Play:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 100
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "InParty"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InParty:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 101
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "InLfg"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InLfg:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 102
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "InGame"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InGame:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 103
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    const-string v1, "Unknown"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 93
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->NotInClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Chat:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Feed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Roster:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Play:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InParty:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InLfg:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InGame:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 93
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    return-object v0
.end method
