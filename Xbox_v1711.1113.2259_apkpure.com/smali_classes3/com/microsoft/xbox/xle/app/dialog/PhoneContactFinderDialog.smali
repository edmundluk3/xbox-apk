.class public Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "PhoneContactFinderDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;,
        Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;
    }
.end annotation


# instance fields
.field private busyIndicator:Landroid/view/View;

.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private dialog_error_message:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private dialog_message:Landroid/widget/TextView;

.field private dialog_title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private errorPanel:Landroid/view/ViewGroup;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

.field private listOrGridView:Landroid/widget/AbsListView;

.field private messagePanel:Landroid/view/ViewGroup;

.field private phoneContactsInvitationSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private phoneContactsPanel:Landroid/view/ViewGroup;

.field private phoneFriendsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;",
            ">;"
        }
    .end annotation
.end field

.field private privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

.field private state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const v1, 0x7f080238

    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 55
    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->NotSet:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 59
    const v1, 0x7f0301cd

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->setContentView(I)V

    .line 61
    const v1, 0x7f0e094b

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->messagePanel:Landroid/view/ViewGroup;

    .line 62
    const v1, 0x7f0e094d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->errorPanel:Landroid/view/ViewGroup;

    .line 63
    const v1, 0x7f0e094f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneContactsPanel:Landroid/view/ViewGroup;

    .line 64
    const v1, 0x7f0e0950

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneContactsInvitationSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 65
    const v1, 0x7f0e0946

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 66
    const v1, 0x7f0e0951

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 67
    const v1, 0x7f0e093f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 68
    const v1, 0x7f0e0952

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->busyIndicator:Landroid/view/View;

    .line 69
    const v1, 0x7f0e094a

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 70
    const v1, 0x7f0e094c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_message:Landroid/widget/TextView;

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_message:Landroid/widget/TextView;

    new-instance v2, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v2}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 72
    const v1, 0x7f0e094e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_error_message:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 74
    const v1, 0x7f0e05d1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AbsListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listOrGridView:Landroid/widget/AbsListView;

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listOrGridView:Landroid/widget/AbsListView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 76
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f03010a

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listOrGridView:Landroid/widget/AbsListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 90
    const v1, 0x7f0e096e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 91
    .local v0, "sendInvitationButton":Landroid/widget/RelativeLayout;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->onAddUserToFollowingListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    return-object p1
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 80
    .local v0, "person":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getIsSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v1, p3, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v1, p3, v3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 92
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsShowInvitation()V

    .line 93
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->show()V

    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dismiss()V

    .line 95
    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Landroid/view/View;)V
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 185
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070546

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 186
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsNext()V

    .line 192
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->DisplayPhoneContactsDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v2, v3, :cond_6

    .line 193
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 194
    .local v1, "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneFriendsList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 195
    .local v0, "people":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getIsSelected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 196
    iget-object v3, v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 187
    .end local v0    # "people":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    .end local v1    # "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070738

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 188
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsOK()V

    goto :goto_0

    .line 189
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07054a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsUnlinkConfirmed()V

    goto :goto_0

    .line 200
    .restart local v1    # "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 201
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsAddFriends(Ljava/util/ArrayList;)V

    .line 202
    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$AddUserToFollowingListAsyncTask;->load(Z)V

    .line 203
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->setBusy(Z)V

    .line 210
    .end local v1    # "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_2
    return-void

    .line 205
    .restart local v1    # "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    const v3, 0x7f070e0b

    invoke-virtual {v2, v3, v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_2

    .line 208
    .end local v1    # "selected":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v2, p0, v5, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->returnFromDialog(Lcom/microsoft/xbox/toolkit/XLEManagedDialog;Z[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Landroid/view/View;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v4, 0x7f070738

    const/4 v3, 0x0

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsOK()V

    .line 219
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->DisplayPhoneContactsDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v0, v1, :cond_2

    .line 220
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->DisplayGameIsBetterDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f070e06

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_message:Landroid/widget/TextView;

    const v1, 0x7f070e0d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneContactsInvitationSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 230
    :goto_1
    return-void

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070549

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsSkip()V

    goto :goto_0

    .line 227
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v3, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->returnFromDialog(Lcom/microsoft/xbox/toolkit/XLEManagedDialog;Z[Ljava/lang/Object;)V

    .line 228
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dismiss()V

    goto :goto_1
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 235
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsClose()V

    .line 236
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dismiss()V

    .line 237
    return-void
.end method

.method private onAddUserToFollowingListCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v2, 0x0

    .line 242
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 255
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dismiss()V

    .line 256
    return-void

    .line 246
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f070e01

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_0

    .line 250
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f070e02

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_0

    .line 242
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateOptinContent()V
    .locals 5

    .prologue
    const v4, 0x7f070532

    .line 295
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070536

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 297
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070534

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070535

    .line 298
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$1;->$SwitchMap$com$microsoft$xbox$service$model$privacy$PrivacySettings$PrivacySettingValue:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 316
    :goto_0
    :pswitch_0
    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070533

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_message:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v2, 0x7f070546

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 320
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->setBusy(Z)V

    .line 321
    return-void

    .line 302
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    sget-object v1, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070531

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    .line 304
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07052f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    .line 305
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070530

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 310
    :pswitch_2
    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 299
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->setBusy(Z)V

    .line 285
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->dismiss()V

    .line 286
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 278
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsClose()V

    .line 279
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dismiss()V

    .line 280
    return-void
.end method

.method protected onGetPrivacyValueAsyncTaskCompleted()V
    .locals 2

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->updateOptinContent()V

    .line 290
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->updatePrivacyValue(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    .line 291
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->setBusy(Z)V

    .line 292
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->DisplayPhoneContactsDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v0, v1, :cond_0

    .line 181
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsFindFriendsView()V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_3

    .line 234
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    :cond_3
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 260
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    :cond_2
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 274
    return-void
.end method

.method public setBusy(Z)V
    .locals 4
    .param p1, "isBusy"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 99
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->busyIndicator:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz p1, :cond_1

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 102
    return-void

    :cond_0
    move v0, v2

    .line 99
    goto :goto_0

    :cond_1
    move v0, v1

    .line 100
    goto :goto_1

    :cond_2
    move v2, v1

    .line 101
    goto :goto_2
.end method

.method public show(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;Z)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;
    .param p2, "isBusy"    # Z

    .prologue
    .line 105
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->setBusy(Z)V

    .line 106
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->updateContent(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;)V

    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->show()V

    .line 108
    return-void
.end method

.method public updateContent(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;)V
    .locals 6
    .param p1, "state"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 111
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->Error:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne p1, v1, :cond_2

    .line 112
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsErrorView()V

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->errorPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->messagePanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneContactsPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v2, 0x7f070738

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptInDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v1, v2, :cond_0

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_error_message:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070537

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 126
    :goto_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 176
    :goto_1
    return-void

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->OptOutDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v1, v2, :cond_1

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_error_message:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f07055e

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_0

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_error_message:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070522

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_0

    .line 130
    :cond_2
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->messagePanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->errorPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneContactsPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 137
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->DisplayPhoneContactsDialog:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    if-ne v1, v2, :cond_4

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneContactsPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 139
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getPhoneContactsList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneFriendsList:Ljava/util/ArrayList;

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneFriendsList:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneContactsInvitationSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070e05

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_message:Landroid/widget/TextView;

    const v2, 0x7f070e04

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v2, 0x7f070e0e

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 145
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v2, 0x7f070549

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->clear()V

    .line 147
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneFriendsList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->addAll(Ljava/util/Collection;)V

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->notifyDataSetChanged()V

    goto/16 :goto_1

    .line 150
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->phoneContactsInvitationSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f070e0c

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 152
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_message:Landroid/widget/TextView;

    const v2, 0x7f070e0d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v2, 0x7f070e0f

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    goto/16 :goto_1

    .line 158
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 159
    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$friendfinder$PhoneContactsManager$State:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->state:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_1

    .line 161
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v2, 0x7f07055d

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 163
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07055a

    .line 164
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07055b

    .line 166
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "dialogMessage":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->dialog_message:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v2, 0x7f07054a

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    goto/16 :goto_1

    .line 172
    .end local v0    # "dialogMessage":Ljava/lang/String;
    :pswitch_1
    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog;)V

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactFinderDialog$GetPrivacyValueAsyncTask;->load(Z)V

    goto/16 :goto_1

    .line 159
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
