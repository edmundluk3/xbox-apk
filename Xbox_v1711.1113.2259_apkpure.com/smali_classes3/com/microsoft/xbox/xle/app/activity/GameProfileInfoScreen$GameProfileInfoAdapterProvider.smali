.class public Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen$GameProfileInfoAdapterProvider;
.super Ljava/lang/Object;
.source "GameProfileInfoScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "GameProfileInfoAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen$GameProfileInfoAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 57
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    .line 58
    .local v0, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameprofileInfoAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
