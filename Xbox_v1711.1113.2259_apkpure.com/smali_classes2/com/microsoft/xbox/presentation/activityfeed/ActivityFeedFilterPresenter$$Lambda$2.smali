.class final synthetic Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;


# static fields
.field private static final instance:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$2;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$2;->instance:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$2;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$2;->instance:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$2;

    return-object v0
.end method


# virtual methods
.method public bind(Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterView;

    check-cast p1, Lcom/microsoft/xbox/presentation/base/MviView;

    invoke-interface {p1}, Lcom/microsoft/xbox/presentation/base/MviView;->viewIntents()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
