.class public Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;
.super Ljava/lang/Object;
.source "EnforcementViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EnforcementConversationDataContext"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;
    }
.end annotation


# instance fields
.field public final clientMessageId:Ljava/lang/String;

.field public final conversationId:Ljava/lang/String;

.field public final messageType:Ljava/lang/String;

.field public final recipientSkypeId:Ljava/lang/String;

.field public final serverMessageId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;)V
    .locals 1
    .param p1, "clientMessageId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "serverMessageId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "recipientSkypeId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "messageType"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 441
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 442
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 443
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 446
    invoke-static {p1, p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;->clientMessageId:Ljava/lang/String;

    .line 447
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;->serverMessageId:Ljava/lang/String;

    .line 448
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;->conversationId:Ljava/lang/String;

    .line 449
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;->recipientSkypeId:Ljava/lang/String;

    .line 450
    invoke-virtual {p5}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;->messageType:Ljava/lang/String;

    .line 451
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 455
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
