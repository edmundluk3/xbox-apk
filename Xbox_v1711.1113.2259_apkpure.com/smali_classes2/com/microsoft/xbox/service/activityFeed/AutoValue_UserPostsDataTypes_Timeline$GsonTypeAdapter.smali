.class public final Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_UserPostsDataTypes_Timeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultTimelineOwner:Ljava/lang/String;

.field private defaultTimelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

.field private defaultTimelineUri:Ljava/lang/String;

.field private final timelineOwnerAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final timelineTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;",
            ">;"
        }
    .end annotation
.end field

.field private final timelineUriAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->defaultTimelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 23
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->defaultTimelineOwner:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->defaultTimelineUri:Ljava/lang/String;

    .line 26
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->timelineTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 27
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->timelineOwnerAdapter:Lcom/google/gson/TypeAdapter;

    .line 28
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->timelineUriAdapter:Lcom/google/gson/TypeAdapter;

    .line 29
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;
    .locals 6
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_0

    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 62
    const/4 v4, 0x0

    .line 93
    :goto_0
    return-object v4

    .line 64
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 65
    iget-object v2, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->defaultTimelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 66
    .local v2, "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->defaultTimelineOwner:Ljava/lang/String;

    .line 67
    .local v1, "timelineOwner":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->defaultTimelineUri:Ljava/lang/String;

    .line 68
    .local v3, "timelineUri":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 69
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_1

    .line 71
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 74
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 88
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 74
    :sswitch_0
    const-string v5, "timelineType"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "timelineOwner"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_2
    const-string v5, "timelineUri"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x2

    goto :goto_2

    .line 76
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->timelineTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    check-cast v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 77
    .restart local v2    # "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    goto :goto_1

    .line 80
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->timelineOwnerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "timelineOwner":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 81
    .restart local v1    # "timelineOwner":Ljava/lang/String;
    goto :goto_1

    .line 84
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->timelineUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "timelineUri":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 85
    .restart local v3    # "timelineUri":Ljava/lang/String;
    goto :goto_1

    .line 92
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 93
    new-instance v4, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline;

    invoke-direct {v4, v2, v1, v3}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 74
    nop

    :sswitch_data_0
    .sparse-switch
        -0x6a58510e -> :sswitch_1
        -0x2eba7a95 -> :sswitch_2
        0x576acf1b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultTimelineOwner(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTimelineOwner"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->defaultTimelineOwner:Ljava/lang/String;

    .line 36
    return-object p0
.end method

.method public setDefaultTimelineType(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTimelineType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->defaultTimelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 32
    return-object p0
.end method

.method public setDefaultTimelineUri(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTimelineUri"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->defaultTimelineUri:Ljava/lang/String;

    .line 40
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    if-nez p2, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 57
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 50
    const-string v0, "timelineType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->timelineTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->timelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 52
    const-string v0, "timelineOwner"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->timelineOwnerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->timelineOwner()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 54
    const-string v0, "timelineUri"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->timelineUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->timelineUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 56
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    check-cast p2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_Timeline$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;)V

    return-void
.end method
