.class public Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PeopleScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/FollowersData;",
        ">;"
    }
.end annotation


# instance fields
.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 55
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .line 56
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/SearchResultPerson;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/model/SearchResultPerson;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # I
    .param p6, "x6"    # I

    .prologue
    .line 49
    invoke-direct/range {p0 .. p6}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->setTextAndStyleForPeopleScreenTextView(Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/SearchResultPerson;Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    return-object v0
.end method

.method private getClubView(Lcom/microsoft/xbox/service/model/FollowersData;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "followersData"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 115
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;

    if-eqz v1, :cond_0

    .line 116
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;

    .line 123
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;
    :goto_0
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;->bindTo(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 125
    return-object p2

    .line 118
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0301b3

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 119
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;

    invoke-direct {v0, p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/view/View;)V

    .line 120
    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$ClubViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getDiscoverClubsView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 131
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;

    if-eqz v1, :cond_0

    .line 132
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;

    .line 139
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;->bindTo(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V

    .line 141
    return-object p1

    .line 134
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0301b4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 135
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/view/View;)V

    .line 136
    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getDummyView(Lcom/microsoft/xbox/service/model/FollowersData;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "followersData"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 147
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;

    if-eqz v1, :cond_0

    .line 148
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;

    .line 155
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->bindTo(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V

    .line 157
    return-object p2

    .line 150
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0301b5

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 151
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;

    invoke-direct {v0, p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/view/View;)V

    .line 152
    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getPersonView(Lcom/microsoft/xbox/service/model/FollowersData;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "followersData"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 99
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;

    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;

    .line 107
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;
    :goto_0
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;->bindTo(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 109
    return-object p2

    .line 102
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f0301b6

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 103
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;

    invoke-direct {v0, p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/view/View;)V

    .line 104
    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$PersonViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private setTextAndStyleForPeopleScreenTextView(Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/SearchResultPerson;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6
    .param p1, "tv"    # Landroid/widget/TextView;
    .param p2, "srp"    # Lcom/microsoft/xbox/service/model/SearchResultPerson;
    .param p3, "data"    # Ljava/lang/String;
    .param p4, "matchString"    # Ljava/lang/String;
    .param p5, "searchStyle"    # I
    .param p6, "defaultStyle"    # I

    .prologue
    const/16 v5, 0x21

    .line 161
    if-eqz p1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 166
    .local v1, "text":Landroid/text/SpannableString;
    if-eqz p2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->showingPeopleSearchResult()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 167
    invoke-virtual {p3, p4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 168
    .local v0, "matchIndex":I
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v2, v0, v3, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 169
    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p1, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto :goto_0

    .line 171
    .end local v0    # "matchIndex":I
    :cond_2
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v3, 0x0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 172
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 62
    .local v0, "followersData":Lcom/microsoft/xbox/service/model/FollowersData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getType()Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->ordinal()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 74
    .local v0, "followersData":Lcom/microsoft/xbox/service/model/FollowersData;
    if-eqz v0, :cond_1

    .line 75
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersData$FollowerDataType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getType()Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 87
    const-string v2, "Invalid view type"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 92
    :goto_0
    return-object v1

    .line 77
    :pswitch_0
    invoke-direct {p0, v0, p2, p3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->getPersonView(Lcom/microsoft/xbox/service/model/FollowersData;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 79
    :pswitch_1
    invoke-direct {p0, v0, p2, p3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->getClubView(Lcom/microsoft/xbox/service/model/FollowersData;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 81
    :pswitch_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_DISCOVER_CLUBS:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v1, v2, :cond_0

    .line 82
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->getDiscoverClubsView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 84
    :cond_0
    invoke-direct {p0, v0, p2, p3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;->getDummyView(Lcom/microsoft/xbox/service/model/FollowersData;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 91
    :cond_1
    const-string v2, "Unexpected null in list"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 67
    invoke-static {}, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->values()[Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
