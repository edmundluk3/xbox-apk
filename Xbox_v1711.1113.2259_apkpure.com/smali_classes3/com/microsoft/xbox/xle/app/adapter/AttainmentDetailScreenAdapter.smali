.class public Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;
.source "AttainmentDetailScreenAdapter.java"


# instance fields
.field private awardIconView:Landroid/widget/TextView;

.field private currentTimer:Landroid/os/CountDownTimer;

.field private descriptionView:Landroid/widget/TextView;

.field private final friendAchievementDetail:Landroid/widget/LinearLayout;

.field private final friendAchievementDetailText:Landroid/widget/TextView;

.field private friendAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

.field private final friendGamerTagText:Landroid/widget/TextView;

.field private final gameGoToButtonText:Landroid/widget/TextView;

.field private gamerscoreIconView:Landroid/widget/TextView;

.field private gamerscoreTextView:Landroid/widget/TextView;

.field private imageView:Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

.field private ingameRewardLabelView:Landroid/widget/TextView;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;

.field private final meAchievementDetailText:Landroid/widget/TextView;

.field private meAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

.field private rareIcon:Landroid/widget/TextView;

.field private rarityText:Landroid/widget/TextView;

.field private rewardsListLayout:Landroid/widget/LinearLayout;

.field private final shareButton:Landroid/widget/RelativeLayout;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final timerHelper:Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;

.field private timerLabelTextView:Landroid/widget/TextView;

.field private timerTextView:Landroid/widget/TextView;

.field private final titleGoToButton:Landroid/widget/RelativeLayout;

.field private titleView:Landroid/widget/TextView;

.field private final useAsClubBackgroundButton:Landroid/widget/RelativeLayout;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;

    .line 68
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerHelper:Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;

    .line 80
    const v0, 0x7f0e0207

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->screenBody:Landroid/view/View;

    .line 81
    const v0, 0x7f0e020b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 83
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    .line 85
    const v0, 0x7f0e020a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    .line 86
    const v0, 0x7f0e01ec

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->titleView:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0e01f1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->descriptionView:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0e01f2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->rarityText:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0e01ee

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->rareIcon:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0e01f0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->gamerscoreTextView:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0e01ed

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->awardIconView:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0e01ef

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->gamerscoreIconView:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0e01fe

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->rewardsListLayout:Landroid/widget/LinearLayout;

    .line 94
    const v0, 0x7f0e01fd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->ingameRewardLabelView:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e01f3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0e01f4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerLabelTextView:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e01ff

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->shareButton:Landroid/widget/RelativeLayout;

    .line 98
    const v0, 0x7f0e0204

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->useAsClubBackgroundButton:Landroid/widget/RelativeLayout;

    .line 99
    const v0, 0x7f0e01f9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetail:Landroid/widget/LinearLayout;

    .line 100
    const v0, 0x7f0e01fa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendGamerTagText:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e01fb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetailText:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e01fc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 103
    const v0, 0x7f0e01f7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementDetailText:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e01f8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 105
    const v0, 0x7f0e0201

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->titleGoToButton:Landroid/widget/RelativeLayout;

    .line 107
    const v0, 0x7f0e0203

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->gameGoToButtonText:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e020c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 110
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030021

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setVisibility(I)V

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->currentTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;
    .param p1, "x1"    # Landroid/os/CountDownTimer;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->currentTimer:Landroid/os/CountDownTimer;

    return-object p1
.end method

.method private createRewardItemView(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v9, 0x7f020055

    const v8, 0x7f020054

    const/4 v7, 0x0

    .line 394
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 395
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f0301eb

    invoke-virtual {v1, v5, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 396
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0e09c3

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    .line 397
    .local v0, "imageView":Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;
    const v5, 0x7f0e09c4

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 399
    .local v2, "textView":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getUnlockedDate()Ljava/util/Date;

    move-result-object v3

    .line 400
    .local v3, "unlockedDate":Ljava/util/Date;
    if-eqz v3, :cond_1

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getPercentComplete()I

    move-result v5

    const/16 v6, 0x64

    if-lt v5, v6, :cond_1

    .line 402
    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->mediaAsset:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;

    if-eqz v5, :cond_0

    .line 403
    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->mediaAsset:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$MediaAsset;->getMediaAssetUri()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, v9, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 406
    :cond_0
    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->name:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 414
    :goto_0
    return-object v4

    .line 410
    :cond_1
    const/4 v5, 0x0

    invoke-virtual {v0, v5, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 411
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->navigateToGamehub()V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->startShareToFlow()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->useAsBackgroundImage()V

    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;

    .line 134
    .local v0, "friendData":Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;
    if-eqz v0, :cond_0

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->navigateToFriendsProfile(Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;)V

    .line 137
    :cond_0
    return-void
.end method

.method private static setFriendsWhoEarnedListViewHeight(Landroid/widget/ListView;)V
    .locals 8
    .param p0, "mListView"    # Landroid/widget/ListView;

    .prologue
    const/4 v7, 0x0

    .line 370
    if-nez p0, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    .line 375
    .local v4, "mListAdapter":Landroid/widget/ListAdapter;
    if-eqz v4, :cond_0

    .line 379
    const/4 v1, 0x0

    .line 380
    .local v1, "height":I
    invoke-virtual {p0}, Landroid/widget/ListView;->getWidth()I

    move-result v6

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 382
    .local v0, "desiredWidth":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 383
    const/4 v6, 0x0

    invoke-interface {v4, v2, v6, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 384
    .local v3, "listItem":Landroid/view/View;
    invoke-virtual {v3, v0, v7}, Landroid/view/View;->measure(II)V

    .line 385
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v1, v6

    .line 382
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 387
    .end local v3    # "listItem":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 388
    .local v5, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v6

    invoke-interface {v4}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    mul-int/2addr v6, v7

    add-int/2addr v6, v1

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 389
    invoke-virtual {p0, v5}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 390
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    goto :goto_0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->onStart()V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->titleGoToButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->titleGoToButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->shareButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->shareButton:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->shareButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->useAsClubBackgroundButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->useAsClubBackgroundButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAttainmentDetailScreenViewModel;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    if-eqz v0, :cond_4

    .line 130
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_4

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 140
    :cond_4
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithResizableList;->onStop()V

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->titleGoToButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->titleGoToButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->shareButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->shareButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->useAsClubBackgroundButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->useAsClubBackgroundButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->currentTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_3

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->currentTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 159
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->currentTimer:Landroid/os/CountDownTimer;

    .line 162
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_4

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 165
    :cond_4
    return-void
.end method

.method public updateViewOverride()V
    .locals 34

    .prologue
    .line 179
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isBusy()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->updateLoadingIndicator(Z)V

    .line 181
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 183
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isSecret()Z

    move-result v5

    if-eqz v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isUnlocked()Z

    move-result v5

    if-nez v5, :cond_4

    .line 184
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    const/4 v6, 0x0

    const v9, 0x7f020054

    const v10, 0x7f020054

    invoke-virtual {v5, v6, v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 196
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->rarityText:Landroid/widget/TextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    .line 198
    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f0705e1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v12}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getRarityPercent()F

    move-result v12

    invoke-static {v12}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v6, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 196
    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 200
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->rareIcon:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getIsRare()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 201
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->titleGoToButton:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTitleName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isGame()Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    :goto_1
    invoke-static {v6, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 203
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getItemGamerScore()Ljava/lang/String;

    move-result-object v24

    .line 204
    .local v24, "gamerscore":Ljava/lang/String;
    if-eqz v24, :cond_7

    .line 205
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->gamerscoreTextView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    .line 206
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isSecret()Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isUnlocked()Z

    move-result v6

    if-nez v6, :cond_1

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f070632

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 205
    .end local v24    # "gamerscore":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, v24

    invoke-static {v5, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 207
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->gamerscoreIconView:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 213
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->ingameRewardLabelView:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 214
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->rewardsListLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 215
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getRewards()Ljava/util/ArrayList;

    move-result-object v28

    .line 216
    .local v28, "rewards":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;>;"
    if-eqz v28, :cond_8

    .line 217
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;

    .line 218
    .local v27, "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->type:Ljava/lang/String;

    sget-object v9, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->Art:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->type:Ljava/lang/String;

    sget-object v9, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->InApp:Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/AchievementRewardType;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 219
    :cond_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->rewardsListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->createRewardItemView(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    .line 220
    .local v25, "itemView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->rewardsListLayout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->ingameRewardLabelView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-eqz v6, :cond_2

    .line 222
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->ingameRewardLabelView:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-static {v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_3

    .line 186
    .end local v25    # "itemView":Landroid/view/View;
    .end local v27    # "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    .end local v28    # "rewards":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;>;"
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    if-eqz v5, :cond_0

    .line 187
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v6

    const v9, 0x7f020053

    const v10, 0x7f020055

    invoke-virtual {v5, v6, v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 188
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isUnlocked()Z

    move-result v5

    if-nez v5, :cond_5

    .line 189
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setAlpha(F)V

    goto/16 :goto_0

    .line 191
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setAlpha(F)V

    goto/16 :goto_0

    .line 201
    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 209
    .restart local v24    # "gamerscore":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->gamerscoreTextView:Landroid/widget/TextView;

    const-string v6, ""

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 210
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->gamerscoreIconView:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_2

    .line 228
    .end local v24    # "gamerscore":Ljava/lang/String;
    .restart local v28    # "rewards":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;>;"
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getUnlockedDate()Ljava/util/Date;

    move-result-object v29

    .line 229
    .local v29, "unlockedDate":Ljava/util/Date;
    if-eqz v29, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getPercentComplete()I

    move-result v5

    const/16 v6, 0x64

    if-lt v5, v6, :cond_10

    .line 230
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v5}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070080

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 231
    invoke-static/range {v29 .. v29}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getValidAchievementUnlockedDateString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v9

    .line 230
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 232
    .local v22, "formattedDate":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementDetailText:Landroid/widget/TextView;

    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-static {v5, v0, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 241
    .end local v22    # "formattedDate":Ljava/lang/String;
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isBusy()Z

    move-result v5

    if-nez v5, :cond_9

    .line 242
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->shareButton:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isUnlocked()Z

    move-result v5

    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isXboxOneGame()Z

    move-result v5

    if-eqz v5, :cond_12

    const/4 v5, 0x1

    :goto_5
    invoke-static {v6, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 243
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->useAsClubBackgroundButton:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isUnlocked()Z

    move-result v5

    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isXboxOneGame()Z

    move-result v5

    if-eqz v5, :cond_13

    const/4 v5, 0x1

    :goto_6
    invoke-static {v6, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 246
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getIsChallenge()Z

    move-result v5

    if-eqz v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isUnlocked()Z

    move-result v5

    if-nez v5, :cond_1a

    .line 247
    new-instance v17, Ljava/util/Date;

    invoke-direct/range {v17 .. v17}, Ljava/util/Date;-><init>()V

    .line 248
    .local v17, "currentDate":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTimeRemaining()Ljava/util/Date;

    move-result-object v5

    if-eqz v5, :cond_19

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTimeRemaining()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v5

    if-lez v5, :cond_19

    .line 249
    const-wide/32 v18, 0x5265c00

    .line 250
    .local v18, "DAY_IN_MILLISECONDS":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTimeRemaining()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    sub-long v7, v10, v12

    .line 251
    .local v7, "timeDiff":J
    const-wide/32 v10, 0x5265c00

    div-long v20, v7, v10

    .line 253
    .local v20, "dayDiff":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getStartDate()Ljava/util/Date;

    move-result-object v4

    .line 254
    .local v4, "startDate":Ljava/util/Date;
    if-eqz v4, :cond_16

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    cmp-long v5, v10, v12

    if-gez v5, :cond_16

    .line 256
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-virtual/range {v17 .. v17}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 257
    .local v2, "startTimeDiff":J
    const-wide/32 v10, 0x5265c00

    div-long v30, v2, v10

    .line 258
    .local v30, "startDateDayDiff":J
    const-wide/16 v10, 0x1

    cmp-long v5, v30, v10

    if-ltz v5, :cond_14

    .line 259
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemainingShort(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v5, v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 260
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0c0142

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 268
    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerLabelTextView:Landroid/widget/TextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f070618

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 289
    .end local v2    # "startTimeDiff":J
    .end local v4    # "startDate":Ljava/util/Date;
    .end local v7    # "timeDiff":J
    .end local v17    # "currentDate":Ljava/util/Date;
    .end local v18    # "DAY_IN_MILLISECONDS":J
    .end local v20    # "dayDiff":J
    .end local v30    # "startDateDayDiff":J
    :goto_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->awardIconView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getHasAvatarReward()Z

    move-result v5

    if-eqz v5, :cond_1b

    const/4 v5, 0x0

    :goto_9
    invoke-static {v6, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 291
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isUnlocked()Z

    move-result v5

    if-nez v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isSecret()Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 292
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->titleView:Landroid/widget/TextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f070083

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 297
    :goto_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isUnlocked()Z

    move-result v5

    if-nez v5, :cond_1d

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isSecret()Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 298
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->descriptionView:Landroid/widget/TextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f070082

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 303
    :goto_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    if-eqz v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isBusy()Z

    move-result v5

    if-nez v5, :cond_a

    .line 304
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setVisibility(I)V

    .line 305
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getPercentComplete()I

    move-result v6

    int-to-long v10, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getPercentComplete()I

    move-result v6

    rsub-int/lit8 v6, v6, 0x64

    int-to-long v12, v6

    invoke-virtual {v5, v10, v11, v12, v13}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setExactValues(JJ)V

    .line 306
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v26

    .line 307
    .local v26, "preferredColor":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 308
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 309
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarAlpha(F)V

    .line 312
    .end local v26    # "preferredColor":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    instance-of v5, v5, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAttainmentDetailScreenViewModel;

    if-nez v5, :cond_b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    instance-of v5, v5, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    if-eqz v5, :cond_f

    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isBusy()Z

    move-result v5

    if-nez v5, :cond_f

    .line 313
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v5, :cond_d

    .line 314
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isBusy()Z

    move-result v5

    if-nez v5, :cond_d

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getFriendsWhoEarnedAchievementData()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_d

    .line 315
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->items:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getFriendsWhoEarnedAchievementData()Ljava/util/List;

    move-result-object v6

    if-eq v5, v6, :cond_d

    .line 316
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getFriendsWhoEarnedAchievementData()Ljava/util/List;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->items:Ljava/util/List;

    .line 318
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_c

    .line 319
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;->clear()V

    .line 321
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->items:Ljava/util/List;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;->addAll(Ljava/util/Collection;)V

    .line 322
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 323
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->setFriendsWhoEarnedListViewHeight(Landroid/widget/ListView;)V

    .line 329
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    instance-of v5, v5, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    if-eqz v5, :cond_20

    .line 330
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetail:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_f

    .line 331
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetail:Landroid/widget/LinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    move-object/from16 v32, v0

    check-cast v32, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    .line 335
    .local v32, "vm":Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendGamerTagText:Landroid/widget/TextView;

    invoke-virtual/range {v32 .. v32}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->getFriendGamertag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 337
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetailText:Landroid/widget/TextView;

    invoke-virtual/range {v32 .. v32}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->getFriendGamertag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 339
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    if-eqz v5, :cond_e

    .line 340
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setVisibility(I)V

    .line 341
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual/range {v32 .. v32}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->getFriendPercentComplete()I

    move-result v6

    int-to-long v10, v6

    invoke-virtual/range {v32 .. v32}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->getFriendPercentComplete()I

    move-result v6

    rsub-int/lit8 v6, v6, 0x64

    int-to-long v12, v6

    invoke-virtual {v5, v10, v11, v12, v13}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setExactValues(JJ)V

    .line 342
    invoke-virtual/range {v32 .. v32}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->getFriendPreferredColor()I

    move-result v33

    .line 343
    .local v33, "youPreferredColor":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 344
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    move/from16 v0, v33

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 345
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarAlpha(F)V

    .line 348
    .end local v33    # "youPreferredColor":I
    :cond_e
    invoke-virtual/range {v32 .. v32}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->getFriendUnlockedDate()Ljava/util/Date;

    move-result-object v23

    .line 349
    .local v23, "friendUnlockedDate":Ljava/util/Date;
    if-eqz v23, :cond_1e

    invoke-virtual/range {v32 .. v32}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->getFriendPercentComplete()I

    move-result v5

    const/16 v6, 0x64

    if-lt v5, v6, :cond_1e

    .line 350
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v5}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070080

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 351
    invoke-static/range {v23 .. v23}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getValidAchievementUnlockedDateString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v9

    .line 350
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 352
    .restart local v22    # "formattedDate":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetailText:Landroid/widget/TextView;

    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-static {v5, v0, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 367
    .end local v22    # "formattedDate":Ljava/lang/String;
    .end local v23    # "friendUnlockedDate":Ljava/util/Date;
    .end local v32    # "vm":Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;
    :cond_f
    :goto_c
    return-void

    .line 234
    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isStarted()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 235
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementDetailText:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    const-string v9, "%s %d%%"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v12}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f070079

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v12}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getPercentComplete()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v6, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v5, v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_4

    .line 237
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->meAchievementDetailText:Landroid/widget/TextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f070079

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v5, v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_4

    .line 242
    :cond_12
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 243
    :cond_13
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 262
    .restart local v2    # "startTimeDiff":J
    .restart local v4    # "startDate":Ljava/util/Date;
    .restart local v7    # "timeDiff":J
    .restart local v17    # "currentDate":Ljava/util/Date;
    .restart local v18    # "DAY_IN_MILLISECONDS":J
    .restart local v20    # "dayDiff":J
    .restart local v30    # "startDateDayDiff":J
    :cond_14
    const-wide/16 v10, 0x0

    cmp-long v5, v2, v10

    if-lez v5, :cond_15

    .line 263
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    const/high16 v6, -0x10000

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTimeRemaining()Ljava/util/Date;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerLabelTextView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerHelper:Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;

    invoke-static/range {v2 .. v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->startCounter(JLjava/util/Date;Landroid/widget/TextView;IJLjava/util/Date;Landroid/widget/TextView;Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;)V

    .line 265
    :cond_15
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemaining(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v5, v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_7

    .line 271
    .end local v2    # "startTimeDiff":J
    .end local v30    # "startDateDayDiff":J
    :cond_16
    const-wide/16 v10, 0x1

    cmp-long v5, v20, v10

    if-ltz v5, :cond_17

    .line 272
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTimeRemaining()Ljava/util/Date;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemainingShort(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v5, v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 273
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v9, 0x7f0c0142

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 280
    :goto_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerLabelTextView:Landroid/widget/TextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f070619

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 275
    :cond_17
    const-wide/16 v10, 0x0

    cmp-long v5, v7, v10

    if-lez v5, :cond_18

    .line 276
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTimeRemaining()Ljava/util/Date;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    const/high16 v11, -0x10000

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerHelper:Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;

    move-object/from16 v16, v0

    invoke-static/range {v7 .. v16}, Lcom/microsoft/xbox/xle/app/XLEUtil;->startCounter(JLjava/util/Date;Landroid/widget/TextView;IJLjava/util/Date;Landroid/widget/TextView;Lcom/microsoft/xbox/xle/app/XLEUtil$TimerHelper;)V

    .line 278
    :cond_18
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTimeRemaining()Ljava/util/Date;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemaining(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v5, v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto :goto_d

    .line 283
    .end local v4    # "startDate":Ljava/util/Date;
    .end local v7    # "timeDiff":J
    .end local v18    # "DAY_IN_MILLISECONDS":J
    .end local v20    # "dayDiff":J
    :cond_19
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTimeRemaining()Ljava/util/Date;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemaining(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v5, v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_8

    .line 286
    .end local v17    # "currentDate":Ljava/util/Date;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->timerTextView:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_8

    .line 289
    :cond_1b
    const/16 v5, 0x8

    goto/16 :goto_9

    .line 294
    :cond_1c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 300
    :cond_1d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->descriptionView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_b

    .line 354
    .restart local v23    # "friendUnlockedDate":Ljava/util/Date;
    .restart local v32    # "vm":Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;
    :cond_1e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isStarted()Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 355
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetailText:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    const-string v9, "%s %d%%"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v12}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f070079

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-virtual/range {v32 .. v32}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->getFriendPercentComplete()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v6, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v5, v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_c

    .line 357
    :cond_1f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetailText:Landroid/widget/TextView;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f070079

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v5, v6, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    goto/16 :goto_c

    .line 362
    .end local v23    # "friendUnlockedDate":Ljava/util/Date;
    .end local v32    # "vm":Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;
    :cond_20
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetail:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_f

    .line 363
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;->friendAchievementDetail:Landroid/widget/LinearLayout;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_c
.end method
