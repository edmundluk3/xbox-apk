.class abstract Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;
.super Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
.source "$AutoValue_OOBESessionDataType_ConsoleSettings.java"


# instance fields
.field private final allowAutoUpdateApps:Ljava/lang/Boolean;

.field private final allowAutoUpdateSystem:Ljava/lang/Boolean;

.field private final allowInstantOn:Ljava/lang/Boolean;

.field private final autoDSTEnabled:Ljava/lang/Boolean;

.field private final clientVersion:Ljava/lang/String;

.field private final currentTimeZone:Ljava/lang/String;

.field private final isNewUser:Ljava/lang/Boolean;

.field private final offConsoleStatus:Ljava/lang/String;

.field private final userEmail:Ljava/lang/String;

.field private final userGamerPicUrl:Ljava/lang/String;

.field private final userGamerTag:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0
    .param p1, "allowAutoUpdateApps"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "allowAutoUpdateSystem"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "currentTimeZone"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "autoDSTEnabled"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "allowInstantOn"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "offConsoleStatus"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "userEmail"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "userGamerPicUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9, "userGamerTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p10, "isNewUser"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p11, "clientVersion"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateApps:Ljava/lang/Boolean;

    .line 36
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateSystem:Ljava/lang/Boolean;

    .line 37
    iput-object p3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->currentTimeZone:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->autoDSTEnabled:Ljava/lang/Boolean;

    .line 39
    iput-object p5, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowInstantOn:Ljava/lang/Boolean;

    .line 40
    iput-object p6, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->offConsoleStatus:Ljava/lang/String;

    .line 41
    iput-object p7, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userEmail:Ljava/lang/String;

    .line 42
    iput-object p8, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerPicUrl:Ljava/lang/String;

    .line 43
    iput-object p9, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerTag:Ljava/lang/String;

    .line 44
    iput-object p10, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->isNewUser:Ljava/lang/Boolean;

    .line 45
    iput-object p11, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->clientVersion:Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method public allowAutoUpdateApps()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AllowAutoUpdateApps"
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateApps:Ljava/lang/Boolean;

    return-object v0
.end method

.method public allowAutoUpdateSystem()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AllowAutoUpdateSystem"
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateSystem:Ljava/lang/Boolean;

    return-object v0
.end method

.method public allowInstantOn()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AllowInstantOn"
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowInstantOn:Ljava/lang/Boolean;

    return-object v0
.end method

.method public autoDSTEnabled()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AutoDSTEnabled"
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->autoDSTEnabled:Ljava/lang/Boolean;

    return-object v0
.end method

.method public clientVersion()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ClientVersion"
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->clientVersion:Ljava/lang/String;

    return-object v0
.end method

.method public currentTimeZone()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CurrentTimeZone"
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->currentTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 144
    if-ne p1, p0, :cond_1

    .line 161
    :cond_0
    :goto_0
    return v1

    .line 147
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    if-eqz v3, :cond_e

    move-object v0, p1

    .line 148
    check-cast v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    .line 149
    .local v0, "that":Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateApps:Ljava/lang/Boolean;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->allowAutoUpdateApps()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateSystem:Ljava/lang/Boolean;

    if-nez v3, :cond_4

    .line 150
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->allowAutoUpdateSystem()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->currentTimeZone:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 151
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->currentTimeZone()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->autoDSTEnabled:Ljava/lang/Boolean;

    if-nez v3, :cond_6

    .line 152
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->autoDSTEnabled()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowInstantOn:Ljava/lang/Boolean;

    if-nez v3, :cond_7

    .line 153
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->allowInstantOn()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->offConsoleStatus:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 154
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->offConsoleStatus()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userEmail:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 155
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->userEmail()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_7
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerPicUrl:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 156
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->userGamerPicUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_8
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerTag:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 157
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->userGamerTag()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_9
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->isNewUser:Ljava/lang/Boolean;

    if-nez v3, :cond_c

    .line 158
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->isNewUser()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_a
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->clientVersion:Ljava/lang/String;

    if-nez v3, :cond_d

    .line 159
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->clientVersion()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 149
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateApps:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->allowAutoUpdateApps()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 150
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateSystem:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->allowAutoUpdateSystem()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 151
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->currentTimeZone:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->currentTimeZone()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 152
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->autoDSTEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->autoDSTEnabled()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 153
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowInstantOn:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->allowInstantOn()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 154
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->offConsoleStatus:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->offConsoleStatus()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_6

    .line 155
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userEmail:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->userEmail()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_7

    .line 156
    :cond_a
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerPicUrl:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->userGamerPicUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_8

    .line 157
    :cond_b
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerTag:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->userGamerTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_9

    .line 158
    :cond_c
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->isNewUser:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->isNewUser()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_a

    .line 159
    :cond_d
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->clientVersion:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;->clientVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    :cond_e
    move v1, v2

    .line 161
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 166
    const/4 v0, 0x1

    .line 167
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 168
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateApps:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 169
    mul-int/2addr v0, v3

    .line 170
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateSystem:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 171
    mul-int/2addr v0, v3

    .line 172
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->currentTimeZone:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 173
    mul-int/2addr v0, v3

    .line 174
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->autoDSTEnabled:Ljava/lang/Boolean;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 175
    mul-int/2addr v0, v3

    .line 176
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowInstantOn:Ljava/lang/Boolean;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 177
    mul-int/2addr v0, v3

    .line 178
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->offConsoleStatus:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    xor-int/2addr v0, v1

    .line 179
    mul-int/2addr v0, v3

    .line 180
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userEmail:Ljava/lang/String;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    xor-int/2addr v0, v1

    .line 181
    mul-int/2addr v0, v3

    .line 182
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerPicUrl:Ljava/lang/String;

    if-nez v1, :cond_7

    move v1, v2

    :goto_7
    xor-int/2addr v0, v1

    .line 183
    mul-int/2addr v0, v3

    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerTag:Ljava/lang/String;

    if-nez v1, :cond_8

    move v1, v2

    :goto_8
    xor-int/2addr v0, v1

    .line 185
    mul-int/2addr v0, v3

    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->isNewUser:Ljava/lang/Boolean;

    if-nez v1, :cond_9

    move v1, v2

    :goto_9
    xor-int/2addr v0, v1

    .line 187
    mul-int/2addr v0, v3

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->clientVersion:Ljava/lang/String;

    if-nez v1, :cond_a

    :goto_a
    xor-int/2addr v0, v2

    .line 189
    return v0

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateApps:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateSystem:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    .line 172
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->currentTimeZone:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 174
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->autoDSTEnabled:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    .line 176
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowInstantOn:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    .line 178
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->offConsoleStatus:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    .line 180
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userEmail:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    .line 182
    :cond_7
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerPicUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    .line 184
    :cond_8
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerTag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    .line 186
    :cond_9
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->isNewUser:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    .line 188
    :cond_a
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->clientVersion:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_a
.end method

.method public isNewUser()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IsNewUser"
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->isNewUser:Ljava/lang/Boolean;

    return-object v0
.end method

.method public offConsoleStatus()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "OffConsoleStatus"
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->offConsoleStatus:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConsoleSettings{allowAutoUpdateApps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateApps:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowAutoUpdateSystem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowAutoUpdateSystem:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currentTimeZone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->currentTimeZone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", autoDSTEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->autoDSTEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allowInstantOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->allowInstantOn:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", offConsoleStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->offConsoleStatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userEmail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userEmail:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userGamerPicUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerPicUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userGamerTag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isNewUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->isNewUser:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clientVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->clientVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userEmail()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "UserEmail"
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userEmail:Ljava/lang/String;

    return-object v0
.end method

.method public userGamerPicUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "UserGamerPicUrl"
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerPicUrl:Ljava/lang/String;

    return-object v0
.end method

.method public userGamerTag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "UserGamerTag"
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_ConsoleSettings;->userGamerTag:Ljava/lang/String;

    return-object v0
.end method
