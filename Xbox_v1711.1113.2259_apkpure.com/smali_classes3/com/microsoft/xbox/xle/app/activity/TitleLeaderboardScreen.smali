.class public Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "TitleLeaderboardScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 21
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attr"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;

    .line 28
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "TitleLeaderboard"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 33
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;->onCreateContentView()V

    .line 35
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 36
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->onCreate()V

    .line 37
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 38
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f030231

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;->setContentView(I)V

    .line 43
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method
