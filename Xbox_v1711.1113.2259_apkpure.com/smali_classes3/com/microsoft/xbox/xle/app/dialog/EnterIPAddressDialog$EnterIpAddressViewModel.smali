.class Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$EnterIpAddressViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;
.source "EnterIPAddressDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EnterIpAddressViewModel"
.end annotation


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/IViewUpdate;)V
    .locals 0
    .param p1, "view"    # Lcom/microsoft/xbox/toolkit/IViewUpdate;

    .prologue
    .line 348
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;-><init>(Lcom/microsoft/xbox/toolkit/IViewUpdate;)V

    .line 349
    return-void
.end method


# virtual methods
.method protected dismissDialog()V
    .locals 1

    .prologue
    .line 353
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissEnterIpAddressDialog()V

    .line 354
    return-void
.end method
