.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCCrash;
.super Ljava/lang/Object;
.source "UTCCrash.java"


# static fields
.field static final MAXCALLSTACKLENGTH:I = 0xc8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static crashExists()Z
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->exists()Z

    move-result v0

    return v0
.end method

.method private static getShortCallStack(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "callstack"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 79
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0xc8

    if-le v4, v5, :cond_0

    .line 83
    const/16 v4, 0xc7

    invoke-virtual {p0, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 86
    :cond_0
    const-string v2, "unknown"

    .line 88
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    const-string v4, "\\n"

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, "frames":[Ljava/lang/String;
    array-length v4, v1

    if-le v4, v6, :cond_1

    .line 90
    const/4 v4, 0x1

    aget-object v3, v1, v4

    .line 91
    .local v3, "target":Ljava/lang/String;
    const-string v4, "[^a-zA-Z0-9.()]"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 98
    .end local v1    # "frames":[Ljava/lang/String;
    .end local v3    # "target":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v2

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "getShortCallStack: %s"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static save(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;-><init>(Ljava/lang/Throwable;)V

    .line 29
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->saveToFile()V

    .line 30
    return-void
.end method

.method public static trackExistingCrash()V
    .locals 14

    .prologue
    .line 37
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCCrash;->crashExists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 39
    :try_start_0
    new-instance v1, Lxbox/smartglass/Crash;

    invoke-direct {v1}, Lxbox/smartglass/Crash;-><init>()V

    .line 41
    .local v1, "crash":Lxbox/smartglass/Crash;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->loadMostRecentCrashData()Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;

    move-result-object v6

    .line 42
    .local v6, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->getAppVersion()Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "crashVersion":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionName()Ljava/lang/String;

    move-result-object v3

    .line 44
    .local v3, "currentVersion":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 48
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->getErrorMessage()Ljava/lang/String;

    move-result-object v5

    .line 49
    .local v5, "errorMessage":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->getCallStack()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "callstack":Ljava/lang/String;
    const/4 v8, 0x1

    invoke-static {v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v8

    invoke-virtual {v1, v8}, Lxbox/smartglass/Crash;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 52
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCCrash;->getShortCallStack(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 53
    .local v7, "shortCallstack":Ljava/lang/String;
    invoke-virtual {v1, v7}, Lxbox/smartglass/Crash;->setCallStack(Ljava/lang/String;)V

    .line 54
    if-nez v5, :cond_2

    const-string v8, "unknown"

    :goto_0
    invoke-virtual {v1, v8}, Lxbox/smartglass/Crash;->setErrorText(Ljava/lang/String;)V

    .line 57
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 58
    const-string v8, "crash:%s, \nCALLSTACK:\n%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    const/4 v10, 0x1

    aput-object v7, v9, v10

    invoke-static {v8, v9}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    .end local v0    # "callstack":Ljava/lang/String;
    .end local v5    # "errorMessage":Ljava/lang/String;
    .end local v7    # "shortCallstack":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->delete()V

    .line 67
    .end local v2    # "crashVersion":Ljava/lang/String;
    .end local v3    # "currentVersion":Ljava/lang/String;
    .end local v6    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    :cond_1
    :goto_1
    return-void

    .restart local v0    # "callstack":Ljava/lang/String;
    .restart local v2    # "crashVersion":Ljava/lang/String;
    .restart local v3    # "currentVersion":Ljava/lang/String;
    .restart local v5    # "errorMessage":Ljava/lang/String;
    .restart local v6    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    .restart local v7    # "shortCallstack":Ljava/lang/String;
    :cond_2
    move-object v8, v5

    .line 54
    goto :goto_0

    .line 60
    .end local v0    # "callstack":Ljava/lang/String;
    .end local v2    # "crashVersion":Ljava/lang/String;
    .end local v3    # "currentVersion":Ljava/lang/String;
    .end local v5    # "errorMessage":Ljava/lang/String;
    .end local v6    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;
    .end local v7    # "shortCallstack":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 61
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v8, "CRASHTELEMETRY"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "An error occurred sending the crash telemetry: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->delete()V

    goto :goto_1

    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCrashModel;->delete()V

    throw v8
.end method
