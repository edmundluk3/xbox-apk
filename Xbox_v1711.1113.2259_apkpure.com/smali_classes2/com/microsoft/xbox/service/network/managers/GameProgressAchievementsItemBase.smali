.class public abstract Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
.super Ljava/lang/Object;
.source "GameProgressAchievementsItemBase.java"


# static fields
.field public static final RARITY_RARE:Ljava/lang/String; = "Rare"

.field private static transient dateFormat:Ljava/text/DateFormat;


# instance fields
.field public description:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public isSecret:Z

.field public lockedDescription:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected percentageComplete:I

.field private rarity:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementRarity;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field protected responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

.field private titleType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/SimpleDateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->percentageComplete:I

    return-void
.end method


# virtual methods
.method public abstract getAchievementIconUri()Ljava/lang/String;
.end method

.method public getAchievementRarity()F
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->rarity:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementRarity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->rarity:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementRarity;

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementRarity;->currentPercentage:F

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract getGamerscore()Ljava/lang/String;
.end method

.method public getIsRare()Z
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->rarity:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementRarity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->rarity:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementRarity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$AchievementRarity;->currentCategory:Ljava/lang/String;

    const-string v1, "Rare"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPercentageComplete()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->percentageComplete:I

    if-gez v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->setPercentageComplete()V

    .line 79
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->percentageComplete:I

    return v0
.end method

.method public abstract getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;
.end method

.method public abstract getRemainingTime()Ljava/util/Date;
.end method

.method public abstract getResponseType()Lcom/microsoft/xbox/service/network/managers/SLSResponseType;
.end method

.method public abstract getTimeUnlocked()Ljava/util/Date;
.end method

.method public getTimeUnlockedString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getTimeUnlocked()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getTimeUnlocked()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getTitleType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->titleType:Ljava/lang/String;

    return-object v0
.end method

.method public abstract hasRewards()Z
.end method

.method public abstract isExpired()Z
.end method

.method protected abstract setPercentageComplete()V
.end method

.method public setTitleType(Ljava/lang/String;)V
    .locals 0
    .param p1, "titleType"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->titleType:Ljava/lang/String;

    .line 72
    return-void
.end method
