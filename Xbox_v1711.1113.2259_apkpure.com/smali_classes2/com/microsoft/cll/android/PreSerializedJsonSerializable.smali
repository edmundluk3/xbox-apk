.class public Lcom/microsoft/cll/android/PreSerializedJsonSerializable;
.super Lcom/microsoft/telemetry/Data;
.source "PreSerializedJsonSerializable.java"


# instance fields
.field public serializedData:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1, "serializedData"    # Ljava/lang/String;
    .param p2, "partCName"    # Ljava/lang/String;
    .param p3, "partBName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p4, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/telemetry/Data;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/microsoft/cll/android/PreSerializedJsonSerializable;->serializedData:Ljava/lang/String;

    move-object v0, p0

    .line 16
    check-cast v0, Lcom/microsoft/telemetry/Data;

    .line 17
    .local v0, "baseData":Lcom/microsoft/telemetry/Data;
    new-instance v1, Lcom/microsoft/telemetry/Domain;

    invoke-direct {v1}, Lcom/microsoft/telemetry/Domain;-><init>()V

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Data;->setBaseData(Lcom/microsoft/telemetry/Domain;)V

    .line 18
    invoke-virtual {v0}, Lcom/microsoft/telemetry/Data;->getBaseData()Lcom/microsoft/telemetry/Domain;

    move-result-object v1

    iput-object p3, v1, Lcom/microsoft/telemetry/Domain;->QualifiedName:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/microsoft/cll/android/PreSerializedJsonSerializable;->QualifiedName:Ljava/lang/String;

    .line 21
    if-eqz p4, :cond_0

    .line 22
    iget-object v1, p0, Lcom/microsoft/cll/android/PreSerializedJsonSerializable;->Attributes:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p4}, Ljava/util/LinkedHashMap;->putAll(Ljava/util/Map;)V

    .line 24
    :cond_0
    return-void
.end method


# virtual methods
.method public serialize(Ljava/io/Writer;)V
    .locals 1
    .param p1, "writer"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/cll/android/PreSerializedJsonSerializable;->serializedData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 29
    return-void
.end method
