.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedPostScreenViewModelBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PinFeedItemAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final itemRoot:Ljava/lang/String;

.field private final model:Lcom/microsoft/xbox/service/model/ProfileModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 173
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 177
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 178
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->itemRoot:Ljava/lang/String;

    .line 179
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 198
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v1

    .line 199
    .local v1, "timelineType":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->access$300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "timelineId":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->itemRoot:Ljava/lang/String;

    const/4 v4, 0x1

    .line 202
    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->pinFeedItem(Ljava/lang/String;ZLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 201
    :goto_0
    return-object v2

    .line 202
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 193
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 189
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 214
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 171
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->updateAdapter()V

    .line 209
    return-void
.end method
