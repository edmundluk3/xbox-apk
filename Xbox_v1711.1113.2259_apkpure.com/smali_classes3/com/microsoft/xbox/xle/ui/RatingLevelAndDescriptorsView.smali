.class public Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;
.super Landroid/widget/LinearLayout;
.source "RatingLevelAndDescriptorsView.java"


# instance fields
.field imageOnly:Z

.field private ratingDescriptorsContainer:Landroid/widget/LinearLayout;

.field private ratingDescriptorsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private ratingDisclaimersContainer:Landroid/widget/LinearLayout;

.field private ratingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private textLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->imageOnly:Z

    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->init(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method private addRatingDescriptor(Ljava/lang/String;)V
    .locals 5
    .param p1, "descriptorText"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 118
    .local v1, "vi":Landroid/view/LayoutInflater;
    const v2, 0x7f0300fa

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 119
    .local v0, "textView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 121
    return-void
.end method

.method private addRatingDisclaimer(Ljava/lang/String;)V
    .locals 5
    .param p1, "disclaimerText"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 125
    .local v1, "vi":Landroid/view/LayoutInflater;
    const v2, 0x7f0300fb

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 126
    .local v0, "textView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 128
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 39
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f0301e0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 40
    const v1, 0x7f0e099d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    .line 41
    const v1, 0x7f0e099b

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 42
    const v1, 0x7f0e099e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    .line 43
    const v1, 0x7f0e099c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->textLayout:Landroid/widget/LinearLayout;

    .line 44
    return-void
.end method


# virtual methods
.method public setRatingLevelAndDescriptors(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;Ljava/util/List;)V
    .locals 9
    .param p1, "rating"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "ratingdescriptors":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;>;"
    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 57
    if-eqz p1, :cond_0

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    if-nez v5, :cond_3

    .line 58
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 59
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 60
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_1

    .line 61
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 63
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v5, :cond_2

    .line 64
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 114
    :cond_2
    return-void

    .line 68
    :cond_3
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_4

    .line 69
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->imageOnly:Z

    if-eqz v5, :cond_7

    move v5, v6

    :goto_0
    invoke-virtual {v8, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 70
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 72
    :cond_4
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_5

    .line 73
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->imageOnly:Z

    if-eqz v5, :cond_8

    move v5, v6

    :goto_1
    invoke-virtual {v8, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 74
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 76
    :cond_5
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v5, :cond_6

    .line 77
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 79
    :cond_6
    iget-object v5, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->RatingImages:Ljava/util/List;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_9

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->RatingImages:Ljava/util/List;

    .line 80
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_9

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->RatingImages:Ljava/util/List;

    .line 81
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;->Url:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 82
    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->LocalizedDetails:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRatingLocalizedDetail;->RatingImages:Ljava/util/List;

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingImage;->Url:Ljava/lang/String;

    invoke-virtual {v6, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 83
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 89
    :goto_2
    iget-object v5, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingDisclaimers:Ljava/util/List;

    if-eqz v5, :cond_a

    iget-object v5, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingDisclaimers:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_a

    .line 90
    iget-object v5, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingDisclaimers:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;

    .line 91
    .local v0, "claimer":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;
    iget-object v6, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;->Text:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->addRatingDisclaimer(Ljava/lang/String;)V

    goto :goto_3

    .end local v0    # "claimer":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDisclaimer;
    :cond_7
    move v5, v7

    .line 69
    goto/16 :goto_0

    :cond_8
    move v5, v7

    .line 73
    goto :goto_1

    .line 85
    :cond_9
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    goto :goto_2

    .line 96
    :cond_a
    if-eqz p2, :cond_2

    .line 97
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v5, :cond_c

    .line 98
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    new-array v2, v5, [Ljava/lang/String;

    .line 99
    .local v2, "descriptorsAsStringArray":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_4
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_b

    .line 100
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;

    .line 101
    .local v4, "ratingDescriptor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->getNonLocalizedDescriptor()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->getGameRatingDescriptor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    .line 99
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 103
    .end local v4    # "ratingDescriptor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;
    :cond_b
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070e6e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v7, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "descriptors":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    .end local v1    # "descriptors":Ljava/lang/String;
    .end local v2    # "descriptorsAsStringArray":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_c
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_2

    .line 107
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;

    .line 108
    .restart local v4    # "ratingDescriptor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;->getNonLocalizedDescriptor()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->getGameRatingDescriptor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->addRatingDescriptor(Ljava/lang/String;)V

    goto :goto_5
.end method

.method public setShowImageOnly(Z)V
    .locals 4
    .param p1, "onlyImage"    # Z

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 47
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->imageOnly:Z

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 49
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDescriptorsContainer:Landroid/widget/LinearLayout;

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->imageOnly:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->ratingDisclaimersContainer:Landroid/widget/LinearLayout;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/RatingLevelAndDescriptorsView;->imageOnly:Z

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 54
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 49
    goto :goto_0

    :cond_3
    move v1, v2

    .line 52
    goto :goto_1
.end method
