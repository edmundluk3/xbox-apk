.class public abstract Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
.super Ljava/lang/Object;
.source "OOBESessionDataType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ConsoleSettings"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static startWith(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    .locals 12
    .param p0, "userEmail"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "userGamerTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "userGamerPicUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "isNewUser"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 162
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 164
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings;

    sget-object v2, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->InProgress:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    .line 170
    invoke-virtual {v2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->name()Ljava/lang/String;

    move-result-object v6

    const-string v11, "1.0"

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    move-object v7, p0

    move-object v8, p2

    move-object v9, p1

    move-object v10, p3

    invoke-direct/range {v0 .. v11}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 164
    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(ZZLjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    .locals 12
    .param p0, "allowAutoUpdateApps"    # Z
    .param p1, "allowAutoUpdateSystem"    # Z
    .param p2, "currentTimeZone"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "autoDSTEnabled"    # Z
    .param p4, "allowInstantOn"    # Z
    .param p5, "userEmail"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "userGamerTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "userGamerPicUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "isNewUser"    # Z

    .prologue
    .line 187
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 188
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings;

    .line 189
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 190
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 192
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 193
    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    sget-object v3, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->Completed:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    .line 194
    invoke-virtual {v3}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->name()Ljava/lang/String;

    move-result-object v6

    .line 198
    invoke-static/range {p8 .. p8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    const-string v11, "1.0"

    move-object v3, p2

    move-object/from16 v7, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v11}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_ConsoleSettings;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 188
    return-object v0
.end method


# virtual methods
.method public abstract allowAutoUpdateApps()Ljava/lang/Boolean;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AllowAutoUpdateApps"
    .end annotation
.end method

.method public abstract allowAutoUpdateSystem()Ljava/lang/Boolean;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AllowAutoUpdateSystem"
    .end annotation
.end method

.method public abstract allowInstantOn()Ljava/lang/Boolean;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AllowInstantOn"
    .end annotation
.end method

.method public abstract autoDSTEnabled()Ljava/lang/Boolean;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AutoDSTEnabled"
    .end annotation
.end method

.method public abstract clientVersion()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ClientVersion"
    .end annotation
.end method

.method public abstract currentTimeZone()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CurrentTimeZone"
    .end annotation
.end method

.method public abstract isNewUser()Ljava/lang/Boolean;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IsNewUser"
    .end annotation
.end method

.method public abstract offConsoleStatus()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "OffConsoleStatus"
    .end annotation
.end method

.method public abstract userEmail()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "UserEmail"
    .end annotation
.end method

.method public abstract userGamerPicUrl()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "UserGamerPicUrl"
    .end annotation
.end method

.method public abstract userGamerTag()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "UserGamerTag"
    .end annotation
.end method
