.class public final Lcom/microsoft/xbox/toolkit/rx/RxViewUtils;
.super Ljava/lang/Object;
.source "RxViewUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method static synthetic lambda$loadMoreOnScroll$0(Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;)Z
    .locals 6
    .param p0, "event"    # Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;->view()Landroid/support/v7/widget/RecyclerView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/LinearLayoutManager;

    .line 27
    .local v0, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;->view()Landroid/support/v7/widget/RecyclerView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v1

    .line 28
    .local v1, "realizedItemCount":I
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v4

    add-int/2addr v4, v1

    int-to-float v2, v4

    .line 29
    .local v2, "realizedWindowPos":F
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->getItemCount()I

    move-result v3

    .line 31
    .local v3, "totalCount":I
    if-eqz v3, :cond_0

    int-to-float v4, v3

    div-float v4, v2, v4

    const/high16 v5, 0x3f400000    # 0.75f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$loadMoreOnScroll$1(Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;)Ljava/lang/Object;
    .locals 1
    .param p0, "event"    # Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    return-object v0
.end method

.method public static loadMoreOnScroll(Landroid/support/v7/widget/RecyclerView;)Lio/reactivex/Observable;
    .locals 4
    .param p0, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 23
    invoke-static {p0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RxRecyclerView;->scrollEvents(Landroid/support/v7/widget/RecyclerView;)Lio/reactivex/Observable;

    move-result-object v0

    .line 24
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/rx/RxViewUtils$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 25
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 33
    invoke-virtual {v0, v2, v3, v1}, Lio/reactivex/Observable;->throttleFirst(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/rx/RxViewUtils$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 23
    return-object v0
.end method
