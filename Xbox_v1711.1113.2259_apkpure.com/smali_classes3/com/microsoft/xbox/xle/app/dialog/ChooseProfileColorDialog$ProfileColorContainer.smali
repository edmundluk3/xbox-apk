.class public interface abstract Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;
.super Ljava/lang/Object;
.source "ChooseProfileColorDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ProfileColorContainer"
.end annotation


# virtual methods
.method public abstract closeColorPickerDialog()V
.end method

.method public abstract getCurrentColorObject()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
.end method

.method public abstract getProfileColorList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setCurrentColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V
.end method
