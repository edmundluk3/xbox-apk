.class public final enum Lcom/microsoft/xbox/service/activityFeed/UserPostsService;
.super Ljava/lang/Enum;
.source "UserPostsService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/UserPostsService;",
        ">;",
        "Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

.field private static final CONTRACT_VERSION:I = 0x2

.field private static final HYDRATE_POST_PREVIEW_ENDPOINT:Ljava/lang/String; = "https://hydrator.xboxlive.com/Preview"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static final USER_POST_ENDPOINT:Ljava/lang/String; = "https://userposts.xboxlive.com/users/me/posts"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    .line 34
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->STATIC_HEADERS:Ljava/util/List;

    .line 45
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 48
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static getHeaders()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v0, Ljava/util/ArrayList;

    sget-object v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->STATIC_HEADERS:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 186
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 187
    .local v1, "legalLocale":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 188
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Accept-Language"

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_0
    return-object v0
.end method

.method static synthetic lambda$getPostPreview$7(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .param p0, "linkText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 180
    const-string v0, "https://hydrator.xboxlive.com/Preview"

    invoke-static {}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->getHeaders()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewRequest;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewRequest;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$postNonClub$0(Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 8
    .param p0, "linkPostData"    # Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x0L
        .end annotation
    .end param
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    const-string v6, "https://userposts.xboxlive.com/users/me/posts"

    invoke-static {}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->getHeaders()Ljava/util/List;

    move-result-object v7

    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;

    if-eqz p0, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Link:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    :goto_0
    sget-object v3, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->User:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-object v2, p1

    move-object v4, p2

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;)V

    .line 62
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 61
    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Text:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    goto :goto_0
.end method

.method static synthetic lambda$postToClub$1(Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;Ljava/lang/String;J)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 8
    .param p0, "linkPostData"    # Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;
    .param p1, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    const-string v6, "https://userposts.xboxlive.com/users/me/posts"

    invoke-static {}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->getHeaders()Ljava/util/List;

    move-result-object v7

    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;

    if-eqz p0, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Link:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    :goto_0
    sget-object v3, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 92
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;)V

    .line 88
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 87
    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Text:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    goto :goto_0
.end method

.method static synthetic lambda$prepareLfgPost$4(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 4
    .param p0, "lfgPostTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 146
    const-string v0, "https://userposts.xboxlive.com/users/me/posts"

    invoke-static {}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->getHeaders()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;

    sget-object v3, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Lfg:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    invoke-direct {v2, v3, p0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)V

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$shareItem$2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 8
    .param p0, "caption"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 112
    const-string v6, "https://userposts.xboxlive.com/users/me/posts"

    invoke-static {}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->getHeaders()Ljava/util/List;

    move-result-object v7

    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->XboxLink:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    sget-object v4, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->User:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-object v2, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$shareItemToClub$3(Ljava/lang/String;Ljava/lang/String;J)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 8
    .param p0, "caption"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 134
    const-string v6, "https://userposts.xboxlive.com/users/me/posts"

    invoke-static {}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->getHeaders()Ljava/util/List;

    move-result-object v7

    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->XboxLink:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    sget-object v4, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$shareLfgPost$5(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;J)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 7
    .param p0, "lfgPostTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "clubId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 157
    const-string v0, "https://userposts.xboxlive.com/users/me/posts"

    invoke-static {}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->getHeaders()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;

    sget-object v3, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Lfg:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    sget-object v4, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, p0, v4, v5}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$shareLfgPost$6(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 5
    .param p0, "lfgPostTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 163
    const-string v0, "https://userposts.xboxlive.com/users/me/posts"

    invoke-static {}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->getHeaders()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;

    sget-object v3, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->Lfg:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    sget-object v4, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->User:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-direct {v2, v3, p0, v4, p1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostRequest;-><init>(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/UserPostsService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityFeed/UserPostsService;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityFeed/UserPostsService;

    return-object v0
.end method


# virtual methods
.method public getPostPreview(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;
    .locals 3
    .param p1, "linkText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 176
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 178
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " getPostPreview of link: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService$$Lambda$8;->lambdaFactory$(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;

    return-object v0
.end method

.method public postNonClub(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x0L
        .end annotation
    .end param
    .param p2, "linkPostData"    # Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 53
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    const-string v2, "postNonClub"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 56
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "xuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 60
    invoke-static {p2, p1, v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    .line 72
    :goto_0
    return-object v1

    .line 70
    :cond_0
    const-string v1, "No currently logged in user"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 71
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    const-string v2, "No currently logged in user"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public postToClub(Ljava/lang/String;JLcom/microsoft/xbox/service/network/managers/WebLinkPostData;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p4, "linkPostData"    # Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 79
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    const-string v1, "postToClub"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   - (clubId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 83
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 84
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 86
    invoke-static {p4, p1, p2, p3}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/WebLinkPostData;Ljava/lang/String;J)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    return-object v0
.end method

.method public prepareLfgPost(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 2
    .param p1, "lfgPostTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 140
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    const-string v1, "prepareLfgPost"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 143
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 145
    invoke-static {p1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    return-object v0
.end method

.method public shareItem(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 4
    .param p1, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "caption"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 100
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    const-string v2, "shareItem"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 103
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 104
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 106
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "   - (locator: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "xuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    invoke-static {p2, p1, v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService$$Lambda$3;->lambdaFactory$(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    .line 116
    :goto_0
    return-object v1

    .line 114
    :cond_0
    const-string v1, "No currently logged in user"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 115
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    const-string v2, "No currently logged in user"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public shareItemToClub(Ljava/lang/String;Ljava/lang/String;J)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 3
    .param p1, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "caption"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    const-string v1, "shareItemToClub"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   - (clubId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 127
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 128
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 129
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p3, p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 131
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   - (locator: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-static {p2, p1, p3, p4}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService$$Lambda$4;->lambdaFactory$(Ljava/lang/String;Ljava/lang/String;J)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    return-object v0
.end method

.method public shareLfgPost(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;J)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .locals 4
    .param p1, "lfgPostTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubId"    # J
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 153
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 155
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-eqz v1, :cond_0

    .line 156
    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;J)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    .line 170
    :goto_0
    return-object v1

    .line 159
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "xuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 162
    invoke-static {p1, v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsService$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    goto :goto_0

    .line 165
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    const-string v2, "No currently logged in user"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/UserPostsService;->TAG:Ljava/lang/String;

    const-string v2, "Did not share LFG Post"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const/4 v1, 0x0

    goto :goto_0
.end method
