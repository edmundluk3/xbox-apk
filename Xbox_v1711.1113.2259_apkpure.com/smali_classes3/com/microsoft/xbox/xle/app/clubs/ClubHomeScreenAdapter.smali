.class public Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubHomeScreenAdapter.java"


# static fields
.field private static final COLUMN_MARGIN:I = 0xf

.field private static final COLUMN_WIDTH:I = 0x64

.field private static final MAX_PLAYER_DISPLAY:I = 0x6


# instance fields
.field private final backgroundImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final bannedText:Landroid/view/View;

.field private final bioHeader:Landroid/view/View;

.field private final bioText:Landroid/widget/TextView;

.field private final customizeButton:Landroid/view/View;

.field private final displayImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final followButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private final followersCount:Landroid/widget/TextView;

.field private final gameGlyph:Landroid/widget/ImageView;

.field private final hereTodayCount:Landroid/widget/TextView;

.field private final inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private final keepClubButton:Landroid/view/View;

.field private final membersCount:Landroid/widget/TextView;

.field private final membershipActionButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private ownerGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private ownerGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private ownerGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private ownerLayout:Landroid/view/View;

.field private ownerRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final playButton:Landroid/view/View;

.field private final playerList:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

.field private playerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;

.field private playersHashCode:I

.field private final playingNow:Landroid/widget/TextView;

.field private final privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private final reportButton:Landroid/view/View;

.field private final rootView:Landroid/view/View;

.field private final suspendedHeader:Landroid/view/View;

.field private final suspendedText:Landroid/widget/TextView;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

.field private tagColor:I

.field private final tagLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

.field private tagsHashCode:I

.field private titlesArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

.field private titlesHashCode:I

.field private final titlesHeader:Landroid/view/View;

.field private final titlesLayout:Landroid/support/v7/widget/RecyclerView;

.field private final titlesLoading:Landroid/view/View;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 93
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 94
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .line 96
    const v0, 0x7f0e0360

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->rootView:Landroid/view/View;

    .line 97
    const v0, 0x7f0e0a95

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 98
    const v0, 0x7f0e0361

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->suspendedHeader:Landroid/view/View;

    .line 99
    const v0, 0x7f0e0362

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->suspendedText:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e0364

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->gameGlyph:Landroid/widget/ImageView;

    .line 101
    const v0, 0x7f0e0365

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 102
    const v0, 0x7f0e0363

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->backgroundImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 103
    const v0, 0x7f0e0366

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->displayImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 104
    const v0, 0x7f0e0368

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->hereTodayCount:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e0369

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playingNow:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e0375

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->membersCount:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e0377

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->followersCount:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e036a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playerList:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .line 109
    const v0, 0x7f0e0378

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    .line 110
    const v0, 0x7f0e036b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->bannedText:Landroid/view/View;

    .line 111
    const v0, 0x7f0e036c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->keepClubButton:Landroid/view/View;

    .line 112
    const v0, 0x7f0e036d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playButton:Landroid/view/View;

    .line 113
    const v0, 0x7f0e036e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->customizeButton:Landroid/view/View;

    .line 114
    const v0, 0x7f0e036f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 117
    const v0, 0x7f0e0370

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->membershipActionButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 118
    const v0, 0x7f0e0371

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->followButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 119
    const v0, 0x7f0e0372

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->reportButton:Landroid/view/View;

    .line 120
    const v0, 0x7f0e0379

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->bioHeader:Landroid/view/View;

    .line 121
    const v0, 0x7f0e037a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->bioText:Landroid/widget/TextView;

    .line 122
    const v0, 0x7f0e037b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesHeader:Landroid/view/View;

    .line 123
    const v0, 0x7f0e037c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesLoading:Landroid/view/View;

    .line 124
    const v0, 0x7f0e037d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesLayout:Landroid/support/v7/widget/RecyclerView;

    .line 126
    const v0, 0x7f0e037e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerLayout:Landroid/view/View;

    .line 127
    const v0, 0x7f0e0380

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 128
    const v0, 0x7f0e0381

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 129
    const v0, 0x7f0e0382

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 130
    const v0, 0x7f0e0384

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->keepClubButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->customizeButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->membershipActionButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->followButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->reportButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerLayout:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->onClickDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->onClickDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->onClickDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->onClickDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->onClickDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->onClickDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->onClickDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->goToOwner()V

    return-void
.end method

.method private makePlayerList(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "userSummaryList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    const/4 v3, 0x6

    .line 347
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x7

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 348
    .local v1, "playerList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 349
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$UserListItem;->with(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$UserListItem;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 352
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_1

    .line 353
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x6

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;->with(I)Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$AndMoreItem;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    :cond_1
    return-object v1
.end method

.method private onClickDecorator(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "clickAction"    # Ljava/lang/Runnable;

    .prologue
    .line 341
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->updateLoadingIndicator(Z)V

    .line 342
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->setButtonsEnabled(Z)V

    .line 343
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 344
    return-void
.end method

.method private setButtonsEnabled(Z)V
    .locals 1
    .param p1, "isEnabled"    # Z

    .prologue
    .line 329
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->keepClubButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 330
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->customizeButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setEnabled(Z)V

    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->membershipActionButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setEnabled(Z)V

    .line 336
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->reportButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 337
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->followButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setEnabled(Z)V

    .line 338
    return-void
.end method

.method private setFollowButton()V
    .locals 3

    .prologue
    .line 311
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getFollowActionIconLabel()Landroid/support/v4/util/Pair;

    move-result-object v0

    .line 312
    .local v0, "buttonLabelPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->followButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, v0, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 313
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->followButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, v0, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 314
    return-void
.end method

.method private setInviteButton()V
    .locals 3

    .prologue
    .line 317
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getInviteButtonIconLabel()Landroid/support/v4/util/Pair;

    move-result-object v0

    .line 318
    .local v0, "buttonLabelPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, v0, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 319
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, v0, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 320
    return-void
.end method

.method private setMembershipActionButton()V
    .locals 3

    .prologue
    .line 323
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getMembershipActionIconLabelPair()Landroid/support/v4/util/Pair;

    move-result-object v0

    .line 324
    .local v0, "buttonLabelPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->membershipActionButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, v0, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 325
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->membershipActionButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, v0, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 326
    return-void
.end method

.method private updateButtonTexts()V
    .locals 0

    .prologue
    .line 294
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->setInviteButton()V

    .line 295
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->setMembershipActionButton()V

    .line 296
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->setFollowButton()V

    .line 297
    return-void
.end method

.method private updateButtonVisibilities()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->keepClubButton:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isKeepClubEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playButton:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isPlayGameEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->customizeButton:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isCustomizeEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->inviteButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isInviteEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 288
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->membershipActionButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isMembershipActionEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->followButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isFollowEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->reportButton:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isReportEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 291
    return-void
.end method

.method private updateOwner()V
    .locals 4

    .prologue
    const v3, 0x7f020125

    .line 300
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    iget-object v0, v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->ownerSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 301
    .local v0, "owner":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerLayout:Landroid/view/View;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 302
    if-eqz v0, :cond_0

    .line 303
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->displayPicUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 304
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->gamertag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->realName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->ownerGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->gamerScore()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    :cond_0
    return-void

    .line 301
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updatePlayerListIfRequired()V
    .locals 3

    .prologue
    .line 272
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getPlayers()Ljava/util/List;

    move-result-object v0

    .line 274
    .local v0, "players":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playerList:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 275
    iget v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playersHashCode:I

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 276
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;->clear()V

    .line 277
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->makePlayerList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;->addAll(Ljava/util/Collection;)V

    .line 279
    :cond_0
    return-void

    .line 274
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateTagAdapterIfRequired()V
    .locals 4

    .prologue
    .line 242
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTags()Ljava/util/List;

    move-result-object v0

    .line 244
    .local v0, "socialTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    iget v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagColor:I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTagColor()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagsHashCode:I

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTagColor()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagColor:I

    .line 246
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagsHashCode:I

    .line 248
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->clear()V

    .line 249
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0046

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->setBackgroundColor(I)V

    .line 250
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 252
    :cond_1
    return-void
.end method

.method private updateTitlesSectionIfRequired()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 255
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTitleImageUris()Ljava/util/List;

    move-result-object v1

    .line 256
    .local v1, "titleImageUris":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTitleIds()Ljava/util/List;

    move-result-object v0

    .line 258
    .local v0, "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesHeader:Landroid/view/View;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v4, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 260
    iget v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesHashCode:I

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v4

    if-eq v2, v4, :cond_0

    .line 261
    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesHashCode:I

    .line 263
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->clear()V

    .line 264
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->addAll(Ljava/util/Collection;)V

    .line 265
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->notifyDataSetChanged()V

    .line 267
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesLoading:Landroid/view/View;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 269
    :cond_0
    return-void

    :cond_1
    move v2, v3

    .line 258
    goto :goto_0
.end method


# virtual methods
.method public onStart()V
    .locals 11

    .prologue
    const/16 v10, 0xf

    .line 146
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 148
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    if-nez v5, :cond_0

    .line 149
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTagColor()I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagColor:I

    .line 151
    new-instance v3, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTags()Ljava/util/List;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 152
    .local v3, "socialTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagsHashCode:I

    .line 154
    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0046

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-direct {v5, v6, v7, v3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 155
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 158
    .end local v3    # "socialTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    if-nez v5, :cond_1

    .line 159
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTitleImageUris()Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 160
    .local v4, "titleImagesUris":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v4}, Ljava/util/List;->hashCode()I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesHashCode:I

    .line 162
    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-direct {v5, v4, v6}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;-><init>(Ljava/util/List;Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;)V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    .line 165
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09001d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v0, v5

    .line 167
    .local v0, "adjustment":I
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    const/16 v6, 0x64

    invoke-static {v5, v10, v6, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->calculateGridColumnCount(Landroid/content/Context;III)I

    move-result v1

    .line 168
    .local v1, "columnNumbers":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesLayout:Landroid/support/v7/widget/RecyclerView;

    new-instance v6, Landroid/support/v7/widget/GridLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct {v6, v7, v1, v8, v9}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;IIZ)V

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 169
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesLayout:Landroid/support/v7/widget/RecyclerView;

    new-instance v6, Lcom/microsoft/xbox/xle/ui/GridSpacingItemDecoration;

    invoke-direct {v6, v1, v10}, Lcom/microsoft/xbox/xle/ui/GridSpacingItemDecoration;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 170
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesLayout:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->titlesArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 173
    .end local v0    # "adjustment":I
    .end local v1    # "columnNumbers":I
    .end local v4    # "titleImagesUris":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;

    if-nez v5, :cond_2

    .line 174
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getPlayers()Ljava/util/List;

    move-result-object v2

    .line 175
    .local v2, "players":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playersHashCode:I

    .line 177
    new-instance v5, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->rootView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f030085

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;

    .line 178
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->makePlayerList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;->addAll(Ljava/util/Collection;)V

    .line 179
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playerList:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playerListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 181
    .end local v2    # "players":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    :cond_2
    return-void
.end method

.method protected updateViewOverride()V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const v7, 0x7f0201fa

    .line 185
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isBusy()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->updateLoadingIndicator(Z)V

    .line 187
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    .line 188
    .local v2, "viewModelState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 190
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_1

    .line 191
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->setButtonsEnabled(Z)V

    .line 193
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getClubTypeHeader()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setLabelText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isTitleClub()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 195
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    const-string v6, ""

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->gameGlyph:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 197
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->gameGlyph:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getGlyphUri()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6, v7, v7}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 203
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getBackgroundImageUri()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 204
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->backgroundImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getBackgroundImageUri()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 207
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getDisplayImageUri()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 208
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->displayImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .line 209
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getDisplayImageUri()Ljava/lang/String;

    move-result-object v6

    .line 208
    invoke-virtual {v3, v6, v7, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 216
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getHereTodayCount()J

    move-result-wide v0

    .line 217
    .local v0, "hereTodayCount":J
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->hereTodayCount:Landroid/widget/TextView;

    const-wide/16 v8, 0x3e8

    cmp-long v3, v0, v8

    if-gez v3, :cond_4

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->playingNow:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getPlayingNowText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->membersCount:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getMembersCount()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->followersCount:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getFollowersCount()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->updateTagAdapterIfRequired()V

    .line 223
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->updateTitlesSectionIfRequired()V

    .line 224
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->updatePlayerListIfRequired()V

    .line 226
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->bannedText:Landroid/view/View;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isViewerBanned()Z

    move-result v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 228
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->suspendedHeader:Landroid/view/View;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSuspended()Z

    move-result v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 229
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->suspendedText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getSuspendedText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->updateButtonVisibilities()V

    .line 232
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->updateButtonTexts()V

    .line 234
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->bioText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 235
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->bioHeader:Landroid/view/View;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    move v3, v4

    :goto_3
    invoke-static {v6, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 237
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->updateOwner()V

    .line 239
    .end local v0    # "hereTodayCount":J
    :cond_1
    return-void

    .line 199
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->privacyHeader:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getPrivacyIcon()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setIconText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->gameGlyph:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 213
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;->displayImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 217
    .restart local v0    # "hereTodayCount":J
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v7, 0x7f070602

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    :cond_5
    move v3, v5

    .line 235
    goto :goto_3
.end method
