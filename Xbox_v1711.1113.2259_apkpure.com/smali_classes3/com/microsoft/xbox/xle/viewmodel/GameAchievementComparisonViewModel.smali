.class public Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "GameAchievementComparisonViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementIdCompare;,
        Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$AchievementHighestProgressCompare;,
        Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private combinedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
            ">;"
        }
    .end annotation
.end field

.field private forceRefresh:Z

.field public heroStats:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;",
            ">;"
        }
    .end annotation
.end field

.field private isLoadingCompareAchievement:Z

.field private isLoadingMeComparisonData:Z

.field private isLoadingStatisticsData:Z

.field private isLoadingTitleData:Z

.field private isLoadingYouComparisonData:Z

.field private loadCompareAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;

.field private loadMeComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;

.field private loadStatisticsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;

.field private loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;

.field private loadYouComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;

.field private maxGamerScore:I

.field private meCurentGamerscore:I

.field private meCurrentAchievementsEarned:I

.field public meGameProgressPercentage:Ljava/lang/String;

.field public meGameProgressValue:I

.field public meMinutesPlayed:Ljava/lang/String;

.field public meMinutesPlayedValue:J

.field private meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private rebuildCombinedList:Z

.field private titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

.field private titleId:J

.field private titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

.field private titleName:Ljava/lang/String;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private youCurentGamerscore:I

.field private youCurrentAchievementsEarned:I

.field public youGameProgressPercentage:Ljava/lang/String;

.field public youGameProgressValue:I

.field public youMinutesPlayed:Ljava/lang/String;

.field public youMinutesPlayedValue:J

.field private youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 8
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 84
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 48
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 80
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->rebuildCombinedList:Z

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->combinedList:Ljava/util/ArrayList;

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedTitleId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleId:J

    .line 86
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedXuid()Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "friendXuid":Ljava/lang/String;
    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleId:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 89
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 91
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 92
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 93
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 94
    return-void

    :cond_0
    move v1, v3

    .line 88
    goto :goto_0

    :cond_1
    move v2, v3

    .line 89
    goto :goto_1
.end method

.method static synthetic access$1002(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingMeComparisonData:Z

    return p1
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->onLoadYouComparisonDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$1402(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingYouComparisonData:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->onLoadStatisticsDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$1602(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingStatisticsData:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->rebuildCombinedList:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingCompareAchievement:Z

    return p1
.end method

.method static synthetic access$2000(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    .line 45
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meCurentGamerscore:I

    return v0
.end method

.method static synthetic access$2002(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meCurentGamerscore:I

    return p1
.end method

.method static synthetic access$2102(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meCurrentAchievementsEarned:I

    return p1
.end method

.method static synthetic access$2200(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    .line 45
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youCurentGamerscore:I

    return v0
.end method

.method static synthetic access$2202(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youCurentGamerscore:I

    return p1
.end method

.method static synthetic access$2302(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youCurrentAchievementsEarned:I

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleId:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->onLoadTitleDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingTitleData:Z

    return p1
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->onLoadMeComparisonDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private loadAchievementsInternal(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v1, 0x0

    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadMeComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadMeComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->cancel()V

    .line 239
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadMeComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;

    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadMeComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->load(Z)V

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadYouComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;

    if-eqz v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadYouComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;->cancel()V

    .line 245
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadYouComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;

    .line 246
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadYouComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;->load(Z)V

    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadStatisticsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;

    if-eqz v0, :cond_2

    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadStatisticsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->cancel()V

    .line 251
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadStatisticsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadStatisticsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->load(Z)V

    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadCompareAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;

    if-eqz v0, :cond_3

    .line 255
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadCompareAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->cancel()V

    .line 257
    :cond_3
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadCompareAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;

    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadCompareAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->load(Z)V

    .line 259
    return-void
.end method

.method private onLoadMeComparisonDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 305
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingMeComparisonData:Z

    .line 307
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 327
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateAdapter()V

    .line 328
    return-void

    .line 311
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getTitleProgressData(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    move-result-object v0

    .line 312
    .local v0, "meTitleProgress":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    if-eqz v0, :cond_0

    .line 313
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meCurentGamerscore:I

    .line 314
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getMaxGamerscore()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->maxGamerScore:I

    .line 315
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getEarnedAchievements()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meCurrentAchievementsEarned:I

    .line 316
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleName:Ljava/lang/String;

    .line 319
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateViewModelState()V

    goto :goto_0

    .line 323
    .end local v0    # "meTitleProgress":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 307
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadStatisticsDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 331
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingStatisticsData:Z

    .line 333
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 345
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateAdapter()V

    .line 346
    return-void

    .line 337
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateViewModelState()V

    goto :goto_0

    .line 341
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 333
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadTitleDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 375
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingTitleData:Z

    .line 376
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 394
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->forceRefresh:Z

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadAchievementsInternal(Z)V

    .line 395
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateAdapter()V

    .line 396
    return-void

    .line 380
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 381
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleName:Ljava/lang/String;

    goto :goto_0

    .line 376
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onLoadYouComparisonDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 399
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingYouComparisonData:Z

    .line 401
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 418
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateAdapter()V

    .line 419
    return-void

    .line 405
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getTitleProgressData(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    move-result-object v0

    .line 406
    .local v0, "youTitleProgress":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    if-eqz v0, :cond_0

    .line 407
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getCurrentGamerscore()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youCurentGamerscore:I

    .line 408
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getEarnedAchievements()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youCurrentAchievementsEarned:I

    .line 410
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateViewModelState()V

    goto :goto_0

    .line 414
    .end local v0    # "youTitleProgress":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 401
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateViewModelState()V
    .locals 4

    .prologue
    .line 350
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->rebuildCombinedList:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingStatisticsData:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingCompareAchievement:Z

    if-nez v2, :cond_1

    .line 351
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->rebuildCombinedList:Z

    .line 352
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    if-eqz v2, :cond_1

    .line 353
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->combinedList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 354
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getOrderedCompareStatisticsList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 355
    .local v1, "statisticsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 356
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->combinedList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 358
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getOrderedCompareAchievementList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 359
    .local v0, "achievementsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 360
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->combinedList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 365
    .end local v0    # "achievementsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;>;"
    .end local v1    # "statisticsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;>;"
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isBusy()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v2, v3, :cond_2

    .line 366
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->combinedList:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->combinedList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 367
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 372
    :cond_2
    :goto_0
    return-void

    .line 369
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getCompareAchievements()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->combinedList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHeroStatsByUser(ILjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .locals 3
    .param p1, "index"    # I
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    .line 291
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->heroStats:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->heroStats:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 292
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->heroStats:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 293
    .local v0, "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->arrangebyfieldid:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 295
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 301
    .end local v0    # "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIsGame()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneGame()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXbox360Game()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    :cond_0
    const/4 v0, 0x1

    .line 194
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMaxGamerscore()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->maxGamerScore:I

    return v0
.end method

.method public getMeCurrentAchievementsEarned()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meCurrentAchievementsEarned:I

    return v0
.end method

.method public getMeCurrentGamerscore()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meCurentGamerscore:I

    return v0
.end method

.method public getMeCurrentRareAchievementScore()I
    .locals 1

    .prologue
    .line 449
    const/4 v0, 0x0

    return v0
.end method

.method public getMeFirstHeroStatistics()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .locals 2

    .prologue
    .line 166
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getHeroStatsByUser(ILjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    move-result-object v0

    return-object v0
.end method

.method public getMeGameProgressPercentage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meGameProgressPercentage:Ljava/lang/String;

    return-object v0
.end method

.method public getMeGameProgressValue()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meGameProgressValue:I

    return v0
.end method

.method public getMeMinutesPlayed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meMinutesPlayed:Ljava/lang/String;

    return-object v0
.end method

.method public getMeMinutesPlayedValue()J
    .locals 2

    .prologue
    .line 158
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meMinutesPlayedValue:J

    return-wide v0
.end method

.method public getMeProfilePicture()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMeSecondHeroStatistics()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getHeroStatsByUser(ILjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    move-result-object v0

    return-object v0
.end method

.method public getRelativeId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1071
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedTitleId()J

    move-result-wide v0

    .line 1072
    .local v0, "titleId":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1073
    const/4 v2, 0x0

    .line 1076
    :goto_0
    return-object v2

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getTitleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleName:Ljava/lang/String;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public getYouCurrentAchievementsEarned()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youCurrentAchievementsEarned:I

    return v0
.end method

.method public getYouCurrentGamerscore()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youCurentGamerscore:I

    return v0
.end method

.method public getYouCurrentRareAchievementScore()I
    .locals 3

    .prologue
    .line 453
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getTitleProgressData(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    move-result-object v0

    .line 455
    .local v0, "youTitleProgress":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    const/4 v1, 0x0

    return v1
.end method

.method public getYouFirstHeroStatistics()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .locals 2

    .prologue
    .line 174
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getHeroStatsByUser(ILjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    move-result-object v0

    return-object v0
.end method

.method public getYouGameProgressPercentage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youGameProgressPercentage:Ljava/lang/String;

    return-object v0
.end method

.method public getYouGameProgressValue()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youGameProgressValue:I

    return v0
.end method

.method public getYouGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getYouMinutesPlayed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youMinutesPlayed:Ljava/lang/String;

    return-object v0
.end method

.method public getYouMinutesPlayedValue()J
    .locals 2

    .prologue
    .line 162
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youMinutesPlayedValue:J

    return-wide v0
.end method

.method public getYouPreferredColor()I
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v0

    return v0
.end method

.method public getYouProfilePicture()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getYouSecondHeroStatistics()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .locals 2

    .prologue
    .line 178
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getHeroStatsByUser(ILjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingTitleData:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingMeComparisonData:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingYouComparisonData:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingStatisticsData:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingCompareAchievement:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->forceRefresh:Z

    .line 224
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-nez v0, :cond_2

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;

    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->cancel()V

    .line 228
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->load(Z)V

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_2
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadAchievementsInternal(Z)V

    goto :goto_0
.end method

.method public navigateToAchievementDetails(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)V
    .locals 4
    .param p1, "compareItem"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    .prologue
    .line 199
    if-nez p1, :cond_0

    .line 211
    :goto_0
    return-void

    .line 203
    :cond_0
    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v2, :cond_1

    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->meAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 205
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :goto_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 206
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putCompareAchievementDetailFriendXuid(Ljava/lang/String;)V

    .line 207
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleId:J

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 208
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putGameProgressAchievement(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    .line 209
    iget-object v2, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putGameCompareProgressAchievement(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V

    .line 210
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/GameProfileFriendAchievementsDetailsScreen;

    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 203
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;->youAchievement:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    goto :goto_1
.end method

.method public navigateToMediaDetails()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1080
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getIsGame()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1081
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>()V

    .line 1082
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    const/16 v1, 0x2329

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setMediaItemTypeFromInt(I)V

    .line 1083
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->titleId:J

    .line 1085
    iget-wide v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->titleId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1086
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 1097
    .end local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    :cond_0
    :goto_0
    return-void

    .line 1089
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;-><init>()V

    .line 1090
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;
    const/16 v1, 0x2328

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->setMediaItemTypeFromInt(I)V

    .line 1091
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->titleId:J

    .line 1093
    iget-wide v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->titleId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1094
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0
.end method

.method public navigateToTitleHub()V
    .locals 4

    .prologue
    .line 214
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 215
    .local v0, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->titleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 217
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 218
    return-void
.end method

.method protected onLoadCompareAchievementsDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    .line 423
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isLoadingCompareAchievement:Z

    .line 425
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 440
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateAdapter()V

    .line 441
    return-void

    .line 429
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateViewModelState()V

    goto :goto_0

    .line 433
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->combinedList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->combinedList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 434
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 425
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 269
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadMeComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadMeComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadMeComparisonDataAsyncTask;->cancel()V

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadYouComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;

    if-eqz v0, :cond_1

    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadYouComparisonDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadYouComparisonDataAsyncTask;->cancel()V

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadStatisticsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;

    if-eqz v0, :cond_2

    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadStatisticsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->cancel()V

    .line 285
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadCompareAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;

    if-eqz v0, :cond_3

    .line 286
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->loadCompareAchievementsDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadXboxOneCompareAchievementsDataAsyncTask;->cancel()V

    .line 288
    :cond_3
    return-void
.end method
