.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$2;
.super Ljava/lang/Object;
.source "AppRTCAudioManager.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->start(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$2;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 4
    .param p1, "focusChange"    # I

    .prologue
    .line 246
    const-string v0, "AUDIOFOCUS_NOT_DEFINED"

    .line 247
    .local v0, "typeOfChange":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 270
    :pswitch_0
    const-string v0, "AUDIOFOCUS_INVALID"

    .line 273
    :goto_0
    const-string v1, "AppRTCAudioManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onAudioFocusChange: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    return-void

    .line 249
    :pswitch_1
    const-string v0, "AUDIOFOCUS_GAIN"

    .line 250
    goto :goto_0

    .line 252
    :pswitch_2
    const-string v0, "AUDIOFOCUS_GAIN_TRANSIENT"

    .line 253
    goto :goto_0

    .line 255
    :pswitch_3
    const-string v0, "AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE"

    .line 256
    goto :goto_0

    .line 258
    :pswitch_4
    const-string v0, "AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK"

    .line 259
    goto :goto_0

    .line 261
    :pswitch_5
    const-string v0, "AUDIOFOCUS_LOSS"

    .line 262
    goto :goto_0

    .line 264
    :pswitch_6
    const-string v0, "AUDIOFOCUS_LOSS_TRANSIENT"

    .line 265
    goto :goto_0

    .line 267
    :pswitch_7
    const-string v0, "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK"

    .line 268
    goto :goto_0

    .line 247
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
