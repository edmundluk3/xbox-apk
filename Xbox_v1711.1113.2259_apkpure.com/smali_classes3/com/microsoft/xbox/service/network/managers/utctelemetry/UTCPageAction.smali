.class public Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;
.super Ljava/lang/Object;
.source "UTCPageAction.java"


# static fields
.field private static final PAGEACTIONVERSION:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static track(Ljava/lang/String;)V
    .locals 1
    .param p0, "actionName"    # Ljava/lang/String;

    .prologue
    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 30
    return-void
.end method

.method public static track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V
    .locals 2
    .param p0, "actionName"    # Ljava/lang/String;
    .param p1, "additionalInfo"    # Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    .prologue
    .line 33
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "pageName":Ljava/lang/String;
    invoke-static {p0, v0, p1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 35
    return-void
.end method

.method public static track(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "actionName"    # Ljava/lang/String;
    .param p1, "pageName"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 39
    return-void
.end method

.method public static track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V
    .locals 9
    .param p0, "actionName"    # Ljava/lang/String;
    .param p1, "pageName"    # Ljava/lang/String;
    .param p2, "additionalInfo"    # Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 43
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 78
    :goto_0
    return-void

    .line 47
    :cond_0
    move-object v2, p1

    .line 49
    .local v2, "updatedPageName":Ljava/lang/String;
    const/4 v3, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v3, :pswitch_data_0

    .line 62
    :goto_2
    new-instance v1, Lxbox/smartglass/PageAction;

    invoke-direct {v1}, Lxbox/smartglass/PageAction;-><init>()V

    .line 65
    .local v1, "pageAction":Lxbox/smartglass/PageAction;
    invoke-virtual {v1, p0}, Lxbox/smartglass/PageAction;->setActionName(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v1, v2}, Lxbox/smartglass/PageAction;->setPageName(Ljava/lang/String;)V

    .line 69
    const/4 v3, 0x1

    invoke-static {v3, p2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(ILcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)Lxbox/smartglass/CommonData;

    move-result-object v3

    invoke-virtual {v1, v3}, Lxbox/smartglass/PageAction;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 70
    const-string v6, "pageActions:%s, pageName:%s, additionalInfo:%s"

    const/4 v3, 0x3

    new-array v7, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v7, v3

    const/4 v3, 0x1

    aput-object v2, v7, v3

    const/4 v8, 0x2

    invoke-virtual {v1}, Lxbox/smartglass/PageAction;->getBaseData()Lcom/microsoft/bond/BondSerializable;

    move-result-object v3

    check-cast v3, Lxbox/smartglass/CommonData;

    invoke-virtual {v3}, Lxbox/smartglass/CommonData;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v8

    invoke-static {v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 75
    .end local v1    # "pageAction":Lxbox/smartglass/PageAction;
    .end local v2    # "updatedPageName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Failed to track page action: %s"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v3, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 49
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "updatedPageName":Ljava/lang/String;
    :sswitch_0
    :try_start_1
    const-string v6, "People Hub"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v3, v4

    goto :goto_1

    :sswitch_1
    const-string v6, "Game Hub"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v3, v5

    goto :goto_1

    :sswitch_2
    const-string v7, "Clubs"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v3, v6

    goto :goto_1

    .line 51
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v2

    .line 52
    goto :goto_2

    .line 54
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v2

    .line 55
    goto :goto_2

    .line 57
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->getCurrentTab()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_2

    .line 49
    :sswitch_data_0
    .sparse-switch
        -0x65b0f039 -> :sswitch_1
        -0x3cc6281c -> :sswitch_0
        0x3e3025d -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
