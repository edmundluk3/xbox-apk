.class public interface abstract Lcom/microsoft/xbox/service/clubs/IClubPresenceService;
.super Ljava/lang/Object;
.source "IClubPresenceService.java"


# virtual methods
.method public abstract updateClubPresence(JJLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Z
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p5    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
