.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "PeopleHubSocialScreenAdapter.java"


# instance fields
.field private currentHashCode:I

.field private final filterSpinner:Landroid/widget/Spinner;

.field private final filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

.field private final noContentStateView:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V
    .locals 4
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 33
    const v0, 0x7f0e0939

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->screenBody:Landroid/view/View;

    .line 34
    const v0, 0x7f0e093a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 35
    const v0, 0x7f0e093b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 36
    const v0, 0x7f0e093d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->noContentStateView:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    .line 39
    const v0, 0x7f0e093c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 44
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x1090008

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    .line 47
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->getModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->getMeProfileFilters()Ljava/util/List;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v1, 0x7f03020a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 58
    return-void

    .line 47
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;->getYouProfileFilters()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-interface {p1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;->onClick(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;

    .line 55
    .local v0, "filter":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackFriendsFilterAction(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)V

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->setFilter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListFilter;)V

    .line 57
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    return-object v0
.end method

.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 62
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->isBusy()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->updateLoadingIndicator(Z)V

    .line 63
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 64
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->noContentStateView:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->getEmptyStateViewText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 66
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->isBusy()Z

    move-result v2

    if-nez v2, :cond_0

    .line 67
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->getFilteredList()Ljava/util/List;

    move-result-object v0

    .line 68
    .local v0, "currentItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;>;"
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v1

    .line 70
    .local v1, "newHashCode":I
    iget v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->currentHashCode:I

    if-eq v2, v1, :cond_0

    .line 71
    iput v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->currentHashCode:I

    .line 73
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->clear()V

    .line 74
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->addAll(Ljava/util/Collection;)V

    .line 75
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->restoreListPosition()V

    .line 76
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListAdapter;->notifyDataSetChanged()V

    .line 79
    .end local v0    # "currentItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialListItems$ISocialListItem;>;"
    .end local v1    # "newHashCode":I
    :cond_0
    return-void
.end method
