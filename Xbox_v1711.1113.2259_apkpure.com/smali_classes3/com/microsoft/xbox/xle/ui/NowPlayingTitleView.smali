.class public abstract Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
.super Ljava/lang/Object;
.source "NowPlayingTitleView.java"


# static fields
.field private static final VIEW_BASE_ID:I = 0x7f000000

.field private static lastViewId:I


# instance fields
.field protected container:Landroid/view/ViewGroup;

.field protected context:Landroid/content/Context;

.field protected inflatedView:Landroid/view/View;

.field private layoutId:I

.field private nowPlayingCompanionButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

.field private nowPlayingDetailsButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

.field private nowPlayingGameProfileButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

.field private nowPlayingHelpButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

.field private nowPlayingLine3:Landroid/widget/TextView;

.field private nowPlayingPanel:Landroid/view/View;

.field private nowPlayingRecordThatButton:Landroid/widget/Button;

.field private nowPlayingSnapFillFullIcon:Landroid/widget/TextView;

.field private nowPlayingSubTitle:Landroid/widget/TextView;

.field private nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private nowPlayingTitle:Landroid/widget/TextView;

.field private nowPlayingUnsnapButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

.field protected panelButtons:Landroid/widget/LinearLayout;

.field private viewId:I

.field protected viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/high16 v0, 0x7f000000

    sput v0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->lastViewId:I

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;I)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "container"    # Landroid/view/ViewGroup;
    .param p4, "layoutId"    # I

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .line 49
    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->context:Landroid/content/Context;

    .line 50
    iput-object p3, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->container:Landroid/view/ViewGroup;

    .line 51
    iput p4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->layoutId:I

    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->initialize()V

    .line 53
    return-void
.end method

.method private canLaunchHelp()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowOOBE()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->titleDetailsFailedToLoad()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public attachToContainer()V
    .locals 3

    .prologue
    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->container:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 67
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e0808

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->panelButtons:Landroid/widget/LinearLayout;

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e0802

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingPanel:Landroid/view/View;

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e0804

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e0805

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingTitle:Landroid/widget/TextView;

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e0806

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingSubTitle:Landroid/widget/TextView;

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e0807

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingLine3:Landroid/widget/TextView;

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e080b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingDetailsButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e0809

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingUnsnapButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e080a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingCompanionButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e080d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingHelpButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e0803

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingSnapFillFullIcon:Landroid/widget/TextView;

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e09a7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingRecordThatButton:Landroid/widget/Button;

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    const v2, 0x7f0e080c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingGameProfileButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingRecordThatButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 81
    .local v0, "recordThatComponent":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowRecordThat()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->canLaunchDetails()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingDetailsButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    new-instance v2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$1;-><init>(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingUnsnapButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    new-instance v2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$2;-><init>(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingCompanionButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    new-instance v2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$3;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$3;-><init>(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingHelpButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    new-instance v2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$4;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$4;-><init>(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingRecordThatButton:Landroid/widget/Button;

    new-instance v2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$5;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$5;-><init>(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->canLaunchGameProfile()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 126
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingGameProfileButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    new-instance v2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$6;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$6;-><init>(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    :cond_2
    return-void

    .line 82
    :cond_3
    const/16 v1, 0x8

    goto :goto_0
.end method

.method protected canLaunchCompanion()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowOOBE()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->hasDefaultCompanion()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected canLaunchDetails()Z
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowOOBE()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->titleDetailsFailedToLoad()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected canLaunchGameProfile()Z
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowOOBE()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getContentType()Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Game:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cleanup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingDetailsButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingDetailsButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingCompanionButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingCompanionButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingHelpButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    if-eqz v0, :cond_2

    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingHelpButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    :cond_2
    return-void
.end method

.method protected getViewId()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewId:I

    return v0
.end method

.method protected initialize()V
    .locals 4

    .prologue
    .line 56
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->layoutId:I

    if-lez v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->context:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 58
    .local v0, "vi":Landroid/view/LayoutInflater;
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->layoutId:I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->container:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    .line 59
    sget v1, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->lastViewId:I

    add-int/lit8 v2, v1, 0x1

    sput v2, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->lastViewId:I

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewId:I

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->inflatedView:Landroid/view/View;

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewId:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 62
    .end local v0    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    return-void
.end method

.method protected setButtonClickListeners(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V
    .locals 0
    .param p1, "button"    # Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;
    .param p3, "longClickListener"    # Landroid/view/View$OnLongClickListener;

    .prologue
    .line 241
    invoke-virtual {p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    invoke-virtual {p1, p3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 243
    return-void
.end method

.method public update()V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 173
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowNowPlaying()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 174
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingPanel:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 176
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getShouldShowOOBE()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 177
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getOOBEImageResourceId()I

    move-result v4

    invoke-static {v1, v4}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;I)V

    .line 194
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingSubTitle()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 197
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingSubTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingLine3:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 204
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingProviderOrArtistName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 205
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingLine3:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 212
    :cond_0
    :goto_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingDetailsButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->canLaunchDetails()Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setVisibility(I)V

    .line 213
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingUnsnapButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->canUnsnap()Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    :goto_4
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setVisibility(I)V

    .line 214
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingCompanionButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->canLaunchCompanion()Z

    move-result v1

    if-eqz v1, :cond_8

    move v1, v2

    :goto_5
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setVisibility(I)V

    .line 215
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingHelpButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->canLaunchHelp()Z

    move-result v1

    if-eqz v1, :cond_9

    move v1, v2

    :goto_6
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setVisibility(I)V

    .line 216
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingGameProfileButton:Lcom/microsoft/xbox/xle/ui/XLEIconButton;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->canLaunchGameProfile()Z

    move-result v4

    if-eqz v4, :cond_a

    :goto_7
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/XLEIconButton;->setVisibility(I)V

    .line 218
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingSnapFillFullIcon:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 219
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingSnapFillFullIcon:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getActiveTitleLocationSymbol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    :cond_1
    :goto_8
    return-void

    .line 179
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    .line 180
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v4, 0x7f0e001d

    new-instance v5, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$7;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView$7;-><init>(Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;)V

    invoke-virtual {v1, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 186
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 187
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_3

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundColor(I)V

    .line 192
    :goto_9
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getTitlePanelImageUri()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingDefaultImageRid()I

    move-result v6

    invoke-virtual {v1, v4, v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 190
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    sget v4, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundColor(I)V

    goto :goto_9

    .line 199
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingSubTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 200
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingSubTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingSubTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 207
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingLine3:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 208
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingLine3:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->getNowPlayingProviderOrArtistName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_6
    move v1, v3

    .line 212
    goto/16 :goto_3

    :cond_7
    move v1, v3

    .line 213
    goto/16 :goto_4

    :cond_8
    move v1, v3

    .line 214
    goto/16 :goto_5

    :cond_9
    move v1, v3

    .line 215
    goto/16 :goto_6

    :cond_a
    move v2, v3

    .line 216
    goto/16 :goto_7

    .line 222
    :cond_b
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->nowPlayingPanel:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_8
.end method

.method public updateViewModel(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    .line 167
    return-void
.end method
