.class final enum Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;
.super Ljava/lang/Enum;
.source "XboxRemoteControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DPAD_LOGIC"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

.field public static final enum CLIENT_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

.field public static final enum CONSOLE_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

.field public static final enum NONE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-instance v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->NONE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    new-instance v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    const-string v1, "CLIENT_SIDE"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->CLIENT_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    new-instance v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    const-string v1, "CONSOLE_SIDE"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->CONSOLE_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    .line 47
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    sget-object v1, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->NONE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->CLIENT_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->CONSOLE_SIDE:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->$VALUES:[Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    const-class v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->$VALUES:[Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$DPAD_LOGIC;

    return-object v0
.end method
