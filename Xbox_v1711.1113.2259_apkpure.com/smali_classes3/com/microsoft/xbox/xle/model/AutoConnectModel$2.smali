.class Lcom/microsoft/xbox/xle/model/AutoConnectModel$2;
.super Ljava/lang/Object;
.source "AutoConnectModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/model/AutoConnectModel;->connectToConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

.field final synthetic val$consoleData:Lcom/microsoft/xbox/xle/model/ConsoleData;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/model/AutoConnectModel;Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    .prologue
    .line 380
    iput-object p1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$2;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$2;->val$consoleData:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 384
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$2;->val$consoleData:Lcom/microsoft/xbox/xle/model/ConsoleData;

    if-eqz v0, :cond_0

    .line 385
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$2;->val$consoleData:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->setCurrentConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$2;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->access$700(Lcom/microsoft/xbox/xle/model/AutoConnectModel;)Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 389
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/AutoConnectModel$2;->this$0:Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 390
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->connectToConsole(ZZ)V

    .line 392
    return-void
.end method
