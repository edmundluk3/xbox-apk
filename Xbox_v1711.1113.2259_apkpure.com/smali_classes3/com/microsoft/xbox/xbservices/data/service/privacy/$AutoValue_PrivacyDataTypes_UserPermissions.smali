.class abstract Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;
.super Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;
.source "$AutoValue_PrivacyDataTypes_UserPermissions.java"


# instance fields
.field private final permissions:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;",
            ">;"
        }
    .end annotation
.end field

.field private final user:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;Lcom/google/common/collect/ImmutableList;)V
    .locals 2
    .param p1, "user"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p2, "permissions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;-><init>()V

    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null user"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->user:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    .line 21
    if-nez p2, :cond_1

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null permissions"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->permissions:Lcom/google/common/collect/ImmutableList;

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 52
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 53
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;

    .line 54
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->user:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;->user()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->permissions:Lcom/google/common/collect/ImmutableList;

    .line 55
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;->permissions()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserPermissions;
    :cond_3
    move v1, v2

    .line 57
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 62
    const/4 v0, 0x1

    .line 63
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->user:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 65
    mul-int/2addr v0, v2

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->permissions:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 67
    return v0
.end method

.method public permissions()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->permissions:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UserPermissions{user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->user:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", permissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->permissions:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public user()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UserPermissions;->user:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    return-object v0
.end method
