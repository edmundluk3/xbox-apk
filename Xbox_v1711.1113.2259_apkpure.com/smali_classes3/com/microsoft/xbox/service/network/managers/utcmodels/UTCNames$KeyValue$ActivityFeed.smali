.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$ActivityFeed;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActivityFeed"
.end annotation


# static fields
.field public static final Achievement:Ljava/lang/String; = "Achievement"

.field public static final ClubsOff:Ljava/lang/String; = "ClubsOff"

.field public static final ClubsOn:Ljava/lang/String; = "ClubsOn"

.field public static final Comment:Ljava/lang/String; = "Comment"

.field public static final FavoritesOff:Ljava/lang/String; = "FavoritesOff"

.field public static final FavoritesOn:Ljava/lang/String; = "FavoritesOn"

.field public static final FriendsOff:Ljava/lang/String; = "FriendsOff"

.field public static final FriendsOn:Ljava/lang/String; = "FriendsOn"

.field public static final Gameclip:Ljava/lang/String; = "Gameclip"

.field public static final GamesOff:Ljava/lang/String; = "GamesOff"

.field public static final GamesOn:Ljava/lang/String; = "GamesOn"

.field public static final ImpressionAuthorPageUser:Ljava/lang/String; = "PageUser"

.field public static final ImpressionAuthorTitleUser:Ljava/lang/String; = "TitleUser"

.field public static final ImpressionAuthorUser:Ljava/lang/String; = "User"

.field public static final ImpressionContentAchievement:Ljava/lang/String; = "ChallengeOrAchievement"

.field public static final ImpressionContentBroadcast:Ljava/lang/String; = "Broadcast"

.field public static final ImpressionContentFollowed:Ljava/lang/String; = "Followed"

.field public static final ImpressionContentGameclip:Ljava/lang/String; = "GameClip"

.field public static final ImpressionContentGamertag:Ljava/lang/String; = "GamertagChanged"

.field public static final ImpressionContentGeneric:Ljava/lang/String; = "GenericFeedContainer"

.field public static final ImpressionContentLegacyAchievement:Ljava/lang/String; = "LegacyAchievement"

.field public static final ImpressionContentRecommendation:Ljava/lang/String; = "SocialRecommendationFeedContainer"

.field public static final ImpressionContentScreenshot:Ljava/lang/String; = "Screenshot"

.field public static final ImpressionContentText:Ljava/lang/String; = "TextEntry"

.field public static final ImpressionContentWebLink:Ljava/lang/String; = "WebLink"

.field public static final ImpressionEditorial:Ljava/lang/String; = "Editorial"

.field public static final Like:Ljava/lang/String; = "Like"

.field public static final PopularNowOff:Ljava/lang/String; = "PopularNowOff"

.field public static final PopularNowOn:Ljava/lang/String; = "PopularNowOn"

.field public static final Screenshot:Ljava/lang/String; = "Screenshot"

.field public static final Share:Ljava/lang/String; = "Share"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
