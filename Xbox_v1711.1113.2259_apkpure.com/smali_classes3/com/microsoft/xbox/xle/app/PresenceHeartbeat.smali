.class final enum Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;
.super Ljava/lang/Enum;
.source "PresenceHeartbeat.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

.field private static final REPORT_INTERVAL_MINS:J = 0x5L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isActiveRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    .line 16
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    sget-object v1, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->$VALUES:[Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    .line 19
    const-class v0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->isActiveRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->isActiveRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 26
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 37
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 39
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->uploadPresence(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$new$0(Ljava/lang/Boolean;)Lio/reactivex/ObservableSource;
    .locals 5
    .param p0, "isActive"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x5

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2, v3, v4}, Lio/reactivex/Observable;->interval(JJLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    .line 31
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$new$1(Ljava/lang/Long;)Ljava/lang/String;
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 34
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$2(Ljava/lang/String;)Z
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private uploadPresence(Ljava/lang/String;)V
    .locals 3
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 46
    sget-object v1, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->TAG:Ljava/lang/String;

    const-string v2, "Heartbeat"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->uploadUserPresenceHeartBeat(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->TAG:Ljava/lang/String;

    const-string v2, "Heartbeat failed: "

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->$VALUES:[Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized setUserState(Z)V
    .locals 2
    .param p1, "isActive"    # Z

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->isActiveRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    monitor-exit p0

    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
