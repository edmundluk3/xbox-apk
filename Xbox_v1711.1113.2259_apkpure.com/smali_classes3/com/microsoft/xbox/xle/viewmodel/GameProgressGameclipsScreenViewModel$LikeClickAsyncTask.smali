.class Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProgressGameclipsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LikeClickAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private capture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

.field private newLikeState:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;ZLjava/lang/String;)V
    .locals 0
    .param p2, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p3, "newLikeState"    # Z
    .param p4, "xuid"    # Ljava/lang/String;

    .prologue
    .line 375
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 376
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->newLikeState:Z

    .line 377
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->capture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 378
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->xuid:Ljava/lang/String;

    .line 379
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 383
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 384
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 399
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->newLikeState:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->capture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->likeCapture(ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 394
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 389
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 390
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 408
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->newLikeState:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->capture:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V

    .line 409
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 370
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LikeClickAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 404
    return-void
.end method
