.class Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;
.super Lcom/microsoft/xbox/smartglass/SessionManagerListener;
.source "Messaging.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/Messaging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/Messaging;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/smartglass/controls/Messaging;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Messaging;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/smartglass/controls/Messaging;Lcom/microsoft/xbox/smartglass/controls/Messaging$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Messaging;
    .param p2, "x1"    # Lcom/microsoft/xbox/smartglass/controls/Messaging$1;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/Messaging;)V

    return-void
.end method


# virtual methods
.method public onMessageReceived(Lcom/microsoft/xbox/smartglass/Message;)V
    .locals 4
    .param p1, "message"    # Lcom/microsoft/xbox/smartglass/Message;

    .prologue
    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Messaging;

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/Message;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/Messaging;->validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/Message;->type:Lcom/microsoft/xbox/smartglass/MessageType;

    sget-object v2, Lcom/microsoft/xbox/smartglass/MessageType;->Json:Lcom/microsoft/xbox/smartglass/MessageType;

    if-ne v1, v2, :cond_0

    instance-of v1, p1, Lcom/microsoft/xbox/smartglass/JsonMessage;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 78
    check-cast v0, Lcom/microsoft/xbox/smartglass/JsonMessage;

    .line 79
    .local v0, "json":Lcom/microsoft/xbox/smartglass/JsonMessage;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Messaging$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Messaging;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/Messaging;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "received"

    iget-object v3, v0, Lcom/microsoft/xbox/smartglass/JsonMessage;->text:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
