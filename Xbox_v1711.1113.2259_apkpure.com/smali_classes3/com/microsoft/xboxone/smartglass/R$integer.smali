.class public final Lcom/microsoft/xboxone/smartglass/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xboxone/smartglass/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final abc_config_activityDefaultDur:I = 0x7f0b0008

.field public static final abc_config_activityShortDur:I = 0x7f0b0009

.field public static final activity_feed_comment_post_text_entry_max_count:I = 0x7f0b000a

.field public static final activity_feed_share_to_feed_text_entry_max_count:I = 0x7f0b000b

.field public static final activity_feed_status_post_text_entry_max_count:I = 0x7f0b000c

.field public static final alertGamertagMaxLength:I = 0x7f0b0000

.field public static final alertRealNameMaxLength:I = 0x7f0b0001

.field public static final app_bar_elevation_anim_duration:I = 0x7f0b000d

.field public static final bottom_sheet_slide_duration:I = 0x7f0b000e

.field public static final cancel_button_image_alpha:I = 0x7f0b000f

.field public static final collectionGridColumnCount:I = 0x7f0b0004

.field public static final collectionGridRowCount:I = 0x7f0b0005

.field public static final compose_message_max_characters:I = 0x7f0b0010

.field public static final compose_message_max_lines:I = 0x7f0b0011

.field public static final compose_message_num_chars_to_show_limit:I = 0x7f0b0012

.field public static final compose_message_with_attachment_max_lines:I = 0x7f0b0013

.field public static final compose_reply_max_lines:I = 0x7f0b0014

.field public static final compose_reply_with_attachment_max_lines:I = 0x7f0b0015

.field public static final create_club_select_type_title_size:I = 0x7f0b0016

.field public static final create_lfg_description_text_max_count:I = 0x7f0b0017

.field public static final customize_profile_bio_text_max_count:I = 0x7f0b0018

.field public static final customize_profile_location_text_max_count:I = 0x7f0b0019

.field public static final default_text_view_ellipsize_lines:I = 0x7f0b001a

.field public static final design_snackbar_text_max_lines:I = 0x7f0b0003

.field public static final detailProvidersGridColCount:I = 0x7f0b0002

.field public static final google_play_services_version:I = 0x7f0b001b

.field public static final home_screen_featured_row_count:I = 0x7f0b0006

.field public static final home_screen_grid_bar_count:I = 0x7f0b001c

.field public static final home_screen_grid_bar_underflow:I = 0x7f0b001d

.field public static final max_column_count_in_quicklplay_list:I = 0x7f0b001e

.field public static final max_search_term_length:I = 0x7f0b001f

.field public static final movieRelatedGridColumnCount:I = 0x7f0b0007

.field public static final mru_row_full_height_percent:I = 0x7f0b0020

.field public static final mru_row_recent_height_percent:I = 0x7f0b0021

.field public static final mru_row_snap_height_percent:I = 0x7f0b0022

.field public static final report_dialog_report_detailed_reason_text_entry_max_count:I = 0x7f0b0023

.field public static final setting_first_last_name_limit:I = 0x7f0b0024

.field public static final status_bar_notification_info_maxnum:I = 0x7f0b0025


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
