.class public final enum Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;
.super Ljava/lang/Enum;
.source "SystemSettingsServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/systemSettings/ISystemSettingsService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/systemSettings/ISystemSettingsService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

.field private static final BACK_COMPAT_ITEMS_STUB_FILE:Ljava/lang/String; = "stubdata/BackCompatItems.json"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/systemSettings/SystemSettingsServiceStub;

    return-object v0
.end method


# virtual methods
.method public getBackCompatItems()Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 21
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/BackCompatItems.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :goto_0
    return-object v1

    .line 22
    :catch_0
    move-exception v0

    .line 23
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method
