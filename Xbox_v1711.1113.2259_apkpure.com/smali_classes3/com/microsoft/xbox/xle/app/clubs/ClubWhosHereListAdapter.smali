.class public Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "ClubWhosHereListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final rowResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p3, "onItemClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 34
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 35
    iput p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->rowResId:I

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->data:Ljava/util/List;

    .line 38
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 39
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public addAll(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    if-eqz p1, :cond_0

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 68
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 62
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;)V

    .line 53
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 43
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    iget v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->rowResId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;Landroid/view/View;)V

    .line 46
    .local v1, "viewHolder":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 47
    return-object v1
.end method
