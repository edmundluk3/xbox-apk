.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "GameProfileInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private displayImageUri:Ljava/lang/String;

.field private followedTitleSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

.field private gameItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

.field private gameTitleId:J

.field private loadGameProfileInfoTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

.field private loadUserTitleFollowingDataAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;

.field toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 6
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 37
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 45
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameTitleId:J

    .line 51
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 52
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 53
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameTitleId:J

    .line 55
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameTitleId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 56
    return-void

    .line 54
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_0

    .line 55
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object p1
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameTitleId:J

    return-wide v0
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-nez v0, :cond_1

    .line 216
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 224
    :goto_0
    return-void

    .line 219
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 222
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public canViewInStore()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneGame()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getData()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    return-object v0
.end method

.method public getDisplayImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->displayImageUri:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->displayImageUri:Ljava/lang/String;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->displayImageUri:Ljava/lang/String;

    return-object v0
.end method

.method public getGameTitleId()J
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameTitleId:J

    return-wide v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadGameProfileInfoTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadGameProfileInfoTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->getIsBusy()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    .line 141
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->getIsBusy()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadUserTitleFollowingDataAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadUserTitleFollowingDataAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;

    .line 142
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    .line 142
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFollowedOnTitle()Z
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->followedTitleSet:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->followedTitleSet:Ljava/util/Set;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameTitleId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public launchGame()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackPlayAction(J)V

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 100
    .local v0, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 102
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 103
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 104
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .line 105
    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v4

    .line 100
    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Ljava/lang/String;IILcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    .line 106
    return-void

    .end local v0    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_0
    move v1, v2

    .line 94
    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v1, 0x0

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadGameProfileInfoTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadGameProfileInfoTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->cancel()V

    .line 231
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadUserTitleFollowingDataAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadUserTitleFollowingDataAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->load(Z)V

    .line 234
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadGameProfileInfoTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

    .line 235
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadGameProfileInfoTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->load(Z)V

    .line 236
    return-void
.end method

.method protected onLoadGameProfileInfoCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v4, 0x0

    .line 166
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 182
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->updateAdapter()V

    .line 183
    return-void

    .line 171
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 172
    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->displayImageUri:Ljava/lang/String;

    .line 173
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 177
    :pswitch_1
    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 178
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onLoadUserTitleFollowingDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 187
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 198
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->updateAdapter()V

    .line 199
    return-void

    .line 191
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getUserTitleFollowingModel()Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->getResult()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->followedTitleSet:Ljava/util/Set;

    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadGameProfileInfoTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadGameProfileInfoTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->cancel()V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->cancel()V

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadUserTitleFollowingDataAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;

    if-eqz v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->loadUserTitleFollowingDataAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->cancel()V

    .line 162
    :cond_2
    return-void
.end method

.method protected onToggleFollowUnfollowCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 202
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissBlocking()V

    .line 203
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 210
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->updateAdapter()V

    .line 211
    return-void

    .line 206
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f0705cb

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public showPlayButton()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameProfileTitleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneGame()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 83
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 82
    :goto_0
    return v0

    .line 83
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggleFollowState()V
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->load(Z)V

    .line 132
    return-void
.end method

.method public viewInStore()V
    .locals 4

    .prologue
    .line 110
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-nez v2, :cond_1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameTitleId:J

    :goto_0
    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackShowInStoreAction(J)V

    .line 113
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 114
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 115
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v2

    const-string v3, "GameHub"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putPurchaseOriginatingSource(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 119
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-nez v0, :cond_0

    .line 120
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Creating empty MediaItem for ViewInStore"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .end local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 122
    .restart local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setNowPlayingTitleId(J)V

    .line 123
    const/16 v2, 0x2329

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setMediaItemTypeFromInt(I)V

    .line 126
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 127
    return-void

    .line 110
    .end local v0    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-wide v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    goto :goto_0
.end method
