.class Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$1;
.super Ljava/lang/Object;
.source "PopularWithFriendsScreenViewModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->processData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/Date;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$1;->this$1:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/util/Pair;Landroid/util/Pair;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ">;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "arg0":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    .local p2, "arg1":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/Date;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 204
    check-cast p1, Landroid/util/Pair;

    check-cast p2, Landroid/util/Pair;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$1;->compare(Landroid/util/Pair;Landroid/util/Pair;)I

    move-result v0

    return v0
.end method
