.class public Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;
.super Ljava/lang/Object;
.source "AppRTCAudioManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$WiredHeadsetReceiver;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
    }
.end annotation


# static fields
.field private static final SPEAKERPHONE_AUTO:Ljava/lang/String; = "auto"

.field private static final SPEAKERPHONE_FALSE:Ljava/lang/String; = "false"

.field private static final SPEAKERPHONE_TRUE:Ljava/lang/String; = "true"

.field private static final TAG:Ljava/lang/String; = "AppRTCAudioManager"


# instance fields
.field private amState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

.field private final apprtcContext:Landroid/content/Context;

.field private audioDevices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;",
            ">;"
        }
    .end annotation
.end field

.field private audioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private audioManager:Landroid/media/AudioManager;

.field private audioManagerEvents:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;

.field private final bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

.field private defaultAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

.field private hasWiredHeadset:Z

.field private proximitySensor:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;

.field private savedAudioMode:I

.field private savedIsMicrophoneMute:Z

.field private savedIsSpeakerPhoneOn:Z

.field private selectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

.field private final useSpeakerphone:Ljava/lang/String;

.field private userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

.field private wiredHeadsetReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/4 v1, -0x2

    iput v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->savedAudioMode:I

    .line 84
    iput-boolean v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->savedIsSpeakerPhoneOn:Z

    .line 85
    iput-boolean v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->savedIsMicrophoneMute:Z

    .line 86
    iput-boolean v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasWiredHeadset:Z

    .line 112
    iput-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->proximitySensor:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;

    .line 119
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    .line 180
    const-string v1, "AppRTCAudioManager"

    const-string v2, "ctor"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-static {}, Lorg/webrtc/ThreadUtils;->checkIsOnMainThread()V

    .line 183
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->apprtcContext:Landroid/content/Context;

    .line 184
    const-string v1, "audio"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    .line 185
    invoke-static {p1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->create(Landroid/content/Context;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 186
    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$WiredHeadsetReceiver;

    invoke-direct {v1, p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$WiredHeadsetReceiver;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->wiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

    .line 187
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->amState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    .line 189
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 190
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string/jumbo v1, "speakerphone_preference"

    const-string v2, "auto"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->useSpeakerphone:Ljava/lang/String;

    .line 193
    const-string v1, "AppRTCAudioManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "useSpeakerphone: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->useSpeakerphone:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->useSpeakerphone:Ljava/lang/String;

    const-string v2, "false"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->EARPIECE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->defaultAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 203
    :goto_0
    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$1;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)V

    invoke-static {p1, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;->create(Landroid/content/Context;Ljava/lang/Runnable;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->proximitySensor:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;

    .line 212
    const-string v1, "AppRTCAudioManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "defaultAudioDevice: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->defaultAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    return-void

    .line 197
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->defaultAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasWiredHeadset:Z

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->onProximitySensorChangedState()V

    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 176
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private hasEarpiece()Z
    .locals 2

    .prologue
    .line 462
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->apprtcContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private hasWiredHeadset()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 474
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method private onProximitySensorChangedState()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->useSpeakerphone:Ljava/lang/String;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->EARPIECE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 139
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->proximitySensor:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;->sensorReportsNearState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 143
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->EARPIECE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setAudioDeviceInternal(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;)V

    goto :goto_0

    .line 147
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setAudioDeviceInternal(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;)V

    goto :goto_0
.end method

.method private registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V
    .locals 1
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;
    .param p2, "filter"    # Landroid/content/IntentFilter;

    .prologue
    .line 434
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->apprtcContext:Landroid/content/Context;

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 435
    return-void
.end method

.method private setAudioDeviceInternal(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .prologue
    const/4 v3, 0x0

    .line 349
    const-string v0, "AppRTCAudioManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setAudioDeviceInternal(device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$3;->$SwitchMap$com$microsoft$xbox$xbservices$data$repository$party$webrtc$AppRTCAudioManager$AudioDevice:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 366
    const-string v0, "AppRTCAudioManager"

    const-string v1, "Invalid audio device selection"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :goto_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->selectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 370
    return-void

    .line 354
    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_0

    .line 357
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_0

    .line 360
    :pswitch_2
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_0

    .line 363
    :pswitch_3
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_0

    .line 352
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setMicrophoneMute(Z)V
    .locals 2
    .param p1, "on"    # Z

    .prologue
    .line 453
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    .line 454
    .local v0, "wasMuted":Z
    if-ne v0, p1, :cond_0

    .line 458
    :goto_0
    return-void

    .line 457
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, p1}, Landroid/media/AudioManager;->setMicrophoneMute(Z)V

    goto :goto_0
.end method

.method private setSpeakerphoneOn(Z)V
    .locals 2
    .param p1, "on"    # Z

    .prologue
    .line 444
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    .line 445
    .local v0, "wasOn":Z
    if-ne v0, p1, :cond_0

    .line 449
    :goto_0
    return-void

    .line 448
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, p1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_0
.end method

.method private unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    .locals 1
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 439
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->apprtcContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 440
    return-void
.end method


# virtual methods
.method public getAudioDevices()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 411
    invoke-static {}, Lorg/webrtc/ThreadUtils;->checkIsOnMainThread()V

    .line 413
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getBluetoothDeviceName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 425
    const/4 v0, 0x0

    .line 426
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    if-eqz v1, :cond_0

    .line 427
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    .line 429
    :cond_0
    return-object v0
.end method

.method public getSelectedAudioDevice()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
    .locals 1

    .prologue
    .line 418
    invoke-static {}, Lorg/webrtc/ThreadUtils;->checkIsOnMainThread()V

    .line 420
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->selectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    return-object v0
.end method

.method public selectAudioDevice(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;)V
    .locals 3
    .param p1, "device"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .prologue
    .line 400
    invoke-static {}, Lorg/webrtc/ThreadUtils;->checkIsOnMainThread()V

    .line 402
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 403
    const-string v0, "AppRTCAudioManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can not select "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from available "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 406
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->updateAudioDeviceState()V

    .line 407
    return-void
.end method

.method public setDefaultAudioDevice(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;)V
    .locals 3
    .param p1, "defaultDevice"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .prologue
    .line 377
    invoke-static {}, Lorg/webrtc/ThreadUtils;->checkIsOnMainThread()V

    .line 379
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$3;->$SwitchMap$com$microsoft$xbox$xbservices$data$repository$party$webrtc$AppRTCAudioManager$AudioDevice:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 391
    const-string v0, "AppRTCAudioManager"

    const-string v1, "Invalid default audio device selection"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :goto_0
    const-string v0, "AppRTCAudioManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDefaultAudioDevice(device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->defaultAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->updateAudioDeviceState()V

    .line 396
    return-void

    .line 381
    :pswitch_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->defaultAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    goto :goto_0

    .line 384
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasEarpiece()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->defaultAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    goto :goto_0

    .line 387
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->defaultAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    goto :goto_0

    .line 379
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public start(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;)V
    .locals 5
    .param p1, "audioManagerEvents"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;

    .prologue
    const/4 v4, 0x0

    .line 217
    const-string v1, "AppRTCAudioManager"

    const-string/jumbo v2, "start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    invoke-static {}, Lorg/webrtc/ThreadUtils;->checkIsOnMainThread()V

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->amState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;->RUNNING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    if-ne v1, v2, :cond_0

    .line 221
    const-string v1, "AppRTCAudioManager"

    const-string v2, "AudioManager is already active"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :goto_0
    return-void

    .line 226
    :cond_0
    const-string v1, "AppRTCAudioManager"

    const-string v2, "AudioManager starts..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManagerEvents:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;

    .line 228
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;->RUNNING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->amState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    .line 231
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->savedAudioMode:I

    .line 232
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->savedIsSpeakerPhoneOn:Z

    .line 233
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->savedIsMicrophoneMute:Z

    .line 234
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasWiredHeadset()Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasWiredHeadset:Z

    .line 237
    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$2;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 278
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v4, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 280
    .local v0, "result":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 281
    const-string v1, "AppRTCAudioManager"

    const-string v2, "Audio focus request granted for VOICE_CALL streams"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setMode(I)V

    .line 292
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setMicrophoneMute(Z)V

    .line 295
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->NONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 296
    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->NONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    iput-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->selectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 297
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 301
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->start()V

    .line 306
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->updateAudioDeviceState()V

    .line 310
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->wiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 311
    const-string v1, "AppRTCAudioManager"

    const-string v2, "AudioManager started"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 283
    :cond_1
    const-string v1, "AppRTCAudioManager"

    const-string v2, "Audio focus request failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 315
    const-string v0, "AppRTCAudioManager"

    const-string/jumbo v1, "stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    invoke-static {}, Lorg/webrtc/ThreadUtils;->checkIsOnMainThread()V

    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->amState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;->RUNNING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    if-eq v0, v1, :cond_0

    .line 319
    const-string v0, "AppRTCAudioManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trying to stop AudioManager in incorrect state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->amState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :goto_0
    return-void

    .line 322
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->amState:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerState;

    .line 324
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->wiredHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 326
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stop()V

    .line 329
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->savedIsSpeakerPhoneOn:Z

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setSpeakerphoneOn(Z)V

    .line 330
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->savedIsMicrophoneMute:Z

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setMicrophoneMute(Z)V

    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->savedAudioMode:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 334
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 335
    iput-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 336
    const-string v0, "AppRTCAudioManager"

    const-string v1, "Abandoned audio focus for VOICE_CALL streams"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->proximitySensor:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;

    if-eqz v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->proximitySensor:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;->stop()V

    .line 340
    iput-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->proximitySensor:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCProximitySensor;

    .line 343
    :cond_1
    iput-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManagerEvents:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;

    .line 344
    const-string v0, "AppRTCAudioManager"

    const-string v1, "AudioManager stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateAudioDeviceState()V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 482
    invoke-static {}, Lorg/webrtc/ThreadUtils;->checkIsOnMainThread()V

    .line 484
    const-string v7, "AppRTCAudioManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "--- updateAudioDeviceState: wired headset="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasWiredHeadset:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", BT state="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 486
    invoke-virtual {v9}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 484
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    const-string v7, "AppRTCAudioManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Device status: available="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", selected="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->selectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", user selected="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_AVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 496
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_UNAVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 497
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_DISCONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v7, v8, :cond_1

    .line 498
    :cond_0
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->updateDevice()V

    .line 502
    :cond_1
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 504
    .local v4, "newAudioDevices":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;>;"
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v7, v8, :cond_2

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 505
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v7, v8, :cond_2

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 506
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_AVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v7, v8, :cond_3

    .line 507
    :cond_2
    sget-object v7, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->BLUETOOTH:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 510
    :cond_3
    iget-boolean v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasWiredHeadset:Z

    if-eqz v7, :cond_10

    .line 512
    sget-object v7, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->WIRED_HEADSET:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 522
    :cond_4
    :goto_0
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    invoke-interface {v7, v4}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_11

    move v0, v5

    .line 524
    .local v0, "audioDeviceSetUpdated":Z
    :goto_1
    iput-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    .line 526
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_UNAVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v7, v8, :cond_5

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->BLUETOOTH:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    if-ne v7, v8, :cond_5

    .line 529
    sget-object v7, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->NONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    iput-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 531
    :cond_5
    iget-boolean v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasWiredHeadset:Z

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    if-ne v7, v8, :cond_6

    .line 534
    sget-object v7, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->WIRED_HEADSET:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    iput-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 536
    :cond_6
    iget-boolean v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasWiredHeadset:Z

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->WIRED_HEADSET:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    if-ne v7, v8, :cond_7

    .line 539
    sget-object v7, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    iput-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 544
    :cond_7
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 545
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_AVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v7, v8, :cond_12

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->NONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    if-eq v7, v8, :cond_8

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->BLUETOOTH:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    if-ne v7, v8, :cond_12

    :cond_8
    move v1, v5

    .line 551
    .local v1, "needBluetoothAudioStart":Z
    :goto_2
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 552
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v7, v8, :cond_9

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 553
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v7, v8, :cond_13

    :cond_9
    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->NONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    if-eq v7, v8, :cond_13

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->userSelectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    sget-object v8, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->BLUETOOTH:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    if-eq v7, v8, :cond_13

    move v2, v5

    .line 557
    .local v2, "needBluetoothAudioStop":Z
    :goto_3
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->HEADSET_AVAILABLE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v5, v6, :cond_a

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 558
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-eq v5, v6, :cond_a

    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 559
    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v5, v6, :cond_b

    .line 560
    :cond_a
    const-string v5, "AppRTCAudioManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Need BT audio: start="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", stop="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", BT state="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 562
    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 560
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    :cond_b
    if-eqz v2, :cond_c

    .line 567
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stopScoAudio()V

    .line 568
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->updateDevice()V

    .line 571
    :cond_c
    if-eqz v1, :cond_d

    if-nez v2, :cond_d

    .line 573
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->startScoAudio()Z

    move-result v5

    if-nez v5, :cond_d

    .line 575
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    sget-object v6, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->BLUETOOTH:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-interface {v5, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 576
    const/4 v0, 0x1

    .line 581
    :cond_d
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->selectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 583
    .local v3, "newAudioDevice":Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->bluetoothManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->getState()Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v5, v6, :cond_14

    .line 587
    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->BLUETOOTH:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 600
    :goto_4
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->selectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    if-ne v3, v5, :cond_e

    if-eqz v0, :cond_f

    .line 602
    :cond_e
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->setAudioDeviceInternal(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;)V

    .line 603
    const-string v5, "AppRTCAudioManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "New device status: available="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", selected="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManagerEvents:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;

    if-eqz v5, :cond_f

    .line 608
    iget-object v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioManagerEvents:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->selectedAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->audioDevices:Ljava/util/Set;

    invoke-interface {v5, v6, v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;->onAudioDeviceChanged(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;Ljava/util/Set;)V

    .line 611
    :cond_f
    const-string v5, "AppRTCAudioManager"

    const-string v6, "--- updateAudioDeviceState done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    return-void

    .line 516
    .end local v0    # "audioDeviceSetUpdated":Z
    .end local v1    # "needBluetoothAudioStart":Z
    .end local v2    # "needBluetoothAudioStop":Z
    .end local v3    # "newAudioDevice":Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
    :cond_10
    sget-object v7, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 517
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasEarpiece()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 518
    sget-object v7, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->EARPIECE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_11
    move v0, v6

    .line 522
    goto/16 :goto_1

    .restart local v0    # "audioDeviceSetUpdated":Z
    :cond_12
    move v1, v6

    .line 545
    goto/16 :goto_2

    .restart local v1    # "needBluetoothAudioStart":Z
    :cond_13
    move v2, v6

    .line 553
    goto/16 :goto_3

    .line 588
    .restart local v2    # "needBluetoothAudioStop":Z
    .restart local v3    # "newAudioDevice":Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
    :cond_14
    iget-boolean v5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->hasWiredHeadset:Z

    if-eqz v5, :cond_15

    .line 591
    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->WIRED_HEADSET:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    goto :goto_4

    .line 597
    :cond_15
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->defaultAudioDevice:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    goto :goto_4
.end method
