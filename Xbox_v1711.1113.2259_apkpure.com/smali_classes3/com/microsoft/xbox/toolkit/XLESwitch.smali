.class public Lcom/microsoft/xbox/toolkit/XLESwitch;
.super Landroid/widget/Switch;
.source "XLESwitch.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Landroid/widget/Switch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 11
    return-void
.end method


# virtual methods
.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLESwitch;->getMeasuredHeight()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/XLESwitch;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_0

    .line 15
    invoke-super {p0, p1}, Landroid/widget/Switch;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 17
    :cond_0
    return-void
.end method
