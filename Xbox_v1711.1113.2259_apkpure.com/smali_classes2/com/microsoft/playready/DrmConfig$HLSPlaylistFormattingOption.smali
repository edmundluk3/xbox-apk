.class public final enum Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;
.super Ljava/lang/Enum;
.source "DrmConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/DrmConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HLSPlaylistFormattingOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEFAULT:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

.field private static final synthetic ENUM$VALUES:[Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

.field public static final enum NONE:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

.field public static final enum STRICT:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

.field public static final enum VARIANT1:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;-><init>(Ljava/lang/String;I)V

    .line 28
    sput-object v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->NONE:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    .line 30
    new-instance v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;-><init>(Ljava/lang/String;I)V

    .line 40
    sput-object v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->DEFAULT:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    .line 42
    new-instance v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    const-string v1, "STRICT"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;-><init>(Ljava/lang/String;I)V

    .line 50
    sput-object v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->STRICT:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    .line 52
    new-instance v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    const-string v1, "VARIANT1"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;-><init>(Ljava/lang/String;I)V

    .line 65
    sput-object v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->VARIANT1:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->NONE:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->DEFAULT:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->STRICT:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->VARIANT1:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->ENUM$VALUES:[Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->ENUM$VALUES:[Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    array-length v1, v0

    new-array v2, v1, [Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
