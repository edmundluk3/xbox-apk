.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
.end method

.method public abstract chat(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
.end method

.method public abstract feed(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
.end method

.method public abstract lfg(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
.end method

.method public abstract profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
.end method

.method public abstract roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
.end method

.method public abstract viewerRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
.end method
