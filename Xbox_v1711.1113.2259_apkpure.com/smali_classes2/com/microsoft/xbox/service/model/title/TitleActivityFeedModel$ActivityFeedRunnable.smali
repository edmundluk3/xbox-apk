.class Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleActivityFeedModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityFeedRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final continuationToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "continuationToken"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;->this$0:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 118
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;->continuationToken:Ljava/lang/String;

    .line 119
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 130
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;->this$0:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->access$100(Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;)J

    move-result-wide v2

    new-instance v1, Ljava/util/Date;

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide v6, 0x1cf7c5800L

    sub-long/2addr v4, v6

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->dateToUrlFormat(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;->continuationToken:Ljava/lang/String;

    .line 130
    invoke-interface {v0, v2, v3, v1, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getTitleActivityFeedInfo(JLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;->buildData()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 142
    const-wide/16 v0, 0xc85

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;->this$0:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 138
    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;->continuationToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel$ActivityFeedRunnable;->this$0:Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;->access$002(Lcom/microsoft/xbox/service/model/title/TitleActivityFeedModel;Z)Z

    .line 126
    :cond_0
    return-void
.end method
