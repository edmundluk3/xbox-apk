.class public Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;
.super Landroid/widget/BaseAdapter;
.source "EPGRecentChannelsView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$IEPGAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EPGRecentChannelsAdapter"
.end annotation


# instance fields
.field private final mBackingAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

.field private mItemHeight:I

.field private mItemWidth:I

.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;Landroid/content/Context;Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "backingAdapter"    # Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    .prologue
    const/high16 v0, -0x80000000

    .line 182
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->this$0:Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 179
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mItemHeight:I

    .line 180
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mItemWidth:I

    .line 183
    iput-object p3, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mBackingAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    .line 184
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mBackingAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mBackingAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->get(I)Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 205
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/high16 v6, -0x80000000

    .line 210
    if-nez p2, :cond_0

    .line 211
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->this$0:Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f0300f5

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 214
    :cond_0
    iget v3, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mItemHeight:I

    if-eq v3, v6, :cond_1

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mItemWidth:I

    if-eq v3, v6, :cond_1

    .line 215
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mItemWidth:I

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mItemHeight:I

    invoke-direct {v2, v3, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 216
    .local v2, "lParams":Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {p2, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 219
    .end local v2    # "lParams":Landroid/widget/AbsListView$LayoutParams;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mBackingAdapter:Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;->get(I)Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    .line 221
    .local v0, "data":Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    if-eqz v0, :cond_2

    move-object v1, p2

    .line 222
    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;

    .line 223
    .local v1, "inflatedView":Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->setChannel(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    .line 226
    .end local v1    # "inflatedView":Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;
    :cond_2
    return-object p2
.end method

.method public setDimens(II)V
    .locals 0
    .param p1, "itemHeight"    # I
    .param p2, "itemWidth"    # I

    .prologue
    .line 188
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mItemHeight:I

    .line 189
    iput p2, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelsView$EPGRecentChannelsAdapter;->mItemWidth:I

    .line 190
    return-void
.end method
