.class public abstract Lcom/microsoft/cll/android/AbstractHandler;
.super Ljava/lang/Object;
.source "AbstractHandler.java"


# static fields
.field protected static final criticalEventFileExtension:Ljava/lang/String; = ".crit.cllevent"

.field protected static final normalEventFileExtension:Ljava/lang/String; = ".norm.cllevent"

.field protected static totalStorageUsed:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final TAG:Ljava/lang/String;

.field protected final clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

.field protected filePath:Ljava/lang/String;

.field protected fileStorage:Lcom/microsoft/cll/android/FileStorage;

.field protected final logger:Lcom/microsoft/cll/android/ILogger;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 22
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/microsoft/cll/android/AbstractHandler;->totalStorageUsed:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/ClientTelemetry;)V
    .locals 1
    .param p1, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "AbstractHandler"

    iput-object v0, p0, Lcom/microsoft/cll/android/AbstractHandler;->TAG:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/microsoft/cll/android/AbstractHandler;->filePath:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/microsoft/cll/android/AbstractHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 27
    iput-object p3, p0, Lcom/microsoft/cll/android/AbstractHandler;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 29
    invoke-direct {p0}, Lcom/microsoft/cll/android/AbstractHandler;->setFileStorageUsed()V

    .line 30
    return-void
.end method

.method private setFileStorageUsed()V
    .locals 8

    .prologue
    .line 101
    sget-object v4, Lcom/microsoft/cll/android/AbstractHandler;->totalStorageUsed:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 104
    const-string v4, ".crit.cllevent"

    invoke-virtual {p0, v4}, Lcom/microsoft/cll/android/AbstractHandler;->findExistingFiles(Ljava/lang/String;)[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 105
    .local v1, "file":Ljava/io/File;
    sget-object v4, Lcom/microsoft/cll/android/AbstractHandler;->totalStorageUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 104
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 109
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    const-string v4, ".norm.cllevent"

    invoke-virtual {p0, v4}, Lcom/microsoft/cll/android/AbstractHandler;->findExistingFiles(Ljava/lang/String;)[Ljava/io/File;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 110
    .restart local v1    # "file":Ljava/io/File;
    sget-object v4, Lcom/microsoft/cll/android/AbstractHandler;->totalStorageUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 109
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 112
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    return-void
.end method


# virtual methods
.method public abstract add(Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/microsoft/cll/android/FileStorage$FileFullException;
        }
    .end annotation
.end method

.method public canAdd(Lcom/microsoft/cll/android/Tuple;)Z
    .locals 4
    .param p1, "serializedEvent"    # Lcom/microsoft/cll/android/Tuple;

    .prologue
    .line 46
    sget-object v0, Lcom/microsoft/cll/android/AbstractHandler;->totalStorageUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    iget-object v0, p1, Lcom/microsoft/cll/android/Tuple;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    sget-object v2, Lcom/microsoft/cll/android/SettingsStore$Settings;->MAXFILESSPACE:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v2}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 47
    const/4 v0, 0x1

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract close()V
.end method

.method public abstract dispose(Lcom/microsoft/cll/android/IStorage;)V
.end method

.method protected findExistingFiles(Ljava/lang/String;)[Ljava/io/File;
    .locals 4
    .param p1, "fileExtension"    # Ljava/lang/String;

    .prologue
    .line 78
    new-instance v1, Lcom/microsoft/cll/android/AbstractHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/cll/android/AbstractHandler$1;-><init>(Lcom/microsoft/cll/android/AbstractHandler;Ljava/lang/String;)V

    .line 89
    .local v1, "filter":Ljava/io/FilenameFilter;
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/microsoft/cll/android/AbstractHandler;->filePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    .line 90
    .local v0, "files":[Ljava/io/File;
    if-nez v0, :cond_0

    .line 91
    const/4 v2, 0x0

    new-array v0, v2, [Ljava/io/File;

    .line 94
    :cond_0
    return-object v0
.end method

.method protected getFilesByExtensionForDraining(Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1, "fileExtension"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/IStorage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v3, "fullFiles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/IStorage;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/cll/android/AbstractHandler;->findExistingFiles(Ljava/lang/String;)[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 61
    .local v2, "file":Ljava/io/File;
    :try_start_0
    new-instance v6, Lcom/microsoft/cll/android/FileStorage;

    iget-object v7, p0, Lcom/microsoft/cll/android/AbstractHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8, p0}, Lcom/microsoft/cll/android/FileStorage;-><init>(Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/AbstractHandler;)V

    .line 62
    .local v6, "storage":Lcom/microsoft/cll/android/IStorage;
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    invoke-interface {v6}, Lcom/microsoft/cll/android/IStorage;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    .end local v6    # "storage":Lcom/microsoft/cll/android/IStorage;
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 64
    :catch_0
    move-exception v1

    .line 65
    .local v1, "e":Ljava/lang/Exception;
    iget-object v7, p0, Lcom/microsoft/cll/android/AbstractHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v8, "AbstractHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is in use still"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 69
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "file":Ljava/io/File;
    :cond_0
    return-object v3
.end method

.method public abstract getFilesForDraining()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/IStorage;",
            ">;"
        }
    .end annotation
.end method
