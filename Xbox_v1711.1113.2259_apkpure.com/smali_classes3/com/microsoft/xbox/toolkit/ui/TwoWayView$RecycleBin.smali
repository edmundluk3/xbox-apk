.class Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;
.super Ljava/lang/Object;
.source "TwoWayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RecycleBin"
.end annotation


# instance fields
.field private mActiveViews:[Landroid/view/View;

.field private mCurrentScrap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstActivePosition:I

.field private mRecyclerListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;

.field private mScrapViews:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mViewTypeCount:I

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .prologue
    .line 5484
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5487
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mActiveViews:[Landroid/view/View;

    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;)Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;

    .prologue
    .line 5484
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mRecyclerListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;

    return-object p1
.end method

.method private pruneScrapViews()V
    .locals 13

    .prologue
    .line 5725
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mActiveViews:[Landroid/view/View;

    array-length v3, v10

    .line 5726
    .local v3, "maxViews":I
    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    .line 5727
    .local v9, "viewTypeCount":I
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    .line 5729
    .local v5, "scrapViews":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v9, :cond_1

    .line 5730
    aget-object v4, v5, v1

    .line 5731
    .local v4, "scrapPile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 5732
    .local v6, "size":I
    sub-int v0, v6, v3

    .line 5734
    .local v0, "extras":I
    add-int/lit8 v6, v6, -0x1

    .line 5736
    const/4 v2, 0x0

    .local v2, "j":I
    move v7, v6

    .end local v6    # "size":I
    .local v7, "size":I
    :goto_1
    if-ge v2, v0, :cond_0

    .line 5737
    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    add-int/lit8 v6, v7, -0x1

    .end local v7    # "size":I
    .restart local v6    # "size":I
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    const/4 v12, 0x0

    invoke-static {v11, v10, v12}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1600(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;Z)V

    .line 5736
    add-int/lit8 v2, v2, 0x1

    move v7, v6

    .end local v6    # "size":I
    .restart local v7    # "size":I
    goto :goto_1

    .line 5729
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5741
    .end local v0    # "extras":I
    .end local v2    # "j":I
    .end local v4    # "scrapPile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v7    # "size":I
    :cond_1
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v10, :cond_3

    .line 5742
    const/4 v1, 0x0

    :goto_2
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v10

    if-ge v1, v10, :cond_3

    .line 5743
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10, v1}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .line 5744
    .local v8, "v":Landroid/view/View;
    invoke-static {v8}, Landroid/support/v4/view/ViewCompat;->hasTransientState(Landroid/view/View;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 5745
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10, v1}, Landroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    .line 5746
    add-int/lit8 v1, v1, -0x1

    .line 5742
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5750
    .end local v8    # "v":Landroid/view/View;
    :cond_3
    return-void
.end method


# virtual methods
.method addScrapView(Landroid/view/View;I)V
    .locals 5
    .param p1, "scrap"    # Landroid/view/View;
    .param p2, "position"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 5631
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    .line 5632
    .local v0, "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    if-nez v0, :cond_1

    .line 5669
    :cond_0
    :goto_0
    return-void

    .line 5636
    :cond_1
    iput p2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->scrappedFromPosition:I

    .line 5638
    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->viewType:I

    .line 5639
    .local v2, "viewType":I
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->hasTransientState(Landroid/view/View;)Z

    move-result v1

    .line 5642
    .local v1, "scrapHasTransientState":Z
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v1, :cond_4

    .line 5643
    :cond_2
    if-eqz v1, :cond_0

    .line 5644
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-nez v3, :cond_3

    .line 5645
    new-instance v3, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v3}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    .line 5648
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v3, p2, p1}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 5654
    :cond_4
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 5655
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5662
    :goto_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_5

    .line 5663
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 5666
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mRecyclerListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;

    if-eqz v3, :cond_0

    .line 5667
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mRecyclerListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;

    invoke-interface {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    goto :goto_0

    .line 5657
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v3, v3, v2

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method clear()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 5539
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 5540
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5541
    .local v2, "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 5543
    .local v3, "scrapCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 5544
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    add-int/lit8 v5, v3, -0x1

    sub-int/2addr v5, v0

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-static {v6, v5, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1200(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;Z)V

    .line 5543
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5547
    .end local v0    # "i":I
    .end local v2    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v3    # "scrapCount":I
    :cond_0
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    .line 5548
    .local v4, "typeCount":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v4, :cond_2

    .line 5549
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v2, v5, v0

    .line 5550
    .restart local v2    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 5552
    .restart local v3    # "scrapCount":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    if-ge v1, v3, :cond_1

    .line 5553
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    add-int/lit8 v5, v3, -0x1

    sub-int/2addr v5, v1

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-static {v6, v5, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1300(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;Z)V

    .line 5552
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5548
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5558
    .end local v1    # "j":I
    .end local v2    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v3    # "scrapCount":I
    .end local v4    # "typeCount":I
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v5, :cond_3

    .line 5559
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v5}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 5561
    :cond_3
    return-void
.end method

.method clearTransientStateViews()V
    .locals 1

    .prologue
    .line 5611
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v0, :cond_0

    .line 5612
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 5614
    :cond_0
    return-void
.end method

.method fillActiveViews(II)V
    .locals 4
    .param p1, "childCount"    # I
    .param p2, "firstActivePosition"    # I

    .prologue
    .line 5564
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mActiveViews:[Landroid/view/View;

    array-length v3, v3

    if-ge v3, p1, :cond_0

    .line 5565
    new-array v3, p1, [Landroid/view/View;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5568
    :cond_0
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mFirstActivePosition:I

    .line 5570
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5571
    .local v0, "activeViews":[Landroid/view/View;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_1

    .line 5572
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5576
    .local v1, "child":Landroid/view/View;
    aput-object v1, v0, v2

    .line 5571
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 5578
    .end local v1    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method getActiveView(I)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 5581
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mFirstActivePosition:I

    sub-int v1, p1, v4

    .line 5582
    .local v1, "index":I
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5584
    .local v0, "activeViews":[Landroid/view/View;
    if-ltz v1, :cond_0

    array-length v4, v0

    if-ge v1, v4, :cond_0

    .line 5585
    aget-object v2, v0, v1

    .line 5586
    .local v2, "match":Landroid/view/View;
    aput-object v3, v0, v1

    .line 5591
    .end local v2    # "match":Landroid/view/View;
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v3

    goto :goto_0
.end method

.method getScrapView(I)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 5617
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 5618
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    .line 5626
    :goto_0
    return-object v1

    .line 5620
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 5621
    .local v0, "whichScrap":I
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 5622
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 5626
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getTransientStateView(I)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 5595
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-nez v2, :cond_1

    .line 5607
    :cond_0
    :goto_0
    return-object v1

    .line 5599
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, p1}, Landroid/support/v4/util/SparseArrayCompat;->indexOfKey(I)I

    move-result v0

    .line 5600
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 5604
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, v0}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 5605
    .local v1, "result":Landroid/view/View;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, v0}, Landroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    goto :goto_0
.end method

.method public markChildrenDirty()V
    .locals 8

    .prologue
    .line 5510
    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 5511
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5512
    .local v3, "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 5514
    .local v4, "scrapCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 5515
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    .line 5514
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5518
    .end local v1    # "i":I
    .end local v3    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v4    # "scrapCount":I
    :cond_0
    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    .line 5519
    .local v5, "typeCount":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, v5, :cond_2

    .line 5520
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 5521
    .local v2, "scrap":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->forceLayout()V

    goto :goto_2

    .line 5519
    .end local v2    # "scrap":Landroid/view/View;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5526
    .end local v5    # "typeCount":I
    :cond_2
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v6, :cond_3

    .line 5527
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v0

    .line 5528
    .local v0, "count":I
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v0, :cond_3

    .line 5529
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6, v1}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    .line 5528
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 5532
    .end local v0    # "count":I
    :cond_3
    return-void
.end method

.method reclaimScrapViews(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5753
    .local p1, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 5754
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    invoke-interface {p1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 5764
    :cond_0
    return-void

    .line 5756
    :cond_1
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    .line 5757
    .local v3, "viewTypeCount":I
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    .line 5759
    .local v2, "scrapViews":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 5760
    aget-object v1, v2, v0

    .line 5761
    .local v1, "scrapPile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-interface {p1, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 5759
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;
    .locals 5
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 5767
    .local p1, "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 5768
    .local v3, "size":I
    if-gtz v3, :cond_0

    .line 5769
    const/4 v2, 0x0

    .line 5782
    :goto_0
    return-object v2

    .line 5772
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_2

    .line 5773
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 5774
    .local v2, "scrapView":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    .line 5776
    .local v1, "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    iget v4, v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->scrappedFromPosition:I

    if-ne v4, p2, :cond_1

    .line 5777
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 5772
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5782
    .end local v1    # "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    .end local v2    # "scrapView":Landroid/view/View;
    :cond_2
    add-int/lit8 v4, v3, -0x1

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    move-object v2, v4

    goto :goto_0
.end method

.method scrapActiveViews()V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 5673
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5674
    .local v0, "activeViews":[Landroid/view/View;
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    if-le v10, v4, :cond_3

    .line 5676
    .local v4, "multipleScraps":Z
    :goto_0
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5677
    .local v6, "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    array-length v1, v0

    .line 5679
    .local v1, "count":I
    add-int/lit8 v2, v1, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_7

    .line 5680
    aget-object v7, v0, v2

    .line 5681
    .local v7, "victim":Landroid/view/View;
    if-eqz v7, :cond_2

    .line 5682
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;

    .line 5683
    .local v3, "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    iget v8, v3, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->viewType:I

    .line 5685
    .local v8, "whichScrap":I
    aput-object v12, v0, v2

    .line 5687
    invoke-static {v7}, Landroid/support/v4/view/ViewCompat;->hasTransientState(Landroid/view/View;)Z

    move-result v5

    .line 5688
    .local v5, "scrapHasTransientState":Z
    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v10

    if-eqz v10, :cond_0

    if-eqz v5, :cond_4

    .line 5689
    :cond_0
    if-eqz v5, :cond_2

    .line 5690
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v10, v7, v9}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1500(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;Z)V

    .line 5692
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-nez v10, :cond_1

    .line 5693
    new-instance v10, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v10}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    .line 5696
    :cond_1
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mFirstActivePosition:I

    add-int/2addr v11, v2

    invoke-virtual {v10, v11, v7}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 5679
    .end local v3    # "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    .end local v5    # "scrapHasTransientState":Z
    .end local v8    # "whichScrap":I
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v4    # "multipleScraps":Z
    .end local v6    # "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v7    # "victim":Landroid/view/View;
    :cond_3
    move v4, v9

    .line 5674
    goto :goto_0

    .line 5702
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    .restart local v3    # "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    .restart local v4    # "multipleScraps":Z
    .restart local v5    # "scrapHasTransientState":Z
    .restart local v6    # "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .restart local v7    # "victim":Landroid/view/View;
    .restart local v8    # "whichScrap":I
    :cond_4
    if-eqz v4, :cond_5

    .line 5703
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v6, v10, v8

    .line 5706
    :cond_5
    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mFirstActivePosition:I

    add-int/2addr v10, v2

    iput v10, v3, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;->scrappedFromPosition:I

    .line 5707
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5711
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0xe

    if-lt v10, v11, :cond_6

    .line 5712
    invoke-virtual {v7, v12}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 5715
    :cond_6
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mRecyclerListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;

    if-eqz v10, :cond_2

    .line 5716
    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mRecyclerListener:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;

    invoke-interface {v10, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    goto :goto_2

    .line 5721
    .end local v3    # "lp":Lcom/microsoft/xbox/toolkit/ui/TwoWayView$LayoutParams;
    .end local v5    # "scrapHasTransientState":Z
    .end local v7    # "victim":Landroid/view/View;
    .end local v8    # "whichScrap":I
    :cond_7
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->pruneScrapViews()V

    .line 5722
    return-void
.end method

.method public setViewTypeCount(I)V
    .locals 4
    .param p1, "viewTypeCount"    # I

    .prologue
    .line 5494
    const/4 v2, 0x1

    if-ge p1, v2, :cond_0

    .line 5495
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Can\'t have a viewTypeCount < 1"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 5499
    :cond_0
    new-array v1, p1, [Ljava/util/ArrayList;

    .line 5500
    .local v1, "scrapViews":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 5501
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v1, v0

    .line 5500
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5504
    :cond_1
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mViewTypeCount:I

    .line 5505
    const/4 v2, 0x0

    aget-object v2, v1, v2

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5506
    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    .line 5507
    return-void
.end method

.method public shouldRecycleViewType(I)Z
    .locals 1
    .param p1, "viewType"    # I

    .prologue
    .line 5535
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
