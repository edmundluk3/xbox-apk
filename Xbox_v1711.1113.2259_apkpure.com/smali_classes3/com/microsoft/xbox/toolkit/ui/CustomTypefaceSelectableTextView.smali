.class public Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceSelectableTextView;
.super Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
.source "CustomTypefaceSelectableTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceSelectableTextView;->setTextIsSelectable(Z)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 14
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceSelectableTextView;->setTextIsSelectable(Z)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "typeface"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 10
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceSelectableTextView;->setTextIsSelectable(Z)V

    .line 11
    return-void
.end method


# virtual methods
.method public setClickable(Z)V
    .locals 0
    .param p1, "clickable"    # Z

    .prologue
    .line 25
    return-void
.end method
