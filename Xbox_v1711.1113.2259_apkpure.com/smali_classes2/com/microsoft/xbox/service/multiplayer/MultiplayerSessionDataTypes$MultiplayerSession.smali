.class public abstract Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
.super Ljava/lang/Object;
.source "MultiplayerSessionDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiplayerSession"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
    }
.end annotation


# static fields
.field public static final transient LFG_SESSION_TEMPLATE:Ljava/lang/String; = "global(lfg)"

.field private static final transient MAX_TARGET_COUNT:I = 0xf

.field public static final transient SELF_MEMBERSHIP_INDEX:Ljava/lang/String; = "me"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;
    .locals 1

    .prologue
    .line 919
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerSession$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerSession$Builder;-><init>()V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 915
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSession$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerSession$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract branch()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract changeNumber()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract constants()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionConstants;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract contractVersion()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract correlationId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public getNeedCount()I
    .locals 1

    .prologue
    .line 903
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->roleTypes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 904
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->roleTypes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 905
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->roleTypes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 906
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->roleTypes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 907
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->roleTypes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;->target()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 908
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->roleTypes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;->lfg()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$LfgRoleInfo;->roles()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerRoles;->confirmed()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ConfirmedRole;->target()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 911
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract members()Ljava/util/Map;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;",
            ">;"
        }
    .end annotation
.end method

.method public abstract properties()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract roleTypes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract searchHandle()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract servers()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerServers;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract startTime()Ljava/util/Date;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
