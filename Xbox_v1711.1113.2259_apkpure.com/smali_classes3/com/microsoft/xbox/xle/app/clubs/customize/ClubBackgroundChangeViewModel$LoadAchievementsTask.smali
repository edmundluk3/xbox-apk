.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubBackgroundChangeViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadAchievementsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final NUM_ACHIEVEMENTS:I = 0x1e


# instance fields
.field private final serviceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)V
    .locals 1

    .prologue
    .line 269
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 271
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;->serviceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$1;

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;->loadDataInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected loadDataInBackground()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 290
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 291
    .local v1, "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_0

    .line 292
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;->serviceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v6

    const/16 v5, 0x1e

    invoke-interface {v4, v6, v7, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getAchievements(JI)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v2

    .line 293
    .local v2, "screenshotResponse":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    if-eqz v2, :cond_0

    .line 294
    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    .end local v1    # "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v2    # "screenshotResponse":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :cond_0
    :goto_0
    return-object v3

    .line 298
    :catch_0
    move-exception v0

    .line 299
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->access$500()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Error getting user achievements"

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;->onError()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 284
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 280
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 269
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 310
    .local p1, "screenshots":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$LoadAchievementsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->access$600(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;Ljava/util/List;)V

    .line 311
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 306
    return-void
.end method
