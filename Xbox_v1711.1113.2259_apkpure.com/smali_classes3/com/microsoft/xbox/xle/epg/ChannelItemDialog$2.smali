.class Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;
.super Ljava/lang/Object;
.source "ChannelItemDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

.field final synthetic val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;->this$0:Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->isFavorite()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "ChannelUnfavorited"

    .line 79
    .local v0, "actionType":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->toggleFavorite()V

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$2;->this$0:Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->updateFavoriteStatus()V

    .line 83
    return-void

    .line 78
    .end local v0    # "actionType":Ljava/lang/String;
    :cond_0
    const-string v0, "ChannelFavorited"

    goto :goto_0
.end method
