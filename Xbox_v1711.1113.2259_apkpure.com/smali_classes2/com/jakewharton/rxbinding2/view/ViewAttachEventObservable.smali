.class final Lcom/jakewharton/rxbinding2/view/ViewAttachEventObservable;
.super Lio/reactivex/Observable;
.source "ViewAttachEventObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jakewharton/rxbinding2/view/ViewAttachEventObservable$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/Observable",
        "<",
        "Lcom/jakewharton/rxbinding2/view/ViewAttachEvent;",
        ">;"
    }
.end annotation


# instance fields
.field private final view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 14
    invoke-direct {p0}, Lio/reactivex/Observable;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/view/ViewAttachEventObservable;->view:Landroid/view/View;

    .line 16
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/view/ViewAttachEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/jakewharton/rxbinding2/view/ViewAttachEvent;>;"
    invoke-static {p1}, Lcom/jakewharton/rxbinding2/internal/Preconditions;->checkMainThread(Lio/reactivex/Observer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 25
    :goto_0
    return-void

    .line 22
    :cond_0
    new-instance v0, Lcom/jakewharton/rxbinding2/view/ViewAttachEventObservable$Listener;

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewAttachEventObservable;->view:Landroid/view/View;

    invoke-direct {v0, v1, p1}, Lcom/jakewharton/rxbinding2/view/ViewAttachEventObservable$Listener;-><init>(Landroid/view/View;Lio/reactivex/Observer;)V

    .line 23
    .local v0, "listener":Lcom/jakewharton/rxbinding2/view/ViewAttachEventObservable$Listener;
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 24
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewAttachEventObservable;->view:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto :goto_0
.end method
