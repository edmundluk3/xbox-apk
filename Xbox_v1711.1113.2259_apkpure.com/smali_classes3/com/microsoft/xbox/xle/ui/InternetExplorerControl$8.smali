.class Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$8;
.super Ljava/lang/Object;
.source "InternetExplorerControl.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$8;->this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$8;->this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-static {v0, p2}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->access$702(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;Z)Z

    .line 168
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz p2, :cond_0

    const/16 v0, 0x20

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$8;->this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->update()V

    .line 170
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$8;->this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->access$800(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;)I

    move-result v0

    goto :goto_0
.end method
