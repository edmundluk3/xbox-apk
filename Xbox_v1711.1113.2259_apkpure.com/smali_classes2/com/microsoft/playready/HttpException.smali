.class public Lcom/microsoft/playready/HttpException;
.super Ljava/io/IOException;
.source "HttpException.java"


# instance fields
.field mHttpStatusCode:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "httpStatusCode"    # I
    .param p2, "exceptionMessage"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 31
    iput p1, p0, Lcom/microsoft/playready/HttpException;->mHttpStatusCode:I

    .line 32
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "httpStatusCode"    # I
    .param p2, "httpStatusMessage"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    .line 19
    const-string v0, "Http request to url: %s returned HTTPStatus code %d: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 20
    aput-object p3, v1, v2

    const/4 v2, 0x1

    .line 21
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 22
    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 24
    iput p1, p0, Lcom/microsoft/playready/HttpException;->mHttpStatusCode:I

    .line 25
    return-void
.end method


# virtual methods
.method public getStatusCode()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/microsoft/playready/HttpException;->mHttpStatusCode:I

    return v0
.end method
