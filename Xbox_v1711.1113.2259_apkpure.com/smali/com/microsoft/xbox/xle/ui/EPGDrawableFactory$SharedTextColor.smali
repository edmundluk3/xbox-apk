.class public Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;
.super Ljava/lang/Object;
.source "EPGDrawableFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SharedTextColor"
.end annotation


# instance fields
.field private mColor:I

.field private mColorStateList:Landroid/content/res/ColorStateList;

.field private mTextViews:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/widget/TextView;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mUseColorStateList:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mTextViews:Ljava/util/WeakHashMap;

    .line 82
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColor:I

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColorStateList:Landroid/content/res/ColorStateList;

    .line 84
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mUseColorStateList:Z

    .line 85
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mTextViews:Ljava/util/WeakHashMap;

    .line 88
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColor:I

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColorStateList:Landroid/content/res/ColorStateList;

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mUseColorStateList:Z

    .line 91
    return-void
.end method

.method private updateTextViews()V
    .locals 4

    .prologue
    .line 138
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mTextViews:Ljava/util/WeakHashMap;

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 139
    .local v1, "textViews":Ljava/util/Set;, "Ljava/util/Set<Landroid/widget/TextView;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 140
    .local v0, "textView":Landroid/widget/TextView;
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mUseColorStateList:Z

    if-eqz v3, :cond_0

    .line 141
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 145
    :goto_1
    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    goto :goto_0

    .line 143
    :cond_0
    iget v3, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColor:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 147
    .end local v0    # "textView":Landroid/widget/TextView;
    :cond_1
    return-void
.end method


# virtual methods
.method public addTextView(Landroid/widget/TextView;)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mTextViews:Ljava/util/WeakHashMap;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mUseColorStateList:Z

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColor:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColor:I

    return v0
.end method

.method public getColorStateList()Landroid/content/res/ColorStateList;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColorStateList:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public isUsingColorStateList()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mUseColorStateList:Z

    return v0
.end method

.method public removeTextView(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mTextViews:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    return-void
.end method

.method public setColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColorStateList:Landroid/content/res/ColorStateList;

    .line 95
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColor:I

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mUseColorStateList:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 96
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mUseColorStateList:Z

    .line 97
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColor:I

    .line 98
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->updateTextViews()V

    .line 100
    :cond_1
    return-void
.end method

.method public setColor(Landroid/content/res/ColorStateList;)V
    .locals 1
    .param p1, "colorStateList"    # Landroid/content/res/ColorStateList;

    .prologue
    .line 103
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColor:I

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColorStateList:Landroid/content/res/ColorStateList;

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mUseColorStateList:Z

    if-nez v0, :cond_1

    .line 106
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mUseColorStateList:Z

    .line 107
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->mColorStateList:Landroid/content/res/ColorStateList;

    .line 108
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->updateTextViews()V

    .line 110
    :cond_1
    return-void
.end method
