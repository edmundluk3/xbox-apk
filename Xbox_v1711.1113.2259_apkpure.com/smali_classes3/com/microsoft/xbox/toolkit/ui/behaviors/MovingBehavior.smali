.class public Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
.super Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;
.source "MovingBehavior.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;
    }
.end annotation


# static fields
.field private static final PARAM_OFFSET:Ljava/lang/String; = "com.microsoft.xbox.toolkit.ui.behaviors.MovingBehavior.OFFSET"


# instance fields
.field private offset:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    .line 29
    return-void
.end method

.method private ensureClipBoundsAndVisibility(Landroid/view/View;)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getClipBounds(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 114
    .local v0, "rect":Landroid/graphics/Rect;
    if-nez v0, :cond_0

    .line 118
    new-instance v0, Landroid/graphics/Rect;

    .end local v0    # "rect":Landroid/graphics/Rect;
    const v2, 0x7fffffff

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-direct {v0, v1, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 120
    .restart local v0    # "rect":Landroid/graphics/Rect;
    :cond_0
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    neg-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 121
    invoke-static {p1, v0}, Landroid/support/v4/view/ViewCompat;->setClipBounds(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 122
    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    if-ne v2, v3, :cond_1

    const/4 v1, 0x4

    :cond_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    return-void
.end method

.method private static findView(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;
    .locals 5
    .param p0, "root"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p1, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 156
    .end local p0    # "root":Landroid/view/View;
    :goto_0
    return-object p0

    .line 146
    .restart local p0    # "root":Landroid/view/View;
    :cond_0
    instance-of v4, p0, Landroid/view/ViewGroup;

    if-eqz v4, :cond_2

    move-object v2, p0

    .line 147
    check-cast v2, Landroid/view/ViewGroup;

    .line 148
    .local v2, "vg":Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 149
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 150
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->findView(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v3

    .line 151
    .local v3, "view":Landroid/view/View;, "TT;"
    if-eqz v3, :cond_1

    move-object p0, v3

    .line 152
    goto :goto_0

    .line 149
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 156
    .end local v0    # "childCount":I
    .end local v1    # "i":I
    .end local v2    # "vg":Landroid/view/ViewGroup;
    .end local v3    # "view":Landroid/view/View;, "TT;"
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method private static getLayoutManager(Landroid/support/v7/widget/RecyclerView;Ljava/lang/Class;)Landroid/support/v7/widget/RecyclerView$LayoutManager;
    .locals 1
    .param p0, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "LM:Landroid/support/v7/widget/RecyclerView$LayoutManager;",
            ">(",
            "Landroid/support/v7/widget/RecyclerView;",
            "Ljava/lang/Class",
            "<T",
            "LM;",
            ">;)T",
            "LM;"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TLM;>;"
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    .line 139
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getViewHeight(Landroid/view/View;)I
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 37
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 38
    .local v0, "lp":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    iget v1, v0, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->topMargin:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, v0, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v1, v2

    return v1
.end method

.method private shouldMoveDown(Landroid/view/View;)Z
    .locals 4
    .param p1, "target"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 88
    instance-of v3, p1, Landroid/support/v4/widget/NestedScrollView;

    if-eqz v3, :cond_2

    .line 89
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v3

    if-nez v3, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 89
    goto :goto_0

    .line 92
    :cond_2
    const-class v3, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p1, v3}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->findView(Landroid/view/View;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 93
    .local v0, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollOffset()I

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public getOffset()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    return v0
.end method

.method protected notifyOnMoved(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)V
    .locals 5
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;

    .prologue
    .line 126
    invoke-static {p1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->computeSortedDependencies(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 127
    .local v1, "dep":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 128
    .local v2, "lp":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    invoke-virtual {v2}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->getBehavior()Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v0

    .line 129
    .local v0, "b":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;

    if-eqz v4, :cond_0

    .line 130
    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;

    .end local v0    # "b":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    invoke-virtual {v0, p1, v1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->onDependencyMoved(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V

    goto :goto_0

    .line 133
    .end local v1    # "dep":Landroid/view/View;
    .end local v2    # "lp":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    :cond_1
    return-void
.end method

.method public onNestedPreScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;II[I)V
    .locals 5
    .param p1, "coordinatorLayout"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "target"    # Landroid/view/View;
    .param p4, "dx"    # I
    .param p5, "dy"    # I
    .param p6, "consumed"    # [I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 49
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->getViewHeight(Landroid/view/View;)I

    move-result v0

    .line 51
    .local v0, "initialHeight":I
    if-ltz p5, :cond_1

    move v1, v2

    .line 52
    .local v1, "scrollingUp":Z
    :goto_0
    if-eqz v1, :cond_3

    .line 54
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    sub-int/2addr v3, p5

    neg-int v4, v0

    if-le v3, v4, :cond_2

    .line 55
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    sub-int/2addr v3, p5

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    .line 56
    aput p5, p6, v2

    .line 62
    :goto_1
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->ensureClipBoundsAndVisibility(Landroid/view/View;)V

    .line 63
    if-eqz p5, :cond_0

    .line 64
    neg-int v2, p5

    invoke-static {p2, v2}, Landroid/support/v4/view/ViewCompat;->offsetTopAndBottom(Landroid/view/View;I)V

    .line 65
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->notifyOnMoved(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)V

    .line 85
    :cond_0
    :goto_2
    return-void

    .end local v1    # "scrollingUp":Z
    :cond_1
    move v1, v3

    .line 51
    goto :goto_0

    .line 58
    .restart local v1    # "scrollingUp":Z
    :cond_2
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    add-int p5, v3, v0

    .line 59
    neg-int v3, v0

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    .line 60
    aput p5, p6, v2

    goto :goto_1

    .line 68
    :cond_3
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->shouldMoveDown(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 70
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    sub-int/2addr v4, p5

    if-gez v4, :cond_4

    .line 71
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    sub-int/2addr v3, p5

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    .line 72
    aput p5, p6, v2

    .line 78
    :goto_3
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->ensureClipBoundsAndVisibility(Landroid/view/View;)V

    .line 79
    if-eqz p5, :cond_0

    .line 80
    neg-int v2, p5

    invoke-static {p2, v2}, Landroid/support/v4/view/ViewCompat;->offsetTopAndBottom(Landroid/view/View;I)V

    .line 81
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->notifyOnMoved(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)V

    goto :goto_2

    .line 74
    :cond_4
    iget p5, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    .line 75
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    .line 76
    aput v3, p6, v2

    goto :goto_3
.end method

.method public onRestoreInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 107
    instance-of v0, p3, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;

    if-eqz v0, :cond_0

    .line 108
    check-cast p3, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;

    .end local p3    # "state":Landroid/os/Parcelable;
    invoke-virtual {p3}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;->getOffset()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    .line 110
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Landroid/os/Parcelable;
    .locals 2
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;

    .prologue
    .line 102
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;->offset:I

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;-><init>(I)V

    return-object v0
.end method

.method public onStartNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;I)Z
    .locals 1
    .param p1, "coordinatorLayout"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "directTargetChild"    # Landroid/view/View;
    .param p4, "target"    # Landroid/view/View;
    .param p5, "nestedScrollAxes"    # I

    .prologue
    .line 43
    invoke-super/range {p0 .. p5}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->onStartNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    and-int/lit8 v0, p5, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
