.class public Lcom/microsoft/xbox/xle/ui/IconFontRingButton;
.super Landroid/widget/LinearLayout;
.source "IconFontRingButton.java"


# instance fields
.field private countText:Ljava/lang/String;

.field countTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e070b
    .end annotation
.end field

.field private iconText:Ljava/lang/String;

.field iconTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e070a
    .end annotation
.end field

.field private subText:Ljava/lang/String;

.field subTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e070d
    .end annotation
.end field

.field private text:Ljava/lang/String;

.field textView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e070c
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method private applyCustomTypeface(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 71
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->textView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 73
    .local v0, "tf":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->textView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 77
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->textView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->countTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->countText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->subTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->subText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->iconTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->iconText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void
.end method

.method private initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 51
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 52
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f03014d

    invoke-virtual {v2, v3, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 54
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/view/View;)Lbutterknife/Unbinder;

    .line 56
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 57
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "typeface":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 60
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->IconFontRingButton:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 61
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->iconText:Ljava/lang/String;

    .line 62
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->countText:Ljava/lang/String;

    .line 63
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->text:Ljava/lang/String;

    .line 64
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->subText:Ljava/lang/String;

    .line 65
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 67
    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->applyCustomTypeface(Landroid/content/Context;Ljava/lang/String;)V

    .line 68
    return-void
.end method


# virtual methods
.method public setCount(Ljava/lang/String;)V
    .locals 2
    .param p1, "countText"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->countText:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->countTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->countText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->invalidate()V

    .line 87
    return-void
.end method

.method public setLabelText(Ljava/lang/String;)V
    .locals 2
    .param p1, "labelText"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->text:Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->invalidate()V

    .line 93
    return-void
.end method

.method public setSubText(Ljava/lang/String;)V
    .locals 2
    .param p1, "subText"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->subText:Ljava/lang/String;

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->subTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->subText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/IconFontRingButton;->invalidate()V

    .line 99
    return-void
.end method
