.class public Lcom/microsoft/xbox/toolkit/XLEMath;
.super Ljava/lang/Object;
.source "XLEMath.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static int32LowBit(I)I
    .locals 2
    .param p0, "x"    # I

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v1, 0x0

    .line 38
    :goto_0
    return v1

    .line 32
    :cond_0
    const/4 v0, 0x0

    .line 33
    .local v0, "zeros":I
    :goto_1
    and-int/lit8 v1, p0, 0x1

    if-nez v1, :cond_1

    .line 34
    add-int/lit8 v0, v0, 0x1

    .line 35
    shr-int/lit8 p0, p0, 0x1

    goto :goto_1

    .line 38
    :cond_1
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    goto :goto_0
.end method

.method public static isPowerOf2(I)Z
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 15
    if-lez p0, :cond_0

    add-int/lit8 v0, p0, -0x1

    and-int/2addr v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static signum(J)I
    .locals 4
    .param p0, "v"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 19
    cmp-long v0, p0, v2

    if-lez v0, :cond_0

    .line 20
    const/4 v0, 0x1

    .line 23
    :goto_0
    return v0

    .line 21
    :cond_0
    cmp-long v0, p0, v2

    if-gez v0, :cond_1

    .line 22
    const/4 v0, -0x1

    goto :goto_0

    .line 23
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
