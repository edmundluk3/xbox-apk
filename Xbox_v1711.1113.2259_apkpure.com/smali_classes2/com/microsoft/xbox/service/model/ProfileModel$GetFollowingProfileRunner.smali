.class Lcom/microsoft/xbox/service/model/ProfileModel$GetFollowingProfileRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetFollowingProfileRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/FollowingPeople;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "xuid"    # Ljava/lang/String;

    .prologue
    .line 2890
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFollowingProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2891
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFollowingProfileRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 2892
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFollowingProfileRunner;->xuid:Ljava/lang/String;

    .line 2893
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/FollowingPeople;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2897
    new-instance v0, Lcom/microsoft/xbox/service/model/FollowingPeople;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/FollowingPeople;-><init>()V

    .line 2899
    .local v0, "followers":Lcom/microsoft/xbox/service/model/FollowingPeople;
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFollowingProfileRunner;->xuid:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2900
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFollowingProfileRunner;->xuid:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getFollowingFromPeopleHub(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    move-result-object v2

    .line 2902
    .local v2, "peopleHubPeopleSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    if-eqz v2, :cond_1

    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    .line 2903
    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2905
    .local v1, "followersData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 2906
    .local v3, "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    new-instance v5, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-direct {v5, v3}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2909
    .end local v3    # "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_0
    new-instance v4, Lcom/microsoft/xbox/service/model/ProfileModel$FollowingAndFavoritesComparator;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFollowingProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/microsoft/xbox/service/model/ProfileModel$FollowingAndFavoritesComparator;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V

    invoke-static {v1, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2911
    iput-object v1, v0, Lcom/microsoft/xbox/service/model/FollowingPeople;->following:Ljava/util/ArrayList;

    .line 2915
    .end local v1    # "followersData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v2    # "peopleHubPeopleSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_1
    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2885
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetFollowingProfileRunner;->buildData()Lcom/microsoft/xbox/service/model/FollowingPeople;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2929
    const-wide/16 v0, 0xc84

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowingPeople;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2924
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/FollowingPeople;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetFollowingProfileRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$2500(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 2925
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2920
    return-void
.end method
