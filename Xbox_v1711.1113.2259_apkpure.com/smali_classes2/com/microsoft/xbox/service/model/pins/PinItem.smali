.class public Lcom/microsoft/xbox/service/model/pins/PinItem;
.super Ljava/lang/Object;
.source "PinItem.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/PinItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final CANONICAL:Ljava/lang/String; = "Canonical"

.field public static final PACKAGE_FAMILY_NAME:Ljava/lang/String; = "PackageFamilyName"

.field public static final SCOPED:Ljava/lang/String; = "ScopedMediaId"

.field private static final TVSEASON:Ljava/lang/String; = "TVSeason"


# instance fields
.field public AltImageUrl:Ljava/lang/String;

.field public ContentType:Ljava/lang/String;

.field public DeviceType:Ljava/lang/String;

.field public ImageUrl:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public ItemId:Ljava/lang/String;

.field public Locale:Ljava/lang/String;

.field public Provider:Ljava/lang/String;

.field public ProviderId:Ljava/lang/String;

.field private transient ProviderName:Ljava/lang/String;

.field private transient ProviderTitleId:J

.field public SubTitle:Ljava/lang/String;

.field public Title:Ljava/lang/String;

.field private transient bingId:Ljava/lang/String;

.field private transient boxArtBackgroundColor:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private transient edsProvider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

.field private transient generatedProvider:Ljava/lang/String;

.field private transient idType:Ljava/lang/String;

.field public transient index:I

.field private transient launchUri:Ljava/lang/String;

.field private transient locallyAdded:Z

.field private transient seriesItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->boxArtBackgroundColor:I

    return-void
.end method

.method private addHashCode(ILjava/lang/Object;)I
    .locals 2
    .param p1, "hashCode"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 344
    if-nez p2, :cond_0

    .end local p1    # "hashCode":I
    :goto_0
    return p1

    .restart local p1    # "hashCode":I
    :cond_0
    mul-int/lit8 v0, p1, 0x1f

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int p1, v0, v1

    goto :goto_0
.end method

.method public static getEPListPinItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 10
    .param p0, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 68
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 70
    new-instance v1, Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/pins/PinItem;-><init>()V

    .line 71
    .local v1, "item":Lcom/microsoft/xbox/service/model/pins/PinItem;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->Locale:Ljava/lang/String;

    .line 72
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    .line 73
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/pins/PinItem;->setBingId(Ljava/lang/String;)V

    .line 75
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "0x%x"

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getNowPlayingTitleId()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderTitleId:J

    .line 79
    const-string v2, "PinItem"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ContentType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - ItemId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Provider: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ProviderId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    .line 83
    const-string v2, "Canonical"

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->idType:Ljava/lang/String;

    .line 95
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSquareIconUrl()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ImageUrl:Ljava/lang/String;

    .line 96
    const-string v2, "XboxOne"

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->DeviceType:Ljava/lang/String;

    .line 97
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->Title:Ljava/lang/String;

    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBoxArtBackgroundColor()I

    move-result v2

    iput v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->boxArtBackgroundColor:I

    .line 101
    return-object v1

    .line 84
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    .line 86
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getApplicationId()Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "appId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 88
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "appx:%1$s!%2$s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    aput-object v0, v4, v9

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->launchUri:Ljava/lang/String;

    .line 90
    :cond_1
    const-string v2, "PackageFamilyName"

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->idType:Ljava/lang/String;

    goto :goto_0

    .line 92
    .end local v0    # "appId":Ljava/lang/String;
    :cond_2
    const-string v2, "ScopedMediaId"

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/pins/PinItem;->idType:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getEPListPinItem(Lcom/microsoft/xbox/service/model/pins/PinItem;Z)Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 4
    .param p0, "org"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p1, "forAdd"    # Z

    .prologue
    const/4 v1, 0x0

    .line 105
    if-nez p0, :cond_0

    move-object v0, v1

    .line 152
    :goto_0
    return-object v0

    .line 109
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;-><init>()V

    .line 110
    .local v0, "item":Lcom/microsoft/xbox/service/model/pins/PinItem;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    .line 111
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    .line 112
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    .line 113
    const-string v2, "XboxOne"

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->DeviceType:Ljava/lang/String;

    .line 116
    const-string v2, "PackageFamilyName"

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->idType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "Canonical"

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->idType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 117
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    .line 122
    :goto_1
    if-eqz p1, :cond_5

    .line 123
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getIsApporGame()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 124
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 125
    iput-object v1, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    .line 126
    iput-object v1, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    .line 145
    :cond_2
    :goto_2
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 147
    iput-object v1, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    .line 150
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Locale:Ljava/lang/String;

    goto :goto_0

    .line 119
    :cond_4
    iput-object v1, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    goto :goto_1

    .line 131
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getLocallyAdded()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 136
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getIsApporGame()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 137
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 138
    iput-object v1, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    .line 139
    iput-object v1, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    goto :goto_2
.end method

.method private getMergedCompanionModel()Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 353
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/pins/PinItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    .line 354
    .local v2, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-nez v2, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-object v1

    .line 358
    :cond_1
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 359
    .local v0, "mediaModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;+Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->hasValidData()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 360
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v3

    .line 361
    .local v3, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    if-nez v3, :cond_2

    const-wide/16 v4, -0x1

    .line 362
    .local v4, "titleId":J
    :goto_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v7

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 363
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v8

    .line 362
    invoke-static {v6, v7, v8, v4, v5}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getModel(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v1

    .line 364
    .local v1, "mergedModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    goto :goto_0

    .line 361
    .end local v1    # "mergedModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    .end local v4    # "titleId":J
    :cond_2
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    goto :goto_1
.end method


# virtual methods
.method public compareTo(Lcom/microsoft/xbox/service/model/pins/PinItem;)I
    .locals 2
    .param p1, "another"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 399
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 401
    iget v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->index:I

    iget v1, p1, Lcom/microsoft/xbox/service/model/pins/PinItem;->index:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 24
    check-cast p1, Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/pins/PinItem;->compareTo(Lcom/microsoft/xbox/service/model/pins/PinItem;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 322
    if-ne p1, p0, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v1

    .line 326
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 327
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 330
    check-cast v0, Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 331
    .local v0, "that":Lcom/microsoft/xbox/service/model/pins/PinItem;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public getBingId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->bingId:Ljava/lang/String;

    return-object v0
.end method

.method public getBoxArtBackgroundColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 317
    iget v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->boxArtBackgroundColor:I

    return v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultCompanion()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 2

    .prologue
    .line 265
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getMergedCompanionModel()Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v0

    .line 266
    .local v0, "mergedModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->hasValidData()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getHasActivities()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getFeaturedActivity()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v1

    .line 269
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIdType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->idType:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getIsApporGame()Z
    .locals 2

    .prologue
    .line 376
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    .line 377
    .local v0, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIsMusicPlayList()Z
    .locals 1

    .prologue
    .line 237
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    return v0
.end method

.method public getIsTVChannel()Z
    .locals 1

    .prologue
    .line 247
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isTVChannel(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    return v0
.end method

.method public getIsTVSeason()Z
    .locals 4

    .prologue
    .line 299
    const-wide/32 v0, 0x3d705025

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderTitleId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-string v0, "TVSeason"

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsWebLink()Z
    .locals 1

    .prologue
    .line 242
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isWebLink(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    return v0
.end method

.method public getItemId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchUri()Ljava/lang/String;
    .locals 5

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->isDLC()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "marketplace://deeplink?destination=details&id=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 307
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->launchUri:Ljava/lang/String;

    goto :goto_0
.end method

.method public getLocallyAdded()Z
    .locals 1

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->locallyAdded:Z

    return v0
.end method

.method public getParentSeriesItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;
    .locals 4

    .prologue
    .line 293
    const-string v0, "TVSeason"

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 294
    const-wide/32 v0, 0x3d705025

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderTitleId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 295
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->seriesItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    return-object v0

    .line 294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .locals 4

    .prologue
    .line 382
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->hasProvider()Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    const/4 v0, 0x0

    .line 393
    :goto_0
    return-object v0

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->edsProvider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    if-nez v0, :cond_1

    .line 387
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->edsProvider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 388
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->edsProvider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderTitleIdString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->parseHexLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setTitleId(J)V

    .line 389
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->edsProvider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProviderMediaId:Ljava/lang/String;

    .line 390
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->edsProvider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    .line 393
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->edsProvider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    goto :goto_0
.end method

.method public getProviderMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderName:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderTitleId()J
    .locals 2

    .prologue
    .line 179
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderTitleId:J

    return-wide v0
.end method

.method public getProviderTitleIdString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    .line 174
    :goto_0
    return-object v0

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->generatedProvider:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->generatedProvider:Ljava/lang/String;

    goto :goto_0

    .line 174
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShouldShowBackgroundColor()Z
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "DApp"

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 158
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    .line 159
    .local v0, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v0, v1, :cond_1

    .line 160
    :cond_0
    const/4 v1, 0x0

    .line 163
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Title:Ljava/lang/String;

    goto :goto_0
.end method

.method public hasCompanion()Z
    .locals 3

    .prologue
    .line 252
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isWebLink(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isTVChannel(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    .line 253
    .local v1, "ret":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 254
    const/4 v1, 0x0

    .line 255
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getMergedCompanionModel()Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v0

    .line 256
    .local v0, "mergedModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->hasValidData()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 257
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getHasFeaturedActivity()Z

    move-result v1

    .line 260
    .end local v0    # "mergedModel":Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
    :cond_0
    return v1

    .line 252
    .end local v1    # "ret":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasDetails()Z
    .locals 1

    .prologue
    .line 227
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->hasDetails(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    return v0
.end method

.method public hasProvider()Z
    .locals 1

    .prologue
    .line 232
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->hasProvider(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 336
    const/4 v0, 0x1

    .line 337
    .local v0, "hashCode":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinItem;->addHashCode(ILjava/lang/Object;)I

    move-result v0

    .line 338
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinItem;->addHashCode(ILjava/lang/Object;)I

    move-result v0

    .line 339
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinItem;->addHashCode(ILjava/lang/Object;)I

    move-result v0

    .line 340
    return v0
.end method

.method public isDLC()Z
    .locals 2

    .prologue
    .line 371
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    .line 372
    .local v0, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DDurable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBingId(Ljava/lang/String;)V
    .locals 0
    .param p1, "bingId"    # Ljava/lang/String;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->bingId:Ljava/lang/String;

    .line 204
    return-void
.end method

.method public setIdType(Ljava/lang/String;)V
    .locals 0
    .param p1, "idType"    # Ljava/lang/String;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->idType:Ljava/lang/String;

    .line 212
    return-void
.end method

.method public setLaunchUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 285
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->launchUri:Ljava/lang/String;

    .line 286
    return-void
.end method

.method public setLocallyAdded(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 223
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->locallyAdded:Z

    .line 224
    return-void
.end method

.method public setParentSeries(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    .prologue
    .line 289
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->seriesItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    .line 290
    return-void
.end method

.method public setProvider(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->generatedProvider:Ljava/lang/String;

    .line 313
    return-void
.end method

.method public setProviderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderName:Ljava/lang/String;

    .line 278
    return-void
.end method

.method public setProviderTitleId(J)V
    .locals 1
    .param p1, "titleId"    # J

    .prologue
    .line 281
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderTitleId:J

    .line 282
    return-void
.end method
