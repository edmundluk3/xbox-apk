.class Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1$1;
.super Ljava/util/TimerTask;
.source "XBoxOneCompanionSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1;

.field final synthetic val$id:Ljava/lang/String;

.field final synthetic val$mediaType:Lcom/microsoft/xbox/smartglass/MediaType;

.field final synthetic val$titleId:I


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1;ILjava/lang/String;Lcom/microsoft/xbox/smartglass/MediaType;)V
    .locals 0
    .param p1, "this$2"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1;

    .prologue
    .line 467
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1$1;->this$2:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1;

    iput p2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1$1;->val$titleId:I

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1$1;->val$id:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1$1;->val$mediaType:Lcom/microsoft/xbox/smartglass/MediaType;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 23

    .prologue
    .line 469
    const-wide v16, 0xdf8475800L

    .line 470
    .local v16, "durationTicks":J
    const-wide v12, 0x12a05f200L

    .line 471
    .local v12, "positionTicks":J
    const/4 v4, 0x1

    new-array v0, v4, [Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    move-object/from16 v22, v0

    .line 472
    .local v22, "metaData":[Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    const-string v4, "hello"

    const-string v5, "world"

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    .local v2, "entry":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    const/4 v4, 0x0

    aput-object v2, v22, v4

    .line 476
    new-instance v3, Lcom/microsoft/xbox/smartglass/SGMediaState;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1$1;->val$titleId:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1$1;->val$id:Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1$1;->val$mediaType:Lcom/microsoft/xbox/smartglass/MediaType;

    .line 480
    invoke-virtual {v7}, Lcom/microsoft/xbox/smartglass/MediaType;->getValue()I

    move-result v7

    sget-object v8, Lcom/microsoft/xbox/smartglass/SoundLevel;->Low:Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 481
    invoke-virtual {v8}, Lcom/microsoft/xbox/smartglass/SoundLevel;->getValue()I

    move-result v8

    sget-object v9, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Play:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 482
    invoke-virtual {v9}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue()I

    move-result v9

    sget-object v10, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->Pause:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v10}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue()I

    move-result v10

    or-int/2addr v9, v10

    sget-object v10, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Playing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 483
    invoke-virtual {v10}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->getValue()I

    move-result v10

    const/high16 v11, 0x3f800000    # 1.0f

    const-wide/16 v14, 0x0

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    invoke-direct/range {v3 .. v22}, Lcom/microsoft/xbox/smartglass/SGMediaState;-><init>(ILjava/lang/String;Ljava/lang/String;IIIIFJJJJJ[Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;)V

    .line 492
    .local v3, "mediaState":Lcom/microsoft/xbox/smartglass/SGMediaState;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1$1;->this$2:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1;->this$1:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->sessionManagerListener:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->onMediaStateChanged(Lcom/microsoft/xbox/smartglass/MediaState;)V

    .line 493
    return-void
.end method
