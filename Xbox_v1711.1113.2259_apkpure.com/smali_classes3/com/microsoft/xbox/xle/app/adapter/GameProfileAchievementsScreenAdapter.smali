.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "GameProfileAchievementsScreenAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private achievementItemsHashcode:I

.field private achievementsReady:Z

.field private compositeAchievementResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
            ">;"
        }
    .end annotation
.end field

.field private compositeLeaderboardResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
            ">;"
        }
    .end annotation
.end field

.field private leaderboardsReady:Z

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

.field private noContentText:Landroid/widget/TextView;

.field private statItemsHashcode:I

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 41
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->achievementsReady:Z

    .line 42
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->leaderboardsReady:Z

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeLeaderboardResults:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeAchievementResults:Ljava/util/List;

    .line 47
    const v0, 0x7f0e0646

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 48
    const v0, 0x7f0e06a2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->noContentText:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f0e0647

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    .line 53
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 55
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 59
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->isBusy()Z

    move-result v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 60
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 62
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->noContentText:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getNoContentText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->hashCode()I

    move-result v4

    iget v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->achievementItemsHashcode:I

    if-eq v4, v5, :cond_1

    .line 65
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->hashCode()I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->achievementItemsHashcode:I

    .line 67
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeAchievementResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 68
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 69
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeAchievementResults:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->achievementItem(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 72
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_0
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->achievementsReady:Z

    .line 75
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getStatistics()Ljava/util/Collection;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getStatistics()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->hashCode()I

    move-result v4

    iget v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->statItemsHashcode:I

    if-eq v4, v5, :cond_3

    .line 76
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getStatistics()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->hashCode()I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->statItemsHashcode:I

    .line 78
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeLeaderboardResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 80
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeLeaderboardResults:Ljava/util/List;

    const/4 v5, 0x0

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->leaderboardItem(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;)Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->getStatistics()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    .line 83
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeLeaderboardResults:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->leaderboardItem(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;)Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 86
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    :cond_2
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->leaderboardsReady:Z

    .line 89
    :cond_3
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->achievementsReady:Z

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->leaderboardsReady:Z

    if-eqz v4, :cond_4

    .line 90
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v2, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->leaderboardHeader()Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeLeaderboardResults:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 94
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->achievementHeader()Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeAchievementResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 96
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->emptyAchievementItems()Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    :goto_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v3

    .line 103
    .local v3, "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->clear()V

    .line 104
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 106
    new-instance v4, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v4, v3, v2}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v4}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v0

    .line 107
    .local v0, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-virtual {v0, v4}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 109
    .end local v0    # "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    .end local v2    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;>;"
    .end local v3    # "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;>;"
    :cond_4
    return-void

    .line 98
    .restart local v2    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;>;"
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;->compositeAchievementResults:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method
