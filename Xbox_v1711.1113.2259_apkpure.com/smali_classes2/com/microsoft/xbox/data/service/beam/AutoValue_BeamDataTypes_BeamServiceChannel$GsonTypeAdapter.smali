.class public final Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_BeamDataTypes_BeamServiceChannel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;",
        ">;"
    }
.end annotation


# instance fields
.field private final audienceAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;",
            ">;"
        }
    .end annotation
.end field

.field private defaultAudience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

.field private defaultId:Ljava/lang/Integer;

.field private defaultName:Ljava/lang/String;

.field private defaultToken:Ljava/lang/String;

.field private defaultViewersCurrent:Ljava/lang/Integer;

.field private final idAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final nameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tokenAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final viewersCurrentAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultId:Ljava/lang/Integer;

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultToken:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultViewersCurrent:Ljava/lang/Integer;

    .line 29
    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultAudience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    .line 31
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    .line 33
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->tokenAdapter:Lcom/google/gson/TypeAdapter;

    .line 34
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->viewersCurrentAdapter:Lcom/google/gson/TypeAdapter;

    .line 35
    const-class v0, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->audienceAdapter:Lcom/google/gson/TypeAdapter;

    .line 36
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;
    .locals 8
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v7, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v7, :cond_0

    .line 80
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 81
    const/4 v0, 0x0

    .line 122
    :goto_0
    return-object v0

    .line 83
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultId:Ljava/lang/Integer;

    .line 85
    .local v1, "id":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 86
    .local v2, "name":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultToken:Ljava/lang/String;

    .line 87
    .local v3, "token":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultViewersCurrent:Ljava/lang/Integer;

    .line 88
    .local v4, "viewersCurrent":Ljava/lang/Integer;
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultAudience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    .line 89
    .local v5, "audience":Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v6

    .line 91
    .local v6, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v7, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v7, :cond_1

    .line 92
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 95
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 117
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 95
    :sswitch_0
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v7, "token"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v7, "viewersCurrent"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v7, "audience"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    .line 97
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "id":Ljava/lang/Integer;
    check-cast v1, Ljava/lang/Integer;

    .line 98
    .restart local v1    # "id":Ljava/lang/Integer;
    goto :goto_1

    .line 101
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "name":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 102
    .restart local v2    # "name":Ljava/lang/String;
    goto :goto_1

    .line 105
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->tokenAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "token":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 106
    .restart local v3    # "token":Ljava/lang/String;
    goto :goto_1

    .line 109
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->viewersCurrentAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "viewersCurrent":Ljava/lang/Integer;
    check-cast v4, Ljava/lang/Integer;

    .line 110
    .restart local v4    # "viewersCurrent":Ljava/lang/Integer;
    goto :goto_1

    .line 113
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->audienceAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "audience":Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;
    check-cast v5, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    .line 114
    .restart local v5    # "audience":Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;
    goto/16 :goto_1

    .line 121
    .end local v6    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 122
    new-instance v0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;)V

    goto/16 :goto_0

    .line 95
    nop

    :sswitch_data_0
    .sparse-switch
        -0x18a38268 -> :sswitch_3
        0xd1b -> :sswitch_0
        0x337a8b -> :sswitch_1
        0x696b9f9 -> :sswitch_2
        0x3a26ea04 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAudience(Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;)Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAudience"    # Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultAudience:Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    .line 55
    return-object p0
.end method

.method public setDefaultId(Ljava/lang/Integer;)Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultId"    # Ljava/lang/Integer;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultId:Ljava/lang/Integer;

    .line 39
    return-object p0
.end method

.method public setDefaultName(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultName"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 43
    return-object p0
.end method

.method public setDefaultToken(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultToken"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultToken:Ljava/lang/String;

    .line 47
    return-object p0
.end method

.method public setDefaultViewersCurrent(Ljava/lang/Integer;)Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultViewersCurrent"    # Ljava/lang/Integer;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->defaultViewersCurrent:Ljava/lang/Integer;

    .line 51
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    if-nez p2, :cond_0

    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 76
    :goto_0
    return-void

    .line 64
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 65
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->id()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 67
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 69
    const-string v0, "token"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->tokenAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->token()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 71
    const-string v0, "viewersCurrent"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->viewersCurrentAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->viewersCurrent()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 73
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->audienceAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;->audience()Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamBroadcastAudience;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 75
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/data/service/beam/AutoValue_BeamDataTypes_BeamServiceChannel$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;)V

    return-void
.end method
