.class public Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;
.super Ljava/lang/Object;
.source "OnRecyclerViewClickListener.java"

# interfaces
.implements Landroid/support/v7/widget/RecyclerView$OnItemTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;
    }
.end annotation


# instance fields
.field private final clickDetector:Landroid/view/GestureDetector;

.field private final listener:Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;->listener:Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;

    .line 19
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$1;-><init>(Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;->clickDetector:Landroid/view/GestureDetector;

    .line 26
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "rv"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 30
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;->listener:Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;

    if-eqz v1, :cond_0

    .line 31
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;->clickDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/support/v7/widget/RecyclerView;->findChildViewUnder(FF)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 34
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;->listener:Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v2

    invoke-interface {v1, p1, v0, v2}, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;->onItemClick(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;I)V

    .line 35
    const/4 v1, 0x1

    .line 39
    .end local v0    # "child":Landroid/view/View;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onRequestDisallowInterceptTouchEvent(Z)V
    .locals 0
    .param p1, "disallowIntercept"    # Z

    .prologue
    .line 48
    return-void
.end method

.method public onTouchEvent(Landroid/support/v7/widget/RecyclerView;Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "rv"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 44
    return-void
.end method
