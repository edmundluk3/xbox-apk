.class public Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "ClubChatNotificationScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChatNotificationParameters"
.end annotation


# instance fields
.field private chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

.field private clubId:J


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;J)V
    .locals 2
    .param p1, "chatChannel"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 106
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 107
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 109
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;->chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 110
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;->clubId:J

    .line 111
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;->chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel$ChatNotificationParameters;->clubId:J

    return-wide v0
.end method
