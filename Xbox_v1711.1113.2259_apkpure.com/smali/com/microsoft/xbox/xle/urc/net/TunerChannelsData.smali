.class public Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;
.super Ljava/lang/Object;
.source "TunerChannelsData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TunerChannelsData"


# instance fields
.field public foundChannels:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;

.field public headendId:Ljava/lang/String;

.field public serviceChannels:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Lorg/json/JSONObject;)[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;
    .locals 11
    .param p0, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 49
    :try_start_0
    const-string v8, "params"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "providers"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 50
    .local v6, "providers":Lorg/json/JSONArray;
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    new-array v0, v8, [Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    .line 51
    .local v0, "data":[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v3, v8, :cond_2

    .line 52
    new-instance v8, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    invoke-direct {v8}, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;-><init>()V

    aput-object v8, v0, v3

    .line 53
    invoke-virtual {v6, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 55
    .local v5, "provider":Lorg/json/JSONObject;
    aget-object v8, v0, v3

    const-string v9, "headendId"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->headendId:Ljava/lang/String;

    .line 57
    const-string v8, "cqsChannels"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 58
    .local v7, "svcChannels":Lorg/json/JSONArray;
    aget-object v8, v0, v3

    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    iput-object v9, v8, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->serviceChannels:[Ljava/lang/String;

    .line 59
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 60
    aget-object v8, v0, v3

    iget-object v8, v8, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->serviceChannels:[Ljava/lang/String;

    invoke-virtual {v7, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    .line 59
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 63
    :cond_0
    const-string v8, "foundChannels"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 64
    .local v2, "fndChannels":Lorg/json/JSONArray;
    aget-object v8, v0, v3

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v9

    new-array v9, v9, [Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;

    iput-object v9, v8, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->foundChannels:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;

    .line 65
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v4, v8, :cond_1

    .line 66
    aget-object v8, v0, v3

    iget-object v8, v8, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->foundChannels:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;

    new-instance v9, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;-><init>(Lorg/json/JSONObject;)V

    aput-object v9, v8, v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 51
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 71
    .end local v0    # "data":[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;
    .end local v2    # "fndChannels":Lorg/json/JSONArray;
    .end local v3    # "i":I
    .end local v4    # "j":I
    .end local v5    # "provider":Lorg/json/JSONObject;
    .end local v6    # "providers":Lorg/json/JSONArray;
    .end local v7    # "svcChannels":Lorg/json/JSONArray;
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Lorg/json/JSONException;
    const-string v8, "TunerChannelsData"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bad json blob: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const/4 v0, 0x0

    .end local v1    # "e":Lorg/json/JSONException;
    :cond_2
    return-object v0
.end method
