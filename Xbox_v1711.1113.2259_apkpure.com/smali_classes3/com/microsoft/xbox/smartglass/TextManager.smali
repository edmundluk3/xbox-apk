.class public Lcom/microsoft/xbox/smartglass/TextManager;
.super Lcom/microsoft/xbox/smartglass/Listenable;
.source "TextManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/Listenable",
        "<",
        "Lcom/microsoft/xbox/smartglass/TextManagerListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final _pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>(J)V
    .locals 5
    .param p1, "pPlatform"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/Listenable;-><init>()V

    .line 18
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/TextManager;->initialize(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/TextManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 19
    return-void
.end method

.method private native complete(JI)V
.end method

.method private native initialize(J)J
.end method

.method private native isSessionActive(J)Z
.end method

.method private onCompleted()V
    .locals 3

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/TextManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/TextManagerListener;

    .line 53
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/TextManagerListener;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/TextManagerListener;->onCompleted()V

    goto :goto_0

    .line 55
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/TextManagerListener;
    :cond_0
    return-void
.end method

.method private onConfigurationChanged(IILjava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1, "options"    # I
    .param p2, "inputScope"    # I
    .param p3, "locale"    # Ljava/lang/String;
    .param p4, "prompt"    # Ljava/lang/String;
    .param p5, "maxTextLength"    # I

    .prologue
    .line 44
    new-instance v0, Lcom/microsoft/xbox/smartglass/TextConfiguration;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/smartglass/TextConfiguration;-><init>(IILjava/lang/String;Ljava/lang/String;I)V

    .line 45
    .local v0, "config":Lcom/microsoft/xbox/smartglass/TextConfiguration;
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/TextManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/smartglass/TextManagerListener;

    .line 46
    .local v6, "listener":Lcom/microsoft/xbox/smartglass/TextManagerListener;
    invoke-virtual {v6, v0}, Lcom/microsoft/xbox/smartglass/TextManagerListener;->onConfigurationChanged(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V

    goto :goto_0

    .line 48
    .end local v6    # "listener":Lcom/microsoft/xbox/smartglass/TextManagerListener;
    :cond_0
    return-void
.end method

.method private onTextChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/TextManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/TextManagerListener;

    .line 60
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/TextManagerListener;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/TextManagerListener;->onTextChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/TextManagerListener;
    :cond_0
    return-void
.end method

.method private onTextSelected(II)V
    .locals 3
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/TextManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/TextManagerListener;

    .line 67
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/TextManagerListener;
    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/smartglass/TextManagerListener;->onTextSelected(II)V

    goto :goto_0

    .line 69
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/TextManagerListener;
    :cond_0
    return-void
.end method

.method private native updateText(JLjava/lang/String;)V
.end method


# virtual methods
.method public complete(Lcom/microsoft/xbox/smartglass/TextAction;)V
    .locals 3
    .param p1, "action"    # Lcom/microsoft/xbox/smartglass/TextAction;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TextManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/TextAction;->getValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/microsoft/xbox/smartglass/TextManager;->complete(JI)V

    .line 35
    return-void
.end method

.method public isSessionActive()Z
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TextManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/TextManager;->isSessionActive(J)Z

    move-result v0

    return v0
.end method

.method public updateText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TextManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/TextManager;->updateText(JLjava/lang/String;)V

    .line 27
    return-void
.end method
