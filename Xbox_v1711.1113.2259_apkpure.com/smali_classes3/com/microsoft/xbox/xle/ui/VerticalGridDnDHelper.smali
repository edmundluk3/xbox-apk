.class public Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;
.super Ljava/lang/Object;
.source "VerticalGridDnDHelper.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;,
        Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;,
        Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnLongClickListener;",
        "Landroid/view/View$OnDragListener;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ANIM_RANGE:I = 0x190

.field private static final DURATION:J = 0x12cL

.field private static final PUSH_THRESH_DP:F = 50.0f

.field private static final SCALE:F = 1.1f

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final adp:Landroid/widget/BaseAdapter;

.field private final animDown:Landroid/animation/ValueAnimator;

.field private final animDownListener:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper",
            "<TT;>.Anim",
            "Listener;"
        }
    .end annotation
.end field

.field private final animUp:Landroid/animation/ValueAnimator;

.field private final animUpListener:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper",
            "<TT;>.Anim",
            "Listener;"
        }
    .end annotation
.end field

.field private final cfg:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config",
            "<TT;>;"
        }
    .end annotation
.end field

.field private currentPosition:I

.field private endPosition:I

.field private final grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

.field private final offsets:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;",
            ">;"
        }
    .end annotation
.end field

.field private final prevOffsets:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;",
            ">;"
        }
    .end annotation
.end field

.field private ptCurrent:Landroid/graphics/PointF;

.field private ptStart:Landroid/graphics/PointF;

.field private final pushThresh:F

.field private startPosition:I

.field private final treeObserver:Landroid/view/ViewTreeObserver$OnPreDrawListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->$assertionsDisabled:Z

    .line 32
    const-class v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->TAG:Ljava/lang/String;

    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;Landroid/widget/BaseAdapter;Lcom/microsoft/xbox/toolkit/ui/XLEGridView;)V
    .locals 4
    .param p2, "adp"    # Landroid/widget/BaseAdapter;
    .param p3, "grid"    # Lcom/microsoft/xbox/toolkit/ui/XLEGridView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config",
            "<TT;>;",
            "Landroid/widget/BaseAdapter;",
            "Lcom/microsoft/xbox/toolkit/ui/XLEGridView;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    .local p1, "cfg":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config<TT;>;"
    const/4 v2, 0x2

    const/4 v3, -0x1

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->offsets:Ljava/util/HashMap;

    .line 42
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->prevOffsets:Ljava/util/HashMap;

    .line 48
    new-array v1, v2, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    .line 49
    new-array v1, v2, [I

    fill-array-data v1, :array_1

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    .line 51
    new-instance v1, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUpListener:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;

    .line 71
    new-instance v1, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$2;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDownListener:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;

    .line 91
    new-instance v1, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->treeObserver:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 111
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cfg:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;

    .line 112
    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->adp:Landroid/widget/BaseAdapter;

    .line 113
    iput-object p3, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    .line 114
    invoke-virtual {p3}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 115
    .local v0, "scale":F
    const/high16 v1, 0x42480000    # 50.0f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->pushThresh:F

    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v3}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 118
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUpListener:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUpListener:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v3}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDownListener:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 123
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDownListener:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 126
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->treeObserver:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 127
    return-void

    .line 48
    :array_0
    .array-data 4
        0x0
        0x18f
    .end array-data

    .line 49
    :array_1
    .array-data 4
        0x0
        0x18f
    .end array-data
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->shouldScrollUp()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptCurrent:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;Landroid/graphics/PointF;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;
    .param p1, "x1"    # Landroid/graphics/PointF;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->processPosition(Landroid/graphics/PointF;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->shouldScrollDown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->offsets:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;Landroid/view/View;Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animateView(Landroid/view/View;Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->prevOffsets:Ljava/util/HashMap;

    return-object v0
.end method

.method private animateView(Landroid/view/View;Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "off"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;

    .prologue
    .line 240
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    iget-object v0, p2, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 241
    iget-object v0, p2, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 242
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p2, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    neg-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationXBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p2, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    neg-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-wide v2, p2, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->duration:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 243
    return-void
.end method

.method private static cancelAnimation(Landroid/animation/ValueAnimator;)V
    .locals 1
    .param p0, "anim"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 313
    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 316
    :cond_0
    return-void
.end method

.method private computeOffsets(II)V
    .locals 21
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 246
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getFirstVisiblePosition()I

    move-result v3

    .line 247
    .local v3, "fvp":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getLastVisiblePosition()I

    move-result v5

    .line 249
    .local v5, "lvp":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->prevOffsets:Ljava/util/HashMap;

    invoke-virtual {v14}, Ljava/util/HashMap;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_4

    .line 250
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->prevOffsets:Ljava/util/HashMap;

    invoke-virtual {v14}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 251
    .local v7, "position":I
    if-gt v3, v7, :cond_0

    if-gt v7, v5, :cond_0

    .line 252
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->prevOffsets:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;

    .line 253
    .local v6, "off":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    sub-int v16, v7, v3

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 254
    .local v9, "v":Landroid/view/View;
    invoke-virtual {v9}, Landroid/view/View;->getTranslationX()F

    move-result v12

    .line 255
    .local v12, "x":F
    invoke-virtual {v9}, Landroid/view/View;->getTranslationY()F

    move-result v13

    .line 256
    .local v13, "y":F
    const/4 v14, 0x0

    cmpl-float v14, v12, v14

    if-nez v14, :cond_1

    const/4 v14, 0x0

    cmpl-float v14, v13, v14

    if-eqz v14, :cond_0

    .line 257
    :cond_1
    iget-object v14, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iget v14, v14, Landroid/graphics/PointF;->x:F

    const/16 v16, 0x0

    cmpl-float v14, v14, v16

    if-eqz v14, :cond_2

    .line 258
    iget-wide v0, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->duration:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    long-to-float v14, v0

    mul-float/2addr v14, v12

    iget-object v0, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v16, v0

    div-float v14, v14, v16

    float-to-long v0, v14

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    iput-wide v0, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->duration:J

    .line 259
    iget-object v14, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iput v12, v14, Landroid/graphics/PointF;->x:F

    .line 260
    iget-object v14, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iput v13, v14, Landroid/graphics/PointF;->y:F

    .line 261
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->offsets:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 262
    :cond_2
    iget-object v14, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iget v14, v14, Landroid/graphics/PointF;->y:F

    const/16 v16, 0x0

    cmpl-float v14, v14, v16

    if-eqz v14, :cond_0

    .line 263
    iget-wide v0, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->duration:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    long-to-float v14, v0

    mul-float/2addr v14, v13

    iget-object v0, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    div-float v14, v14, v16

    float-to-long v0, v14

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    iput-wide v0, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->duration:J

    .line 264
    iget-object v14, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iput v12, v14, Landroid/graphics/PointF;->x:F

    .line 265
    iget-object v14, v6, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;->p:Landroid/graphics/PointF;

    iput v13, v14, Landroid/graphics/PointF;->y:F

    .line 266
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->offsets:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 271
    .end local v6    # "off":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;
    .end local v7    # "position":I
    .end local v9    # "v":Landroid/view/View;
    .end local v12    # "x":F
    .end local v13    # "y":F
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->prevOffsets:Ljava/util/HashMap;

    invoke-virtual {v14}, Ljava/util/HashMap;->clear()V

    .line 274
    :cond_4
    move/from16 v0, p1

    move/from16 v1, p2

    if-ge v0, v1, :cond_5

    .line 275
    move/from16 v0, p1

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 276
    .local v8, "start":I
    move v4, v8

    .local v4, "i":I
    :goto_1
    move/from16 v0, p2

    if-ge v4, v0, :cond_6

    .line 277
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    sub-int v15, v4, v3

    invoke-virtual {v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 278
    .local v10, "v1":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    sub-int v15, v4, v3

    add-int/lit8 v15, v15, 0x1

    invoke-virtual {v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 279
    .local v11, "v2":Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v14

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v15

    sub-int/2addr v14, v15

    int-to-float v12, v14

    .line 280
    .restart local v12    # "x":F
    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v14

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v15

    sub-int/2addr v14, v15

    int-to-float v13, v14

    .line 281
    .restart local v13    # "y":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->offsets:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    new-instance v16, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;

    new-instance v17, Landroid/graphics/PointF;

    invoke-virtual {v11}, Landroid/view/View;->getTranslationX()F

    move-result v18

    add-float v18, v18, v12

    invoke-virtual {v11}, Landroid/view/View;->getTranslationY()F

    move-result v19

    add-float v19, v19, v13

    invoke-direct/range {v17 .. v19}, Landroid/graphics/PointF;-><init>(FF)V

    const-wide/16 v18, 0x12c

    const/16 v20, 0x0

    invoke-direct/range {v16 .. v20}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;-><init>(Landroid/graphics/PointF;JLcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;)V

    invoke-virtual/range {v14 .. v16}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 284
    .end local v4    # "i":I
    .end local v8    # "start":I
    .end local v10    # "v1":Landroid/view/View;
    .end local v11    # "v2":Landroid/view/View;
    .end local v12    # "x":F
    .end local v13    # "y":F
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getLastVisiblePosition()I

    move-result v14

    move/from16 v0, p1

    invoke-static {v0, v14}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 285
    .local v2, "end":I
    add-int/lit8 v4, p2, 0x1

    .restart local v4    # "i":I
    :goto_2
    if-gt v4, v2, :cond_6

    .line 286
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    sub-int v15, v4, v3

    invoke-virtual {v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 287
    .restart local v10    # "v1":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    sub-int v15, v4, v3

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v14, v15}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 288
    .restart local v11    # "v2":Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v14

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v15

    sub-int/2addr v14, v15

    int-to-float v12, v14

    .line 289
    .restart local v12    # "x":F
    invoke-virtual {v11}, Landroid/view/View;->getTop()I

    move-result v14

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v15

    sub-int/2addr v14, v15

    int-to-float v13, v14

    .line 290
    .restart local v13    # "y":F
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->offsets:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    new-instance v16, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;

    new-instance v17, Landroid/graphics/PointF;

    invoke-virtual {v11}, Landroid/view/View;->getTranslationX()F

    move-result v18

    add-float v18, v18, v12

    invoke-virtual {v11}, Landroid/view/View;->getTranslationY()F

    move-result v19

    add-float v19, v19, v13

    invoke-direct/range {v17 .. v19}, Landroid/graphics/PointF;-><init>(FF)V

    const-wide/16 v18, 0x12c

    const/16 v20, 0x0

    invoke-direct/range {v16 .. v20}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;-><init>(Landroid/graphics/PointF;JLcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;)V

    invoke-virtual/range {v14 .. v16}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 293
    .end local v2    # "end":I
    .end local v10    # "v1":Landroid/view/View;
    .end local v11    # "v2":Landroid/view/View;
    .end local v12    # "x":F
    .end local v13    # "y":F
    :cond_6
    return-void
.end method

.method private findPosition(Landroid/graphics/PointF;)I
    .locals 9
    .param p1, "p"    # Landroid/graphics/PointF;

    .prologue
    .line 150
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    const/4 v4, -0x1

    .line 151
    .local v4, "position":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildCount()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 152
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v7, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 153
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getHorizontalSpacingCompat()I

    move-result v8

    sub-int v3, v7, v8

    .line 154
    .local v3, "left":I
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getVerticalSpacingCompat()I

    move-result v8

    add-int v6, v7, v8

    .line 155
    .local v6, "top":I
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getHorizontalSpacingCompat()I

    move-result v8

    add-int v5, v7, v8

    .line 156
    .local v5, "right":I
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getVerticalSpacingCompat()I

    move-result v8

    add-int v0, v7, v8

    .line 157
    .local v0, "bottom":I
    int-to-float v7, v3

    iget v8, p1, Landroid/graphics/PointF;->x:F

    cmpg-float v7, v7, v8

    if-gtz v7, :cond_1

    iget v7, p1, Landroid/graphics/PointF;->x:F

    int-to-float v8, v5

    cmpg-float v7, v7, v8

    if-gez v7, :cond_1

    int-to-float v7, v6

    iget v8, p1, Landroid/graphics/PointF;->y:F

    cmpg-float v7, v7, v8

    if-gtz v7, :cond_1

    iget v7, p1, Landroid/graphics/PointF;->y:F

    int-to-float v8, v0

    cmpg-float v7, v7, v8

    if-gez v7, :cond_1

    .line 158
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v7, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getPositionForView(Landroid/view/View;)I

    move-result v4

    .line 162
    .end local v0    # "bottom":I
    .end local v1    # "child":Landroid/view/View;
    .end local v3    # "left":I
    .end local v5    # "right":I
    .end local v6    # "top":I
    :cond_0
    return v4

    .line 151
    .restart local v0    # "bottom":I
    .restart local v1    # "child":Landroid/view/View;
    .restart local v3    # "left":I
    .restart local v5    # "right":I
    .restart local v6    # "top":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private processPosition(Landroid/view/DragEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 222
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/DragEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/DragEvent;->getY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptCurrent:Landroid/graphics/PointF;

    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptCurrent:Landroid/graphics/PointF;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->processPosition(Landroid/graphics/PointF;)Z

    .line 224
    return-void
.end method

.method private processPosition(Landroid/graphics/PointF;)Z
    .locals 4
    .param p1, "p"    # Landroid/graphics/PointF;

    .prologue
    .line 227
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    const/4 v1, 0x0

    .line 228
    .local v1, "ret":Z
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->findPosition(Landroid/graphics/PointF;)I

    move-result v0

    .line 229
    .local v0, "position":I
    if-ltz v0, :cond_0

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->currentPosition:I

    if-eq v0, v2, :cond_0

    .line 230
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cfg:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;->getData()Ljava/util/List;

    move-result-object v2

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->currentPosition:I

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->moveElement(Ljava/util/List;II)V

    .line 231
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->currentPosition:I

    invoke-direct {p0, v2, v0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->computeOffsets(II)V

    .line 232
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->currentPosition:I

    .line 233
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->adp:Landroid/widget/BaseAdapter;

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 234
    const/4 v1, 0x1

    .line 236
    :cond_0
    return v1
.end method

.method private processScrollAnimation()V
    .locals 1

    .prologue
    .line 296
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->shouldScrollUp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cancelAnimation(Landroid/animation/ValueAnimator;)V

    .line 299
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 310
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->shouldScrollDown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 302
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cancelAnimation(Landroid/animation/ValueAnimator;)V

    .line 304
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cancelAnimation(Landroid/animation/ValueAnimator;)V

    .line 308
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cancelAnimation(Landroid/animation/ValueAnimator;)V

    goto :goto_0
.end method

.method private shouldScrollDown()Z
    .locals 8

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 354
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getPaddingBottom()I

    move-result v6

    sub-int v0, v5, v6

    .line 355
    .local v0, "bottom":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptCurrent:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    int-to-float v6, v0

    iget v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->pushThresh:F

    sub-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_0

    int-to-float v5, v0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptCurrent:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    :cond_0
    move v3, v4

    .line 367
    :cond_1
    :goto_0
    return v3

    .line 358
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cfg:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;

    invoke-interface {v5}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;->getData()Ljava/util/List;

    move-result-object v1

    .line 359
    .local v1, "data":Ljava/util/List;, "Ljava/util/List<TT;>;"
    sget-boolean v5, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    if-nez v1, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 360
    :cond_3
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getLastVisiblePosition()I

    move-result v5

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-lt v5, v6, :cond_1

    .line 363
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 364
    .local v2, "lastChild":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    if-gt v5, v6, :cond_1

    move v3, v4

    .line 367
    goto :goto_0
.end method

.method private shouldScrollUp()Z
    .locals 6

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 339
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getPaddingTop()I

    move-result v1

    .line 340
    .local v1, "top":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptCurrent:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    int-to-float v5, v1

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_0

    int-to-float v4, v1

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->pushThresh:F

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptCurrent:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    :cond_0
    move v2, v3

    .line 350
    :cond_1
    :goto_0
    return v2

    .line 343
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getFirstVisiblePosition()I

    move-result v4

    if-gtz v4, :cond_1

    .line 346
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 347
    .local v0, "firstChild":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getPaddingTop()I

    move-result v5

    if-lt v4, v5, :cond_1

    move v2, v3

    .line 350
    goto :goto_0
.end method


# virtual methods
.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 134
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->currentPosition:I

    return v0
.end method

.method public getEndPosition()I
    .locals 1

    .prologue
    .line 142
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->endPosition:I

    return v0
.end method

.method public getStartPosition()I
    .locals 1

    .prologue
    .line 138
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->startPosition:I

    return v0
.end method

.method public isDragging()Z
    .locals 1

    .prologue
    .line 130
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptStart:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 167
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    const/4 v1, 0x1

    .line 168
    .local v1, "ret":Z
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 218
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 170
    :pswitch_1
    sget-object v5, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->TAG:Ljava/lang/String;

    const-string v6, "ACTION_DRAG_STARTED"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :pswitch_2
    sget-object v5, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->TAG:Ljava/lang/String;

    const-string v6, "ACTION_DRAG_ENTERED"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :pswitch_3
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptStart:Landroid/graphics/PointF;

    if-nez v5, :cond_1

    .line 177
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v5

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v6

    invoke-direct {v2, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 178
    .local v2, "startPoint":Landroid/graphics/PointF;
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->findPosition(Landroid/graphics/PointF;)I

    move-result v3

    .line 179
    .local v3, "startPos":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getFirstVisiblePosition()I

    move-result v6

    sub-int v6, v3, v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 180
    .local v4, "startView":Landroid/view/View;
    if-eqz v4, :cond_1

    .line 181
    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptCurrent:Landroid/graphics/PointF;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptStart:Landroid/graphics/PointF;

    .line 182
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->currentPosition:I

    iput v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->startPosition:I

    .line 183
    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 184
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cfg:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;

    iget v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->startPosition:I

    invoke-interface {v5, v6}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;->onDrag(I)V

    .line 187
    .end local v2    # "startPoint":Landroid/graphics/PointF;
    .end local v3    # "startPos":I
    .end local v4    # "startView":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->isDragging()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 188
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->processPosition(Landroid/view/DragEvent;)V

    .line 189
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->processScrollAnimation()V

    goto :goto_0

    .line 193
    :pswitch_4
    sget-object v5, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->TAG:Ljava/lang/String;

    const-string v6, "ACTION_DRAG_EXITED"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->isDragging()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 195
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cancelAnimation(Landroid/animation/ValueAnimator;)V

    .line 196
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cancelAnimation(Landroid/animation/ValueAnimator;)V

    goto :goto_0

    .line 202
    :pswitch_5
    sget-object v5, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->TAG:Ljava/lang/String;

    const-string v6, "ACTION_DRAG_ENDED"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->isDragging()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 204
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animUp:Landroid/animation/ValueAnimator;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cancelAnimation(Landroid/animation/ValueAnimator;)V

    .line 205
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->animDown:Landroid/animation/ValueAnimator;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cancelAnimation(Landroid/animation/ValueAnimator;)V

    .line 206
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    iget v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->currentPosition:I

    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->grid:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getFirstVisiblePosition()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 207
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 208
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 212
    :goto_1
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptCurrent:Landroid/graphics/PointF;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->ptStart:Landroid/graphics/PointF;

    .line 213
    iget v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->currentPosition:I

    iput v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->endPosition:I

    .line 214
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->cfg:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;

    iget v6, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->endPosition:I

    invoke-interface {v5, v6}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;->onDrop(I)V

    goto/16 :goto_0

    .line 210
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->adp:Landroid/widget/BaseAdapter;

    invoke-virtual {v5}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_1

    .line 168
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 320
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    const/4 v0, 0x0

    .line 321
    .local v0, "clipData":Landroid/content/ClipData;
    new-instance v1, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$4;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;Landroid/view/View;)V

    .line 334
    .local v1, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 335
    const/4 v2, 0x1

    return v2
.end method

.method public onViewBound(Landroid/view/View;I)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 146
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->getCurrentPosition()I

    move-result v0

    if-ne p2, v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 147
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
