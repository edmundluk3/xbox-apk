.class Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;
.super Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullGameRecentHolder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 0
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "parentWidth"    # I
    .param p3, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p4, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 438
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    .line 439
    return-void
.end method


# virtual methods
.method public bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 7
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    const/4 v5, 0x0

    .line 443
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->bindNPI(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 445
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v2

    .line 447
    .local v2, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 448
    .local v1, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBatGameUri()Ljava/lang/String;

    move-result-object v3

    .line 449
    .local v3, "uri":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 450
    .local v0, "defaultRid":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v6, 0x7f0e001d

    invoke-virtual {v4, v6, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 451
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v4, v3, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 452
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->updateImageBackground(Landroid/widget/ImageView;)V

    .line 455
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-nez v3, :cond_1

    const/4 v4, 0x4

    :goto_1
    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 457
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v6, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v6, v6, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 459
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 460
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->setStatsControlsVisible(Z)V

    .line 461
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->heroStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeroStat()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 462
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->progress:Lcom/microsoft/xbox/xle/ui/LabeledArcView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getProgressPercentage()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->setPercentage(I)V

    .line 463
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->bindGamerScore(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;)V

    .line 464
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->achievements:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getAchievementsEarned()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 469
    :goto_2
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 470
    return-void

    .line 449
    .end local v0    # "defaultRid":I
    :cond_0
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    goto :goto_0

    .restart local v0    # "defaultRid":I
    :cond_1
    move v4, v5

    .line 455
    goto :goto_1

    .line 466
    :cond_2
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->setStatsControlsVisible(Z)V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 474
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;->onRecentClick(I)V

    .line 475
    return-void
.end method
