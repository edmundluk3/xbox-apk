.class public Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "TVEpisodeSeasonScreenAdapter.java"


# instance fields
.field private episodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    .line 34
    const v0, 0x7f0e0b5d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->screenBody:Landroid/view/View;

    .line 35
    const v0, 0x7f0e0b5e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 37
    const v0, 0x7f0e0b5f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 38
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030254

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 47
    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->updateLoadingIndicator(Z)V

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->getEpisodes()Ljava/util/ArrayList;

    move-result-object v0

    .line 57
    .local v0, "newEpisodes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->episodes:Ljava/util/ArrayList;

    if-eq v1, v0, :cond_1

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;->clear()V

    .line 59
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->episodes:Ljava/util/ArrayList;

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->episodes:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->episodes:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 67
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->isBusy()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->episodes:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/TVEpisodeSeasonScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->removeScreenFromPivot(Ljava/lang/Class;)V

    .line 71
    :cond_2
    return-void
.end method
