.class public final Lcom/google/android/exoplayer2/util/AmazonQuirks;
.super Ljava/lang/Object;
.source "AmazonQuirks.java"


# static fields
.field private static final AMAZON:Ljava/lang/String; = "Amazon"

.field private static final AUDIO_HARDWARE_LATENCY_FOR_TABLETS:I = 0x15f90

.field private static final DEVICEMODEL:Ljava/lang/String;

.field private static final FIRETV_GEN1_DEVICE_MODEL:Ljava/lang/String; = "AFTB"

.field private static final FIRETV_GEN2_DEVICE_MODEL:Ljava/lang/String; = "AFTS"

.field private static final FIRETV_STICK_DEVICE_MODEL:Ljava/lang/String; = "AFTM"

.field private static final FIRETV_STICK_GEN2_DEVICE_MODEL:Ljava/lang/String; = "AFTT"

.field private static final FIRE_PHONE_DEVICE_MODEL:Ljava/lang/String; = "SD"

.field private static final KINDLE_TABLET_DEVICE_MODEL:Ljava/lang/String; = "KF"

.field private static final MANUFACTURER:Ljava/lang/String;

.field private static final MAX_INPUT_SECURE_AVC_SIZE_FIRETV_GEN2:I = 0x2ccccc

.field private static final TAG:Ljava/lang/String;

.field private static final isAmazonDevice:Z

.field private static final isFirePhone:Z

.field private static final isFireTVGen1:Z

.field private static final isFireTVGen2:Z

.field private static final isFireTVStick:Z

.field private static final isKindleTablet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24
    const-class v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->TAG:Ljava/lang/String;

    .line 33
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->DEVICEMODEL:Ljava/lang/String;

    .line 34
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sput-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->MANUFACTURER:Ljava/lang/String;

    .line 52
    sget-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "Amazon"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isAmazonDevice:Z

    .line 53
    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isAmazonDevice:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->DEVICEMODEL:Ljava/lang/String;

    const-string v3, "AFTB"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen1:Z

    .line 54
    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isAmazonDevice:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->DEVICEMODEL:Ljava/lang/String;

    const-string v3, "AFTS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen2:Z

    .line 55
    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isAmazonDevice:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->DEVICEMODEL:Ljava/lang/String;

    const-string v3, "AFTM"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    sput-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVStick:Z

    .line 56
    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isAmazonDevice:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->DEVICEMODEL:Ljava/lang/String;

    const-string v3, "KF"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    sput-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isKindleTablet:Z

    .line 57
    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isAmazonDevice:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->DEVICEMODEL:Ljava/lang/String;

    const-string v3, "SD"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_4
    sput-boolean v1, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFirePhone:Z

    .line 58
    return-void

    :cond_0
    move v0, v2

    .line 53
    goto :goto_0

    :cond_1
    move v0, v2

    .line 54
    goto :goto_1

    :cond_2
    move v0, v2

    .line 55
    goto :goto_2

    :cond_3
    move v0, v2

    .line 56
    goto :goto_3

    :cond_4
    move v1, v2

    .line 57
    goto :goto_4
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static codecNeedsEosPropagationWorkaround(Ljava/lang/String;)Z
    .locals 4
    .param p0, "codecName"    # Ljava/lang/String;

    .prologue
    .line 122
    invoke-static {}, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen2()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ".secure"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 123
    .local v0, "needsWorkaround":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 124
    sget-object v1, Lcom/google/android/exoplayer2/util/AmazonQuirks;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Codec Needs EOS Propagation Workaround "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_0
    return v0

    .line 122
    .end local v0    # "needsWorkaround":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getAudioHWLatency()I
    .locals 1

    .prologue
    .line 103
    const v0, 0x15f90

    return v0
.end method

.method public static isAmazonDevice()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isAmazonDevice:Z

    return v0
.end method

.method public static isDolbyPassthroughQuirkEnabled()Z
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen1Family()Z

    move-result v0

    return v0
.end method

.method public static isFireTVGen1Family()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen1:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVStick:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFireTVGen2()Z
    .locals 1

    .prologue
    .line 76
    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen2:Z

    return v0
.end method

.method public static isLatencyQuirkEnabled()Z
    .locals 2

    .prologue
    .line 96
    sget v0, Lcom/google/android/exoplayer2/util/Util;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_1

    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isKindleTablet:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFirePhone:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMaxInputSizeSupported(Ljava/lang/String;I)Z
    .locals 1
    .param p0, "codecName"    # Ljava/lang/String;
    .param p1, "inputSize"    # I

    .prologue
    .line 129
    sget-boolean v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen2:Z

    if-eqz v0, :cond_0

    if-eqz p0, :cond_0

    .line 130
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "AVC.secure"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x2ccccc

    if-gt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    .line 130
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static shouldExtractPlayReadyHeader()Z
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen1Family()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen2()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static useDefaultPassthroughDecoder()Z
    .locals 2

    .prologue
    .line 85
    invoke-static {}, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen1Family()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    sget-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->TAG:Ljava/lang/String;

    const-string v1, "Using platform Dolby decoder"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const/4 v0, 0x0

    .line 91
    :goto_0
    return v0

    .line 90
    :cond_0
    sget-object v0, Lcom/google/android/exoplayer2/util/AmazonQuirks;->TAG:Ljava/lang/String;

    const-string v1, "Using default Dolby pass-through decoder"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static waitForDRMKeysBeforeInitCodec()Z
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/google/android/exoplayer2/util/AmazonQuirks;->isFireTVGen1Family()Z

    move-result v0

    return v0
.end method
