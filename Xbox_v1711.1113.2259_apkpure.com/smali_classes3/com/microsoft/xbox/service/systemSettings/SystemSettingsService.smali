.class public final enum Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;
.super Ljava/lang/Enum;
.source "SystemSettingsService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/systemSettings/ISystemSettingsService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;",
        ">;",
        "Lcom/microsoft/xbox/service/systemSettings/ISystemSettingsService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

.field private static final BACK_COMPAT_ITEMS_ENDPOINT:Ljava/lang/String; = "https://settings.data.microsoft.com/settings/v2.0/xbox/backcompatcatalogidmapall?scenarioid=all"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->INSTANCE:Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    .line 16
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    sget-object v1, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->INSTANCE:Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->$VALUES:[Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    .line 19
    sget-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->INSTANCE:Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->TAG:Ljava/lang/String;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->STATIC_HEADERS:Ljava/util/List;

    .line 27
    sget-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->$VALUES:[Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;

    return-object v0
.end method


# virtual methods
.method public getBackCompatItems()Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 33
    const-string v0, "https://settings.data.microsoft.com/settings/v2.0/xbox/backcompatcatalogidmapall?scenarioid=all"

    sget-object v1, Lcom/microsoft/xbox/service/systemSettings/SystemSettingsService;->STATIC_HEADERS:Ljava/util/List;

    .line 34
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;

    .line 33
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/systemSettings/BackCompatDataTypes$BackCompatResponse;

    return-object v0
.end method
