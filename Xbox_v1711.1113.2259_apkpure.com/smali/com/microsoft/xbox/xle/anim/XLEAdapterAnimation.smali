.class public Lcom/microsoft/xbox/xle/anim/XLEAdapterAnimation;
.super Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;
.source "XLEAdapterAnimation.java"


# instance fields
.field public animations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;",
            ">;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;-><init>()V

    return-void
.end method


# virtual methods
.method public compile()Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 4

    .prologue
    .line 37
    new-instance v1, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;-><init>()V

    .line 39
    .local v1, "pack":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/anim/XLEAdapterAnimation;->animations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;

    .line 40
    .local v0, "anim":Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;->compile()Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;->add(Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;)V

    goto :goto_0

    .line 43
    .end local v0    # "anim":Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;
    :cond_0
    return-object v1
.end method

.method public compile(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 4
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 47
    new-instance v1, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;-><init>()V

    .line 49
    .local v1, "pack":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/anim/XLEAdapterAnimation;->animations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;

    .line 50
    .local v0, "anim":Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;->compileWithRoot(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;->add(Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;)V

    goto :goto_0

    .line 53
    .end local v0    # "anim":Lcom/microsoft/xbox/xle/anim/XLEMAASAnimation;
    :cond_0
    return-object v1
.end method
