.class public abstract Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;
.super Ljava/lang/Object;
.source "ClubModerationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubBatchDeleteReportRequest"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;
    .locals 2
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "reports":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 104
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubBatchDeleteReportRequest;

    invoke-static {p0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubBatchDeleteReportRequest;-><init>(Lcom/google/common/collect/ImmutableList;)V

    return-object v0
.end method


# virtual methods
.method public abstract reportedItemsDeleteRequests()Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;",
            ">;"
        }
    .end annotation
.end method
