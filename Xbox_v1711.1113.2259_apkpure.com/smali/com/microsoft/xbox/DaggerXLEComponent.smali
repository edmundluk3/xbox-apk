.class public final Lcom/microsoft/xbox/DaggerXLEComponent;
.super Ljava/lang/Object;
.source "DaggerXLEComponent.java"

# interfaces
.implements Lcom/microsoft/xbox/XLEComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private activityFeedFilterDialogMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;",
            ">;"
        }
    .end annotation
.end field

.field private activityFeedFilterInteractorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private activityFeedFilterPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private activityFeedFilterViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private activityFeedScreenViewModelBaseMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;",
            ">;"
        }
    .end annotation
.end field

.field private beamNavigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/BeamNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private beamRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/beam/BeamRepository;",
            ">;"
        }
    .end annotation
.end field

.field private clubAdminMembersScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private clubChatNotificationScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private clubChatScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private clubCustomizeViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private clubHomeScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private clubPlayScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private clubWatchInteractorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private clubWatchPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private clubWatchViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private consoleConnectionScreenAdapterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private conversationDetailsActivityViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private conversationsActivityViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private customizeProfileScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private drawerAdapterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private friendPickerInteractorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private friendPickerPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private friendPickerViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private gcmBroadcastReceiverMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private homeScreenTelemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;",
            ">;"
        }
    .end annotation
.end field

.field private hoverChatBroadcastReceiverMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private hoverChatHeadContentDescriptionDownloaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private hoverChatHeadImageDownloaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private hoverChatManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;",
            ">;"
        }
    .end annotation
.end field

.field private hoverChatMenuScreenMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;",
            ">;"
        }
    .end annotation
.end field

.field private hoverChatServiceMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;",
            ">;"
        }
    .end annotation
.end field

.field private hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;",
            ">;"
        }
    .end annotation
.end field

.field private languageSettingsInteractorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private languageSettingsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private languageSettingsTelemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;",
            ">;"
        }
    .end annotation
.end field

.field private languageSettingsViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private lfgDetailsViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private lfgVettingScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private localeHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private localeProviderRnModuleMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;",
            ">;"
        }
    .end annotation
.end field

.field private mainActivityMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/MainActivity;",
            ">;"
        }
    .end annotation
.end field

.field private messageModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/service/model/MessageModel;",
            ">;"
        }
    .end annotation
.end field

.field private multiplayerSessionModelManagerMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;",
            ">;"
        }
    .end annotation
.end field

.field private multiplayerTelemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
            ">;"
        }
    .end annotation
.end field

.field private myClubsInteractorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private myClubsNavigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private myClubsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private myClubsViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private myXuidProviderRnModuleMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule;",
            ">;"
        }
    .end annotation
.end field

.field private navigationManagerMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;"
        }
    .end annotation
.end field

.field private oOBEInteractorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private oOBEPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private oOBETelemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService;",
            ">;"
        }
    .end annotation
.end field

.field private oOBEViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private pagesModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/service/model/PagesModel;",
            ">;"
        }
    .end annotation
.end field

.field private partyAndLfgScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private partyChatRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;",
            ">;"
        }
    .end annotation
.end field

.field private partyDetailsNavigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private partyDetailsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private partyDetailsViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private partyEventNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private partyInteractorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/party/PartyInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private partyMemberIconListAdapterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private partyTextListAdapterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private partyTextPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private partyTextViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private partyWebRtcRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;",
            ">;"
        }
    .end annotation
.end field

.field private peerConnectionClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;",
            ">;"
        }
    .end annotation
.end field

.field private peopleHubInfoScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private peopleScreenAdapterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private profileColorsRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;",
            ">;"
        }
    .end annotation
.end field

.field private profileModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/service/model/ProfileModel;",
            ">;"
        }
    .end annotation
.end field

.field private projectSpecificDialogManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideActivityFeedFilterRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;",
            ">;"
        }
    .end annotation
.end field

.field private provideApplicationContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private provideAuthManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideBeamEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideBeamRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideBeamServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamService;",
            ">;"
        }
    .end annotation
.end field

.field private provideChatHeadManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideChatHeadViewAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private provideCircularDrawableFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;",
            ">;"
        }
    .end annotation
.end field

.field private provideClubModelManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideClubWatchRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;",
            ">;"
        }
    .end annotation
.end field

.field private provideDefaultOkHttpClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private provideEditorialEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideEditorialRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideEditorialServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/editorial/EditorialService;",
            ">;"
        }
    .end annotation
.end field

.field private provideFacebookManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideGameServerEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideGameServerRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideGameServerServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/GameServerService;",
            ">;"
        }
    .end annotation
.end field

.field private provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/converter/gson/GsonConverterFactory;",
            ">;"
        }
    .end annotation
.end field

.field private provideHomeScreenRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;",
            ">;"
        }
    .end annotation
.end field

.field private provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;"
        }
    .end annotation
.end field

.field private provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;",
            ">;"
        }
    .end annotation
.end field

.field private provideMediaHubEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideMediaHubEndpointProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideMediaHubRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideMediaHubServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
            ">;"
        }
    .end annotation
.end field

.field private provideMultiplayerServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerService;",
            ">;"
        }
    .end annotation
.end field

.field private provideMultiplayerServiceProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;",
            ">;"
        }
    .end annotation
.end field

.field private provideMultiplayerSessionEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideMultiplayerSessionEndpointProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideMultiplayerSessionRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideMultiplayerSessionRetrofitProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideMyProfileProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/MyProfileProvider;",
            ">;"
        }
    .end annotation
.end field

.field private provideMyXuidProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;",
            ">;"
        }
    .end annotation
.end field

.field private provideNavigationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideNotificationDisplayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;",
            ">;"
        }
    .end annotation
.end field

.field private provideOOBESessionEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideOOBESessionRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideOOBESessionServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBEService;",
            ">;"
        }
    .end annotation
.end field

.field private provideOOBESettingsRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;",
            ">;"
        }
    .end annotation
.end field

.field private providePeopleHubEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private providePeopleHubRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private providePeopleHubServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;",
            ">;"
        }
    .end annotation
.end field

.field private providePrivacyClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private providePrivacyEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private providePrivacyRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private providePrivacyServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;",
            ">;"
        }
    .end annotation
.end field

.field private provideProfileColorRetrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field

.field private provideProfileColorServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;",
            ">;"
        }
    .end annotation
.end field

.field private provideReactInstanceManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/react/ReactInstanceManager;",
            ">;"
        }
    .end annotation
.end field

.field private provideSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field

.field private provideSharedPreferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private provideSystemSettingsModelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;"
        }
    .end annotation
.end field

.field private provideTitleBarViewProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/ui/TitleBarView;",
            ">;"
        }
    .end annotation
.end field

.field private provideTutorialRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserSummaryRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;",
            ">;"
        }
    .end annotation
.end field

.field private provideWelcomeCardCompletionRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;",
            ">;"
        }
    .end annotation
.end field

.field private provideWindowManagerContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;",
            ">;"
        }
    .end annotation
.end field

.field private provideXTokenOkHttpClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private provideXbLogProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;"
        }
    .end annotation
.end field

.field private providesEditorialClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private providesGameServerClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private providesMediaHubClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private providesMultiplayerSessionClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private providesMultiplayerSessionClientProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private providesOOBESessionClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private providesPeopleHubClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private qoSMeasurementRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository;",
            ">;"
        }
    .end annotation
.end field

.field private reactActivityBaseMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/base/ReactActivityBase;",
            ">;"
        }
    .end annotation
.end field

.field private reactModalActivityBaseMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;",
            ">;"
        }
    .end annotation
.end field

.field private rtaDataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private rtaRepositoryMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
            ">;"
        }
    .end annotation
.end field

.field private rtaRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
            ">;"
        }
    .end annotation
.end field

.field private settingsGeneralPageAdapterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private settingsGeneralPageViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private settingsNotificationsPageViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private socialTagModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/service/model/SocialTagModel;",
            ">;"
        }
    .end annotation
.end field

.field private storeServiceCacheManagerMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;",
            ">;"
        }
    .end annotation
.end field

.field private systemSettingsModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;"
        }
    .end annotation
.end field

.field private telemetryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;"
        }
    .end annotation
.end field

.field private trendingBeamInteractorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private trendingBeamPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private trendingBeamViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private tutorialInteractorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private tutorialNavigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private tutorialPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private tutorialTelemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;",
            ">;"
        }
    .end annotation
.end field

.field private tutorialViewImplMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;",
            ">;"
        }
    .end annotation
.end field

.field private uploadCustomPicScreenViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private userSummaryFromPeopleHubPersonDataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private userSummaryFromUserSearchResultDataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelWithConversationMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;",
            ">;"
        }
    .end annotation
.end field

.field private vortexServiceManagerMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;",
            ">;"
        }
    .end annotation
.end field

.field private xLEGlobalDataMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;",
            ">;"
        }
    .end annotation
.end field

.field private xTokenAuthenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private xXblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private xboxAuthActivityViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private xleProjectSpecificDataProviderMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;",
            ">;"
        }
    .end annotation
.end field

.field private xsapiSilentSignInViewModelMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 354
    const-class v0, Lcom/microsoft/xbox/DaggerXLEComponent;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/DaggerXLEComponent;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 735
    sget-boolean v0, Lcom/microsoft/xbox/DaggerXLEComponent;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 736
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/DaggerXLEComponent;->initialize(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)V

    .line 737
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/DaggerXLEComponent;->initialize2(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)V

    .line 738
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;Lcom/microsoft/xbox/DaggerXLEComponent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .param p2, "x1"    # Lcom/microsoft/xbox/DaggerXLEComponent$1;

    .prologue
    .line 358
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/DaggerXLEComponent;-><init>(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/DaggerXLEComponent$Builder;
    .locals 2

    .prologue
    .line 741
    new-instance v0, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;-><init>(Lcom/microsoft/xbox/DaggerXLEComponent$1;)V

    return-object v0
.end method

.method private initialize(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)V
    .locals 8
    .param p1, "builder"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 747
    .line 750
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    move-result-object v0

    .line 749
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule_ProvideMediaHubEndpointFactory;->create(Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 748
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubEndpointProvider:Ljavax/inject/Provider;

    .line 752
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    .line 755
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 758
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 763
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$200(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/ReleaseServiceModule;

    move-result-object v0

    .line 762
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/ReleaseServiceModule_ProvideHttpLoggingInterceptorFactory;->create(Lcom/microsoft/xbox/data/service/ReleaseServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 761
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    .line 766
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 769
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 774
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 773
    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule_ProvidesMediaHubClientFactory;->create(Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 772
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesMediaHubClientProvider:Ljavax/inject/Provider;

    .line 784
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$300(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/ServiceModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideGsonConvertorFactoryFactory;->create(Lcom/microsoft/xbox/data/service/ServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 783
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 789
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubEndpointProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesMediaHubClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 788
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule_ProvideMediaHubRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 787
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubRetrofitProvider:Ljavax/inject/Provider;

    .line 797
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$400(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubRetrofitProvider:Ljavax/inject/Provider;

    .line 796
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule_ProvideMediaHubServiceFactory;->create(Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 795
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubServiceProvider:Ljavax/inject/Provider;

    .line 801
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProvideSharedPreferencesFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 800
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSharedPreferencesProvider:Ljavax/inject/Provider;

    .line 806
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSharedPreferencesProvider:Ljavax/inject/Provider;

    .line 805
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideLanguageSettingsRespositoryFactory;->create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 804
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;

    .line 808
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;

    .line 809
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xLEGlobalDataMembersInjector:Ldagger/MembersInjector;

    .line 811
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;

    .line 812
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xleProjectSpecificDataProviderMembersInjector:Ldagger/MembersInjector;

    .line 817
    invoke-static {}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    .line 816
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedFilterDialogMembersInjector:Ldagger/MembersInjector;

    .line 822
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSharedPreferencesProvider:Ljavax/inject/Provider;

    .line 824
    invoke-static {}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper_Factory;->create()Ldagger/internal/Factory;

    move-result-object v2

    .line 825
    invoke-static {}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService_Factory;->create()Ldagger/internal/Factory;

    move-result-object v3

    .line 821
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideActivityFeedFilterRepositoryFactory;->create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 820
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideActivityFeedFilterRepositoryProvider:Ljavax/inject/Provider;

    .line 829
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProvideSchedulerProviderFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 828
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 831
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideActivityFeedFilterRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 832
    invoke-static {v0, v1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedFilterInteractorProvider:Ljavax/inject/Provider;

    .line 837
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedFilterInteractorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 836
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedFilterPresenterProvider:Ljavax/inject/Provider;

    .line 841
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedFilterPresenterProvider:Ljavax/inject/Provider;

    .line 842
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedFilterViewImplMembersInjector:Ldagger/MembersInjector;

    .line 846
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$700(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamEndpointFactory;->create(Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 845
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideBeamEndpointProvider:Ljavax/inject/Provider;

    .line 851
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$300(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/ServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 850
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->create(Lcom/microsoft/xbox/data/service/ServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 849
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideDefaultOkHttpClientProvider:Ljavax/inject/Provider;

    .line 858
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$700(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideBeamEndpointProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideDefaultOkHttpClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 857
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 856
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideBeamRetrofitProvider:Ljavax/inject/Provider;

    .line 866
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$700(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideBeamRetrofitProvider:Ljavax/inject/Provider;

    .line 865
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/beam/BeamServiceModule_ProvideBeamServiceFactory;->create(Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 864
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideBeamServiceProvider:Ljavax/inject/Provider;

    .line 870
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$800(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/domain/ModelManagerModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideClubModelManagerFactory;->create(Lcom/microsoft/xbox/domain/ModelManagerModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 869
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideClubModelManagerProvider:Ljavax/inject/Provider;

    .line 875
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideBeamServiceProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubServiceProvider:Ljavax/inject/Provider;

    .line 878
    invoke-static {}, Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper_Factory;->create()Ldagger/internal/Factory;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideClubModelManagerProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 874
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 873
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideClubWatchRepositoryProvider:Ljavax/inject/Provider;

    .line 882
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideClubWatchRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 883
    invoke-static {v0, v1}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubWatchInteractorProvider:Ljavax/inject/Provider;

    .line 888
    invoke-static {}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/data/service/beam/BeamTelemetryService_Factory;->create()Ldagger/internal/Factory;

    move-result-object v1

    .line 887
    invoke-static {v0, v1}, Lcom/microsoft/xbox/presentation/beam/BeamNavigator_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->beamNavigatorProvider:Ljavax/inject/Provider;

    .line 892
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$900(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/DataModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;->create(Lcom/microsoft/xbox/data/DataModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 891
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    .line 896
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProvideNavigationManagerFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 895
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideNavigationManagerProvider:Ljavax/inject/Provider;

    .line 900
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubWatchInteractorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->beamNavigatorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideNavigationManagerProvider:Ljavax/inject/Provider;

    .line 899
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubWatchPresenterProvider:Ljavax/inject/Provider;

    .line 907
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubWatchPresenterProvider:Ljavax/inject/Provider;

    .line 908
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubWatchViewImplMembersInjector:Ldagger/MembersInjector;

    .line 913
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1000(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    move-result-object v0

    .line 912
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubEndpointFactory;->create(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 911
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePeopleHubEndpointProvider:Ljavax/inject/Provider;

    .line 918
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1000(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 917
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidesPeopleHubClientFactory;->create(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 916
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesPeopleHubClientProvider:Ljavax/inject/Provider;

    .line 928
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1000(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePeopleHubEndpointProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesPeopleHubClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 927
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 926
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePeopleHubRetrofitProvider:Ljavax/inject/Provider;

    .line 936
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1000(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePeopleHubRetrofitProvider:Ljavax/inject/Provider;

    .line 935
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule_ProvidePeopleHubServiceFactory;->create(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 934
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePeopleHubServiceProvider:Ljavax/inject/Provider;

    .line 939
    invoke-static {}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->userSummaryFromPeopleHubPersonDataMapperProvider:Ljavax/inject/Provider;

    .line 941
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePeopleHubServiceProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->userSummaryFromPeopleHubPersonDataMapperProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 942
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->friendPickerInteractorProvider:Ljavax/inject/Provider;

    .line 949
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->friendPickerInteractorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 948
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->friendPickerPresenterProvider:Ljavax/inject/Provider;

    .line 953
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->friendPickerPresenterProvider:Ljavax/inject/Provider;

    .line 954
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->friendPickerViewImplMembersInjector:Ldagger/MembersInjector;

    .line 957
    invoke-static {}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 959
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 960
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatMenuScreenMembersInjector:Ldagger/MembersInjector;

    .line 962
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;

    .line 963
    invoke-static {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor_Factory;->create(Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->languageSettingsInteractorProvider:Ljavax/inject/Provider;

    .line 966
    invoke-static {}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->languageSettingsTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 970
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->languageSettingsInteractorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->languageSettingsTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 969
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->languageSettingsPresenterProvider:Ljavax/inject/Provider;

    .line 975
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->languageSettingsPresenterProvider:Ljavax/inject/Provider;

    .line 976
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->languageSettingsViewImplMembersInjector:Ldagger/MembersInjector;

    .line 979
    invoke-static {}, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->homeScreenTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 984
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSharedPreferencesProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->homeScreenTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 983
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHomeScreenRepositoryFactory;->create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 982
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHomeScreenRepositoryProvider:Ljavax/inject/Provider;

    .line 990
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProvideReactInstanceManagerFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 989
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideReactInstanceManagerProvider:Ljavax/inject/Provider;

    .line 992
    invoke-static {}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->rtaDataMapperProvider:Ljavax/inject/Provider;

    .line 994
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->rtaDataMapperProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->rtaRepositoryMembersInjector:Ldagger/MembersInjector;

    .line 999
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$300(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/ServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 998
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideXTokenOkHttpClientFactory;->create(Lcom/microsoft/xbox/data/service/ServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 997
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideXTokenOkHttpClientProvider:Ljavax/inject/Provider;

    .line 1008
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1007
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideXbLogProvider:Ljavax/inject/Provider;

    .line 1010
    invoke-static {}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->telemetryProvider:Ljavax/inject/Provider;

    .line 1012
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->rtaRepositoryMembersInjector:Ldagger/MembersInjector;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideXTokenOkHttpClientProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideXbLogProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->telemetryProvider:Ljavax/inject/Provider;

    .line 1014
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1013
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->rtaRepositoryProvider:Ljavax/inject/Provider;

    .line 1023
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    move-result-object v0

    .line 1022
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionEndpointFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1021
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerSessionEndpointProvider:Ljavax/inject/Provider;

    .line 1026
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xXblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 1031
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xXblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 1030
    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvidesMultiplayerSessionClientFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1029
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesMultiplayerSessionClientProvider:Ljavax/inject/Provider;

    .line 1043
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerSessionEndpointProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesMultiplayerSessionClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 1042
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerSessionRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1041
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerSessionRetrofitProvider:Ljavax/inject/Provider;

    .line 1051
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1100(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerSessionRetrofitProvider:Ljavax/inject/Provider;

    .line 1050
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideMultiplayerServiceFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1049
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerServiceProvider:Ljavax/inject/Provider;

    .line 1056
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1200(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    move-result-object v0

    .line 1055
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyEndpointFactory;->create(Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1054
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePrivacyEndpointProvider:Ljavax/inject/Provider;

    .line 1061
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1200(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xXblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 1060
    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyClientFactory;->create(Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1059
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePrivacyClientProvider:Ljavax/inject/Provider;

    .line 1073
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1200(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePrivacyEndpointProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePrivacyClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 1072
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1071
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePrivacyRetrofitProvider:Ljavax/inject/Provider;

    .line 1081
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1200(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePrivacyRetrofitProvider:Ljavax/inject/Provider;

    .line 1080
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;->create(Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1079
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePrivacyServiceProvider:Ljavax/inject/Provider;

    .line 1086
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1300(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    move-result-object v0

    .line 1085
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule_ProvideGameServerEndpointFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1084
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGameServerEndpointProvider:Ljavax/inject/Provider;

    .line 1091
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1300(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xXblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 1090
    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule_ProvidesGameServerClientFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1089
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesGameServerClientProvider:Ljavax/inject/Provider;

    .line 1103
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1300(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGameServerEndpointProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesGameServerClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 1102
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule_ProvideGameServerRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1101
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGameServerRetrofitProvider:Ljavax/inject/Provider;

    .line 1111
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1300(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGameServerRetrofitProvider:Ljavax/inject/Provider;

    .line 1110
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule_ProvideGameServerServiceFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1109
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGameServerServiceProvider:Ljavax/inject/Provider;

    .line 1113
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGameServerServiceProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideXbLogProvider:Ljavax/inject/Provider;

    .line 1115
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/QoSMeasurementRepository_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1114
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->qoSMeasurementRepositoryProvider:Ljavax/inject/Provider;

    .line 1120
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProvideMyXuidProviderFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1119
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyXuidProvider:Ljavax/inject/Provider;

    .line 1123
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProvideApplicationContextFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideApplicationContextProvider:Ljavax/inject/Provider;

    .line 1125
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideApplicationContextProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideXbLogProvider:Ljavax/inject/Provider;

    .line 1127
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1126
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->peerConnectionClientProvider:Ljavax/inject/Provider;

    .line 1130
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideApplicationContextProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideXbLogProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->peerConnectionClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->telemetryProvider:Ljavax/inject/Provider;

    .line 1131
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyWebRtcRepositoryProvider:Ljavax/inject/Provider;

    .line 1137
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->rtaRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerServiceProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePrivacyServiceProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->qoSMeasurementRepositoryProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyXuidProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideXbLogProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyWebRtcRepositoryProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->telemetryProvider:Ljavax/inject/Provider;

    .line 1139
    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1138
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 1152
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$800(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/domain/ModelManagerModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 1151
    invoke-static {v0, v1}, Lcom/microsoft/xbox/domain/ModelManagerModule_ProvideAuthManagerFactory;->create(Lcom/microsoft/xbox/domain/ModelManagerModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1150
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1157
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSharedPreferencesProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1156
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideTutorialRepositoryFactory;->create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1155
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideTutorialRepositoryProvider:Ljavax/inject/Provider;

    .line 1164
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProvideNotificationDisplayFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1163
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideNotificationDisplayProvider:Ljavax/inject/Provider;

    .line 1166
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyXuidProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideNavigationManagerProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideNotificationDisplayProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 1168
    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1167
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyEventNotifierProvider:Ljavax/inject/Provider;

    .line 1175
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHomeScreenRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideReactInstanceManagerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideTutorialRepositoryProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyEventNotifierProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1176
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/MainActivity_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->mainActivityMembersInjector:Ldagger/MembersInjector;

    .line 1184
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyXuidProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideClubModelManagerProvider:Ljavax/inject/Provider;

    .line 1185
    invoke-static {v0, v1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myClubsInteractorProvider:Ljavax/inject/Provider;

    .line 1190
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSharedPreferencesProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1189
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideHoverChatHeadRepositoryFactory;->create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1188
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    .line 1195
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideApplicationContextProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1196
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myClubsNavigatorProvider:Ljavax/inject/Provider;

    .line 1203
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myClubsInteractorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myClubsNavigatorProvider:Ljavax/inject/Provider;

    .line 1202
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myClubsPresenterProvider:Ljavax/inject/Provider;

    .line 1208
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myClubsPresenterProvider:Ljavax/inject/Provider;

    .line 1209
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myClubsViewImplMembersInjector:Ldagger/MembersInjector;

    .line 1214
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSharedPreferencesProvider:Ljavax/inject/Provider;

    .line 1216
    invoke-static {}, Lcom/microsoft/xbox/domain/oobe/OOBESettingsDataMapper_Factory;->create()Ldagger/internal/Factory;

    move-result-object v2

    .line 1213
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideOOBESettingsRepositoryFactory;->create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1212
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideOOBESettingsRepositoryProvider:Ljavax/inject/Provider;

    .line 1220
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1400(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule_ProvideOOBESessionEndpointFactory;->create(Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1219
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideOOBESessionEndpointProvider:Ljavax/inject/Provider;

    .line 1225
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1400(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xXblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 1224
    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule_ProvidesOOBESessionClientFactory;->create(Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1223
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesOOBESessionClientProvider:Ljavax/inject/Provider;

    .line 1237
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1400(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideOOBESessionEndpointProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesOOBESessionClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 1236
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule_ProvideOOBESessionRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1235
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideOOBESessionRetrofitProvider:Ljavax/inject/Provider;

    .line 1245
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1400(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideOOBESessionRetrofitProvider:Ljavax/inject/Provider;

    .line 1244
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule_ProvideOOBESessionServiceFactory;->create(Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1243
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideOOBESessionServiceProvider:Ljavax/inject/Provider;

    .line 1247
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideOOBESettingsRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideOOBESessionServiceProvider:Ljavax/inject/Provider;

    .line 1248
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->oOBEInteractorProvider:Ljavax/inject/Provider;

    .line 1253
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideOOBESettingsRepositoryProvider:Ljavax/inject/Provider;

    .line 1254
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/oobe/OOBETelemetryService_Factory;->create(Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->oOBETelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1258
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProvideMyProfileProviderFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1257
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyProfileProvider:Ljavax/inject/Provider;

    .line 1262
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->oOBEInteractorProvider:Ljavax/inject/Provider;

    .line 1264
    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBENavigator_Factory;->create()Ldagger/internal/Factory;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->oOBETelemetryServiceProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyProfileProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1261
    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->oOBEPresenterProvider:Ljavax/inject/Provider;

    .line 1270
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->oOBEPresenterProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->oOBEViewImplMembersInjector:Ldagger/MembersInjector;

    .line 1275
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providePeopleHubServiceProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->userSummaryFromPeopleHubPersonDataMapperProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 1274
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideUserSummaryRepositoryFactory;->create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1273
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideUserSummaryRepositoryProvider:Ljavax/inject/Provider;

    .line 1280
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->telemetryProvider:Ljavax/inject/Provider;

    .line 1281
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;->create(Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->multiplayerTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1283
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideUserSummaryRepositoryProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->multiplayerTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1284
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/domain/party/PartyInteractor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyInteractorProvider:Ljavax/inject/Provider;

    .line 1289
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->multiplayerTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1290
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator_Factory;->create(Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyDetailsNavigatorProvider:Ljavax/inject/Provider;

    .line 1294
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyXuidProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyInteractorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyDetailsNavigatorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 1293
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyDetailsPresenterProvider:Ljavax/inject/Provider;

    .line 1300
    return-void
.end method

.method private initialize2(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)V
    .locals 8
    .param p1, "builder"    # Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    .prologue
    .line 1305
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyDetailsPresenterProvider:Ljavax/inject/Provider;

    .line 1306
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyDetailsViewImplMembersInjector:Ldagger/MembersInjector;

    .line 1310
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyInteractorProvider:Ljavax/inject/Provider;

    .line 1309
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/presentation/party/PartyTextPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyTextPresenterProvider:Ljavax/inject/Provider;

    .line 1314
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyTextPresenterProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyXuidProvider:Ljavax/inject/Provider;

    .line 1315
    invoke-static {v0, v1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyTextViewImplMembersInjector:Ldagger/MembersInjector;

    .line 1317
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideReactInstanceManagerProvider:Ljavax/inject/Provider;

    .line 1318
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/base/ReactActivityBase_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->reactActivityBaseMembersInjector:Ldagger/MembersInjector;

    .line 1320
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideReactInstanceManagerProvider:Ljavax/inject/Provider;

    .line 1321
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->reactModalActivityBaseMembersInjector:Ldagger/MembersInjector;

    .line 1324
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProvideTitleBarViewFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideTitleBarViewProvider:Ljavax/inject/Provider;

    .line 1329
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideBeamServiceProvider:Ljavax/inject/Provider;

    .line 1331
    invoke-static {}, Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper_Factory;->create()Ldagger/internal/Factory;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 1328
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/repository/beam/BeamRepository_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1327
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->beamRepositoryProvider:Ljavax/inject/Provider;

    .line 1334
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->beamRepositoryProvider:Ljavax/inject/Provider;

    .line 1335
    invoke-static {v0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor_Factory;->create(Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->trendingBeamInteractorProvider:Ljavax/inject/Provider;

    .line 1339
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->trendingBeamInteractorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->beamNavigatorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    .line 1338
    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->trendingBeamPresenterProvider:Ljavax/inject/Provider;

    .line 1345
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideTitleBarViewProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->trendingBeamPresenterProvider:Ljavax/inject/Provider;

    .line 1346
    invoke-static {v0, v1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->trendingBeamViewImplMembersInjector:Ldagger/MembersInjector;

    .line 1350
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$900(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/DataModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/data/DataModule_ProvideFacebookManagerFactory;->create(Lcom/microsoft/xbox/data/DataModule;)Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideFacebookManagerProvider:Ljavax/inject/Provider;

    .line 1355
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/repository/RepositoryModule;

    move-result-object v0

    .line 1356
    invoke-static {}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionDataMapper_Factory;->create()Ldagger/internal/Factory;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSharedPreferencesProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1354
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideWelcomeCardCompletionRepositoryFactory;->create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1353
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideWelcomeCardCompletionRepositoryProvider:Ljavax/inject/Provider;

    .line 1360
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideFacebookManagerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideWelcomeCardCompletionRepositoryProvider:Ljavax/inject/Provider;

    .line 1361
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialInteractorProvider:Ljavax/inject/Provider;

    .line 1367
    invoke-static {}, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1369
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideNavigationManagerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideFacebookManagerProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideTutorialRepositoryProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1370
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialNavigatorProvider:Ljavax/inject/Provider;

    .line 1378
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialInteractorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialNavigatorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideWelcomeCardCompletionRepositoryProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideFacebookManagerProvider:Ljavax/inject/Provider;

    .line 1377
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialPresenterProvider:Ljavax/inject/Provider;

    .line 1385
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialPresenterProvider:Ljavax/inject/Provider;

    .line 1386
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialViewImplMembersInjector:Ldagger/MembersInjector;

    .line 1388
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideActivityFeedFilterRepositoryProvider:Ljavax/inject/Provider;

    .line 1391
    invoke-static {}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService_Factory;->create()Ldagger/internal/Factory;

    move-result-object v1

    .line 1389
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedScreenViewModelBaseMembersInjector:Ldagger/MembersInjector;

    .line 1394
    invoke-static {}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper_Factory;->create()Ldagger/internal/Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->userSummaryFromUserSearchResultDataMapperProvider:Ljavax/inject/Provider;

    .line 1396
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideUserSummaryRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->userSummaryFromUserSearchResultDataMapperProvider:Ljavax/inject/Provider;

    .line 1397
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubAdminMembersScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1401
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideUserSummaryRepositoryProvider:Ljavax/inject/Provider;

    .line 1402
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubHomeScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1404
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1405
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubChatScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1408
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    .line 1409
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubChatNotificationScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1415
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    move-result-object v0

    .line 1414
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule_ProvideMediaHubEndpointFactory;->create(Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1413
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubEndpointProvider2:Ljavax/inject/Provider;

    .line 1420
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubEndpointProvider2:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideDefaultOkHttpClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 1419
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule_ProvideProfileColorRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1418
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideProfileColorRetrofitProvider:Ljavax/inject/Provider;

    .line 1428
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideProfileColorRetrofitProvider:Ljavax/inject/Provider;

    .line 1427
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule_ProvideProfileColorServiceFactory;->create(Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1426
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideProfileColorServiceProvider:Ljavax/inject/Provider;

    .line 1430
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideProfileColorServiceProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 1432
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1431
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->profileColorsRepositoryProvider:Ljavax/inject/Provider;

    .line 1435
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->profileColorsRepositoryProvider:Ljavax/inject/Provider;

    .line 1436
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubCustomizeViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1441
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    move-result-object v0

    .line 1440
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvideMultiplayerSessionEndpointFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1439
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerSessionEndpointProvider2:Ljavax/inject/Provider;

    .line 1446
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xXblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 1445
    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1444
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesMultiplayerSessionClientProvider2:Ljavax/inject/Provider;

    .line 1458
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerSessionEndpointProvider2:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesMultiplayerSessionClientProvider2:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 1457
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvideMultiplayerSessionRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1456
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerSessionRetrofitProvider2:Ljavax/inject/Provider;

    .line 1466
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1600(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerSessionRetrofitProvider2:Ljavax/inject/Provider;

    .line 1465
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvideMultiplayerServiceFactory;->create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1464
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerServiceProvider2:Ljavax/inject/Provider;

    .line 1468
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerServiceProvider2:Ljavax/inject/Provider;

    .line 1469
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubPlayScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1471
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1472
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->conversationsActivityViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1475
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1476
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->conversationDetailsActivityViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1479
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->profileColorsRepositoryProvider:Ljavax/inject/Provider;

    .line 1480
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->customizeProfileScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1482
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerServiceProvider2:Ljavax/inject/Provider;

    .line 1483
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->lfgDetailsViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1485
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerServiceProvider2:Ljavax/inject/Provider;

    .line 1486
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->lfgVettingScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1488
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->multiplayerTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1489
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyAndLfgScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1494
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->multiplayerTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1495
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->peopleHubInfoScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1500
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHomeScreenRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1501
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->settingsGeneralPageViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1506
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    .line 1507
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->settingsNotificationsPageViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1510
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubServiceProvider:Ljavax/inject/Provider;

    .line 1511
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->uploadCustomPicScreenViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1513
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    .line 1514
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->viewModelWithConversationMembersInjector:Ldagger/MembersInjector;

    .line 1516
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1517
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xboxAuthActivityViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1519
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1520
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xsapiSilentSignInViewModelMembersInjector:Ldagger/MembersInjector;

    .line 1522
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    .line 1523
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->consoleConnectionScreenAdapterMembersInjector:Ldagger/MembersInjector;

    .line 1525
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideTutorialRepositoryProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHomeScreenRepositoryProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    .line 1526
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->drawerAdapterMembersInjector:Ldagger/MembersInjector;

    .line 1532
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyChatRepositoryProvider:Ljavax/inject/Provider;

    .line 1533
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyMemberIconListAdapterMembersInjector:Ldagger/MembersInjector;

    .line 1535
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyProfileProvider:Ljavax/inject/Provider;

    .line 1536
    invoke-static {v0}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyTextListAdapterMembersInjector:Ldagger/MembersInjector;

    .line 1538
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;

    .line 1539
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->peopleScreenAdapterMembersInjector:Ldagger/MembersInjector;

    .line 1541
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideWelcomeCardCompletionRepositoryProvider:Ljavax/inject/Provider;

    .line 1542
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->settingsGeneralPageAdapterMembersInjector:Ldagger/MembersInjector;

    .line 1545
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1546
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->messageModelMembersInjector:Ldagger/MembersInjector;

    .line 1552
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1700(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    move-result-object v0

    .line 1551
    invoke-static {v0}, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialEndpointFactory;->create(Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1550
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideEditorialEndpointProvider:Ljavax/inject/Provider;

    .line 1557
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1700(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHttpLoggingInterceptorProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 1556
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvidesEditorialClientFactory;->create(Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1555
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesEditorialClientProvider:Ljavax/inject/Provider;

    .line 1567
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1700(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideEditorialEndpointProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->providesEditorialClientProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideGsonConvertorFactoryProvider:Ljavax/inject/Provider;

    .line 1566
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialRetrofitFactory;->create(Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1565
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideEditorialRetrofitProvider:Ljavax/inject/Provider;

    .line 1575
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1700(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideEditorialRetrofitProvider:Ljavax/inject/Provider;

    .line 1574
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule_ProvideEditorialServiceFactory;->create(Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1573
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideEditorialServiceProvider:Ljavax/inject/Provider;

    .line 1577
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMultiplayerServiceProvider2:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideEditorialServiceProvider:Ljavax/inject/Provider;

    .line 1578
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->multiplayerSessionModelManagerMembersInjector:Ldagger/MembersInjector;

    .line 1581
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideEditorialServiceProvider:Ljavax/inject/Provider;

    .line 1582
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/PagesModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->pagesModelMembersInjector:Ldagger/MembersInjector;

    .line 1584
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->profileColorsRepositoryProvider:Ljavax/inject/Provider;

    .line 1585
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->profileModelMembersInjector:Ldagger/MembersInjector;

    .line 1587
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideEditorialServiceProvider:Ljavax/inject/Provider;

    .line 1588
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/SocialTagModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->socialTagModelMembersInjector:Ldagger/MembersInjector;

    .line 1590
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSharedPreferencesProvider:Ljavax/inject/Provider;

    .line 1591
    invoke-static {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->systemSettingsModelMembersInjector:Ldagger/MembersInjector;

    .line 1593
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHomeScreenRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideTutorialRepositoryProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1594
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->navigationManagerMembersInjector:Ldagger/MembersInjector;

    .line 1599
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;

    .line 1600
    invoke-static {v0}, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->storeServiceCacheManagerMembersInjector:Ldagger/MembersInjector;

    .line 1602
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;

    .line 1603
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->vortexServiceManagerMembersInjector:Ldagger/MembersInjector;

    .line 1608
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1800(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideApplicationContextProvider:Ljavax/inject/Provider;

    .line 1607
    invoke-static {v0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;->create(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1606
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideWindowManagerContainerProvider:Ljavax/inject/Provider;

    .line 1613
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1800(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideApplicationContextProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideWindowManagerContainerProvider:Ljavax/inject/Provider;

    .line 1612
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->create(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1611
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideChatHeadManagerProvider:Ljavax/inject/Provider;

    .line 1619
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1900(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/toolkit/ToolkitModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ToolkitModule_ProvideCircularDrawableFactoryFactory;->create(Lcom/microsoft/xbox/toolkit/ToolkitModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1618
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideCircularDrawableFactoryProvider:Ljavax/inject/Provider;

    .line 1624
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$1800(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideApplicationContextProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideCircularDrawableFactoryProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    .line 1623
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadViewAdapterFactory;->create(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1622
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideChatHeadViewAdapterProvider:Ljavax/inject/Provider;

    .line 1629
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideUserSummaryRepositoryProvider:Ljavax/inject/Provider;

    .line 1630
    invoke-static {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload_HoverChatHeadImageDownloaderFactory_Factory;->create(Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatHeadImageDownloaderFactoryProvider:Ljavax/inject/Provider;

    .line 1633
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideUserSummaryRepositoryProvider:Ljavax/inject/Provider;

    .line 1635
    invoke-static {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload_HoverChatHeadContentDescriptionDownloaderFactory_Factory;->create(Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatHeadContentDescriptionDownloaderFactoryProvider:Ljavax/inject/Provider;

    .line 1639
    invoke-static {p1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->access$500(Lcom/microsoft/xbox/DaggerXLEComponent$Builder;)Lcom/microsoft/xbox/XLEAppModule;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/XLEAppModule_ProjectSpecificDialogManagerFactory;->create(Lcom/microsoft/xbox/XLEAppModule;)Ldagger/internal/Factory;

    move-result-object v0

    .line 1638
    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->projectSpecificDialogManagerProvider:Ljavax/inject/Provider;

    .line 1641
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideChatHeadManagerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideChatHeadViewAdapterProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatHeadImageDownloaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatHeadContentDescriptionDownloaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideNavigationManagerProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->projectSpecificDialogManagerProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    .line 1642
    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatManagerProvider:Ljavax/inject/Provider;

    .line 1652
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatManagerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSchedulerProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideAuthManagerProvider:Ljavax/inject/Provider;

    .line 1653
    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatServiceMembersInjector:Ldagger/MembersInjector;

    .line 1659
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideSystemSettingsModelProvider:Ljavax/inject/Provider;

    .line 1660
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->gcmBroadcastReceiverMembersInjector:Ldagger/MembersInjector;

    .line 1663
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideHoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 1664
    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver_MembersInjector;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatBroadcastReceiverMembersInjector:Ldagger/MembersInjector;

    .line 1667
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideLanguageSettingsRespositoryProvider:Ljavax/inject/Provider;

    .line 1668
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeProviderRnModuleMembersInjector:Ldagger/MembersInjector;

    .line 1670
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMyXuidProvider:Ljavax/inject/Provider;

    .line 1671
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule_MembersInjector;->create(Ljavax/inject/Provider;)Ldagger/MembersInjector;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myXuidProviderRnModuleMembersInjector:Ldagger/MembersInjector;

    .line 1672
    return-void
.end method


# virtual methods
.method public inject(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;)V
    .locals 1
    .param p1, "broadcastReceiver"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatBroadcastReceiver;

    .prologue
    .line 1961
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatBroadcastReceiverMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1962
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;)V
    .locals 1
    .param p1, "service"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;

    .prologue
    .line 1951
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatServiceMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1952
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterDialog;

    .prologue
    .line 1691
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedFilterDialogMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1692
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl;

    .prologue
    .line 1696
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedFilterViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1697
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/base/ReactActivityBase;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/base/ReactActivityBase;

    .prologue
    .line 1746
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->reactActivityBaseMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1747
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/base/ReactModalActivityBase;

    .prologue
    .line 1751
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->reactModalActivityBaseMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1752
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;

    .prologue
    .line 1756
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->trendingBeamViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1757
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;

    .prologue
    .line 1701
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubWatchViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1702
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;

    .prologue
    .line 1726
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myClubsViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1727
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;

    .prologue
    .line 1706
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->friendPickerViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1707
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;

    .prologue
    .line 1711
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->hoverChatMenuScreenMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1712
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

    .prologue
    .line 1731
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->oOBEViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1732
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    .prologue
    .line 1881
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1882
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;

    .prologue
    .line 1736
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyDetailsViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1737
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    .prologue
    .line 1886
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyMemberIconListAdapterMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1887
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    .prologue
    .line 1891
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyTextListAdapterMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1892
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;

    .prologue
    .line 1741
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyTextViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1742
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewImpl;

    .prologue
    .line 1716
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->languageSettingsViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1717
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;

    .prologue
    .line 1761
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->tutorialViewImplMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1762
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/service/model/MessageModel;)V
    .locals 1
    .param p1, "model"    # Lcom/microsoft/xbox/service/model/MessageModel;

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->messageModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1907
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;)V
    .locals 1
    .param p1, "model"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    .prologue
    .line 1911
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->multiplayerSessionModelManagerMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1912
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/service/model/PagesModel;)V
    .locals 1
    .param p1, "model"    # Lcom/microsoft/xbox/service/model/PagesModel;

    .prologue
    .line 1916
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->pagesModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1917
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/service/model/ProfileModel;)V
    .locals 1
    .param p1, "model"    # Lcom/microsoft/xbox/service/model/ProfileModel;

    .prologue
    .line 1921
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->profileModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1922
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/service/model/SocialTagModel;)V
    .locals 1
    .param p1, "model"    # Lcom/microsoft/xbox/service/model/SocialTagModel;

    .prologue
    .line 1926
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->socialTagModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1927
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;)V
    .locals 1
    .param p1, "broadcastReceiver"    # Lcom/microsoft/xbox/service/model/gcm/GcmBroadcastReceiver;

    .prologue
    .line 1956
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->gcmBroadcastReceiverMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1957
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;)V
    .locals 1
    .param p1, "serviceManager"    # Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    .prologue
    .line 1946
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->vortexServiceManagerMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1947
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;)V
    .locals 1
    .param p1, "cacheManager"    # Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;

    .prologue
    .line 1941
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->storeServiceCacheManagerMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1942
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;)V
    .locals 1
    .param p1, "localeProviderRnModule"    # Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;

    .prologue
    .line 1966
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->localeProviderRnModuleMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1967
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule;)V
    .locals 1
    .param p1, "myXuidProviderRnModule"    # Lcom/microsoft/xbox/toolkit/rn/MyXuidProviderRnModule;

    .prologue
    .line 1971
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->myXuidProviderRnModuleMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1972
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 1
    .param p1, "navigationManager"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    .line 1936
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->navigationManagerMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1937
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/MainActivity;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/xle/app/MainActivity;

    .prologue
    .line 1721
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->mainActivityMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1722
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;)V
    .locals 1
    .param p1, "provider"    # Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    .prologue
    .line 1686
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xleProjectSpecificDataProviderMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1687
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;

    .prologue
    .line 1871
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->consoleConnectionScreenAdapterMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1872
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;

    .prologue
    .line 1876
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->drawerAdapterMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1877
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    .prologue
    .line 1896
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->peopleScreenAdapterMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1897
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .prologue
    .line 1781
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubChatScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1782
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 1
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 1776
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubHomeScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1777
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    .prologue
    .line 1796
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubPlayScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1797
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V
    .locals 1
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;

    .prologue
    .line 1771
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubAdminMembersScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1772
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V
    .locals 1
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;

    .prologue
    .line 1791
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubCustomizeViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1792
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    .prologue
    .line 1821
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->lfgDetailsViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1822
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    .prologue
    .line 1826
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->lfgVettingScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1827
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->peopleHubInfoScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1837
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;

    .prologue
    .line 1901
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->settingsGeneralPageAdapterMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1902
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .prologue
    .line 1841
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->settingsGeneralPageViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1842
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    .prologue
    .line 1846
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->settingsNotificationsPageViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1847
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)V
    .locals 1
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;

    .prologue
    .line 1851
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->uploadCustomPicScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1852
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/model/SystemSettingsModel;)V
    .locals 1
    .param p1, "model"    # Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    .prologue
    .line 1931
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->systemSettingsModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1932
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V
    .locals 1
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .prologue
    .line 1766
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->activityFeedScreenViewModelBaseMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1767
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    .prologue
    .line 1786
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->clubChatNotificationScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1787
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .prologue
    .line 1806
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->conversationDetailsActivityViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1807
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V
    .locals 1
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    .prologue
    .line 1801
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->conversationsActivityViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1802
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 1
    .param p1, "viewModelBase"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 1811
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->customizeProfileScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1812
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    .prologue
    .line 1816
    invoke-static {}, Ldagger/internal/MembersInjectors;->noOp()Ldagger/MembersInjector;

    move-result-object v0

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1817
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    .prologue
    .line 1831
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->partyAndLfgScreenViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1832
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    .prologue
    .line 1856
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->viewModelWithConversationMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1857
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;)V
    .locals 1
    .param p1, "xleGlobalData"    # Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    .prologue
    .line 1681
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xLEGlobalDataMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1682
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;

    .prologue
    .line 1861
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xboxAuthActivityViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1862
    return-void
.end method

.method public inject(Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/XsapiSilentSignInViewModel;

    .prologue
    .line 1866
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->xsapiSilentSignInViewModelMembersInjector:Ldagger/MembersInjector;

    invoke-interface {v0, p1}, Ldagger/MembersInjector;->injectMembers(Ljava/lang/Object;)V

    .line 1867
    return-void
.end method

.method public mediaHubService()Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    .locals 1

    .prologue
    .line 1676
    iget-object v0, p0, Lcom/microsoft/xbox/DaggerXLEComponent;->provideMediaHubServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    return-object v0
.end method
