.class Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;
.super Lcom/microsoft/xbox/smartglass/SessionManagerListener;
.source "XBoxOneCompanionSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "XBoxOneSessionManagerListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->isDisplayableLocation(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Z

    move-result v0

    return v0
.end method

.method private isDisplayableLocation(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Z
    .locals 1
    .param p1, "atl"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 360
    sget-object v0, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Full:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Fill:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Snapped:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onChannelEstablished(Lcom/microsoft/xbox/smartglass/MessageTarget;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 8
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 121
    const-string v2, "XBoxOneCompanionSession"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "onChannelEstablished with channel=%s title id=%d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->channelEstablishedListenerList:Ljava/util/LinkedList;

    monitor-enter v3

    .line 123
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->channelEstablishedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;

    .line 124
    .local v1, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;
    move-object v0, v1

    .line 125
    .local v0, "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$1;

    invoke-direct {v4, p0, v0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$1;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;Lcom/microsoft/xbox/smartglass/MessageTarget;Lcom/microsoft/xbox/smartglass/SGResult;)V

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 132
    .end local v0    # "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;
    .end local v1    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/ConnectionState;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 112
    const-string v0, "XBoxOneCompanionSession"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConnectionStateChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/ConnectionState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Success="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/microsoft/xbox/smartglass/SGResult;->isSuccess()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v1, "onConnectionStateChanged received an unexpected state and result"

    invoke-virtual {p2}, Lcom/microsoft/xbox/smartglass/SGResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/smartglass/ConnectionState;->Error:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p2}, Lcom/microsoft/xbox/smartglass/SGResult;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/microsoft/xbox/smartglass/ConnectionState;->Error:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->notifySessionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V

    .line 117
    return-void

    .line 113
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMediaStateChanged(Lcom/microsoft/xbox/smartglass/MediaState;)V
    .locals 18
    .param p1, "mediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    .line 263
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v11, v10, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->mediaTitleStateListenerList:Ljava/util/LinkedList;

    monitor-enter v11

    .line 264
    :try_start_0
    const-string v10, "mtc"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "onMediaStateChanged with title id %s asset id %s: %s %d %d %d %f"

    const/4 v14, 0x7

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    const/4 v15, 0x2

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->name()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x3

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->position:J

    move-wide/from16 v16, v0

    .line 265
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x4

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->mediaStart:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x5

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->mediaEnd:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x6

    move-object/from16 v0, p1

    iget v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackRate:F

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v16

    aput-object v16, v14, v15

    .line 264
    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 282
    .local v3, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/smartglass/SessionManager;->getActiveTitles()Ljava/util/List;

    move-result-object v2

    .line 283
    .local v2, "activeTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/ActiveTitleState;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 284
    .local v4, "ats":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    iget v12, v4, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    move-object/from16 v0, p1

    iget v13, v0, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    if-ne v12, v13, :cond_0

    .line 285
    iget-object v3, v4, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 290
    .end local v4    # "ats":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->isDisplayableLocation(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 297
    const/4 v5, 0x0

    .line 298
    .local v5, "filtered":Z
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    invoke-static {v10}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->access$100(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;)Ljava/util/Hashtable;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/smartglass/SGMediaState;

    .line 299
    .local v9, "old":Lcom/microsoft/xbox/smartglass/SGMediaState;
    if-nez v9, :cond_2

    .line 300
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    invoke-static {v10}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->access$100(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;)Ljava/util/Hashtable;

    move-result-object v10

    new-instance v12, Lcom/microsoft/xbox/smartglass/SGMediaState;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lcom/microsoft/xbox/smartglass/SGMediaState;-><init>(Lcom/microsoft/xbox/smartglass/MediaState;)V

    invoke-virtual {v10, v3, v12}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    :goto_0
    if-nez v5, :cond_5

    .line 313
    move-object v7, v3

    .line 314
    .local v7, "finalLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v10, v10, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->mediaTitleStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;

    .line 315
    .local v8, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;
    move-object v6, v8

    .line 316
    .local v6, "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;
    new-instance v12, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v12, v0, v1, v6, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$4;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/smartglass/MediaState;Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V

    invoke-static {v12}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 339
    .end local v2    # "activeTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/ActiveTitleState;>;"
    .end local v3    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .end local v5    # "filtered":Z
    .end local v6    # "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;
    .end local v7    # "finalLocation":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .end local v8    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;
    .end local v9    # "old":Lcom/microsoft/xbox/smartglass/SGMediaState;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 303
    .restart local v2    # "activeTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/ActiveTitleState;>;"
    .restart local v3    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .restart local v5    # "filtered":Z
    .restart local v9    # "old":Lcom/microsoft/xbox/smartglass/SGMediaState;
    :cond_2
    :try_start_1
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    sget-object v12, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Playing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    if-ne v10, v12, :cond_3

    .line 304
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    iget-wide v14, v9, Lcom/microsoft/xbox/smartglass/SGMediaState;->timeStamp:J

    sub-long/2addr v12, v14

    const-wide/16 v14, 0x7d0

    cmp-long v10, v12, v14

    if-gez v10, :cond_3

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/microsoft/xbox/smartglass/SGMediaState;->areEqualExceptPosition(Lcom/microsoft/xbox/smartglass/MediaState;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 305
    const-string v10, "mtc"

    const-string v12, "ignoring mediaState because either we got the last one not long ago or this one isn\'t much different from the last one"

    invoke-static {v10, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const/4 v5, 0x1

    goto :goto_0

    .line 308
    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    invoke-static {v10}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->access$100(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;)Ljava/util/Hashtable;

    move-result-object v10

    new-instance v12, Lcom/microsoft/xbox/smartglass/SGMediaState;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lcom/microsoft/xbox/smartglass/SGMediaState;-><init>(Lcom/microsoft/xbox/smartglass/MediaState;)V

    invoke-virtual {v10, v3, v12}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 337
    .end local v5    # "filtered":Z
    .end local v9    # "old":Lcom/microsoft/xbox/smartglass/SGMediaState;
    :cond_4
    const-string v10, "mtc"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "mediaState title id %d is in bad location: %s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->name()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :cond_5
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 340
    return-void
.end method

.method public onMessageReceived(Lcom/microsoft/xbox/smartglass/Message;)V
    .locals 7
    .param p1, "message"    # Lcom/microsoft/xbox/smartglass/Message;

    .prologue
    .line 222
    const-string v3, "XBoxOneCompanionSession"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onMessageReceived with "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/Message;->type:Lcom/microsoft/xbox/smartglass/MessageType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/smartglass/MessageType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/Message;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v5, v5, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->datalock:Ljava/lang/Object;

    monitor-enter v4

    .line 227
    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$1;->$SwitchMap$com$microsoft$xbox$smartglass$MessageType:[I

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/Message;->type:Lcom/microsoft/xbox/smartglass/MessageType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/smartglass/MessageType;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_0

    .line 255
    const-string v3, "XBoxOneCompanionSession"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onMessageReceived: unhanded message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/microsoft/xbox/smartglass/Message;->type:Lcom/microsoft/xbox/smartglass/MessageType;

    invoke-virtual {v6}, Lcom/microsoft/xbox/smartglass/MessageType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :goto_0
    monitor-exit v4

    .line 259
    return-void

    .line 229
    :pswitch_0
    const-string v5, "XBoxOneCompanionSession"

    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/smartglass/JsonMessage;

    move-object v3, v0

    iget-object v3, v3, Lcom/microsoft/xbox/smartglass/JsonMessage;->text:Ljava/lang/String;

    invoke-static {v5, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->titleMessageListenerList:Ljava/util/LinkedList;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 231
    :try_start_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v3, v3, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->titleMessageListenerList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;

    .line 232
    .local v2, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;
    move-object v1, v2

    .line 233
    .local v1, "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;
    new-instance v6, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$3;

    invoke-direct {v6, p0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$3;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;Lcom/microsoft/xbox/smartglass/Message;)V

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 251
    .end local v1    # "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;
    .end local v2    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;
    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3

    .line 258
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    .line 251
    :cond_0
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 227
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPrimaryDeviceStateChanged(Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;)V
    .locals 5
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;

    .prologue
    .line 344
    const-string v2, "XBoxOneCompanionSession"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "primary device state changed. locale="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->locale:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;

    monitor-enter v3

    .line 346
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;

    .line 347
    .local v1, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;
    move-object v0, v1

    .line 348
    .local v0, "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$5;

    invoke-direct {v4, p0, v0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$5;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;)V

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 355
    .end local v0    # "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;
    .end local v1    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 356
    return-void
.end method

.method public onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 2
    .param p1, "activeTitleState"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    .line 137
    iget v0, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    if-gtz v0, :cond_0

    .line 138
    const-string v0, "XBoxOneCompanionSession"

    const-string v1, "received zero or -1 title id onTitleChanged ignore"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :goto_0
    return-void

    .line 142
    :cond_0
    iget v0, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    const v1, 0x6a53fd52

    if-ne v0, v1, :cond_1

    .line 143
    const-string v0, "XBoxOneCompanionSession"

    const-string v1, "ignoring \"Snap an app\" onTitleChanged"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Default:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v0, v1, :cond_2

    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->SystemUI:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v0, v1, :cond_3

    .line 148
    :cond_2
    const-string v0, "XBoxOneCompanionSession"

    const-string v1, "received Default or SystemUI title location onTitleChanged ignore"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 153
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener$2;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 217
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
