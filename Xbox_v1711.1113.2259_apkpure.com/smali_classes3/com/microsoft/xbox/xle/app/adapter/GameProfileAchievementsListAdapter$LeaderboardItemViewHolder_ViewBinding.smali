.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "GameProfileAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;

    .line 24
    const v0, 0x7f0e0683

    const-string v1, "field \'heroStatTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatTextView:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f0e0684

    const-string v1, "field \'heroStatDetailsTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatDetailsTextView:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f0e0688

    const-string v1, "field \'statValueTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f0e0689

    const-string v1, "field \'statRibbonView\'"

    const-class v2, Landroid/widget/RelativeLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statRibbonView:Landroid/widget/RelativeLayout;

    .line 28
    const v0, 0x7f0e068a

    const-string v1, "field \'statRibbonBackground\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statRibbonBackground:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    .line 29
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;

    .line 35
    .local v0, "target":Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;

    .line 38
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatTextView:Landroid/widget/TextView;

    .line 39
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->heroStatDetailsTextView:Landroid/widget/TextView;

    .line 40
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statValueTextView:Landroid/widget/TextView;

    .line 41
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statRibbonView:Landroid/widget/RelativeLayout;

    .line 42
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;->statRibbonBackground:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    .line 43
    return-void
.end method
