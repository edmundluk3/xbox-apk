.class public Lcom/microsoft/xbox/smartglass/ActiveTitleState;
.super Ljava/lang/Object;
.source "ActiveTitleState.java"


# instance fields
.field public final aumId:Ljava/lang/String;

.field public final hasFocus:Z

.field public final productId:Ljava/lang/String;

.field public final titleId:I

.field public final titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;


# direct methods
.method constructor <init>(IIZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "titleId"    # I
    .param p2, "titleLocation"    # I
    .param p3, "hasFocus"    # Z
    .param p4, "productId"    # Ljava/lang/String;
    .param p5, "aumId"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    .line 22
    invoke-static {p2}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->fromInt(I)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 23
    iput-boolean p3, p0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->hasFocus:Z

    .line 24
    iput-object p4, p0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->productId:Ljava/lang/String;

    .line 25
    iput-object p5, p0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->aumId:Ljava/lang/String;

    .line 26
    return-void
.end method
