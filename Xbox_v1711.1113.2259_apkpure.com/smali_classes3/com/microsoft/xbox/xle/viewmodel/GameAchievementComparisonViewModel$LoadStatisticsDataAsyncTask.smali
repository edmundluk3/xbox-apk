.class Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameAchievementComparisonViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadStatisticsDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V
    .locals 0

    .prologue
    .line 585
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;

    .prologue
    .line 585
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V

    return-void
.end method

.method private addMeStatisticsItemToList(Ljava/util/Hashtable;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 2
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;",
            ">;",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;",
            ")V"
        }
    .end annotation

    .prologue
    .line 712
    .local p1, "statList":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;>;"
    if-nez p2, :cond_1

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 716
    :cond_1
    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;

    move-object v0, v1

    .line 718
    .local v0, "statItem":Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;
    :goto_1
    iput-object p2, v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;->meSatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 720
    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 721
    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 716
    .end local v0    # "statItem":Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;-><init>()V

    goto :goto_1
.end method

.method private addYouStatisticsItemToList(Ljava/util/Hashtable;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 2
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;",
            ">;",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;",
            ")V"
        }
    .end annotation

    .prologue
    .line 726
    .local p1, "statList":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;>;"
    if-nez p2, :cond_1

    .line 737
    :cond_0
    :goto_0
    return-void

    .line 730
    :cond_1
    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;

    move-object v0, v1

    .line 732
    .local v0, "statItem":Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;
    :goto_1
    iput-object p2, v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;->youSatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 734
    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 735
    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 730
    .end local v0    # "statItem":Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;-><init>()V

    goto :goto_1
.end method

.method private getProgressPercentage(Ljava/lang/String;)I
    .locals 8
    .param p1, "percentage"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x64

    .line 740
    const-wide/16 v4, 0x0

    invoke-static {p1, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseDouble(Ljava/lang/String;D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 742
    .local v1, "percent":Ljava/lang/Double;
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v4, v6

    if-lez v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 743
    .local v0, "finalPercent":I
    :goto_0
    if-ltz v0, :cond_2

    .line 744
    if-le v0, v2, :cond_0

    move v0, v2

    .line 750
    .end local v0    # "finalPercent":I
    :cond_0
    :goto_1
    return v0

    .line 742
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    goto :goto_0

    .line 750
    .restart local v0    # "finalPercent":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 589
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 590
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    .line 618
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 619
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1702(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z

    .line 620
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v6

    iget-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->forceLoad:Z

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadCompareStatistics(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v5

    .line 622
    .local v5, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 623
    const-string v6, "GameAchievementComparisonViewModel"

    const-string v7, "Unable to statistics"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    sget-object v5, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 708
    .end local v5    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_0
    :goto_0
    return-object v5

    .line 627
    .restart local v5    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCompareStatisticsData(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v4

    .line 628
    .local v4, "statsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    .line 630
    .local v2, "statList":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;>;"
    if-eqz v4, :cond_d

    .line 631
    iget-object v6, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    if-eqz v6, :cond_d

    iget-object v6, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    if-eqz v6, :cond_d

    iget-object v6, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_d

    .line 632
    if-eqz v4, :cond_5

    .line 633
    iget-object v6, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;

    .line 634
    .local v0, "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    if-eqz v0, :cond_2

    iget-object v7, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->titleid:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 635
    iget-object v7, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->titleid:Ljava/lang/String;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 636
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v8, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->statlistscollection:Ljava/util/ArrayList;

    iput-object v8, v7, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->heroStats:Ljava/util/ArrayList;

    .line 638
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v7, v7, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->heroStats:Ljava/util/ArrayList;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v7, v7, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->heroStats:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 639
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v7, v7, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->heroStats:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 640
    .local v3, "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v3, :cond_4

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->arrangebyfieldid:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 642
    if-eqz v3, :cond_3

    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_3

    .line 643
    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 644
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->addYouStatisticsItemToList(Ljava/util/Hashtable;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    goto :goto_1

    .line 648
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    :cond_4
    if-eqz v3, :cond_3

    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_3

    .line 649
    iget-object v8, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 650
    .restart local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->addMeStatisticsItemToList(Ljava/util/Hashtable;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    goto :goto_2

    .line 661
    .end local v0    # "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .end local v3    # "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    :cond_5
    iget-object v6, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->statlistscollection:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 662
    .restart local v3    # "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    iget-object v7, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    if-eqz v7, :cond_6

    iget-object v7, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_6

    .line 663
    iget-object v7, v3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_7
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 664
    .restart local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v9, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v9}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 665
    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->xuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 666
    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->addMeStatisticsItemToList(Ljava/util/Hashtable;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 668
    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 669
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v9, v12, v13}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v10

    iput-wide v10, v8, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meMinutesPlayedValue:J

    .line 671
    :cond_8
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertFromMinuteToHours(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meMinutesPlayed:Ljava/lang/String;

    goto :goto_3

    .line 674
    :cond_9
    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->addYouStatisticsItemToList(Ljava/util/Hashtable;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 676
    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 677
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v9, v12, v13}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v10

    iput-wide v10, v8, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youMinutesPlayedValue:J

    .line 679
    :cond_a
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertFromMinuteToHours(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youMinutesPlayed:Ljava/lang/String;

    goto :goto_3

    .line 681
    :cond_b
    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v9, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->GameProgress:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v9}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 682
    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->xuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 683
    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->addMeStatisticsItemToList(Ljava/util/Hashtable;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 685
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->getProgressPercentage(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meGameProgressValue:I

    .line 686
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget v9, v9, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meGameProgressValue:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->meGameProgressPercentage:Ljava/lang/String;

    goto/16 :goto_3

    .line 688
    :cond_c
    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->addYouStatisticsItemToList(Ljava/util/Hashtable;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 690
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->getProgressPercentage(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youGameProgressValue:I

    .line 691
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget v9, v9, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youGameProgressValue:I

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->youGameProgressPercentage:Ljava/lang/String;

    goto/16 :goto_3

    .line 700
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .end local v3    # "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    :cond_d
    if-eqz v2, :cond_0

    .line 701
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->setOrderedCompareStatisticsList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 708
    .end local v2    # "statList":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;>;"
    .end local v4    # "statsData":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .end local v5    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_e
    sget-object v5, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 613
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 595
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 596
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1500(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 597
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 608
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1500(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 609
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 585
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 601
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 602
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$1602(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z

    .line 603
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadStatisticsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->updateAdapter()V

    .line 604
    return-void
.end method
