.class final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubAdminSettingsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "GetClubAccountDetailsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V
    .locals 1

    .prologue
    .line 798
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 800
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubAccountsService()Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->accountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;

    .prologue
    .line 798
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 804
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 4

    .prologue
    .line 821
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->accountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/IClubAccountsService;->getClubDetails(J)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 826
    :goto_0
    return-object v1

    .line 822
    :catch_0
    move-exception v0

    .line 823
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->access$400()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to load club account details"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 826
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 798
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 1

    .prologue
    .line 815
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 798
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->onError()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 809
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Z)Z

    .line 810
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V

    .line 811
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V
    .locals 1
    .param p1, "clubAccountsResponse"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .prologue
    .line 835
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V

    .line 836
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 798
    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 831
    return-void
.end method
