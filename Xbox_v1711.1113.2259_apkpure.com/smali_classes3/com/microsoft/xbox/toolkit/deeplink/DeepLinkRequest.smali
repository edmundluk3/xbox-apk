.class public final Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;
.super Ljava/lang/Object;
.source "DeepLinkRequest.java"


# instance fields
.field private final intents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private final launchPackages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final packageInStore:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageInStore"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->intents:Ljava/util/List;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->launchPackages:Ljava/util/List;

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->packageInStore:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public static newInstance()Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static withStorePackage(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;
    .locals 1
    .param p0, "packageInStore"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 32
    new-instance v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public addIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 42
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->intents:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    return-object p0
.end method

.method public addLaunchPackage(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;
    .locals 1
    .param p1, "launchPackage"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 49
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->launchPackages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    return-object p0
.end method

.method public defaultExecute()Landroid/content/Intent;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 71
    invoke-static {}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->defaultExecutor()Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkExecutor;->execute(Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76
    if-ne p0, p1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v1

    .line 78
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    if-nez v3, :cond_2

    move v1, v2

    .line 79
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 81
    check-cast v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    .line 82
    .local v0, "other":Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->intents:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->intents:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->launchPackages:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->launchPackages:Ljava/util/List;

    .line 83
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->packageInStore:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->packageInStore:Ljava/lang/String;

    .line 84
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getIntents()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->intents:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLaunchPackages()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->launchPackages:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPackageInStore()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->packageInStore:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 90
    const/16 v0, 0x11

    .line 91
    .local v0, "hashCode":I
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->intents:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 92
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->launchPackages:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 93
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->packageInStore:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 94
    return v0
.end method
