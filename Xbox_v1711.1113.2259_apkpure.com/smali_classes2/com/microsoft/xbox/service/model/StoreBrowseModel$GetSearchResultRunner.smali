.class Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "StoreBrowseModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/StoreBrowseModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetSearchResultRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
        ">;"
    }
.end annotation


# instance fields
.field private loadMore:Z

.field private final storeService:Lcom/microsoft/xbox/service/store/IStoreService;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/StoreBrowseModel;Z)V
    .locals 1
    .param p2, "loadMore"    # Z

    .prologue
    .line 140
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 137
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    .line 141
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->loadMore:Z

    .line 142
    return-void
.end method

.method private getAddonItemsFromBigCat(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 4
    .param p1, "skipItem"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    .line 194
    const/4 v0, 0x0

    .line 195
    .local v0, "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$StoreBrowseFilter:[I

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported filter type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 209
    :goto_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->getEDSV2ItemsFromRecommendationList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v1

    return-object v1

    .line 197
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v1, v3, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getTopPaidAddonList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 198
    goto :goto_0

    .line 200
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v1, v3, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getTopFreeAddonList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 201
    goto :goto_0

    .line 203
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v1, v3, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getNewAddonList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 204
    goto :goto_0

    .line 195
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getAppItemsFromBigCat(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 4
    .param p1, "skipItem"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    .line 214
    const/4 v0, 0x0

    .line 215
    .local v0, "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$StoreBrowseFilter:[I

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sparse-switch v1, :sswitch_data_0

    .line 223
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported filter type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 226
    :goto_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->getEDSV2ItemsFromRecommendationList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v1

    return-object v1

    .line 217
    :sswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v1, v3, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getNewAppList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 218
    goto :goto_0

    .line 220
    :sswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v1, v3, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getPopularAppList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 221
    goto :goto_0

    .line 215
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method private getEDSV2ItemsFromRecommendationList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 6
    .param p1, "list"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 260
    const/4 v3, 0x0

    .line 263
    .local v3, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v4, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductsReducedInfoFromList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    .line 264
    .local v2, "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 266
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 267
    .local v1, "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 268
    .local v0, "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 271
    .end local v0    # "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    :cond_0
    new-instance v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .end local v3    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;-><init>()V

    .line 272
    .restart local v3    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->setItems(Ljava/util/ArrayList;)V

    .line 273
    if-eqz p1, :cond_1

    iget-object v4, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->pagingInfo:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;

    if-eqz v4, :cond_1

    .line 274
    iget-object v4, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->pagingInfo:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;

    iget v4, v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecoListPagingInfo;->totalItems:I

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->setTotalCount(I)V

    .line 278
    .end local v1    # "mediaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_1
    return-object v3
.end method

.method private getGamepassItemsFromBigCat(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 5
    .param p1, "skipItem"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 231
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 233
    .local v1, "storeListType":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;
    sget-object v2, Lcom/microsoft/xbox/service/model/StoreBrowseModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$StoreBrowseFilter:[I

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 250
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported filter type: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 253
    :goto_0
    const-string v4, "Store list type is still unknown"

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    if-eq v1, v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-static {v4, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 254
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    const/16 v4, 0xa

    invoke-interface {v2, v1, v4, v3, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getGamepassList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;III)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 255
    .local v0, "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->getEDSV2ItemsFromRecommendationList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v2

    return-object v2

    .line 235
    .end local v0    # "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList1:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 236
    goto :goto_0

    .line 238
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList2:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 239
    goto :goto_0

    .line 241
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList3:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 242
    goto :goto_0

    .line 244
    :pswitch_3
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList4:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 245
    goto :goto_0

    .line 247
    :pswitch_4
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList5:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 248
    goto :goto_0

    :cond_0
    move v2, v3

    .line 253
    goto :goto_1

    .line 233
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getItemsFromBigCat(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 4
    .param p1, "skipItem"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/16 v3, 0xa

    .line 174
    const/4 v0, 0x0

    .line 175
    .local v0, "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$StoreBrowseFilter:[I

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported filter type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseFilter()Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 189
    :goto_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->getEDSV2ItemsFromRecommendationList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v1

    return-object v1

    .line 177
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v1, v3, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getMostPlayedList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 178
    goto :goto_0

    .line 180
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v1, v3, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getRecentList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 181
    goto :goto_0

    .line 183
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v1, v3, p1}, Lcom/microsoft/xbox/service/store/IStoreService;->getComingSoonList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    .line 184
    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 146
    const/4 v2, 0x0

    .line 148
    .local v2, "skipItems":I
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->loadMore:Z

    if-eqz v3, :cond_0

    .line 149
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getResult()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v1

    .line 150
    .local v1, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v1, :cond_0

    .line 151
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    .line 152
    .local v0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 153
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 158
    .end local v0    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v1    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseType()Lcom/microsoft/xbox/service/model/StoreBrowseType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/StoreBrowseType;->AddOns:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    if-ne v3, v4, :cond_1

    .line 159
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->getAddonItemsFromBigCat(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v3

    .line 168
    :goto_0
    return-object v3

    .line 161
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseType()Lcom/microsoft/xbox/service/model/StoreBrowseType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Apps:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    if-ne v3, v4, :cond_2

    .line 162
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->getAppItemsFromBigCat(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v3

    goto :goto_0

    .line 164
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseType()Lcom/microsoft/xbox/service/model/StoreBrowseType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Gamepass:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    if-ne v3, v4, :cond_3

    .line 165
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->getGamepassItemsFromBigCat(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v3

    goto :goto_0

    .line 168
    :cond_3
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->getItemsFromBigCat(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v3

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->getBrowseType()Lcom/microsoft/xbox/service/model/StoreBrowseType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Games:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    if-ne v0, v1, :cond_0

    .line 293
    const-wide/16 v0, 0x2328

    .line 295
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x2329

    goto :goto_0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 287
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->this$0:Lcom/microsoft/xbox/service/model/StoreBrowseModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/StoreBrowseModel$GetSearchResultRunner;->loadMore:Z

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/StoreBrowseModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V

    .line 288
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 283
    return-void
.end method
