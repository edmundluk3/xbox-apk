.class public Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ActivityAlertScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen$ActivityAlertAdapterProvider;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "Activity Alerts"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 20
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 21
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;->onCreateContentView()V

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen$ActivityAlertAdapterProvider;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen$ActivityAlertAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->onCreate()V

    .line 25
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 34
    const v0, 0x7f030023

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;->setContentView(I)V

    .line 35
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 29
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 30
    return-void
.end method
