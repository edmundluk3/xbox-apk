.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PeopleHubAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LeaderboardItemViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
        ">;"
    }
.end annotation


# instance fields
.field gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06a3
    .end annotation
.end field

.field gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06a6
    .end annotation
.end field

.field monthlyGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06a8
    .end annotation
.end field

.field progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06a9
    .end annotation
.end field

.field ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06a5
    .end annotation
.end field

.field ribbonLayout:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06a4
    .end annotation
.end field

.field subtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e06a7
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 373
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    .line 374
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 375
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 377
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder$$Lambda$1;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 384
    return-void
.end method

.method static synthetic lambda$new$0(Landroid/view/View;)V
    .locals 3
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 379
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreen;

    if-nez v0, :cond_0

    .line 380
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "do navigate to Gamerscore Leaderboard"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 383
    :cond_0
    return-void
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;Ljava/lang/String;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/view/MenuItem;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;
    .param p1, "friendXuid"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    .param p3, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 458
    invoke-interface {p3}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 469
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 460
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->shareLeaderboardCompare(Ljava/lang/String;)V

    goto :goto_0

    .line 463
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    goto :goto_0

    .line 466
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v0

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getUser()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->navigateToSendMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    goto :goto_0

    .line 458
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0c2b
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic lambda$onBind$1(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 449
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->goToProfile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$onBind$3(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/view/View;)Z
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 452
    new-instance v1, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 453
    .local v1, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f0f0007

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 455
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getXuid()Ljava/lang/String;

    move-result-object v0

    .line 456
    .local v0, "friendXuid":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 457
    invoke-static {p0, v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;Ljava/lang/String;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 471
    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    .line 473
    :cond_0
    const/4 v2, 0x1

    return v2
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;)V
    .locals 18
    .param p1, "compositeDataItem"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 389
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 390
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->leaderboardItem()Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    move-result-object v7

    .line 391
    .local v7, "item":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getGamerpic()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 394
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v12

    const-string v13, "%1$s, %2$s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    sget-object v16, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v17, 0x7f07066c

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getGamertag()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 395
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f070600

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget-wide v0, v7, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->rank:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getGamertag()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->monthlyGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0705ff

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget-wide v0, v7, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 399
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 400
    .local v5, "gamerscoreString":Ljava/lang/StringBuilder;
    iget-wide v12, v7, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v11}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->getSelfMonthlyGamerscore()J

    move-result-wide v14

    sub-long v2, v12, v14

    .line 401
    .local v2, "gamerscoreDiff":J
    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f070f51

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/16 v12, 0x20

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 403
    const-wide/16 v12, 0x0

    cmp-long v11, v2, v12

    if-lez v11, :cond_1

    .line 404
    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0706a5

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 414
    .local v10, "unconvertedText":Ljava/lang/String;
    :goto_0
    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f070f51

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 415
    .local v4, "gamerscoreIconIndex":I
    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, v10}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 416
    .local v6, "gamerscoreText":Landroid/text/SpannableString;
    if-ltz v4, :cond_0

    .line 417
    new-instance v11, Lcom/microsoft/xbox/xle/app/XLEUtil$TypefaceSpan;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v12}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v12

    const-string v13, "fonts/SegXboxSymbol.ttf"

    invoke-static {v12, v13}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/microsoft/xbox/xle/app/XLEUtil$TypefaceSpan;-><init>(Landroid/graphics/Typeface;)V

    add-int/lit8 v12, v4, 0x1

    const/16 v13, 0x21

    invoke-virtual {v6, v11, v4, v12, v13}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 419
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->subtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v11, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    const/high16 v11, 0x42c80000    # 100.0f

    iget-wide v12, v7, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    long-to-float v12, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v13}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->getTopMonthlyGamerscore()J

    move-result-wide v14

    long-to-float v13, v14

    div-float/2addr v12, v13

    mul-float/2addr v11, v12

    float-to-long v8, v11

    .line 422
    .local v8, "percentOfMax":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    const-wide/16 v12, 0x64

    sub-long/2addr v12, v8

    invoke-virtual {v11, v8, v9, v12, v13}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setExactValues(JJ)V

    .line 424
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->getAdapterPosition()I

    move-result v11

    packed-switch v11, :pswitch_data_0

    .line 444
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 445
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getPrimaryUserColor()I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 446
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->getSecondaryUserColor()I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 449
    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;)Landroid/view/View$OnClickListener;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->itemView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;)Landroid/view/View$OnLongClickListener;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 476
    return-void

    .line 405
    .end local v4    # "gamerscoreIconIndex":I
    .end local v6    # "gamerscoreText":Landroid/text/SpannableString;
    .end local v8    # "percentOfMax":J
    .end local v10    # "unconvertedText":Ljava/lang/String;
    :cond_1
    const-wide/16 v12, 0x0

    cmp-long v11, v2, v12

    if-gez v11, :cond_2

    .line 406
    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0706a6

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "unconvertedText":Ljava/lang/String;
    goto/16 :goto_0

    .line 407
    .end local v10    # "unconvertedText":Ljava/lang/String;
    :cond_2
    iget-boolean v11, v7, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->isSelf:Z

    if-eqz v11, :cond_3

    .line 408
    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0706a8

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "unconvertedText":Ljava/lang/String;
    goto/16 :goto_0

    .line 410
    .end local v10    # "unconvertedText":Ljava/lang/String;
    :cond_3
    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f0706a7

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "unconvertedText":Ljava/lang/String;
    goto/16 :goto_0

    .line 426
    .restart local v4    # "gamerscoreIconIndex":I
    .restart local v6    # "gamerscoreText":Landroid/text/SpannableString;
    .restart local v8    # "percentOfMax":J
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 427
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0c00c5

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;->setImageColor(I)V

    .line 428
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0c00c5

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 429
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0c00c6

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    goto/16 :goto_1

    .line 432
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 433
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0c012d

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;->setImageColor(I)V

    .line 434
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0c012d

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 435
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0c012e

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    goto/16 :goto_1

    .line 438
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbonLayout:Landroid/widget/RelativeLayout;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 439
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->ribbon:Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0c0034

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/toolkit/ui/XLERibbonView;->setImageColor(I)V

    .line 440
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0c0034

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 441
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->progressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0c0035

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    goto/16 :goto_1

    .line 424
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 351
    check-cast p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$LeaderboardItemViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;)V

    return-void
.end method
