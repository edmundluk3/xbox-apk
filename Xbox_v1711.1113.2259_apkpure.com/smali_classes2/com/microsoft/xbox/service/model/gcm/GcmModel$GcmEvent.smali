.class public final enum Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;
.super Ljava/lang/Enum;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GcmEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

.field public static final enum STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 776
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    const-string v1, "STATE_CHANGED"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    .line 775
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->$VALUES:[Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 775
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 775
    const-class v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;
    .locals 1

    .prologue
    .line 775
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->$VALUES:[Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    return-object v0
.end method
