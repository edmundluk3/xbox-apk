.class public Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;
.source "ComposeMessageWithAttachmentViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final activityFeedItemLocator:Ljava/lang/String;

.field private conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

.field private final entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

.field private item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;

.field private final originatingScreen:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 2
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 54
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 55
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getActivityFeedItemLocator()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->activityFeedItemLocator:Ljava/lang/String;

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->activityFeedItemLocator:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    .line 57
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getMessagesOriginatingScreen()Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->originatingScreen:Ljava/lang/Class;

    .line 58
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getComposeMessageConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 60
    return-void

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->activityFeedItemLocator:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getInstance(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;)Lcom/microsoft/xbox/service/model/entity/EntityModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->onFeedItemComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private getItemRootPath()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "itemRootPath":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v2, :cond_0

    .line 230
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v1

    .line 231
    .local v1, "itemType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 241
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    .line 244
    .end local v1    # "itemType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    :cond_0
    :goto_0
    return-object v0

    .line 238
    .restart local v1    # "itemType":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->activityFeedItemLocator:Ljava/lang/String;

    .line 239
    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getSkypeMessageRequest()Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getItemRootPath()Ljava/lang/String;

    move-result-object v0

    .line 252
    .local v0, "itemRootPath":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 253
    .local v1, "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v3, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    sget-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->messageBody:Ljava/lang/String;

    invoke-direct {v5, v2, v0}, Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-direct {v3, v4, v5, v2}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SkypeContentAttachmentFormat;Ljava/lang/String;)V

    move-object v2, v3

    :goto_1
    return-object v2

    :cond_0
    const-string v2, ""

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private goToReturnScreen()V
    .locals 4

    .prologue
    .line 129
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->returnScreen:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->CountPopsToScreen(Ljava/lang/Class;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreens(I)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Unable to pop return screen from compose message"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onFeedItemComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 267
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 280
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->updateAdapter()V

    .line 281
    return-void

    .line 271
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    .line 272
    .local v0, "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    goto :goto_0

    .line 267
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public cancelSendClick()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->updateMessageBody()V

    .line 214
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->isDirty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->goBack()V

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->showDiscardChangesGoBack()V

    goto :goto_0
.end method

.method public getGroupTopic()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupTopic(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;

    move-result-object v0

    .line 293
    :goto_0
    return-object v0

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    goto :goto_0

    .line 293
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 3

    .prologue
    .line 201
    const/4 v1, 0x0

    .line 202
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    if-eqz v2, :cond_0

    .line 203
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    .line 204
    .local v0, "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    .line 208
    .end local v0    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    :cond_0
    return-object v1
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->isLoadingFeedItem()Z

    move-result v0

    return v0
.end method

.method public isFromConversationDetailScreen()Z
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->originatingScreen:Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->originatingScreen:Ljava/lang/Class;

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadingFeedItem()Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public leaveViewModel(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "closeHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->updateMessageBody()V

    .line 259
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->isDirty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->showDiscardChangeWithRunnable(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->isLoadingFeedItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->load(Z)V

    .line 99
    :cond_0
    return-void
.end method

.method public navigateToConversationsSelector()V
    .locals 2

    .prologue
    .line 194
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 195
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->activityFeedItemLocator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putActivityFeedItemLocator(Ljava/lang/String;)V

    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->returnScreen:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMessagesOriginatingScreen(Ljava/lang/Class;)V

    .line 197
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ConversationsSelectorScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 198
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->onStartOverride()V

    .line 65
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 66
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getComposeMessageConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 67
    return-void
.end method

.method protected onStopOverride()V
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->isLoadingFeedItem()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->cancel()V

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    .line 81
    .local v0, "composeAdapter":Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->updateMessageBody()V

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 83
    return-void
.end method

.method protected onUpdateFinished()V
    .locals 4

    .prologue
    .line 137
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageWithAttachementSend:Lcom/microsoft/xbox/service/model/UpdateType;

    const-wide/16 v2, 0xbec

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->checkErrorCode(Lcom/microsoft/xbox/service/model/UpdateType;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const v0, 0x7f07074c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->showError(I)V

    .line 141
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->onUpdateFinished()V

    .line 142
    return-void
.end method

.method public sendSkypeMessageWithAttachment()V
    .locals 6

    .prologue
    const v5, 0x7f07074c

    .line 145
    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->MessageWithAttachementSend:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->setUpdateTypesToCheck(Ljava/util/EnumSet;)V

    .line 147
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v3, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->updateMessageBody()V

    .line 150
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 151
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getSkypeMessageRequest()Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    move-result-object v2

    .line 153
    .local v2, "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    if-eqz v2, :cond_1

    .line 154
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-eqz v3, :cond_0

    .line 155
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeGroupMessage(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 190
    .end local v2    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->updateAdapter()V

    .line 191
    return-void

    .line 157
    .restart local v2    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeMessage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    goto :goto_0

    .line 160
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Could not get itemLocator for attachment"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->showError(I)V

    goto :goto_0

    .line 164
    .end local v2    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->recipients:Ljava/util/ArrayList;

    if-eqz v3, :cond_7

    .line 165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v1, "recipientsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->recipients:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 168
    .local v0, "recipient":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 171
    .end local v0    # "recipient":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getSkypeMessageRequest()Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    move-result-object v2

    .line 172
    .restart local v2    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    if-eqz v2, :cond_6

    .line 173
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 174
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_5

    .line 175
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v3, v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeGroupMessage(Ljava/util/List;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 181
    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShareMessagePost(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    goto :goto_0

    .line 177
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeMessageWithAttachment(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    goto :goto_2

    .line 183
    :cond_6
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Could not get itemLocator for attachment"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->showError(I)V

    goto :goto_0

    .line 187
    .end local v1    # "recipientsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :cond_7
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Tried to send message without any recipients"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendToConversation()Z
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->updateAdapter()V

    .line 125
    :goto_0
    return-void

    .line 109
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->returnScreen:Ljava/lang/Class;

    if-eqz v0, :cond_1

    .line 112
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissBlocking()V

    .line 113
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->goToReturnScreen()V

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->goBack()V

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
