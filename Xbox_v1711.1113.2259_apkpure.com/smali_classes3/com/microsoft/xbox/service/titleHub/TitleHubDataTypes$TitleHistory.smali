.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TitleHistory"
.end annotation


# instance fields
.field private final lastTimePlayed:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "lastTimePlayed"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;->lastTimePlayed:Ljava/lang/String;

    .line 272
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 280
    if-ne p1, p0, :cond_0

    .line 281
    const/4 v1, 0x1

    .line 286
    :goto_0
    return v1

    .line 282
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;

    if-nez v1, :cond_1

    .line 283
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 285
    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;

    .line 286
    .local v0, "that":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;->lastTimePlayed:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;->lastTimePlayed:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public getLastTimePlayed()Ljava/util/Date;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;->lastTimePlayed:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->convert(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;->lastTimePlayed:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleHistory;->lastTimePlayed:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
