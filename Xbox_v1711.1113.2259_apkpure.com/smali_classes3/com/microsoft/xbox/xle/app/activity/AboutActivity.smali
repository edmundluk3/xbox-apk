.class public Lcom/microsoft/xbox/xle/app/activity/AboutActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "AboutActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-string v0, "About"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 19
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 21
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/AboutActivity;->onCreateContentView()V

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/AboutActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 24
    return-void
.end method

.method public onCreateContentView()V
    .locals 3

    .prologue
    .line 28
    const v0, 0x7f03001b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/AboutActivity;->setContentView(I)V

    .line 29
    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/activity/AboutActivity;->setAppBarLayout(IZZ)V

    .line 30
    return-void
.end method
