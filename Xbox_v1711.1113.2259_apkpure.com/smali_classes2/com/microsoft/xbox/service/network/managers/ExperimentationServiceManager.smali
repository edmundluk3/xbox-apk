.class public Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;
.super Ljava/lang/Object;
.source "ExperimentationServiceManager.java"


# static fields
.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public GetSettings(J)Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    .locals 11
    .param p1, "xuid"    # J

    .prologue
    .line 28
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSmartglassExperimentsUrl()Ljava/lang/String;

    move-result-object v1

    .line 29
    .local v1, "endpoint":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v4

    .line 30
    .local v4, "legalLocale":Ljava/lang/String;
    new-instance v5, Ljava/util/Locale;

    invoke-direct {v5, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 31
    .local v5, "locale":Ljava/util/Locale;
    invoke-virtual {v5}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "country":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .local v3, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v8, Lorg/apache/http/message/BasicHeader;

    const-string v9, "X-MSEDGE-CLIENTID"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    new-instance v8, Lorg/apache/http/message/BasicHeader;

    const-string v9, "X-MSEDGE-MARKET"

    invoke-direct {v8, v9, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    const/4 v7, 0x0

    .line 38
    .local v7, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v6, 0x0

    .line 40
    .local v6, "result":Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    :try_start_0
    invoke-static {v1, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v7

    .line 41
    const/16 v8, 0xc8

    iget v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v8, v9, :cond_0

    .line 42
    iget-object v8, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {p1, p2, v8}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->deserialize(JLjava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    move-result-object v6

    .line 43
    invoke-virtual {v6, p1, p2}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->setXuid(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :cond_0
    :goto_0
    return-object v6

    .line 46
    :catch_0
    move-exception v2

    .line 47
    .local v2, "ex":Ljava/lang/Exception;
    sget-object v8, Lcom/microsoft/xbox/service/network/managers/ExperimentationServiceManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "failed to get activity alert summary"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
