.class Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGameClipRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/TitleModel;

.field private qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;

.field private titleId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p3, "titleId"    # Ljava/lang/String;
    .param p4, "qualifier"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .param p5, "xuid"    # Ljava/lang/String;

    .prologue
    .line 841
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 842
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->xuid:Ljava/lang/String;

    .line 843
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 844
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->titleId:Ljava/lang/String;

    .line 845
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 846
    return-void
.end method

.method private getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;
    .locals 4
    .param p1, "user"    # Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    .param p2, "settingId"    # Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .prologue
    .line 929
    if-eqz p1, :cond_1

    .line 930
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->settings:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;

    .line 931
    .local v0, "setting":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->id:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 932
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;->value:Ljava/lang/String;

    .line 937
    .end local v0    # "setting":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$Settings;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 850
    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    if-nez v9, :cond_0

    .line 851
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string v10, "Invalid game clips filter"

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 854
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v9

    iget-object v10, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->titleId:Ljava/lang/String;

    iget-object v11, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->xuid:Ljava/lang/String;

    invoke-interface {v9, v10, v11, v12}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameClipsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    move-result-object v5

    .line 856
    .local v5, "titleGameClipResult":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    if-eqz v5, :cond_a

    iget-object v9, v5, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    if-eqz v9, :cond_a

    .line 857
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 858
    .local v3, "profileGameClips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 860
    .local v8, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v9, v5, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 862
    .local v0, "clip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->state:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->state:Ljava/lang/String;

    sget-object v11, Lcom/microsoft/xbox/service/model/CaptureState;->Published:Lcom/microsoft/xbox/service/model/CaptureState;

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/CaptureState;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 866
    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 867
    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 870
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 871
    .local v1, "gameClipUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;>;"
    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    if-eqz v10, :cond_1

    .line 872
    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;

    .line 873
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    iget-object v11, v2, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;->uriType:Ljava/lang/String;

    sget-object v12, Lcom/microsoft/xbox/service/model/DVRVideoType;->Download:Lcom/microsoft/xbox/service/model/DVRVideoType;

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/DVRVideoType;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 874
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 879
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_1

    .line 880
    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipUris:Ljava/util/ArrayList;

    .line 881
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 886
    .end local v0    # "clip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .end local v1    # "gameClipUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClipUri;>;"
    :cond_5
    new-instance v7, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;

    invoke-direct {v7, v8}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;-><init>(Ljava/util/ArrayList;)V

    .line 887
    .local v7, "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    const/4 v4, 0x0

    .line 888
    .local v4, "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    if-eqz v7, :cond_6

    iget-object v9, v7, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->userIds:Ljava/util/ArrayList;

    if-eqz v9, :cond_6

    iget-object v9, v7, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->userIds:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_6

    .line 889
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v9

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->getUserProfileRequestBody(Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    move-result-object v4

    .line 892
    :cond_6
    if-eqz v4, :cond_9

    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    if-eqz v9, :cond_9

    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_9

    .line 893
    iget-object v9, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_7
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 894
    .local v6, "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    iget-object v10, v5, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_8
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 897
    .restart local v0    # "clip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    iget-object v11, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->state:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_8

    iget-object v11, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->state:Ljava/lang/String;

    sget-object v12, Lcom/microsoft/xbox/service/model/CaptureState;->Published:Lcom/microsoft/xbox/service/model/CaptureState;

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/CaptureState;->name()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 901
    iget-object v11, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    iget-object v12, v6, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 902
    sget-object v11, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-direct {p0, v6, v11}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->setGamerTag(Ljava/lang/String;)V

    goto :goto_1

    .line 908
    .end local v0    # "clip":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .end local v6    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :cond_9
    iput-object v3, v5, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    .line 911
    .end local v3    # "profileGameClips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;>;"
    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .end local v7    # "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    .end local v8    # "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_a
    return-object v5
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 834
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 925
    const-wide/16 v0, 0xbda

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 920
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;->qualifier:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->access$200(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    .line 921
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 916
    return-void
.end method
