.class public Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;
.super Ljava/lang/Object;
.source "SendMessageRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;,
        Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Recipient;
    }
.end annotation


# instance fields
.field public header:Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;

.field public messageText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;-><init>(Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;->header:Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;

    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;->header:Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;->recipients:Ljava/util/ArrayList;

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 4
    .param p2, "messageText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "recipients":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v1, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;-><init>(Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;->header:Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;

    .line 22
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;->header:Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;->recipients:Ljava/util/ArrayList;

    .line 23
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 24
    .local v0, "recipient":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;->header:Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Header;->recipients:Ljava/util/ArrayList;

    new-instance v3, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Recipient;

    invoke-direct {v3, p0, v0}, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest$Recipient;-><init>(Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 26
    .end local v0    # "recipient":Ljava/lang/String;
    :cond_0
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;->messageText:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public static getSendMessageRequestBody(Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;)Ljava/lang/String;
    .locals 4
    .param p0, "sendMessageRequest"    # Lcom/microsoft/xbox/service/model/sls/SendMessageRequest;

    .prologue
    .line 31
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/gson/JsonIOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 35
    :goto_0
    return-object v1

    .line 32
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Lcom/google/gson/JsonIOException;
    const-string v1, "SendMessageRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialzation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/gson/JsonIOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const/4 v1, 0x0

    goto :goto_0
.end method
