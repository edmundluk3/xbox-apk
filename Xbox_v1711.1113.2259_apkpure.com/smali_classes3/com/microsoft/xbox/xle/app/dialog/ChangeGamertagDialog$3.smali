.class Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$3;
.super Ljava/lang/Object;
.source "ChangeGamertagDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 141
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 130
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->resetReservedGamertag()V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->updateDialog()V

    .line 137
    return-void
.end method
