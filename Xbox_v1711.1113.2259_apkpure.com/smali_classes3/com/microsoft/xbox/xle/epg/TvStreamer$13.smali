.class Lcom/microsoft/xbox/xle/epg/TvStreamer$13;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupControlButton(Landroid/view/View;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

.field final synthetic val$command:Ljava/lang/String;

.field final synthetic val$eventName:Ljava/lang/String;

.field final synthetic val$extra:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 601
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->val$eventName:Ljava/lang/String;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->val$command:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->val$extra:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 604
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->val$eventName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 605
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->val$eventName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 608
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 609
    .local v0, "branchSession":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_1

    .line 610
    const-string v1, "TvStreamer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending command: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->val$command:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getCurrentSource()Ljava/lang/String;

    move-result-object v1

    const-string v2, "tuner"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "tuner"

    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->val$command:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$13;->val$extra:Ljava/util/Map;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->sendButton(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z

    .line 613
    :cond_1
    return-void

    .line 611
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
