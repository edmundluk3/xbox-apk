.class public final enum Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;
.super Ljava/lang/Enum;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AchievementType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

.field public static final enum Xbox360:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

.field public static final enum XboxOne:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;


# instance fields
.field private typeId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    const-string v1, "Xbox360"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->Xbox360:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    .line 45
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    const-string v1, "XboxOne"

    invoke-direct {v0, v1, v2, v4}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->XboxOne:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    .line 43
    new-array v0, v4, [Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->Xbox360:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->XboxOne:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->$VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "typeId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput p3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->typeId:I

    .line 51
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    const-class v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->$VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;

    return-object v0
.end method


# virtual methods
.method public getTypeId()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementType;->typeId:I

    return v0
.end method
