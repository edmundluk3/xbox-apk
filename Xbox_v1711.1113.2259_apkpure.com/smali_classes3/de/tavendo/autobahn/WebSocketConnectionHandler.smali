.class public Lde/tavendo/autobahn/WebSocketConnectionHandler;
.super Ljava/lang/Object;
.source "WebSocketConnectionHandler.java"

# interfaces
.implements Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBinaryMessage([B)V
    .locals 0
    .param p1, "payload"    # [B

    .prologue
    .line 68
    return-void
.end method

.method public onClose(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V
    .locals 0
    .param p1, "code"    # Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 42
    return-void
.end method

.method public onOpen()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public onRawTextMessage([B)V
    .locals 0
    .param p1, "payload"    # [B

    .prologue
    .line 60
    return-void
.end method

.method public onTextMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "payload"    # Ljava/lang/String;

    .prologue
    .line 51
    return-void
.end method
