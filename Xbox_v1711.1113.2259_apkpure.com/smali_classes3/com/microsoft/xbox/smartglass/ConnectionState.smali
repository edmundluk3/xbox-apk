.class public final enum Lcom/microsoft/xbox/smartglass/ConnectionState;
.super Ljava/lang/Enum;
.source "ConnectionState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/ConnectionState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/ConnectionState;

.field public static final enum Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

.field public static final enum Connecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

.field public static final enum Disconnected:Lcom/microsoft/xbox/smartglass/ConnectionState;

.field public static final enum Disconnecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

.field public static final enum Error:Lcom/microsoft/xbox/smartglass/ConnectionState;

.field public static final enum Reconnecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/ConnectionState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/ConnectionState;

    const-string v3, "Disconnected"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Disconnected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/ConnectionState;

    const-string v3, "Connecting"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/ConnectionState;

    const-string v3, "Connected"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/smartglass/ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/ConnectionState;

    const-string v3, "Error"

    invoke-direct {v2, v3, v8, v8}, Lcom/microsoft/xbox/smartglass/ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Error:Lcom/microsoft/xbox/smartglass/ConnectionState;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/ConnectionState;

    const-string v3, "Disconnecting"

    invoke-direct {v2, v3, v9, v9}, Lcom/microsoft/xbox/smartglass/ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Disconnecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/ConnectionState;

    const-string v3, "Reconnecting"

    const/4 v4, 0x5

    const/4 v5, 0x5

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Reconnecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

    .line 10
    const/4 v2, 0x6

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/ConnectionState;

    sget-object v3, Lcom/microsoft/xbox/smartglass/ConnectionState;->Disconnected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/smartglass/ConnectionState;->Error:Lcom/microsoft/xbox/smartglass/ConnectionState;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/smartglass/ConnectionState;->Disconnecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/ConnectionState;->Reconnecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->$VALUES:[Lcom/microsoft/xbox/smartglass/ConnectionState;

    .line 25
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->_lookup:Landroid/util/SparseArray;

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/smartglass/ConnectionState;->values()[Lcom/microsoft/xbox/smartglass/ConnectionState;

    move-result-object v2

    array-length v3, v2

    .local v0, "val":Lcom/microsoft/xbox/smartglass/ConnectionState;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 29
    sget-object v4, Lcom/microsoft/xbox/smartglass/ConnectionState;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/ConnectionState;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 28
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 31
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p3, p0, Lcom/microsoft/xbox/smartglass/ConnectionState;->_value:I

    .line 35
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/ConnectionState;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 51
    sget-object v1, Lcom/microsoft/xbox/smartglass/ConnectionState;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ConnectionState;

    .line 52
    .local v0, "val":Lcom/microsoft/xbox/smartglass/ConnectionState;
    if-nez v0, :cond_0

    .line 53
    sget-object v0, Lcom/microsoft/xbox/smartglass/ConnectionState;->Disconnected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    .line 56
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/ConnectionState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/smartglass/ConnectionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ConnectionState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/ConnectionState;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/smartglass/ConnectionState;->$VALUES:[Lcom/microsoft/xbox/smartglass/ConnectionState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/ConnectionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/ConnectionState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/microsoft/xbox/smartglass/ConnectionState;->_value:I

    return v0
.end method
