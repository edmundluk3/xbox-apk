.class Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MergedActivitySummaryModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadCompanionRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;->this$0:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 127
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 135
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getActivitiesServiceManager()Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;

    move-result-object v0

    .line 136
    .local v0, "serviceManager":Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;->this$0:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->access$000(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;->this$0:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->access$100(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;->this$0:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->access$200(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;->this$0:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->access$300(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;->getCompanions(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 147
    const-wide/16 v0, 0xfb6

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel$LoadCompanionRunner;->this$0:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->access$400(Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 143
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method
