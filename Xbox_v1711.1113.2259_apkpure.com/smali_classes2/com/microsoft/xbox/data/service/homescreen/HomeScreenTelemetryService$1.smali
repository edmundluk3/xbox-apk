.class Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;
.super Ljava/lang/Object;
.source "HomeScreenTelemetryService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;->changeDefault(ILcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;

.field final synthetic val$currentSetting:I

.field final synthetic val$preference:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;ILcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;->this$0:Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;

    iput p2, p0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;->val$currentSetting:I

    iput-object p3, p0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;->val$preference:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    .prologue
    .line 29
    const-wide/16 v0, 0x0

    iget v2, p0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;->val$currentSetting:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;->val$preference:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 32
    iget v0, p0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;->val$currentSetting:I

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;->val$preference:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;->val$preference:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->getTelemetryActionName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 36
    :cond_0
    return-void
.end method
