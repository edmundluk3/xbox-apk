.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PeopleHubSocialScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadFollowersAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V
    .locals 0

    .prologue
    .line 554
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$1;

    .prologue
    .line 554
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 557
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 558
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshPeopleHubFollowersProfile()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 586
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 587
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->forceLoad:Z

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersFilter;->FOLLOWERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPeopleHubFollowers(ZLcom/microsoft/xbox/service/model/FollowersFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 554
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 581
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 554
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 563
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 564
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 565
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 576
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 577
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 554
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 569
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 570
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$702(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    .line 571
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadFollowersAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$802(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    .line 572
    return-void
.end method
