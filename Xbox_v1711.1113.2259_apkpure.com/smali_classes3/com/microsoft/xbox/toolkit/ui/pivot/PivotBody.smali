.class public Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;
.super Landroid/widget/FrameLayout;
.source "PivotBody.java"


# instance fields
.field private activepane:I

.field private classToChildMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private linearLayout:Landroid/widget/LinearLayout;

.field private nameToChildMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private panes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field

.field private resumed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    .line 44
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    .line 45
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    .line 49
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->linearLayout:Landroid/widget/LinearLayout;

    .line 50
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->activepane:I

    .line 51
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->resumed:Z

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 72
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    .line 44
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    .line 45
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    .line 49
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->linearLayout:Landroid/widget/LinearLayout;

    .line 50
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->activepane:I

    .line 51
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->resumed:Z

    .line 74
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method private getImportantPanes(I)Ljava/util/ArrayList;
    .locals 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 323
    const/4 v0, 0x0

    .line 336
    :cond_0
    :goto_0
    return-object v0

    .line 326
    :cond_1
    if-ltz p1, :cond_3

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 328
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 329
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 330
    if-lez p1, :cond_2

    .line 331
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge p1, v1, :cond_0

    .line 334
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 326
    .end local v0    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public addPivotPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V
    .locals 7
    .param p1, "pane"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "index"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 110
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    const-string v3, "PivotBody"

    const-string v4, "you are inserting multiple panes of the same class. Use unique name instead! or findPivotPane will return non prodictable results."

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_0
    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setDrawingCacheEnabled(Z)V

    .line 116
    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setVisibility(I)V

    .line 117
    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setIsPivotPane(Z)V

    .line 120
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge p2, v3, :cond_1

    .line 121
    .local v1, "needToAdjustMap":Z
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 122
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->linearLayout:Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v4

    invoke-direct {v3, v4, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, p1, p2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 123
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->linearLayout:Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->size()I

    move-result v4

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v5

    mul-int/2addr v4, v5

    invoke-direct {v3, v4, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    if-eqz v1, :cond_2

    .line 126
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 127
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 129
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 130
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "needToAdjustMap":Z
    :cond_1
    move v1, v2

    .line 120
    goto :goto_0

    .line 135
    .restart local v1    # "needToAdjustMap":Z
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    :cond_3
    return-void
.end method

.method public adjustBottomMargin(I)V
    .locals 3
    .param p1, "bottomMargin"    # I

    .prologue
    .line 443
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 444
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->adjustBottomMargin(I)V

    goto :goto_0

    .line 446
    .end local v0    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public getIndexOfScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)I
    .locals 3
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 465
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 470
    :goto_0
    return v0

    .line 469
    :cond_0
    const-string v0, "PivotBody"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t find index for screen class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getIndexOfScreen(Ljava/lang/Class;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 455
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 460
    :goto_0
    return v0

    .line 459
    :cond_0
    const-string v0, "PivotBody"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t find index for screen class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getPivotPane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 412
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 416
    :goto_0
    return-object v0

    .line 415
    :cond_0
    const-string v0, "PivotBody"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "try go get pivot pane for invalid index:  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize([Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 6
    .param p1, "panes"    # [Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v5, 0x0

    .line 87
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    .line 88
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    .line 89
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    .line 91
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->linearLayout:Landroid/widget/LinearLayout;

    .line 92
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 96
    if-eqz p1, :cond_0

    .line 97
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 98
    aget-object v1, p1, v0

    .line 99
    .local v1, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->addPivotPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    .end local v0    # "i":I
    .end local v1    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->size()I

    move-result v3

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v4

    mul-int/2addr v3, v4

    const/4 v4, -0x1

    invoke-virtual {p0, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->addView(Landroid/view/View;II)V

    .line 104
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->setScrollX(I)V

    .line 105
    return-void
.end method

.method public onAnimateInCompleted(I)V
    .locals 3
    .param p1, "currentPaneIndex"    # I

    .prologue
    .line 379
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 380
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceUpdateViewImmediately()V

    goto :goto_0

    .line 383
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public onAnimateInStarted()V
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->activepane:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceUpdateViewImmediately()V

    .line 368
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 404
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->activepane:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 263
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 264
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreate()V

    goto :goto_0

    .line 266
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 408
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->activepane:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 409
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 386
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 387
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onDestroy()V

    goto :goto_0

    .line 389
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 349
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 351
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    goto :goto_0

    .line 354
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method protected onRehydrate()V
    .locals 3

    .prologue
    .line 398
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 399
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onRehydrate()V

    goto :goto_0

    .line 401
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 357
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 359
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    goto :goto_0

    .line 363
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->resumed:Z

    .line 364
    return-void
.end method

.method public onStart(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 274
    const-string v1, "PivotBody"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start pivot with index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const/4 v1, -0x1

    if-gt p1, v1, :cond_0

    .line 277
    const/4 p1, 0x0

    .line 280
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 291
    :cond_1
    return-void

    .line 287
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 288
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 340
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 342
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStop()V

    goto :goto_0

    .line 345
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->resumed:Z

    .line 346
    return-void
.end method

.method protected onTombstone()V
    .locals 3

    .prologue
    .line 392
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 393
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onTombstone()V

    goto :goto_0

    .line 395
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public optimizeStart(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getImportantPanes(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 300
    .local v1, "panesToInit":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    if-nez v1, :cond_1

    .line 313
    :cond_0
    return-void

    .line 304
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 305
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getIsStarted()Z

    move-result v3

    if-nez v3, :cond_3

    .line 306
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    .line 308
    :cond_3
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->resumed:Z

    if-eqz v3, :cond_2

    .line 310
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    goto :goto_0
.end method

.method public removePivotPane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 151
    if-ltz p1, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 152
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 153
    .local v1, "removedScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeViewFromParent(Landroid/view/View;)V

    .line 156
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 158
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 159
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 161
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 162
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->nameToChildMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->classToChildMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    .end local v0    # "i":I
    .end local v1    # "removedScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1
.end method

.method public resetBottomMargin()V
    .locals 3

    .prologue
    .line 449
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 450
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->resetBottomMargin()V

    goto :goto_0

    .line 452
    .end local v0    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public setActivePivotPane(I)V
    .locals 4
    .param p1, "activePivotPaneIndex"    # I

    .prologue
    .line 421
    if-ltz p1, :cond_2

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_2

    .line 422
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->activepane:I

    .line 424
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 425
    if-eq v0, p1, :cond_0

    .line 426
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    .line 424
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 430
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    .line 434
    .end local v0    # "i":I
    :goto_1
    return-void

    .line 432
    :cond_2
    const-string v1, "PivotBody"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set active pane to invalid index:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setInactive()V
    .locals 3

    .prologue
    .line 437
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 438
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    goto :goto_0

    .line 440
    .end local v0    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public setScrollX(I)V
    .locals 1
    .param p1, "x"    # I

    .prologue
    .line 179
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->scrollTo(II)V

    .line 180
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
