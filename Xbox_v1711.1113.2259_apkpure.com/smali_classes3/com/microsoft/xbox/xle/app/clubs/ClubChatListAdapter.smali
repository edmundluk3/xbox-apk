.class public Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubChatListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final COMBINE_MESSAGE_TIME_RANGE:J

.field private static final MESSAGE_VIEW_TYPE:I = 0x7f03006f

.field private static final STATUS_MESSAGE_VIEW_TYPE:I = 0x7f030070

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final clubId:J

.field private final meXuid:Ljava/lang/String;

.field private final messageHandler:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

.field private final personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 39
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->TAG:Ljava/lang/String;

    .line 41
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->COMBINE_MESSAGE_TIME_RANGE:J

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;J)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 53
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 54
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->messageHandler:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->meXuid:Ljava/lang/String;

    .line 60
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->clubId:J

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->messageHandler:Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;

    return-object v0
.end method

.method static synthetic access$200()J
    .locals 2

    .prologue
    .line 38
    sget-wide v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->COMBINE_MESSAGE_TIME_RANGE:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->meXuid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->clubId:J

    return-wide v0
.end method

.method private static isStatus(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;)Z
    .locals 1
    .param p0, "type"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .prologue
    .line 116
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->JoinChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->LeaveChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 103
    .local v0, "item":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    if-eqz v0, :cond_1

    .line 104
    iget-object v1, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->type:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->isStatus(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    const v1, 0x7f030070

    .line 112
    :goto_0
    return v1

    .line 108
    :cond_0
    const v1, 0x7f03006f

    goto :goto_0

    .line 112
    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 88
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 89
    .local v0, "item":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;->bindTo(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V

    .line 92
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$BaseViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 67
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 69
    .local v0, "activityContext":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 83
    :goto_0
    return-object v2

    .line 73
    :cond_0
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 75
    .local v1, "inflater":Landroid/view/LayoutInflater;
    packed-switch p2, :pswitch_data_0

    .line 82
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown viewType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :pswitch_0
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;

    const v3, 0x7f03006f

    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$MessageViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 79
    :pswitch_1
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;

    const v3, 0x7f030070

    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$StatusViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x7f03006f
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
