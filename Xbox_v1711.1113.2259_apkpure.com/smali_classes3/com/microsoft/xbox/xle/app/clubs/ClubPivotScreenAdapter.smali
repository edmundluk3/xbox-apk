.class public Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubPivotScreenAdapter.java"


# instance fields
.field private final pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 18
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;

    .line 21
    const v0, 0x7f0e03b5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .line 22
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->getClubName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;->getPreferredColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderBackgroundColor(I)V

    .line 28
    return-void
.end method
