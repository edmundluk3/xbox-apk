.class public interface abstract Lcom/microsoft/playready/networkdevice/IReceiverSessionTask;
.super Ljava/lang/Object;
.source "IReceiverSessionTask.java"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<",
        "Lcom/microsoft/playready/networkdevice/IReceiverSession;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract addReceiverSessionListener(Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;)V
.end method

.method public abstract removeReceiverSessionListener(Lcom/microsoft/playready/networkdevice/IReceiverSessionListener;)V
.end method
