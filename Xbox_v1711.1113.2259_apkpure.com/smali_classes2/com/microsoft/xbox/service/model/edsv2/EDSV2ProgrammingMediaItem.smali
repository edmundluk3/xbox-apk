.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2ProgrammingMediaItem.java"


# instance fields
.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;)V
    .locals 2
    .param p1, "data"    # Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 20
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->Action:Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->type:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->type:Ljava/lang/String;

    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->type:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Game:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->setMediaItemTypeFromInt(I)V

    .line 36
    :goto_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->Title:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->Name:Ljava/lang/String;

    .line 37
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->Description:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->Description:Ljava/lang/String;

    .line 38
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->ImageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->imageUrl:Ljava/lang/String;

    .line 39
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;->Action:Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Target:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->ID:Ljava/lang/String;

    .line 40
    return-void

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->type:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Movie:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25
    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 26
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->type:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->TV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 27
    const/16 v0, 0x3ec

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 28
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->type:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->Album:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 29
    const/16 v0, 0x3ee

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 30
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->type:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;->App:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 31
    const/16 v0, 0x3d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0

    .line 33
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ProgrammingMediaItem;->setMediaItemTypeFromInt(I)V

    goto :goto_0
.end method


# virtual methods
.method public getIsProgrammingOverride()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method
