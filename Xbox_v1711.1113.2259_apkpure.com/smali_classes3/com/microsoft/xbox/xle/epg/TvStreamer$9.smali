.class Lcom/microsoft/xbox/xle/epg/TvStreamer$9;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;-><init>(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$9;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v5, 0x1

    .line 297
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 315
    const-string v2, "Bad item ID in quality picker menu"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 326
    :cond_0
    :goto_0
    return v5

    .line 303
    :pswitch_0
    const-string v1, "high"

    .line 304
    .local v1, "quality":Ljava/lang/String;
    const/4 v0, 0x0

    .line 319
    .local v0, "biQualityIndexPos":I
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$9;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getQuality()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 320
    const-string v2, "TvStreamer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " quality selected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$9;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setQuality(Ljava/lang/String;)V

    .line 322
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$9;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->onStreamFormatChanged()V

    .line 323
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "Stream Quality Picker"

    invoke-virtual {v2, v3, v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;I)V

    goto :goto_0

    .line 307
    .end local v0    # "biQualityIndexPos":I
    .end local v1    # "quality":Ljava/lang/String;
    :pswitch_1
    const-string v1, "medium"

    .line 308
    .restart local v1    # "quality":Ljava/lang/String;
    const/4 v0, 0x1

    .line 309
    .restart local v0    # "biQualityIndexPos":I
    goto :goto_1

    .line 311
    .end local v0    # "biQualityIndexPos":I
    .end local v1    # "quality":Ljava/lang/String;
    :pswitch_2
    const-string v1, "low"

    .line 312
    .restart local v1    # "quality":Ljava/lang/String;
    const/4 v0, 0x2

    .line 313
    .restart local v0    # "biQualityIndexPos":I
    goto :goto_1

    .line 297
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0c36
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
