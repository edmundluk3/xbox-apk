.class public Lcom/microsoft/playready/PlayReadyFactory;
.super Ljava/lang/Object;
.source "PlayReadyFactory.java"

# interfaces
.implements Lcom/microsoft/playready/IPlayReadyFactory;


# static fields
.field private static final TAG:Ljava/lang/String; = "PlayReadyFactory"

.field private static drmConfigInit:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-boolean v0, Lcom/microsoft/playready/PlayReadyFactory;->drmConfigInit:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    if-nez p1, :cond_0

    .line 24
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A non null App context must be provided in order to initialzie the DrmSubsystem before any new objects can be created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_0
    const-class v1, Lcom/microsoft/playready/PlayReadyFactory;

    monitor-enter v1

    .line 35
    :try_start_0
    sget-boolean v0, Lcom/microsoft/playready/PlayReadyFactory;->drmConfigInit:Z

    if-nez v0, :cond_1

    .line 37
    invoke-static {p1}, Lcom/microsoft/playready/DrmConfig;->setAndroidId(Landroid/content/Context;)V

    .line 38
    invoke-static {p1}, Lcom/microsoft/playready/DrmConfig;->initDrmPath(Landroid/content/Context;)V

    .line 39
    const/4 v0, 0x1

    sput-boolean v0, Lcom/microsoft/playready/PlayReadyFactory;->drmConfigInit:Z

    .line 33
    :cond_1
    monitor-exit v1

    .line 42
    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public createDomainHandler()Lcom/microsoft/playready/IDomainHandler;
    .locals 1

    .prologue
    .line 74
    invoke-static {p0}, Lcom/microsoft/playready/DomainHandler;->createDomainHandler(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/IDomainHandler;

    move-result-object v0

    return-object v0
.end method

.method public createLicenseAcquirer()Lcom/microsoft/playready/ILicenseAcquirer;
    .locals 1

    .prologue
    .line 52
    invoke-static {p0}, Lcom/microsoft/playready/LicenseAcquirer;->createLicenseAcquirer(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/ILicenseAcquirer;

    move-result-object v0

    return-object v0
.end method

.method public createMeteringReporter()Lcom/microsoft/playready/IMeteringReporter;
    .locals 1

    .prologue
    .line 63
    invoke-static {p0}, Lcom/microsoft/playready/MeteringReporter;->createMeteringReporter(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/IMeteringReporter;

    move-result-object v0

    return-object v0
.end method

.method public createPRActivator()Lcom/microsoft/playready/IPRActivator;
    .locals 1

    .prologue
    .line 96
    invoke-static {p0}, Lcom/microsoft/playready/PRActivator;->createPRActivator(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/PRActivator;

    move-result-object v0

    return-object v0
.end method

.method public createPRMediaPlayer()Lcom/microsoft/playready/PRMediaPlayer;
    .locals 1

    .prologue
    .line 85
    invoke-static {p0}, Lcom/microsoft/playready/PRMediaPlayer;->createPRMediaPlayer(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/PRMediaPlayer;

    move-result-object v0

    return-object v0
.end method
