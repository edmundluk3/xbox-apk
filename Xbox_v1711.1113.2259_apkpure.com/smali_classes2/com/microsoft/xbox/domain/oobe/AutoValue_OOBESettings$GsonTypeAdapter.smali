.class public final Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_OOBESettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/domain/oobe/OOBESettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final autoDSTAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final autoUpdateAppsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final autoUpdateSystemAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private defaultAutoDST:Z

.field private defaultAutoUpdateApps:Z

.field private defaultAutoUpdateSystem:Z

.field private defaultInstantOn:Ljava/lang/Boolean;

.field private defaultTimeZone:Ljava/lang/String;

.field private final instantOnAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final timeZoneAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 2
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultAutoUpdateApps:Z

    .line 26
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultAutoUpdateSystem:Z

    .line 27
    iput-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultInstantOn:Ljava/lang/Boolean;

    .line 28
    iput-boolean v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultAutoDST:Z

    .line 29
    iput-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultTimeZone:Ljava/lang/String;

    .line 31
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->autoUpdateAppsAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->autoUpdateSystemAdapter:Lcom/google/gson/TypeAdapter;

    .line 33
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->instantOnAdapter:Lcom/google/gson/TypeAdapter;

    .line 34
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->autoDSTAdapter:Lcom/google/gson/TypeAdapter;

    .line 35
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->timeZoneAdapter:Lcom/google/gson/TypeAdapter;

    .line 36
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .locals 8
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v7, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v7, :cond_0

    .line 79
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 80
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    .line 82
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 83
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultAutoUpdateApps:Z

    .line 84
    .local v1, "autoUpdateApps":Z
    iget-boolean v2, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultAutoUpdateSystem:Z

    .line 85
    .local v2, "autoUpdateSystem":Z
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultInstantOn:Ljava/lang/Boolean;

    .line 86
    .local v3, "instantOn":Ljava/lang/Boolean;
    iget-boolean v4, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultAutoDST:Z

    .line 87
    .local v4, "autoDST":Z
    iget-object v5, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultTimeZone:Ljava/lang/String;

    .line 88
    .local v5, "timeZone":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 89
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v6

    .line 90
    .local v6, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v7, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v7, :cond_1

    .line 91
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 94
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 116
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 94
    :sswitch_0
    const-string v7, "allowAutoUpdateApps"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v7, "allowAutoUpdateSystem"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v7, "AllowInstantOn"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v7, "AutoDSTEnabled"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v7, "currentTimeZone"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    .line 96
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->autoUpdateAppsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 97
    goto :goto_1

    .line 100
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->autoUpdateSystemAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 101
    goto :goto_1

    .line 104
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->instantOnAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "instantOn":Ljava/lang/Boolean;
    check-cast v3, Ljava/lang/Boolean;

    .line 105
    .restart local v3    # "instantOn":Ljava/lang/Boolean;
    goto :goto_1

    .line 108
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->autoDSTAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 109
    goto/16 :goto_1

    .line 112
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->timeZoneAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "timeZone":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 113
    .restart local v5    # "timeZone":Ljava/lang/String;
    goto/16 :goto_1

    .line 120
    .end local v6    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 121
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings;-><init>(ZZLjava/lang/Boolean;ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 94
    :sswitch_data_0
    .sparse-switch
        -0x31fbd989 -> :sswitch_2
        -0x17048af0 -> :sswitch_1
        0xa8254b2 -> :sswitch_4
        0x42021193 -> :sswitch_0
        0x75b780eb -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAutoDST(Z)Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAutoDST"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultAutoDST:Z

    .line 51
    return-object p0
.end method

.method public setDefaultAutoUpdateApps(Z)Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAutoUpdateApps"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultAutoUpdateApps:Z

    .line 39
    return-object p0
.end method

.method public setDefaultAutoUpdateSystem(Z)Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAutoUpdateSystem"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultAutoUpdateSystem:Z

    .line 43
    return-object p0
.end method

.method public setDefaultInstantOn(Ljava/lang/Boolean;)Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultInstantOn"    # Ljava/lang/Boolean;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultInstantOn:Ljava/lang/Boolean;

    .line 47
    return-object p0
.end method

.method public setDefaultTimeZone(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTimeZone"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->defaultTimeZone:Ljava/lang/String;

    .line 55
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    if-nez p2, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 75
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 64
    const-string v0, "allowAutoUpdateApps"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->autoUpdateAppsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateApps()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 66
    const-string v0, "allowAutoUpdateSystem"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->autoUpdateSystemAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateSystem()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 68
    const-string v0, "AllowInstantOn"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->instantOnAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->instantOn()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 70
    const-string v0, "AutoDSTEnabled"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->autoDSTAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoDST()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 72
    const-string v0, "currentTimeZone"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->timeZoneAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->timeZone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 74
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBESettings$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V

    return-void
.end method
