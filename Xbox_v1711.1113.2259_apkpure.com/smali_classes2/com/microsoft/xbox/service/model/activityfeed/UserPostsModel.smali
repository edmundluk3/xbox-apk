.class public Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "UserPostsModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PrepareLfgShareRunner;,
        Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$GetLinkPreviewRunner;,
        Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;,
        Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field public static INSTANCE:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

.field private static final TAG:Ljava/lang/String;

.field private static final userPostsService:Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->TAG:Ljava/lang/String;

    .line 33
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getUserPostsService()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->userPostsService:Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->INSTANCE:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->userPostsService:Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    return-object v0
.end method


# virtual methods
.method public getLinkPostPreview(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "previewLink"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 69
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$GetLinkPreviewRunner;

    invoke-direct {v6, p0, p1}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$GetLinkPreviewRunner;-><init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public postTextToClubFeed(Ljava/lang/String;JLcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 12
    .param p1, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p4, "linkPreview"    # Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 48
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 50
    const/4 v0, 0x1

    const-wide/16 v8, 0x0

    new-instance v10, Ljava/util/Date;

    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    new-instance v7, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v7}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v1, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;-><init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;JLcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)V

    move v3, v0

    move-wide v4, v8

    move-object v6, v10

    move-object v8, v1

    invoke-static/range {v3 .. v8}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public postTextToUserFeed(Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 9
    .param p1, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "linkPreview"    # Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 39
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    const/4 v0, 0x1

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    new-instance v7, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v7}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v1, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PostToFeedRunner;-><init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;JLcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkPreviewResponse;)V

    move v3, v0

    move-object v6, v8

    move-object v8, v1

    invoke-static/range {v3 .. v8}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public prepareLfgShare(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "postTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 75
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PrepareLfgShareRunner;

    invoke-direct {v6, p0, p1}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$PrepareLfgShareRunner;-><init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public sharePostToClubFeed(Ljava/lang/String;Ljava/lang/String;J)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 11
    .param p1, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "caption"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 61
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p3, p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 63
    const/4 v6, 0x1

    const-wide/16 v8, 0x0

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    new-instance v10, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v10}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;-><init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;Ljava/lang/String;J)V

    move v1, v6

    move-wide v2, v8

    move-object v4, v7

    move-object v5, v10

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public sharePostToUserFeed(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "caption"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 56
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;

    const-string v0, ""

    invoke-static {p2, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v6, p0, p1, v0}, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel$ShareItemToFeedRunner;-><init>(Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method
