.class public Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
.super Ljava/lang/Object;
.source "ClubWhosHereScreenViewModel.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WhosHereListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
        ">;"
    }
.end annotation


# instance fields
.field public currentTimeDelta:J

.field public headingText:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public isModerator:Z

.field public isOwner:Z

.field public presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;ZZ)V
    .locals 0
    .param p1, "presenceData"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "isModerator"    # Z
    .param p3, "isOwner"    # Z

    .prologue
    .line 674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 675
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 677
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 678
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isModerator:Z

    .line 679
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isOwner:Z

    .line 680
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;ZZ)V
    .locals 2
    .param p1, "data"    # Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "isModerator"    # Z
    .param p3, "isOwner"    # Z

    .prologue
    .line 690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 691
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 693
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;-><init>()V

    .line 694
    .local v0, "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v1, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    .line 695
    iget-object v1, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->gamertag:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->gamertag:Ljava/lang/String;

    .line 696
    iget-object v1, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->gamertag:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    .line 697
    iget-object v1, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->displayPicUri:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    .line 699
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 700
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isModerator:Z

    .line 701
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isOwner:Z

    .line 702
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2
    .param p1, "headingText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "currentTimeDelta"    # J
        .annotation build Landroid/support/annotation/Size;
            max = 0x0L
        .end annotation
    .end param

    .prologue
    .line 682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 683
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 684
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeTo(JJ)V

    .line 686
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->headingText:Ljava/lang/String;

    .line 687
    iput-wide p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->currentTimeDelta:J

    .line 688
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;)I
    .locals 3
    .param p1, "other"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 725
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 727
    if-ne p0, p1, :cond_1

    .line 738
    :cond_0
    :goto_0
    return v0

    .line 731
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eq v1, v2, :cond_0

    .line 733
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 734
    :cond_2
    const/4 v0, -0x1

    goto :goto_0

    .line 735
    :cond_3
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 736
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 738
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 662
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->compareTo(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;)I

    move-result v0

    return v0
.end method

.method public isHeading()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 705
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->headingText:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 706
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->headingText:Ljava/lang/String;

    if-eqz v0, :cond_2

    :goto_1
    return v2

    :cond_1
    move v0, v1

    .line 705
    goto :goto_0

    :cond_2
    move v2, v1

    .line 706
    goto :goto_1
.end method

.method public matchesSearchTerm(Ljava/lang/String;)Z
    .locals 6
    .param p1, "searchTerm"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 710
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 711
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 713
    .local v1, "lowerTerm":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v4, :cond_1

    .line 714
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    const-string v5, ""

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 715
    .local v0, "displayName":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->realName:Ljava/lang/String;

    const-string v5, ""

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 717
    .local v2, "realName":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v3, 0x1

    .line 720
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v2    # "realName":Ljava/lang/String;
    :cond_1
    return v3
.end method
