.class public Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;
.super Landroid/widget/LinearLayout;
.source "UtilityBarMessageButton.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/LinearLayout;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field private static final UNREAD_MESSAGE_MAX_COUNT:I = 0x63


# instance fields
.field private messageNoTextView:Landroid/widget/TextView;

.field private unReadMessageCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    .line 44
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 45
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f03026e

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 46
    const v1, 0x7f0e0bc1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->messageNoTextView:Landroid/widget/TextView;

    .line 48
    new-instance v1, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton$1;-><init>(Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;

    .prologue
    .line 28
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    return v0
.end method


# virtual methods
.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v6, 0x1

    .line 65
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 66
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v5

    if-ne v4, v5, :cond_7

    .line 67
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/MessageModel;->getUnReadMessageCount()I

    move-result v3

    .line 68
    .local v3, "unReadMessageCount":I
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    if-eq v4, v3, :cond_3

    .line 69
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 70
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isLoadingConversation()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v4

    instance-of v4, v4, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;

    if-nez v4, :cond_0

    .line 71
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/MessageModel;->loadSkypeConversationMessages()V

    .line 73
    :cond_0
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    .line 74
    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->messageNoTextView:Landroid/widget/TextView;

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    if-lez v4, :cond_4

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 76
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    const/16 v5, 0x63

    if-le v4, v5, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->messageNoTextView:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 77
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->messageNoTextView:Landroid/widget/TextView;

    const-string v5, "99+"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :cond_1
    if-eqz v1, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->messageNoTextView:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    .line 81
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->messageNoTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 85
    :cond_2
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    if-nez v4, :cond_5

    .line 86
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070d62

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 101
    .end local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    .end local v3    # "unReadMessageCount":I
    :cond_3
    :goto_1
    return-void

    .line 74
    .restart local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    .restart local v3    # "unReadMessageCount":I
    :cond_4
    const/4 v4, 0x0

    goto :goto_0

    .line 87
    :cond_5
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    if-ne v4, v6, :cond_6

    .line 88
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070d70

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 90
    :cond_6
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070d65

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->unReadMessageCount:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 93
    .end local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    .end local v3    # "unReadMessageCount":I
    :cond_7
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v4

    if-ne v4, v1, :cond_3

    .line 94
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    .line 95
    .local v2, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileColorChange:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v2, v4, :cond_3

    .line 96
    if-eqz v1, :cond_3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->messageNoTextView:Landroid/widget/TextView;

    if-eqz v4, :cond_3

    .line 97
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->messageNoTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto :goto_1
.end method
