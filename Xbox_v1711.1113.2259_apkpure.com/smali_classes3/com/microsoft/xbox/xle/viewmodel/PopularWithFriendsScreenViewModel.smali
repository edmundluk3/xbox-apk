.class public Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "PopularWithFriendsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;
    }
.end annotation


# instance fields
.field private isLoadingPopularGames:Z

.field private loadPopularGamesTask:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

.field private popularGames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/PopularGame;",
            ">;"
        }
    .end annotation
.end field

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 1
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 28
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 35
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 37
    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->isLoadingPopularGames:Z

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->popularGames:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->popularGames:Ljava/util/ArrayList;

    return-object p1
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->popularGames:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->popularGames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 89
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->isLoadingPopularGames:Z

    if-eqz v0, :cond_1

    .line 90
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 97
    :goto_0
    return-void

    .line 92
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 95
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getPopularGames()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/PopularGame;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->popularGames:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->isLoadingPopularGames:Z

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->loadPopularGamesTask:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->loadPopularGamesTask:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->cancel()V

    .line 104
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->loadPopularGamesTask:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->loadPopularGamesTask:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->load(Z)V

    .line 106
    return-void
.end method

.method public navigateToGameDetails(Ljava/lang/String;)V
    .locals 5
    .param p1, "bingId"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Game Item Popular"

    const-string v3, ""

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, p1, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 110
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 111
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iput-object p1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 112
    const-string v1, "DGame"

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 113
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 114
    return-void
.end method

.method protected onLoadPopularGamesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 67
    const-string v0, "PopularWithFriendsScreenViewModel"

    const-string v1, "onLoadPopularGamesCompleted Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->isLoadingPopularGames:Z

    .line 70
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 83
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->updateAdapter()V

    .line 84
    return-void

    .line 74
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 78
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->popularGames:Ljava/util/ArrayList;

    .line 79
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->loadPopularGamesTask:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->loadPopularGamesTask:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->cancel()V

    .line 60
    :cond_0
    return-void
.end method
