.class public Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;
.source "PeopleScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;,
        Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SearchGamertagRunner;,
        Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SortedIndexComparator;
    }
.end annotation


# static fields
.field private static final SHOW_RECOMMENDATIONS_COUNT:I = 0x4

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final clubsAndPeopleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private filteredFriendList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private followerData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private followersRecentChangeCount:I

.field private friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

.field private isLoadingFollowerData:Z

.field private isLoadingFollowingData:Z

.field private isLoadingRecentPlayersData:Z

.field private isLoadingRecommendationsData:Z

.field private isLoadingUserClubs:Z

.field private isSearchGamertag:Z

.field private lastSelectedPosition:I

.field private final linkFacebookViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

.field private final linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

.field private listStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/toolkit/network/ListState;",
            ">;"
        }
    .end annotation
.end field

.field private loadFollowerTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;

.field private loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;

.field private loadRecentPlayersTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;

.field private loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;

.field private loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;

.field private onlineRecentPlayersCount:I

.field private peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

.field private queryText:Ljava/lang/String;

.field private recentPlayersData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private recommendationsData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private recommendationsListState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private recommendationsPreview:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private final searchGamertagLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private searchTask:Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

.field private final trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

.field private final userClubsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;-><init>()V

    .line 64
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->lastSelectedPosition:I

    .line 96
    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followersRecentChangeCount:I

    .line 97
    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onlineRecentPlayersCount:I

    .line 104
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->ALL:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/FilterPreferences;->get(Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 106
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPeopleAdapter(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 107
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchGamertagLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 109
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->linkFacebookViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->linkFacebookViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Z

    .line 112
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Z

    .line 115
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    .line 116
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->initListStates()V

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->userClubsList:Ljava/util/List;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->clubsAndPeopleList:Ljava/util/List;

    .line 120
    return-void
.end method

.method private static GetTrieInputs(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;",
            ">;"
        }
    .end annotation

    .prologue
    .line 802
    .local p0, "listItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 804
    .local v2, "trieInputs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;>;"
    if-eqz p0, :cond_1

    .line 805
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 806
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 807
    .local v1, "item":Lcom/microsoft/xbox/service/model/FollowersData;
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 808
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 809
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData;->getType()Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->Club:Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    if-eq v3, v4, :cond_0

    .line 810
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;

    iget-object v4, v1, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 805
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 815
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_1
    return-object v2
.end method

.method static synthetic access$1702(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isSearchGamertag:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onSearchGamertagCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onLoadFollowerCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$2002(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingFollowerData:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onLoadRecentPlayersCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$2202(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingRecentPlayersData:Z

    return p1
.end method

.method static synthetic access$2302(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingFollowingData:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onLoadRecommendationsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$2502(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingRecommendationsData:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingUserClubs:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingUserClubs:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onLoadUserClubsTaskCompleted(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->userClubsList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsPreview:Ljava/util/List;

    return-object v0
.end method

.method private buildClubsAndPeopleList()V
    .locals 3

    .prologue
    .line 683
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingFollowingData:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingUserClubs:Z

    if-eqz v1, :cond_1

    .line 693
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->clubsAndPeopleList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 688
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->clubsAndPeopleList:Ljava/util/List;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->userClubsList:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 689
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFavoritesAndFriends()Ljava/util/List;

    move-result-object v0

    .line 690
    .local v0, "favoritesAndFriends":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 691
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->clubsAndPeopleList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private buildRecommendationsPreviewList()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 843
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 844
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleHubRecommendationsData()Ljava/util/ArrayList;

    move-result-object v2

    .line 847
    .local v2, "newRecommendations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsData:Ljava/util/List;

    if-eq v4, v2, :cond_3

    .line 848
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsData:Ljava/util/List;

    .line 849
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsPreview:Ljava/util/List;

    .line 851
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsData:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 853
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->showLinkPhoneAccountButtonInSuggestions()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 855
    new-instance v1, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    sget-object v4, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LINK_TO_PHONE_CONTACT:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {v1, v7, v4}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    .line 856
    .local v1, "linkButton":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsData:Ljava/util/List;

    invoke-interface {v4, v6, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 858
    .end local v1    # "linkButton":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsData:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x4

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 859
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsPreview:Ljava/util/List;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsData:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 858
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 844
    .end local v0    # "i":I
    .end local v2    # "newRecommendations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 863
    .restart local v2    # "newRecommendations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsPreview:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 865
    new-instance v3, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    sget-object v4, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {v3, v7, v4}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    .line 866
    .local v3, "recommendationsExtra":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsPreview:Ljava/util/List;

    invoke-interface {v4, v6, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 869
    .end local v3    # "recommendationsExtra":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    :cond_3
    return-void
.end method

.method private cancelTasks()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchTask:Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchTask:Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;->cancel()V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadFollowerTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadFollowerTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;->cancel()V

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecentPlayersTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecentPlayersTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;->cancel()V

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;

    if-eqz v0, :cond_3

    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;->cancel()V

    .line 171
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;

    if-eqz v0, :cond_4

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->cancel()V

    .line 175
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;

    if-eqz v0, :cond_5

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;->cancel()V

    .line 178
    :cond_5
    return-void
.end method

.method private getFriendsListByFilter(Lcom/microsoft/xbox/service/model/FollowersFilter;)Ljava/util/List;
    .locals 3
    .param p1, "filter"    # Lcom/microsoft/xbox/service/model/FollowersFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/FollowersFilter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 362
    if-eqz p1, :cond_0

    .line 363
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersFilter:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 415
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "unknown filter type"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFavoritesAndFriends()Ljava/util/List;

    move-result-object v0

    .line 420
    :goto_0
    return-object v0

    .line 365
    :pswitch_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    .line 366
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$600(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_FRIENDS_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 367
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 368
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$800(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 369
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$900(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 370
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 371
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->NOT_SET:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 372
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 373
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 374
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1300(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 375
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1400(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 376
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1500(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 378
    :pswitch_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_CLUBS_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 379
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 380
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$800(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 381
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_FRIENDS_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 382
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 383
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$900(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 384
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 385
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->NOT_SET:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 386
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 387
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1400(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 388
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1500(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 390
    :pswitch_2
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_FRIENDS_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 391
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 392
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$900(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 393
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->NOT_SET:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 394
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 395
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1300(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 396
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1500(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 398
    :pswitch_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFollowers()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 400
    :pswitch_4
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_FRIENDS_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 401
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 402
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$900(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 403
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_CLUBS_HEADER:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 404
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 405
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$800(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 406
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 407
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1100(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->NOT_SET:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 408
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 409
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1300(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 410
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1400(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    .line 411
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->access$1500(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 413
    :pswitch_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getRecentPlayers()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 420
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 363
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method private getIsLoadingByFilter(Lcom/microsoft/xbox/service/model/FollowersFilter;)Z
    .locals 2
    .param p1, "filter"    # Lcom/microsoft/xbox/service/model/FollowersFilter;

    .prologue
    .line 343
    if-eqz p1, :cond_0

    .line 344
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersFilter:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 353
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "unknown filter type"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingFollowingData:Z

    .line 358
    :goto_0
    return v0

    .line 347
    :pswitch_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingFollowingData:Z

    goto :goto_0

    .line 349
    :pswitch_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingFollowerData:Z

    goto :goto_0

    .line 351
    :pswitch_2
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingRecentPlayersData:Z

    goto :goto_0

    .line 358
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 344
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getOfflineClubsCount()I
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->userClubsList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ListUtil;->countIf(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)I

    move-result v0

    return v0
.end method

.method private getOnlineClubsCount()I
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->userClubsList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$$Lambda$3;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ListUtil;->countIf(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)I

    move-result v0

    return v0
.end method

.method private initListStates()V
    .locals 3

    .prologue
    .line 679
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {}, Lcom/microsoft/xbox/service/model/FollowersFilter;->values()[Lcom/microsoft/xbox/service/model/FollowersFilter;

    move-result-object v1

    array-length v1, v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-static {v1, v2}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    .line 680
    return-void
.end method

.method private initializePeopleSearch()V
    .locals 2

    .prologue
    .line 792
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingFollowingData:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingUserClubs:Z

    if-eqz v1, :cond_1

    .line 799
    :cond_0
    :goto_0
    return-void

    .line 796
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->clubsAndPeopleList:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->GetTrieInputs(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 798
    .local v0, "trieInputs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/Search/TrieInput;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;->initialize(Ljava/util/List;)V

    goto :goto_0
.end method

.method static synthetic lambda$getOfflineClubsCount$2(Lcom/microsoft/xbox/service/model/FollowersData;)Z
    .locals 1
    .param p0, "club"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 499
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsOnline()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$navigateToProfile$0(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 249
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$updateOverride$1(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .prologue
    .line 272
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->load(Z)V

    return-void
.end method

.method private onLoadFollowerCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v0, 0x0

    .line 724
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onLoadFollowersCompleted"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingFollowerData:Z

    .line 727
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 729
    .local v0, "shouldSetErrorState":Z
    :cond_1
    if-eqz v0, :cond_3

    .line 730
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersFilter;->FOLLOWERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-interface {v1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 737
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateViewModelState()V

    .line 738
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 739
    return-void

    .line 731
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_2

    .line 732
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleHubFollowersData()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followerData:Ljava/util/List;

    .line 733
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowersRecentChangeCount()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followersRecentChangeCount:I

    .line 734
    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->FOLLOWERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateFriendsListState(Lcom/microsoft/xbox/service/model/FollowersFilter;)V

    goto :goto_0
.end method

.method private onLoadRecentPlayersCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v2, 0x0

    .line 742
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v4, "onLoadRecentPlayersCompleted"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingRecentPlayersData:Z

    .line 745
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v3, :cond_0

    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    .line 747
    .local v1, "shouldSetErrorState":Z
    :goto_0
    if-eqz v1, :cond_3

    .line 748
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    sget-object v3, Lcom/microsoft/xbox/service/model/FollowersFilter;->RECENTPLAYERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-interface {v2, v3, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 763
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateViewModelState()V

    .line 764
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 765
    return-void

    .end local v1    # "shouldSetErrorState":Z
    :cond_2
    move v1, v2

    .line 745
    goto :goto_0

    .line 749
    .restart local v1    # "shouldSetErrorState":Z
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v3, :cond_1

    .line 750
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRecentPlayersData()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recentPlayersData:Ljava/util/List;

    .line 752
    iput v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onlineRecentPlayersCount:I

    .line 753
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recentPlayersData:Ljava/util/List;

    if-eqz v2, :cond_5

    .line 754
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recentPlayersData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 755
    .local v0, "f":Lcom/microsoft/xbox/service/model/FollowersData;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsOnline()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 756
    iget v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onlineRecentPlayersCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onlineRecentPlayersCount:I

    goto :goto_2

    .line 760
    .end local v0    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_5
    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersFilter;->RECENTPLAYERS:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateFriendsListState(Lcom/microsoft/xbox/service/model/FollowersFilter;)V

    goto :goto_1
.end method

.method private onLoadRecommendationsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 819
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onLoadRecommendations Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingRecommendationsData:Z

    .line 822
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 835
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateViewModelState()V

    .line 836
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 837
    return-void

    .line 826
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->buildRecommendationsPreviewList()V

    .line 827
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateRecommendationsListState()V

    goto :goto_0

    .line 831
    :pswitch_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsPreview:Ljava/util/List;

    .line 832
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 822
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadUserClubsTaskCompleted(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 872
    .local p1, "userClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onLoadUserClubsTaskCompleted"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingUserClubs:Z

    .line 875
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->userClubsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 876
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 877
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 878
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->userClubsList:Ljava/util/List;

    new-instance v3, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-direct {v3, v0}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 882
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->buildClubsAndPeopleList()V

    .line 883
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->initializePeopleSearch()V

    .line 885
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateViewModelState()V

    .line 886
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 887
    return-void
.end method

.method private onSearchGamertagCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "gamertag"    # Ljava/lang/String;
    .param p3, "gamertagXuid"    # Ljava/lang/String;

    .prologue
    .line 696
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onSearchGamertagCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isSearchGamertag:Z

    .line 699
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 711
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 712
    return-void

    .line 703
    :pswitch_0
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->navigateToProfile(Ljava/lang/String;)V

    goto :goto_0

    .line 707
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to get search result for gamertag - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    const v0, 0x7f070573

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->showError(I)V

    goto :goto_0

    .line 699
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private search(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p2, "searchText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 540
    .local p1, "people":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 542
    .local v5, "results":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    if-eqz p1, :cond_0

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    if-eqz v8, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 594
    :cond_0
    return-object v5

    .line 546
    :cond_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    invoke-virtual {v8, p2}, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;->search(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 548
    .local v3, "matchingWords":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_0

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    iget-object v8, v8, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;->WordsDictionary:Ljava/util/Hashtable;

    if-eqz v8, :cond_0

    .line 549
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 552
    .local v2, "indexesSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 553
    .local v7, "word":Ljava/lang/String;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->trieSearch:Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;

    iget-object v9, v9, Lcom/microsoft/xbox/toolkit/ui/Search/TrieSearch;->WordsDictionary:Ljava/util/Hashtable;

    invoke-virtual {v9, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 554
    .local v1, "indexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-eqz v1, :cond_2

    .line 555
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 556
    .local v0, "index":Ljava/lang/Object;
    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "index":Ljava/lang/Object;
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 582
    .end local v1    # "indexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v7    # "word":Ljava/lang/String;
    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 583
    .local v6, "sortedIndexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v8, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SortedIndexComparator;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SortedIndexComparator;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    invoke-static {v6, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 587
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 588
    .local v0, "index":I
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 589
    .local v4, "person":Lcom/microsoft/xbox/service/model/FollowersData;
    new-instance v8, Lcom/microsoft/xbox/service/model/SearchResultPerson;

    invoke-direct {v8, v4, p2}, Lcom/microsoft/xbox/service/model/SearchResultPerson;-><init>(Lcom/microsoft/xbox/service/model/FollowersData;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/service/model/FollowersData;->setSearchResultPerson(Lcom/microsoft/xbox/service/model/SearchResultPerson;)V

    .line 590
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private showSuggestedPeopleSection()Z
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->ALL:Lcom/microsoft/xbox/service/model/FollowersFilter;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->FAVORITES:Lcom/microsoft/xbox/service/model/FollowersFilter;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->CLUB_FIRST:Lcom/microsoft/xbox/service/model/FollowersFilter;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->FRIENDS_FIRST:Lcom/microsoft/xbox/service/model/FollowersFilter;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateFriendsListState(Lcom/microsoft/xbox/service/model/FollowersFilter;)V
    .locals 5
    .param p1, "filter"    # Lcom/microsoft/xbox/service/model/FollowersFilter;

    .prologue
    .line 635
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 636
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFriendsListByFilter(Lcom/microsoft/xbox/service/model/FollowersFilter;)Ljava/util/List;

    move-result-object v0

    .line 637
    .local v0, "followers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getIsLoadingByFilter(Lcom/microsoft/xbox/service/model/FollowersFilter;)Z

    move-result v1

    .line 639
    .local v1, "isLoadingFriends":Z
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 640
    if-eqz v1, :cond_1

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 644
    .local v2, "newState":Lcom/microsoft/xbox/toolkit/network/ListState;
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v4

    invoke-interface {v3, v4, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 646
    .end local v0    # "followers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v1    # "isLoadingFriends":Z
    .end local v2    # "newState":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_0
    return-void

    .line 640
    .restart local v0    # "followers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .restart local v1    # "isLoadingFriends":Z
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 642
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .restart local v2    # "newState":Lcom/microsoft/xbox/toolkit/network/ListState;
    goto :goto_0
.end method

.method private updateRecommendationsListState()V
    .locals 1

    .prologue
    .line 649
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsPreview:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 650
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingRecommendationsData:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 654
    :goto_1
    return-void

    .line 650
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 652
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method


# virtual methods
.method public buildFilteredFriendList(Ljava/lang/String;)V
    .locals 4
    .param p1, "gamerSearchTag"    # Ljava/lang/String;

    .prologue
    .line 608
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->queryText:Ljava/lang/String;

    .line 610
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->clubsAndPeopleList:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->search(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->filteredFriendList:Ljava/util/List;

    .line 613
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->filteredFriendList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->filteredFriendList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->filteredFriendList:Ljava/util/List;

    new-instance v1, Lcom/microsoft/xbox/service/model/FollowersData;

    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_SEARCH_TAG:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_1

    if-eqz p1, :cond_1

    .line 618
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 620
    :cond_1
    return-void
.end method

.method public getFilter()Lcom/microsoft/xbox/service/model/FollowersFilter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 513
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    return-object v0
.end method

.method public getFilteredDescriptionText(Lcom/microsoft/xbox/service/model/FollowersData;)Ljava/lang/String;
    .locals 2
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 323
    if-nez p1, :cond_0

    .line 324
    const/4 v0, 0x0

    .line 333
    :goto_0
    return-object v0

    .line 327
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersFilter:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 333
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    goto :goto_0

    .line 329
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getFollowersTitleText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 331
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getRecentPlayerTitleText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 327
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getFilteredNoContentText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 298
    const v0, 0x7f07057c

    .line 300
    .local v0, "resId":I
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersFilter:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 315
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 302
    :pswitch_0
    const v0, 0x7f07057c

    .line 303
    goto :goto_0

    .line 305
    :pswitch_1
    const v0, 0x7f070579

    .line 306
    goto :goto_0

    .line 308
    :pswitch_2
    const v0, 0x7f07057b

    .line 309
    goto :goto_0

    .line 311
    :pswitch_3
    const v0, 0x7f070585

    goto :goto_0

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getFilteredPeople()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 426
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->queryText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->queryText:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getQueryResults(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 430
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getFriendsListByFilter(Lcom/microsoft/xbox/service/model/FollowersFilter;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getFilteredPeopleBasedOnSearchText()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->filteredFriendList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFilteredPeopleCountLabel()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const v5, 0x7f07057e

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 441
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isBusy()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 457
    :goto_0
    return-object v0

    .line 444
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersFilter:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 446
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOnlineFavoritesAndFriendsCount()I

    move-result v2

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOnlineClubsCount()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 449
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOnlineFavoritesAndFriendsCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 451
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOnlineFavoritesCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 453
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07057d

    new-array v2, v3, [Ljava/lang/Object;

    iget v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followersRecentChangeCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 455
    :pswitch_4
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOnlineRecentPlayersCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 444
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getFollowers()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 482
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followerData:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLastSelectedPosition()I
    .locals 1

    .prologue
    .line 517
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->lastSelectedPosition:I

    return v0
.end method

.method public getOfflinePeopleCountLabel()Ljava/lang/String;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const v5, 0x7f070583

    const v3, 0x7f070582

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 464
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 475
    :goto_0
    return-object v0

    .line 467
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$FollowersFilter:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 475
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 471
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOfflineFavoritesAndFriendsCount()I

    move-result v2

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOfflineClubsCount()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 473
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOfflineFavoritesCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v5, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 467
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getOnlineRecentPlayersCount()I
    .locals 1

    .prologue
    .line 491
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->onlineRecentPlayersCount:I

    return v0
.end method

.method public getQueryText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->queryText:Ljava/lang/String;

    return-object v0
.end method

.method public getRecentPlayers()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 487
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recentPlayersData:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTabletBackgroundColor()I
    .locals 2

    .prologue
    .line 319
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c00bc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public hideClubDiscoveryBlock()V
    .locals 2

    .prologue
    .line 261
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->saveHideClubDiscoveryBlock(Z)V

    .line 262
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 263
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isSearchGamertag:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getIsLoadingByFilter(Lcom/microsoft/xbox/service/model/FollowersFilter;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingRecommendationsData:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->linkFacebookViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    .line 138
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    .line 139
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 136
    :goto_0
    return v0

    .line 139
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 191
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->cancelTasks()V

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->linkFacebookViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->load(Z)V

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->load(Z)V

    .line 196
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;

    invoke-direct {v0, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadFollowingTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowingAsyncTask;->load(Z)V

    .line 199
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;

    invoke-direct {v0, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadFollowerTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;

    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadFollowerTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadFollowerAsyncTask;->load(Z)V

    .line 202
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;

    invoke-direct {v0, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecentPlayersTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecentPlayersTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecentPlayersAsyncTask;->load(Z)V

    .line 205
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;

    invoke-direct {v0, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadUserClubsAsyncTask;->load(Z)V

    .line 208
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecommendations(Z)V

    .line 209
    return-void
.end method

.method public loadRecommendations(Z)V
    .locals 2
    .param p1, "shouldRefresh"    # Z

    .prologue
    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->cancel()V

    .line 216
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecommendationsTask:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$LoadRecommendationsAsyncTask;->load(Z)V

    .line 218
    return-void
.end method

.method public navigateToClubDiscovery()V
    .locals 3

    .prologue
    .line 254
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 255
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreen;

    if-eq v1, v2, :cond_1

    .line 256
    :cond_0
    const-class v1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreen;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 258
    :cond_1
    return-void
.end method

.method public navigateToProfile(Ljava/lang/String;)V
    .locals 3
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 243
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 244
    const v2, 0x7f070573

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->showError(I)V

    .line 251
    :goto_0
    return-void

    .line 248
    :cond_0
    move-object v1, p0

    .line 249
    .local v1, "vm":Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    invoke-static {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v0

    .line 250
    .local v0, "postAnimateOutRunnable":Ljava/lang/Runnable;
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 124
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookCallbackManager()Lcom/facebook/CallbackManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookCallbackManager()Lcom/facebook/CallbackManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/CallbackManager;->onActivityResult(IILandroid/content/Intent;)Z

    .line 127
    :cond_0
    return-void
.end method

.method public onLoadFollowingCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v0, 0x0

    .line 769
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onLoadFollowing Completed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->isLoadingFollowingData:Z

    .line 772
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 774
    .local v0, "shouldSetErrorState":Z
    :cond_1
    if-eqz v0, :cond_3

    .line 775
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersFilter;->ALL:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-interface {v1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 776
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    sget-object v2, Lcom/microsoft/xbox/service/model/FollowersFilter;->FAVORITES:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-interface {v1, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 787
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateViewModelState()V

    .line 788
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 789
    return-void

    .line 777
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_2

    .line 778
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleHubFollowingData()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followingData:Ljava/util/ArrayList;

    .line 779
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->buildFavorites()V

    .line 780
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->buildFavoritesAndFriendsList()V

    .line 781
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->buildClubsAndPeopleList()V

    .line 782
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->initializePeopleSearch()V

    .line 783
    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->ALL:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateFriendsListState(Lcom/microsoft/xbox/service/model/FollowersFilter;)V

    .line 784
    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersFilter;->FAVORITES:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateFriendsListState(Lcom/microsoft/xbox/service/model/FollowersFilter;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 222
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onPause is called"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->cancelTasks()V

    .line 226
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->onPause()V

    .line 227
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 131
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPeopleAdapter(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 132
    return-void
.end method

.method public onSearchBarClear()V
    .locals 1

    .prologue
    .line 230
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->queryText:Ljava/lang/String;

    .line 231
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 232
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->queryText:Ljava/lang/String;

    .line 146
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 151
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->onStartOverride()V

    .line 152
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->cancelTasks()V

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 187
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 623
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->reset()V

    .line 624
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followerData:Ljava/util/List;

    .line 625
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recentPlayersData:Ljava/util/List;

    .line 626
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followingData:Ljava/util/ArrayList;

    .line 627
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsPreview:Ljava/util/List;

    .line 628
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->initListStates()V

    .line 629
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 630
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateViewModelState()V

    .line 631
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 632
    return-void
.end method

.method public searchGamertag(Ljava/lang/String;)V
    .locals 7
    .param p1, "gamerTag"    # Ljava/lang/String;

    .prologue
    .line 525
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/SearchHelper;->checkValidSearchTag(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 526
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "searchGamertag"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    :goto_0
    return-void

    .line 530
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Friend Find"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 532
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchTask:Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    if-eqz v0, :cond_1

    .line 533
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchTask:Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;->cancel()V

    .line 536
    :cond_1
    const/4 v1, 0x1

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchGamertagLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SearchGamertagRunner;

    invoke-direct {v6, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SearchGamertagRunner;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->searchTask:Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    goto :goto_0
.end method

.method public setFilter(Lcom/microsoft/xbox/service/model/FollowersFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/microsoft/xbox/service/model/FollowersFilter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 503
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 505
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    .line 506
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/FilterPreferences;->set(Ljava/lang/String;Ljava/lang/Enum;)Z

    .line 507
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateViewModelState()V

    .line 508
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 509
    return-void
.end method

.method public setLastSelectedPosition(I)V
    .locals 0
    .param p1, "lastSelectedPosition"    # I

    .prologue
    .line 521
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->lastSelectedPosition:I

    .line 522
    return-void
.end method

.method public showingPeopleSearchResult()Z
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->queryText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateAdapter()V
    .locals 1

    .prologue
    .line 716
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 717
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->forceUpdateViewImmediately()V

    .line 721
    :goto_0
    return-void

    .line 719
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->updateAdapter()V

    goto :goto_0
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 266
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    .line 268
    .local v2, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 295
    :goto_0
    return-void

    .line 272
    :pswitch_0
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0xc8

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 275
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getCurrentAction()Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    move-result-object v0

    .line 276
    .local v0, "currentAction":Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getResult()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v1

    .line 277
    .local v1, "newResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshPeopleHubRecommendationsProfile()Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v0, v3, :cond_2

    .line 280
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->buildRecommendationsList()V

    .line 282
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->buildRecommendationsPreviewList()V

    .line 284
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateRecommendationsListState()V

    .line 285
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateViewModelState()V

    .line 286
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->updateAdapter()V

    .line 292
    :cond_1
    :goto_1
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    goto :goto_0

    .line 287
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 289
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->loadRecommendations(Z)V

    goto :goto_1

    .line 268
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected updateViewModelState()V
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 658
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->listStates:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->peopleFilter:Lcom/microsoft/xbox/service/model/FollowersFilter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersFilter;->ordinal()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 663
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->showSuggestedPeopleSection()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 664
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 668
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->showSuggestedPeopleSection()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->recommendationsListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_1

    .line 670
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 676
    :cond_1
    :goto_0
    return-void

    .line 673
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 674
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "listStates is not init"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
