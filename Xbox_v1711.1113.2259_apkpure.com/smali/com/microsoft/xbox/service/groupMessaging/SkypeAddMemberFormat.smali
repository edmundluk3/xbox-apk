.class public Lcom/microsoft/xbox/service/groupMessaging/SkypeAddMemberFormat;
.super Ljava/lang/Object;
.source "SkypeAddMemberFormat.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    name = "addmember"
.end annotation


# instance fields
.field public eventtime:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "eventtime"
        required = false
    .end annotation
.end field

.field public initiator:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "initiator"
        required = false
    .end annotation
.end field

.field public target:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        entry = "target"
        inline = true
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
