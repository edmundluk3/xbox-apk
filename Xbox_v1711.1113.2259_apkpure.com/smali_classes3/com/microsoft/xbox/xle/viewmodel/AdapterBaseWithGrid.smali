.class public abstract Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "AdapterBaseWithGrid.java"


# static fields
.field protected static final GRID_VIEW_ANIMATION_NAME:Ljava/lang/String; = "GridView"


# instance fields
.field protected gridView:Landroid/widget/AbsListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->gridView:Landroid/widget/AbsListView;

    return-void
.end method

.method private saveListPosition()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v3

    .line 33
    .local v3, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->gridView:Landroid/widget/AbsListView;

    if-eqz v4, :cond_0

    .line 34
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->gridView:Landroid/widget/AbsListView;

    invoke-virtual {v4}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    .line 35
    .local v0, "index":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->gridView:Landroid/widget/AbsListView;

    invoke-virtual {v4, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 36
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_1

    .line 37
    .local v1, "offset":I
    :goto_0
    invoke-virtual {v3, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->setListPosition(II)V

    .line 39
    .end local v0    # "index":I
    .end local v1    # "offset":I
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    return-void

    .line 36
    .restart local v0    # "index":I
    .restart local v2    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method protected abstract getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
.end method

.method protected abstract getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 22
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 23
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 27
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->saveListPosition()V

    .line 29
    return-void
.end method

.method protected restorePosition()V
    .locals 3

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    .line 43
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->gridView:Landroid/widget/AbsListView;

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->gridView:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 48
    :cond_0
    return-void
.end method

.method protected setGridView(Lcom/microsoft/xbox/toolkit/ui/XLEGridView;)V
    .locals 1
    .param p1, "gridView"    # Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    .prologue
    .line 16
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 17
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->gridView:Landroid/widget/AbsListView;

    .line 18
    return-void

    .line 16
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScreenState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 59
    :cond_0
    return-void
.end method
