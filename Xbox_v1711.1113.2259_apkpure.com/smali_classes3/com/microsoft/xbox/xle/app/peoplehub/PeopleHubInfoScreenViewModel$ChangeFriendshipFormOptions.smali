.class public final enum Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;
.super Ljava/lang/Enum;
.source "PeopleHubInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChangeFriendshipFormOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

.field public static final enum ShouldAddUserToFavoriteList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

.field public static final enum ShouldAddUserToFriendList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

.field public static final enum ShouldAddUserToShareIdentityList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

.field public static final enum ShouldRemoveUserFromFavoriteList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

.field public static final enum ShouldRemoveUserFromFriendList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

.field public static final enum ShouldRemoveUserFromShareIdentityList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 84
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    const-string v1, "ShouldAddUserToFriendList"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldAddUserToFriendList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    .line 85
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    const-string v1, "ShouldRemoveUserFromFriendList"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldRemoveUserFromFriendList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    .line 86
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    const-string v1, "ShouldAddUserToFavoriteList"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldAddUserToFavoriteList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    .line 87
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    const-string v1, "ShouldRemoveUserFromFavoriteList"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldRemoveUserFromFavoriteList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    .line 88
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    const-string v1, "ShouldAddUserToShareIdentityList"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldAddUserToShareIdentityList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    .line 89
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    const-string v1, "ShouldRemoveUserFromShareIdentityList"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldRemoveUserFromShareIdentityList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    .line 83
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldAddUserToFriendList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldRemoveUserFromFriendList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldAddUserToFavoriteList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldRemoveUserFromFavoriteList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldAddUserToShareIdentityList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->ShouldRemoveUserFromShareIdentityList:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->$VALUES:[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->$VALUES:[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$ChangeFriendshipFormOptions;

    return-object v0
.end method
