.class public Lcom/microsoft/xbox/service/model/TitleModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$LikeCaptureRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$HeroStatComparator;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetTitleImagesRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360AchievementRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneAchievementRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;,
        Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;",
        ">;"
    }
.end annotation


# static fields
.field public static final KEY_OWNER_ID:Ljava/lang/String; = "OWNER_ID"

.field public static final KEY_SCID:Ljava/lang/String; = "SCID"

.field public static final KEY_SCREENSHOT_ID:Ljava/lang/String; = "SCREENSHOT_ID"

.field private static final MAX_COMPARE_ACHIEVEMENT_ENTRIES:I = 0xa

.field private static final MAX_TITLE_MODELS:I = 0x14

.field private static final TAG:Ljava/lang/String;

.field private static titleModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/TitleModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private captures:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;>;"
        }
    .end annotation
.end field

.field private capturesLoadingState:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;",
            ">;"
        }
    .end annotation
.end field

.field private compare360AchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;"
        }
    .end annotation
.end field

.field private compareAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;",
            ">;"
        }
    .end annotation
.end field

.field private compareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;"
        }
    .end annotation
.end field

.field private gameClips:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;>;"
        }
    .end annotation
.end field

.field private gameClipsLoadingState:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;",
            ">;"
        }
    .end annotation
.end field

.field private gameClipsMostViewed:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;"
        }
    .end annotation
.end field

.field private gameProgress360AchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private gameProgress360AchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

.field private gameProgress360EarnedAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private gameProgress360EarnedAchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

.field private gameProgressLimitedTimeChallengeLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private gameProgressLimitedTimeChallengeResult:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

.field private gameProgressXboxoneAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private gameProgressXboxoneAchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

.field private isLoadingGameClipsMostRecent:Z

.field private isLoadingGameClipsMostViews:Z

.field private lastRefreshCompare360Achievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private lastRefreshCompareAchievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private lastRefreshGameProgress360Achievements:Ljava/util/Date;

.field private lastRefreshGameProgress360EarnedAchievements:Ljava/util/Date;

.field private lastRefreshGameProgressLimitedTimeChallenge:Ljava/util/Date;

.field private lastRefreshGameProgressXboxoneAchievements:Ljava/util/Date;

.field private lastRefreshTitleImages:Ljava/util/Date;

.field private lastRefreshTitleStatistics:Ljava/util/Date;

.field private leaderboards:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/TimestampedObject",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;",
            ">;>;"
        }
    .end annotation
.end field

.field private orderedCompareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private rxGameProgress360Achievements:Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;"
        }
    .end annotation
.end field

.field private rxGameProgressXboxoneAchievements:Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;"
        }
    .end annotation
.end field

.field private storeCollectionItemLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private titleId:Ljava/lang/String;

.field private titleImageDetails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private titleImagesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private titlePurchaseInfoLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private titlePurchaseInfoResult:Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;

.field private titleStatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

.field private titleStatisticsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 85
    const-class v0, Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/TitleModel;->TAG:Ljava/lang/String;

    .line 120
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/TitleModel;->titleModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "titleId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0xa

    .line 147
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 92
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameClips:Ljava/util/Hashtable;

    .line 93
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->captures:Ljava/util/Hashtable;

    .line 94
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameClipsLoadingState:Ljava/util/Hashtable;

    .line 95
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->capturesLoadingState:Ljava/util/Hashtable;

    .line 101
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->isLoadingGameClipsMostRecent:Z

    .line 105
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->isLoadingGameClipsMostViews:Z

    .line 117
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->leaderboards:Ljava/util/Hashtable;

    .line 122
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 124
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compare360AchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 126
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompareAchievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 127
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompare360Achievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 129
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 131
    new-instance v0, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->orderedCompareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 148
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    .line 149
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressXboxoneAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 150
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360AchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 151
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360EarnedAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 152
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressLimitedTimeChallengeLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 153
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleStatisticsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 154
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleImagesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 155
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetLeaderBoardCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetTitleStatisticsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetGameClipsLoadCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetGameProgressXboxoneAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetGameProgressLimitedTimeChallengesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetXboxoneCompareAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/TitleModel;->onGet360CompareAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetGameProgress360AchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetGameProgress360EarnedAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetTitleImagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method public static getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 2
    .param p0, "titleId"    # J

    .prologue
    .line 158
    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public static getTitleModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 2
    .param p0, "titleId"    # Ljava/lang/String;

    .prologue
    .line 167
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 171
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/TitleModel;->titleModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/TitleModel;

    .line 172
    .local v0, "model":Lcom/microsoft/xbox/service/model/TitleModel;
    if-nez v0, :cond_1

    .line 173
    new-instance v0, Lcom/microsoft/xbox/service/model/TitleModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/TitleModel;
    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/TitleModel;-><init>(Ljava/lang/String;)V

    .line 174
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/TitleModel;
    sget-object v1, Lcom/microsoft/xbox/service/model/TitleModel;->titleModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p0, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 176
    :cond_1
    return-object v0
.end method

.method static synthetic lambda$rxLoadGameProgress360Achievements$4(Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "meXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 396
    sget-object v5, Lcom/microsoft/xbox/service/model/TitleModel;->TAG:Ljava/lang/String;

    const-string v6, "Fetching GameProgress360AchievementsResult"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    const/4 v3, 0x0

    .line 399
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    const/4 v4, 0x0

    .line 400
    .local v4, "temp":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    const/4 v1, 0x5

    .line 401
    .local v1, "maxTryCount":I
    const/4 v0, 0x0

    .line 403
    .local v0, "continuationToken":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-interface {v5, p1, v6, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgress360AchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    move-result-object v4

    .line 404
    if-eqz v4, :cond_0

    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 405
    if-nez v3, :cond_2

    .line 406
    move-object v3, v4

    .line 411
    :goto_1
    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    if-eqz v5, :cond_0

    .line 412
    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    .line 415
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    add-int/lit8 v2, v1, -0x1

    .end local v1    # "maxTryCount":I
    .local v2, "maxTryCount":I
    if-gtz v1, :cond_3

    move v1, v2

    .line 417
    .end local v2    # "maxTryCount":I
    .restart local v1    # "maxTryCount":I
    :cond_1
    return-object v3

    .line 409
    :cond_2
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    iget-object v6, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .end local v1    # "maxTryCount":I
    .restart local v2    # "maxTryCount":I
    :cond_3
    move v1, v2

    .end local v2    # "maxTryCount":I
    .restart local v1    # "maxTryCount":I
    goto :goto_0
.end method

.method static synthetic lambda$rxLoadGameProgress360Achievements$5(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "results"    # Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 420
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetGameProgress360AchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic lambda$rxLoadGameProgressAchievements$2(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;)Lio/reactivex/SingleSource;
    .locals 2
    .param p0, "result"    # Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 377
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$rxLoadGameProgressAchievements$3(Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;)Lio/reactivex/SingleSource;
    .locals 2
    .param p0, "result"    # Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 380
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$rxLoadGameProgressXboxoneAchievements$0(Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "meXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 298
    sget-object v3, Lcom/microsoft/xbox/service/model/TitleModel;->TAG:Ljava/lang/String;

    const-string v4, "Fetching GameProgressXboxoneAchievementsResult"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const/4 v0, 0x0

    .line 301
    .local v0, "continuationToken":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    .line 302
    invoke-interface {v3, p1, v4, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgressXboxoneAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v2

    .line 304
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;

    if-eqz v3, :cond_0

    .line 305
    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;

    iget-object v0, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    .line 306
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 307
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    .line 308
    invoke-interface {v3, p1, v4, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgressXboxoneAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v1

    .line 310
    .local v1, "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    if-nez v1, :cond_1

    .line 326
    .end local v1    # "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :cond_0
    return-object v2

    .line 314
    .restart local v1    # "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :cond_1
    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 315
    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 318
    :cond_2
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;

    if-eqz v3, :cond_3

    .line 319
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;

    iget-object v0, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    goto :goto_0

    .line 321
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$rxLoadGameProgressXboxoneAchievements$1(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p1, "result"    # Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 329
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetGameProgressXboxoneAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private onGet360CompareAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 3
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 636
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;>;"
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 637
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 638
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    .line 639
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompare360Achievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 640
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompare360Achievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, p2, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 641
    if-eqz v0, :cond_0

    .line 642
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compare360AchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 643
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compare360AchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p2, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 646
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    :cond_0
    return-void

    .line 636
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onGetCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 5
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 570
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->capturesLoadingState:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;

    .line 571
    .local v1, "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 572
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->isLoading:Z

    .line 573
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_0

    .line 574
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    .line 575
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->lastRefreshTimestamp:Ljava/util/Date;

    .line 576
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 577
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    :cond_0
    :goto_0
    return-void

    .line 578
    .restart local v0    # "data":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    :cond_1
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->screenshots:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 579
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->screenshots:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private onGetGameClipsLoadCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 5
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 585
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameClipsLoadingState:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;

    .line 586
    .local v1, "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 587
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->isLoading:Z

    .line 588
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_0

    .line 589
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    .line 590
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->lastRefreshTimestamp:Ljava/util/Date;

    .line 591
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 592
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameClips:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;->gameClips:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    :cond_0
    return-void
.end method

.method private onGetGameProgress360AchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 649
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 650
    sget-object v1, Lcom/microsoft/xbox/service/model/TitleModel;->TAG:Ljava/lang/String;

    const-string v2, "Got 360 achievements"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    .line 652
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgress360Achievements:Ljava/util/Date;

    .line 653
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360AchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    .line 655
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    :cond_0
    return-void
.end method

.method private onGetGameProgress360EarnedAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 658
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 659
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    .line 660
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgress360EarnedAchievements:Ljava/util/Date;

    .line 661
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360EarnedAchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    .line 663
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    :cond_0
    return-void
.end method

.method private onGetGameProgressLimitedTimeChallengesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 607
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 608
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    .line 609
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgressLimitedTimeChallenge:Ljava/util/Date;

    .line 610
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressLimitedTimeChallengeResult:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    .line 613
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :cond_0
    return-void
.end method

.method private onGetGameProgressXboxoneAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 598
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 599
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    .line 600
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgressXboxoneAchievements:Ljava/util/Date;

    .line 601
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressXboxoneAchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    .line 604
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :cond_0
    return-void
.end method

.method private onGetLeaderBoardCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 686
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_0

    .line 687
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    .line 689
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    if-eqz v0, :cond_0

    .line 690
    new-instance v1, Lcom/microsoft/xbox/service/model/TimestampedObject;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/service/model/TimestampedObject;-><init>(Ljava/lang/Object;)V

    .line 691
    .local v1, "entry":Lcom/microsoft/xbox/service/model/TimestampedObject;, "Lcom/microsoft/xbox/service/model/TimestampedObject<Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->leaderboards:Ljava/util/Hashtable;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->statName:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 694
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    .end local v1    # "entry":Lcom/microsoft/xbox/service/model/TimestampedObject;, "Lcom/microsoft/xbox/service/model/TimestampedObject<Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;>;"
    :cond_0
    return-void
.end method

.method private onGetTitleImagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 676
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 677
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 678
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshTitleImages:Ljava/util/Date;

    .line 679
    if-eqz v0, :cond_0

    .line 680
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleImageDetails:Ljava/util/ArrayList;

    .line 683
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_0
    return-void
.end method

.method private onGetTitlePurchaseInfoCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 616
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 617
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;

    .line 618
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titlePurchaseInfoResult:Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;

    .line 620
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;
    :cond_0
    return-void
.end method

.method private onGetTitleStatisticsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 666
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 667
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    .line 668
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshTitleStatistics:Ljava/util/Date;

    .line 669
    if-eqz v0, :cond_0

    .line 670
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleStatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    .line 673
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    :cond_0
    return-void
.end method

.method private onGetXboxoneCompareAchievementsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V
    .locals 3
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 623
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;>;"
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 624
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 625
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    .line 626
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompareAchievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 627
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompareAchievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, p2, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 628
    if-eqz v0, :cond_0

    .line 629
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 630
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, p2, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 633
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :cond_0
    return-void

    .line 623
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static reset()V
    .locals 3

    .prologue
    .line 180
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 181
    sget-object v1, Lcom/microsoft/xbox/service/model/TitleModel;->titleModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/microsoft/xbox/service/model/TitleModel;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 182
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TitleModel;->clearObservers()V

    goto :goto_1

    .line 180
    .end local v0    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/microsoft/xbox/service/model/TitleModel;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 185
    .restart local v0    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/microsoft/xbox/service/model/TitleModel;>;"
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;-><init>(I)V

    sput-object v1, Lcom/microsoft/xbox/service/model/TitleModel;->titleModelCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    .line 186
    return-void
.end method


# virtual methods
.method public getCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCompare360AchievementData(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compare360AchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compare360AchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    .line 222
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCompareAchievementData(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    .line 214
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGameClipMostViewed()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameClipsMostViewed:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGameClips(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameClips:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getGameProgress360Achievements()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360AchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360AchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGameProgress360EarnedAchievements()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360EarnedAchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360EarnedAchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGameProgressLimitedTimeChallenge()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressLimitedTimeChallengeResult:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressLimitedTimeChallengeResult:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGameProgressXboxoneAchievements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressXboxoneAchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressXboxoneAchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsLoadingCompareAchievements(Ljava/lang/String;)Z
    .locals 2
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 498
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 499
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v1

    :cond_0
    return v1

    :cond_1
    move v0, v1

    .line 498
    goto :goto_0
.end method

.method public getIsLoadingGameClipsMostRecent()Z
    .locals 1

    .prologue
    .line 486
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->isLoadingGameClipsMostRecent:Z

    return v0
.end method

.method public getIsLoadingGameClipsMostViews()Z
    .locals 1

    .prologue
    .line 470
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->isLoadingGameClipsMostViews:Z

    return v0
.end method

.method public getIsLoadingGameProgressLimitedTimeChallenges()Z
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressLimitedTimeChallengeLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public getIsLoadingTitleImages()Z
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleImagesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public getIsLoadingTitlePurhcaseInfo()Z
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleStatisticsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public getIsLoadingTitleStatistics()Z
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleStatisticsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public getIsLoadingXboxoneGameProgressAchievements()Z
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressXboxoneAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public getLeaderBoard(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    .locals 2
    .param p1, "statName"    # Ljava/lang/String;

    .prologue
    .line 238
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->leaderboards:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/TimestampedObject;

    .line 239
    .local v0, "entry":Lcom/microsoft/xbox/service/model/TimestampedObject;, "Lcom/microsoft/xbox/service/model/TimestampedObject<Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;>;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TimestampedObject;->data:Ljava/lang/Object;

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;

    goto :goto_0
.end method

.method public getOrderedCompareAchievementList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->orderedCompareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTitleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleImageDetailsData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleImageDetails:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleImageDetails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleImageDetails:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 234
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitlePurchaseInfo()Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titlePurchaseInfoResult:Lcom/microsoft/xbox/service/network/managers/TitlePurchaseInfoResultContainer$TitlePurchaseInfoResult;

    return-object v0
.end method

.method public getTitleStatisticsData()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleStatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    return-object v0
.end method

.method public likeCapture(ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "currLikeState"    # Z
    .param p2, "capture"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .param p3, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1516
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    .line 1517
    .local v5, "status":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIsScreenshot()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1518
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    if-eqz p1, :cond_0

    const-string v0, "Screenshot Like"

    :goto_0
    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 1523
    :goto_1
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$LikeCaptureRunner;

    invoke-direct {v6, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/TitleModel$LikeCaptureRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0

    .line 1518
    :cond_0
    const-string v0, "Screenshot Un-Like"

    goto :goto_0

    .line 1520
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    if-eqz p1, :cond_2

    const-string v0, "Game DVR Like"

    :goto_2
    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v0, "Game DVR Un-Like"

    goto :goto_2
.end method

.method public likeGameClip(ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "currLikeState"    # Z
    .param p2, "clip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .param p3, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1510
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    .line 1511
    .local v5, "status":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    if-eqz p1, :cond_0

    const-string v0, "Game DVR Like"

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 1512
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;

    invoke-direct {v6, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;Ljava/lang/String;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0

    .line 1511
    :cond_0
    const-string v0, "Game DVR Un-Like"

    goto :goto_0
.end method

.method public loadCaptures(ZLcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 11
    .param p1, "forceRefresh"    # Z
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->capturesLoadingState:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;

    .line 267
    .local v7, "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    if-nez v7, :cond_0

    .line 268
    new-instance v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;

    .end local v7    # "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    const/4 v0, 0x0

    invoke-direct {v7, p0, v0}, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel$1;)V

    .line 269
    .restart local v7    # "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->capturesLoadingState:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->isLoading:Z

    .line 272
    iget-wide v8, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v6, v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->lastRefreshTimestamp:Ljava/util/Date;

    iget-object v10, v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->loadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    .line 273
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    move-object v2, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;Ljava/lang/String;)V

    move v1, p1

    move-wide v2, v8

    move-object v4, v6

    move-object v5, v10

    move-object v6, v0

    .line 272
    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadGameClipSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 7
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;"
        }
    .end annotation

    .prologue
    .line 1614
    .local p1, "clipData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    .line 1615
    .local v5, "status":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;

    invoke-direct {v6, p0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method public loadGameClips(ZLcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 11
    .param p1, "forceRefresh"    # Z
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameClipsLoadingState:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;

    .line 256
    .local v7, "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    if-nez v7, :cond_0

    .line 257
    new-instance v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;

    .end local v7    # "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    const/4 v0, 0x0

    invoke-direct {v7, p0, v0}, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel$1;)V

    .line 258
    .restart local v7    # "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameClipsLoadingState:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->isLoading:Z

    .line 261
    iget-wide v8, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v6, v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->lastRefreshTimestamp:Ljava/util/Date;

    iget-object v10, v7, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->loadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    .line 262
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    move-object v2, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameClipRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;Ljava/lang/String;)V

    move v1, p1

    move-wide v2, v8

    move-object v4, v6

    move-object v5, v10

    move-object v6, v0

    .line 261
    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadGameProgress360Achievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 427
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 428
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 429
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 430
    .local v0, "meXuid":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 432
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgress360Achievements:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360AchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360AchievementRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-direct {v6, p0, p0, v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360AchievementRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadGameProgress360CompareAchievements(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 349
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 350
    move-object v0, p2

    .line 351
    .local v0, "compareStatisticsCacheKey":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 352
    .local v5, "compareStatisticsLoadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    if-nez v5, :cond_0

    .line 353
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .end local v5    # "compareStatisticsLoadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    .line 354
    .restart local v5    # "compareStatisticsLoadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v0, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 357
    :cond_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompare360Achievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Date;

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-direct {v6, p0, p0, p2, v1}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadGameProgress360EarnedAchievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 437
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 438
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 439
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 440
    .local v0, "meXuid":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 442
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgress360EarnedAchievements:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360EarnedAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-direct {v6, p0, p0, v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360EarnedAchievementRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadGameProgressLimitedTimeChallenge(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 362
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 363
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 364
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 365
    .local v0, "meXuid":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 367
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgressLimitedTimeChallenge:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressLimitedTimeChallengeLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-direct {v6, p0, p0, v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadGameProgressXboxoneAchievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 278
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 279
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 280
    .local v0, "meXuid":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 282
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgressXboxoneAchievements:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressXboxoneAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneAchievementRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-direct {v6, p0, p0, v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneAchievementRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadGameProgressXboxoneCompareAchievements(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 337
    move-object v0, p2

    .line 338
    .local v0, "compareStatisticsCacheKey":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 339
    .local v5, "compareStatisticsLoadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    if-nez v5, :cond_0

    .line 340
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .end local v5    # "compareStatisticsLoadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    .line 341
    .restart local v5    # "compareStatisticsLoadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->compareAchievementLoadingStatus:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v0, v5}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 344
    :cond_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompareAchievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Date;

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-direct {v6, p0, p0, p2, v1}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadLeaderBoard(ZLcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .param p2, "stat"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 461
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 462
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 463
    iget-object v1, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 464
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->leaderboards:Ljava/util/Hashtable;

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/TimestampedObject;

    .line 465
    .local v0, "entry":Lcom/microsoft/xbox/service/model/TimestampedObject;, "Lcom/microsoft/xbox/service/model/TimestampedObject<Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;>;"
    if-nez v0, :cond_0

    const/4 v4, 0x0

    .line 466
    .local v4, "lastRefresh":Ljava/util/Date;
    :goto_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;

    invoke-direct {v6, p0, p2}, Lcom/microsoft/xbox/service/model/TitleModel$GetLeaderBoardRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1

    .line 465
    .end local v4    # "lastRefresh":Ljava/util/Date;
    :cond_0
    iget-object v4, v0, Lcom/microsoft/xbox/service/model/TimestampedObject;->timestamp:Ljava/util/Date;

    goto :goto_0
.end method

.method public loadTitleImages(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 455
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 456
    .local v0, "titleIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshTitleImages:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleImagesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleImagesRunner;

    invoke-direct {v6, p0, p0, v0}, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleImagesRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/util/ArrayList;)V

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public loadTitleStatistics(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 4
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->loadTitleStatisticsForUser(ZLjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadTitleStatisticsForUser(ZLjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 10
    .param p1, "forceRefresh"    # Z
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 451
    .local p3, "statsToGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshTitleStatistics:Ljava/util/Date;

    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleStatisticsLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel;->titleId:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    move v1, p1

    move-wide v2, v6

    move-object v4, v8

    move-object v5, v9

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public onGetCaptureSocialCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 3
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;>;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1700
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 1701
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1702
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->captures:Ljava/util/Hashtable;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1704
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    :cond_0
    return-void
.end method

.method public rxLoadGameProgress360Achievements(Z)Lio/reactivex/Single;
    .locals 3
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 385
    sget-object v1, Lcom/microsoft/xbox/service/model/TitleModel;->TAG:Ljava/lang/String;

    const-string v2, "rxLoadGameProgress360Achievements"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefresh360GameProgressAchievements()Z

    move-result v1

    if-nez v1, :cond_1

    .line 388
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->rxGameProgress360Achievements:Lio/reactivex/Single;

    if-nez v1, :cond_0

    .line 389
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgress360AchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->rxGameProgress360Achievements:Lio/reactivex/Single;

    .line 423
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->rxGameProgress360Achievements:Lio/reactivex/Single;

    return-object v1

    .line 392
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 393
    .local v0, "meXuid":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 395
    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/model/TitleModel$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v1

    .line 419
    invoke-virtual {v1}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/TitleModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/service/model/TitleModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 420
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->rxGameProgress360Achievements:Lio/reactivex/Single;

    goto :goto_0
.end method

.method public rxLoadGameProgressAchievements(ZLcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)Lio/reactivex/Single;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2, "titleData"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 373
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 375
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->rxLoadGameProgressXboxoneAchievements(Z)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleModel$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 377
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 379
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->rxLoadGameProgress360Achievements(Z)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleModel$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 380
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0
.end method

.method public rxLoadGameProgressXboxoneAchievements(Z)Lio/reactivex/Single;
    .locals 3
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    sget-object v1, Lcom/microsoft/xbox/service/model/TitleModel;->TAG:Ljava/lang/String;

    const-string v2, "rxLoadGameProgressXboxoneAchievements"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshXboxoneGameProgressAchievements()Z

    move-result v1

    if-nez v1, :cond_1

    .line 290
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->rxGameProgressXboxoneAchievements:Lio/reactivex/Single;

    if-nez v1, :cond_0

    .line 291
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameProgressXboxoneAchievementResult:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->rxGameProgressXboxoneAchievements:Lio/reactivex/Single;

    .line 332
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->rxGameProgressXboxoneAchievements:Lio/reactivex/Single;

    return-object v1

    .line 294
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 295
    .local v0, "meXuid":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 297
    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/model/TitleModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v1

    .line 328
    invoke-virtual {v1}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/TitleModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/model/TitleModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 329
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->rxGameProgressXboxoneAchievements:Lio/reactivex/Single;

    goto :goto_0
.end method

.method public setOrderedCompareAchievementList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 565
    .local p2, "orderedCompareAchievementList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->orderedCompareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 566
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->orderedCompareAchievementsCache:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 567
    return-void
.end method

.method public shouldRefresh360GameProgressAchievements()Z
    .locals 4

    .prologue
    .line 545
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgress360Achievements:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public shouldRefresh360GameProgressEarnedAchievements()Z
    .locals 4

    .prologue
    .line 549
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgress360EarnedAchievements:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public shouldRefreshCaptures(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Z
    .locals 4
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .prologue
    .line 512
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->capturesLoadingState:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;

    .line 513
    .local v0, "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    if-eqz v0, :cond_0

    .line 514
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->lastRefreshTimestamp:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v1

    .line 516
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public shouldRefreshCompare360Achievements(Ljava/lang/String;)Z
    .locals 4
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 526
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 527
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompare360Achievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0

    .line 526
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldRefreshCompareAchievements(Ljava/lang/String;)Z
    .locals 4
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 521
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 522
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshCompareAchievements:Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0

    .line 521
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldRefreshGameClips(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Z
    .locals 4
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .prologue
    .line 503
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel;->gameClipsLoadingState:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;

    .line 504
    .local v0, "state":Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;
    if-eqz v0, :cond_0

    .line 505
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TitleModel$LoadingState;->lastRefreshTimestamp:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v1

    .line 507
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public shouldRefreshGameProgressLimitedTimeChallenges()Z
    .locals 4

    .prologue
    .line 553
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgressLimitedTimeChallenge:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public shouldRefreshGameProgressTitleImages()Z
    .locals 4

    .prologue
    .line 557
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshTitleImages:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public shouldRefreshLeaderBoard(Ljava/lang/String;)Z
    .locals 4
    .param p1, "statName"    # Ljava/lang/String;

    .prologue
    .line 539
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->leaderboards:Ljava/util/Hashtable;

    invoke-virtual {v2, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/TimestampedObject;

    .line 540
    .local v0, "entry":Lcom/microsoft/xbox/service/model/TimestampedObject;, "Lcom/microsoft/xbox/service/model/TimestampedObject<Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;>;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 541
    .local v1, "lastRefresh":Ljava/util/Date;
    :goto_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v2

    return v2

    .line 540
    .end local v1    # "lastRefresh":Ljava/util/Date;
    :cond_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/TimestampedObject;->timestamp:Ljava/util/Date;

    goto :goto_0
.end method

.method public shouldRefreshTitleStatistics()Z
    .locals 4

    .prologue
    .line 535
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshTitleStatistics:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public shouldRefreshXboxoneGameProgressAchievements()Z
    .locals 4

    .prologue
    .line 531
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lastRefreshGameProgressXboxoneAchievements:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method
