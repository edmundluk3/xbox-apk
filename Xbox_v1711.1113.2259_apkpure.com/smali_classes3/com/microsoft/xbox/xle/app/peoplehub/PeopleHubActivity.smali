.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "PeopleHubActivity.java"


# instance fields
.field private content:Ljava/lang/String;

.field private contentKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "People Hub"

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getContentKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->contentKey:Ljava/lang/String;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 18
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 20
    const v2, 0x7f0301c3

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->setContentView(I)V

    .line 22
    const v2, 0x7f0e090f

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .local v1, "pivotWithTabs":Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 24
    if-eqz v1, :cond_0

    .line 25
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackHubPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->TAB_NAMES:[Ljava/lang/String;

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->ACTION_NAMES:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTelemetryTabSelectCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 26
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackHubPageResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->TAB_NAMES:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setTelemetrySetActiveTabCallback(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;)V

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 30
    .local v0, "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 31
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferredTertiaryColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setSelectedTabIndicatorColor(I)V

    .line 35
    .end local v0    # "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 37
    new-instance v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedProfile()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->content:Ljava/lang/String;

    .line 41
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->content:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 42
    const-string v2, "TargetXuid"

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->contentKey:Ljava/lang/String;

    .line 43
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->content:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->setTargetXUID(Ljava/lang/String;)V

    .line 48
    :goto_0
    return-void

    .line 45
    :cond_1
    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;->contentKey:Ljava/lang/String;

    .line 46
    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->setTargetXUID(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateContentView()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method
