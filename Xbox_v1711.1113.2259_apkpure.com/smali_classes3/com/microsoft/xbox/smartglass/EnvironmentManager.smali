.class public Lcom/microsoft/xbox/smartglass/EnvironmentManager;
.super Lcom/microsoft/xbox/smartglass/Listenable;
.source "EnvironmentManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/Listenable",
        "<",
        "Lcom/microsoft/xbox/smartglass/EnvironmentManagerListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final _pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>(J)V
    .locals 5
    .param p1, "pPlatform"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/Listenable;-><init>()V

    .line 12
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->initialize(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 13
    return-void
.end method

.method private native getEnvironmentSettings(J)Lcom/microsoft/xbox/smartglass/EnvironmentSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native initialize(J)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private onEnvironmentChanged(Ljava/lang/String;IJ)V
    .locals 5
    .param p1, "xliveAudienceUri"    # Ljava/lang/String;
    .param p2, "currentEnvironment"    # I
    .param p3, "pEnvironmentSettings"    # J

    .prologue
    .line 43
    new-instance v1, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;-><init>(Ljava/lang/String;IJ)V

    .line 45
    .local v1, "settings":Lcom/microsoft/xbox/smartglass/EnvironmentSettings;
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/EnvironmentManagerListener;

    .line 46
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/EnvironmentManagerListener;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/EnvironmentManagerListener;->onEnvironmentChanged(Lcom/microsoft/xbox/smartglass/EnvironmentSettings;)V

    goto :goto_0

    .line 48
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/EnvironmentManagerListener;
    :cond_0
    return-void
.end method

.method private native setEnvironment(JI)V
.end method


# virtual methods
.method public getEnvironment()Lcom/microsoft/xbox/smartglass/Environment;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->getSettings()Lcom/microsoft/xbox/smartglass/EnvironmentSettings;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->currentEnvironment:Lcom/microsoft/xbox/smartglass/Environment;

    return-object v0
.end method

.method public getSettings()Lcom/microsoft/xbox/smartglass/EnvironmentSettings;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->getEnvironmentSettings(J)Lcom/microsoft/xbox/smartglass/EnvironmentSettings;

    move-result-object v0

    return-object v0
.end method

.method public setEnvironment(Lcom/microsoft/xbox/smartglass/Environment;)V
    .locals 3
    .param p1, "env"    # Lcom/microsoft/xbox/smartglass/Environment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/Environment;->getValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->setEnvironment(JI)V

    .line 39
    return-void
.end method
