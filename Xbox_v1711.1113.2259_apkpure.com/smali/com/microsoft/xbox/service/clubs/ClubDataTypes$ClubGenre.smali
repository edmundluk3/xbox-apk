.class public final enum Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;
.super Ljava/lang/Enum;
.source "ClubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClubGenre"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

.field public static final enum Social:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "social"
    .end annotation
.end field

.field public static final enum Title:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "title"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 87
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    const-string v1, "Social"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Social:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    .line 89
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    const-string v1, "Title"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Title:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    .line 86
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Social:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Title:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 86
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    return-object v0
.end method
