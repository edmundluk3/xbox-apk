.class public Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "TvHubTabletScreenViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 13
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvHubTabletAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 14
    return-void
.end method

.method private getCurrentScreen()Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    .locals 2

    .prologue
    .line 55
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;

    if-eqz v1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;

    .line 57
    .local v0, "tvAdapter":Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;->getCurrentScreen()Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    move-result-object v1

    .line 59
    .end local v0    # "tvAdapter":Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProfilePicUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;->getCurrentScreen()Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    move-result-object v0

    .line 46
    .local v0, "base":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    const-string v1, "EPG"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Hub: load  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 50
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceRefresh()V

    .line 52
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 26
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvHubTabletAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 27
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method
