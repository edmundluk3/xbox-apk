.class public Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;
.super Lcom/microsoft/xbox/presentation/base/BaseRxDialog;
.source "FriendPickerDialog.java"


# instance fields
.field resultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field

.field private view:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I
    .param p3, "titleStringResId"    # I
    .param p4, "choiceMode"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/BaseRxDialog;-><init>(Landroid/content/Context;I)V

    .line 21
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->resultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 30
    invoke-direct {p0, p1, p3, p4}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->init(Landroid/content/Context;ILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleStringResId"    # I
    .param p3, "choiceMode"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/base/BaseRxDialog;-><init>(Landroid/content/Context;)V

    .line 21
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->resultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->init(Landroid/content/Context;ILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;ILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cancelable"    # Z
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;
    .param p4, "titleStringResId"    # I
    .param p5, "choiceMode"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/base/BaseRxDialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 21
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->resultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 35
    invoke-direct {p0, p1, p4, p5}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->init(Landroid/content/Context;ILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;)V

    .line 36
    return-void
.end method

.method private init(Landroid/content/Context;ILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleStringResId"    # I
    .param p3, "choiceMode"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;

    .prologue
    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->requestWindowFeature(I)Z

    .line 40
    new-instance v0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->resultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog$$Lambda$1;->lambdaFactory$(Lcom/jakewharton/rxrelay2/PublishRelay;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v5

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewImpl$ChoiceMode;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->view:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerView;

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->view:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerView;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->setContentView(Landroid/view/View;)V

    .line 42
    return-void
.end method

.method static synthetic lambda$onAttachedToWindow$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Z
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    instance-of v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$OkIntent;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerViewIntents$CancelIntent;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onAttachedToWindow$1(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;
    .param p1, "ignore"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->dismiss()V

    return-void
.end method


# virtual methods
.method public activate()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->show()V

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->resultRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 5

    .prologue
    .line 51
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/BaseRxDialog;->onAttachedToWindow()V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;->view:Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerView;

    .line 54
    invoke-interface {v1}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerView;->viewIntents()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v2

    .line 55
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x64

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 60
    invoke-virtual {v1, v2, v3, v4}, Lio/reactivex/Observable;->delay(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/presentation/friendpicker/FriendPickerDialog;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 62
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 64
    return-void
.end method
