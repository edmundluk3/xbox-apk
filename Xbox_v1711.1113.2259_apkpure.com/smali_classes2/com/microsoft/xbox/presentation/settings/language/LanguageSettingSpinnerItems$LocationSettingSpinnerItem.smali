.class public abstract Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
.super Ljava/lang/Object;
.source "LanguageSettingSpinnerItems.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LocationSettingSpinnerItem"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;
    .locals 1
    .param p0, "location"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 57
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 58
    new-instance v0, Lcom/microsoft/xbox/presentation/settings/language/AutoValue_LanguageSettingSpinnerItems_LocationSettingSpinnerItem;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/settings/language/AutoValue_LanguageSettingSpinnerItems_LocationSettingSpinnerItem;-><init>(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)V

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 64
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 66
    .local v1, "resources":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;->location()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->DefaultMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-ne v3, v4, :cond_0

    .line 67
    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsUtils;->getSystemConfiguredLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v2

    .line 68
    .local v2, "systemLocation":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    const v3, 0x7f070b82

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "defaultString":Ljava/lang/String;
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->getDisplayResourceId()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 72
    .end local v0    # "defaultString":Ljava/lang/String;
    .end local v2    # "systemLocation":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingSpinnerItems$LocationSettingSpinnerItem;->location()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->getDisplayResourceId()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract location()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
