.class public Lcom/microsoft/xbox/service/network/managers/friendfinder/SmsBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SmsBroadcastReceiver.java"


# static fields
.field public static final SMS_BUNDLE:Ljava/lang/String; = "pdus"

.field private static SMS_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "\\b[0-9]{4}\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/SmsBroadcastReceiver;->SMS_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 22
    .local v1, "intentExtras":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 23
    const-string v7, "pdus"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Object;

    move-object v4, v7

    check-cast v4, [Ljava/lang/Object;

    .line 24
    .local v4, "sms":[Ljava/lang/Object;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v7, v4

    if-ge v0, v7, :cond_1

    .line 25
    aget-object v7, v4, v0

    check-cast v7, [B

    check-cast v7, [B

    invoke-static {v7}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v5

    .line 26
    .local v5, "smsMessage":Landroid/telephony/SmsMessage;
    if-eqz v5, :cond_0

    .line 27
    invoke-virtual {v5}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v3

    .line 28
    .local v3, "msgBody":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 29
    sget-object v7, Lcom/microsoft/xbox/service/network/managers/friendfinder/SmsBroadcastReceiver;->SMS_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 30
    .local v2, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 31
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    .line 32
    .local v6, "token":Ljava/lang/String;
    new-instance v7, Lcom/microsoft/xbox/service/network/managers/friendfinder/SmsBroadcastReceiver$1;

    invoke-direct {v7, p0, v6}, Lcom/microsoft/xbox/service/network/managers/friendfinder/SmsBroadcastReceiver$1;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/SmsBroadcastReceiver;Ljava/lang/String;)V

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 24
    .end local v2    # "matcher":Ljava/util/regex/Matcher;
    .end local v3    # "msgBody":Ljava/lang/String;
    .end local v6    # "token":Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    .end local v0    # "i":I
    .end local v4    # "sms":[Ljava/lang/Object;
    .end local v5    # "smsMessage":Landroid/telephony/SmsMessage;
    :cond_1
    return-void
.end method
