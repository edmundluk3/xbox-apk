.class final Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;
.super Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;
.source "AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent.java"


# instance fields
.field private final bytes:Lokio/ByteString;


# direct methods
.method constructor <init>(Lokio/ByteString;)V
    .locals 0
    .param p1, "bytes"    # Lokio/ByteString;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;->bytes:Lokio/ByteString;

    .line 16
    return-void
.end method


# virtual methods
.method public bytes()Lokio/ByteString;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;->bytes:Lokio/ByteString;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    if-ne p1, p0, :cond_1

    .line 40
    :cond_0
    :goto_0
    return v1

    .line 36
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;

    .line 38
    .local v0, "that":Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;->bytes:Lokio/ByteString;

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;->bytes()Lokio/ByteString;

    move-result-object v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;->bytes:Lokio/ByteString;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;->bytes()Lokio/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/toolkit/rx/RxWebSocketDataTypes$RxWebSocketByteStringEvent;
    :cond_3
    move v1, v2

    .line 40
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x1

    .line 46
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;->bytes:Lokio/ByteString;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 48
    return v0

    .line 47
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;->bytes:Lokio/ByteString;

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RxWebSocketByteStringEvent{bytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/rx/AutoValue_RxWebSocketDataTypes_RxWebSocketByteStringEvent;->bytes:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
