.class public Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "GameProfileFriendsWhoPlayModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameProfileFriendsWhoPlayDataModel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/FollowersData;",
        ">;>;"
    }
.end annotation


# instance fields
.field private broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

.field private gameTitleId:J

.field private result:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "gameTitleId"    # J

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->xuid:Ljava/lang/String;

    .line 67
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->gameTitleId:J

    .line 68
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->xuid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->gameTitleId:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;Lcom/microsoft/xbox/service/model/FollowersData;)Lcom/microsoft/xbox/service/model/FollowersData;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    return-object p1
.end method


# virtual methods
.method public getBroadcaster()Lcom/microsoft/xbox/service/model/FollowersData;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->broadcaster:Lcom/microsoft/xbox/service/model/FollowersData;

    return-object v0
.end method

.method public getResult()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->result:Ljava/util/ArrayList;

    return-object v0
.end method

.method public load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;-><init>(Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$1;)V

    .line 85
    .local v0, "runner":Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel$GetGameProfileFriendsRunner;
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public shouldRefresh()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->result:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->lastRefreshTime:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->shouldRefresh(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 90
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 91
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->result:Ljava/util/ArrayList;

    .line 94
    :cond_0
    return-void

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
