.class public final Lcom/microsoft/playready/MediaStream;
.super Ljava/lang/Object;
.source "MediaStream.java"


# instance fields
.field private mNativeClass:Lcom/microsoft/playready/Native_Class10;

.field private mRepresentationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/MediaRepresentation;",
            ">;"
        }
    .end annotation
.end field

.field private mShouldBeSelected:Ljava/lang/Boolean;

.field private mStreamIndex:I


# direct methods
.method constructor <init>(Lcom/microsoft/playready/Native_Class10;I)V
    .locals 6
    .param p1, "nativeClass"    # Lcom/microsoft/playready/Native_Class10;
    .param p2, "streamIndex"    # I

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/playready/MediaStream;->mShouldBeSelected:Ljava/lang/Boolean;

    .line 92
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/playready/MediaStream;->mRepresentationList:Ljava/util/ArrayList;

    .line 79
    iput p2, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    .line 80
    iput-object p1, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    .line 81
    invoke-virtual {p0}, Lcom/microsoft/playready/MediaStream;->isCurrentlySelected()Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/playready/MediaStream;->mShouldBeSelected:Ljava/lang/Boolean;

    .line 82
    iget-object v2, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v3, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    invoke-virtual {v2, v3}, Lcom/microsoft/playready/Native_Class10;->method_5(I)I

    move-result v1

    .line 83
    .local v1, "repCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 87
    return-void

    .line 85
    :cond_0
    iget-object v2, p0, Lcom/microsoft/playready/MediaStream;->mRepresentationList:Ljava/util/ArrayList;

    new-instance v3, Lcom/microsoft/playready/MediaRepresentation;

    iget-object v4, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v5, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    invoke-direct {v3, p0, v4, v5, v0}, Lcom/microsoft/playready/MediaRepresentation;-><init>(Lcom/microsoft/playready/MediaStream;Lcom/microsoft/playready/Native_Class10;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_11(I)J

    move-result-wide v0

    return-wide v0
.end method

.method getFragmentCount()J
    .locals 2

    .prologue
    .line 69
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_10(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMediaInfo()Lcom/microsoft/playready/MediaInfo;
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lcom/microsoft/playready/StreamMediaInfo;

    iget-object v1, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v2, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/StreamMediaInfo;-><init>(Lcom/microsoft/playready/Native_Class10;I)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_8(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRepresentationAt(I)Lcom/microsoft/playready/MediaRepresentation;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/playready/MediaStream;->mRepresentationList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/MediaRepresentation;

    return-object v0
.end method

.method public getRepresentationCount()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/playready/MediaStream;->mRepresentationList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method getStreamIndex()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    return v0
.end method

.method public isCurrentlySelected()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_13(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isProtected()Z
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/Native_Class10;->method_12(I)Z

    move-result v0

    return v0
.end method

.method select(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "isSelected"    # Ljava/lang/Boolean;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/playready/MediaStream;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/MediaStream;->mStreamIndex:I

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_50(IZ)V

    .line 36
    return-void
.end method

.method public setShouldBeSelected(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "shouldBeSelected"    # Ljava/lang/Boolean;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/microsoft/playready/MediaStream;->mShouldBeSelected:Ljava/lang/Boolean;

    .line 40
    return-void
.end method

.method public shouldBeSelected()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/playready/MediaStream;->mShouldBeSelected:Ljava/lang/Boolean;

    return-object v0
.end method
