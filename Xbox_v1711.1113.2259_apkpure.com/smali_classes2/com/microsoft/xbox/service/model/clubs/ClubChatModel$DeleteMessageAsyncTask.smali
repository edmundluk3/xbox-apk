.class Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubChatModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteMessageAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "message"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .prologue
    .line 717
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 718
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 720
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 721
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 725
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Ljava/lang/Boolean;
    .locals 6

    .prologue
    .line 740
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$700(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)Lcom/microsoft/xbox/service/chat/IChatService;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$600(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    iget-wide v4, v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageId:J

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/service/chat/IChatService;->deleteMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;J)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 743
    :goto_0
    return-object v1

    .line 741
    :catch_0
    move-exception v0

    .line 742
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$300()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DeleteMessageAsyncTask - delete failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 714
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->loadDataInBackground()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 734
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 714
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->onError()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 730
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "aBoolean"    # Ljava/lang/Boolean;

    .prologue
    .line 753
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->message:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->access$800(Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;ZLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V

    .line 754
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 714
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel$DeleteMessageAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 749
    return-void
.end method
