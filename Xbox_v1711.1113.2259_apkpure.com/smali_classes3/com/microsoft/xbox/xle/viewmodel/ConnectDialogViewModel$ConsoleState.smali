.class public final enum Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
.super Ljava/lang/Enum;
.source "ConnectDialogViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConsoleState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

.field public static final enum CONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

.field public static final enum CONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

.field public static final enum CONNECTION_FAILED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

.field public static final enum DISCONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

.field public static final enum DISCONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

.field public static final enum NOT_LISTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    const-string v1, "CONNECTION_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTION_FAILED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    const-string v1, "DISCONNECTING"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    const-string v1, "NOT_LISTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->NOT_LISTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    .line 61
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTION_FAILED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->NOT_LISTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromSessionState(I)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    .locals 2
    .param p0, "sessionState"    # I

    .prologue
    .line 65
    packed-switch p0, :pswitch_data_0

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown session state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 78
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 67
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    goto :goto_0

    .line 69
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    goto :goto_0

    .line 71
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    goto :goto_0

    .line 73
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->CONNECTION_FAILED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    goto :goto_0

    .line 75
    :pswitch_4
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTING:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    goto :goto_0

    .line 65
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getConsoleState(Lcom/microsoft/xbox/xle/model/ConsoleData;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    .locals 1
    .param p0, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 83
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/model/ConsoleData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->DISCONNECTED:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    .line 87
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->fromSessionState(I)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    move-result-object v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$ConsoleState;

    return-object v0
.end method
