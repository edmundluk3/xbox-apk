.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;
.source "EDSV2TVSeasonBrowseEpisodeModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 9
    return-void
.end method


# virtual methods
.method protected bridge synthetic createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    .locals 1
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-object v0
.end method

.method protected getDesiredMediaItemType()I
    .locals 1

    .prologue
    .line 13
    const/16 v0, 0x3eb

    return v0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x4

    return v0
.end method

.method protected getOrderBy()Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_NUMBERDESCENDING:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    return-object v0
.end method

.method protected getRelatedCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->getSeriesCanonicalId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRelatedMediaType()I
    .locals 1

    .prologue
    .line 40
    const/16 v0, 0x3ec

    return v0
.end method

.method public getSeasonNumber()I
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->getSeasonNumber()I

    move-result v0

    return v0
.end method

.method public getSeriesTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonBrowseEpisodeModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->getSeriesTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
