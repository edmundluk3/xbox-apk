.class public final enum Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;
.super Ljava/lang/Enum;
.source "FriendPickerInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CancelAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;",
        ">;",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

.field private static final TAG:Ljava/lang/String;

.field public static final enum ToggleAllowBroadcastAction:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->INSTANCE:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    new-instance v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    const-string v1, "ToggleAllowBroadcastAction"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->ToggleAllowBroadcastAction:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    .line 37
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    sget-object v1, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->INSTANCE:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->ToggleAllowBroadcastAction:Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->$VALUES:[Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    .line 40
    const-class v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->$VALUES:[Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;->TAG:Ljava/lang/String;

    return-object v0
.end method
