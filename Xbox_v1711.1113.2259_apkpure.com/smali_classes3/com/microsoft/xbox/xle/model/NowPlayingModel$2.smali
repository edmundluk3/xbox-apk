.class synthetic Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;
.super Ljava/lang/Object;
.source "NowPlayingModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/NowPlayingModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 497
    invoke-static {}, Lcom/microsoft/xbox/service/model/UpdateType;->values()[Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->InternetExplorerData:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    .line 135
    :goto_2
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->values()[Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    :try_start_3
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingApp:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingGame:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingVideo:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ConnectedPlayingMusic:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Connecting:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel$2;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->Disconnected:Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    move-exception v0

    goto :goto_8

    :catch_1
    move-exception v0

    goto :goto_7

    :catch_2
    move-exception v0

    goto :goto_6

    :catch_3
    move-exception v0

    goto :goto_5

    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v0

    goto :goto_3

    .line 497
    :catch_6
    move-exception v0

    goto :goto_2

    :catch_7
    move-exception v0

    goto :goto_1

    :catch_8
    move-exception v0

    goto :goto_0
.end method
