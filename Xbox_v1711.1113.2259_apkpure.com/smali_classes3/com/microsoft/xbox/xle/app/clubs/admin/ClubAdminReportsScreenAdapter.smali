.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;
.source "ClubAdminReportsScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;


# instance fields
.field private final countAppearance:Landroid/text/style/TextAppearanceSpan;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

.field private final title:Landroid/widget/TextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)V
    .locals 5
    .param p1, "clubViewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;)V

    .line 40
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    .line 43
    const v2, 0x7f0e028d

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->title:Landroid/widget/TextView;

    .line 44
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->title:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08021e

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->countAppearance:Landroid/text/style/TextAppearanceSpan;

    .line 46
    const v2, 0x7f0e06bd

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 47
    .local v0, "noContent":Landroid/widget/TextView;
    const v2, 0x7f07027a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 49
    const v2, 0x7f0e06bc

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 50
    .local v1, "noContentIcon":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 51
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070f03

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    return-void
.end method

.method private setTitle(I)V
    .locals 7
    .param p1, "reportCount"    # I

    .prologue
    .line 103
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07028c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "titleBase":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 106
    .local v2, "titleCount":Ljava/lang/String;
    new-instance v0, Landroid/text/SpannableString;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 107
    .local v0, "styledTitle":Landroid/text/SpannableString;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->countAppearance:Landroid/text/style/TextAppearanceSpan;

    .line 109
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    .line 110
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    const/16 v6, 0x21

    .line 107
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 113
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->title:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method

.method private setupRecyclerView()V
    .locals 6

    .prologue
    .line 87
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->isViewerOwner()Z

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getClubId()J

    move-result-wide v4

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;ZJ)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    .line 89
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 91
    .local v0, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    const v2, 0x7f0e028e

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 93
    .local v1, "reportsList":Landroid/support/v7/widget/RecyclerView;
    new-instance v2, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-direct {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;-><init>(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;Landroid/support/v7/widget/LinearLayoutManager;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 98
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 99
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 100
    return-void
.end method

.method private updateMissingListItems()V
    .locals 4

    .prologue
    .line 117
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getLoadedReportedItems()Ljava/util/List;

    move-result-object v0

    .line 118
    .local v0, "allItems":Ljava/util/List;, "Ljava/util/List<+Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v2

    .line 120
    .local v2, "oldItems":Ljava/util/List;, "Ljava/util/List<+Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->clear()V

    .line 121
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 123
    new-instance v3, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v3, v2, v0}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v3}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v1

    .line 124
    .local v1, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-virtual {v1, v3}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 125
    return-void
.end method


# virtual methods
.method public banUser(JLjava/lang/String;)V
    .locals 1
    .param p1, "xuid"    # J
    .param p3, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 141
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->banUser(JLjava/lang/String;)V

    .line 143
    return-void
.end method

.method public deleteItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getNumReports()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->setTitle(I)V

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->deleteItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    .line 131
    return-void
.end method

.method public ignoreReport(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getNumReports()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->setTitle(I)V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->ignoreReport(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    .line 137
    return-void
.end method

.method public navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;)V
    .locals 1
    .param p1, "commentItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;)V

    .line 158
    return-void
.end method

.method public navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;)V
    .locals 1
    .param p1, "feedListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;)V

    .line 153
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->onStart()V

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->setIncrementalLoader(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;)V

    .line 61
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->onStop()V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->setIncrementalLoader(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;)V

    .line 70
    :cond_0
    return-void
.end method

.method public sendUserMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 1
    .param p1, "user"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->sendUserMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 148
    return-void
.end method

.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 74
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->updateViewOverride()V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_1

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    if-nez v0, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->setupRecyclerView()V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getNumReports()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->setTitle(I)V

    .line 82
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;->updateMissingListItems()V

    .line 84
    :cond_1
    return-void
.end method
