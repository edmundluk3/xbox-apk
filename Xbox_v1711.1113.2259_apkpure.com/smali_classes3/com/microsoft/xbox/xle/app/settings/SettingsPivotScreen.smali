.class public Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "SettingsPivotScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 25
    const v1, 0x7f0301fc

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;->setContentView(I)V

    .line 27
    const v1, 0x7f0e0a13

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .line 28
    .local v0, "pivot":Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 29
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c0149

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderBackgroundColor(I)V

    .line 30
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setSelectedTabIndicatorColorToProfileColor()V

    .line 31
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 33
    new-instance v1, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 34
    return-void
.end method
