.class public Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "GameProgressChallengesListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 25
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 30
    if-nez p2, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->inflateView(Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    .line 32
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;

    .line 33
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 38
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .line 39
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->updateUI(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;)V

    .line 40
    return-object p2

    .line 35
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;
    .end local v1    # "item":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;

    .line 36
    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter$GameProgressChallengeViewHolder;->onNewTimer(Landroid/os/CountDownTimer;)V

    goto :goto_0
.end method
