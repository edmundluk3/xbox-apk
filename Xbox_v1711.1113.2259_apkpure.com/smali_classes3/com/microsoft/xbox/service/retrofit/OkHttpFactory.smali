.class public final Lcom/microsoft/xbox/service/retrofit/OkHttpFactory;
.super Ljava/lang/Object;
.source "OkHttpFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$XUserAgentInterceptorHolder;,
        Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$XTokenHeaderInterceptorHolder;,
        Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$XTokenAuthenticatorHolder;,
        Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$DefaultLoggingHolder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static getDefaultLoggingInterceptor()Lokhttp3/Interceptor;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$DefaultLoggingHolder;->access$300()Lokhttp3/logging/HttpLoggingInterceptor;

    move-result-object v0

    return-object v0
.end method

.method public static getXTokenOkHttpBuilder()Lokhttp3/OkHttpClient$Builder;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$XTokenAuthenticatorHolder;->access$200()Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->authenticator(Lokhttp3/Authenticator;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$XTokenHeaderInterceptorHolder;->access$100()Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 41
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory$XUserAgentInterceptorHolder;->access$000()Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 38
    return-object v0
.end method
