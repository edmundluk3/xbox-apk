.class public Lcom/microsoft/xbox/smartglass/TokenResult;
.super Ljava/lang/Object;
.source "TokenResult.java"


# instance fields
.field private final _pTokenResult:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method private constructor <init>(J)V
    .locals 1
    .param p1, "pTokenResult"    # J

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/TokenResult;->_pTokenResult:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 20
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TokenResult;->_pTokenResult:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->reset()V

    .line 39
    return-void
.end method

.method public finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/TokenResult;->cancel()V

    .line 28
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 29
    return-void
.end method
