.class public Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;
.super Ljava/lang/Object;
.source "TunerChannelsData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FoundChannelInfo"
.end annotation


# instance fields
.field public channelId:Ljava/lang/String;

.field public channelName:Ljava/lang/String;

.field public channelNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "channelId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;->channelId:Ljava/lang/String;

    .line 37
    const-string v0, "displayName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;->channelName:Ljava/lang/String;

    .line 38
    const-string v0, "channelNumber"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData$FoundChannelInfo;->channelNumber:Ljava/lang/String;

    .line 39
    return-void
.end method
