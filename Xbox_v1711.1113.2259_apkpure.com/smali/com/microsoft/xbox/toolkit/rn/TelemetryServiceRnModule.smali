.class public Lcom/microsoft/xbox/toolkit/rn/TelemetryServiceRnModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "TelemetryServiceRnModule.java"


# static fields
.field private static final NAME:Ljava/lang/String; = "TelemetryService"


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 0
    .param p1, "reactContext"    # Lcom/facebook/react/bridge/ReactApplicationContext;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 20
    return-void
.end method

.method private deserializeData(Lcom/facebook/react/bridge/ReadableMap;)Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .locals 6
    .param p1, "data"    # Lcom/facebook/react/bridge/ReadableMap;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 47
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 49
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 51
    .local v0, "additionalInfoModel":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-interface {p1}, Lcom/facebook/react/bridge/ReadableMap;->keySetIterator()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v1

    .line 52
    .local v1, "it":Lcom/facebook/react/bridge/ReadableMapKeySetIterator;
    :goto_0
    invoke-interface {v1}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    invoke-interface {v1}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "key":Ljava/lang/String;
    sget-object v3, Lcom/microsoft/xbox/toolkit/rn/TelemetryServiceRnModule$1;->$SwitchMap$com$facebook$react$bridge$ReadableType:[I

    invoke-interface {p1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 57
    :pswitch_0
    invoke-interface {p1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :pswitch_1
    invoke-interface {p1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 65
    :pswitch_2
    invoke-interface {p1, v2}, Lcom/facebook/react/bridge/ReadableMap;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    .end local v2    # "key":Ljava/lang/String;
    :cond_0
    return-object v0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const-string v0, "TelemetryService"

    return-object v0
.end method

.method public pageAction(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 1
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "pageName"    # Ljava/lang/String;
    .param p3, "args"    # Lcom/facebook/react/bridge/ReadableMap;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 36
    const/4 v0, 0x0

    .line 37
    .local v0, "additionalInfo":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    if-eqz p3, :cond_0

    .line 38
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/toolkit/rn/TelemetryServiceRnModule;->deserializeData(Lcom/facebook/react/bridge/ReadableMap;)Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v0

    .line 43
    :goto_0
    invoke-static {p1, p2, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 44
    return-void

    .line 40
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    .end local v0    # "additionalInfo":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .restart local v0    # "additionalInfo":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    goto :goto_0
.end method

.method public pageView(Ljava/lang/String;Lcom/facebook/react/bridge/ReadableMap;)V
    .locals 1
    .param p1, "pageName"    # Ljava/lang/String;
    .param p2, "args"    # Lcom/facebook/react/bridge/ReadableMap;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 24
    const/4 v0, 0x0

    .line 25
    .local v0, "additionalInfo":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    if-eqz p2, :cond_0

    .line 26
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/rn/TelemetryServiceRnModule;->deserializeData(Lcom/facebook/react/bridge/ReadableMap;)Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v0

    .line 31
    :goto_0
    invoke-static {p1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 32
    return-void

    .line 28
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    .end local v0    # "additionalInfo":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .restart local v0    # "additionalInfo":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    goto :goto_0
.end method
