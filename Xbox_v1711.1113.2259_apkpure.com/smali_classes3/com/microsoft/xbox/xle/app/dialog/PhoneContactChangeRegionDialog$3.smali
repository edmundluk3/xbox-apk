.class Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;
.super Ljava/lang/Object;
.source "PhoneContactChangeRegionDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 70
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 73
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 8
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 76
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "prefix":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Landroid/widget/ArrayAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->clear()V

    .line 78
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 79
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 80
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$400(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 81
    .local v1, "region":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 82
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 86
    .end local v1    # "region":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$400(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$002(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 88
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x1090003

    const v6, 0x1020014

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$302(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;

    .line 89
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$500(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Landroid/widget/ArrayAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog$3;->this$0:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactChangeRegionDialog;)Landroid/widget/ArrayAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 91
    return-void
.end method
