.class public abstract Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;
.super Ljava/lang/Object;
.source "OOBEInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TimeZoneChangedAction"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;
    .locals 1
    .param p0, "timeZone"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 278
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_TimeZoneChangedAction;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_TimeZoneChangedAction;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract timeZone()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
