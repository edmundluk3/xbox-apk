.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;
.super Ljava/lang/Object;
.source "UTCGameHub.java"


# static fields
.field public static final ACTION_NAMES:[Ljava/lang/String;

.field private static final GAMETABSCROLLEDPATTERN:Ljava/lang/String; = "012"

.field private static final GAMETABSELECTEDPATTERN:Ljava/lang/String; = "02"

.field public static final TAB_NAMES:[Ljava/lang/String;

.field private static currentTab:Ljava/lang/String;

.field private static gameTitleId:J

.field private static previousTab:Ljava/lang/String;

.field public static trackHubPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public static trackHubPageResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->gameTitleId:J

    .line 24
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Game Hub - Info View"

    aput-object v1, v0, v3

    const-string v1, "Game Hub - Activity View"

    aput-object v1, v0, v4

    const-string v1, "Game Hub - Friends View"

    aput-object v1, v0, v5

    const-string v1, "Game Hub - Achievements View"

    aput-object v1, v0, v6

    const-string v1, "Game Hub - Captures View"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Game Hub - LFG View"

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->TAB_NAMES:[Ljava/lang/String;

    .line 33
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Game Hub - Info"

    aput-object v1, v0, v3

    const-string v1, "Game Hub - Activity"

    aput-object v1, v0, v4

    const-string v1, "Game Hub - Friends"

    aput-object v1, v0, v5

    const-string v1, "Game Hub - Achievements"

    aput-object v1, v0, v6

    const-string v1, "Game Hub - Captures"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Game Hub - LFG"

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->ACTION_NAMES:[Ljava/lang/String;

    .line 42
    const-string v0, "Game Hub - Info View"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->currentTab:Ljava/lang/String;

    .line 43
    const-string v0, "Game Hub - Info View"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->previousTab:Ljava/lang/String;

    .line 67
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackHubPageResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    .line 89
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$2;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackHubPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()J
    .locals 2

    .prologue
    .line 19
    sget-wide v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->gameTitleId:J

    return-wide v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->previousTab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 19
    sput-object p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->previousTab:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->currentTab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 19
    sput-object p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->currentTab:Ljava/lang/String;

    return-object p0
.end method

.method public static getCurrentTab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->currentTab:Ljava/lang/String;

    return-object v0
.end method

.method public static getPreviousTab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->previousTab:Ljava/lang/String;

    return-object v0
.end method

.method public static resetTracking(Ljava/lang/String;)V
    .locals 1
    .param p0, "toPage"    # Ljava/lang/String;

    .prologue
    .line 54
    const-string v0, "Game Hub"

    if-eq p0, v0, :cond_0

    const-string v0, "Game Hub - Achievements View"

    if-eq p0, v0, :cond_0

    const-string v0, "Game Hub - Activity View"

    if-eq p0, v0, :cond_0

    const-string v0, "Game Hub - Captures View"

    if-eq p0, v0, :cond_0

    const-string v0, "Game Hub - Friends View"

    if-eq p0, v0, :cond_0

    const-string v0, "Game Hub - Info View"

    if-eq p0, v0, :cond_0

    const-string v0, "Game Hub - LFG View"

    if-eq p0, v0, :cond_0

    .line 62
    const-string v0, "Game Hub - Info View"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->currentTab:Ljava/lang/String;

    .line 63
    const-string v0, "Game Hub - Info View"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->previousTab:Ljava/lang/String;

    .line 65
    :cond_0
    return-void
.end method

.method public static setGameTitleId(J)V
    .locals 0
    .param p0, "gameTitle"    # J

    .prologue
    .line 303
    sput-wide p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->gameTitleId:J

    .line 304
    return-void
.end method

.method public static trackCompareButtonAction()V
    .locals 1

    .prologue
    .line 230
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$8;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$8;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 242
    return-void
.end method

.method public static trackFollowToggleAction(JZ)V
    .locals 2
    .param p0, "titleid"    # J
    .param p2, "follow"    # Z

    .prologue
    .line 154
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$4;-><init>(JZ)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 167
    return-void
.end method

.method public static trackInfoPageView()V
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$3;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$3;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 145
    return-void
.end method

.method public static trackPlayAction(J)V
    .locals 2
    .param p0, "titleid"    # J

    .prologue
    .line 175
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$5;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$5;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 184
    return-void
.end method

.method public static trackShowAchievementAction()V
    .locals 1

    .prologue
    .line 248
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$9;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$9;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 258
    return-void
.end method

.method public static trackShowFriendAction(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 1
    .param p0, "gamer"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 209
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$7;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$7;-><init>(Lcom/microsoft/xbox/service/model/FollowersData;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 224
    return-void
.end method

.method public static trackShowGameLeaderboardAction(J)V
    .locals 2
    .param p0, "titleId"    # J

    .prologue
    .line 266
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$10;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$10;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 276
    return-void
.end method

.method public static trackShowInStoreAction(J)V
    .locals 2
    .param p0, "titleid"    # J

    .prologue
    .line 192
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$6;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$6;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 201
    return-void
.end method

.method public static trackSpotlightAction()V
    .locals 1

    .prologue
    .line 282
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$11;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$11;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 292
    return-void
.end method
