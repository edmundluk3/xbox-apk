.class public Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "EDSV2MediaItemListViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;

    .prologue
    .line 125
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 129
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->shouldRefreshChild()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 154
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->loadListDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 149
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 134
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->onListDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 135
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 144
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->onListDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 145
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 125
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 139
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel<TT;>.LoadListTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel$LoadListTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;->updateAdapter()V

    .line 140
    return-void
.end method
