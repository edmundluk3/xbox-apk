.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubTypeContainer"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;)Z
    .locals 1
    .param p0, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 658
    if-eqz p0, :cond_0

    .line 659
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 658
    :goto_0
    return v0

    .line 659
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 663
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubTypeContainer$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubTypeContainer$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
    .locals 2
    .param p0, "type"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "genre"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 651
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 652
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 654
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubTypeContainer;

    invoke-direct {v0, p0, p1, v1, v1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubTypeContainer;-><init>(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract localizedTitleFamilyName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract titleFamilyId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
