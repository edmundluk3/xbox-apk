.class public abstract Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyMemberChangeResult;
.super Ljava/lang/Object;
.source "PartyInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/party/PartyInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PartyMemberChangeResult"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyMemberChangeResult;
    .locals 1
    .param p0, "partyMember"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 298
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 299
    new-instance v0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyMemberChangeResult;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyMemberChangeResult;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V

    return-object v0
.end method


# virtual methods
.method public abstract member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartyMemberChange: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyMemberChangeResult;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyMemberChangeResult;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->currentState()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
