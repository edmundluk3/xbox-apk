.class Lcom/microsoft/xbox/service/model/ProfileModel$GetRecentGamesAndAchievementRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetRecentGamesAndAchievementRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final titleHubService:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

.field private final xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;)V
    .locals 1
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    .line 4049
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecentGamesAndAchievementRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 4050
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecentGamesAndAchievementRunner;->xuid:Ljava/lang/String;

    .line 4051
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getTitleHubService()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecentGamesAndAchievementRunner;->titleHubService:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    .line 4052
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4056
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecentGamesAndAchievementRunner;->titleHubService:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecentGamesAndAchievementRunner;->xuid:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/microsoft/xbox/service/titleHub/ITitleHubService;->getAllRecentlyPlayedTitles(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 4058
    .local v3, "titles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    if-nez v3, :cond_1

    .line 4059
    const/4 v1, 0x0

    .line 4091
    :cond_0
    return-object v1

    .line 4062
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;-><init>()V

    .line 4063
    .local v1, "result":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v5, v1, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;->titles:Ljava/util/ArrayList;

    .line 4068
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 4070
    .local v4, "unlockDate":Ljava/util/Date;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 4071
    .local v2, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v2, :cond_2

    .line 4072
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;-><init>()V

    .line 4074
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    iget-object v6, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    if-eqz v6, :cond_3

    .line 4075
    iget-object v6, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    iget v6, v6, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentAchievements:I

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setEarnedAchievements(I)V

    .line 4076
    iget-object v6, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    iget v6, v6, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->currentGamerscore:I

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setCurrentGamerscore(I)V

    .line 4077
    iget-object v6, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->achievement:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;

    iget v6, v6, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleAchievement;->totalGamerscore:I

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setMaxGamerscore(I)V

    .line 4080
    :cond_3
    iget-object v6, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    iput-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->name:Ljava/lang/String;

    .line 4081
    iget-wide v6, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    iput-wide v6, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->titleId:J

    .line 4082
    iget-object v6, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->type:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setTitleType(Ljava/lang/String;)V

    .line 4083
    iget-object v6, v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->mediaItemType:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setMediaItemType(Ljava/lang/String;)V

    .line 4084
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setLastUnlock(Ljava/util/Date;)V

    .line 4085
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->getDevices()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setDevices(Ljava/util/List;)V

    .line 4087
    iget-object v6, v1, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;->titles:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4045
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecentGamesAndAchievementRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4105
    const-wide/16 v0, 0xbd9

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4100
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetRecentGamesAndAchievementRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$5200(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 4101
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4096
    return-void
.end method
