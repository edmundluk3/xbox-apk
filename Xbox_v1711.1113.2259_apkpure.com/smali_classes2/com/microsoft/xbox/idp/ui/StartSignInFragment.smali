.class public Lcom/microsoft/xbox/idp/ui/StartSignInFragment;
.super Lcom/microsoft/xbox/idp/compat/BaseFragment;
.source "StartSignInFragment.java"

# interfaces
.implements Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;,
        Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Status;,
        Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;
    }
.end annotation


# static fields
.field private static final KEY_STATE:Ljava/lang/String; = "KEY_STATE"

.field private static final NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private callbacks:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

.field private final loaderMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final startSignInCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/StartSignInLoader$Result;",
            ">;"
        }
    .end annotation
.end field

.field private state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->TAG:Ljava/lang/String;

    .line 178
    new-instance v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$2;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;-><init>()V

    .line 28
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->loaderMap:Landroid/util/SparseArray;

    .line 31
    sget-object v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

    .line 108
    new-instance v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$1;-><init>(Lcom/microsoft/xbox/idp/ui/StartSignInFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->startSignInCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_start_sign_in:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ResultLoaderInfo;

    const-class v3, Lcom/microsoft/xbox/idp/toolkit/StartSignInLoader$Result;

    iget-object v4, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->startSignInCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/idp/util/ResultLoaderInfo;-><init>(Ljava/lang/Class;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 35
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/idp/ui/StartSignInFragment;)Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/StartSignInFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/idp/ui/StartSignInFragment;)Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/StartSignInFragment;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 24
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public getLoaderInfo(I)Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;
    .locals 1
    .param p1, "loaderId"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->loaderMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 82
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v1, p1, p2, p3}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->getActivityResult(IILandroid/content/Intent;)Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;

    move-result-object v0

    .line 86
    .local v0, "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;->isTryAgain()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 88
    sget-object v1, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->TAG:Ljava/lang/String;

    const-string v2, "Trying again"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->deleteLoader()V

    .line 96
    .end local v0    # "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    :cond_0
    :goto_0
    return-void

    .line 91
    .restart local v0    # "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Status;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;->onComplete(Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Status;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onAttach(Landroid/content/Context;)V

    .line 40
    check-cast p1, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

    .line 41
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    if-nez p1, :cond_1

    new-instance v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    iget-object v0, v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->setActivityContext(Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityContext;)V

    .line 58
    :cond_0
    return-void

    .line 53
    :cond_1
    const-string v0, "KEY_STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    sget v0, Lcom/microsoft/xbox/idp/R$layout;->xbid_fragment_busy:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onDetach()V

    .line 46
    sget-object v0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$Callbacks;

    .line 47
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 62
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onResume()V

    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 66
    .local v0, "args":Landroid/os/Bundle;
    sget-object v2, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->TAG:Ljava/lang/String;

    const-string v3, "Initializing loader_start_sign_in"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 68
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "KEY_RESULT_KEY"

    new-instance v3, Lcom/microsoft/xbox/idp/util/FragmentLoaderKey;

    const-class v4, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;

    sget v5, Lcom/microsoft/xbox/idp/R$id;->loader_start_sign_in:I

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/idp/util/FragmentLoaderKey;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    if-eqz v2, :cond_0

    .line 70
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    sget v3, Lcom/microsoft/xbox/idp/R$id;->loader_start_sign_in:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->initLoader(ILandroid/os/Bundle;Z)Z

    .line 72
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 77
    const-string v0, "KEY_STATE"

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/StartSignInFragment;->state:Lcom/microsoft/xbox/idp/ui/StartSignInFragment$State;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 78
    return-void
.end method
