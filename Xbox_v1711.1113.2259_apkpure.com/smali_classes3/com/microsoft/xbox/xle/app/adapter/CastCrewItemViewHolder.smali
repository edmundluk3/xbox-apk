.class public Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;
.super Ljava/lang/Object;
.source "CastCrewItemViewHolder.java"


# instance fields
.field private final name:Landroid/widget/TextView;

.field private final role:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "parentView"    # Landroid/view/View;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const v0, 0x7f0e07aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;->name:Landroid/widget/TextView;

    .line 16
    const v0, 0x7f0e07ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;->role:Landroid/widget/TextView;

    .line 17
    return-void
.end method


# virtual methods
.method public getNameView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;->name:Landroid/widget/TextView;

    return-object v0
.end method

.method public getRoleView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewItemViewHolder;->role:Landroid/widget/TextView;

    return-object v0
.end method
