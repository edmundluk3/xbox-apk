.class public final Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubModeratorPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClubModeratorListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
        "Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final selectAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "selectAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 80
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 81
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;->selectAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;->selectAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 96
    const v0, 0x7f03008f

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 74
    check-cast p1, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 91
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;->onBind(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 92
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 86
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter$ModeratorViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;Landroid/view/View;)V

    return-object v0
.end method
