.class public Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
.super Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;
.source "UserSummaryRepository.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository",
        "<",
        "Ljava/lang/Long;",
        "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;",
        "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final MAX_SIZE:I = 0x3ff


# instance fields
.field private final peopleHubService:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 1
    .param p1, "peopleHubService"    # Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "dataMapper"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 31
    const/16 v0, 0x3ff

    invoke-direct {p0, p2, p3, v0}, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;-><init>(Lcom/microsoft/xbox/data/repository/DataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;I)V

    .line 33
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;->peopleHubService:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;

    .line 35
    return-void
.end method

.method static synthetic lambda$loadData$0(Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubResponse;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "response"    # Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubResponse;->people()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 45
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubResponse;->people()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected loadData(Ljava/util/Collection;)Lio/reactivex/Observable;
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "xuids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;->peopleHubService:Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;

    invoke-static {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;->with(Ljava/util/Collection;)Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;->getSummaries(Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$BatchRequest;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMapObservable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 42
    return-object v0
.end method

.method protected mapKey(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)Ljava/lang/Long;
    .locals 2
    .param p1, "item"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic mapKey(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 20
    check-cast p1, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;->mapKey(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
