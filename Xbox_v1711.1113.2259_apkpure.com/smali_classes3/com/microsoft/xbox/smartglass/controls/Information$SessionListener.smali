.class Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;
.super Lcom/microsoft/xbox/smartglass/SessionManagerListener;
.source "Information.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/Information;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/Information;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/smartglass/controls/Information;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/SessionManagerListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/smartglass/controls/Information;Lcom/microsoft/xbox/smartglass/controls/Information$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Information;
    .param p2, "x1"    # Lcom/microsoft/xbox/smartglass/controls/Information$1;

    .prologue
    .line 326
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/Information;)V

    return-void
.end method


# virtual methods
.method public onConnectionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 4
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/ConnectionState;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 340
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    invoke-static {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/Information;->access$200(Lcom/microsoft/xbox/smartglass/controls/Information;Lcom/microsoft/xbox/smartglass/ConnectionState;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonConnectionStateChanged;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/Information;->access$300(Lcom/microsoft/xbox/smartglass/controls/Information;)Z

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/Information;->access$300(Lcom/microsoft/xbox/smartglass/controls/Information;)Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/JsonConnectionStateChanged;-><init>(ZZ)V

    .line 343
    .local v0, "json":Lcom/microsoft/xbox/smartglass/controls/JsonConnectionStateChanged;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "connectionStateChanged"

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/JsonConnectionStateChanged;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    .end local v0    # "json":Lcom/microsoft/xbox/smartglass/controls/JsonConnectionStateChanged;
    :cond_0
    :goto_0
    return-void

    .line 344
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onPairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 4
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/PairedIdentityState;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 351
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    invoke-static {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/Information;->access$400(Lcom/microsoft/xbox/smartglass/controls/Information;Lcom/microsoft/xbox/smartglass/PairedIdentityState;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 353
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonPairedIdentityStateChanged;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/Information;->access$500(Lcom/microsoft/xbox/smartglass/controls/Information;)Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonPairedIdentityStateChanged;-><init>(Z)V

    .line 354
    .local v0, "json":Lcom/microsoft/xbox/smartglass/controls/JsonPairedIdentityStateChanged;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "pairedIdentityStateChanged"

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/JsonPairedIdentityStateChanged;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    .end local v0    # "json":Lcom/microsoft/xbox/smartglass/controls/JsonPairedIdentityStateChanged;
    :cond_0
    :goto_0
    return-void

    .line 355
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 4
    .param p1, "activeTitleState"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    .line 330
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/Information;->updateTitleInformation(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V

    .line 332
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonTitleChanged;

    iget v1, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/Information;->access$100(Lcom/microsoft/xbox/smartglass/controls/Information;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/JsonTitleChanged;-><init>(ILjava/util/Collection;)V

    .line 333
    .local v0, "json":Lcom/microsoft/xbox/smartglass/controls/JsonTitleChanged;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Information$SessionListener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Information;

    iget-object v1, v1, Lcom/microsoft/xbox/smartglass/controls/Information;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "titleChanged"

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/JsonTitleChanged;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    .end local v0    # "json":Lcom/microsoft/xbox/smartglass/controls/JsonTitleChanged;
    :goto_0
    return-void

    .line 334
    :catch_0
    move-exception v1

    goto :goto_0
.end method
