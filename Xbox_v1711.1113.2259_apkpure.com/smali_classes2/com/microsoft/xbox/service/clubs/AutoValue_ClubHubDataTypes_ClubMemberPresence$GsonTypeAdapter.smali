.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubHubDataTypes_ClubMemberPresence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultLastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field private defaultLastSeenTimestamp:Ljava/lang/String;

.field private defaultXuid:J

.field private final lastSeenStateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;",
            ">;"
        }
    .end annotation
.end field

.field private final lastSeenTimestampAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final xuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 3
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->defaultXuid:J

    .line 24
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->defaultLastSeenTimestamp:Ljava/lang/String;

    .line 25
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->defaultLastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 27
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->xuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 28
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->lastSeenTimestampAdapter:Lcom/google/gson/TypeAdapter;

    .line 29
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->lastSeenStateAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    .locals 7
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v6, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 63
    const/4 v3, 0x0

    .line 94
    :goto_0
    return-object v3

    .line 65
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 66
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->defaultXuid:J

    .line 67
    .local v4, "xuid":J
    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->defaultLastSeenTimestamp:Ljava/lang/String;

    .line 68
    .local v2, "lastSeenTimestamp":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->defaultLastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 69
    .local v1, "lastSeenState":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 70
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v6, :cond_1

    .line 72
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 75
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 89
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 75
    :sswitch_0
    const-string/jumbo v6, "xuid"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v6, "lastSeenTimestamp"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :sswitch_2
    const-string v6, "lastSeenState"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v3, 0x2

    goto :goto_2

    .line 77
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->xuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 78
    goto :goto_1

    .line 81
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->lastSeenTimestampAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "lastSeenTimestamp":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 82
    .restart local v2    # "lastSeenTimestamp":Ljava/lang/String;
    goto :goto_1

    .line 85
    :pswitch_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->lastSeenStateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "lastSeenState":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 86
    .restart local v1    # "lastSeenState":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    goto :goto_1

    .line 93
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 94
    new-instance v3, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence;

    invoke-direct {v3, v4, v5, v2, v1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence;-><init>(JLjava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    goto :goto_0

    .line 75
    :sswitch_data_0
    .sparse-switch
        -0x3beb6a1b -> :sswitch_1
        -0x350d0f20 -> :sswitch_2
        0x3850d8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultLastSeenState(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLastSeenState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->defaultLastSeenState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 41
    return-object p0
.end method

.method public setDefaultLastSeenTimestamp(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLastSeenTimestamp"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->defaultLastSeenTimestamp:Ljava/lang/String;

    .line 37
    return-object p0
.end method

.method public setDefaultXuid(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultXuid"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->defaultXuid:J

    .line 33
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    if-nez p2, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 58
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 51
    const-string/jumbo v0, "xuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->xuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 53
    const-string v0, "lastSeenTimestamp"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->lastSeenTimestampAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenTimestamp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 55
    const-string v0, "lastSeenState"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->lastSeenStateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenState()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 57
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubMemberPresence$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;)V

    return-void
.end method
