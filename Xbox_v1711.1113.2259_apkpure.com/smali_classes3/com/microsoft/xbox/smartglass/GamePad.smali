.class public Lcom/microsoft/xbox/smartglass/GamePad;
.super Ljava/lang/Object;
.source "GamePad.java"


# instance fields
.field public buttons:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/GamePadButtons;",
            ">;"
        }
    .end annotation
.end field

.field public leftThumbstickX:F

.field public leftThumbstickY:F

.field public leftTrigger:F

.field public rightThumbstickX:F

.field public rightThumbstickY:F

.field public rightTrigger:F


# direct methods
.method public constructor <init>(Ljava/util/EnumSet;FFFFFF)V
    .locals 0
    .param p2, "leftTrigger"    # F
    .param p3, "rightTrigger"    # F
    .param p4, "leftThumbstickX"    # F
    .param p5, "leftThumbstickY"    # F
    .param p6, "rightThumbstickX"    # F
    .param p7, "rightThumbstickY"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/GamePadButtons;",
            ">;FFFFFF)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "buttons":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/GamePadButtons;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/GamePad;->buttons:Ljava/util/EnumSet;

    .line 38
    iput p2, p0, Lcom/microsoft/xbox/smartglass/GamePad;->leftTrigger:F

    .line 39
    iput p3, p0, Lcom/microsoft/xbox/smartglass/GamePad;->rightTrigger:F

    .line 40
    iput p4, p0, Lcom/microsoft/xbox/smartglass/GamePad;->leftThumbstickX:F

    .line 41
    iput p5, p0, Lcom/microsoft/xbox/smartglass/GamePad;->leftThumbstickY:F

    .line 42
    iput p6, p0, Lcom/microsoft/xbox/smartglass/GamePad;->rightThumbstickX:F

    .line 43
    iput p7, p0, Lcom/microsoft/xbox/smartglass/GamePad;->rightThumbstickY:F

    .line 44
    return-void
.end method
