.class final Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;
.super Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;
.source "AutoValue_CommonViewIntents_OnActivityResultIntent.java"


# instance fields
.field private final data:Landroid/content/Intent;

.field private final requestCode:I

.field private final resultCode:I


# direct methods
.method constructor <init>(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;-><init>()V

    .line 19
    iput p1, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->requestCode:I

    .line 20
    iput p2, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->resultCode:I

    .line 21
    if-nez p3, :cond_0

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null data"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->data:Landroid/content/Intent;

    .line 25
    return-void
.end method


# virtual methods
.method public data()Landroid/content/Intent;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->data:Landroid/content/Intent;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    if-ne p1, p0, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 58
    check-cast v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;

    .line 59
    .local v0, "that":Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;
    iget v3, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->requestCode:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;->requestCode()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->resultCode:I

    .line 60
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;->resultCode()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->data:Landroid/content/Intent;

    .line 61
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;->data()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;
    :cond_3
    move v1, v2

    .line 63
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 68
    const/4 v0, 0x1

    .line 69
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 70
    iget v1, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->requestCode:I

    xor-int/2addr v0, v1

    .line 71
    mul-int/2addr v0, v2

    .line 72
    iget v1, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->resultCode:I

    xor-int/2addr v0, v1

    .line 73
    mul-int/2addr v0, v2

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->data:Landroid/content/Intent;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 75
    return v0
.end method

.method public requestCode()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->requestCode:I

    return v0
.end method

.method public resultCode()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->resultCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnActivityResultIntent{requestCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->requestCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resultCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->resultCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;->data:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
