.class public Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "EnforcementViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementClubChatModerationDataContext;,
        Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;,
        Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;,
        Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;
    }
.end annotation


# static fields
.field public static final CLUB_CHAT_CONTENT_ID:Ljava/lang/String; = "chatfd.xboxlive.com/channels/%s/%s/messages/%d"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private activityFeedFeedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

.field public clubId:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

.field public feedbackType:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

.field private volatile isSendingReport:Z

.field private final params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

.field private sendClubReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;

.field private sendXboxReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;

.field private shouldReportToClubModerator:Z

.field public textReason:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$000(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->displayName:Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$100(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$200(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->shouldReportToClubModerator:Z

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ClubsFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    if-ne v0, v1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$300(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->clubId:Ljava/lang/String;

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$400(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->activityFeedFeedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getEnforcementScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 71
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->onSendReportCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$902(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->isSendingReport:Z

    return p1
.end method

.method private onSendReportCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 244
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSendReportCompleted "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 259
    :goto_0
    return-void

    .line 250
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->close()V

    .line 251
    const v0, 0x7f070a86

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->showError(I)V

    goto :goto_0

    .line 255
    :pswitch_1
    const v0, 0x7f070a73

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->showError(I)V

    .line 256
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->updateAdapter()V

    goto :goto_0

    .line 246
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 237
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GoBack()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    return-void

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate back from enforcement page"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getDisplayReasons(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;)Ljava/util/List;
    .locals 3
    .param p1, "feedbackContext"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v0, "reportDisplayReasons":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;>;"
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->shouldReportToClubModerator:Z

    if-eqz v1, :cond_0

    .line 106
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$sls$ReportUserData$ReportSource:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 164
    :goto_0
    return-object v0

    .line 108
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentActivityFeedBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentCommentBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 114
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->CommsTextMessageBreaksClubRules:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 118
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$sls$ReportUserData$ReportSource:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 153
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->BioLoc:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Cheating:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$500(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->PlayerName:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->PlayerPic:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->QuitEarly:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Unsporting:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->VoiceComm:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 120
    :pswitch_3
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubLogo:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubBackground:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubName:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubDescription:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    :pswitch_4
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateComment:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :pswitch_5
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateGameClip:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 133
    :pswitch_6
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateScreenshot:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 137
    :pswitch_7
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateFeedItem:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    :pswitch_8
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Message:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->Spam:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 144
    :pswitch_9
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserContentClubChat:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 147
    :pswitch_a
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->HostInappropriateMessage:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 150
    :pswitch_b
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->UserInappropriateMessage:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 155
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->InappropriateGamerTag:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    goto :goto_1

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 118
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_4
        :pswitch_9
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public getShouldReportToClubModerator()Z
    .locals 1

    .prologue
    .line 232
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->shouldReportToClubModerator:Z

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->isSendingReport:Z

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 101
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method protected onStopOverride()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendXboxReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendXboxReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->cancel()V

    .line 90
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendXboxReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendClubReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendClubReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->cancel()V

    .line 95
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendClubReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;

    .line 97
    :cond_1
    return-void
.end method

.method public sendReportAsync()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 168
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendXboxReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;

    if-eqz v2, :cond_0

    .line 169
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendXboxReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->cancel()V

    .line 172
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendClubReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;

    if-eqz v2, :cond_1

    .line 173
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendClubReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->cancel()V

    .line 176
    :cond_1
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->shouldReportToClubModerator:Z

    if-eqz v2, :cond_2

    .line 179
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$sls$ReportUserData$ReportSource:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 190
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$600(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ActivityFeedItem"

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->textReason:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;

    move-result-object v6

    .line 194
    .local v6, "clubReportItemRequest":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Report sending - TargetId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$300(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ClubReportItemRequest : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$300(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v6, v3}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendClubReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;

    .line 196
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendClubReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->load(Z)V

    .line 223
    .end local v6    # "clubReportItemRequest":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
    :goto_1
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->isSendingReport:Z

    .line 224
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->updateAdapter()V

    .line 225
    return-void

    .line 181
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$600(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ActivityFeedItem"

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->textReason:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;

    move-result-object v6

    .line 182
    .restart local v6    # "clubReportItemRequest":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
    goto :goto_0

    .line 184
    .end local v6    # "clubReportItemRequest":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$600(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Comment"

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->textReason:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;

    move-result-object v6

    .line 185
    .restart local v6    # "clubReportItemRequest":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
    goto :goto_0

    .line 187
    .end local v6    # "clubReportItemRequest":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$700(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Chat"

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->textReason:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;

    move-result-object v6

    .line 188
    .restart local v6    # "clubReportItemRequest":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
    goto :goto_0

    .line 199
    .end local v6    # "clubReportItemRequest":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
    :cond_2
    const-string v1, ""

    .line 201
    .local v1, "feedbackContext":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->activityFeedFeedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    if-nez v2, :cond_3

    .line 202
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$sls$ReportUserData$ReportSource:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->feedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 217
    :goto_2
    :pswitch_3
    new-instance v0, Lcom/microsoft/xbox/service/model/sls/ReportUserData;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->feedbackType:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->getFeedbackType()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$600(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->textReason:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$800(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/sls/ReportUserData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)V

    .line 218
    .local v0, "reportUserData":Lcom/microsoft/xbox/service/model/sls/ReportUserData;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Report sending - TargetId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$300(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ReportData : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCLog;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->params:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;->access$300(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v0, v3}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Lcom/microsoft/xbox/service/model/sls/ReportUserData;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendXboxReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;

    .line 220
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->sendXboxReportAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendXboxReportAsyncTask;->load(Z)V

    goto/16 :goto_1

    .line 205
    .end local v0    # "reportUserData":Lcom/microsoft/xbox/service/model/sls/ReportUserData;
    :pswitch_4
    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;->ActivityFeed:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;->name()Ljava/lang/String;

    move-result-object v1

    .line 206
    goto :goto_2

    .line 209
    :pswitch_5
    sget-object v2, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;->Club:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;->name()Ljava/lang/String;

    move-result-object v1

    .line 210
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->feedbackType:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEnforcement;->TrackClubEnforcement(Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;)V

    goto :goto_2

    .line 214
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->activityFeedFeedbackContext:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ActivityFeedFeedbackContext;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 179
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 202
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setShouldReportToClubModerator(Z)V
    .locals 0
    .param p1, "shouldReportToClubModerator"    # Z

    .prologue
    .line 228
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->shouldReportToClubModerator:Z

    .line 229
    return-void
.end method
