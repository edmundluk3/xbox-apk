.class Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$FriendsWhoPlayDetailAdapterProvider;
.super Ljava/lang/Object;
.source "FriendsWhoPlayDetailScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FriendsWhoPlayDetailAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$FriendsWhoPlayDetailAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$1;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen$FriendsWhoPlayDetailAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayDetailScreen;)V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 3
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 37
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    .line 38
    .local v0, "friendsWhoPlayViewModel":Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getFriendsWhoPlayScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;

    .line 40
    .local v1, "screenAdapter":Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;->setUseDarkStyle(Z)V

    .line 41
    return-object v1
.end method
