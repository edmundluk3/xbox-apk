.class abstract Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;
.super Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;
.source "$AutoValue_RtaDataTypes_RtaMpsdEvent.java"


# instance fields
.field private final notificationCorrelationId:Ljava/lang/String;

.field private final shoulderTaps:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V
    .locals 2
    .param p1, "notificationCorrelationId"    # Ljava/lang/String;
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p2, "shoulderTaps":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null notificationCorrelationId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->notificationCorrelationId:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->shoulderTaps:Lcom/google/common/collect/ImmutableList;

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 52
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 53
    check-cast v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;

    .line 54
    .local v0, "that":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->notificationCorrelationId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;->notificationCorrelationId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->shoulderTaps:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_3

    .line 55
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;->shoulderTaps()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->shoulderTaps:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;->shoulderTaps()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaMpsdEvent;
    :cond_4
    move v1, v2

    .line 57
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 62
    const/4 v0, 0x1

    .line 63
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->notificationCorrelationId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 65
    mul-int/2addr v0, v2

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->shoulderTaps:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 67
    return v0

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->shoulderTaps:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public notificationCorrelationId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ncid"
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->notificationCorrelationId:Ljava/lang/String;

    return-object v0
.end method

.method public shoulderTaps()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->shoulderTaps:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RtaMpsdEvent{notificationCorrelationId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->notificationCorrelationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", shoulderTaps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_RtaMpsdEvent;->shoulderTaps:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
