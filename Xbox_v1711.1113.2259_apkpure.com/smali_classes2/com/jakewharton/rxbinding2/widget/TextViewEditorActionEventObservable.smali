.class final Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable;
.super Lio/reactivex/Observable;
.source "TextViewEditorActionEventObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/Observable",
        "<",
        "Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;",
        ">;"
    }
.end annotation


# instance fields
.field private final handled:Lio/reactivex/functions/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Predicate",
            "<-",
            "Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final view:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;Lio/reactivex/functions/Predicate;)V
    .locals 0
    .param p1, "view"    # Landroid/widget/TextView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lio/reactivex/functions/Predicate",
            "<-",
            "Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p2, "handled":Lio/reactivex/functions/Predicate;, "Lio/reactivex/functions/Predicate<-Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;>;"
    invoke-direct {p0}, Lio/reactivex/Observable;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable;->view:Landroid/widget/TextView;

    .line 20
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable;->handled:Lio/reactivex/functions/Predicate;

    .line 21
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;>;"
    invoke-static {p1}, Lcom/jakewharton/rxbinding2/internal/Preconditions;->checkMainThread(Lio/reactivex/Observer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 31
    :goto_0
    return-void

    .line 28
    :cond_0
    new-instance v0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable;->view:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable;->handled:Lio/reactivex/functions/Predicate;

    invoke-direct {v0, v1, p1, v2}, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;-><init>(Landroid/widget/TextView;Lio/reactivex/Observer;Lio/reactivex/functions/Predicate;)V

    .line 29
    .local v0, "listener":Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 30
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable;->view:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    goto :goto_0
.end method
