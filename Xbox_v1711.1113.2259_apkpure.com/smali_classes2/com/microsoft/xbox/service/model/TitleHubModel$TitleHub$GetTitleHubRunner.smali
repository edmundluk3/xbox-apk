.class Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub$GetTitleHubRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleHubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetTitleHubRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

.field private titleId:J


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;J)V
    .locals 0
    .param p2, "titleId"    # J

    .prologue
    .line 195
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub$GetTitleHubRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 196
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub$GetTitleHubRunner;->titleId:J

    .line 197
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 201
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getTitleHubService()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub$GetTitleHubRunner;->titleId:J

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/service/titleHub/ITitleHubService;->getTitleSummary(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub$GetTitleHubRunner;->buildData()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 215
    const-wide/16 v0, 0xfa7

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 210
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub$GetTitleHubRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/TitleHubModel$TitleHub;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 211
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 206
    return-void
.end method
