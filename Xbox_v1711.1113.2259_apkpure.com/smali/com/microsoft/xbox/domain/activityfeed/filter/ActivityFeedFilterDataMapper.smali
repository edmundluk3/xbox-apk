.class public final Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;
.super Ljava/lang/Object;
.source "ActivityFeedFilterDataMapper.java"


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method


# virtual methods
.method public fromString(Ljava/lang/String;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .locals 2
    .param p1, "json"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 18
    const-class v1, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    invoke-static {p1, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .line 20
    .local v0, "prefs":Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    if-nez v0, :cond_0

    .line 21
    invoke-static {}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->defaultPrefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    .line 20
    .end local v0    # "prefs":Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    :cond_0
    return-object v0
.end method

.method public toString(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Ljava/lang/String;
    .locals 1
    .param p1, "pref"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 26
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 27
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
