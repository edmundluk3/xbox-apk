.class public Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;
.super Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;
.source "HomeScreenPopupScreenViewModelFullSnap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;"
        }
    .end annotation
.end field

.field private final launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

.field private final provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

.field private final selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V
    .locals 4
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    .line 40
    const/4 v0, 0x0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->SHOW_DETAILS_PAGE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;[Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    .line 67
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .line 41
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->provider:Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    return-object v0
.end method


# virtual methods
.method public createScreenAdapter()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->createContentItemScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderData()Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    return-object v0
.end method

.method protected getLaunchableItem()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->launchableItem:Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    return-object v0
.end method

.method protected getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->selectedMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method protected getMediaItemForDetails()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelFullSnap;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method
