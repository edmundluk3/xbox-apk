.class public final Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes;
.super Ljava/lang/Object;
.source "ChatHistoryDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageRequest;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;,
        Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type should not be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
