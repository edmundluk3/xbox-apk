.class Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;
.super Ljava/util/TimerTask;
.source "TextInputControlViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForDirtyContentTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$1;

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;->asyncSendTextUpdateToConsole()V

    return-void
.end method

.method private asyncSendTextUpdateToConsole()V
    .locals 3

    .prologue
    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->access$500(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->access$402(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Ljava/lang/String;)Ljava/lang/String;

    .line 150
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->access$500(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sendTextInput(Ljava/lang/String;)V

    .line 151
    const-string v0, "textinputvm"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "text sent to console: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->access$500(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->access$202(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Z)Z

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->access$602(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Z)Z

    .line 154
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask$1;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$CheckForDirtyContentTask;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 146
    return-void
.end method
