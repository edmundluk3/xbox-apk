.class public Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;
.super Ljava/lang/Object;
.source "SkypeEndpointSubscriptionBody.java"


# instance fields
.field public final channelType:Ljava/lang/String;

.field public final eventChannel:Ljava/lang/String;

.field public final eventServiceName:Ljava/lang/String;

.field public interestedResources:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final template:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "eventChannel"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-string v0, "PushNotification"

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->channelType:Ljava/lang/String;

    .line 12
    const-string v0, "pnh"

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->eventServiceName:Ljava/lang/String;

    .line 13
    const-string v0, "AndroidSmartglassChat"

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->template:Ljava/lang/String;

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->interestedResources:Ljava/util/ArrayList;

    .line 17
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->eventChannel:Ljava/lang/String;

    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->interestedResources:Ljava/util/ArrayList;

    const-string v1, "/v1/users/ME/contacts/ALL"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->interestedResources:Ljava/util/ArrayList;

    const-string v1, "/v1/users/ME/conversations/ALL/messages"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->interestedResources:Ljava/util/ArrayList;

    const-string v1, "/v1/users/ME/conversations/ALL/properties?view=consumptionHorizon"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->interestedResources:Ljava/util/ArrayList;

    const-string v1, "/v1/users/ME/conversations/ALL/properties"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;->interestedResources:Ljava/util/ArrayList;

    const-string v1, "/v1/threads/ALL"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 23
    return-void
.end method

.method public static getSkypeEndpointSubscriptionBody(Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;)Ljava/lang/String;
    .locals 4
    .param p0, "skypeEndpointSubscriptionBody"    # Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointSubscriptionBody;

    .prologue
    .line 27
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 32
    :goto_0
    return-object v1

    .line 28
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SkypeEndpointSubscriptionBody"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialzation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const/4 v1, 0x0

    goto :goto_0
.end method
