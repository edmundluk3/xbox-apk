.class Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity$1;
.super Ljava/lang/Object;
.source "DetailsPivotActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 96
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->access$000(Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;)Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 97
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 98
    .local v0, "currentScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 100
    const-string v2, "VerticalScrollDock_ScrollContainer"

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    .line 101
    .local v1, "scrollableView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 103
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->access$000(Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;)Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->setScrollContainerView(Landroid/view/View;)V

    .line 109
    .end local v0    # "currentScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .end local v1    # "scrollableView":Landroid/view/View;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->getCurrentPaneIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setCurrentPage(I)V

    .line 110
    return-void
.end method
