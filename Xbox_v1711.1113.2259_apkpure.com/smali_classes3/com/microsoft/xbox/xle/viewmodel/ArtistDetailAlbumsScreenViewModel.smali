.class public Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;
.source "ArtistDetailAlbumsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemListViewModel;-><init>()V

    .line 19
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getArtistDetailAlbumsAdapter(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 20
    return-void
.end method


# virtual methods
.method public NavigateToAlbumDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 32
    return-void
.end method

.method public getArtistAlbums()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getArtistName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 54
    const v0, 0x7f070683

    return v0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getArtistDetailAlbumsAdapter(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 50
    return-void
.end method

.method public playAlbum(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const-wide/32 v6, 0x18ffc9f4

    .line 36
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v2, p1, :cond_1

    .line 37
    :cond_0
    const-string v2, "ArtistDetailAlbumsScreenViewModel"

    const-string v3, "click out of bound"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v2, "out of bound"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 45
    :goto_0
    return-void

    .line 42
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistBrowseAlbumModel;->getMediaItemListData()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    .line 43
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    const-string v2, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0x18ffc9f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->ID:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "Album"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, "uri":Ljava/lang/String;
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v6, v7, v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v2

    invoke-static {v1, v2, v6, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUriThenRemoteIfTitleNotPlaying(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;J)V

    goto :goto_0
.end method
