.class Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubCreationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadOwnedClubsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$1;

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    .locals 4

    .prologue
    .line 230
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubAccountsService()Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    move-result-object v0

    .line 232
    .local v0, "clubAccountsService":Lcom/microsoft/xbox/service/clubs/IClubAccountsService;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/service/clubs/IClubAccountsService;->getClubsByOwner(J)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 236
    :goto_0
    return-object v2

    .line 233
    :catch_0
    move-exception v1

    .line 234
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->printStackTrace()V

    .line 236
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;->loadDataInBackground()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;->onError()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$202(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->access$300(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;)Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;->onClubInformationListener(Z)V

    .line 248
    return-void

    .line 247
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 212
    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$LoadOwnedClubsTask;->onPostExecute(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 242
    return-void
.end method
