.class abstract Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;
.super Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
.source "$AutoValue_UserPostsDataTypes_PostTypeData.java"


# instance fields
.field private final achievementIds:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final description:Ljava/lang/String;

.field private final hostXuid:Ljava/lang/String;

.field private final locator:Ljava/lang/String;

.field private final postedTime:Ljava/util/Date;

.field private final scid:Ljava/lang/String;

.field private final sessionId:Ljava/lang/String;

.field private final startTime:Ljava/util/Date;

.field private final tags:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titleId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Ljava/util/Date;Ljava/util/Date;)V
    .locals 0
    .param p1, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "hostXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "titleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "sessionId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9, "startTime"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p10, "postedTime"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    .local p7, "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    .local p8, "achievementIds":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->locator:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->hostXuid:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->titleId:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->scid:Ljava/lang/String;

    .line 38
    iput-object p5, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->sessionId:Ljava/lang/String;

    .line 39
    iput-object p6, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->description:Ljava/lang/String;

    .line 40
    iput-object p7, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->tags:Lcom/google/common/collect/ImmutableList;

    .line 41
    iput-object p8, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->achievementIds:Lcom/google/common/collect/ImmutableList;

    .line 42
    iput-object p9, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->startTime:Ljava/util/Date;

    .line 43
    iput-object p10, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->postedTime:Ljava/util/Date;

    .line 44
    return-void
.end method


# virtual methods
.method public achievementIds()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->achievementIds:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public description()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->description:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 124
    if-ne p1, p0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v1

    .line 127
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    if-eqz v3, :cond_d

    move-object v0, p1

    .line 128
    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 129
    .local v0, "that":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->locator:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->locator()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->hostXuid:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 130
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->hostXuid()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->titleId:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 131
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->titleId()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->scid:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 132
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->scid()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->sessionId:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 133
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->sessionId()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->description:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 134
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->description()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->tags:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_9

    .line 135
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->achievementIds:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_a

    .line 136
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->achievementIds()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->startTime:Ljava/util/Date;

    if-nez v3, :cond_b

    .line 137
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->startTime()Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->postedTime:Ljava/util/Date;

    if-nez v3, :cond_c

    .line 138
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->postedTime()Ljava/util/Date;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 129
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->locator:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->locator()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 130
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->hostXuid:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->hostXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 131
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->titleId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->titleId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 132
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->scid:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->scid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 133
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->sessionId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->sessionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 134
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->description:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->description()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_6

    .line 135
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->tags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_7

    .line 136
    :cond_a
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->achievementIds:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->achievementIds()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_8

    .line 137
    :cond_b
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->startTime:Ljava/util/Date;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->startTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_9

    .line 138
    :cond_c
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->postedTime:Ljava/util/Date;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->postedTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    :cond_d
    move v1, v2

    .line 140
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 145
    const/4 v0, 0x1

    .line 146
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 147
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->locator:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 148
    mul-int/2addr v0, v3

    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->hostXuid:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 150
    mul-int/2addr v0, v3

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->titleId:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 152
    mul-int/2addr v0, v3

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->scid:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 154
    mul-int/2addr v0, v3

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->sessionId:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 156
    mul-int/2addr v0, v3

    .line 157
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->description:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    xor-int/2addr v0, v1

    .line 158
    mul-int/2addr v0, v3

    .line 159
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->tags:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    xor-int/2addr v0, v1

    .line 160
    mul-int/2addr v0, v3

    .line 161
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->achievementIds:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_7

    move v1, v2

    :goto_7
    xor-int/2addr v0, v1

    .line 162
    mul-int/2addr v0, v3

    .line 163
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->startTime:Ljava/util/Date;

    if-nez v1, :cond_8

    move v1, v2

    :goto_8
    xor-int/2addr v0, v1

    .line 164
    mul-int/2addr v0, v3

    .line 165
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->postedTime:Ljava/util/Date;

    if-nez v1, :cond_9

    :goto_9
    xor-int/2addr v0, v2

    .line 166
    return v0

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->locator:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 149
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->hostXuid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 151
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->titleId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 153
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->scid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 155
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->sessionId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 157
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    .line 159
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->tags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_6

    .line 161
    :cond_7
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->achievementIds:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto :goto_7

    .line 163
    :cond_8
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->startTime:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    goto :goto_8

    .line 165
    :cond_9
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->postedTime:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v2

    goto :goto_9
.end method

.method public hostXuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->hostXuid:Ljava/lang/String;

    return-object v0
.end method

.method public locator()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->locator:Ljava/lang/String;

    return-object v0
.end method

.method public postedTime()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->postedTime:Ljava/util/Date;

    return-object v0
.end method

.method public scid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->scid:Ljava/lang/String;

    return-object v0
.end method

.method public sessionId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->sessionId:Ljava/lang/String;

    return-object v0
.end method

.method public startTime()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->startTime:Ljava/util/Date;

    return-object v0
.end method

.method public tags()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->tags:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public titleId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->titleId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostTypeData{locator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->locator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hostXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->hostXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->titleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->scid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->sessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->tags:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", achievementIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->achievementIds:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->startTime:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postedTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;->postedTime:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
