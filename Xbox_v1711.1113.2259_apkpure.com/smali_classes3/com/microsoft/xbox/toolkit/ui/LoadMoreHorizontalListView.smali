.class public Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;
.super Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.source "LoadMoreHorizontalListView.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$VerticalScrollDetector;,
        Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;
    }
.end annotation


# static fields
.field private static final SCROLL_BLOCK_TIMEOUT_MS:I = 0x7530


# instance fields
.field private isLoadingMore:Z

.field private loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;

.field private verticalScrollDetector:Landroid/view/GestureDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$VerticalScrollDetector;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$VerticalScrollDetector;-><init>(Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->verticalScrollDetector:Landroid/view/GestureDetector;

    .line 33
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;->HORIZONTAL:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setOrientation(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$Orientation;)V

    .line 34
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setOnScrollListener(Lcom/microsoft/xbox/toolkit/ui/TwoWayView$OnScrollListener;)V

    .line 59
    return-void
.end method

.method private disallowInterceptOfHorizontalScroll(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->verticalScrollDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 120
    .local v0, "isVerticalScroll":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v2, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 121
    return-void

    .line 120
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAnimationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->getFirstVisiblePosition()I

    move-result v0

    .line 101
    .local v0, "index":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 102
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_0

    .line 103
    .local v1, "offset":I
    :goto_0
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    invoke-direct {v3, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;-><init>(II)V

    return-object v3

    .line 102
    .end local v1    # "offset":I
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v1

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 95
    return-object p0
.end method

.method public onDataUpdated()V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public onLoadMoreFinished()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->isLoadingMore:Z

    .line 82
    return-void
.end method

.method public onLoadMoreStart()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;

    if-nez v0, :cond_1

    .line 67
    const-string v0, "LoadMoreHorizontalListView"

    const-string v1, "Load more listener not set, ignore"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->isLoadingMore:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;->isNeedLoadMore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->isLoadingMore:Z

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;->loadMore()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->disallowInterceptOfHorizontalScroll(Landroid/view/MotionEvent;)V

    .line 115
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public restoreScrollState(II)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "offset"    # I

    .prologue
    .line 108
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setSelectionFromOffset(II)V

    .line 109
    return-void
.end method

.method public setLoadMoreListener(Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;)V
    .locals 0
    .param p1, "loadMoreListener"    # Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->loadMoreListener:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView$LoadMoreListener;

    .line 63
    return-void
.end method
