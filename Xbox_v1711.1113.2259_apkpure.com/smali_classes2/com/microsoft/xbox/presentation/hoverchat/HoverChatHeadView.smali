.class public interface abstract Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;
.super Ljava/lang/Object;
.source "HoverChatHeadView.java"


# virtual methods
.method public abstract getKey()Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract getParentX()D
.end method

.method public abstract getX()D
.end method

.method public abstract getXChangedEvents()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract render(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V
.end method
