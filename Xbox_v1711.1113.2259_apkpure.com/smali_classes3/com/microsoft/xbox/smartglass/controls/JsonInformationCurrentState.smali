.class Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;
.super Lorg/json/JSONObject;
.source "JsonInformationCurrentState.java"


# direct methods
.method public constructor <init>(ILjava/util/Collection;ZZZZLjava/lang/String;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "activeTitleId"    # I
    .param p3, "isUsingLocalConnection"    # Z
    .param p4, "isConnected"    # Z
    .param p5, "isIdentityPaired"    # Z
    .param p6, "isTitleChannelEstablished"    # Z
    .param p7, "locale"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lorg/json/JSONObject;",
            ">;ZZZZ",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 23
    .local p2, "activeTitles":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/json/JSONObject;>;"
    .local p8, "associatedChannels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 24
    const-string v0, "activeTitleId"

    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/controls/Json;->unsignedInt(I)J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 25
    const-string v0, "activeTitles"

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 26
    const-string v0, "isUsingLocalConnection"

    invoke-virtual {p0, v0, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 27
    const-string v0, "isConnected"

    invoke-virtual {p0, v0, p4}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 28
    const-string v0, "isIdentityPaired"

    invoke-virtual {p0, v0, p5}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 29
    const-string v0, "isTitleChannelEstablished"

    invoke-virtual {p0, v0, p6}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 30
    const-string v0, "locale"

    invoke-virtual {p0, v0, p7}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 31
    const-string v0, "associatedChannels"

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p8}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonInformationCurrentState;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32
    return-void
.end method
