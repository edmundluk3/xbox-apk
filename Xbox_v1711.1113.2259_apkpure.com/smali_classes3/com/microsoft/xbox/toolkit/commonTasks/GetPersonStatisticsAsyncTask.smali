.class public Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GetPersonStatisticsAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final actionRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
            ">;>;"
        }
    .end annotation
.end field

.field private final individualStats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;",
            ">;"
        }
    .end annotation
.end field

.field private final slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

.field private final statGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final xuids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "statGroups":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;>;"
    .local p3, "individualStats":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;>;"
    .local p4, "callbacks":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 36
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 37
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 38
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 39
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->xuids:Ljava/util/List;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->statGroups:Ljava/util/List;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->individualStats:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->actionRef:Ljava/lang/ref/WeakReference;

    .line 46
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .locals 5

    .prologue
    .line 64
    new-instance v1, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->xuids:Ljava/util/List;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->statGroups:Ljava/util/List;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->individualStats:Ljava/util/List;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 67
    .local v1, "request":Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->getProfileStatisticsRequestBody(Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getProfileStatisticsInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 72
    :goto_0
    return-object v2

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->TAG:Ljava/lang/String;

    const-string v3, "Failed to load user stats"

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->onError()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;)V
    .locals 2
    .param p1, "profileStatisticsResult"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    .prologue
    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->actionRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 83
    .local v0, "callback":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;>;"
    if-eqz v0, :cond_0

    .line 84
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 86
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonStatisticsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
