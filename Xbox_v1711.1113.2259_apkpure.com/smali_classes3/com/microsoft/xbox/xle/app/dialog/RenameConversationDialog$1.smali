.class Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;
.super Ljava/lang/Object;
.source "RenameConversationDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->setRenameEditText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field renameStr:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->renameStr:Ljava/lang/String;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->renameStr:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;Ljava/lang/String;)V

    .line 80
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 61
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->renameStr:Ljava/lang/String;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;->renameStr:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;Ljava/lang/String;)V

    .line 70
    :cond_0
    return-void
.end method
