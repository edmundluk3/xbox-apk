.class public interface abstract Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;
.super Ljava/lang/Object;
.source "MviPresenter.java"


# annotations
.annotation runtime Lcom/microsoft/xbox/toolkit/java8/FunctionalInterface;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/base/MviPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60c
    name = "ViewIntentBinder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        "VI:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract bind(Ljava/lang/Object;)Lio/reactivex/Observable;
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lio/reactivex/Observable",
            "<+TVI;>;"
        }
    .end annotation
.end method
