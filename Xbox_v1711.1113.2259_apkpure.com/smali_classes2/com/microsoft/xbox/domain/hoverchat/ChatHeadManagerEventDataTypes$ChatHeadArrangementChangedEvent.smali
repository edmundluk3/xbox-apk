.class public abstract Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;
.super Ljava/lang/Object;
.source "ChatHeadManagerEventDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ChatHeadArrangementChangedEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;
    .locals 1
    .param p0, "old"    # Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .param p1, "newArrangement"    # Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ")",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)V

    return-object v0
.end method


# virtual methods
.method public abstract newArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract oldArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
