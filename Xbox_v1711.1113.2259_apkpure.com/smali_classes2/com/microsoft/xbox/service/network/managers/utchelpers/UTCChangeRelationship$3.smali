.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;
.super Ljava/lang/Object;
.source "UTCChangeRelationship.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->trackChangeRelationshipDone(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$state:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;

.field final synthetic val$xuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;->val$xuid:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;->val$state:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;->val$xuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;->val$state:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 118
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 119
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "RelationshipStatus"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;->val$state:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->getCurrentRelationshipStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 120
    const-string v1, "FavoriteStatus"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;->val$state:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->getCurrentFavoriteStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 121
    const-string v1, "RealNameStatus"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;->val$state:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->getCurrentRealNameStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 122
    const-string v1, "FriendType"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;->val$state:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->getCurrentGamerType()Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 123
    const-string v1, "TargetXuid"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;->val$xuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 125
    const-string v1, "Change Relationship - Done"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 126
    return-void
.end method
