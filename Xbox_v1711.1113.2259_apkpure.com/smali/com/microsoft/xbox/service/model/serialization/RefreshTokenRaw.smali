.class public Lcom/microsoft/xbox/service/model/serialization/RefreshTokenRaw;
.super Ljava/lang/Object;
.source "RefreshTokenRaw.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    name = "RefreshToken"
.end annotation


# instance fields
.field public Expires:Ljava/util/Date;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = true
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/convert/Convert;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;
    .end annotation
.end field

.field public Scope:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = true
    .end annotation
.end field

.field public Token:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = true
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
