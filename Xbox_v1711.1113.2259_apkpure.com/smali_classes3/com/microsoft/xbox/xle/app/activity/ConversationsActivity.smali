.class public Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ConversationsActivity.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method


# virtual methods
.method public adjustBottomMargin(I)V
    .locals 0
    .param p1, "bottomMargin"    # I

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;->forceAdjustBottomMargin(I)V

    .line 44
    return-void
.end method

.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const-string v0, "Messaging - Conversations View"

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070441

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 24
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;->onCreateContentView()V

    .line 26
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 27
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 31
    const v0, 0x7f0300c2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;->setContentView(I)V

    .line 32
    return-void
.end method
