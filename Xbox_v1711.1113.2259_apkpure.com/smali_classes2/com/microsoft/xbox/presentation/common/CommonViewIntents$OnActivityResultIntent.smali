.class public abstract Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;
.super Ljava/lang/Object;
.source "CommonViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/common/CommonViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OnActivityResultIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(IILandroid/content/Intent;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$OnActivityResultIntent;
    .locals 1
    .param p0, "requestCode"    # I
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 122
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 124
    new-instance v0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_OnActivityResultIntent;-><init>(IILandroid/content/Intent;)V

    return-object v0
.end method


# virtual methods
.method public abstract data()Landroid/content/Intent;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract requestCode()I
.end method

.method public abstract resultCode()I
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
