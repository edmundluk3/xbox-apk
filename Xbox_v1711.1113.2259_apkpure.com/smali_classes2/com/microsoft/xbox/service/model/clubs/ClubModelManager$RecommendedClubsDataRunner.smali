.class Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ClubModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecommendedClubsDataRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

.field private final criteria:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final maxItems:I

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

.field private final titleId:J


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;JI)V
    .locals 2
    .param p2, "titleId"    # J
    .param p4, "maxItems"    # I

    .prologue
    .line 228
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 217
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubHubService()Lcom/microsoft/xbox/service/clubs/IClubHubService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->criteria:Ljava/util/List;

    .line 230
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->titleId:J

    .line 231
    iput p4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->maxItems:I

    .line 232
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;JILcom/microsoft/xbox/service/model/clubs/ClubModelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
    .param p2, "x1"    # J
    .param p4, "x2"    # I
    .param p5, "x3"    # Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$1;

    .prologue
    .line 216
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;JI)V

    return-void
.end method

.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;I)V
    .locals 2
    .param p3, "maxItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 222
    .local p2, "criteria":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 217
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubHubService()Lcom/microsoft/xbox/service/clubs/IClubHubService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    .line 223
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->criteria:Ljava/util/List;

    .line 224
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->titleId:J

    .line 225
    iput p3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->maxItems:I

    .line 226
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;ILcom/microsoft/xbox/service/model/clubs/ClubModelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
    .param p2, "x1"    # Ljava/util/List;
    .param p3, "x2"    # I
    .param p4, "x3"    # Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$1;

    .prologue
    .line 216
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->buildData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 242
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->criteria:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 243
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->criteria:Ljava/util/List;

    iget v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->maxItems:I

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/IClubHubService;->getRecommendedClubs(Ljava/util/List;I)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    move-result-object v0

    .line 251
    .local v0, "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;->clubs()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    :goto_1
    return-object v1

    .line 244
    .end local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    :cond_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->titleId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 245
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->titleId:J

    iget v4, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->maxItems:I

    invoke-interface {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/IClubHubService;->getRecommendedClubs(JI)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    move-result-object v0

    .restart local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    goto :goto_0

    .line 247
    .end local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    :cond_1
    const-string v1, "Unexpected condition"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 248
    const/4 v0, 0x0

    .restart local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    goto :goto_0

    .line 251
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 261
    const-wide/16 v0, 0x2580

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$RecommendedClubsDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->access$200(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/util/List;)V

    .line 257
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 237
    return-void
.end method
