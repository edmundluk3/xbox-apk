.class public Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;
.super Ljava/lang/Object;
.source "ChatTicket.java"


# static fields
.field private static final CHANNELID_OFFSET:I = 0x14

.field public static final CHATTICKET_SIZE:I = 0x3c

.field private static final CREATIONTIME_OFFSET:I = 0x4

.field private static final EXPIRETIME_OFFSET:I = 0xc

.field private static final FLAGS_OFFSET:I = 0x38

.field private static final HISTORYSTARTMESSAGEID_OFFSET:I = 0x30

.field private static final MODERATE_FLAG:I = 0x4

.field private static final READ_FLAG:I = 0x1

.field private static final SET_MOTD_FLAG:I = 0x8

.field private static final TAG:Ljava/lang/String;

.field public static final VERSION:I = 0x2

.field private static final VERSION_OFFSET:I = 0x0

.field private static final WRITE_FLAG:I = 0x2

.field private static final XUID_OFFSET:I = 0x28


# instance fields
.field private chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

.field private creationTime:J

.field private expireTime:J

.field private flags:I

.field private volatile transient hashCode:I

.field private historyStartMessageId:J

.field private version:I

.field private xuid:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(IJJLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;JJI)V
    .locals 0
    .param p1, "version"    # I
    .param p2, "creationTime"    # J
    .param p4, "expireTime"    # J
    .param p6, "chatChannelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p7, "xuid"    # J
    .param p9, "historyStartMessageId"    # J
    .param p11, "flags"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 54
    iput p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->version:I

    .line 55
    iput-wide p2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->creationTime:J

    .line 56
    iput-wide p4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->expireTime:J

    .line 57
    iput-object p6, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 58
    iput-wide p7, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->xuid:J

    .line 59
    iput-wide p9, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->historyStartMessageId:J

    .line 60
    iput p11, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->flags:I

    .line 61
    return-void
.end method

.method public static readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;
    .locals 12
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "offset"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 109
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 110
    const-wide/16 v0, 0x0

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 112
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    sub-int/2addr v0, p1

    const/16 v1, 0x3c

    if-ge v0, v1, :cond_0

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "not enough buffer space to read ChatHeader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

    add-int/lit8 v1, p1, 0x0

    .line 117
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    add-int/lit8 v2, p1, 0x4

    .line 118
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v2

    add-int/lit8 v4, p1, 0xc

    .line 119
    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v4

    add-int/lit8 v6, p1, 0x14

    .line 120
    invoke-static {p0, v6}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v6

    add-int/lit8 v7, p1, 0x28

    .line 121
    invoke-virtual {p0, v7}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v7

    add-int/lit8 v9, p1, 0x30

    .line 122
    invoke-virtual {p0, v9}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v9

    add-int/lit8 v11, p1, 0x38

    .line 123
    invoke-virtual {p0, v11}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v11

    invoke-direct/range {v0 .. v11}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;-><init>(IJJLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;JJI)V

    .line 116
    return-object v0
.end method


# virtual methods
.method public canModerate()Z
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canRead()Z
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canSetMessageOfTheDay()Z
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->flags:I

    and-int/lit8 v0, v0, 0x8

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canWrite()Z
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 128
    if-ne p1, p0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v1

    .line 130
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

    if-nez v3, :cond_2

    move v1, v2

    .line 131
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 133
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

    .line 134
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;
    iget v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->version:I

    iget v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->version:I

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->creationTime:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->creationTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->expireTime:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->expireTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 137
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->xuid:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->xuid:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->historyStartMessageId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->historyStartMessageId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->flags:I

    iget v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->flags:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getChatChannelId()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    return-object v0
.end method

.method public getCreationTime()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->creationTime:J

    return-wide v0
.end method

.method public getExpireTime()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->expireTime:J

    return-wide v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->flags:I

    return v0
.end method

.method public getHistoryStartMessageId()J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->historyStartMessageId:J

    return-wide v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->version:I

    return v0
.end method

.method public getXuid()J
    .locals 2

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->xuid:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 146
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    if-nez v0, :cond_0

    .line 147
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    .line 148
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->version:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    .line 149
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->creationTime:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    .line 150
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->expireTime:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    .line 151
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->chatChannelId:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    .line 152
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->xuid:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    .line 153
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->historyStartMessageId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    .line 154
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->flags:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    .line 157
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
