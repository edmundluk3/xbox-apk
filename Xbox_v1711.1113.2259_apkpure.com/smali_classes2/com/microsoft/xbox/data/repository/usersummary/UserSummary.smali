.class public abstract Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
.super Ljava/lang/Object;
.source "UserSummary.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;Z)Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .locals 12
    .param p0, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "realName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "displayPicUri"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5, "presenceText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6, "gamerScore"    # J
    .param p8, "colors"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p9, "inParty"    # Z

    .prologue
    .line 45
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 46
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 47
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 48
    invoke-static/range {p4 .. p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 49
    invoke-static/range {p5 .. p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 50
    invoke-static/range {p8 .. p8}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 52
    new-instance v1, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-wide/from16 v8, p6

    move/from16 v10, p9

    move-object/from16 v11, p8

    invoke-direct/range {v1 .. v11}, Lcom/microsoft/xbox/data/repository/usersummary/AutoValue_UserSummary;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZLcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V

    return-object v1
.end method


# virtual methods
.method public abstract colors()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract displayPicUri()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract gamerScore()J
.end method

.method public abstract gamertag()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract inParty()Z
.end method

.method public abstract presenceText()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract realName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract xuid()J
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x1L
    .end annotation
.end method
