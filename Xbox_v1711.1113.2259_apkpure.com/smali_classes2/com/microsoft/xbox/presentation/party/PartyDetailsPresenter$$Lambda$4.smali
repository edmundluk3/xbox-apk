.class final synthetic Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$4;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;


# static fields
.field private static final instance:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$4;

    invoke-direct {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$4;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$4;->instance:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$4;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$4;->instance:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$4;

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsView;

    check-cast p1, Lcom/microsoft/xbox/presentation/base/MviView;

    check-cast p2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    invoke-interface {p1, p2}, Lcom/microsoft/xbox/presentation/base/MviView;->render(Ljava/lang/Object;)V

    return-void
.end method
