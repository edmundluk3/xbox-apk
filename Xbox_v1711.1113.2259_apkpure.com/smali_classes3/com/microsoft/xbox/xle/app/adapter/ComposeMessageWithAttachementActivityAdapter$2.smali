.class Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;
.super Ljava/lang/Object;
.source "ComposeMessageWithAttachementActivityAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 90
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getIsRecipientNonEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->sendToConversation()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;Z)V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->isFromConversationDetailScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;Z)V

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setCharacterCountVisibility(I)V

    .line 100
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 90
    goto :goto_0

    .line 96
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;Z)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setCharacterCountVisibility(I)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 103
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 106
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "messageText":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x1

    .line 109
    .local v0, "enableSendButton":Z
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;Z)V

    .line 110
    return-void

    .line 107
    .end local v0    # "enableSendButton":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
