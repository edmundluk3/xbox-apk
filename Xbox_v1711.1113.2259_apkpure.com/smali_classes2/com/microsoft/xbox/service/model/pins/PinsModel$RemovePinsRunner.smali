.class Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "PinsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/pins/PinsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemovePinsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final pinItems:[Lcom/microsoft/xbox/service/model/pins/PinItem;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;


# direct methods
.method public varargs constructor <init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V
    .locals 0
    .param p2, "pinItems"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;

    .prologue
    .line 404
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 405
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;->pinItems:[Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 406
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 414
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getESServiceManager()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v2

    .line 416
    .local v2, "manager":Lcom/microsoft/xbox/service/network/managers/IESServiceManager;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 417
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;->pinItems:[Lcom/microsoft/xbox/service/model/pins/PinItem;

    array-length v7, v6

    move v4, v5

    :goto_0
    if-ge v4, v7, :cond_0

    aget-object v0, v6, v4

    .line 418
    .local v0, "item":Lcom/microsoft/xbox/service/model/pins/PinItem;
    invoke-static {v0, v5}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getEPListPinItem(Lcom/microsoft/xbox/service/model/pins/PinItem;Z)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v3

    .line 419
    .local v3, "newItem":Lcom/microsoft/xbox/service/model/pins/PinItem;
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 423
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/pins/PinItem;
    .end local v3    # "newItem":Lcom/microsoft/xbox/service/model/pins/PinItem;
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-interface {v2, v4}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->delete([Lcom/microsoft/xbox/service/model/pins/PinItem;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    return-object v4
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 433
    const-wide/16 v0, 0xfbc

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 428
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$RemovePinsRunner;->pinItems:[Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$1000(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    .line 429
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 410
    return-void
.end method
