.class public final enum Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;
.super Ljava/lang/Enum;
.source "PrimaryDeviceFlags.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

.field public static final enum AllowAnonymousUsers:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

.field public static final enum AllowAuthenticatedUsers:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

.field public static final enum AllowConsoleUsers:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

.field public static final enum CertificateRequestPending:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

.field public static final enum None:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    const-string v3, "None"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->None:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    const-string v3, "AllowConsoleUsers"

    invoke-direct {v2, v3, v5, v5}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->AllowConsoleUsers:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    const-string v3, "AllowAuthenticatedUsers"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->AllowAuthenticatedUsers:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    const-string v3, "AllowAnonymousUsers"

    invoke-direct {v2, v3, v8, v7}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->AllowAnonymousUsers:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    const-string v3, "CertificateRequestPending"

    const/16 v4, 0x8

    invoke-direct {v2, v3, v7, v4}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->CertificateRequestPending:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    .line 12
    const/4 v2, 0x5

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    sget-object v3, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->None:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->AllowConsoleUsers:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    aput-object v3, v2, v5

    sget-object v3, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->AllowAuthenticatedUsers:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->AllowAnonymousUsers:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->CertificateRequestPending:Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    aput-object v3, v2, v7

    sput-object v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->$VALUES:[Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    .line 25
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->_lookup:Landroid/util/SparseArray;

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->values()[Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    move-result-object v2

    array-length v3, v2

    .local v0, "flags":Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 29
    sget-object v4, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 28
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 31
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p3, p0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->_value:I

    .line 35
    return-void
.end method

.method public static fromInt(I)Ljava/util/EnumSet;
    .locals 6
    .param p0, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    const-class v2, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 52
    .local v1, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;>;"
    invoke-static {}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->values()[Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 53
    .local v0, "flags":Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->getValue()I

    move-result v5

    and-int/2addr v5, p0

    if-eqz v5, :cond_0

    .line 54
    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 58
    .end local v0    # "flags":Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;
    :cond_1
    return-object v1
.end method

.method public static getValue(Ljava/util/EnumSet;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "deviceFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;>;"
    const/4 v1, 0x0

    .line 69
    .local v1, "value":I
    if-eqz p0, :cond_0

    .line 70
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    .line 71
    .local v0, "flags":Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->getValue()I

    move-result v3

    or-int/2addr v1, v3

    .line 72
    goto :goto_0

    .line 75
    .end local v0    # "flags":Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;
    :cond_0
    return v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->$VALUES:[Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/microsoft/xbox/smartglass/PrimaryDeviceFlags;->_value:I

    return v0
.end method
