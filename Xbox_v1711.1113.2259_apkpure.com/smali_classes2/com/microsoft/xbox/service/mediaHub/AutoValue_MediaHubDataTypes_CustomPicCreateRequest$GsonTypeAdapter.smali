.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_CustomPicCreateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultExpectedBlocks:J

.field private defaultFileSize:J

.field private defaultInitialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

.field private final expectedBlocksAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final fileSizeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final initialMetaDataAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 2
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const-wide/16 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 23
    iput-wide v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->defaultExpectedBlocks:J

    .line 24
    iput-wide v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->defaultFileSize:J

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->defaultInitialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    .line 27
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->expectedBlocksAdapter:Lcom/google/gson/TypeAdapter;

    .line 28
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->fileSizeAdapter:Lcom/google/gson/TypeAdapter;

    .line 29
    const-class v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->initialMetaDataAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
    .locals 8
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v7, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v1, v7, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 63
    const/4 v1, 0x0

    .line 94
    :goto_0
    return-object v1

    .line 65
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 66
    iget-wide v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->defaultExpectedBlocks:J

    .line 67
    .local v2, "expectedBlocks":J
    iget-wide v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->defaultFileSize:J

    .line 68
    .local v4, "fileSize":J
    iget-object v6, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->defaultInitialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    .line 69
    .local v6, "initialMetaData":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 70
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v7, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v1, v7, :cond_1

    .line 72
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 75
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 89
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 75
    :sswitch_0
    const-string v7, "expectedBlocks"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string v7, "fileSize"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :sswitch_2
    const-string v7, "initialMetaData"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v1, 0x2

    goto :goto_2

    .line 77
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->expectedBlocksAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 78
    goto :goto_1

    .line 81
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->fileSizeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 82
    goto :goto_1

    .line 85
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->initialMetaDataAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "initialMetaData":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;
    check-cast v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    .line 86
    .restart local v6    # "initialMetaData":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;
    goto :goto_1

    .line 93
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 94
    new-instance v1, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest;

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest;-><init>(JJLcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;)V

    goto :goto_0

    .line 75
    nop

    :sswitch_data_0
    .sparse-switch
        -0x362ce9a2 -> :sswitch_0
        -0x2bd7d463 -> :sswitch_1
        0x500126b3 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultExpectedBlocks(J)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultExpectedBlocks"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->defaultExpectedBlocks:J

    .line 33
    return-object p0
.end method

.method public setDefaultFileSize(J)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultFileSize"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->defaultFileSize:J

    .line 37
    return-object p0
.end method

.method public setDefaultInitialMetaData(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultInitialMetaData"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->defaultInitialMetaData:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    .line 41
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    if-nez p2, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 58
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 51
    const-string v0, "expectedBlocks"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->expectedBlocksAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;->expectedBlocks()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 53
    const-string v0, "fileSize"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->fileSizeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;->fileSize()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 55
    const-string v0, "initialMetaData"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->initialMetaDataAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;->initialMetaData()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 57
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicCreateRequest$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicCreateRequest;)V

    return-void
.end method
