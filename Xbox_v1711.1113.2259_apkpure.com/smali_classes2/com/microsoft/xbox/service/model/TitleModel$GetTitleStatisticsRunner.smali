.class Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetTitleStatisticsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/TitleModel;

.field private statsToGet:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;

.field private titleId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "titleId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/TitleModel;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1422
    .local p5, "statsToGet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1420
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->statsToGet:Ljava/util/List;

    .line 1423
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 1424
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->xuid:Ljava/lang/String;

    .line 1425
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->titleId:Ljava/lang/String;

    .line 1427
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1428
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->statsToGet:Ljava/util/List;

    invoke-interface {v0, p5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1430
    :cond_0
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1434
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1435
    .local v7, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->xuid:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1438
    .local v2, "groups":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;>;"
    new-instance v8, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;

    sget-object v9, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->Hero:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->titleId:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest$StatisticsGroup;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1440
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1441
    .local v6, "stats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;>;"
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->statsToGet:Ljava/util/List;

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1442
    iget-object v8, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->statsToGet:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1443
    .local v5, "statType":Ljava/lang/String;
    new-instance v9, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;

    iget-object v10, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->titleId:Ljava/lang/String;

    invoke-direct {v9, v5, v10}, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1447
    .end local v5    # "statType":Ljava/lang/String;
    :cond_0
    new-instance v3, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;

    invoke-direct {v3, v7, v2, v6}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 1448
    .local v3, "profileStatisticsRequest":Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v8

    .line 1449
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;->getProfileStatisticsRequestBody(Lcom/microsoft/xbox/service/model/sls/ProfileStatisticsRequest;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getProfileStatisticsInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v4

    .line 1451
    .local v4, "profileStatisticsResult":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    if-eqz v4, :cond_2

    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    if-eqz v8, :cond_2

    .line 1452
    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->groups:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;

    .line 1453
    .local v1, "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    if-eqz v1, :cond_1

    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->statlistscollection:Ljava/util/ArrayList;

    if-eqz v9, :cond_1

    .line 1454
    iget-object v9, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;->statlistscollection:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 1455
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    iget-object v10, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    new-instance v11, Lcom/microsoft/xbox/service/model/TitleModel$HeroStatComparator;

    iget-object v12, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    const/4 v13, 0x0

    invoke-direct {v11, v12, v13}, Lcom/microsoft/xbox/service/model/TitleModel$HeroStatComparator;-><init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel$1;)V

    invoke-static {v10, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    .line 1461
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    .end local v1    # "group":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticGroup;
    :cond_2
    return-object v4
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1415
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1475
    const-wide/16 v0, 0xbf6

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1470
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetTitleStatisticsRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->access$1200(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1471
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1466
    return-void
.end method
