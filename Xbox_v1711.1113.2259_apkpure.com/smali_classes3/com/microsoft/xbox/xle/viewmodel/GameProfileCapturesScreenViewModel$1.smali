.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$1;
.super Ljava/lang/Object;
.source "GameProfileCapturesScreenViewModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->onLoadCapturesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ICapture;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/service/network/managers/ICapture;Lcom/microsoft/xbox/service/network/managers/ICapture;)I
    .locals 3
    .param p1, "f0"    # Lcom/microsoft/xbox/service/network/managers/ICapture;
    .param p2, "f1"    # Lcom/microsoft/xbox/service/network/managers/ICapture;

    .prologue
    .line 280
    invoke-interface {p2}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getViews()I

    move-result v1

    invoke-interface {p1}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getViews()I

    move-result v2

    sub-int v0, v1, v2

    .line 281
    .local v0, "viewDiff":I
    if-eqz v0, :cond_0

    .line 284
    .end local v0    # "viewDiff":I
    :goto_0
    return v0

    .restart local v0    # "viewDiff":I
    :cond_0
    invoke-interface {p2}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getDate()Ljava/util/Date;

    move-result-object v1

    invoke-interface {p1}, Lcom/microsoft/xbox/service/network/managers/ICapture;->getDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 277
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/ICapture;

    check-cast p2, Lcom/microsoft/xbox/service/network/managers/ICapture;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel$1;->compare(Lcom/microsoft/xbox/service/network/managers/ICapture;Lcom/microsoft/xbox/service/network/managers/ICapture;)I

    move-result v0

    return v0
.end method
