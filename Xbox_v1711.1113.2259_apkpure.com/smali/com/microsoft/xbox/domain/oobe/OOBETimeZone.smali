.class public final enum Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;
.super Ljava/lang/Enum;
.source "OOBETimeZone.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Abu_Dhabi_Muscat:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Adelaide:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Alaska:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Aleutian_Islands:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Amman:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Amsterdam_Berlin_Bern_Rome_Stockholm_Vienna:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Anadyr_Petropavlovsk_Kamchatsky:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Araguaina:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Arizona:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Ashgabat_Tashkent:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Astana:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Astrakhan_Ulyanovsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Asuncion:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Athens_Bucharest:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Atlantic_Time_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Auckland_Wellington:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Azores:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Baghdad:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Baja_California:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Baku:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Bangkok_Hanoi_Jakarta:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Barnaul_Gorno_Altaysk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Beijing_Chongqing_Hong_Kong_Urumqi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Beirut:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Belgrade_Bratislava_Budapest_Ljubljana_Prague:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Bogota_Lima_Quito_Rio_Branco:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Bougainville_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Brasilia:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Brisbane:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Brussels_Copenhagen_Madrid_Paris:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Cabo_Verde_Is:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Cairo:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Canberra_Melbourne_Sydney:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Caracas:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Casablanca:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Cayenne_Fortaleza:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Central_America:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Central_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Chatham_Islands:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Chennai_Kolkata_Mumbai_New_Delhi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Chetumal:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Chihuahua_La_Paz_Mazatlan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Chisinau:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Chita:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Chokurdakh:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_City_of_Buenos_Aires:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Coordinated_Universal_Time:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Coordinated_Universal_Time_02:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Coordinated_Universal_Time_08:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Coordinated_Universal_Time_09:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Coordinated_Universal_Time_11:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Coordinated_Universal_Time_12:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Coordinated_Universal_Time_13:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Cuiaba:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Damascus:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Darwin:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Dhaka:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Dublin_Edinburgh_Lisbon_London:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Easter_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Eastern_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Ekaterinburg:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Eucla:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Fiji:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Gaza_Hebron:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Georgetown_La_Paz_Manaus_San_Juan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Greenland:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Guadalajara_Mexico_City_Monterrey:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Guam_Port_Moresby:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Haiti:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Harare_Pretoria:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Havanah:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Hawaii:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Helsinki_Kyiv_Riga_Sofia_Tallinn_Vilnius:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Hobart:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Hovd:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Indiana_East:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_International_Date_Line_West:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Irkutsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Islamabad_Karachi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Istanbul:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Izhevsk_Samara:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Jerusalem:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Kabul:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Kaliningrad:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Kathmandu:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Kiritimati_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Krasnoyarsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Kuala_Lumpur_Singapore:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Kuwait_Riyadh:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Lord_Howe_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Magadan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Marquesas_Islands:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Minsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Monrovia_Reykjavik:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Montevideo:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Moscow_St_Petersburg_Volgograd:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Mountain_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Nairobi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Newfoundland:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Norfolk_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Novosibirsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Nukualofa:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Omsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Osaka_Sapporo_Tokyo:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Pacific_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Perth:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Port_Louis:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Pyongyang:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Saint_Pierre_and_Miquelon:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Sakhalin:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Salvador:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Samoa:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Santiago:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Sarajevo_Skopje_Warsaw_Zagreb:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Saratov:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Saskatchewan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Seoul:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Solomon_Is_New_Caledonia:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Sri_Jayawardenepura:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Taipei:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Tehran:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Tibilis:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Tomsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Tripoli:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Turks_and_Caicos:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Ulaanbaatar:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Vladivostok:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_West_Central_Africa:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Windhoek:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Yakutsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Yangon_Rangoon:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

.field public static final enum OOBE_Timezone_Yeravan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;


# instance fields
.field public serializedName:Ljava/lang/String;

.field public stringID:I
    .annotation runtime Ljavax/annotation/Resource;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 8
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_International_Date_Line_West"

    const v2, 0x7f070853

    const-string v3, "Dateline Standard Time"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_International_Date_Line_West:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 9
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Coordinated_Universal_Time_11"

    const v2, 0x7f070839

    const-string v3, "UTC-11"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_11:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 10
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Aleutian_Islands"

    const v2, 0x7f07080a

    const-string v3, "Aleutian Standard Time"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Aleutian_Islands:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 11
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Hawaii"

    const v2, 0x7f07084e

    const-string v3, "Hawaiian Standard Time"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Hawaii:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Marquesas_Islands"

    const v2, 0x7f070862

    const-string v3, "Marquesas Standard Time"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Marquesas_Islands:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Alaska"

    const/4 v2, 0x5

    const v3, 0x7f070809

    const-string v4, "Alaskan Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Alaska:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Coordinated_Universal_Time_09"

    const/4 v2, 0x6

    const v3, 0x7f070838

    const-string v4, "UTC-09"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_09:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Baja_California"

    const/4 v2, 0x7

    const v3, 0x7f070819

    const-string v4, "Pacific Standard Time (Mexico)"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Baja_California:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Coordinated_Universal_Time_08"

    const/16 v2, 0x8

    const v3, 0x7f070837

    const-string v4, "UTC-08"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_08:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Pacific_Time_US_Canada"

    const/16 v2, 0x9

    const v3, 0x7f07086f

    const-string v4, "Pacific Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Pacific_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Arizona"

    const/16 v2, 0xa

    const v3, 0x7f07080f

    const-string v4, "US Mountain Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Arizona:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Chihuahua_La_Paz_Mazatlan"

    const/16 v2, 0xb

    const v3, 0x7f070830

    const-string v4, "Mountain Standard Time (Mexico)"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chihuahua_La_Paz_Mazatlan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Mountain_Time_US_Canada"

    const/16 v2, 0xc

    const v3, 0x7f070867

    const-string v4, "Mountain Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Mountain_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Central_America"

    const/16 v2, 0xd

    const v3, 0x7f07082b

    const-string v4, "Central America Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Central_America:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Central_Time_US_Canada"

    const/16 v2, 0xe

    const v3, 0x7f07082c

    const-string v4, "Central Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Central_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Easter_Island"

    const/16 v2, 0xf

    const v3, 0x7f070841

    const-string v4, "Easter Island Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Easter_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Guadalajara_Mexico_City_Monterrey"

    const/16 v2, 0x10

    const v3, 0x7f070849

    const-string v4, "Central Standard Time (Mexico)"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Guadalajara_Mexico_City_Monterrey:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Saskatchewan"

    const/16 v2, 0x11

    const v3, 0x7f07087a

    const-string v4, "Canada Central Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Saskatchewan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Bogota_Lima_Quito_Rio_Branco"

    const/16 v2, 0x12

    const v3, 0x7f070820

    const-string v4, "SA Pacific Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Bogota_Lima_Quito_Rio_Branco:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Chetumal"

    const/16 v2, 0x13

    const v3, 0x7f07082f

    const-string v4, "Eastern Standard Time (Mexico)"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chetumal:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Eastern_Time_US_Canada"

    const/16 v2, 0x14

    const v3, 0x7f070842

    const-string v4, "Eastern Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Eastern_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Haiti"

    const/16 v2, 0x15

    const v3, 0x7f07084b

    const-string v4, "Haiti Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Haiti:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Havanah"

    const/16 v2, 0x16

    const v3, 0x7f07084d

    const-string v4, "Cuba Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Havanah:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Indiana_East"

    const/16 v2, 0x17

    const v3, 0x7f070852

    const-string v4, "US Eastern Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Indiana_East:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Asuncion"

    const/16 v2, 0x18

    const v3, 0x7f070813

    const-string v4, "Paraguay Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Asuncion:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Atlantic_Time_Canada"

    const/16 v2, 0x19

    const v3, 0x7f070815

    const-string v4, "Atlantic Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Atlantic_Time_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Caracas"

    const/16 v2, 0x1a

    const v3, 0x7f070828

    const-string v4, "Venezuela Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Caracas:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Cuiaba"

    const/16 v2, 0x1b

    const v3, 0x7f07083c

    const-string v4, "Central Brazilian Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Cuiaba:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Georgetown_La_Paz_Manaus_San_Juan"

    const/16 v2, 0x1c

    const v3, 0x7f070847

    const-string v4, "SA Western Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Georgetown_La_Paz_Manaus_San_Juan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Santiago"

    const/16 v2, 0x1d

    const v3, 0x7f070877

    const-string v4, "Pacific SA Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Santiago:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Turks_and_Caicos"

    const/16 v2, 0x1e

    const v3, 0x7f070883

    const-string v4, "Turks And Caicos Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Turks_and_Caicos:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Newfoundland"

    const/16 v2, 0x1f

    const v3, 0x7f070869

    const-string v4, "Newfoundland Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Newfoundland:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Araguaina"

    const/16 v2, 0x20

    const v3, 0x7f07080e

    const-string v4, "Tocantins Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Araguaina:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Brasilia"

    const/16 v2, 0x21

    const v3, 0x7f070822

    const-string v4, "E. South America Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Brasilia:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Cayenne_Fortaleza"

    const/16 v2, 0x22

    const v3, 0x7f07082a

    const-string v4, "SA Eastern Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Cayenne_Fortaleza:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_City_of_Buenos_Aires"

    const/16 v2, 0x23

    const v3, 0x7f070834

    const-string v4, "Argentina Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_City_of_Buenos_Aires:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Greenland"

    const/16 v2, 0x24

    const v3, 0x7f070848

    const-string v4, "Greenland Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Greenland:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 45
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Montevideo"

    const/16 v2, 0x25

    const v3, 0x7f070865

    const-string v4, "Montevideo Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Montevideo:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 46
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Saint_Pierre_and_Miquelon"

    const/16 v2, 0x26

    const v3, 0x7f070873

    const-string v4, "Saint Pierre Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Saint_Pierre_and_Miquelon:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Salvador"

    const/16 v2, 0x27

    const v3, 0x7f070875

    const-string v4, "Bahia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Salvador:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 48
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Coordinated_Universal_Time_02"

    const/16 v2, 0x28

    const v3, 0x7f070836

    const-string v4, "UTC-02"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_02:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Azores"

    const/16 v2, 0x29

    const v3, 0x7f070817

    const-string v4, "Azores Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Azores:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Cabo_Verde_Is"

    const/16 v2, 0x2a

    const v3, 0x7f070825

    const-string v4, "Cape Verde Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Cabo_Verde_Is:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Coordinated_Universal_Time"

    const/16 v2, 0x2b

    const v3, 0x7f070835

    const-string v4, "UTC"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Casablanca"

    const/16 v2, 0x2c

    const v3, 0x7f070829

    const-string v4, "Morocco Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Casablanca:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 53
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Dublin_Edinburgh_Lisbon_London"

    const/16 v2, 0x2d

    const v3, 0x7f070840

    const-string v4, "GMT Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Dublin_Edinburgh_Lisbon_London:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 54
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Monrovia_Reykjavik"

    const/16 v2, 0x2e

    const v3, 0x7f070864

    const-string v4, "Greenwich Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Monrovia_Reykjavik:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 55
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Amsterdam_Berlin_Bern_Rome_Stockholm_Vienna"

    const/16 v2, 0x2f

    const v3, 0x7f07080c

    const-string v4, "W. Europe Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Amsterdam_Berlin_Bern_Rome_Stockholm_Vienna:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 56
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Belgrade_Bratislava_Budapest_Ljubljana_Prague"

    const/16 v2, 0x30

    const v3, 0x7f07081f

    const-string v4, "Central Europe Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Belgrade_Bratislava_Budapest_Ljubljana_Prague:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 57
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Brussels_Copenhagen_Madrid_Paris"

    const/16 v2, 0x31

    const v3, 0x7f070824

    const-string v4, "Romance Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Brussels_Copenhagen_Madrid_Paris:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 58
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Sarajevo_Skopje_Warsaw_Zagreb"

    const/16 v2, 0x32

    const v3, 0x7f070878

    const-string v4, "Central European Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Sarajevo_Skopje_Warsaw_Zagreb:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 59
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_West_Central_Africa"

    const/16 v2, 0x33

    const v3, 0x7f070886

    const-string v4, "W. Central Africa Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_West_Central_Africa:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 60
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Windhoek"

    const/16 v2, 0x34

    const v3, 0x7f070887

    const-string v4, "Namibia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Windhoek:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 61
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Amman"

    const/16 v2, 0x35

    const v3, 0x7f07080b

    const-string v4, "Jordan Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Amman:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 62
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Athens_Bucharest"

    const/16 v2, 0x36

    const v3, 0x7f070814

    const-string v4, "GTB Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Athens_Bucharest:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 63
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Beirut"

    const/16 v2, 0x37

    const v3, 0x7f07081e

    const-string v4, "Middle East Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Beirut:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 64
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Cairo"

    const/16 v2, 0x38

    const v3, 0x7f070826

    const-string v4, "Egypt Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Cairo:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 65
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Chisinau"

    const/16 v2, 0x39

    const v3, 0x7f070831

    const-string v4, "E. Europe Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chisinau:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 66
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Damascus"

    const/16 v2, 0x3a

    const v3, 0x7f07083d

    const-string v4, "Syria Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Damascus:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 67
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Gaza_Hebron"

    const/16 v2, 0x3b

    const v3, 0x7f070846

    const-string v4, "West Bank Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Gaza_Hebron:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 68
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Harare_Pretoria"

    const/16 v2, 0x3c

    const v3, 0x7f07084c

    const-string v4, "South Africa Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Harare_Pretoria:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 69
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Helsinki_Kyiv_Riga_Sofia_Tallinn_Vilnius"

    const/16 v2, 0x3d

    const v3, 0x7f07084f

    const-string v4, "FLE Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Helsinki_Kyiv_Riga_Sofia_Tallinn_Vilnius:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 70
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Jerusalem"

    const/16 v2, 0x3e

    const v3, 0x7f070858

    const-string v4, "Israel Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Jerusalem:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 71
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Kaliningrad"

    const/16 v2, 0x3f

    const v3, 0x7f07085a

    const-string v4, "Kaliningrad Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kaliningrad:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 72
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Tripoli"

    const/16 v2, 0x40

    const v3, 0x7f070882

    const-string v4, "Libya Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Tripoli:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 73
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Baghdad"

    const/16 v2, 0x41

    const v3, 0x7f070818

    const-string v4, "Arabic Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Baghdad:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 74
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Istanbul"

    const/16 v2, 0x42

    const v3, 0x7f070856

    const-string v4, "Turkey Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Istanbul:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 75
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Kuwait_Riyadh"

    const/16 v2, 0x43

    const v3, 0x7f07085f

    const-string v4, "Arab Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kuwait_Riyadh:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 76
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Minsk"

    const/16 v2, 0x44

    const v3, 0x7f070863

    const-string v4, "Belarus Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Minsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 77
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Moscow_St_Petersburg_Volgograd"

    const/16 v2, 0x45

    const v3, 0x7f070866

    const-string v4, "Russian Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Moscow_St_Petersburg_Volgograd:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 78
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Nairobi"

    const/16 v2, 0x46

    const v3, 0x7f070868

    const-string v4, "E. Africa Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Nairobi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 79
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Tehran"

    const/16 v2, 0x47

    const v3, 0x7f07087f

    const-string v4, "Iran Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Tehran:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 80
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Abu_Dhabi_Muscat"

    const/16 v2, 0x48

    const v3, 0x7f070807

    const-string v4, "Arabian Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Abu_Dhabi_Muscat:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 81
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Astrakhan_Ulyanovsk"

    const/16 v2, 0x49

    const v3, 0x7f070812

    const-string v4, "Astrakhan Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Astrakhan_Ulyanovsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 82
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Baku"

    const/16 v2, 0x4a

    const v3, 0x7f07081a

    const-string v4, "Azerbaijan Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Baku:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 83
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Izhevsk_Samara"

    const/16 v2, 0x4b

    const v3, 0x7f070857

    const-string v4, "Russia Time Zone 3"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Izhevsk_Samara:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 84
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Port_Louis"

    const/16 v2, 0x4c

    const v3, 0x7f070871

    const-string v4, "Mauritius Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Port_Louis:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 85
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Saratov"

    const/16 v2, 0x4d

    const v3, 0x7f070879

    const-string v4, "Saratov Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Saratov:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 86
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Tibilis"

    const/16 v2, 0x4e

    const v3, 0x7f070880

    const-string v4, "Georgian Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Tibilis:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 87
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Yeravan"

    const/16 v2, 0x4f

    const v3, 0x7f07088a

    const-string v4, "Caucasus Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Yeravan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 88
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Kabul"

    const/16 v2, 0x50

    const v3, 0x7f070859

    const-string v4, "Afghanistan Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kabul:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 89
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Ashgabat_Tashkent"

    const/16 v2, 0x51

    const v3, 0x7f070810

    const-string v4, "West Asia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Ashgabat_Tashkent:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 90
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Ekaterinburg"

    const/16 v2, 0x52

    const v3, 0x7f070843

    const-string v4, "Ekaterinburg Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Ekaterinburg:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 91
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Islamabad_Karachi"

    const/16 v2, 0x53

    const v3, 0x7f070855

    const-string v4, "Pakistan Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Islamabad_Karachi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 92
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Chennai_Kolkata_Mumbai_New_Delhi"

    const/16 v2, 0x54

    const v3, 0x7f07082e

    const-string v4, "India Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chennai_Kolkata_Mumbai_New_Delhi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 93
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Sri_Jayawardenepura"

    const/16 v2, 0x55

    const v3, 0x7f07087d

    const-string v4, "Sri Lanka Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Sri_Jayawardenepura:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 94
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Kathmandu"

    const/16 v2, 0x56

    const v3, 0x7f07085b

    const-string v4, "Nepal Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kathmandu:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 95
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Astana"

    const/16 v2, 0x57

    const v3, 0x7f070811

    const-string v4, "Central Asia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Astana:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 96
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Dhaka"

    const/16 v2, 0x58

    const v3, 0x7f07083f

    const-string v4, "Bangladesh Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Dhaka:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 97
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Omsk"

    const/16 v2, 0x59

    const v3, 0x7f07086d

    const-string v4, "Omsk Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Omsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 98
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Yangon_Rangoon"

    const/16 v2, 0x5a

    const v3, 0x7f070889

    const-string v4, "Myanmar Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Yangon_Rangoon:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 99
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Bangkok_Hanoi_Jakarta"

    const/16 v2, 0x5b

    const v3, 0x7f07081b

    const-string v4, "SE Asia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Bangkok_Hanoi_Jakarta:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 100
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Barnaul_Gorno_Altaysk"

    const/16 v2, 0x5c

    const v3, 0x7f07081c

    const-string v4, "Altai Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Barnaul_Gorno_Altaysk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 101
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Hovd"

    const/16 v2, 0x5d

    const v3, 0x7f070851

    const-string v4, "W. Mongolia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Hovd:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 102
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Krasnoyarsk"

    const/16 v2, 0x5e

    const v3, 0x7f07085d

    const-string v4, "North Asia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Krasnoyarsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 103
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Novosibirsk"

    const/16 v2, 0x5f

    const v3, 0x7f07086b

    const-string v4, "N. Central Asia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Novosibirsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 104
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Tomsk"

    const/16 v2, 0x60

    const v3, 0x7f070881

    const-string v4, "Tomsk Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Tomsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 105
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Beijing_Chongqing_Hong_Kong_Urumqi"

    const/16 v2, 0x61

    const v3, 0x7f07081d

    const-string v4, "China Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Beijing_Chongqing_Hong_Kong_Urumqi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 106
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Irkutsk"

    const/16 v2, 0x62

    const v3, 0x7f070854

    const-string v4, "North Asia East Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Irkutsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 107
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Kuala_Lumpur_Singapore"

    const/16 v2, 0x63

    const v3, 0x7f07085e

    const-string v4, "Singapore Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kuala_Lumpur_Singapore:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 108
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Perth"

    const/16 v2, 0x64

    const v3, 0x7f070870

    const-string v4, "W. Australia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Perth:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 109
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Taipei"

    const/16 v2, 0x65

    const v3, 0x7f07087e

    const-string v4, "Taipei Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Taipei:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 110
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Ulaanbaatar"

    const/16 v2, 0x66

    const v3, 0x7f070884

    const-string v4, "Ulaanbaatar Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Ulaanbaatar:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 111
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Pyongyang"

    const/16 v2, 0x67

    const v3, 0x7f070872

    const-string v4, "North Korea Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Pyongyang:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 112
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Eucla"

    const/16 v2, 0x68

    const v3, 0x7f070844

    const-string v4, "Aus Central W. Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Eucla:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 113
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Chita"

    const/16 v2, 0x69

    const v3, 0x7f070832

    const-string v4, "Transbaikal Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chita:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 114
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Osaka_Sapporo_Tokyo"

    const/16 v2, 0x6a

    const v3, 0x7f07086e

    const-string v4, "Tokyo Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Osaka_Sapporo_Tokyo:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 115
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Seoul"

    const/16 v2, 0x6b

    const v3, 0x7f07087b

    const-string v4, "Korea Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Seoul:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 116
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Yakutsk"

    const/16 v2, 0x6c

    const v3, 0x7f070888

    const-string v4, "Yakutsk Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Yakutsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 117
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Adelaide"

    const/16 v2, 0x6d

    const v3, 0x7f070808

    const-string v4, "Cen. Australia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Adelaide:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 118
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Darwin"

    const/16 v2, 0x6e

    const v3, 0x7f07083e

    const-string v4, "AUS Central Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Darwin:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 119
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Brisbane"

    const/16 v2, 0x6f

    const v3, 0x7f070823

    const-string v4, "E. Australia Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Brisbane:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 120
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Canberra_Melbourne_Sydney"

    const/16 v2, 0x70

    const v3, 0x7f070827

    const-string v4, "AUS Eastern Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Canberra_Melbourne_Sydney:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 121
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Guam_Port_Moresby"

    const/16 v2, 0x71

    const v3, 0x7f07084a

    const-string v4, "West Pacific Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Guam_Port_Moresby:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 122
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Hobart"

    const/16 v2, 0x72

    const v3, 0x7f070850

    const-string v4, "Tasmania Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Hobart:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 123
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Vladivostok"

    const/16 v2, 0x73

    const v3, 0x7f070885

    const-string v4, "Vladivostok Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Vladivostok:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 124
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Lord_Howe_Island"

    const/16 v2, 0x74

    const v3, 0x7f070860

    const-string v4, "Lord Howe Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Lord_Howe_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 125
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Bougainville_Island"

    const/16 v2, 0x75

    const v3, 0x7f070821

    const-string v4, "Bougainville Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Bougainville_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 126
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Chokurdakh"

    const/16 v2, 0x76

    const v3, 0x7f070833

    const-string v4, "Russia Time Zone 10"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chokurdakh:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 127
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Magadan"

    const/16 v2, 0x77

    const v3, 0x7f070861

    const-string v4, "Magadan Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Magadan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 128
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Norfolk_Island"

    const/16 v2, 0x78

    const v3, 0x7f07086a

    const-string v4, "Norfolk Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Norfolk_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 129
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Sakhalin"

    const/16 v2, 0x79

    const v3, 0x7f070874

    const-string v4, "Sakhalin Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Sakhalin:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 130
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Solomon_Is_New_Caledonia"

    const/16 v2, 0x7a

    const v3, 0x7f07087c

    const-string v4, "Central Pacific Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Solomon_Is_New_Caledonia:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 131
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Anadyr_Petropavlovsk_Kamchatsky"

    const/16 v2, 0x7b

    const v3, 0x7f07080d

    const-string v4, "Russia Time Zone 11"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Anadyr_Petropavlovsk_Kamchatsky:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 132
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Auckland_Wellington"

    const/16 v2, 0x7c

    const v3, 0x7f070816

    const-string v4, "New Zealand Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Auckland_Wellington:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 133
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Coordinated_Universal_Time_12"

    const/16 v2, 0x7d

    const v3, 0x7f07083a

    const-string v4, "UTC+12"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_12:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 134
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Fiji"

    const/16 v2, 0x7e

    const v3, 0x7f070845

    const-string v4, "Fiji Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Fiji:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 135
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Chatham_Islands"

    const/16 v2, 0x7f

    const v3, 0x7f07082d

    const-string v4, "Chatham Islands Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chatham_Islands:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 136
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Coordinated_Universal_Time_13"

    const/16 v2, 0x80

    const v3, 0x7f07083b

    const-string v4, "UTC+13"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_13:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 137
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Nukualofa"

    const/16 v2, 0x81

    const v3, 0x7f07086c

    const-string v4, "Tonga Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Nukualofa:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 138
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Samoa"

    const/16 v2, 0x82

    const v3, 0x7f070876

    const-string v4, "Samoa Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Samoa:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 139
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    const-string v1, "OOBE_Timezone_Kiritimati_Island"

    const/16 v2, 0x83

    const v3, 0x7f07085c

    const-string v4, "Line Islands Standard Time"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kiritimati_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .line 7
    const/16 v0, 0x84

    new-array v0, v0, [Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_International_Date_Line_West:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_11:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Aleutian_Islands:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Hawaii:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v1, v0, v8

    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Marquesas_Islands:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Alaska:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_09:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Baja_California:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_08:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Pacific_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Arizona:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chihuahua_La_Paz_Mazatlan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Mountain_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Central_America:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Central_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Easter_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Guadalajara_Mexico_City_Monterrey:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Saskatchewan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Bogota_Lima_Quito_Rio_Branco:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chetumal:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Eastern_Time_US_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Haiti:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Havanah:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Indiana_East:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Asuncion:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Atlantic_Time_Canada:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Caracas:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Cuiaba:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Georgetown_La_Paz_Manaus_San_Juan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Santiago:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Turks_and_Caicos:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Newfoundland:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Araguaina:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Brasilia:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Cayenne_Fortaleza:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_City_of_Buenos_Aires:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Greenland:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Montevideo:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Saint_Pierre_and_Miquelon:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Salvador:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_02:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Azores:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Cabo_Verde_Is:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Casablanca:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Dublin_Edinburgh_Lisbon_London:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Monrovia_Reykjavik:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Amsterdam_Berlin_Bern_Rome_Stockholm_Vienna:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Belgrade_Bratislava_Budapest_Ljubljana_Prague:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Brussels_Copenhagen_Madrid_Paris:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Sarajevo_Skopje_Warsaw_Zagreb:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_West_Central_Africa:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Windhoek:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Amman:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Athens_Bucharest:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Beirut:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Cairo:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chisinau:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Damascus:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Gaza_Hebron:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Harare_Pretoria:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Helsinki_Kyiv_Riga_Sofia_Tallinn_Vilnius:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Jerusalem:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kaliningrad:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Tripoli:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Baghdad:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Istanbul:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kuwait_Riyadh:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Minsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Moscow_St_Petersburg_Volgograd:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Nairobi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Tehran:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Abu_Dhabi_Muscat:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Astrakhan_Ulyanovsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Baku:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Izhevsk_Samara:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Port_Louis:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Saratov:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Tibilis:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Yeravan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kabul:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Ashgabat_Tashkent:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Ekaterinburg:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Islamabad_Karachi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chennai_Kolkata_Mumbai_New_Delhi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Sri_Jayawardenepura:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kathmandu:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Astana:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Dhaka:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Omsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Yangon_Rangoon:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Bangkok_Hanoi_Jakarta:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Barnaul_Gorno_Altaysk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Hovd:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Krasnoyarsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Novosibirsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Tomsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Beijing_Chongqing_Hong_Kong_Urumqi:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Irkutsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kuala_Lumpur_Singapore:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Perth:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Taipei:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Ulaanbaatar:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Pyongyang:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Eucla:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chita:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Osaka_Sapporo_Tokyo:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Seoul:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Yakutsk:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Adelaide:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Darwin:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Brisbane:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Canberra_Melbourne_Sydney:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Guam_Port_Moresby:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Hobart:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Vladivostok:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Lord_Howe_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Bougainville_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chokurdakh:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Magadan:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Norfolk_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Sakhalin:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Solomon_Is_New_Caledonia:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Anadyr_Petropavlovsk_Kamchatsky:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Auckland_Wellington:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_12:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Fiji:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Chatham_Islands:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Coordinated_Universal_Time_13:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Nukualofa:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Samoa:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->OOBE_Timezone_Kiritimati_Island:Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->$VALUES:[Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "stringID"    # I
    .param p4, "serializedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 147
    iput p3, p0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->stringID:I

    .line 148
    iput-object p4, p0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->serializedName:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->$VALUES:[Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    return-object v0
.end method
