.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings;
.super Ljava/lang/Object;
.source "UTCSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackNotificationSettingChange(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;Z)V
    .locals 1
    .param p0, "settingName"    # Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;
    .param p1, "enabled"    # Z

    .prologue
    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$1;

    invoke-direct {v0, p1, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$1;-><init>(ZLcom/microsoft/xbox/service/network/managers/utchelpers/UTCSettings$NotificationName;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 48
    return-void
.end method
