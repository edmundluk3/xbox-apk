.class public Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;
.super Ljava/lang/Object;
.source "URCButtonHandler.java"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "URCButtonHandler"


# instance fields
.field private buttonsNonSTBShouldCheck:[Ljava/lang/String;

.field private buttonsSTBShouldNotCheck:[Ljava/lang/String;

.field private device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

.field private id2buttonsInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field private parent:Landroid/view/ViewGroup;

.field private view2buttonsInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 7
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "btn.vol_up"

    aput-object v2, v1, v3

    const-string v2, "btn.vol_down"

    aput-object v2, v1, v4

    const-string v2, "btn.vol_mute"

    aput-object v2, v1, v5

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->buttonsSTBShouldNotCheck:[Ljava/lang/String;

    .line 28
    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "btn.vol_up"

    aput-object v2, v1, v3

    const-string v2, "btn.vol_down"

    aput-object v2, v1, v4

    const-string v2, "btn.vol_mute"

    aput-object v2, v1, v5

    const-string v2, "btn.input"

    aput-object v2, v1, v6

    const/4 v2, 0x4

    const-string v3, "btn.power"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "btn.power_on"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "btn.power_off"

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->buttonsNonSTBShouldCheck:[Ljava/lang/String;

    .line 35
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->view2buttonsInfo:Ljava/util/HashMap;

    .line 36
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->id2buttonsInfo:Ljava/util/HashMap;

    .line 39
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->parent:Landroid/view/ViewGroup;

    .line 40
    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 43
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-nez v0, :cond_0

    .line 44
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getDevices()[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->updateDevices([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;
    .param p1, "x1"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->updateDevices([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->parent:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private updateDevices([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)Z
    .locals 7
    .param p1, "_devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 123
    const-string v4, "URCButtonHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updating devices (D "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p1, :cond_1

    const-string v3, "null"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ")"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    const/4 v2, 0x0

    .line 125
    .local v2, "newDevice":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    const/4 v1, 0x0

    .line 126
    .local v1, "didHandle":Z
    if-eqz p1, :cond_2

    .line 127
    array-length v4, p1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v0, p1, v3

    .line 128
    .local v0, "di":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    const-string v6, "tuner"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 129
    move-object v2, v0

    .line 130
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    if-eq v2, v5, :cond_0

    .line 131
    iput-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .line 132
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->rearrangeButtons()V

    .line 133
    const/4 v1, 0x1

    .line 127
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 123
    .end local v0    # "di":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .end local v1    # "didHandle":Z
    .end local v2    # "newDevice":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    :cond_1
    array-length v3, p1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0

    .line 139
    .restart local v1    # "didHandle":Z
    .restart local v2    # "newDevice":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    :cond_2
    return v1
.end method


# virtual methods
.method protected checkButton(Landroid/view/View;)V
    .locals 9
    .param p1, "vi"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 173
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->view2buttonsInfo:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;

    .line 175
    .local v0, "bi":Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    if-nez v5, :cond_1

    const/4 v1, 0x0

    .line 176
    .local v1, "buttons":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->parent:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getInstance(Landroid/content/Context;)Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;

    move-result-object v2

    .line 178
    .local v2, "model":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;
    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;->id:Ljava/lang/String;

    invoke-virtual {v2, v5, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;->getRealID(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    .line 180
    .local v3, "registeredID":Ljava/lang/String;
    const/4 v4, 0x0

    .line 182
    .local v4, "shouldNotCheck":Z
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    if-eqz v5, :cond_0

    .line 183
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    iget-object v5, v5, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    const-string v8, "stb"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 187
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->buttonsSTBShouldNotCheck:[Ljava/lang/String;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    iget-object v8, v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;->id:Ljava/lang/String;

    invoke-interface {v5, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 197
    :cond_0
    :goto_1
    if-eqz v4, :cond_4

    .line 222
    :goto_2
    return-void

    .line 175
    .end local v1    # "buttons":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "model":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;
    .end local v3    # "registeredID":Ljava/lang/String;
    .end local v4    # "shouldNotCheck":Z
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    iget-object v1, v5, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    goto :goto_0

    .line 193
    .restart local v1    # "buttons":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v2    # "model":Lcom/microsoft/xbox/xle/urc/ui/URCViewModel;
    .restart local v3    # "registeredID":Ljava/lang/String;
    .restart local v4    # "shouldNotCheck":Z
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->buttonsNonSTBShouldCheck:[Ljava/lang/String;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    iget-object v8, v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;->id:Ljava/lang/String;

    invoke-interface {v5, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    move v4, v6

    :goto_3
    goto :goto_1

    :cond_3
    move v4, v7

    goto :goto_3

    .line 201
    :cond_4
    if-eqz v3, :cond_6

    .line 202
    instance-of v5, p1, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    if-eqz v5, :cond_5

    move-object v5, p1

    .line 203
    check-cast v5, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->setContentIsEnabled(Z)V

    .line 205
    :cond_5
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {p1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 206
    invoke-virtual {p1, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 208
    iput-object v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;->registered_command:Ljava/lang/String;

    goto :goto_2

    .line 211
    :cond_6
    instance-of v5, p1, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    if-eqz v5, :cond_7

    move-object v5, p1

    .line 212
    check-cast v5, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->setContentIsEnabled(Z)V

    .line 215
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 216
    const-string v5, "URCButtonHandler"

    const-string v6, "Disabling previously enabled button"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_8
    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {p1, v5}, Landroid/view/View;->setAlpha(F)V

    .line 220
    invoke-virtual {p1, v7}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 92
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 93
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 95
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getDevices()[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->updateDevices([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)Z

    .line 97
    :cond_0
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 6
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "child"    # Landroid/view/View;

    .prologue
    .line 52
    instance-of v5, p2, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;

    if-eqz v5, :cond_1

    move-object v5, p2

    .line 53
    check-cast v5, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/urc/ui/BranchButton;->getCommand()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p2, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->registerChild(Landroid/view/View;Ljava/lang/String;)V

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    instance-of v5, p2, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;

    if-eqz v5, :cond_2

    move-object v5, p2

    .line 55
    check-cast v5, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/urc/ui/BranchImageButton;->getCommand()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p2, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->registerChild(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_2
    instance-of v5, p2, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    if-eqz v5, :cond_0

    move-object v3, p2

    .line 57
    check-cast v3, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    .line 58
    .local v3, "urcPercentView":Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getCommand()Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "command":Ljava/lang/String;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 60
    invoke-virtual {p0, p2, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->registerChild(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_3
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildCount()I

    move-result v0

    .line 63
    .local v0, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_0

    .line 64
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 65
    .local v4, "v2":Landroid/view/View;
    if-eqz v4, :cond_4

    .line 66
    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->onChildViewAdded(Landroid/view/View;Landroid/view/View;)V

    .line 63
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 7
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "child"    # Landroid/view/View;

    .prologue
    .line 76
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->view2buttonsInfo:Ljava/util/HashMap;

    invoke-virtual {v5, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 77
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->view2buttonsInfo:Ljava/util/HashMap;

    invoke-virtual {v5, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;

    .line 78
    .local v0, "bi":Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->id2buttonsInfo:Ljava/util/HashMap;

    iget-object v6, v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    .end local v0    # "bi":Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;
    :cond_0
    return-void

    .line 79
    :cond_1
    instance-of v5, p2, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    if-eqz v5, :cond_0

    move-object v3, p2

    .line 80
    check-cast v3, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;

    .line 81
    .local v3, "urcPercentView":Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildCount()I

    move-result v1

    .line 82
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 83
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 84
    .local v4, "v2":Landroid/view/View;
    if-eqz v4, :cond_2

    .line 85
    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V

    .line 82
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 2
    .param p1, "_devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 112
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$1;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 120
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    .line 108
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 101
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 104
    :cond_0
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 234
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 239
    return-void
.end method

.method protected rearrangeButtons()V
    .locals 4

    .prologue
    .line 225
    const-string v2, "URCButtonHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Rearranging buttons (D "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "null"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " V "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->view2buttonsInfo:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->view2buttonsInfo:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 227
    .local v0, "vi":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->checkButton(Landroid/view/View;)V

    goto :goto_1

    .line 225
    .end local v0    # "vi":Landroid/view/View;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->device:Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 229
    :cond_2
    return-void
.end method

.method protected registerChild(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 143
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$1;)V

    .line 148
    .local v0, "bi":Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;
    iput-object p2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;->id:Ljava/lang/String;

    .line 149
    iput-object p2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;->registered_command:Ljava/lang/String;

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->view2buttonsInfo:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->id2buttonsInfo:Ljava/util/HashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    new-instance v1, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$2;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$2;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->checkButton(Landroid/view/View;)V

    goto :goto_0
.end method
