.class Lcom/microsoft/xbox/xle/epg/AppChannelsModel$1;
.super Ljava/lang/Object;
.source "AppChannelsModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->sortChannelsArray(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$1;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;)I
    .locals 3
    .param p1, "c1"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    .param p2, "c2"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .prologue
    .line 241
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderName()Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "n1":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderName()Ljava/lang/String;

    move-result-object v1

    .line 244
    .local v1, "n2":Ljava/lang/String;
    const-string v2, "OneGuide"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 245
    const/4 v2, -0x1

    .line 251
    :goto_0
    return v2

    .line 247
    :cond_0
    const-string v2, "OneGuide"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 248
    const/4 v2, 0x1

    goto :goto_0

    .line 251
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 239
    check-cast p1, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    check-cast p2, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$1;->compare(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;)I

    move-result v0

    return v0
.end method
