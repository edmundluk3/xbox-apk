.class public Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "LanguageSettingsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)V
    .locals 1
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "interactor"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;
    .param p3, "telemetryService"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 34
    invoke-static {p0, p2, p3}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 53
    return-void
.end method

.method static synthetic lambda$initialSetup$9(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;)V
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    sget-object v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->TAG:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$8(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;
    .param p2, "telemetryService"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;
    .param p3, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 35
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 52
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 35
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LanguageChangedIntent;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LanguageChangedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LanguageChangedIntent;->newLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;->with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;)V
    .locals 1
    .param p0, "telemetryService"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;
    .param p1, "intent"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LanguageChangedAction;->newLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;->languageSettingChanged(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LocationChangedIntent;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;
    .locals 1
    .param p0, "intent"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LocationChangedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LocationChangedIntent;->newLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;->with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;)V
    .locals 1
    .param p0, "telemetryService"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;
    .param p1, "intent"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor$LocationChangedAction;->newLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;->marketSettingChanged(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)V

    return-void
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "interactor"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;
    .param p1, "telemetryService"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;
    .param p2, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    const-class v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 38
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LanguageChangedIntent;

    .line 42
    invoke-virtual {p2, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 43
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 44
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 45
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->getLanguageChangedTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LocationChangedIntent;

    .line 47
    invoke-virtual {p2, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 48
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 49
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 50
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;->getLocationChangedTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    .line 37
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;
    .param p2, "telemetryService"    # Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;
    .param p3, "i"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    invoke-static {p3}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 36
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 35
    return-object v0
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;
    .locals 1
    .param p0, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;->content()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;->with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 57
    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 58
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 59
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 60
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 61
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 63
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 64
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 66
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 67
    return-void
.end method
