.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;
.super Ljava/lang/Object;
.source "UTCPurchase.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->track(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actionName:Ljava/lang/String;

.field final synthetic val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;->val$actionName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 15

    .prologue
    .line 40
    new-instance v6, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 43
    .local v6, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v5

    .line 46
    .local v5, "fromPage":Ljava/lang/String;
    const-string v11, "ConsoleLanguage"

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/SessionModel;->getConsoleLocale()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v11, v12}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    const-string v11, "ServiceLanguage"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v11, v12}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    iget-object v11, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 50
    iget-object v11, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v7

    .line 52
    .local v7, "purchaseData":Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    const-string v10, "NotUsed"

    .line 53
    .local v10, "storeFilter":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->getHasStoreFilterPosition()Z

    move-result v11

    if-eqz v11, :cond_2

    .line 54
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->getStoreFilterPosition()I

    move-result v4

    .line 55
    .local v4, "filterPos":I
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Undefined:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    .line 56
    .local v2, "filter":Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->values()[Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    move-result-object v12

    array-length v13, v12

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v13, :cond_1

    aget-object v3, v12, v11

    .line 57
    .local v3, "filterItem":Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->ordinal()I

    move-result v14

    if-ne v14, v4, :cond_0

    .line 58
    move-object v2, v3

    .line 56
    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 61
    .end local v3    # "filterItem":Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    :cond_1
    sget-object v11, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->Undefined:Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;

    if-ne v2, v11, :cond_4

    .line 62
    packed-switch v4, :pswitch_data_0

    .line 70
    const-string v10, "NotUsed"

    .line 78
    .end local v2    # "filter":Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .end local v4    # "filterPos":I
    :cond_2
    :goto_1
    const-string v11, "Filter"

    invoke-virtual {v6, v11, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 81
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->getPurchaseOriginatingSource()Ljava/lang/String;

    move-result-object v9

    .line 82
    .local v9, "sourcePage":Ljava/lang/String;
    const-string v11, "SourcePage"

    invoke-virtual {v6, v11, v9}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 85
    iget-object v11, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseAvailabilityId()Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "availabilityId":Ljava/lang/String;
    const-string v11, "AvailabilityId"

    invoke-virtual {v6, v11, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    iget-object v11, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseProductId()Ljava/lang/String;

    move-result-object v1

    .line 90
    .local v1, "catId":Ljava/lang/String;
    const-string v11, "BigCatId"

    invoke-virtual {v6, v11, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 92
    iget-object v11, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseSkuId()Ljava/lang/String;

    move-result-object v8

    .line 93
    .local v8, "skuId":Ljava/lang/String;
    const-string v11, "SkuId"

    invoke-virtual {v6, v11, v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 97
    .end local v0    # "availabilityId":Ljava/lang/String;
    .end local v1    # "catId":Ljava/lang/String;
    .end local v7    # "purchaseData":Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    .end local v8    # "skuId":Ljava/lang/String;
    .end local v9    # "sourcePage":Ljava/lang/String;
    .end local v10    # "storeFilter":Ljava/lang/String;
    :cond_3
    iget-object v11, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$1;->val$actionName:Ljava/lang/String;

    invoke-static {v11, v5, v6}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 98
    return-void

    .line 64
    .restart local v2    # "filter":Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;
    .restart local v4    # "filterPos":I
    .restart local v7    # "purchaseData":Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;
    .restart local v10    # "storeFilter":Ljava/lang/String;
    :pswitch_0
    const-string v10, "Featured"

    .line 65
    goto :goto_1

    .line 67
    :pswitch_1
    const-string v10, "Gold"

    .line 68
    goto :goto_1

    .line 74
    :cond_4
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreBrowseFilter;->getTelemetryName()Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    .line 62
    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
