.class public Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;
.super Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;
.source "CreateLfgSelectTitleViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private clubId:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "parentScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 33
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedClubId()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->clubId:Ljava/lang/Long;

    .line 34
    return-void
.end method


# virtual methods
.method protected createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getCreateLfgSelectTitleAdapter(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    return-object v0
.end method

.method public getSearchResultTitleInfoList()Ljava/util/List;
    .locals 10
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getSearchResults()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    .line 84
    .local v2, "searchResults":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v2, :cond_1

    .line 85
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 86
    .local v3, "title":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    new-instance v4, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    invoke-direct {v4, v3}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;-><init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;)V

    .line 88
    .local v4, "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getTitleId()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 89
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 94
    .end local v3    # "title":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .end local v4    # "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getPcSearchResults()Ljava/util/List;

    move-result-object v0

    .line 96
    .local v0, "pcTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 97
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;

    .line 98
    .local v3, "title":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;
    new-instance v6, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    invoke-direct {v6, v3}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;-><init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 102
    .end local v3    # "title":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$MiniTitleInfo;
    :cond_2
    return-object v1
.end method

.method public getTitleInfoList()Ljava/util/List;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v3, "titleInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->getRecentlyPlayedTitles()Ljava/util/List;

    move-result-object v0

    .line 67
    .local v0, "recentTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 68
    .local v1, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    new-instance v2, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    invoke-direct {v2, v1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;-><init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;)V

    .line 70
    .local v2, "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 71
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    .end local v1    # "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .end local v2    # "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    :cond_1
    return-object v3
.end method

.method protected onRecentTitleListUpdated()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public selectTitle(JLjava/lang/String;)V
    .locals 5
    .param p1, "titleId"    # J
    .param p3, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 39
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 41
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->clubId:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->clubId:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedClubId(Ljava/lang/Long;)V

    .line 45
    :cond_0
    if-eqz p3, :cond_1

    .line 46
    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedScid(Ljava/lang/String;)V

    .line 49
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_3

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->clubId:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 51
    const-class v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsScreen;

    invoke-virtual {p0, v1, v4, v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 59
    :goto_0
    return-void

    .line 53
    :cond_2
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    invoke-virtual {p0, v1, v4, v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 56
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not select title with titleId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and SCID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0705a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;)V

    goto :goto_0
.end method
