.class final synthetic Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$24;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

.field private final arg$2:Lcom/microsoft/xbox/domain/auth/AuthStateManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$24;->arg$1:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$24;->arg$2:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$24;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$24;-><init>(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$24;->arg$1:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$24;->arg$2:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    check-cast p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->lambda$null$16(Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$FinishAction;

    move-result-object v0

    return-object v0
.end method
