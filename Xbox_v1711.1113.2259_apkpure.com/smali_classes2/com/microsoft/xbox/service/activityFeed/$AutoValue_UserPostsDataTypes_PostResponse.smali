.class abstract Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;
.super Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
.source "$AutoValue_UserPostsDataTypes_PostResponse.java"


# instance fields
.field private final postAuthor:Ljava/lang/String;

.field private final postDate:Ljava/util/Date;

.field private final postId:Ljava/lang/String;

.field private final postText:Ljava/lang/String;

.field private final postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

.field private final postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

.field private final postUri:Ljava/lang/String;

.field private final timelines:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V
    .locals 2
    .param p1, "postAuthor"    # Ljava/lang/String;
    .param p2, "postDate"    # Ljava/util/Date;
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "postText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "postType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    .param p6, "postTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "postUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p8, "timelines":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;-><init>()V

    .line 31
    if-nez p1, :cond_0

    .line 32
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null postAuthor"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postAuthor:Ljava/lang/String;

    .line 35
    if-nez p2, :cond_1

    .line 36
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null postDate"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postDate:Ljava/util/Date;

    .line 39
    if-nez p3, :cond_2

    .line 40
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null postId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postId:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postText:Ljava/lang/String;

    .line 44
    if-nez p5, :cond_3

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null postType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_3
    iput-object p5, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 48
    iput-object p6, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    .line 49
    if-nez p7, :cond_4

    .line 50
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null postUri"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_4
    iput-object p7, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postUri:Ljava/lang/String;

    .line 53
    if-nez p8, :cond_5

    .line 54
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null timelines"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_5
    iput-object p8, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->timelines:Lcom/google/common/collect/ImmutableList;

    .line 57
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 123
    if-ne p1, p0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v1

    .line 126
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 127
    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    .line 128
    .local v0, "that":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postAuthor:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postAuthor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postDate:Ljava/util/Date;

    .line 129
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postId:Ljava/lang/String;

    .line 130
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postText:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 131
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postText()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    .line 132
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    if-nez v3, :cond_4

    .line 133
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postTypeData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postUri:Ljava/lang/String;

    .line 134
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->timelines:Lcom/google/common/collect/ImmutableList;

    .line 135
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->timelines()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 131
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postText:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 133
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->postTypeData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .end local v0    # "that":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    :cond_5
    move v1, v2

    .line 137
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 142
    const/4 v0, 0x1

    .line 143
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postAuthor:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 145
    mul-int/2addr v0, v3

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 147
    mul-int/2addr v0, v3

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 149
    mul-int/2addr v0, v3

    .line 150
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postText:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 151
    mul-int/2addr v0, v3

    .line 152
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 153
    mul-int/2addr v0, v3

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    if-nez v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 155
    mul-int/2addr v0, v3

    .line 156
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postUri:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 157
    mul-int/2addr v0, v3

    .line 158
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->timelines:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 159
    return v0

    .line 150
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public postAuthor()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postAuthor:Ljava/lang/String;

    return-object v0
.end method

.method public postDate()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postDate:Ljava/util/Date;

    return-object v0
.end method

.method public postId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postId:Ljava/lang/String;

    return-object v0
.end method

.method public postText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postText:Ljava/lang/String;

    return-object v0
.end method

.method public postType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    return-object v0
.end method

.method public postTypeData()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    return-object v0
.end method

.method public postUri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postUri:Ljava/lang/String;

    return-object v0
.end method

.method public timelines()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->timelines:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PostResponse{postAuthor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postAuthor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postTypeData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postTypeData:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->postUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timelines="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;->timelines:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
