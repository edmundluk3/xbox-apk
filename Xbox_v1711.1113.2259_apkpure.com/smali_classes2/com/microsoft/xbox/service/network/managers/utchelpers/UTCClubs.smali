.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;
.super Ljava/lang/Object;
.source "UTCClubs.java"


# static fields
.field public static final ACTION_NAMES:[Ljava/lang/String;

.field private static final CLUBS_TAB_SELECTED_PATTERN:Ljava/lang/String; = "02"

.field private static final CLUBS_TAB_SWIPED_PATTERN:Ljava/lang/String; = "012"

.field public static final TAB_NAMES:[Ljava/lang/String;

.field private static clubId:J

.field private static currentTab:Ljava/lang/String;

.field private static previousTab:Ljava/lang/String;

.field public static trackHubPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public static trackHubPageResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 37
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Clubs - Club Home view"

    aput-object v1, v0, v3

    const-string v1, "Clubs - Club Activity Feed view"

    aput-object v1, v0, v4

    const-string v1, "Clubs - Club Chat view"

    aput-object v1, v0, v5

    const-string v1, "Clubs - Club Play view"

    aput-object v1, v0, v6

    const-string v1, "Clubs - Watch View"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Clubs - Club Roster view"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Clubs - Club Admin view"

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->TAB_NAMES:[Ljava/lang/String;

    .line 47
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Clubs - Home Tab"

    aput-object v1, v0, v3

    const-string v1, "Clubs - Activity Feed Tab"

    aput-object v1, v0, v4

    const-string v1, "Clubs - Chat Tab"

    aput-object v1, v0, v5

    const-string v1, "Clubs - Play Tab"

    aput-object v1, v0, v6

    const-string v1, "Clubs - Watch Tab"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Clubs - Roster Tab"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Clubs - Admin Tab"

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->ACTION_NAMES:[Ljava/lang/String;

    .line 57
    const-string v0, "Clubs - Club Home view"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->currentTab:Ljava/lang/String;

    .line 58
    const-string v0, "Clubs - Club Home view"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->previousTab:Ljava/lang/String;

    .line 83
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHubPageResume:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    .line 104
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$2;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHubPage:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()J
    .locals 2

    .prologue
    .line 32
    sget-wide v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->clubId:J

    return-wide v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->previousTab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 32
    sput-object p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->previousTab:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->currentTab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 32
    sput-object p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->currentTab:Ljava/lang/String;

    return-object p0
.end method

.method public static getCurrentTab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->currentTab:Ljava/lang/String;

    return-object v0
.end method

.method public static getPreviousTab()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->previousTab:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic lambda$trackDiscoverExecuteSearch$0(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .param p0, "tags"    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "titleIds"    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 347
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 348
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "Tags"

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 349
    const-string v1, "TitleIds"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 351
    const-string v1, "Clubs - Discover Search Text"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 352
    return-void
.end method

.method public static resetTracking(Ljava/lang/String;)V
    .locals 1
    .param p0, "toPage"    # Ljava/lang/String;

    .prologue
    .line 69
    const-string v0, "Clubs"

    if-eq p0, v0, :cond_0

    const-string v0, "Clubs - Club Home view"

    if-eq p0, v0, :cond_0

    const-string v0, "Clubs - Club Activity Feed view"

    if-eq p0, v0, :cond_0

    const-string v0, "Clubs - Club Play view"

    if-eq p0, v0, :cond_0

    const-string v0, "Clubs - Club Chat view"

    if-eq p0, v0, :cond_0

    const-string v0, "Clubs - Club Roster view"

    if-eq p0, v0, :cond_0

    const-string v0, "Clubs - Club Admin view"

    if-eq p0, v0, :cond_0

    const-string v0, "Clubs - Watch View"

    if-eq p0, v0, :cond_0

    .line 78
    const-string v0, "Clubs - Club Home view"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->currentTab:Ljava/lang/String;

    .line 79
    const-string v0, "Clubs - Club Home view"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->previousTab:Ljava/lang/String;

    .line 81
    :cond_0
    return-void
.end method

.method public static setClubId(J)V
    .locals 0
    .param p0, "id"    # J

    .prologue
    .line 136
    sput-wide p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->clubId:J

    .line 137
    return-void
.end method

.method public static trackAdminBanMember(JLjava/lang/String;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 845
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$43;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$43;-><init>(JLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 858
    return-void
.end method

.method public static trackAdminBanned(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 773
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$39;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$39;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 784
    return-void
.end method

.method public static trackAdminDemoteMember(JLjava/lang/String;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 826
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$42;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$42;-><init>(JLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 839
    return-void
.end method

.method public static trackAdminMembersModerators(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 756
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$38;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$38;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 767
    return-void
.end method

.method public static trackAdminPromoteMember(JLjava/lang/String;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 807
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$41;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$41;-><init>(JLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 820
    return-void
.end method

.method public static trackAdminRemoveMember(JLjava/lang/String;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 864
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$44;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$44;-><init>(JLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 877
    return-void
.end method

.method public static trackAdminReportMember(JLjava/lang/String;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 883
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$45;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$45;-><init>(JLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 896
    return-void
.end method

.method public static trackAdminReports(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 739
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$37;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$37;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 750
    return-void
.end method

.method public static trackAdminReportsCreatorMessage(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "itemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "creatorXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "reporterXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1025
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$52;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$52;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1042
    return-void
.end method

.method public static trackAdminReportsDelete(JLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "itemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 926
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$47;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$47;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 941
    return-void
.end method

.method public static trackAdminReportsIgnore(JLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "itemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 948
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$48;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$48;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 963
    return-void
.end method

.method public static trackAdminReportsReporterMessage(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "itemId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "creatorXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "reporterXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1051
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;

    move-wide v2, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$53;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1068
    return-void
.end method

.method public static trackAdminRequestsIgnore(JLjava/lang/String;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 966
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$49;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$49;-><init>(JLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 979
    return-void
.end method

.method public static trackAdminRequestsIgnoreAll(JLjava/util/List;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 982
    .local p2, "targetXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$50;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$50;-><init>(JLjava/util/List;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 995
    return-void
.end method

.method public static trackAdminRequestsRecommendations(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 722
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$36;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$36;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 733
    return-void
.end method

.method public static trackAdminSendAllInvites(JLjava/util/List;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 998
    .local p2, "targetXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$51;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$51;-><init>(JLjava/util/List;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1016
    return-void
.end method

.method public static trackAdminSettings(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 790
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$40;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$40;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 801
    return-void
.end method

.method public static trackAdminSettingsActivityFeedPostFilter(JI)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "position"    # I

    .prologue
    .line 1197
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$61;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$61;-><init>(JI)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1221
    return-void
.end method

.method public static trackAdminSettingsChatPostFilter(JI)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "position"    # I

    .prologue
    .line 1283
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$64;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$64;-><init>(JI)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1304
    return-void
.end method

.method public static trackAdminSettingsDeleteClub(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1144
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$58;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$58;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1155
    return-void
.end method

.method public static trackAdminSettingsDeleteClubConfirm(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1161
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$59;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$59;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1172
    return-void
.end method

.method public static trackAdminSettingsInviteFilter(JI)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "position"    # I

    .prologue
    .line 1257
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$63;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$63;-><init>(JI)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1277
    return-void
.end method

.method public static trackAdminSettingsMembershipToggleJoin(JZ)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "enable"    # Z

    .prologue
    .line 1178
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$60;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$60;-><init>(JZ)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1191
    return-void
.end method

.method public static trackAdminSettingsPlayPostFilter(JI)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "position"    # I

    .prologue
    .line 1227
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$62;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$62;-><init>(JI)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1251
    return-void
.end method

.method public static trackAdminSettingsResetDefault(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1074
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$54;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$54;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1085
    return-void
.end method

.method public static trackAdminSettingsTransferOwnership(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1091
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$55;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$55;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1102
    return-void
.end method

.method public static trackAdminSettingsTransferOwnershipSelectOwner()V
    .locals 1

    .prologue
    .line 1108
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$56;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$56;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1118
    return-void
.end method

.method public static trackAdminSettingsTransferOwnershipSubmit(JLjava/lang/String;)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1124
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$57;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$57;-><init>(JLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1138
    return-void
.end method

.method public static trackAdminSettingsWatchClubTitlesOnly(JZ)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "enable"    # Z

    .prologue
    .line 1328
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$66;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$66;-><init>(JZ)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1340
    return-void
.end method

.method public static trackAdminSettingsWatchMatureContent(JZ)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "enable"    # Z

    .prologue
    .line 1310
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$65;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$65;-><init>(JZ)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1322
    return-void
.end method

.method public static trackChatDeleteMessage(JJ)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "messageId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 640
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$31;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$31;-><init>(JJ)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 653
    return-void
.end method

.method public static trackChatEditTopic(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 563
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$26;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$26;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 575
    return-void
.end method

.method public static trackChatEditTopicCancel(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 578
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$27;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$27;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 590
    return-void
.end method

.method public static trackChatEditTopicPageView(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 623
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$30;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$30;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 637
    return-void
.end method

.method public static trackChatEditTopicPost(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 608
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$29;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$29;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 620
    return-void
.end method

.method public static trackChatEditTopicReport(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 593
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$28;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$28;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 605
    return-void
.end method

.method public static trackChatManageNotifications(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 686
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$34;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$34;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 698
    return-void
.end method

.method public static trackChatPickMentionedUser(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 701
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$35;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$35;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 714
    return-void
.end method

.method public static trackChatReport(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 550
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$25;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$25;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 560
    return-void
.end method

.method public static trackChatSendMessage(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 656
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$32;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$32;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 668
    return-void
.end method

.method public static trackChatServiceError(J)V
    .locals 2
    .param p0, "errorCode"    # J

    .prologue
    .line 1486
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$75;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$75;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1492
    return-void
.end method

.method public static trackChatShowMentions(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 671
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$33;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$33;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 683
    return-void
.end method

.method public static trackCreateBack(I)V
    .locals 1
    .param p0, "createClubPageNum"    # I

    .prologue
    .line 403
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$18;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$18;-><init>(I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 422
    return-void
.end method

.method public static trackCreateCancel(I)V
    .locals 1
    .param p0, "createClubPageNum"    # I

    .prologue
    .line 440
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$20;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$20;-><init>(I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 460
    return-void
.end method

.method public static trackCreateCheckAvailableResult(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "clubName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "available"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 466
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$21;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$21;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 479
    return-void
.end method

.method public static trackCreateClubCheckAvailability(Ljava/lang/String;)V
    .locals 1
    .param p0, "clubName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 500
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$23;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$23;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 510
    return-void
.end method

.method public static trackCreateClubResult(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;JLjava/lang/String;)V
    .locals 6
    .param p0, "clubName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p4, "created"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 519
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 544
    return-void
.end method

.method public static trackCreateClubSubmit(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V
    .locals 1
    .param p0, "clubName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 485
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$22;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$22;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 497
    return-void
.end method

.method public static trackCreateNext()V
    .locals 1

    .prologue
    .line 428
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$19;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$19;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 434
    return-void
.end method

.method public static trackCustomizeClubAddBackgroundImage(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1450
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$73;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$73;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1461
    return-void
.end method

.method public static trackCustomizeClubChangeColor(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1416
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$71;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$71;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1427
    return-void
.end method

.method public static trackCustomizeClubDisplayImage(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1467
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$74;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$74;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1478
    return-void
.end method

.method public static trackCustomizeClubDone(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1433
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$72;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$72;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1444
    return-void
.end method

.method public static trackCustomizeClubEditDescription(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1382
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$69;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$69;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1393
    return-void
.end method

.method public static trackCustomizeClubEditGames(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1399
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$70;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$70;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1410
    return-void
.end method

.method public static trackCustomizeClubEditName(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1348
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$67;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$67;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1359
    return-void
.end method

.method public static trackCustomizeClubEditTags(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1365
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$68;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$68;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1376
    return-void
.end method

.method public static trackDiscoverAcceptInvitation(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 260
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$9;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$9;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 271
    return-void
.end method

.method public static trackDiscoverCreate()V
    .locals 1

    .prologue
    .line 308
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$12;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$12;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 314
    return-void
.end method

.method public static trackDiscoverExecuteGamesSearch()V
    .locals 1

    .prologue
    .line 359
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$15;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$15;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 365
    return-void
.end method

.method public static trackDiscoverExecuteSearch(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 344
    .local p0, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p1, "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 345
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 346
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$$Lambda$1;->lambdaFactory$(Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 353
    return-void
.end method

.method public static trackDiscoverIgnoreInvitation(J)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 277
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$10;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$10;-><init>(J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 288
    return-void
.end method

.method public static trackDiscoverNavigateToClub(Ljava/lang/String;J)V
    .locals 1
    .param p0, "actionName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 161
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$4;-><init>(Ljava/lang/String;J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 173
    return-void
.end method

.method public static trackDiscoverSearchGames()V
    .locals 1

    .prologue
    .line 320
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$13;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$13;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 326
    return-void
.end method

.method public static trackDiscoverSearchSelect(Ljava/lang/String;)V
    .locals 1
    .param p0, "selectedSuggestion"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 371
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$16;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$16;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 382
    return-void
.end method

.method public static trackDiscoverSearchTags()V
    .locals 1

    .prologue
    .line 332
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$14;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$14;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 338
    return-void
.end method

.method public static trackDiscoverSeeAll(Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V
    .locals 1
    .param p0, "item"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 291
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$11;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$11;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 302
    return-void
.end method

.method public static trackGameHubNavigateToSuggestedClub(JJ)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 179
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$5;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$5;-><init>(JJ)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 192
    return-void
.end method

.method public static trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1
    .param p0, "actionName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "clubId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 241
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$8;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$8;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 253
    return-void
.end method

.method public static trackMemberFilter(JI)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "position"    # I

    .prologue
    .line 902
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$46;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$46;-><init>(JI)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 920
    return-void
.end method

.method public static trackNavigateToClub(Ljava/lang/String;J)V
    .locals 1
    .param p0, "actionName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 143
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$3;-><init>(Ljava/lang/String;J)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 155
    return-void
.end method

.method public static trackPeopleHubNavigateToClub(JLjava/lang/String;I)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "targetXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "index"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 198
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$6;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$6;-><init>(JLjava/lang/String;I)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 216
    return-void
.end method

.method public static trackPeopleNavigateToClub(JI)V
    .locals 2
    .param p0, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "index"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 222
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$7;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$7;-><init>(JI)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 235
    return-void
.end method

.method public static trackSelectClubType(Ljava/lang/String;)V
    .locals 1
    .param p0, "clubType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 390
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$17;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$17;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 397
    return-void
.end method

.method public static trackUseCustomBackground()V
    .locals 1

    .prologue
    .line 1498
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$76;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$76;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1509
    return-void
.end method

.method public static trackUseCustomLogo()V
    .locals 1

    .prologue
    .line 1515
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$77;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$77;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 1526
    return-void
.end method
