.class public Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;
.super Ljava/lang/Object;
.source "LiveTvInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LiveTvInfo"


# instance fields
.field public currentHdmiChannelId:Ljava/lang/String;

.field public currentTunerChannelId:Ljava/lang/String;

.field public isInHdmiMode:Z

.field public pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

.field public streamingPort:Ljava/lang/String;

.field public tunerChannelType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;
    .locals 8
    .param p0, "json"    # Lorg/json/JSONObject;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 37
    new-instance v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;-><init>()V

    .line 40
    .local v1, "info":Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;
    :try_start_0
    const-string v6, "params"

    invoke-virtual {p0, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 41
    .local v2, "params":Lorg/json/JSONObject;
    const-string v6, "inHdmiMode"

    const-string v7, "0"

    invoke-virtual {v2, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_1

    move v6, v4

    :goto_0
    iput-boolean v6, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->isInHdmiMode:Z

    .line 42
    const-string v6, "streamingPort"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->streamingPort:Ljava/lang/String;

    .line 43
    const-string v6, "currentTunerChannelId"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->currentTunerChannelId:Ljava/lang/String;

    .line 44
    const-string v6, "tunerChannelType"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->tunerChannelType:Ljava/lang/String;

    .line 45
    const-string v6, "currentHdmiChannelId"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->currentHdmiChannelId:Ljava/lang/String;

    .line 47
    new-instance v6, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    invoke-direct {v6}, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;-><init>()V

    iput-object v6, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    .line 48
    const-string v6, "pauseBufferInfo"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 49
    .local v3, "pauseBufferInfo":Lorg/json/JSONObject;
    if-eqz v3, :cond_0

    .line 50
    iget-object v6, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    const-string v7, "Enabled"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    if-ne v7, v4, :cond_2

    :goto_1
    iput-boolean v4, v6, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->enabled:Z

    .line 51
    iget-object v4, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    const-string v5, "MaxBufferSize"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->maxBufferSize:J

    .line 52
    iget-object v4, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    const-string v5, "BufferStart"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->bufferStart:J

    .line 53
    iget-object v4, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    const-string v5, "BufferEnd"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->bufferEnd:J

    .line 54
    iget-object v4, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    const-string v5, "BufferCurrent"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->bufferCurrent:J

    .line 55
    iget-object v4, v1, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->pauseBufferInfo:Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;

    const-string v5, "Epoch"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo$PauseBufferInfo;->epoch:J
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    .end local v1    # "info":Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;
    .end local v2    # "params":Lorg/json/JSONObject;
    .end local v3    # "pauseBufferInfo":Lorg/json/JSONObject;
    :cond_0
    :goto_2
    return-object v1

    .restart local v1    # "info":Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;
    .restart local v2    # "params":Lorg/json/JSONObject;
    :cond_1
    move v6, v5

    .line 41
    goto :goto_0

    .restart local v3    # "pauseBufferInfo":Lorg/json/JSONObject;
    :cond_2
    move v4, v5

    .line 50
    goto :goto_1

    .line 57
    .end local v2    # "params":Lorg/json/JSONObject;
    .end local v3    # "pauseBufferInfo":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Lorg/json/JSONException;
    const-string v4, "LiveTvInfo"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bad json blob: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const/4 v1, 0x0

    goto :goto_2
.end method
