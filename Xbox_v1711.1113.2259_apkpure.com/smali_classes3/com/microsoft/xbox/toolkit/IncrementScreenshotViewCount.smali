.class public final Lcom/microsoft/xbox/toolkit/IncrementScreenshotViewCount;
.super Ljava/lang/Object;
.source "IncrementScreenshotViewCount.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static execute(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Lio/reactivex/Single;
    .locals 4
    .param p0, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 22
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getMediaHubService()Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    move-result-object v0

    .line 23
    .local v0, "mediaHubService":Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    .line 25
    .local v1, "slsServiceManager":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotId:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->incrementScreenshotViewCount(Ljava/lang/String;)Lio/reactivex/Completable;

    move-result-object v2

    invoke-static {v1, p0}, Lcom/microsoft/xbox/toolkit/IncrementScreenshotViewCount$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Ljava/util/concurrent/Callable;

    move-result-object v3

    .line 26
    invoke-static {v3}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Completable;->andThen(Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object v2

    .line 25
    return-object v2
.end method

.method static synthetic lambda$execute$0(Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
    .locals 4
    .param p0, "slsServiceManager"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    .param p1, "screenshot"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 27
    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->xuid:Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->scid:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->screenshotId:Ljava/lang/String;

    invoke-interface {p0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getScreenshotInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;

    move-result-object v0

    .line 28
    .local v0, "result":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;->screenshot:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    iget v1, v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->views:I

    iput v1, p1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;->views:I

    .line 29
    return-object p1
.end method
