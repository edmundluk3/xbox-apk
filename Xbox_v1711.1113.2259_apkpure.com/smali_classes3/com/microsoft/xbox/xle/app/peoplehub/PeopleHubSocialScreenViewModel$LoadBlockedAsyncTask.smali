.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PeopleHubSocialScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadBlockedAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field blockedUserData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V
    .locals 0

    .prologue
    .line 673
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$1;

    .prologue
    .line 673
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V

    return-void
.end method

.method private loadBlockedUserData()Z
    .locals 5

    .prologue
    .line 720
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 721
    .local v2, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getNeverListData()Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    move-result-object v3

    iget-object v3, v3, Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;->users:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverUser;

    .line 722
    .local v1, "user":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverUser;
    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverUser;->xuid:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 725
    .end local v1    # "user":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverUser;
    :cond_0
    :try_start_0
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->buildProfileDataWithPresenceInfoFromXuids(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->blockedUserData:Ljava/util/ArrayList;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 731
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 726
    :catch_0
    move-exception v0

    .line 727
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Failed to load blocked user profile data"

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 728
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 678
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 681
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 709
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 710
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->forceLoad:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadUserNeverList(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 711
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 712
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->loadBlockedUserData()Z

    move-result v1

    if-nez v1, :cond_0

    .line 713
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 716
    :cond_0
    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 704
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 1

    .prologue
    .line 687
    const-string v0, "This task should always take an action."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 688
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 699
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->blockedUserData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->onLoadBlockedCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/util/ArrayList;)V

    .line 700
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 673
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 692
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 693
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1302(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    .line 694
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel$LoadBlockedAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;->access$1402(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;Z)Z

    .line 695
    return-void
.end method
