.class public Lxbox/smartglass/Crash;
.super LMicrosoft/Telemetry/Data;
.source "Crash.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxbox/smartglass/Crash$Schema;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LMicrosoft/Telemetry/Data",
        "<",
        "Lxbox/smartglass/CommonData;",
        ">;"
    }
.end annotation


# instance fields
.field private callStack:Ljava/lang/String;

.field private errorCode:Ljava/lang/String;

.field private errorText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0}, LMicrosoft/Telemetry/Data;-><init>()V

    .line 229
    return-void
.end method

.method public static getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 222
    sget-object v0, Lxbox/smartglass/Crash$Schema;->schemaDef:Lcom/microsoft/bond/SchemaDef;

    return-object v0
.end method


# virtual methods
.method public clone()Lcom/microsoft/bond/BondSerializable;
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lxbox/smartglass/Crash;->clone()Lcom/microsoft/bond/BondSerializable;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Lcom/microsoft/bond/StructDef;)Lcom/microsoft/bond/BondMirror;
    .locals 1
    .param p1, "structDef"    # Lcom/microsoft/bond/StructDef;

    .prologue
    .line 206
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCallStack()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    return-object v0
.end method

.method public final getErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    return-object v0
.end method

.method public final getErrorText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    return-object v0
.end method

.method public getField(Lcom/microsoft/bond/FieldDef;)Ljava/lang/Object;
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 179
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 173
    :sswitch_0
    iget-object v0, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    goto :goto_0

    .line 175
    :sswitch_1
    iget-object v0, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    goto :goto_0

    .line 177
    :sswitch_2
    iget-object v0, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    goto :goto_0

    .line 171
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
    .end sparse-switch
.end method

.method public getSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 214
    invoke-static {}, Lxbox/smartglass/Crash;->getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v0

    return-object v0
.end method

.method public marshal(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 0
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    invoke-static {p0, p1}, Lcom/microsoft/bond/internal/Marshaler;->marshal(Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/ProtocolWriter;)V

    .line 361
    return-void
.end method

.method public memberwiseCompare(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 411
    if-nez p1, :cond_1

    .line 417
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 415
    check-cast v0, Lxbox/smartglass/Crash;

    .line 417
    .local v0, "that":Lxbox/smartglass/Crash;
    invoke-virtual {p0, v0}, Lxbox/smartglass/Crash;->memberwiseCompareQuick(Lxbox/smartglass/Crash;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lxbox/smartglass/Crash;->memberwiseCompareDeep(Lxbox/smartglass/Crash;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected memberwiseCompareDeep(Lxbox/smartglass/Crash;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/Crash;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 433
    const/4 v0, 0x1

    .line 434
    .local v0, "equals":Z
    if-eqz v0, :cond_3

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareDeep(LMicrosoft/Telemetry/Data;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    .line 435
    :goto_0
    if-eqz v0, :cond_5

    iget-object v3, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    if-nez v3, :cond_4

    :cond_0
    move v0, v1

    .line 436
    :goto_1
    if-eqz v0, :cond_7

    iget-object v3, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    if-nez v3, :cond_6

    :cond_1
    move v0, v1

    .line 437
    :goto_2
    if-eqz v0, :cond_9

    iget-object v3, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    if-nez v3, :cond_8

    :cond_2
    move v0, v1

    .line 438
    :goto_3
    return v0

    :cond_3
    move v0, v2

    .line 434
    goto :goto_0

    .line 435
    :cond_4
    iget-object v3, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_5
    move v0, v2

    goto :goto_1

    .line 436
    :cond_6
    iget-object v3, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_7
    move v0, v2

    goto :goto_2

    .line 437
    :cond_8
    iget-object v3, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_9
    move v0, v2

    goto :goto_3
.end method

.method protected memberwiseCompareQuick(Lxbox/smartglass/Crash;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/Crash;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 421
    const/4 v0, 0x1

    .line 422
    .local v0, "equals":Z
    if-eqz v0, :cond_3

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareQuick(LMicrosoft/Telemetry/Data;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v0, v2

    .line 423
    :goto_0
    if-eqz v0, :cond_6

    iget-object v1, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    iget-object v4, p1, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    if-nez v4, :cond_5

    move v4, v2

    :goto_2
    if-ne v1, v4, :cond_6

    move v0, v2

    .line 424
    :goto_3
    if-eqz v0, :cond_8

    iget-object v1, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    if-nez v1, :cond_7

    :cond_0
    move v0, v2

    .line 425
    :goto_4
    if-eqz v0, :cond_b

    iget-object v1, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    iget-object v4, p1, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    if-nez v4, :cond_a

    move v4, v2

    :goto_6
    if-ne v1, v4, :cond_b

    move v0, v2

    .line 426
    :goto_7
    if-eqz v0, :cond_d

    iget-object v1, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    if-nez v1, :cond_c

    :cond_1
    move v0, v2

    .line 427
    :goto_8
    if-eqz v0, :cond_10

    iget-object v1, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    if-nez v1, :cond_e

    move v1, v2

    :goto_9
    iget-object v4, p1, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    if-nez v4, :cond_f

    move v4, v2

    :goto_a
    if-ne v1, v4, :cond_10

    move v0, v2

    .line 428
    :goto_b
    if-eqz v0, :cond_12

    iget-object v1, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    if-nez v1, :cond_11

    :cond_2
    move v0, v2

    .line 429
    :goto_c
    return v0

    :cond_3
    move v0, v3

    .line 422
    goto :goto_0

    :cond_4
    move v1, v3

    .line 423
    goto :goto_1

    :cond_5
    move v4, v3

    goto :goto_2

    :cond_6
    move v0, v3

    goto :goto_3

    .line 424
    :cond_7
    iget-object v1, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_0

    :cond_8
    move v0, v3

    goto :goto_4

    :cond_9
    move v1, v3

    .line 425
    goto :goto_5

    :cond_a
    move v4, v3

    goto :goto_6

    :cond_b
    move v0, v3

    goto :goto_7

    .line 426
    :cond_c
    iget-object v1, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_1

    :cond_d
    move v0, v3

    goto :goto_8

    :cond_e
    move v1, v3

    .line 427
    goto :goto_9

    :cond_f
    move v4, v3

    goto :goto_a

    :cond_10
    move v0, v3

    goto :goto_b

    .line 428
    :cond_11
    iget-object v1, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_2

    :cond_12
    move v0, v3

    goto :goto_c
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readBegin()V

    .line 264
    invoke-virtual {p0, p1}, Lxbox/smartglass/Crash;->readNested(Lcom/microsoft/bond/ProtocolReader;)V

    .line 265
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readEnd()V

    .line 266
    return-void
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 284
    return-void
.end method

.method public readNested(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 272
    sget-object v0, Lcom/microsoft/bond/ProtocolCapability;->TAGGED:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v0}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 273
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Crash;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Crash;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    invoke-static {p1}, Lcom/microsoft/bond/internal/ReadHelper;->skipPartialStruct(Lcom/microsoft/bond/ProtocolReader;)V

    goto :goto_0
.end method

.method protected readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z
    .locals 5
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 313
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 315
    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 352
    :goto_0
    return v2

    .line 332
    .local v0, "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :sswitch_0
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    .line 346
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldEnd()V

    .line 322
    .end local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldBegin()Lcom/microsoft/bond/ProtocolReader$FieldTag;

    move-result-object v0

    .line 324
    .restart local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP:Lcom/microsoft/bond/BondDataType;

    if-eq v3, v4, :cond_1

    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_3

    .line 326
    :cond_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_2

    .line 349
    .local v1, "isPartial":Z
    :goto_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    move v2, v1

    .line 352
    goto :goto_0

    .end local v1    # "isPartial":Z
    :cond_2
    move v1, v2

    .line 326
    goto :goto_2

    .line 330
    :cond_3
    iget v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->id:I

    sparse-switch v3, :sswitch_data_0

    .line 342
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-virtual {p1, v3}, Lcom/microsoft/bond/ProtocolReader;->skip(Lcom/microsoft/bond/BondDataType;)V

    goto :goto_1

    .line 336
    :sswitch_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    goto :goto_1

    .line 339
    :sswitch_2
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    goto :goto_1

    .line 330
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
    .end sparse-switch
.end method

.method protected readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 287
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 289
    .local v0, "canOmitFields":Z
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 290
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 292
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 293
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    .line 300
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 301
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    .line 304
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_5

    .line 305
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    .line 307
    :cond_5
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    .line 308
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 235
    const-string v0, "Crash"

    const-string v1, "xbox.smartglass.Crash"

    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/Crash;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method protected reset(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "qualifiedName"    # Ljava/lang/String;

    .prologue
    .line 239
    invoke-super {p0, p1, p2}, LMicrosoft/Telemetry/Data;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    .line 241
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    .line 242
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    .line 243
    return-void
.end method

.method public final setCallStack(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public final setErrorCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public final setErrorText(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setField(Lcom/microsoft/bond/FieldDef;Ljava/lang/Object;)V
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 188
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 199
    .end local p2    # "value":Ljava/lang/Object;
    :goto_0
    return-void

    .line 190
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_0
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    goto :goto_0

    .line 193
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_1
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    goto :goto_0

    .line 196
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_2
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    goto :goto_0

    .line 188
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
    .end sparse-switch
.end method

.method public unmarshal(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    invoke-static {p1, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V

    .line 250
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 256
    check-cast p2, Lcom/microsoft/bond/SchemaDef;

    .end local p2    # "schema":Lcom/microsoft/bond/BondSerializable;
    invoke-static {p1, p2, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/BondSerializable;)V

    .line 257
    return-void
.end method

.method public write(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 2
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 367
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeBegin()V

    .line 369
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->getFirstPassWriter()Lcom/microsoft/bond/ProtocolWriter;

    move-result-object v0

    .local v0, "firstPassWriter":Lcom/microsoft/bond/ProtocolWriter;
    if-eqz v0, :cond_0

    .line 371
    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/Crash;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 372
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Crash;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 378
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeEnd()V

    .line 379
    return-void

    .line 376
    :cond_0
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Crash;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    goto :goto_0
.end method

.method public writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V
    .locals 6
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x1e

    const/16 v4, 0x14

    .line 382
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 383
    .local v0, "canOmitFields":Z
    sget-object v1, Lxbox/smartglass/Crash$Schema;->metadata:Lcom/microsoft/bond/Metadata;

    invoke-virtual {p1, v1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructBegin(Lcom/microsoft/bond/BondSerializable;Z)V

    .line 384
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 386
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0xa

    invoke-static {}, Lxbox/smartglass/Crash$Schema;->access$000()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 387
    iget-object v1, p0, Lxbox/smartglass/Crash;->errorText:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 388
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 390
    if-eqz v0, :cond_0

    iget-object v1, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    invoke-static {}, Lxbox/smartglass/Crash$Schema;->access$100()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/Metadata;->getDefault_value()Lcom/microsoft/bond/Variant;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/Variant;->getWstring_value()Ljava/lang/String;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 391
    :cond_0
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Crash$Schema;->access$100()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v4, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 392
    iget-object v1, p0, Lxbox/smartglass/Crash;->errorCode:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 393
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 398
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    invoke-static {}, Lxbox/smartglass/Crash$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/Metadata;->getDefault_value()Lcom/microsoft/bond/Variant;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/bond/Variant;->getWstring_value()Ljava/lang/String;

    move-result-object v2

    if-eq v1, v2, :cond_3

    .line 399
    :cond_1
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Crash$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v5, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 400
    iget-object v1, p0, Lxbox/smartglass/Crash;->callStack:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 401
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 406
    :goto_1
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructEnd(Z)V

    .line 407
    return-void

    .line 395
    :cond_2
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Crash$Schema;->access$100()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v4, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldOmitted(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    goto :goto_0

    .line 403
    :cond_3
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Crash$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v5, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldOmitted(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    goto :goto_1
.end method
