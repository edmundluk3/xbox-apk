.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;
.source "EDSV2MusicAlbumDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 37
    return-void
.end method


# virtual methods
.method protected bridge synthetic createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    .locals 1
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getArtistName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getArtistZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getArtistZuneId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getDesiredMediaItemType()I
    .locals 1

    .prologue
    .line 114
    const/16 v0, 0x3ef

    return v0
.end method

.method public getIsExplicit()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->IsExplicit:Z

    return v0
.end method

.method public getIsLoading()Z
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->getIsLoading()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getIsLoadingChild()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLabelOwner()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getLabelOwner()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLaunchType()Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->AppLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x5

    return v0
.end method

.method public getNumberOfTracks()I
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getNumberOfTracks()I

    move-result v0

    return v0
.end method

.method public getOrderBy()Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;->EDSV2ORDERBY_NUMBERASCENDING:Lcom/microsoft/xbox/service/model/media/MediaItemBase$OrderBy;

    return-object v0
.end method

.method public getPrimaryArtist()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getPrimaryArtist()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getTitleType()Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Application:Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method

.method public getTracks()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->browseListData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getZuneId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->detailRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;

    invoke-super {p0, p1, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 71
    .local v0, "item":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->loadListDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 72
    return-object v0
.end method

.method public shouldGetProviderActivities()Z
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    return v0
.end method
