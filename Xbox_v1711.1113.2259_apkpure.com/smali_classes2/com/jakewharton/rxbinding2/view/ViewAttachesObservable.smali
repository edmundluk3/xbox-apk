.class final Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable;
.super Lio/reactivex/Observable;
.source "ViewAttachesObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/reactivex/Observable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final callOnAttach:Z

.field private final view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Z)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "callOnAttach"    # Z

    .prologue
    .line 16
    invoke-direct {p0}, Lio/reactivex/Observable;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable;->view:Landroid/view/View;

    .line 18
    iput-boolean p2, p0, Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable;->callOnAttach:Z

    .line 19
    return-void
.end method


# virtual methods
.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Ljava/lang/Object;>;"
    invoke-static {p1}, Lcom/jakewharton/rxbinding2/internal/Preconditions;->checkMainThread(Lio/reactivex/Observer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 28
    :goto_0
    return-void

    .line 25
    :cond_0
    new-instance v0, Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable$Listener;

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable;->view:Landroid/view/View;

    iget-boolean v2, p0, Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable;->callOnAttach:Z

    invoke-direct {v0, v1, v2, p1}, Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable$Listener;-><init>(Landroid/view/View;ZLio/reactivex/Observer;)V

    .line 26
    .local v0, "listener":Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable$Listener;
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 27
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewAttachesObservable;->view:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto :goto_0
.end method
