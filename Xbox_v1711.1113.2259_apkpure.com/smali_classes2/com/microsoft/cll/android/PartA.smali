.class public abstract Lcom/microsoft/cll/android/PartA;
.super Ljava/lang/Object;
.source "PartA.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field protected final appExt:Lcom/microsoft/telemetry/extensions/app;

.field protected appId:Ljava/lang/String;

.field protected appVer:Ljava/lang/String;

.field private correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

.field private final csVer:Ljava/lang/String;

.field protected final deviceExt:Lcom/microsoft/telemetry/extensions/device;

.field private epoch:J

.field private flags:J

.field private final hexArray:[C

.field private iKey:Ljava/lang/String;

.field protected final logger:Lcom/microsoft/cll/android/ILogger;

.field protected final osExt:Lcom/microsoft/telemetry/extensions/os;

.field protected osName:Ljava/lang/String;

.field protected osVer:Ljava/lang/String;

.field private random:Ljava/util/Random;

.field private final salt:Ljava/lang/String;

.field protected final seqCounter:Ljava/util/concurrent/atomic/AtomicLong;

.field private serializer:Lcom/microsoft/cll/android/EventSerializer;

.field protected uniqueId:Ljava/lang/String;

.field private useLegacyCS:Z

.field protected final userExt:Lcom/microsoft/telemetry/extensions/user;


# direct methods
.method public constructor <init>(Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/CorrelationVector;)V
    .locals 4
    .param p1, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p2, "iKey"    # Ljava/lang/String;
    .param p3, "correlationVector"    # Lcom/microsoft/cll/android/CorrelationVector;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, "2.1"

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->csVer:Ljava/lang/String;

    .line 40
    const-string v0, "PartA"

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->TAG:Ljava/lang/String;

    .line 41
    const-string v0, "oRq=MAHHHC~6CCe|JfEqRZ+gc0ESI||g2Jlb^PYjc5UYN2P 27z_+21xxd2n"

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->salt:Ljava/lang/String;

    .line 42
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->hexArray:[C

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/cll/android/PartA;->useLegacyCS:Z

    .line 60
    iput-object p1, p0, Lcom/microsoft/cll/android/PartA;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 61
    iput-object p2, p0, Lcom/microsoft/cll/android/PartA;->iKey:Ljava/lang/String;

    .line 62
    iput-object p3, p0, Lcom/microsoft/cll/android/PartA;->correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

    .line 63
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->seqCounter:Ljava/util/concurrent/atomic/AtomicLong;

    .line 64
    new-instance v0, Lcom/microsoft/cll/android/EventSerializer;

    invoke-direct {v0, p1}, Lcom/microsoft/cll/android/EventSerializer;-><init>(Lcom/microsoft/cll/android/ILogger;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->serializer:Lcom/microsoft/cll/android/EventSerializer;

    .line 66
    new-instance v0, Lcom/microsoft/telemetry/extensions/user;

    invoke-direct {v0}, Lcom/microsoft/telemetry/extensions/user;-><init>()V

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->userExt:Lcom/microsoft/telemetry/extensions/user;

    .line 67
    new-instance v0, Lcom/microsoft/telemetry/extensions/device;

    invoke-direct {v0}, Lcom/microsoft/telemetry/extensions/device;-><init>()V

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->deviceExt:Lcom/microsoft/telemetry/extensions/device;

    .line 68
    new-instance v0, Lcom/microsoft/telemetry/extensions/os;

    invoke-direct {v0}, Lcom/microsoft/telemetry/extensions/os;-><init>()V

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->osExt:Lcom/microsoft/telemetry/extensions/os;

    .line 69
    new-instance v0, Lcom/microsoft/telemetry/extensions/app;

    invoke-direct {v0}, Lcom/microsoft/telemetry/extensions/app;-><init>()V

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->appExt:Lcom/microsoft/telemetry/extensions/app;

    .line 71
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/microsoft/cll/android/PartA;->random:Ljava/util/Random;

    .line 72
    iget-object v0, p0, Lcom/microsoft/cll/android/PartA;->random:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/cll/android/PartA;->epoch:J

    .line 73
    return-void
.end method

.method private bytesToHex([B)Ljava/lang/String;
    .locals 6
    .param p1, "bytes"    # [B

    .prologue
    .line 230
    array-length v3, p1

    mul-int/lit8 v3, v3, 0x2

    new-array v0, v3, [C

    .line 231
    .local v0, "hexChars":[C
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_0

    .line 232
    aget-byte v3, p1, v1

    and-int/lit16 v2, v3, 0xff

    .line 233
    .local v2, "v":I
    mul-int/lit8 v3, v1, 0x2

    iget-object v4, p0, Lcom/microsoft/cll/android/PartA;->hexArray:[C

    ushr-int/lit8 v5, v2, 0x4

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 234
    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/microsoft/cll/android/PartA;->hexArray:[C

    and-int/lit8 v5, v2, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 231
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 236
    .end local v2    # "v":I
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    return-object v3
.end method

.method private createExtensions(Ljava/util/List;)Ljava/util/LinkedHashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/telemetry/Extension;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 241
    .local v1, "extensions":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Lcom/microsoft/telemetry/Extension;>;"
    const-string v2, "user"

    iget-object v3, p0, Lcom/microsoft/cll/android/PartA;->userExt:Lcom/microsoft/telemetry/extensions/user;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const-string v2, "os"

    iget-object v3, p0, Lcom/microsoft/cll/android/PartA;->osExt:Lcom/microsoft/telemetry/extensions/os;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    const-string v2, "device"

    iget-object v3, p0, Lcom/microsoft/cll/android/PartA;->deviceExt:Lcom/microsoft/telemetry/extensions/device;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    new-instance v0, Lcom/microsoft/telemetry/extensions/android;

    invoke-direct {v0}, Lcom/microsoft/telemetry/extensions/android;-><init>()V

    .line 246
    .local v0, "androidExt":Lcom/microsoft/telemetry/extensions/android;
    const-string v2, "3.0.2"

    invoke-virtual {v0, v2}, Lcom/microsoft/telemetry/extensions/android;->setLibVer(Ljava/lang/String;)V

    .line 247
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 248
    invoke-virtual {v0, p1}, Lcom/microsoft/telemetry/extensions/android;->setTickets(Ljava/util/List;)V

    .line 251
    :cond_0
    const-string v2, "android"

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    iget-object v2, p0, Lcom/microsoft/cll/android/PartA;->appExt:Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v2}, Lcom/microsoft/telemetry/extensions/app;->getExpId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/cll/android/PartA;->appExt:Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v2}, Lcom/microsoft/telemetry/extensions/app;->getUserId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 254
    :cond_1
    const-string v2, "app"

    iget-object v3, p0, Lcom/microsoft/cll/android/PartA;->appExt:Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v1, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    :cond_2
    return-object v1
.end method

.method private getDateTime()Ljava/lang/String;
    .locals 3

    .prologue
    .line 356
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 357
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 359
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private varargs getSensitivityLevel([Lcom/microsoft/cll/android/EventSensitivity;)I
    .locals 6
    .param p1, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;

    .prologue
    .line 334
    sget-object v5, Lcom/microsoft/cll/android/EventSensitivity;->None:Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v5}, Lcom/microsoft/cll/android/EventSensitivity;->getCode()I

    move-result v3

    .line 335
    .local v3, "level":I
    move-object v0, p1

    .local v0, "arr$":[Lcom/microsoft/cll/android/EventSensitivity;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 336
    .local v4, "sensitivity":Lcom/microsoft/cll/android/EventSensitivity;
    sget-object v5, Lcom/microsoft/cll/android/EventSensitivity;->Drop:Lcom/microsoft/cll/android/EventSensitivity;

    if-ne v4, v5, :cond_1

    .line 337
    sget-object v5, Lcom/microsoft/cll/android/EventSensitivity;->Drop:Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v5}, Lcom/microsoft/cll/android/EventSensitivity;->getCode()I

    move-result v3

    .line 347
    .end local v4    # "sensitivity":Lcom/microsoft/cll/android/EventSensitivity;
    :cond_0
    return v3

    .line 341
    .restart local v4    # "sensitivity":Lcom/microsoft/cll/android/EventSensitivity;
    :cond_1
    sget-object v5, Lcom/microsoft/cll/android/EventSensitivity;->Hash:Lcom/microsoft/cll/android/EventSensitivity;

    if-ne v4, v5, :cond_2

    .line 342
    sget-object v5, Lcom/microsoft/cll/android/EventSensitivity;->Hash:Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v5}, Lcom/microsoft/cll/android/EventSensitivity;->getCode()I

    move-result v3

    .line 335
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private populateSerializedEvent(Ljava/lang/String;Lcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;DLjava/lang/String;)Lcom/microsoft/cll/android/SerializedEvent;
    .locals 2
    .param p1, "eventData"    # Ljava/lang/String;
    .param p2, "persistence"    # Lcom/microsoft/cll/android/Cll$EventPersistence;
    .param p3, "latency"    # Lcom/microsoft/cll/android/Cll$EventLatency;
    .param p4, "sampleRate"    # D
    .param p6, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 428
    new-instance v0, Lcom/microsoft/cll/android/SerializedEvent;

    invoke-direct {v0}, Lcom/microsoft/cll/android/SerializedEvent;-><init>()V

    .line 429
    .local v0, "event":Lcom/microsoft/cll/android/SerializedEvent;
    invoke-virtual {v0, p1}, Lcom/microsoft/cll/android/SerializedEvent;->setSerializedData(Ljava/lang/String;)V

    .line 430
    invoke-virtual {v0, p4, p5}, Lcom/microsoft/cll/android/SerializedEvent;->setSampleRate(D)V

    .line 431
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->deviceExt:Lcom/microsoft/telemetry/extensions/device;

    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getLocalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/cll/android/SerializedEvent;->setDeviceId(Ljava/lang/String;)V

    .line 432
    invoke-virtual {v0, p2}, Lcom/microsoft/cll/android/SerializedEvent;->setPersistence(Lcom/microsoft/cll/android/Cll$EventPersistence;)V

    .line 433
    invoke-virtual {v0, p3}, Lcom/microsoft/cll/android/SerializedEvent;->setLatency(Lcom/microsoft/cll/android/Cll$EventLatency;)V

    .line 434
    return-object v0
.end method

.method private varargs scrubPII(Lcom/microsoft/telemetry/Envelope;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .locals 13
    .param p1, "envelope"    # Lcom/microsoft/telemetry/Envelope;
    .param p2, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;

    .prologue
    const/4 v12, 0x0

    .line 261
    if-nez p2, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    invoke-direct {p0, p2}, Lcom/microsoft/cll/android/PartA;->getSensitivityLevel([Lcom/microsoft/cll/android/EventSensitivity;)I

    move-result v2

    .line 266
    .local v2, "level":I
    sget-object v7, Lcom/microsoft/cll/android/EventSensitivity;->None:Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v7}, Lcom/microsoft/cll/android/EventSensitivity;->getCode()I

    move-result v7

    if-eq v2, v7, :cond_0

    .line 271
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "user"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/telemetry/extensions/user;

    .line 272
    .local v6, "userExtensionFromEnvelope":Lcom/microsoft/telemetry/extensions/user;
    new-instance v5, Lcom/microsoft/telemetry/extensions/user;

    invoke-direct {v5}, Lcom/microsoft/telemetry/extensions/user;-><init>()V

    .line 273
    .local v5, "newUserExtension":Lcom/microsoft/telemetry/extensions/user;
    invoke-virtual {v6}, Lcom/microsoft/telemetry/extensions/user;->getLocalId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/microsoft/telemetry/extensions/user;->setLocalId(Ljava/lang/String;)V

    .line 274
    invoke-virtual {v6}, Lcom/microsoft/telemetry/extensions/user;->getAuthId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/microsoft/telemetry/extensions/user;->setAuthId(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v6}, Lcom/microsoft/telemetry/extensions/user;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/microsoft/telemetry/extensions/user;->setId(Ljava/lang/String;)V

    .line 276
    invoke-virtual {v6}, Lcom/microsoft/telemetry/extensions/user;->getVer()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/microsoft/telemetry/extensions/user;->setVer(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "user"

    invoke-interface {v7, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "device"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/telemetry/extensions/device;

    .line 280
    .local v1, "deviceExtensionFromEnvelope":Lcom/microsoft/telemetry/extensions/device;
    new-instance v4, Lcom/microsoft/telemetry/extensions/device;

    invoke-direct {v4}, Lcom/microsoft/telemetry/extensions/device;-><init>()V

    .line 281
    .local v4, "newDeviceExtension":Lcom/microsoft/telemetry/extensions/device;
    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getLocalId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/telemetry/extensions/device;->setLocalId(Ljava/lang/String;)V

    .line 282
    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getVer()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/telemetry/extensions/device;->setVer(Ljava/lang/String;)V

    .line 283
    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/telemetry/extensions/device;->setId(Ljava/lang/String;)V

    .line 284
    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getAuthId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/telemetry/extensions/device;->setAuthId(Ljava/lang/String;)V

    .line 285
    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getAuthSecId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/telemetry/extensions/device;->setAuthSecId(Ljava/lang/String;)V

    .line 286
    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getDeviceClass()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/telemetry/extensions/device;->setDeviceClass(Ljava/lang/String;)V

    .line 287
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "device"

    invoke-interface {v7, v8, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "app"

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 291
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "app"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/telemetry/extensions/app;

    .line 292
    .local v0, "appExtensionFromEnvelope":Lcom/microsoft/telemetry/extensions/app;
    new-instance v3, Lcom/microsoft/telemetry/extensions/app;

    invoke-direct {v3}, Lcom/microsoft/telemetry/extensions/app;-><init>()V

    .line 293
    .local v3, "newAppExtension":Lcom/microsoft/telemetry/extensions/app;
    invoke-virtual {v0}, Lcom/microsoft/telemetry/extensions/app;->getExpId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/microsoft/telemetry/extensions/app;->setExpId(Ljava/lang/String;)V

    .line 294
    invoke-virtual {v0}, Lcom/microsoft/telemetry/extensions/app;->getUserId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/microsoft/telemetry/extensions/app;->setUserId(Ljava/lang/String;)V

    .line 295
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "app"

    invoke-interface {v7, v8, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    .end local v0    # "appExtensionFromEnvelope":Lcom/microsoft/telemetry/extensions/app;
    .end local v3    # "newAppExtension":Lcom/microsoft/telemetry/extensions/app;
    :cond_2
    sget-object v7, Lcom/microsoft/cll/android/EventSensitivity;->Drop:Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v7}, Lcom/microsoft/cll/android/EventSensitivity;->getCode()I

    move-result v7

    if-ne v2, v7, :cond_5

    .line 300
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "user"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/telemetry/extensions/user;

    invoke-virtual {v7, v12}, Lcom/microsoft/telemetry/extensions/user;->setLocalId(Ljava/lang/String;)V

    .line 301
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "device"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/telemetry/extensions/device;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "r:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/microsoft/cll/android/PartA;->random:Ljava/util/Random;

    invoke-virtual {v9}, Ljava/util/Random;->nextLong()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/telemetry/extensions/device;->setLocalId(Ljava/lang/String;)V

    .line 303
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "app"

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 304
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "app"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v7, v12}, Lcom/microsoft/telemetry/extensions/app;->setUserId(Ljava/lang/String;)V

    .line 309
    :cond_3
    iget-object v7, p0, Lcom/microsoft/cll/android/PartA;->correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

    iget-boolean v7, v7, Lcom/microsoft/cll/android/CorrelationVector;->isInitialized:Z

    if-eqz v7, :cond_4

    .line 310
    invoke-virtual {p1, v12}, Lcom/microsoft/telemetry/Envelope;->setCV(Ljava/lang/String;)V

    .line 313
    :cond_4
    invoke-virtual {p1, v12}, Lcom/microsoft/telemetry/Envelope;->setEpoch(Ljava/lang/String;)V

    .line 314
    const-wide/16 v8, 0x0

    invoke-virtual {p1, v8, v9}, Lcom/microsoft/telemetry/Envelope;->setSeqNum(J)V

    goto/16 :goto_0

    .line 315
    :cond_5
    sget-object v7, Lcom/microsoft/cll/android/EventSensitivity;->Hash:Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v7}, Lcom/microsoft/cll/android/EventSensitivity;->getCode()I

    move-result v7

    if-ne v2, v7, :cond_0

    .line 317
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "user"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/telemetry/extensions/user;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "d:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v8

    const-string v10, "user"

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/telemetry/extensions/user;

    invoke-virtual {v8}, Lcom/microsoft/telemetry/extensions/user;->getLocalId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/microsoft/cll/android/PartA;->HashStringSha256(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/telemetry/extensions/user;->setLocalId(Ljava/lang/String;)V

    .line 318
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "device"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/telemetry/extensions/device;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "d:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v8

    const-string v10, "device"

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/telemetry/extensions/device;

    invoke-virtual {v8}, Lcom/microsoft/telemetry/extensions/device;->getLocalId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/microsoft/cll/android/PartA;->HashStringSha256(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/telemetry/extensions/device;->setLocalId(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "app"

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 321
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v7

    const-string v8, "app"

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/telemetry/extensions/app;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "d:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getExt()Ljava/util/Map;

    move-result-object v8

    const-string v10, "app"

    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v8}, Lcom/microsoft/telemetry/extensions/app;->getUserId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/microsoft/cll/android/PartA;->HashStringSha256(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/telemetry/extensions/app;->setUserId(Ljava/lang/String;)V

    .line 325
    :cond_6
    iget-object v7, p0, Lcom/microsoft/cll/android/PartA;->correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

    iget-boolean v7, v7, Lcom/microsoft/cll/android/CorrelationVector;->isInitialized:Z

    if-eqz v7, :cond_7

    .line 326
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getCV()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/microsoft/cll/android/PartA;->HashStringSha256(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/microsoft/telemetry/Envelope;->setCV(Ljava/lang/String;)V

    .line 329
    :cond_7
    invoke-virtual {p1}, Lcom/microsoft/telemetry/Envelope;->getEpoch()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/microsoft/cll/android/PartA;->HashStringSha256(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/microsoft/telemetry/Envelope;->setEpoch(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private setBaseType(Lcom/microsoft/telemetry/Base;)V
    .locals 6
    .param p1, "base"    # Lcom/microsoft/telemetry/Base;

    .prologue
    .line 369
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/microsoft/telemetry/Data;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/microsoft/telemetry/Data;->getBaseData()Lcom/microsoft/telemetry/Domain;

    move-result-object v3

    iget-object v1, v3, Lcom/microsoft/telemetry/Domain;->QualifiedName:Ljava/lang/String;

    .line 370
    .local v1, "baseType":Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/microsoft/telemetry/Base;->setBaseType(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 374
    .end local v1    # "baseType":Ljava/lang/String;
    :goto_0
    return-void

    .line 371
    :catch_0
    move-exception v2

    .line 372
    .local v2, "e":Ljava/lang/ClassCastException;
    iget-object v3, p0, Lcom/microsoft/cll/android/PartA;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v4, "PartA"

    const-string v5, "This event doesn\'t extend data"

    invoke-interface {v3, v4, v5}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private varargs setFlags(Lcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;Lcom/microsoft/telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)J
    .locals 10
    .param p1, "persistence"    # Lcom/microsoft/cll/android/Cll$EventPersistence;
    .param p2, "latency"    # Lcom/microsoft/cll/android/Cll$EventLatency;
    .param p3, "event"    # Lcom/microsoft/telemetry/Base;
    .param p4, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;

    .prologue
    .line 383
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    .line 386
    iget-object v5, p3, Lcom/microsoft/telemetry/Base;->Attributes:Ljava/util/LinkedHashMap;

    const-string v6, "Sensitivity"

    invoke-virtual {v5, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 387
    .local v4, "userSensitivity":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "UserSensitive"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    .line 388
    iget-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    sget-object v5, Lcom/microsoft/cll/android/EventSensitivity;->Mark:Lcom/microsoft/cll/android/EventSensitivity;

    invoke-virtual {v5}, Lcom/microsoft/cll/android/EventSensitivity;->getCode()I

    move-result v5

    int-to-long v8, v5

    or-long/2addr v6, v8

    iput-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    .line 392
    :cond_0
    if-eqz p4, :cond_1

    .line 393
    move-object v0, p4

    .local v0, "arr$":[Lcom/microsoft/cll/android/EventSensitivity;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 394
    .local v3, "sensitivity":Lcom/microsoft/cll/android/EventSensitivity;
    iget-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    invoke-virtual {v3}, Lcom/microsoft/cll/android/EventSensitivity;->getCode()I

    move-result v5

    int-to-long v8, v5

    or-long/2addr v6, v8

    iput-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    .line 393
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 399
    .end local v0    # "arr$":[Lcom/microsoft/cll/android/EventSensitivity;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "sensitivity":Lcom/microsoft/cll/android/EventSensitivity;
    :cond_1
    iget-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    invoke-virtual {p2}, Lcom/microsoft/cll/android/Cll$EventLatency;->getCode()I

    move-result v5

    int-to-long v8, v5

    or-long/2addr v6, v8

    iput-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    .line 402
    iget-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    invoke-virtual {p1}, Lcom/microsoft/cll/android/Cll$EventPersistence;->getCode()I

    move-result v5

    int-to-long v8, v5

    or-long/2addr v6, v8

    iput-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    .line 404
    iget-wide v6, p0, Lcom/microsoft/cll/android/PartA;->flags:J

    return-wide v6
.end method

.method private varargs setSeq([Lcom/microsoft/cll/android/EventSensitivity;)J
    .locals 7
    .param p1, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;

    .prologue
    .line 415
    if-eqz p1, :cond_1

    .line 416
    move-object v0, p1

    .local v0, "arr$":[Lcom/microsoft/cll/android/EventSensitivity;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 417
    .local v3, "sensitivity":Lcom/microsoft/cll/android/EventSensitivity;
    sget-object v6, Lcom/microsoft/cll/android/EventSensitivity;->Drop:Lcom/microsoft/cll/android/EventSensitivity;

    if-ne v3, v6, :cond_0

    .line 418
    const-wide/16 v4, 0x0

    .line 424
    .end local v0    # "arr$":[Lcom/microsoft/cll/android/EventSensitivity;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "sensitivity":Lcom/microsoft/cll/android/EventSensitivity;
    :goto_1
    return-wide v4

    .line 416
    .restart local v0    # "arr$":[Lcom/microsoft/cll/android/EventSensitivity;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "sensitivity":Lcom/microsoft/cll/android/EventSensitivity;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 423
    .end local v0    # "arr$":[Lcom/microsoft/cll/android/EventSensitivity;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "sensitivity":Lcom/microsoft/cll/android/EventSensitivity;
    :cond_1
    iget-object v6, p0, Lcom/microsoft/cll/android/PartA;->seqCounter:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v4

    .line 424
    .local v4, "uploadId":J
    goto :goto_1
.end method


# virtual methods
.method protected HashStringSha256(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 193
    if-nez p1, :cond_0

    .line 194
    const-string v3, ""

    .line 210
    :goto_0
    return-object v3

    .line 199
    :cond_0
    :try_start_0
    const-string v3, "SHA-256"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 200
    .local v1, "hash":Ljava/security/MessageDigest;
    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    .line 201
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 202
    const-string v3, "oRq=MAHHHC~6CCe|JfEqRZ+gc0ESI||g2Jlb^PYjc5UYN2P 27z_+21xxd2n"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 203
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .line 204
    .local v2, "hashed":[B
    invoke-direct {p0, v2}, Lcom/microsoft/cll/android/PartA;->bytesToHex([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 205
    .end local v1    # "hash":Ljava/security/MessageDigest;
    .end local v2    # "hashed":[B
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 210
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected abstract PopulateConstantValues()V
.end method

.method getAppUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/microsoft/cll/android/PartA;->appExt:Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v0}, Lcom/microsoft/telemetry/extensions/app;->getUserId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public varargs populate(Lcom/microsoft/telemetry/Base;Ljava/util/List;Ljava/util/Map;[Lcom/microsoft/cll/android/EventSensitivity;)Lcom/microsoft/cll/android/SerializedEvent;
    .locals 14
    .param p1, "base"    # Lcom/microsoft/telemetry/Base;
    .param p4, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")",
            "Lcom/microsoft/cll/android/SerializedEvent;"
        }
    .end annotation

    .prologue
    .line 81
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "tags":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->SAMPLERATE:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {p1, v0}, Lcom/microsoft/cll/android/SettingsStore;->getSetting(Lcom/microsoft/telemetry/Base;Lcom/microsoft/cll/android/SettingsStore$Settings;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 82
    .local v3, "sampleRate":I
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->PERSISTENCE:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {p1, v0}, Lcom/microsoft/cll/android/SettingsStore;->getSetting(Lcom/microsoft/telemetry/Base;Lcom/microsoft/cll/android/SettingsStore$Settings;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/cll/android/Cll$EventPersistence;->valueOf(Ljava/lang/String;)Lcom/microsoft/cll/android/Cll$EventPersistence;

    move-result-object v4

    .line 83
    .local v4, "persistence":Lcom/microsoft/cll/android/Cll$EventPersistence;
    sget-object v0, Lcom/microsoft/cll/android/SettingsStore$Settings;->LATENCY:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {p1, v0}, Lcom/microsoft/cll/android/SettingsStore;->getSetting(Lcom/microsoft/telemetry/Base;Lcom/microsoft/cll/android/SettingsStore$Settings;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/cll/android/Cll$EventLatency;->valueOf(Ljava/lang/String;)Lcom/microsoft/cll/android/Cll$EventLatency;

    move-result-object v5

    .line 85
    .local v5, "latency":Lcom/microsoft/cll/android/Cll$EventLatency;
    iget-boolean v0, p0, Lcom/microsoft/cll/android/PartA;->useLegacyCS:Z

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/cll/android/PartA;->correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/CorrelationVector;->GetValue()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/microsoft/cll/android/PartA;->populateLegacyEnvelope(Lcom/microsoft/telemetry/Base;Ljava/lang/String;ILcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;Ljava/util/Map;[Lcom/microsoft/cll/android/EventSensitivity;)Lcom/microsoft/telemetry/cs2/Envelope;

    move-result-object v13

    .line 87
    .local v13, "envelope":Lcom/microsoft/telemetry/cs2/Envelope;
    iget-object v0, p0, Lcom/microsoft/cll/android/PartA;->serializer:Lcom/microsoft/cll/android/EventSerializer;

    invoke-virtual {v0, v13}, Lcom/microsoft/cll/android/EventSerializer;->serialize(Lcom/microsoft/telemetry/IJsonSerializable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v13}, Lcom/microsoft/telemetry/cs2/Envelope;->getSampleRate()D

    move-result-wide v10

    invoke-virtual {v13}, Lcom/microsoft/telemetry/cs2/Envelope;->getDeviceId()Ljava/lang/String;

    move-result-object v12

    move-object v6, p0

    move-object v8, v4

    move-object v9, v5

    invoke-direct/range {v6 .. v12}, Lcom/microsoft/cll/android/PartA;->populateSerializedEvent(Ljava/lang/String;Lcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;DLjava/lang/String;)Lcom/microsoft/cll/android/SerializedEvent;

    move-result-object v0

    .line 90
    .end local v13    # "envelope":Lcom/microsoft/telemetry/cs2/Envelope;
    :goto_0
    return-object v0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/microsoft/cll/android/PartA;->correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/CorrelationVector;->GetValue()Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/microsoft/cll/android/PartA;->populateEnvelope(Lcom/microsoft/telemetry/Base;Ljava/lang/String;ILcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)Lcom/microsoft/telemetry/Envelope;

    move-result-object v13

    .line 90
    .local v13, "envelope":Lcom/microsoft/telemetry/Envelope;
    iget-object v0, p0, Lcom/microsoft/cll/android/PartA;->serializer:Lcom/microsoft/cll/android/EventSerializer;

    invoke-virtual {v0, v13}, Lcom/microsoft/cll/android/EventSerializer;->serialize(Lcom/microsoft/telemetry/IJsonSerializable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v13}, Lcom/microsoft/telemetry/Envelope;->getPopSample()D

    move-result-wide v10

    iget-object v0, p0, Lcom/microsoft/cll/android/PartA;->deviceExt:Lcom/microsoft/telemetry/extensions/device;

    invoke-virtual {v0}, Lcom/microsoft/telemetry/extensions/device;->getLocalId()Ljava/lang/String;

    move-result-object v12

    move-object v6, p0

    move-object v8, v4

    move-object v9, v5

    invoke-direct/range {v6 .. v12}, Lcom/microsoft/cll/android/PartA;->populateSerializedEvent(Ljava/lang/String;Lcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;DLjava/lang/String;)Lcom/microsoft/cll/android/SerializedEvent;

    move-result-object v0

    goto :goto_0
.end method

.method public varargs populateEnvelope(Lcom/microsoft/telemetry/Base;Ljava/lang/String;ILcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)Lcom/microsoft/telemetry/Envelope;
    .locals 4
    .param p1, "base"    # Lcom/microsoft/telemetry/Base;
    .param p2, "cV"    # Ljava/lang/String;
    .param p3, "sampleRate"    # I
    .param p4, "persistence"    # Lcom/microsoft/cll/android/Cll$EventPersistence;
    .param p5, "latency"    # Lcom/microsoft/cll/android/Cll$EventLatency;
    .param p7, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/lang/String;",
            "I",
            "Lcom/microsoft/cll/android/Cll$EventPersistence;",
            "Lcom/microsoft/cll/android/Cll$EventLatency;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")",
            "Lcom/microsoft/telemetry/Envelope;"
        }
    .end annotation

    .prologue
    .line 95
    .local p6, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/microsoft/telemetry/Envelope;

    invoke-direct {v0}, Lcom/microsoft/telemetry/Envelope;-><init>()V

    .line 96
    .local v0, "envelope":Lcom/microsoft/telemetry/Envelope;
    invoke-direct {p0, p1}, Lcom/microsoft/cll/android/PartA;->setBaseType(Lcom/microsoft/telemetry/Base;)V

    .line 97
    const-string v1, "2.1"

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setVer(Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Lcom/microsoft/cll/android/PartA;->getDateTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setTime(Ljava/lang/String;)V

    .line 99
    iget-object v1, p1, Lcom/microsoft/telemetry/Base;->QualifiedName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setName(Ljava/lang/String;)V

    .line 100
    int-to-double v2, p3

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/telemetry/Envelope;->setPopSample(D)V

    .line 101
    iget-wide v2, p0, Lcom/microsoft/cll/android/PartA;->epoch:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setEpoch(Ljava/lang/String;)V

    .line 102
    invoke-direct {p0, p7}, Lcom/microsoft/cll/android/PartA;->setSeq([Lcom/microsoft/cll/android/EventSensitivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/telemetry/Envelope;->setSeqNum(J)V

    .line 103
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->osName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setOs(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->osVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setOsVer(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v0, p1}, Lcom/microsoft/telemetry/Envelope;->setData(Lcom/microsoft/telemetry/Base;)V

    .line 106
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setAppId(Ljava/lang/String;)V

    .line 107
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->appVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setAppVer(Ljava/lang/String;)V

    .line 109
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

    iget-boolean v1, v1, Lcom/microsoft/cll/android/CorrelationVector;->isInitialized:Z

    if-eqz v1, :cond_0

    .line 110
    invoke-virtual {v0, p2}, Lcom/microsoft/telemetry/Envelope;->setCV(Ljava/lang/String;)V

    .line 113
    :cond_0
    invoke-direct {p0, p4, p5, p1, p7}, Lcom/microsoft/cll/android/PartA;->setFlags(Lcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;Lcom/microsoft/telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/telemetry/Envelope;->setFlags(J)V

    .line 114
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->iKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setIKey(Ljava/lang/String;)V

    .line 115
    invoke-direct {p0, p6}, Lcom/microsoft/cll/android/PartA;->createExtensions(Ljava/util/List;)Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/Envelope;->setExt(Ljava/util/Map;)V

    .line 117
    invoke-direct {p0, v0, p7}, Lcom/microsoft/cll/android/PartA;->scrubPII(Lcom/microsoft/telemetry/Envelope;[Lcom/microsoft/cll/android/EventSensitivity;)V

    .line 118
    return-object v0
.end method

.method public varargs populateLegacyEnvelope(Lcom/microsoft/telemetry/Base;Ljava/lang/String;ILcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;Ljava/util/Map;[Lcom/microsoft/cll/android/EventSensitivity;)Lcom/microsoft/telemetry/cs2/Envelope;
    .locals 4
    .param p1, "base"    # Lcom/microsoft/telemetry/Base;
    .param p2, "cV"    # Ljava/lang/String;
    .param p3, "sampleRate"    # I
    .param p4, "persistence"    # Lcom/microsoft/cll/android/Cll$EventPersistence;
    .param p5, "latency"    # Lcom/microsoft/cll/android/Cll$EventLatency;
    .param p7, "sensitivities"    # [Lcom/microsoft/cll/android/EventSensitivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/lang/String;",
            "I",
            "Lcom/microsoft/cll/android/Cll$EventPersistence;",
            "Lcom/microsoft/cll/android/Cll$EventLatency;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")",
            "Lcom/microsoft/telemetry/cs2/Envelope;"
        }
    .end annotation

    .prologue
    .line 122
    .local p6, "tags":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p6, :cond_0

    .line 123
    new-instance p6, Ljava/util/HashMap;

    .end local p6    # "tags":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p6}, Ljava/util/HashMap;-><init>()V

    .line 126
    .restart local p6    # "tags":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->correlationVector:Lcom/microsoft/cll/android/CorrelationVector;

    iget-boolean v1, v1, Lcom/microsoft/cll/android/CorrelationVector;->isInitialized:Z

    if-eqz v1, :cond_1

    .line 127
    const-string v1, "cV"

    invoke-interface {p6, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    :cond_1
    new-instance v0, Lcom/microsoft/telemetry/cs2/Envelope;

    invoke-direct {v0}, Lcom/microsoft/telemetry/cs2/Envelope;-><init>()V

    .line 131
    .local v0, "envelope":Lcom/microsoft/telemetry/cs2/Envelope;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setVer(I)V

    .line 132
    invoke-direct {p0}, Lcom/microsoft/cll/android/PartA;->getDateTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setTime(Ljava/lang/String;)V

    .line 133
    iget-object v1, p1, Lcom/microsoft/telemetry/Base;->QualifiedName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setName(Ljava/lang/String;)V

    .line 134
    int-to-double v2, p3

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/telemetry/cs2/Envelope;->setSampleRate(D)V

    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/microsoft/cll/android/PartA;->epoch:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p7}, Lcom/microsoft/cll/android/PartA;->setSeq([Lcom/microsoft/cll/android/EventSensitivity;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setSeq(Ljava/lang/String;)V

    .line 136
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->osName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setOs(Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->osVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setOsVer(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v0, p1}, Lcom/microsoft/telemetry/cs2/Envelope;->setData(Lcom/microsoft/telemetry/Base;)V

    .line 139
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->appId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setAppId(Ljava/lang/String;)V

    .line 140
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->appVer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setAppVer(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v0, p6}, Lcom/microsoft/telemetry/cs2/Envelope;->setTags(Ljava/util/Map;)V

    .line 142
    invoke-direct {p0, p4, p5, p1, p7}, Lcom/microsoft/cll/android/PartA;->setFlags(Lcom/microsoft/cll/android/Cll$EventPersistence;Lcom/microsoft/cll/android/Cll$EventLatency;Lcom/microsoft/telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/telemetry/cs2/Envelope;->setFlags(J)V

    .line 143
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->iKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setIKey(Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->userExt:Lcom/microsoft/telemetry/extensions/user;

    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/user;->getLocalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setUserId(Ljava/lang/String;)V

    .line 145
    iget-object v1, p0, Lcom/microsoft/cll/android/PartA;->deviceExt:Lcom/microsoft/telemetry/extensions/device;

    invoke-virtual {v1}, Lcom/microsoft/telemetry/extensions/device;->getLocalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/telemetry/cs2/Envelope;->setDeviceId(Ljava/lang/String;)V

    .line 147
    return-object v0
.end method

.method protected abstract setAppInfo()V
.end method

.method setAppUserId(Ljava/lang/String;)V
    .locals 5
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 151
    if-nez p1, :cond_0

    .line 152
    iget-object v2, p0, Lcom/microsoft/cll/android/PartA;->appExt:Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v2, v3}, Lcom/microsoft/telemetry/extensions/app;->setUserId(Ljava/lang/String;)V

    .line 168
    :goto_0
    return-void

    .line 157
    :cond_0
    const-string v2, "^((c:)|(i:)|(w:)).*"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 158
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 159
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-nez v2, :cond_1

    .line 161
    iget-object v2, p0, Lcom/microsoft/cll/android/PartA;->appExt:Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v2, v3}, Lcom/microsoft/telemetry/extensions/app;->setUserId(Ljava/lang/String;)V

    .line 162
    iget-object v2, p0, Lcom/microsoft/cll/android/PartA;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v3, "PartA"

    const-string v4, "The userId supplied does not match the required format which requires the appId to start with \'c:\', \'i:\', or \'w:\'."

    invoke-interface {v2, v3, v4}, Lcom/microsoft/cll/android/ILogger;->warn(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_1
    iget-object v2, p0, Lcom/microsoft/cll/android/PartA;->appExt:Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v2, p1}, Lcom/microsoft/telemetry/extensions/app;->setUserId(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract setDeviceInfo()V
.end method

.method protected setExpId(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 218
    iget-object v0, p0, Lcom/microsoft/cll/android/PartA;->appExt:Lcom/microsoft/telemetry/extensions/app;

    invoke-virtual {v0, p1}, Lcom/microsoft/telemetry/extensions/app;->setExpId(Ljava/lang/String;)V

    .line 219
    return-void
.end method

.method protected abstract setOs()V
.end method

.method protected abstract setUserId()V
.end method

.method useLegacyCS(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 179
    iput-boolean p1, p0, Lcom/microsoft/cll/android/PartA;->useLegacyCS:Z

    .line 180
    return-void
.end method
