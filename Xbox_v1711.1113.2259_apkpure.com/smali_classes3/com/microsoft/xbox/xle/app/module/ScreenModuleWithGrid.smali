.class public abstract Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithGrid;
.super Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
.source "ScreenModuleWithGrid.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    return-void
.end method


# virtual methods
.method public abstract getGridView()Lcom/microsoft/xbox/toolkit/ui/XLEGridView;
.end method

.method public abstract getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.end method

.method protected restorePosition()V
    .locals 3

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithGrid;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    .line 25
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithGrid;->getGridView()Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithGrid;->getGridView()Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setSelection(I)V

    .line 30
    :cond_0
    return-void
.end method

.method public abstract setViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
.end method

.method public abstract updateView()V
.end method
