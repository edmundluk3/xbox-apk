.class public Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "SearchScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private autoSuggestTask:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

.field private searchTag:Ljava/lang/String;

.field private storeService:Lcom/microsoft/xbox/service/store/IStoreService;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private suggestedTerms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 38
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    .line 45
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 46
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;)Lcom/microsoft/xbox/service/store/IStoreService;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->onLoadAutoSuggestionCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/util/List;)V

    return-void
.end method

.method private onLoadAutoSuggestionCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/util/List;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 188
    .local p2, "suggests":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 190
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 191
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 197
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->updateAdapter()V

    .line 198
    return-void

    .line 193
    :pswitch_0
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->suggestedTerms:Ljava/util/List;

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public confirm()V
    .locals 0

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->goBack()V

    .line 50
    return-void
.end method

.method public getSearchTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->searchTag:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggested()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->suggestedTerms:Ljava/util/List;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 96
    return-void
.end method

.method public loadAutoSuggestAsync(Ljava/lang/String;)V
    .locals 1
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    .line 110
    if-nez p1, :cond_0

    .line 120
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->autoSuggestTask:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->autoSuggestTask:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->cancel()V

    .line 118
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->autoSuggestTask:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->autoSuggestTask:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->execute()V

    goto :goto_0
.end method

.method public navigateToSearchResultDetails(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;)V
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;

    .prologue
    .line 123
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromStoreAutoSuggestItem(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 124
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v1

    .line 127
    .local v1, "mediaType":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 128
    .local v2, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 129
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v3

    const-string v4, "Search"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;->putPurchaseOriginatingSource(Ljava/lang/String;)V

    .line 131
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$model$edsv2$EDSV3MediaType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 149
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 151
    :goto_0
    return-void

    .line 135
    :pswitch_0
    new-instance v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v3, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    invoke-virtual {p0, v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 141
    :pswitch_1
    new-instance v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;

    invoke-direct {v3, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    invoke-virtual {p0, v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 145
    :pswitch_2
    new-instance v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    invoke-direct {v3, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    invoke-virtual {p0, v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public navigateToSearchResults(Ljava/lang/String;)V
    .locals 3
    .param p1, "searchTag"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->searchTag:Ljava/lang/String;

    .line 155
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/SearchHelper;->checkValidSearchTag(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    :goto_0
    return-void

    .line 159
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->searchTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSearchTag(Ljava/lang/String;)V

    .line 161
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;)V

    .line 184
    .local v0, "postRunnable":Ljava/lang/Runnable;
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 55
    return-void
.end method

.method public onSearchBarClear()V
    .locals 1

    .prologue
    .line 99
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->searchTag:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->autoSuggestTask:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->autoSuggestTask:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->cancel()V

    .line 104
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->suggestedTerms:Ljava/util/List;

    .line 105
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 106
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->updateAdapter()V

    .line 107
    return-void
.end method

.method public onStartOverride()V
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSearchTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->searchTag:Ljava/lang/String;

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->searchTag:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 85
    return-void

    .line 84
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method public onStopOverride()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->autoSuggestTask:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->autoSuggestTask:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel$LoadAutoSuggestTask;->cancel()V

    .line 92
    :cond_0
    return-void
.end method

.method public setSearchTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "searchTag"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->searchTag:Ljava/lang/String;

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 72
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 73
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->updateAdapter()V

    .line 75
    :cond_0
    return-void
.end method
