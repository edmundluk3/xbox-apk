.class public Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;
.source "ActivityGalleryActivityViewModel.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;-><init>()V

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getActivityParentMediaItemData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->parentMediaItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getActivityGalleryAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 46
    return-void
.end method


# virtual methods
.method public getSlideShows()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getSlideShow()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public navigateToImageViewer(I)V
    .locals 5
    .param p1, "startIndex"    # I

    .prologue
    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .local v1, "images":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->getSlideShows()Ljava/util/ArrayList;

    move-result-object v2

    .line 90
    .local v2, "slideshow":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 91
    :cond_0
    const-string v3, "ImageGalleryScreen"

    const-string v4, "no slide show but clickable"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :goto_0
    return-void

    .line 94
    :cond_1
    move v0, p1

    .local v0, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 95
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 97
    :cond_2
    const/4 v0, 0x0

    :goto_2
    if-ge v0, p1, :cond_3

    .line 98
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 101
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedImages(Ljava/util/List;)V

    .line 102
    const-class v3, Lcom/microsoft/xbox/xle/app/activity/ImageViewerScreen;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->NavigateTo(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method protected onLoadDetailsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->isLoading:Z

    .line 60
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 83
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->updateAdapter()V

    .line 84
    return-void

    .line 64
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getSlideShow()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->getActivityData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->activityData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 67
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 69
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 77
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 78
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getActivityGalleryAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 55
    return-void
.end method
