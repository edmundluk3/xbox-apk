.class final synthetic Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$3;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# static fields
.field private static final instance:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$3;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$3;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$3;->instance:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$3;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/Function;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$3;->instance:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$3;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method
