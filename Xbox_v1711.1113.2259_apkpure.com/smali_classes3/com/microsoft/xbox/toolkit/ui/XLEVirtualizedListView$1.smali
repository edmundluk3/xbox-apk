.class Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$1;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "XLEVirtualizedListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->notifyDataSetChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;
    .param p2, "executorService"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method


# virtual methods
.method protected doInBackground()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$000(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;

    move-result-object v0

    if-nez v0, :cond_0

    .line 95
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$000(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;)Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$VirtualListLoader;->getSize()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$1;->doInBackground()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$1;->this$0:Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;->access$102(Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView;I)I

    .line 104
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 86
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEVirtualizedListView$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method
