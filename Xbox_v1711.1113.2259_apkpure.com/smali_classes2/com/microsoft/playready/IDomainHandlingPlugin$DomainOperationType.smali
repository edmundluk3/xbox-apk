.class public final enum Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;
.super Ljava/lang/Enum;
.source "IDomainHandlingPlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/IDomainHandlingPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DomainOperationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

.field public static final enum joinDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

.field public static final enum leaveDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    const-string v1, "joinDomain"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->joinDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    .line 38
    new-instance v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    const-string v1, "leaveDomain"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->leaveDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    sget-object v1, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->joinDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->leaveDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->ENUM$VALUES:[Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->ENUM$VALUES:[Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    array-length v1, v0

    new-array v2, v1, [Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
