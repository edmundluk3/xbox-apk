.class public Lcom/microsoft/playready/FragmentIterator$FragmentInfo;
.super Ljava/lang/Object;
.source "FragmentIterator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/FragmentIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FragmentInfo"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$java$util$concurrent$TimeUnit:[I


# instance fields
.field private final mDurationUs:J

.field private final mStartTimeUs:J

.field private final mUrl:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$java$util$concurrent$TimeUnit()[I
    .locals 3

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->$SWITCH_TABLE$java$util$concurrent$TimeUnit:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/concurrent/TimeUnit;->values()[Ljava/util/concurrent/TimeUnit;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->$SWITCH_TABLE$java$util$concurrent$TimeUnit:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(JJLjava/lang/String;)V
    .locals 3
    .param p1, "startTimeInMs"    # J
    .param p3, "durationInMs"    # J
    .param p5, "url"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->ConvertTime(JLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mStartTimeUs:J

    .line 27
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, p3, p4, v0, v1}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->ConvertTime(JLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mDurationUs:J

    .line 28
    iput-object p5, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mUrl:Ljava/lang/String;

    .line 29
    return-void
.end method

.method constructor <init>(JJLjava/util/concurrent/TimeUnit;Ljava/lang/String;)V
    .locals 3
    .param p1, "startTime"    # J
    .param p3, "duration"    # J
    .param p5, "timeScale"    # Ljava/util/concurrent/TimeUnit;
    .param p6, "url"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, p1, p2, p5, v0}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->ConvertTime(JLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mStartTimeUs:J

    .line 34
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, p3, p4, p5, v0}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->ConvertTime(JLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mDurationUs:J

    .line 35
    iput-object p6, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mUrl:Ljava/lang/String;

    .line 36
    return-void
.end method

.method private ConvertTime(JLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/TimeUnit;)J
    .locals 11
    .param p1, "time"    # J
    .param p3, "timeScaleFrom"    # Ljava/util/concurrent/TimeUnit;
    .param p4, "timeScaleTo"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 65
    const-wide/16 v6, 0x1

    .line 66
    .local v6, "multiplier":J
    const-wide/16 v0, 0x1

    .line 69
    .local v0, "divider":J
    invoke-static {}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->$SWITCH_TABLE$java$util$concurrent$TimeUnit()[I

    move-result-object v8

    invoke-virtual {p3}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 84
    :pswitch_0
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Invalid TimeUnit parameter"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 72
    :pswitch_1
    const-wide/16 v6, 0x1

    .line 87
    :goto_0
    invoke-static {}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->$SWITCH_TABLE$java$util$concurrent$TimeUnit()[I

    move-result-object v8

    invoke-virtual {p4}, Ljava/util/concurrent/TimeUnit;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    .line 102
    :pswitch_2
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Invalid TimeUnit parameter"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 76
    :pswitch_3
    const-wide/16 v6, 0x3e8

    .line 77
    goto :goto_0

    .line 80
    :pswitch_4
    const-wide/32 v6, 0xf4240

    .line 81
    goto :goto_0

    .line 90
    :pswitch_5
    const-wide/16 v0, 0x1

    .line 106
    :goto_1
    const-wide v8, 0x7fffffffffffffffL

    div-long v4, v8, v6

    .line 108
    .local v4, "multiplicandMax":J
    cmp-long v8, p1, v4

    if-lez v8, :cond_1

    .line 110
    div-long v2, p1, v0

    .line 112
    .local v2, "multiplicand":J
    cmp-long v8, v2, v4

    if-lez v8, :cond_0

    .line 114
    new-instance v8, Ljava/lang/RuntimeException;

    const-string v9, "Integer Overflow"

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 94
    .end local v2    # "multiplicand":J
    .end local v4    # "multiplicandMax":J
    :pswitch_6
    const-wide/16 v0, 0x3e8

    .line 95
    goto :goto_1

    .line 98
    :pswitch_7
    const-wide/32 v0, 0xf4240

    .line 99
    goto :goto_1

    .line 117
    .restart local v2    # "multiplicand":J
    .restart local v4    # "multiplicandMax":J
    :cond_0
    mul-long v8, v2, v6

    .line 121
    .end local v2    # "multiplicand":J
    :goto_2
    return-wide v8

    :cond_1
    mul-long v8, p1, v6

    div-long/2addr v8, v0

    goto :goto_2

    .line 69
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 87
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_2
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic access$1(Lcom/microsoft/playready/FragmentIterator$FragmentInfo;)J
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mStartTimeUs:J

    return-wide v0
.end method


# virtual methods
.method public getDuration(Ljava/util/concurrent/TimeUnit;)J
    .locals 2
    .param p1, "timeScale"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mDurationUs:J

    return-wide v0
.end method

.method public getDurationInMs()J
    .locals 4

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mDurationUs:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getStartTime(Ljava/util/concurrent/TimeUnit;)J
    .locals 3
    .param p1, "timeScale"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mStartTimeUs:J

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->ConvertTime(JLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getStartTimeInMs()J
    .locals 4

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mStartTimeUs:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->mUrl:Ljava/lang/String;

    return-object v0
.end method
