.class final synthetic Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$3;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

.field private final arg$2:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$3;->arg$1:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$3;->arg$2:Ljava/lang/String;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$3;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$3;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$3;->arg$1:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository$$Lambda$3;->arg$2:Ljava/lang/String;

    check-cast p1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->lambda$ensureSinglePointOfPresence$3(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
