.class Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGameProgress360CompareAchievementRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/TitleModel;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;

.field private titleId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "titleModel"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "titleId"    # Ljava/lang/String;

    .prologue
    .line 1114
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1115
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 1116
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->xuid:Ljava/lang/String;

    .line 1117
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->titleId:Ljava/lang/String;

    .line 1118
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1122
    const/4 v0, 0x0

    .line 1123
    .local v0, "continuationToken":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1125
    .local v1, "continuationTokenUnearned":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->xuid:Ljava/lang/String;

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->titleId:Ljava/lang/String;

    .line 1126
    invoke-interface {v5, v6, v7, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgress360EarnedAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    move-result-object v3

    .line 1127
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->xuid:Ljava/lang/String;

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->titleId:Ljava/lang/String;

    invoke-interface {v5, v6, v7, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgress360AchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    move-result-object v4

    .line 1129
    .local v4, "unearnedResult":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    if-eqz v3, :cond_0

    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    if-eqz v5, :cond_0

    .line 1130
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    .line 1131
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1132
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->xuid:Ljava/lang/String;

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->titleId:Ljava/lang/String;

    .line 1133
    invoke-interface {v5, v6, v7, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgress360EarnedAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    move-result-object v2

    .line 1135
    .local v2, "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    if-nez v2, :cond_3

    .line 1151
    .end local v2    # "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    :cond_0
    if-eqz v4, :cond_1

    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    if-eqz v5, :cond_1

    .line 1152
    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    iget-object v1, v5, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    .line 1153
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1154
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->xuid:Ljava/lang/String;

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->titleId:Ljava/lang/String;

    .line 1155
    invoke-interface {v5, v6, v7, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgress360AchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    move-result-object v2

    .line 1156
    .restart local v2    # "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    if-nez v2, :cond_6

    .line 1172
    .end local v2    # "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    :cond_1
    if-eqz v4, :cond_2

    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    .line 1173
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    iget-object v6, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1176
    :cond_2
    return-object v3

    .line 1139
    .restart local v2    # "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    :cond_3
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    if-eqz v5, :cond_4

    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    if-eqz v5, :cond_4

    .line 1140
    iget-object v5, v3, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1143
    :cond_4
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    if-eqz v5, :cond_5

    .line 1144
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    goto :goto_0

    .line 1146
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 1160
    :cond_6
    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    if-eqz v5, :cond_7

    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    if-eqz v5, :cond_7

    .line 1161
    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->achievements:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1164
    :cond_7
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    if-eqz v5, :cond_8

    .line 1165
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;

    iget-object v1, v5, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    goto :goto_1

    .line 1167
    :cond_8
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1108
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1190
    const-wide/16 v0, 0xbe2

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1185
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgress360CompareAchievementRunner;->xuid:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->access$600(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    .line 1186
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1181
    return-void
.end method
