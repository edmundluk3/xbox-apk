.class Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SortedIndexComparator;
.super Ljava/lang/Object;
.source "PeopleScreenViewModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SortedIndexComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
    .locals 0

    .prologue
    .line 597
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SortedIndexComparator;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;

    .prologue
    .line 597
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SortedIndexComparator;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Integer;Ljava/lang/Integer;)I
    .locals 2
    .param p1, "lhs"    # Ljava/lang/Integer;
    .param p2, "rhs"    # Ljava/lang/Integer;

    .prologue
    .line 601
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 597
    check-cast p1, Ljava/lang/Integer;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$SortedIndexComparator;->compare(Ljava/lang/Integer;Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method
