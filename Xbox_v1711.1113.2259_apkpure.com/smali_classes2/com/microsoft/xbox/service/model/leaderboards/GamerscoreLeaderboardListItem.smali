.class public final Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
.super Ljava/lang/Object;
.source "GamerscoreLeaderboardListItem.java"


# instance fields
.field private volatile transient hashCode:I

.field public final isSelf:Z

.field public final monthlyGamerscore:J

.field public final rank:J

.field private final userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;JJZ)V
    .locals 2
    .param p1, "userSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "rank"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p4, "monthlyGamerscore"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p6, "isSelf"    # Z

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 32
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 33
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p4, p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 36
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->rank:J

    .line 37
    iput-wide p4, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    .line 38
    iput-boolean p6, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->isSelf:Z

    .line 39
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    if-ne p1, p0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v1

    .line 83
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 84
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 86
    check-cast v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    .line 87
    .local v0, "other":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->getXuidLong()J

    move-result-wide v4

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->getXuidLong()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->rank:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->rank:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getGamerpic()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getGamertag()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->gamertag:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPrimaryUserColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;->primaryColor:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v0

    .line 67
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    goto :goto_0
.end method

.method public getSecondaryUserColor()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;->secondaryColor:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v0

    .line 76
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_SECONDARY_COLOR:I

    goto :goto_0
.end method

.method public getUser()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public getXuid()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 95
    iget v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 96
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode:I

    .line 97
    iget v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->userSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->getXuidLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode:I

    .line 98
    iget v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->rank:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode:I

    .line 99
    iget v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->monthlyGamerscore:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode:I

    .line 102
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode:I

    return v0
.end method
