.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"

# interfaces
.implements Lorg/webrtc/DataChannel$Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->onDataChannel(Lorg/webrtc/DataChannel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

.field final synthetic val$dc:Lorg/webrtc/DataChannel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;Lorg/webrtc/DataChannel;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    .prologue
    .line 897
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->val$dc:Lorg/webrtc/DataChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBufferedAmountChange(J)V
    .locals 4
    .param p1, "previousAmount"    # J

    .prologue
    .line 899
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Data channel buffered amount changed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->val$dc:Lorg/webrtc/DataChannel;

    invoke-virtual {v3}, Lorg/webrtc/DataChannel;->label()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->val$dc:Lorg/webrtc/DataChannel;

    invoke-virtual {v3}, Lorg/webrtc/DataChannel;->state()Lorg/webrtc/DataChannel$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    return-void
.end method

.method public onMessage(Lorg/webrtc/DataChannel$Buffer;)V
    .locals 7
    .param p1, "buffer"    # Lorg/webrtc/DataChannel$Buffer;

    .prologue
    .line 907
    iget-boolean v3, p1, Lorg/webrtc/DataChannel$Buffer;->binary:Z

    if-eqz v3, :cond_0

    .line 908
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v3, v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v3

    const-string v4, "PCRTCClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received binary msg over "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->val$dc:Lorg/webrtc/DataChannel;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    :goto_0
    return-void

    .line 911
    :cond_0
    iget-object v1, p1, Lorg/webrtc/DataChannel$Buffer;->data:Ljava/nio/ByteBuffer;

    .line 912
    .local v1, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    new-array v0, v3, [B

    .line 913
    .local v0, "bytes":[B
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 914
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    .line 915
    .local v2, "strData":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v3, v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v3

    const-string v4, "PCRTCClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got msg: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " over "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->val$dc:Lorg/webrtc/DataChannel;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStateChange()V
    .locals 4

    .prologue
    .line 903
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->this$1:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;

    iget-object v0, v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Data channel state changed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->val$dc:Lorg/webrtc/DataChannel;

    invoke-virtual {v3}, Lorg/webrtc/DataChannel;->label()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;->val$dc:Lorg/webrtc/DataChannel;

    invoke-virtual {v3}, Lorg/webrtc/DataChannel;->state()Lorg/webrtc/DataChannel$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    return-void
.end method
