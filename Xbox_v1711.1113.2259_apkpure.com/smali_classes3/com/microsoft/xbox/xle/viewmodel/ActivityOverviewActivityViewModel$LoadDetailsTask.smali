.class public Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityOverviewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadDetailsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;
    .param p2, "caller"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 196
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->caller:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    .line 197
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->detailModel:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->caller:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->onLoadDetailsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 207
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->caller:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->onLoadDetailsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 227
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 191
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel$LoadDetailsTask;->caller:Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;->isLoading:Z

    .line 222
    return-void
.end method
