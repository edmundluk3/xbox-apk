.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "EDSV2SearchDataModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$EDSV2SearchDataModelHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_MAX_RESULT_COUNT:I = 0xa

.field private static final MAX_SEARCH_ENCODED_TEXT_LENGTH:I = 0xfa


# instance fields
.field private currentSearchTag:Ljava/lang/String;

.field private getSearchResultMoreRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;

.field private getSearchResultRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;

.field private hasMoreToLoad:Z

.field private isNeedResetTypeAllFiterResultCount:Z

.field private pageCount:I

.field private searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

.field private selectedFilterType:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field private typeAllFiterResultCount:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 37
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->isLoading:Z

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->hasMoreToLoad:Z

    .line 39
    iput v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->pageCount:I

    .line 40
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$1;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->currentSearchTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->selectedFilterType:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->onGetSearchDatasFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->onGetSearchDataMoreFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private cancel()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->getSearchResultRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->getSearchResultRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;->cancel()V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->getSearchResultMoreRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->getSearchResultMoreRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->cancel()V

    .line 69
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->hasMoreToLoad:Z

    .line 70
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;
    .locals 2

    .prologue
    .line 52
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 53
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$EDSV2SearchDataModelHolder;->access$100()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    move-result-object v0

    return-object v0

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onGetSearchDataMoreFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 163
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 165
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 166
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 167
    .local v0, "searchResultMoreData":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 168
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 169
    iget v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->pageCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->pageCount:I

    .line 170
    const-string v1, "EdsV2SearchDataModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loaded page "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->pageCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getContinuationToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->setContinuationToken(Ljava/lang/String;)V

    .line 176
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->lastRefreshTime:Ljava/util/Date;

    .line 179
    .end local v0    # "searchResultMoreData":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_0
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->isLoading:Z

    .line 180
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v3, v4, v2}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v1, v3, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 181
    return-void

    :cond_1
    move v1, v3

    .line 163
    goto :goto_0

    .line 172
    .restart local v0    # "searchResultMoreData":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    :cond_2
    const-string v1, "EdsV2SearchDataModel"

    const-string v4, "the service returns empty data, there are no more to load"

    invoke-static {v1, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->hasMoreToLoad:Z

    goto :goto_1
.end method

.method private onGetSearchDatasFeedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 143
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 144
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 145
    iput v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->pageCount:I

    .line 147
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->isNeedResetTypeAllFiterResultCount:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->selectedFilterType:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_ALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    if-ne v0, v3, :cond_1

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->typeAllFiterResultCount:Ljava/util/ArrayList;

    .line 151
    :cond_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->lastRefreshTime:Ljava/util/Date;

    .line 153
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_3

    .line 154
    :cond_2
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->hasMoreToLoad:Z

    .line 158
    :cond_3
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->isLoading:Z

    .line 159
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v2, v3, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    invoke-direct {v0, v2, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 160
    return-void

    :cond_4
    move v0, v2

    .line 141
    goto :goto_0

    .line 148
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterResultCount()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1
.end method

.method public static reset()V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->getInstance()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->clearObservers()V

    .line 58
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$EDSV2SearchDataModelHolder;->access$200()V

    .line 59
    return-void
.end method


# virtual methods
.method public doSearch(ZLjava/lang/String;Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)V
    .locals 5
    .param p1, "forceRefresh"    # Z
    .param p2, "searchTag"    # Ljava/lang/String;
    .param p3, "selectedFilterType"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 101
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 102
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 104
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->currentSearchTag:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->selectedFilterType:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    if-ne v0, p3, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-eqz v0, :cond_2

    .line 105
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v2, v3, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v0, v2, p0, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 124
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 101
    goto :goto_0

    :cond_1
    move v0, v2

    .line 102
    goto :goto_1

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->currentSearchTag:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 110
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->isNeedResetTypeAllFiterResultCount:Z

    .line 115
    :goto_3
    iput-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    .line 116
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->currentSearchTag:Ljava/lang/String;

    .line 117
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->selectedFilterType:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 119
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->cancel()V

    .line 121
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->isLoading:Z

    .line 122
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;

    invoke-direct {v0, p0, p0, p2, p3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Ljava/lang/String;Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->getSearchResultRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;

    .line 123
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->SearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->getSearchResultRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultRunner;

    invoke-virtual {p0, v1, v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    goto :goto_2

    .line 112
    :cond_3
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->isNeedResetTypeAllFiterResultCount:Z

    goto :goto_3
.end method

.method public getFilterCount()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->typeAllFiterResultCount:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHasMoreToLoad()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->hasMoreToLoad:Z

    return v0
.end method

.method public getImpressionGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getImpressionGuid()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->pageCount:I

    return v0
.end method

.method public getSearchFilterCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I
    .locals 1
    .param p1, "filter"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I

    move-result v0

    goto :goto_0
.end method

.method public getSearchResult()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    .line 74
    :cond_0
    const/4 v0, 0x0

    .line 76
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public loadMoreSearchResults()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 127
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 129
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->hasMoreToLoad:Z

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->searchResultData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getContinuationToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 132
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v2, v3, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v1, 0x0

    invoke-direct {v0, v2, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 138
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 127
    goto :goto_0

    .line 136
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;

    invoke-direct {v0, p0, p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->getSearchResultMoreRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;

    .line 137
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MoreSearchSummaryResult:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->getSearchResultMoreRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;

    invoke-virtual {p0, v1, v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    goto :goto_1
.end method
