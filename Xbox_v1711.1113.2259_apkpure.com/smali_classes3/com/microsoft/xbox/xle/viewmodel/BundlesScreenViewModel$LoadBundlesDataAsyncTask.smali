.class Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "BundlesScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadBundlesDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;)V
    .locals 0

    .prologue
    .line 107
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$1;

    .prologue
    .line 107
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 111
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefreshBundles()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 141
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->forceLoad:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 142
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsPartOfAnyBundle()Z

    move-result v1

    if-nez v1, :cond_1

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->setShouldHideScreen(Z)V

    .line 149
    .end local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_0
    :goto_0
    return-object v0

    .line 146
    .restart local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->forceLoad:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadBundles(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 107
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 134
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 107
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 117
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->onLoadBundlesDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 119
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 129
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->onLoadBundlesDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 130
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 107
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 123
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>.LoadBundlesDataAsyncTask;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;Z)Z

    .line 125
    return-void
.end method
