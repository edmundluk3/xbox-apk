.class Lcom/microsoft/xbox/xle/epg/TvStreamer$15;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupMediaPlayer(Lcom/microsoft/playready/PRMediaPlayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 672
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$15;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v0, 0x0

    .line 676
    const-string v1, "TvStreamer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onInfo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    const/4 v1, 0x3

    if-ne p2, v1, :cond_0

    .line 679
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$15;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setIsLoading(Z)V

    .line 681
    const/4 v0, 0x1

    .line 683
    :cond_0
    return v0
.end method
