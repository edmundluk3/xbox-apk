.class public Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "GameAchievementComparisonScreenAdapter.java"


# instance fields
.field private achievementMeLayout:Landroid/view/View;

.field private achievementYouLayout:Landroid/view/View;

.field private gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

.field private gamerscoreMeLayout:Landroid/view/View;

.field private gamerscoreYouLayout:Landroid/view/View;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

.field private meAchievementScore:Landroid/widget/TextView;

.field private meGamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private meGamerScore:Landroid/widget/TextView;

.field private meGamerTag:Landroid/widget/TextView;

.field private meGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private meRareAchievementScore:Landroid/widget/TextView;

.field private rareAchievementMeLayout:Landroid/view/View;

.field private rareAchievementYouLayout:Landroid/view/View;

.field private stopComparing:Landroid/widget/RelativeLayout;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private title:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private youAchievementScore:Landroid/widget/TextView;

.field private youGamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private youGamerScore:Landroid/widget/TextView;

.field private youGamerTag:Landroid/widget/TextView;

.field private youGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private youRareAchievementScore:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V
    .locals 5
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    const/16 v4, 0x8

    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 56
    const v0, 0x7f0e05fa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->screenBody:Landroid/view/View;

    .line 57
    const v0, 0x7f0e05fe

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .line 60
    const v0, 0x7f0e05fd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 61
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f03010d

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 64
    const v0, 0x7f0e05fb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 65
    const v0, 0x7f0e03f9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerScore:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0e0404

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerScore:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0e03fa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 69
    const v0, 0x7f0e0405

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 72
    const v0, 0x7f0e03fd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meAchievementScore:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0e0408

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youAchievementScore:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0e0400

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meRareAchievementScore:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0e040a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youRareAchievementScore:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f0e03fe

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->rareAchievementMeLayout:Landroid/view/View;

    .line 77
    const v0, 0x7f0e0409

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->rareAchievementYouLayout:Landroid/view/View;

    .line 79
    const v0, 0x7f0e03f7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 80
    const v0, 0x7f0e0402

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 81
    const v0, 0x7f0e0401

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerTag:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0e03f6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerTag:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0e03f8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gamerscoreMeLayout:Landroid/view/View;

    .line 84
    const v0, 0x7f0e0403

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gamerscoreYouLayout:Landroid/view/View;

    .line 85
    const v0, 0x7f0e03fc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->achievementMeLayout:Landroid/view/View;

    .line 86
    const v0, 0x7f0e0407

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->achievementYouLayout:Landroid/view/View;

    .line 87
    const v0, 0x7f0e05fc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->stopComparing:Landroid/widget/RelativeLayout;

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->stopComparing:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 121
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 128
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 129
    return-void
.end method

.method protected updateViewOverride()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const v6, 0x7f020125

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->updateLoadingIndicator(Z)V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getTitleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerTag:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouGamerTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getIsGame()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerScore:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMeCurrentGamerscore()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerScore:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouCurrentGamerscore()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meAchievementScore:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMeCurrentAchievementsEarned()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youAchievementScore:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouCurrentAchievementsEarned()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meRareAchievementScore:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMeCurrentRareAchievementScore()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youRareAchievementScore:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouCurrentRareAchievementScore()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMaxGamerscore()I

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerScore:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMeCurrentGamerscore()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMaxGamerscore()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerScore:Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070d71

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .line 155
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMeCurrentGamerscore()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .line 156
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMaxGamerscore()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    .line 154
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerScore:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouCurrentGamerscore()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMaxGamerscore()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerScore:Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070d71

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .line 159
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouCurrentGamerscore()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .line 160
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMaxGamerscore()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    .line 158
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateContentDescriptionIfNotNull(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gamerscoreYouLayout:Landroid/view/View;

    invoke-static {v0, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gamerscoreMeLayout:Landroid/view/View;

    invoke-static {v0, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->achievementYouLayout:Landroid/view/View;

    invoke-static {v0, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->achievementMeLayout:Landroid/view/View;

    invoke-static {v0, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->rareAchievementMeLayout:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->rareAchievementYouLayout:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 182
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->youGamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouProfilePicture()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v6}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v0, :cond_2

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->meGamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getMeProfilePicture()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v6}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 190
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getCompareAchievements()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->items:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getCompareAchievements()Ljava/util/ArrayList;

    move-result-object v1

    if-eq v0, v1, :cond_5

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gacViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getCompareAchievements()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->items:Ljava/util/List;

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->clear()V

    .line 198
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->items:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->addAll(Ljava/util/Collection;)V

    .line 200
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->restoreListPosition()V

    .line 203
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 206
    :cond_5
    return-void

    .line 174
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gamerscoreYouLayout:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->gamerscoreMeLayout:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->achievementYouLayout:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 177
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->achievementMeLayout:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->rareAchievementMeLayout:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->rareAchievementYouLayout:Landroid/view/View;

    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0
.end method
