.class public abstract Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
.super Ljava/lang/Object;
.source "OOBESessionDataType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OOBESessionResponse"
.end annotation


# static fields
.field public static final CONSOLE_UPDATE_FINAL_STEP:I = 0x4

.field public static final CONSOLE_UPDATE_MAXIMUM_PROGRESS:I = 0x64


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    .locals 12
    .param p0, "sessionId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "consoleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "consoleUpdateProgress"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "consoleUpdateStep"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "consoleSettings"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/RestrictTo;
        value = {
            .enum Landroid/support/annotation/RestrictTo$Scope;->TESTS:Landroid/support/annotation/RestrictTo$Scope;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse;

    const/4 v3, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v7, p2

    move-object v8, p3

    move-object/from16 v10, p4

    invoke-direct/range {v0 .. v11}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract consoleId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleId"
    .end annotation
.end method

.method public abstract consoleOSVersion()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleOSVersion"
    .end annotation
.end method

.method public abstract consoleSettings()Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleSettings"
    .end annotation
.end method

.method public abstract consoleUpdateProgress()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleUpdateProgress"
    .end annotation
.end method

.method public abstract consoleUpdateStep()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleUpdateStep"
    .end annotation
.end method

.method public abstract encryptionKey()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "EncryptionKey"
    .end annotation
.end method

.method public abstract sessionExpiration()Ljava/util/Date;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SessionExpiration"
    .end annotation
.end method

.method public abstract sessionId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SessionId"
    .end annotation
.end method

.method public abstract sessionState()I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SessionState"
    .end annotation
.end method

.method public abstract transferToken()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TransferToken"
    .end annotation
.end method

.method public abstract userName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "UserName"
    .end annotation
.end method
