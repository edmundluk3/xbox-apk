.class public Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubCardListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ClubCardViewHolder"
.end annotation


# instance fields
.field clubBanner:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02b8
    .end annotation
.end field

.field clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02ba
    .end annotation
.end field

.field clubPic:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02b9
    .end annotation
.end field

.field clubPicFrame:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02b7
    .end annotation
.end field

.field clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02be
    .end annotation
.end field

.field clubTypeIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02bc
    .end annotation
.end field

.field customGlyph:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02bd
    .end annotation
.end field

.field description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02c3
    .end annotation
.end field

.field hereToday:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02c2
    .end annotation
.end field

.field members:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02c1
    .end annotation
.end field

.field final rootView:Landroid/view/View;

.field private tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

.field tagsList:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e02c4
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .line 87
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 89
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->rootView:Landroid/view/View;

    .line 90
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagsList:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setMaxRows(I)V

    .line 92
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;)V
    .locals 9
    .param p1, "cardModel"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    const v6, 0x7f0201fa

    .line 95
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 97
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubPicFrame:Landroid/view/View;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->backgroundColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 98
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->rootView:Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setClickable(Z)V

    .line 99
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->rootView:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubPic:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->displayImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6, v6}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 103
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->bannerImageUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 104
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubBanner:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubBanner:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->bannerImageUrl()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v5, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 110
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->members:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->memberCount()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->presenceCount()J

    move-result-wide v0

    .line 113
    .local v0, "presenceCount":J
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->hereToday:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-wide/16 v4, 0x3e8

    cmp-long v2, v0, v4

    if-gez v2, :cond_1

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->description()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubType:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->glyphImageUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->customGlyph:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 118
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->customGlyph:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->glyphImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6, v6}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 119
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubTypeIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 126
    :goto_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    if-nez v2, :cond_3

    .line 128
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    .line 130
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->access$000()I

    move-result v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->backgroundColor()I

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    .line 131
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->socialTags()Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;)Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;)V

    .line 133
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagsList:Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 134
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->notifyDataSetChanged()V

    .line 141
    :goto_3
    return-void

    .line 107
    .end local v0    # "presenceCount":J
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubBanner:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 113
    .restart local v0    # "presenceCount":J
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v4, 0x7f070602

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 121
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->customGlyph:Landroid/widget/ImageView;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 122
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubTypeIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 123
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->clubTypeIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->getClubTypeIcon()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 136
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->clear()V

    .line 137
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->socialTags()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 138
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->backgroundColor()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->setSelectedColor(I)V

    .line 139
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->notifyDataSetChanged()V

    goto :goto_3
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter$ClubCardViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 146
    return-void
.end method
