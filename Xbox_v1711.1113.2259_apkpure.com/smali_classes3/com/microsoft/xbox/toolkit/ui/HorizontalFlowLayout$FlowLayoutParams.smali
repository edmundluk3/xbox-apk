.class public Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "HorizontalFlowLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FlowLayoutParams"
.end annotation


# instance fields
.field protected centerVertical:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 195
    invoke-direct {p0, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;-><init>(II)V

    .line 196
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 199
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 200
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->setCenterVertical(Z)V

    .line 201
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 204
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 205
    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x1010191

    aput v2, v1, v3

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 206
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->setCenterVertical(Z)V

    .line 207
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 208
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p1, "source"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 211
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 0
    .param p1, "source"    # Landroid/view/ViewGroup$MarginLayoutParams;

    .prologue
    .line 215
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 216
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;)V
    .locals 2
    .param p1, "source"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    .prologue
    .line 219
    iget v0, p1, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->width:I

    iget v1, p1, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->height:I

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;-><init>(II)V

    .line 220
    return-void
.end method


# virtual methods
.method public setCenterVertical(Z)V
    .locals 0
    .param p1, "centerVertical"    # Z

    .prologue
    .line 223
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->centerVertical:Z

    .line 224
    return-void
.end method
