.class public Lcom/appsflyer/InstanceIDListener;
.super Lcom/google/android/gms/iid/InstanceIDListenerService;
.source "InstanceIDListener.java"


# instance fields
.field private _refreshedToken:Ljava/lang/String;

.field private _tokenTimestamp:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/gms/iid/InstanceIDListenerService;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenRefresh()V
    .locals 12

    .prologue
    .line 21
    invoke-super {p0}, Lcom/google/android/gms/iid/InstanceIDListenerService;->onTokenRefresh()V

    .line 22
    const-string v9, "onTokenRefresh called"

    invoke-static {v9}, Lcom/appsflyer/AFLogger;->afLog(Ljava/lang/String;)V

    .line 24
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v9

    const-string v10, "gcmProjectNumber"

    invoke-virtual {v9, v10}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 25
    .local v2, "gcmProjectNumber":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/gms/iid/InstanceID;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/iid/InstanceID;

    move-result-object v3

    .line 27
    .local v3, "instanceID":Lcom/google/android/gms/iid/InstanceID;
    :try_start_0
    const-string v9, "GCM"

    const/4 v10, 0x0

    invoke-virtual {v3, v2, v9, v10}, Lcom/google/android/gms/iid/InstanceID;->getToken(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/appsflyer/InstanceIDListener;->_refreshedToken:Ljava/lang/String;

    .line 29
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/appsflyer/InstanceIDListener;->_tokenTimestamp:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 35
    :goto_0
    iget-object v9, p0, Lcom/appsflyer/InstanceIDListener;->_refreshedToken:Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 36
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "new token="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/appsflyer/InstanceIDListener;->_refreshedToken:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/appsflyer/AFLogger;->afLog(Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v9

    const-string v10, "gcmToken"

    invoke-virtual {v9, v10}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 38
    .local v7, "token":Ljava/lang/String;
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v9

    const-string v10, "gcmInstanceId"

    invoke-virtual {v9, v10}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 39
    .local v4, "instanceId":Ljava/lang/String;
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v9

    const-string v10, "gcmTokenTimestamp"

    invoke-virtual {v9, v10}, Lcom/appsflyer/AppsFlyerProperties;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 41
    .local v8, "tokenTimestamp":Ljava/lang/String;
    new-instance v1, Lcom/appsflyer/GcmToken;

    invoke-direct {v1, v8, v7, v4}, Lcom/appsflyer/GcmToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .local v1, "existingGcmToken":Lcom/appsflyer/GcmToken;
    new-instance v5, Lcom/appsflyer/GcmToken;

    iget-wide v10, p0, Lcom/appsflyer/InstanceIDListener;->_tokenTimestamp:J

    iget-object v9, p0, Lcom/appsflyer/InstanceIDListener;->_refreshedToken:Ljava/lang/String;

    invoke-direct {v5, v10, v11, v9, v4}, Lcom/appsflyer/GcmToken;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    .line 43
    .local v5, "newGcmToken":Lcom/appsflyer/GcmToken;
    invoke-virtual {v1, v5}, Lcom/appsflyer/GcmToken;->update(Lcom/appsflyer/GcmToken;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 44
    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->getInstance()Lcom/appsflyer/AppsFlyerLib;

    move-result-object v9

    invoke-virtual {p0}, Lcom/appsflyer/InstanceIDListener;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Lcom/appsflyer/AppsFlyerLib;->updateServerGcmToken(Lcom/appsflyer/GcmToken;Landroid/content/Context;)V

    .line 47
    .end local v1    # "existingGcmToken":Lcom/appsflyer/GcmToken;
    .end local v4    # "instanceId":Ljava/lang/String;
    .end local v5    # "newGcmToken":Lcom/appsflyer/GcmToken;
    .end local v7    # "token":Ljava/lang/String;
    .end local v8    # "tokenTimestamp":Ljava/lang/String;
    :cond_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Ljava/io/IOException;
    const-string v9, "Could not load registration ID"

    invoke-static {v9}, Lcom/appsflyer/AFLogger;->afLog(Ljava/lang/String;)V

    goto :goto_0

    .line 32
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v6

    .line 33
    .local v6, "t":Ljava/lang/Throwable;
    const-string v9, "Error registering for uninstall feature"

    invoke-static {v9}, Lcom/appsflyer/AFLogger;->afLog(Ljava/lang/String;)V

    goto :goto_0
.end method
