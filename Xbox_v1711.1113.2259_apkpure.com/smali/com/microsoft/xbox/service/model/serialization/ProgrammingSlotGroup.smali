.class public Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlotGroup;
.super Ljava/lang/Object;
.source "ProgrammingSlotGroup.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    name = "SlotGroup"
.end annotation


# instance fields
.field public SequenceId:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "SequenceId"
    .end annotation
.end field

.field public SlotList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;",
            ">;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        entry = "Slot"
        inline = true
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
