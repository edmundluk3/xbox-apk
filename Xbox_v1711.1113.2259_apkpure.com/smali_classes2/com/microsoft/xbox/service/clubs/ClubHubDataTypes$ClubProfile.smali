.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubProfile"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
    .locals 1

    .prologue
    .line 593
    new-instance v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubProfile$Builder;-><init>()V

    return-object v0
.end method

.method public static isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Z
    .locals 1
    .param p0, "profile"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 566
    if-eqz p0, :cond_0

    .line 567
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->associatedTitles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->backgroundImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->primaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->secondaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tertiaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->description()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->preferredLocale()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tags()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isRecommendable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 578
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isSearchable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->leaveEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->transferOwnershipEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 566
    :goto_0
    return v0

    .line 583
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubProfile$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract associatedTitles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end method

.method public abstract backgroundImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract description()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isRecommendable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isSearchable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract leaveEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract preferredLocale()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract primaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract secondaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract tags()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract tertiaryColor()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;
.end method

.method public abstract transferOwnershipEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method
