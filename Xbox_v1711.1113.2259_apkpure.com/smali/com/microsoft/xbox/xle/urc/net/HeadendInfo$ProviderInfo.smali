.class public Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
.super Ljava/lang/Object;
.source "HeadendInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProviderInfo"
.end annotation


# static fields
.field public static final default_title_id:Ljava/lang/String; = "162615AD"


# instance fields
.field public can_stream:Z

.field public filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

.field public headend_id:Ljava/lang/String;

.field public isPreferred:Z

.field public provider_name:Ljava/lang/String;

.field public provider_source:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

.field public title_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;ZLcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;Z)V
    .locals 0
    .param p1, "headend"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "source"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;
    .param p5, "preferred"    # Z
    .param p6, "filter"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;
    .param p7, "stream"    # Z

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->title_id:Ljava/lang/String;

    .line 68
    iput-object p3, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_name:Ljava/lang/String;

    .line 69
    iput-object p4, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_source:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    .line 70
    iput-boolean p5, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->isPreferred:Z

    .line 71
    iput-object p6, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    .line 72
    iput-boolean p7, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->can_stream:Z

    .line 73
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    if-ne p0, p1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 90
    goto :goto_0

    .line 92
    :cond_2
    instance-of v3, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    if-nez v3, :cond_3

    move v1, v2

    .line 93
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 95
    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    .line 96
    .local v0, "other":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 97
    goto :goto_0
.end method

.method public hasSettingDiff(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;)Z
    .locals 3
    .param p1, "other"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    .prologue
    const/4 v0, 0x0

    .line 103
    if-nez p1, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    iget-object v2, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 78
    const/16 v0, 0x1f

    .line 79
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 80
    .local v1, "result":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 81
    return v1

    .line 80
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0
.end method
