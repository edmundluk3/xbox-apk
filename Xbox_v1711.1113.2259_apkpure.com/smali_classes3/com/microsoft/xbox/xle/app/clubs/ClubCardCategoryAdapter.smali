.class public Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubCardCategoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final seeAllClickHandler:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private final viewModelForNavigation:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "viewModelForNavigation"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p3, "seeAllClickHandler":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 34
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 35
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 37
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 38
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->viewModelForNavigation:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 39
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->seeAllClickHandler:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->seeAllClickHandler:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->viewModelForNavigation:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;->bindTo(Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V

    .line 50
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 44
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030069

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter$ClubCardCategoryViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubCardCategoryAdapter;Landroid/view/View;)V

    return-object v0
.end method
