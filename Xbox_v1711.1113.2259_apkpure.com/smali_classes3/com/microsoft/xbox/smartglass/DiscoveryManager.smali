.class public Lcom/microsoft/xbox/smartglass/DiscoveryManager;
.super Lcom/microsoft/xbox/smartglass/Listenable;
.source "DiscoveryManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/Listenable",
        "<",
        "Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final _pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>(J)V
    .locals 5
    .param p1, "pPlatform"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/Listenable;-><init>()V

    .line 16
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->initialize(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 17
    return-void
.end method

.method private native cancel(J)Z
.end method

.method private native discover(J)V
.end method

.method private native forgetDevice(JLjava/lang/String;)V
.end method

.method private native getDevices(J)[Lcom/microsoft/xbox/smartglass/PrimaryDevice;
.end method

.method private native initialize(J)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private onDiscoverCompleted()V
    .locals 3

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;

    .line 87
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;->onDiscoverCompleted()V

    goto :goto_0

    .line 89
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;
    :cond_0
    return-void
.end method

.method private onPresenceUpdated(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/util/Calendar;Ljava/util/Calendar;Z)V
    .locals 15
    .param p1, "pPrimaryDevice"    # J
    .param p3, "hardwareId"    # Ljava/lang/String;
    .param p4, "id"    # Ljava/lang/String;
    .param p5, "friendlyName"    # Ljava/lang/String;
    .param p6, "host"    # Ljava/lang/String;
    .param p7, "service"    # Ljava/lang/String;
    .param p8, "status"    # I
    .param p9, "type"    # I
    .param p10, "flags"    # I
    .param p11, "lastUpdated"    # Ljava/util/Calendar;
    .param p12, "lastConnected"    # Ljava/util/Calendar;
    .param p13, "isRemembered"    # Z

    .prologue
    .line 77
    new-instance v1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;

    move-wide/from16 v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move/from16 v14, p13

    invoke-direct/range {v1 .. v14}, Lcom/microsoft/xbox/smartglass/PrimaryDevice;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/util/Calendar;Ljava/util/Calendar;Z)V

    .line 79
    .local v1, "device":Lcom/microsoft/xbox/smartglass/PrimaryDevice;
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->cloneListeners()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;

    .line 80
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;->onPresenceUpdated(Lcom/microsoft/xbox/smartglass/PrimaryDevice;)V

    goto :goto_0

    .line 82
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;
    :cond_0
    return-void
.end method

.method private native powerOff(JLjava/lang/String;)V
.end method

.method private native powerOn(JLjava/lang/String;)V
.end method

.method private native startContinuousDiscover(J)V
.end method


# virtual methods
.method public cancel()Z
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->cancel(J)Z

    move-result v0

    return v0
.end method

.method public discover()V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->discover(J)V

    .line 22
    return-void
.end method

.method public forgetDevice(Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 39
    if-nez p1, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "deviceId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->forgetDevice(JLjava/lang/String;)V

    .line 44
    return-void
.end method

.method public getDevices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/smartglass/PrimaryDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->getDevices(J)[Lcom/microsoft/xbox/smartglass/PrimaryDevice;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public powerOff(Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 63
    if-nez p1, :cond_0

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "deviceId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->powerOff(JLjava/lang/String;)V

    .line 68
    return-void
.end method

.method public powerOn(Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "deviceId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->powerOn(JLjava/lang/String;)V

    .line 56
    return-void
.end method

.method public startContinuousDiscover()V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->startContinuousDiscover(J)V

    .line 27
    return-void
.end method
