.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Clubs;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Clubs"
.end annotation


# static fields
.field public static final ActivityFeedNavigateClub:Ljava/lang/String; = "Clubs - Activity Feed Navigate to Club"

.field public static final ActivityFeedTab:Ljava/lang/String; = "Clubs - Activity Feed Tab"

.field public static final AddGame:Ljava/lang/String; = "Clubs - Customize Club Add Game"

.field public static final AdminBanMember:Ljava/lang/String; = "Clubs - Admin Ban Member"

.field public static final AdminBanned:Ljava/lang/String; = "Clubs - Admin Banned"

.field public static final AdminDemoteMember:Ljava/lang/String; = "Clubs - Admin Demote Member"

.field public static final AdminFilterMembers:Ljava/lang/String; = "Clubs - Admin Members Filter Members"

.field public static final AdminFilterModerators:Ljava/lang/String; = "Clubs - Admin Members Filter Moderators"

.field public static final AdminIgnoreAll:Ljava/lang/String; = "Clubs - Admin Requests Ignore All"

.field public static final AdminMembersModerators:Ljava/lang/String; = "Clubs - Admin Members and moderators"

.field public static final AdminPromoteMember:Ljava/lang/String; = "Clubs - Admin Promote Member"

.field public static final AdminRemoveMember:Ljava/lang/String; = "Clubs - Admin Remove Member"

.field public static final AdminReportMember:Ljava/lang/String; = "Clubs - Admin Report Member"

.field public static final AdminReports:Ljava/lang/String; = "Clubs - Admin Reports"

.field public static final AdminReportsBanCreator:Ljava/lang/String; = "Clubs - Admin Reports Ban Creator"

.field public static final AdminReportsBanReporter:Ljava/lang/String; = "Clubs - Admin Reports Ban Reporter"

.field public static final AdminReportsCreatorMessage:Ljava/lang/String; = "Clubs - Admin Reports Creator Message"

.field public static final AdminReportsDelete:Ljava/lang/String; = "Clubs - Admin Reports Delete"

.field public static final AdminReportsIgnore:Ljava/lang/String; = "Clubs - Admin Reports Ignore"

.field public static final AdminReportsReporterMessage:Ljava/lang/String; = "Clubs - Admin Reports Reporter Message"

.field public static final AdminRequestsIgnore:Ljava/lang/String; = "Clubs - Admin Requests Ignore"

.field public static final AdminRequestsRecommendations:Ljava/lang/String; = "Clubs - Admin Requests and Recommendations"

.field public static final AdminSendAllInvites:Ljava/lang/String; = "Clubs - Admin Requests Send All Invites"

.field public static final AdminSendInvite:Ljava/lang/String; = "Clubs - Admin Send Invite"

.field public static final AdminSettings:Ljava/lang/String; = "Clubs - Admin Settings"

.field public static final AdminSettingsChatMembers:Ljava/lang/String; = "Clubs - Admin Settings Chat Post Members"

.field public static final AdminSettingsChatModeratorsOwner:Ljava/lang/String; = "Clubs - Admin Settings Chat Post Moderators and Owner"

.field public static final AdminSettingsDelete:Ljava/lang/String; = "Clubs - Admin Settings Delete"

.field public static final AdminSettingsDeleteConfirm:Ljava/lang/String; = "Clubs - Admin Settings Delete Confirm"

.field public static final AdminSettingsDisableGuestLFG:Ljava/lang/String; = "Clubs - Admin Settings Disable Guest LFG"

.field public static final AdminSettingsDisableJoin:Ljava/lang/String; = "Clubs - Admin Settings Disable Join Membership"

.field public static final AdminSettingsDisableMatureBroadcasts:Ljava/lang/String; = "Clubs - Admin Settings Disable Mature Broadcasts"

.field public static final AdminSettingsDisableWatchClubTitlesOnly:Ljava/lang/String; = "Clubs - Admin Settings Disable Watch Club Titles Only"

.field public static final AdminSettingsEnableGuestLFG:Ljava/lang/String; = "Clubs - Admin Settings Enable Guest LFG"

.field public static final AdminSettingsEnableJoin:Ljava/lang/String; = "Clubs - Admin Settings Enable Join Membership"

.field public static final AdminSettingsEnableMatureBroadcasts:Ljava/lang/String; = "Clubs - Admin Settings Enable Mature Broadcasts"

.field public static final AdminSettingsEnableWatchClubTitlesOnly:Ljava/lang/String; = "Clubs - Admin Settings Enable Watch Club Titles Only"

.field public static final AdminSettingsFeedPostAdmins:Ljava/lang/String; = "Clubs - Admin Settings Feed Post Admins"

.field public static final AdminSettingsFeedPostMembers:Ljava/lang/String; = "Clubs - Admin Settings Feed Post Members"

.field public static final AdminSettingsFeedPostOwner:Ljava/lang/String; = "Clubs - Admin Settings Feed Post Owner"

.field public static final AdminSettingsInviteAdmins:Ljava/lang/String; = "Clubs - Admin Settings Invite Moderators"

.field public static final AdminSettingsInviteMembers:Ljava/lang/String; = "Clubs - Admin Settings Invite Members"

.field public static final AdminSettingsInviteOwner:Ljava/lang/String; = "Clubs - Admin Settings Invite Owner"

.field public static final AdminSettingsPlayPostMembers:Ljava/lang/String; = "Clubs - Admin Settings Play Post Members"

.field public static final AdminSettingsPlayPostModerators:Ljava/lang/String; = "Clubs - Admin Settings Play Post Moderators"

.field public static final AdminSettingsPlayPostOwner:Ljava/lang/String; = "Clubs - Admin Settings Play Post Owner"

.field public static final AdminSettingsResetToDefault:Ljava/lang/String; = "Clubs - Admin Settings Reset to Default"

.field public static final AdminSettingsTransferOwnership:Ljava/lang/String; = "Clubs - Admin Settings Transfer Ownership"

.field public static final AdminSettingsTransferOwnershipSelectOwner:Ljava/lang/String; = "Clubs - Admin Settings Transfer Ownership Select Owner"

.field public static final AdminSettingsTransferOwnershipSubmit:Ljava/lang/String; = "Clubs - Admin Settings Transfer Ownership Submit"

.field public static final AdminTab:Ljava/lang/String; = "Clubs - Admin Tab"

.field public static final AdminUnban:Ljava/lang/String; = "Clubs - Admin Unban"

.field public static final ChangeColor:Ljava/lang/String; = "Clubs - Customize Club Change Color"

.field public static final ChatCopyMessage:Ljava/lang/String; = "Clubs - Chat Copy Message"

.field public static final ChatDeleteMessage:Ljava/lang/String; = "Clubs - Chat Delete Message"

.field public static final ChatEditTopic:Ljava/lang/String; = "Clubs - Chat Edit Topic"

.field public static final ChatEditTopicCancel:Ljava/lang/String; = "Clubs - Chat Edit Topic Cancel"

.field public static final ChatEditTopicEdit:Ljava/lang/String; = "Clubs - Chat Edit Topic Edit"

.field public static final ChatEditTopicPost:Ljava/lang/String; = "Clubs - Chat Edit Topic Post"

.field public static final ChatEditTopicReport:Ljava/lang/String; = "Clubs - Chat Edit Topic Report"

.field public static final ChatManageNotifications:Ljava/lang/String; = "Clubs - Chat Manage Notifications"

.field public static final ChatPickMentionedUser:Ljava/lang/String; = "Clubs - Chat Pick Mentioned User"

.field public static final ChatReport:Ljava/lang/String; = "Clubs - Chat Report"

.field public static final ChatSendMessage:Ljava/lang/String; = "Clubs - Chat Send Message"

.field public static final ChatShowMentions:Ljava/lang/String; = "Clubs - Chat Show Mentions"

.field public static final ChatTab:Ljava/lang/String; = "Clubs - Chat Tab"

.field public static final CreateBack:Ljava/lang/String; = "Clubs - Create Back"

.field public static final CreateCancel:Ljava/lang/String; = "Clubs - Create Cancel"

.field public static final CreateCheckAvailability:Ljava/lang/String; = "Clubs - Create Check Availability"

.field public static final CreateCheckAvailabilityResult:Ljava/lang/String; = "Clubs - Create Check Availability Result"

.field public static final CreateClubResult:Ljava/lang/String; = "Clubs - Create Club Result"

.field public static final CreateClubSubmit:Ljava/lang/String; = "Clubs - Create Club Submit"

.field public static final CreateHidden:Ljava/lang/String; = "Clubs - Create Hidden"

.field public static final CreateNext:Ljava/lang/String; = "Clubs - Create Next"

.field public static final CreatePrivate:Ljava/lang/String; = "Clubs - Create Private"

.field public static final CreatePublic:Ljava/lang/String; = "Clubs - Create Public"

.field public static final CustomizeBackground:Ljava/lang/String; = "Clubs - Customize Club Add Background"

.field public static final CustomizeBackgroundCamera:Ljava/lang/String; = "Clubs - Customize Club Use Camera for Background"

.field public static final CustomizeBackgroundCaptures:Ljava/lang/String; = "Clubs - Customize Club Use Captures for Background"

.field public static final CustomizeBackgroundGamerPics:Ljava/lang/String; = "Clubs - Customize Club Use GamerPics for Background"

.field public static final CustomizeCancel:Ljava/lang/String; = "Clubs - Customize Club Cancel"

.field public static final CustomizeDone:Ljava/lang/String; = "Clubs - Customize Club Done"

.field public static final CustomizeEditDescription:Ljava/lang/String; = "Clubs - Customize Club Edit Description"

.field public static final CustomizeEditTags:Ljava/lang/String; = "Clubs - Customize Club Edit Tags"

.field public static final CustomizeName:Ljava/lang/String; = "Clubs - Customize Club Edit Name"

.field public static final DiscoverAccept:Ljava/lang/String; = "Clubs - Discover Accept Invitation"

.field public static final DiscoverCreate:Ljava/lang/String; = "Clubs - Discover Create"

.field public static final DiscoverDoSearch:Ljava/lang/String; = "Clubs - Discover Execute Search"

.field public static final DiscoverIgnore:Ljava/lang/String; = "Clubs - Discover Ignore Invitation"

.field public static final DiscoverNavigateInvitedClub:Ljava/lang/String; = "Clubs - Discover Navigate to Invited Club"

.field public static final DiscoverNavigateOwnClub:Ljava/lang/String; = "Clubs - Discover Navigate to Own Club"

.field public static final DiscoverSearch:Ljava/lang/String; = "Clubs - Discover Search"

.field public static final DiscoverSearchGames:Ljava/lang/String; = "Clubs - Discover Search Games"

.field public static final DiscoverSearchGamesText:Ljava/lang/String; = "Clubs - Discover Search Games Text"

.field public static final DiscoverSearchNavigateClub:Ljava/lang/String; = "Clubs - Discover Search Navigate to Club"

.field public static final DiscoverSearchSelect:Ljava/lang/String; = "Clubs - Discover Search Select"

.field public static final DiscoverSearchTags:Ljava/lang/String; = "Clubs - Discover Search Tags"

.field public static final DiscoverSearchText:Ljava/lang/String; = "Clubs - Discover Search Text"

.field public static final DiscoverSeeAllSuggested:Ljava/lang/String; = "Clubs - Discover See All Game Suggested Clubs"

.field public static final EditGames:Ljava/lang/String; = "Clubs - Customize Club Edit Games"

.field public static final HomeAcceptInvitation:Ljava/lang/String; = "Clubs - Home Accept Invitation"

.field public static final HomeCancelRequestJoin:Ljava/lang/String; = "Clubs - Home Cancel Request to Join"

.field public static final HomeCustomize:Ljava/lang/String; = "Clubs - Home Customize"

.field public static final HomeDeclineInvitation:Ljava/lang/String; = "Clubs - Home Decline Invitation"

.field public static final HomeFollow:Ljava/lang/String; = "Clubs - Home Follow"

.field public static final HomeInvite:Ljava/lang/String; = "Clubs - Home Invite"

.field public static final HomeNotifications:Ljava/lang/String; = "Clubs - Home Notifications"

.field public static final HomeReport:Ljava/lang/String; = "Clubs - Home Report"

.field public static final HomeRequestToJoin:Ljava/lang/String; = "Clubs - Home Request to Join"

.field public static final HomeResign:Ljava/lang/String; = "Clubs - Home Resign from Club"

.field public static final HomeTab:Ljava/lang/String; = "Clubs - Home Tab"

.field public static final HomeUnfollow:Ljava/lang/String; = "Clubs - Home Unfollow"

.field public static final MeProfileNavigateClub:Ljava/lang/String; = "Clubs - Me Profile Navigate to Club"

.field public static final PeopleNavigateClub:Ljava/lang/String; = "Clubs - People Navigate to Club"

.field public static final PlayGame:Ljava/lang/String; = "Clubs - Play Game"

.field public static final PlayTab:Ljava/lang/String; = "Clubs - Play Tab"

.field public static final RemoveGame:Ljava/lang/String; = "Clubs - Customize Club Remove Game"

.field public static final Report:Ljava/lang/String; = "Clubs - Report"

.field public static final RosterFilter:Ljava/lang/String; = "Clubs - Roster Filter"

.field public static final RosterNavigateProfile:Ljava/lang/String; = "Clubs - Roster Navigate to Profile"

.field public static final RosterTab:Ljava/lang/String; = "Clubs - Roster Tab"

.field public static final SuggestedClub:Ljava/lang/String; = "Clubs - Navigate To Suggested Club"

.field public static final UseCustomBackgroundPic:Ljava/lang/String; = "Clubs - Use Custom Background Pic"

.field public static final UseCustomLogo:Ljava/lang/String; = "Clubs - Use Custom Club Logo"

.field public static final WatchTab:Ljava/lang/String; = "Clubs - Watch Tab"

.field public static final YouProfileNavigateClub:Ljava/lang/String; = "Clubs - You Profile Navigate to Club"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 552
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
