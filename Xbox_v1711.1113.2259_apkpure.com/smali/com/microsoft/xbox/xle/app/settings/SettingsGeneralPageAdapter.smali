.class public Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "SettingsGeneralPageAdapter.java"


# instance fields
.field private ShareToggleOnOffLayout:Landroid/view/View;

.field private final developerSection:Landroid/view/View;

.field private final editRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private facebookLoginButton:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private facebookLoginSubtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private facebookLoginView:Landroid/widget/RelativeLayout;

.field private facebookNotInterestedButton:Landroid/widget/RelativeLayout;

.field private facebookRepairLinkButton:Landroid/widget/RelativeLayout;

.field private final gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private homeScreenSpinner:Landroid/widget/Spinner;

.field private homeScreenSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;",
            ">;"
        }
    .end annotation
.end field

.field private final linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

.field private final linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

.field private linkedAccountTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private phoneLinkUnlinkButton:Landroid/widget/RelativeLayout;

.field private phoneLinkUnlinkSubtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private phoneLinkUnlinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private phoneNotInterestedButton:Landroid/widget/RelativeLayout;

.field private final profilePic:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field final r:Landroid/content/res/Resources;

.field private final realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final sandboxId:Landroid/widget/TextView;

.field private final secretListener:Landroid/view/View$OnLongClickListener;

.field private settingsEditAndShareRealNameSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final shareRealNameRadioGroup:Landroid/widget/RadioGroup;

.field private final shareRealNameWithFriendsSelective:Landroid/widget/RadioButton;

.field private final shareSettingsDescription:Landroid/view/View;

.field private shareToggleButton:Landroid/widget/ToggleButton;

.field private shareToggleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final shareTransitivelyCheckbox:Landroid/widget/CheckBox;

.field private final shareWithEveryoneButton:Landroid/widget/RadioButton;

.field private final shareWithFriendsButton:Landroid/widget/RadioButton;

.field private final stayAwakeAlwaysRadioButton:Landroid/widget/RadioButton;

.field private final stayAwakeOnlyRemoteRadioButton:Landroid/widget/RadioButton;

.field private final stayAwakeUseDeviceRadioButton:Landroid/widget/RadioButton;

.field private final version:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

.field welcomeCardCompletionRepository:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
    .locals 11
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 96
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v7}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v7

    invoke-interface {v7, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)V

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/MainActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v7

    const-string v8, "fonts/SegoeWP.ttf"

    invoke-static {v7, v8}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v4

    .line 100
    .local v4, "segoeFont":Landroid/graphics/Typeface;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->r:Landroid/content/res/Resources;

    .line 101
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .line 102
    const-class v7, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {p1, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->findFirstChildViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    .line 103
    const-class v7, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-virtual {p1, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->findFirstChildViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    .line 104
    const v7, 0x7f0e09e9

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->screenBody:Landroid/view/View;

    .line 106
    const v7, 0x7f0e09ec

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    const v7, 0x7f0e09ed

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkedAccountTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 112
    const v7, 0x7f0e09de

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginButton:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 114
    const v7, 0x7f0e09dc

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginView:Landroid/widget/RelativeLayout;

    .line 115
    const v7, 0x7f0e09dd

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 116
    .local v1, "facebookImageView":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    sget-object v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->FacebookFriend:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->name()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;->MEDIUM:Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->getIconBySize(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;)Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "facebookIcon":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 120
    :cond_0
    const v7, 0x7f0e09df

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginSubtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 122
    const v7, 0x7f0e09e1

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookNotInterestedButton:Landroid/widget/RelativeLayout;

    .line 123
    const v7, 0x7f0e09e5

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookRepairLinkButton:Landroid/widget/RelativeLayout;

    .line 125
    const v7, 0x7f0e0a15

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->version:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 126
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->version:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    const v7, 0x7f0e0a16

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 129
    .local v6, "whatsNewDetails":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    const v7, 0x7f0e0a18

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 132
    .local v5, "thirdPartyNotice":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    const v7, 0x7f0e0a17

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 135
    .local v2, "help":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    const v7, 0x7f0e0a19

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 138
    .local v3, "privacy":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    const v7, 0x7f0e0a1b

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->developerSection:Landroid/view/View;

    .line 142
    const v7, 0x7f0e0a1c

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->sandboxId:Landroid/widget/TextView;

    .line 144
    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getPaintFlags()I

    move-result v7

    or-int/lit8 v7, v7, 0x8

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setPaintFlags(I)V

    .line 145
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getPaintFlags()I

    move-result v7

    or-int/lit8 v7, v7, 0x8

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setPaintFlags(I)V

    .line 146
    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getPaintFlags()I

    move-result v7

    or-int/lit8 v7, v7, 0x8

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setPaintFlags(I)V

    .line 147
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getPaintFlags()I

    move-result v7

    or-int/lit8 v7, v7, 0x8

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setPaintFlags(I)V

    .line 149
    const v7, 0x7f0e0a1e

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeAlwaysRadioButton:Landroid/widget/RadioButton;

    .line 150
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeAlwaysRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v7, v4}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 151
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeAlwaysRadioButton:Landroid/widget/RadioButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    const v7, 0x7f0e0a1f

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeOnlyRemoteRadioButton:Landroid/widget/RadioButton;

    .line 154
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeOnlyRemoteRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v7, v4}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 155
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeOnlyRemoteRadioButton:Landroid/widget/RadioButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    const v7, 0x7f0e0a20

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeUseDeviceRadioButton:Landroid/widget/RadioButton;

    .line 158
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeUseDeviceRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v7, v4}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 159
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeUseDeviceRadioButton:Landroid/widget/RadioButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    const v7, 0x7f0e09ee

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareSettingsDescription:Landroid/view/View;

    .line 162
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnLongClickListener;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->secretListener:Landroid/view/View$OnLongClickListener;

    .line 167
    const v7, 0x7f0e0a2f

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 168
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getPaintFlags()I

    move-result v8

    or-int/lit8 v8, v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setPaintFlags(I)V

    .line 169
    const v7, 0x7f0e0236

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioGroup;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameRadioGroup:Landroid/widget/RadioGroup;

    .line 171
    const v7, 0x7f0e0a27

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->profilePic:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 172
    const v7, 0x7f0e0a28

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 173
    const v7, 0x7f0e055b

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 174
    const v7, 0x7f0e0a29

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->editRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 175
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->editRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getPaintFlags()I

    move-result v8

    or-int/lit8 v8, v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setPaintFlags(I)V

    .line 177
    const v7, 0x7f0e0a2c

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareWithEveryoneButton:Landroid/widget/RadioButton;

    .line 178
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareWithEveryoneButton:Landroid/widget/RadioButton;

    invoke-virtual {v7, v4}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 179
    const v7, 0x7f0e0a2d

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareWithFriendsButton:Landroid/widget/RadioButton;

    .line 180
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareWithFriendsButton:Landroid/widget/RadioButton;

    invoke-virtual {v7, v4}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 181
    const v7, 0x7f0e0a2e

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameWithFriendsSelective:Landroid/widget/RadioButton;

    .line 182
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameWithFriendsSelective:Landroid/widget/RadioButton;

    invoke-virtual {v7, v4}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    .line 183
    const v7, 0x7f0e0a30

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    .line 184
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v4}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 186
    const v7, 0x7f0e0a21

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->settingsEditAndShareRealNameSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 187
    const v7, 0x7f0e0a22

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ToggleButton;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareToggleButton:Landroid/widget/ToggleButton;

    .line 188
    const v7, 0x7f0e0a23

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareToggleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 189
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 190
    const v7, 0x7f0e0a24

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->ShareToggleOnOffLayout:Landroid/view/View;

    .line 192
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareToggleButton:Landroid/widget/ToggleButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 209
    const v7, 0x7f0e0a0d

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkButton:Landroid/widget/RelativeLayout;

    .line 210
    const v7, 0x7f0e0a10

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneNotInterestedButton:Landroid/widget/RelativeLayout;

    .line 211
    const v7, 0x7f0e0a0e

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 212
    const v7, 0x7f0e0a0f

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkSubtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 214
    const v7, 0x7f0e0a31

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Spinner;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->homeScreenSpinner:Landroid/widget/Spinner;

    .line 215
    new-instance v7, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v8

    const v9, 0x7f030202

    invoke-static {}, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->getItemList()Ljava/util/List;

    move-result-object v10

    invoke-direct {v7, v8, v9, v10}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->homeScreenSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 216
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->homeScreenSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v8, 0x7f03020a

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 217
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->homeScreenSpinner:Landroid/widget/Spinner;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->homeScreenSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v7, v8}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 218
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->homeScreenSpinner:Landroid/widget/Spinner;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ordinal()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/Spinner;->setSelection(I)V

    .line 219
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->homeScreenSpinner:Landroid/widget/Spinner;

    new-instance v8, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$1;

    invoke-direct {v8, p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)V

    invoke-virtual {v7, v8}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 232
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->homeScreenSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    return-object v0
.end method

.method private createShareWithSelectedFriendsTextCount(I)Ljava/lang/String;
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 464
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->r:Landroid/content/res/Resources;

    const v2, 0x7f070b19

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private facebookLoginUpdate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 495
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->showLinkFacebookAccountButtonInSettings()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->setFacebookLoginButtonText()V

    .line 497
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkedAccountTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 498
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 503
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->showNotInterestedButton()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 504
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookNotInterestedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 505
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookRepairLinkButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 513
    :goto_1
    return-void

    .line 500
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkedAccountTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 501
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 506
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->nextActionRequired()Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->TokenRenewalRequired:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v0, v1, :cond_2

    .line 507
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookNotInterestedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 508
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookRepairLinkButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    .line 510
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookRepairLinkButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 511
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookNotInterestedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private getUsersSharingRealNameCount()I
    .locals 6

    .prologue
    .line 468
    const/4 v0, 0x0

    .line 469
    .local v0, "i":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->fetchFollowingSummary()Ljava/util/ArrayList;

    move-result-object v2

    .line 470
    .local v2, "people":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;>;"
    if-nez v2, :cond_0

    move v1, v0

    .line 479
    .end local v0    # "i":I
    .local v1, "i":I
    :goto_0
    return v1

    .line 474
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;

    .line 475
    .local v3, "person":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    iget-boolean v5, v3, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->isIdentityShared:Z

    if-eqz v5, :cond_1

    .line 476
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v3    # "person":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    :cond_2
    move v1, v0

    .line 479
    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 107
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Sign Out"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->signOut()V

    .line 109
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->NavigateToAboutScreen()V

    return-void
.end method

.method static synthetic lambda$new$10(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 193
    if-eqz p2, :cond_1

    .line 198
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v0

    .line 199
    .local v0, "oldSetting":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Blocked:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-ne v0, v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->PeopleOnMyList:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    .line 206
    .end local v0    # "oldSetting":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareToggleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v2, p2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getShareText(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    return-void

    .line 204
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Blocked:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->navigateToWhatsNewDetailsScreen()V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->navigateToThirdPartyNoticeScreen()V

    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->navigateToHelpScreen()V

    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->navigateToPrivacyScreen()V

    return-void
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->Always:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->setStayAwakeSetting(Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;)V

    return-void
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->OnlyRemote:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->setStayAwakeSetting(Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;)V

    return-void
.end method

.method static synthetic lambda$new$8(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->UseDeviceSettings:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->setStayAwakeSetting(Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;)V

    return-void
.end method

.method static synthetic lambda$new$9(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "ignored"    # Landroid/view/View;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->incrementSecretEnable()V

    .line 164
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic lambda$onStart$11(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->linkUnlink()V

    .line 244
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->setFacebookLoginButtonText()V

    .line 245
    return-void
.end method

.method static synthetic lambda$onStart$12(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->repairLink()V

    .line 253
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$13(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    if-eqz v0, :cond_0

    .line 259
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCFacebookIntegration;->trackOptOut()V

    .line 260
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->hideLinkButtonInSuggestions()V

    .line 262
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$14(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/widget/RadioGroup;I)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 269
    invoke-virtual {p1, v1}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 270
    packed-switch p2, :pswitch_data_0

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 272
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareWithEveryoneButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Everyone:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    goto :goto_0

    .line 278
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 279
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareWithFriendsButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->PeopleOnMyList:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    goto :goto_0

    .line 284
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameWithFriendsSelective:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->FriendCategoryShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    goto :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x7f0e0a2c
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic lambda$onStart$15(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->navigateToPeopleSelectorActivity()V

    return-void
.end method

.method static synthetic lambda$onStart$16(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->ShareIdentityTransitively:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Everyone:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    .line 305
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->ShareIdentityTransitively:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Blocked:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->changeUserShareIdentityStatus(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$17(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unlink phone number"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsUnlink()V

    .line 315
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->linkUnlink()V

    .line 316
    return-void

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Find phone contacts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsLinkAction()V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$18(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 321
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackOptOut()V

    .line 322
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->notInterested()V

    .line 323
    return-void
.end method

.method static synthetic lambda$updateViewOverride$19(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->navigateToEditRealNameDialog()V

    return-void
.end method

.method private phoneLinkUnlinkUIUpdate()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 533
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->showLinkPhoneAccountButtonInSettings()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 534
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 535
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->isPhoneNumberLinked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f07056b

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 537
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkSubtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f07055f

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 546
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkPhoneAccountViewModel:Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkPhoneAccountViewModel;->showNotInterestedButton()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 547
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneNotInterestedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 551
    :goto_1
    return-void

    .line 539
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f070568

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 540
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkSubtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v1, 0x7f070538

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    goto :goto_0

    .line 543
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 549
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneNotInterestedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private setEnabledIfNotNull(Landroid/view/View;Z)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "enabled"    # Z

    .prologue
    .line 488
    if-eqz p1, :cond_0

    .line 489
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 490
    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 492
    :cond_0
    return-void

    .line 490
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setFacebookLoginButtonText()V
    .locals 4

    .prologue
    .line 517
    sget-object v2, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$2;->$SwitchMap$com$microsoft$xbox$service$model$friendfinder$OptInStatus:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->linkFacebookVM:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->nextActionRequired()Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 524
    const v0, 0x7f070567

    .line 525
    .local v0, "linkUnlinkTextId":I
    const v1, 0x7f07052e

    .line 528
    .local v1, "subtextId":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginButton:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 529
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginSubtext:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 530
    return-void

    .line 520
    .end local v0    # "linkUnlinkTextId":I
    .end local v1    # "subtextId":I
    :pswitch_0
    const v0, 0x7f07056a

    .line 521
    .restart local v0    # "linkUnlinkTextId":I
    const v1, 0x7f070559

    .line 522
    .restart local v1    # "subtextId":I
    goto :goto_0

    .line 517
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private setSelectionForRadioGroup(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V
    .locals 3
    .param p1, "selection"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .prologue
    const/4 v2, 0x0

    .line 441
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getIsChangingUserShareIdentityStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 461
    :goto_0
    return-void

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameRadioGroup:Landroid/widget/RadioGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 445
    sget-object v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$2;->$SwitchMap$com$microsoft$xbox$service$model$privacy$PrivacySettings$PrivacySettingValue:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 456
    const-string v0, "SettingsActivityAdapter"

    const-string v1, "Incorrect option for real name radio group"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    goto :goto_0

    .line 447
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameRadioGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f0e0a2c

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 450
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameRadioGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f0e0a2d

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 453
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameRadioGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f0e0a2e

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 445
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onDestroy()V
    .locals 0

    .prologue
    .line 484
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onDestroy()V

    .line 485
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 236
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 238
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginUpdate()V

    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginView:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookRepairLinkButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookRepairLinkButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookNotInterestedButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookNotInterestedButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameRadioGroup:Landroid/widget/RadioGroup;

    if-eqz v0, :cond_3

    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameRadioGroup:Landroid/widget/RadioGroup;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/widget/RadioGroup$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 293
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_4

    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_5

    .line 299
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_6

    .line 309
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$18;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneNotInterestedButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_7

    .line 320
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneNotInterestedButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$19;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    :cond_7
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 329
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 330
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginView:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookNotInterestedButton:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookRepairLinkButton:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 337
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkButton:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 338
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneNotInterestedButton:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeOnClickListenerIfNotNull(Landroid/view/View;)V

    .line 339
    return-void
.end method

.method public updateViewOverride()V
    .locals 9

    .prologue
    const v8, 0x7f020125

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 343
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->facebookLoginUpdate()V

    .line 345
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->setFacebookLoginButtonText()V

    .line 347
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->phoneLinkUnlinkUIUpdate()V

    .line 349
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->version:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->r:Landroid/content/res/Resources;

    const v3, 0x7f070b71

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getVersion()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 350
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isDeveloperMode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 351
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->developerSection:Landroid/view/View;

    invoke-direct {p0, v1, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->setEnabledIfNotNull(Landroid/view/View;Z)V

    .line 352
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->sandboxId:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->r:Landroid/content/res/Resources;

    const v4, 0x7f070b77

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getSandboxId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 357
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$StayAwakeSettings:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getStayAwakeSetting()Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 366
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeOnlyRemoteRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 370
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getRealName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getRealName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getCanShareRealName()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 371
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->settingsEditAndShareRealNameSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v1, v7}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 372
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->setSelectionForRadioGroup(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)V

    .line 374
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->FriendCategoryShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-ne v1, v2, :cond_7

    .line 375
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 376
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getIsLoadingUserFollowingSummaryStatus()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 377
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 387
    :goto_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    if-eqz v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isBusy()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 389
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 402
    :cond_0
    :goto_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->profilePic:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v1, :cond_1

    .line 403
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->profilePic:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getProfilePic()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 406
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v1, :cond_2

    .line 407
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getRealName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v1, :cond_3

    .line 411
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getGamerTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 414
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->editRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_4

    .line 415
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->editRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$$Lambda$20;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 418
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Blocked:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-ne v1, v2, :cond_a

    .line 419
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareToggleButton:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v6}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 420
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareToggleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getShareText(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->ShareToggleOnOffLayout:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 435
    :goto_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareSettingsDescription:Landroid/view/View;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isSecretAvailable()Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->secretListener:Landroid/view/View$OnLongClickListener;

    :goto_5
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 437
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isBlockingBusy()Z

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getBlockingStatusText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->setBlocking(ZLjava/lang/String;)V

    .line 438
    return-void

    .line 354
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->developerSection:Landroid/view/View;

    invoke-direct {p0, v1, v6}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->setEnabledIfNotNull(Landroid/view/View;Z)V

    goto/16 :goto_0

    .line 359
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeAlwaysRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_1

    .line 362
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->stayAwakeUseDeviceRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_1

    .line 379
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 380
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->getUsersSharingRealNameCount()I

    move-result v0

    .line 381
    .local v0, "followersSharingRealNameCount":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameWithFriendsSelective:Landroid/widget/RadioButton;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->createShareWithSelectedFriendsTextCount(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 384
    .end local v0    # "followersSharingRealNameCount":I
    :cond_7
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareRealNameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 391
    :cond_8
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getShareRealNameStatus()Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->PeopleOnMyList:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-ne v1, v2, :cond_9

    .line 392
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 393
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 394
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getSharingTransitivelyStatus()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_3

    .line 396
    :cond_9
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 397
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareTransitivelyCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_3

    .line 423
    :cond_a
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareToggleButton:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v7}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 424
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->shareToggleText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getShareText(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 425
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->ShareToggleOnOffLayout:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    .line 428
    :cond_b
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getIsLoadingProfile()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 429
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->settingsEditAndShareRealNameSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    goto/16 :goto_4

    .line 431
    :cond_c
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->settingsEditAndShareRealNameSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    goto/16 :goto_4

    .line 435
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 357
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
