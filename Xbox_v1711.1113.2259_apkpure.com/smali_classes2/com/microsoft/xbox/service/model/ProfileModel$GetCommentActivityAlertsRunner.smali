.class Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetCommentActivityAlertsRunner"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<[",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field resetReadCount:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Z)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "resetReadCount"    # Z

    .prologue
    .line 3307
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 3308
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 3309
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->xuid:Ljava/lang/String;

    .line 3310
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->resetReadCount:Z

    .line 3311
    return-void
.end method

.method static synthetic access$4000(Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .prologue
    .line 3301
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionDate(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method private getAlertActionDate(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/util/Date;
    .locals 2
    .param p1, "action"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .prologue
    .line 3610
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3611
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3612
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->like:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->date:Ljava/util/Date;

    .line 3620
    :goto_0
    return-object v0

    .line 3613
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3614
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->share:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->date:Ljava/util/Date;

    goto :goto_0

    .line 3615
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3616
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->comment:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->date:Ljava/util/Date;

    goto :goto_0

    .line 3620
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getAlertActionId(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;
    .locals 2
    .param p1, "action"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .prologue
    .line 3638
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3639
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3640
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->like:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->id:Ljava/lang/String;

    .line 3648
    :goto_0
    return-object v0

    .line 3641
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3642
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->share:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->id:Ljava/lang/String;

    goto :goto_0

    .line 3643
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3644
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->comment:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->id:Ljava/lang/String;

    goto :goto_0

    .line 3648
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getAlertActionRootItem(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;
    .locals 2
    .param p1, "action"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .prologue
    .line 3624
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3625
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3626
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->like:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->rootPath:Ljava/lang/String;

    .line 3634
    :goto_0
    return-object v0

    .line 3627
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3628
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->share:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->rootPath:Ljava/lang/String;

    goto :goto_0

    .line 3629
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3630
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->comment:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->rootPath:Ljava/lang/String;

    goto :goto_0

    .line 3634
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getAlertActionXuid(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;
    .locals 2
    .param p1, "action"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .prologue
    .line 3652
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3653
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Like:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3654
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->like:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->xuid:Ljava/lang/String;

    .line 3662
    :goto_0
    return-object v0

    .line 3655
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Share:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3656
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->share:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->xuid:Ljava/lang/String;

    goto :goto_0

    .line 3657
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    sget-object v1, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->Comment:Lcom/microsoft/xbox/service/model/ActivityAlertActionType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ActivityAlertActionType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3658
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->comment:Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Entity;->xuid:Ljava/lang/String;

    goto :goto_0

    .line 3662
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getLastMonthDataForCommentAlerts(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3523
    .local p1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3524
    const/4 v1, 0x0

    .line 3534
    :cond_0
    return-object v1

    .line 3527
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3528
    .local v1, "activityData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .line 3529
    .local v0, "action":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionDate(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/util/Date;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isDateWithinLastMonth(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3530
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getLastMonthDataForFollowers(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3580
    .local p1, "followers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3581
    const/4 v0, 0x0

    .line 3591
    :cond_0
    return-object v0

    .line 3584
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3585
    .local v0, "activityData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;

    .line 3586
    .local v1, "follower":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;->followedDateTimeUtc:Ljava/util/Date;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isDateWithinLastMonth(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3587
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private mergeActivityAlerts(Ljava/util/ArrayList;Ljava/util/ArrayList;)[Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;)[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .local p1, "commentAlerts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;>;"
    .local p2, "followerAlerts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    const/4 v1, 0x0

    .line 3492
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3518
    :cond_0
    :goto_0
    return-object v1

    .line 3496
    :cond_1
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3497
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3498
    new-instance v2, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V

    invoke-static {p1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 3499
    invoke-virtual {p1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 3505
    :cond_2
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 3506
    new-instance v2, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V

    invoke-static {p2, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 3507
    invoke-virtual {p2}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 3510
    :cond_3
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3511
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3512
    .local v0, "rawCombinedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3513
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3514
    new-instance v2, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner$ActivityAlertComparator;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 3515
    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method private mergeAndCollapseActivityAlerts(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;Ljava/util/ArrayList;)[Ljava/lang/Object;
    .locals 12
    .param p1, "commentAlerts"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;)[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .local p2, "followerData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    const-wide/16 v10, 0x0

    const/4 v7, 0x0

    .line 3467
    if-eqz p1, :cond_1

    iget-object v6, p1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->actions:Ljava/util/ArrayList;

    :goto_0
    invoke-direct {p0, v6, p2}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->mergeActivityAlerts(Ljava/util/ArrayList;Ljava/util/ArrayList;)[Ljava/lang/Object;

    move-result-object v5

    .line 3469
    .local v5, "mergeResult":[Ljava/lang/Object;
    if-eqz v5, :cond_4

    .line 3470
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3471
    .local v4, "mergeAndCollapsedResultSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    array-length v7, v5

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_3

    aget-object v1, v5, v6

    .line 3472
    .local v1, "item":Ljava/lang/Object;
    instance-of v8, v1, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    if-eqz v8, :cond_2

    move-object v0, v1

    .line 3473
    check-cast v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .line 3474
    .local v0, "action":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;
    invoke-direct {p0, v4, v0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->setActionItemCountMatchingRoot(Ljava/util/ArrayList;Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)J

    move-result-wide v2

    .line 3476
    .local v2, "count":J
    cmp-long v8, v2, v10

    if-nez v8, :cond_0

    .line 3477
    iput-wide v10, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->count:J

    .line 3478
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3471
    .end local v0    # "action":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;
    .end local v2    # "count":J
    :cond_0
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .end local v1    # "item":Ljava/lang/Object;
    .end local v4    # "mergeAndCollapsedResultSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v5    # "mergeResult":[Ljava/lang/Object;
    :cond_1
    move-object v6, v7

    .line 3467
    goto :goto_0

    .line 3481
    .restart local v1    # "item":Ljava/lang/Object;
    .restart local v4    # "mergeAndCollapsedResultSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v5    # "mergeResult":[Ljava/lang/Object;
    :cond_2
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3485
    .end local v1    # "item":Ljava/lang/Object;
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v7

    .line 3488
    .end local v4    # "mergeAndCollapsedResultSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_4
    return-object v7
.end method

.method private setActionItemCountMatchingRoot(Ljava/util/ArrayList;Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)J
    .locals 13
    .param p2, "action"    # Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;",
            ")J"
        }
    .end annotation

    .prologue
    .local p1, "activityData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-wide/16 v6, 0x0

    const-wide/16 v8, -0x1

    .line 3538
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 3575
    :cond_0
    :goto_0
    return-wide v6

    .line 3542
    :cond_1
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionRootItem(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v5

    .line 3543
    .local v5, "rootPath":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    move-wide v6, v8

    .line 3544
    goto :goto_0

    .line 3547
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 3548
    .local v2, "item":Ljava/lang/Object;
    instance-of v11, v2, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    if-eqz v11, :cond_3

    move-object v0, v2

    .line 3549
    check-cast v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .line 3550
    .local v0, "actionItem":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionRootItem(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v3

    .line 3553
    .local v3, "itemRootPath":Ljava/lang/String;
    iget-object v11, p2, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    move-wide v6, v8

    .line 3554
    goto :goto_0

    .line 3557
    :cond_4
    iget-object v11, p2, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    iget-object v12, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->actionType:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    if-eqz v3, :cond_3

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 3558
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionXuid(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v1

    .line 3559
    .local v1, "actionXuid":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionXuid(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v4

    .line 3560
    .local v4, "matchingActionXuid":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_3

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 3561
    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 3564
    iget-wide v6, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->count:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->count:J

    goto :goto_0

    :cond_5
    move-wide v6, v8

    .line 3567
    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3301
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->buildData()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public buildData()[Ljava/lang/Object;
    .locals 31
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->xuid:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v28

    if-nez v28, :cond_12

    .line 3316
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v23

    .line 3317
    .local v23, "slsService":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->xuid:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getCommentAlertInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;

    move-result-object v5

    .line 3318
    .local v5, "commentAlerts":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->xuid:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getFollowerInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;

    move-result-object v10

    .line 3320
    .local v10, "followerAlerts":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    if-eqz v5, :cond_0

    .line 3321
    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->actions:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getLastMonthDataForCommentAlerts(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v28

    move-object/from16 v0, v28

    iput-object v0, v5, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->actions:Ljava/util/ArrayList;

    .line 3324
    :cond_0
    if-eqz v10, :cond_1

    .line 3325
    iget-object v0, v10, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;->followers:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getLastMonthDataForFollowers(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v28

    move-object/from16 v0, v28

    iput-object v0, v10, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;->followers:Ljava/util/ArrayList;

    .line 3328
    :cond_1
    const/16 v20, 0x0

    .line 3329
    .local v20, "resetFollowerNewAlert":Z
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 3330
    .local v27, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v5, :cond_5

    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->actions:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v28

    if-nez v28, :cond_5

    .line 3331
    const/4 v12, 0x0

    .line 3332
    .local v12, "foundLastSeenAlertId":Z
    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->actions:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :cond_2
    :goto_0
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_5

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .line 3333
    .local v3, "action":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionXuid(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v26

    .line 3335
    .local v26, "xuid":Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_3

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_3

    .line 3336
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionXuid(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3339
    :cond_3
    const/16 v29, 0x1

    move/from16 v0, v29

    iput-boolean v0, v3, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->isNew:Z

    .line 3340
    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->lastSeenAlertId:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_2

    .line 3341
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionId(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v13

    .line 3342
    .local v13, "id":Ljava/lang/String;
    if-eqz v12, :cond_4

    .line 3343
    const/16 v29, 0x0

    move/from16 v0, v29

    iput-boolean v0, v3, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->isNew:Z

    goto :goto_0

    .line 3345
    :cond_4
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_2

    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->lastSeenAlertId:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v29

    if-eqz v29, :cond_2

    .line 3346
    const/4 v12, 0x1

    .line 3347
    const/16 v29, 0x0

    move/from16 v0, v29

    iput-boolean v0, v3, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->isNew:Z

    goto :goto_0

    .line 3354
    .end local v3    # "action":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;
    .end local v12    # "foundLastSeenAlertId":Z
    .end local v13    # "id":Ljava/lang/String;
    .end local v26    # "xuid":Ljava/lang/String;
    :cond_5
    const/4 v11, 0x0

    .line 3355
    .local v11, "followerData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    new-instance v9, Ljava/util/Hashtable;

    invoke-direct {v9}, Ljava/util/Hashtable;-><init>()V

    .line 3356
    .local v9, "followedLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;>;"
    if-eqz v10, :cond_8

    iget-object v0, v10, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;->followers:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v28

    if-nez v28, :cond_8

    .line 3358
    iget-object v0, v10, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;->followers:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :cond_6
    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_8

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;

    .line 3359
    .local v15, "p":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;
    iget-object v0, v15, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;->xuid:Ljava/lang/String;

    move-object/from16 v29, v0

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-nez v29, :cond_6

    .line 3360
    iget-object v0, v15, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;->xuid:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_7

    .line 3361
    iget-object v0, v15, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;->xuid:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3364
    :cond_7
    iget-object v0, v15, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;->xuid:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-virtual {v9, v0, v15}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 3369
    .end local v15    # "p":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;
    :cond_8
    const/16 v21, 0x0

    .line 3370
    .local v21, "response":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v28

    if-lez v28, :cond_9

    .line 3371
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 3372
    .local v22, "settings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v28, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3373
    sget-object v28, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->AppDisplayName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3374
    sget-object v28, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->GameDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3375
    sget-object v28, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual/range {v28 .. v28}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3377
    new-instance v16, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3378
    .local v16, "profileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    invoke-static/range {v16 .. v16}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->getUserProfileRequestBody(Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    move-result-object v21

    .line 3381
    .end local v16    # "profileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    .end local v22    # "settings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_9
    if-eqz v21, :cond_10

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v28

    if-nez v28, :cond_10

    .line 3382
    new-instance v17, Ljava/util/Hashtable;

    invoke-direct/range {v17 .. v17}, Ljava/util/Hashtable;-><init>()V

    .line 3383
    .local v17, "profileUserLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;>;"
    const/4 v6, 0x0

    .line 3384
    .local v6, "currentNewFollowerCount":I
    new-instance v11, Ljava/util/ArrayList;

    .end local v11    # "followerData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 3385
    .restart local v11    # "followerData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :cond_a
    :goto_2
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_c

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 3386
    .local v25, "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3387
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 3391
    new-instance v8, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-direct {v8}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>()V

    .line 3392
    .local v8, "f":Lcom/microsoft/xbox/service/model/FollowersData;
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iput-object v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    .line 3393
    new-instance v28, Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-direct/range {v28 .. v28}, Lcom/microsoft/xbox/service/model/UserProfileData;-><init>()V

    move-object/from16 v0, v28

    iput-object v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    .line 3394
    iget-object v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    move-object/from16 v28, v0

    sget-object v30, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    .line 3395
    iget-object v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    move-object/from16 v28, v0

    sget-object v30, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->AppDisplayName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/UserProfileData;->appDisplayName:Ljava/lang/String;

    .line 3396
    iget-object v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    move-object/from16 v28, v0

    sget-object v30, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerRealName:Ljava/lang/String;

    .line 3397
    iget-object v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    move-object/from16 v28, v0

    sget-object v30, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->GameDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/UserProfileData;->profileImageUrl:Ljava/lang/String;

    .line 3398
    iget-object v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;->followedDateTimeUtc:Ljava/util/Date;

    move-object/from16 v24, v0

    .line 3399
    .local v24, "timeStamp":Ljava/util/Date;
    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Lcom/microsoft/xbox/service/model/FollowersData;->setTimeStamp(Ljava/util/Date;)V

    .line 3401
    const/16 v28, 0x0

    move/from16 v0, v28

    iput-boolean v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->isNew:Z

    .line 3402
    iget v0, v10, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;->recentChangeCount:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v6, v0, :cond_b

    .line 3403
    iget-object v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v9, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;->recentChanges:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    .line 3404
    .local v18, "recentChanges":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v28

    if-nez v28, :cond_b

    const/16 v28, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/lang/String;

    const-string v30, "followed"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 3406
    const/16 v28, 0x1

    move/from16 v0, v28

    iput-boolean v0, v8, Lcom/microsoft/xbox/service/model/FollowersData;->isNew:Z

    .line 3407
    add-int/lit8 v6, v6, 0x1

    .line 3408
    if-nez v20, :cond_b

    .line 3409
    const/16 v20, 0x1

    .line 3414
    .end local v18    # "recentChanges":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_b
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 3417
    .end local v8    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v24    # "timeStamp":Ljava/util/Date;
    .end local v25    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-static {v0, v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$3602(Lcom/microsoft/xbox/service/model/ProfileModel;I)I

    .line 3419
    if-eqz v5, :cond_10

    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->actions:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v28

    if-nez v28, :cond_10

    .line 3420
    const/4 v14, 0x0

    .line 3421
    .local v14, "lastSeenId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static/range {v28 .. v29}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$3702(Lcom/microsoft/xbox/service/model/ProfileModel;I)I

    .line 3422
    iget-object v0, v5, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->actions:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :cond_d
    :goto_3
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_f

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .line 3423
    .restart local v3    # "action":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionXuid(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v4

    .line 3424
    .local v4, "actionXuid":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 3425
    .restart local v25    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    sget-object v29, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->GameDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    iput-object v0, v3, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->profileImageUrl:Ljava/lang/String;

    .line 3426
    sget-object v29, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileSettingValue(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v29

    iput-object v0, v3, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->realName:Ljava/lang/String;

    .line 3428
    iget-boolean v0, v3, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->isNew:Z

    move/from16 v29, v0

    if-eqz v29, :cond_e

    .line 3429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    move-object/from16 v29, v0

    invoke-static/range {v29 .. v29}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$3708(Lcom/microsoft/xbox/service/model/ProfileModel;)I

    .line 3431
    :cond_e
    iget-boolean v0, v3, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;->isNew:Z

    move/from16 v29, v0

    if-eqz v29, :cond_d

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v29

    if-eqz v29, :cond_d

    .line 3432
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->getAlertActionId(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)Ljava/lang/String;

    move-result-object v14

    goto :goto_3

    .line 3436
    .end local v3    # "action":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;
    .end local v4    # "actionXuid":Ljava/lang/String;
    .end local v25    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :cond_f
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v28

    if-nez v28, :cond_10

    if-eqz v5, :cond_10

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->resetReadCount:Z

    move/from16 v28, v0

    if-eqz v28, :cond_10

    .line 3438
    :try_start_0
    new-instance v19, Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;

    move-object/from16 v0, v19

    invoke-direct {v0, v14}, Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;-><init>(Ljava/lang/String;)V

    .line 3439
    .local v19, "request":Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;
    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;->getNewCommentAlertRequestBody(Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->resetNewCommentAlert(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3449
    .end local v6    # "currentNewFollowerCount":I
    .end local v14    # "lastSeenId":Ljava/lang/String;
    .end local v17    # "profileUserLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;>;"
    .end local v19    # "request":Lcom/microsoft/xbox/service/model/sls/ResetNewCommentAlertRequest;
    :cond_10
    :goto_4
    if-eqz v20, :cond_11

    if-eqz v10, :cond_11

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->resetReadCount:Z

    move/from16 v28, v0

    if-eqz v28, :cond_11

    .line 3451
    :try_start_1
    new-instance v19, Lcom/microsoft/xbox/service/model/sls/ResetNewFollowerAlertRequest;

    iget-object v0, v10, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;->watermark:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/sls/ResetNewFollowerAlertRequest;-><init>(Ljava/lang/String;)V

    .line 3452
    .local v19, "request":Lcom/microsoft/xbox/service/model/sls/ResetNewFollowerAlertRequest;
    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/service/model/sls/ResetNewFollowerAlertRequest;->getNewFollowerAlertRequestBody(Lcom/microsoft/xbox/service/model/sls/ResetNewFollowerAlertRequest;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v23

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->resetNewFollowerAlert(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_1

    .line 3460
    .end local v19    # "request":Lcom/microsoft/xbox/service/model/sls/ResetNewFollowerAlertRequest;
    :cond_11
    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v11}, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->mergeAndCollapseActivityAlerts(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;Ljava/util/ArrayList;)[Ljava/lang/Object;

    move-result-object v28

    .line 3463
    .end local v5    # "commentAlerts":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    .end local v9    # "followedLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;>;"
    .end local v10    # "followerAlerts":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    .end local v11    # "followerData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v20    # "resetFollowerNewAlert":Z
    .end local v21    # "response":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .end local v23    # "slsService":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    .end local v27    # "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_6
    return-object v28

    .line 3440
    .restart local v5    # "commentAlerts":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    .restart local v6    # "currentNewFollowerCount":I
    .restart local v9    # "followedLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;>;"
    .restart local v10    # "followerAlerts":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    .restart local v11    # "followerData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .restart local v14    # "lastSeenId":Ljava/lang/String;
    .restart local v17    # "profileUserLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;>;"
    .restart local v20    # "resetFollowerNewAlert":Z
    .restart local v21    # "response":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .restart local v23    # "slsService":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    .restart local v27    # "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v7

    .line 3443
    .local v7, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v28, Lcom/microsoft/xbox/service/model/ProfileModel;->TAG:Ljava/lang/String;

    const-string v29, "Error in resetting new comment alerts"

    invoke-static/range {v28 .. v29}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 3453
    .end local v6    # "currentNewFollowerCount":I
    .end local v7    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v14    # "lastSeenId":Ljava/lang/String;
    .end local v17    # "profileUserLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;>;"
    :catch_1
    move-exception v7

    .line 3456
    .restart local v7    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v28, Lcom/microsoft/xbox/service/model/ProfileModel;->TAG:Ljava/lang/String;

    const-string v29, "Error in resetting new follower alert"

    invoke-static/range {v28 .. v29}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 3463
    .end local v5    # "commentAlerts":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    .end local v7    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v9    # "followedLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IFollowersResult$Follower;>;"
    .end local v10    # "followerAlerts":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    .end local v11    # "followerData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v20    # "resetFollowerNewAlert":Z
    .end local v21    # "response":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .end local v23    # "slsService":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    .end local v27    # "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_12
    const/16 v28, 0x0

    goto :goto_6
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 3606
    const-wide/16 v0, 0xc86

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<[",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3600
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<[Ljava/lang/Object;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetCommentActivityAlertsRunner;->resetReadCount:Z

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$3900(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V

    .line 3601
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 3596
    return-void
.end method
