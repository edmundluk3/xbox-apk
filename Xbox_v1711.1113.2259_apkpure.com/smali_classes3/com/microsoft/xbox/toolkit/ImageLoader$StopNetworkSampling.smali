.class final Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Lcom/bumptech/glide/request/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StopNetworkSampling"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/bumptech/glide/request/RequestListener",
        "<TT;TR;>;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 155
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;, "Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling<TT;TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ImageLoader$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ImageLoader$1;

    .prologue
    .line 155
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;, "Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling<TT;TR;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;-><init>()V

    return-void
.end method


# virtual methods
.method public onException(Ljava/lang/Exception;Ljava/lang/Object;Lcom/bumptech/glide/request/target/Target;Z)Z
    .locals 1
    .param p1, "e"    # Ljava/lang/Exception;
    .param p4, "isFirstResource"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Exception;",
            "TT;",
            "Lcom/bumptech/glide/request/target/Target",
            "<TR;>;Z)Z"
        }
    .end annotation

    .prologue
    .line 158
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;, "Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling<TT;TR;>;"
    .local p2, "model":Ljava/lang/Object;, "TT;"
    .local p3, "target":Lcom/bumptech/glide/request/target/Target;, "Lcom/bumptech/glide/request/target/Target<TR;>;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->stopSampling()V

    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public onResourceReady(Ljava/lang/Object;Ljava/lang/Object;Lcom/bumptech/glide/request/target/Target;ZZ)Z
    .locals 1
    .param p4, "isFromMemoryCache"    # Z
    .param p5, "isFirstResource"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TT;",
            "Lcom/bumptech/glide/request/target/Target",
            "<TR;>;ZZ)Z"
        }
    .end annotation

    .prologue
    .line 164
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling;, "Lcom/microsoft/xbox/toolkit/ImageLoader$StopNetworkSampling<TT;TR;>;"
    .local p1, "resource":Ljava/lang/Object;, "TR;"
    .local p2, "model":Ljava/lang/Object;, "TT;"
    .local p3, "target":Lcom/bumptech/glide/request/target/Target;, "Lcom/bumptech/glide/request/target/Target<TR;>;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->stopSampling()V

    .line 165
    const/4 v0, 0x0

    return v0
.end method
