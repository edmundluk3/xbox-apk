.class public Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;
.super Ljava/lang/Object;
.source "ActivityFeedFilterRepositoryImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final PREFS_KEY:Ljava/lang/String; = "af_filter_prefs"


# instance fields
.field private final dataMapper:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;

.field private final filterPrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;"
        }
    .end annotation
.end field

.field private final rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private final telemetryService:Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;)V
    .locals 3
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "dataMapper"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;
    .param p3, "telemetryService"    # Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->filterPrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 33
    invoke-static {p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->create(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 35
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;

    .line 36
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->rxSharedPreferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    const-string v1, "af_filter_prefs"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    .line 39
    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->filterPrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 42
    return-void
.end method


# virtual methods
.method public getPrefs()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->filterPrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public setPrefs(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lio/reactivex/Completable;
    .locals 3
    .param p1, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 51
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 53
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->telemetryService:Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->filterPrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    invoke-virtual {v1, v0, p1}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;->trackApplyFeedFilter(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)V

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 56
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "af_filter_prefs"

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl;->dataMapper:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;

    .line 57
    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;->toString(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 58
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 60
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method
