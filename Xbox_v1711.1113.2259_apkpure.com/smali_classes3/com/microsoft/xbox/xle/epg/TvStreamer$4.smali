.class Lcom/microsoft/xbox/xle/epg/TvStreamer$4;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;-><init>(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$4;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 217
    const-string v0, "TvStreamer"

    const-string v1, "MTC: Skip back pressed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$4;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$000(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    move-result-object v0

    const/4 v1, -0x7

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->seek(I)V

    .line 219
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Stream FullScreen SkipBack"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 220
    return-void
.end method
