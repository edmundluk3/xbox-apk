.class public interface abstract Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$IAdapter;
.super Ljava/lang/Object;
.source "VirtualGrid.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IAdapter"
.end annotation


# virtual methods
.method public abstract getBottomView(ILandroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract getHeaderRowCount()I
.end method

.method public abstract getHeightSpec(I)I
.end method

.method public abstract getRowCount()I
.end method

.method public abstract getTitleView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract getViewIterator(IIZ)Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;
.end method

.method public abstract reclaimView(Landroid/view/View;)V
.end method

.method public abstract saveStartingRow(I)V
.end method
