.class public final Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;
.super Ljava/lang/Object;
.source "LeaderboardsDataTypes.java"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PagingInfo"
.end annotation


# instance fields
.field private final continuationToken:Ljava/lang/Long;

.field private volatile transient hashCode:I


# direct methods
.method public constructor <init>(Ljava/lang/Long;)V
    .locals 0
    .param p1, "continuationToken"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    iput-object p1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->continuationToken:Ljava/lang/Long;

    .line 214
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;)Ljava/lang/Long;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->continuationToken:Ljava/lang/Long;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 218
    if-ne p1, p0, :cond_0

    .line 219
    const/4 v1, 0x1

    .line 224
    :goto_0
    return v1

    .line 220
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;

    if-nez v1, :cond_1

    .line 221
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 223
    check-cast v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;

    .line 224
    .local v0, "other":Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;
    iget-object v1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->continuationToken:Ljava/lang/Long;

    iget-object v2, v0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->continuationToken:Ljava/lang/Long;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 230
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->hashCode:I

    if-nez v0, :cond_0

    .line 231
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->hashCode:I

    .line 232
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->continuationToken:Ljava/lang/Long;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->hashCode:I

    .line 235
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$PagingInfo;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
