.class final synthetic Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final arg$1:Ljava/lang/String;

.field private final arg$2:Ljava/util/List;

.field private final arg$3:Ljava/lang/String;

.field private final arg$4:Ljava/util/List;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;->arg$1:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;->arg$2:Ljava/util/List;

    iput-object p3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;->arg$3:Ljava/lang/String;

    iput-object p4, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;->arg$4:Ljava/util/List;

    return-void
.end method

.method public static lambdaFactory$(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;->arg$1:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;->arg$2:Ljava/util/List;

    iget-object v2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;->arg$3:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService$$Lambda$11;->arg$4:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->lambda$createSkypeEndpointForLongPoll$10(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method
