.class Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;
.super Ljava/lang/Object;
.source "XBoxOneCompanionSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "XboxOneEventSimulator"
.end annotation


# instance fields
.field private currentTitleIndex:I

.field private final delayBetweenTitleCycles:J

.field private final mediaStates:[[Ljava/lang/String;

.field private final mediaTypes:[Lcom/microsoft/xbox/smartglass/MediaType;

.field private final musicMediaAssetIds:[Ljava/lang/String;

.field private started:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

.field private timer:Ljava/util/Timer;

.field private final titleIds:[I

.field private final videoMediaAssetIds:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 437
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383
    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->delayBetweenTitleCycles:J

    .line 401
    new-array v0, v7, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->titleIds:[I

    .line 408
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "3d45210f-0a18-46cc-a3fe-5a1598207976"

    aput-object v1, v0, v3

    const-string v1, "d9b4b7d4-3105-41bf-982d-cc306c817e78"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->videoMediaAssetIds:[Ljava/lang/String;

    .line 413
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "B7C41701-0100-11DB-89CA-0019B92A3933"

    aput-object v1, v0, v3

    const-string v1, "BFC41701-0100-11DB-89CA-0019B92A3933"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->musicMediaAssetIds:[Ljava/lang/String;

    .line 418
    new-array v0, v7, [[Ljava/lang/String;

    aput-object v6, v0, v3

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->videoMediaAssetIds:[Ljava/lang/String;

    aput-object v1, v0, v4

    aput-object v6, v0, v5

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->musicMediaAssetIds:[Ljava/lang/String;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->mediaStates:[[Ljava/lang/String;

    .line 425
    new-array v0, v7, [Lcom/microsoft/xbox/smartglass/MediaType;

    aput-object v6, v0, v3

    sget-object v1, Lcom/microsoft/xbox/smartglass/MediaType;->Video:Lcom/microsoft/xbox/smartglass/MediaType;

    aput-object v1, v0, v4

    aput-object v6, v0, v5

    const/4 v1, 0x3

    sget-object v2, Lcom/microsoft/xbox/smartglass/MediaType;->Music:Lcom/microsoft/xbox/smartglass/MediaType;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->mediaTypes:[Lcom/microsoft/xbox/smartglass/MediaType;

    .line 434
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->currentTitleIndex:I

    .line 435
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->started:Z

    .line 438
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->timer:Ljava/util/Timer;

    .line 439
    return-void

    .line 401
    :array_0
    .array-data 4
        0x2a992d3a
        0x3d705025
        0x22c59613
        0x18ffc9f4
    .end array-data
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    .prologue
    .line 382
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->currentTitleIndex:I

    return v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;
    .param p1, "x1"    # I

    .prologue
    .line 382
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->currentTitleIndex:I

    return p1
.end method

.method static synthetic access$208(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;)I
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    .prologue
    .line 382
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->currentTitleIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->currentTitleIndex:I

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;)[I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->titleIds:[I

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;)[Lcom/microsoft/xbox/smartglass/MediaType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->mediaTypes:[Lcom/microsoft/xbox/smartglass/MediaType;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;)[[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->mediaStates:[[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->timer:Ljava/util/Timer;

    return-object v0
.end method


# virtual methods
.method public isStarted()Z
    .locals 1

    .prologue
    .line 514
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->started:Z

    return v0
.end method

.method public start()V
    .locals 6

    .prologue
    .line 442
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator$1;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;)V

    const-wide/16 v2, 0x7d0

    const-wide/16 v4, 0x7530

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 505
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->started:Z

    .line 506
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->started:Z

    .line 511
    return-void
.end method
