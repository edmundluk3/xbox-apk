.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;
.super Ljava/lang/Object;
.source "ContentRatingsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContentRating"
.end annotation


# instance fields
.field public final age:I
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x0L
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Age"
    .end annotation
.end field

.field private final alternateIds:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AlternateIds"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;",
            ">;"
        }
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final isExpired:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IsExpired"
    .end annotation
.end field

.field private final localizedProperties:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LocalizedProperties"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/util/List;ZLjava/util/List;)V
    .locals 4
    .param p1, "age"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "isExpired"    # Z
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;",
            ">;Z",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 440
    .local p2, "alternateIds":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;>;"
    .local p4, "localizedProperties":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441
    const-wide/16 v0, 0x0

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 442
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 443
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 445
    iput p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->age:I

    .line 446
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->alternateIds:Ljava/util/List;

    .line 447
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->isExpired:Z

    .line 448
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->localizedProperties:Ljava/util/List;

    .line 449
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 463
    if-ne p1, p0, :cond_1

    .line 469
    :cond_0
    :goto_0
    return v1

    .line 465
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;

    if-nez v3, :cond_2

    move v1, v2

    .line 466
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 468
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;

    .line 469
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;
    iget v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->age:I

    iget v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->age:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->alternateIds:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->alternateIds:Ljava/util/List;

    .line 470
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->isExpired:Z

    iget-boolean v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->isExpired:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->localizedProperties:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->localizedProperties:Ljava/util/List;

    .line 472
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getAlternateIds()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 453
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->alternateIds:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getLocalizedProperties()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 458
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->localizedProperties:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 478
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    if-nez v0, :cond_0

    .line 479
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    .line 480
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->age:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    .line 481
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->alternateIds:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    .line 482
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->isExpired:Z

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    .line 483
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->localizedProperties:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    .line 486
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 491
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
