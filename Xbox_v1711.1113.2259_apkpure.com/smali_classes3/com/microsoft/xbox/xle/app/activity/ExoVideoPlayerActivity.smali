.class public Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ExoVideoPlayerActivity.java"


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(I)V

    .line 10
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;->TAG:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public isAnimateOnPop()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public isAnimateOnPush()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 18
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 19
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;->onCreateContentView()V

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ExoVideoPlayerActivityViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 21
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Game DVR Start"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->beginTrackPerformance(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public onCreateContentView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    const v0, 0x7f0300fe

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;->setContentView(I)V

    .line 33
    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;->setAppBarLayout(IZZ)V

    .line 34
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;->setShowUtilityBar(Z)V

    .line 35
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/ExoVideoPlayerActivity;->setShowRightPane(Z)V

    .line 36
    return-void
.end method
