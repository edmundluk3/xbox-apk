.class Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;
.super Ljava/lang/Object;
.source "MediaProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/MediaProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "HLSMasterPlaylistInformation"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final mMasterPlaylistBody:Ljava/lang/String;

.field private final mStreamPlaylistRelativeURLs:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const-class v0, Lcom/microsoft/playready/MediaProxy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;[I[Ljava/lang/String;)V
    .locals 4
    .param p1, "masterPlaylistBody"    # Ljava/lang/String;
    .param p2, "streamIDs"    # [I
    .param p3, "streamPlaylistRelativeURLs"    # [Ljava/lang/String;

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;->mStreamPlaylistRelativeURLs:Ljava/util/Map;

    .line 117
    iput-object p1, p0, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;->mMasterPlaylistBody:Ljava/lang/String;

    .line 118
    sget-boolean v1, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    array-length v1, p2

    array-length v2, p3

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 120
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-lt v0, v1, :cond_1

    .line 123
    return-void

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;->mStreamPlaylistRelativeURLs:Ljava/util/Map;

    aget v2, p2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aget-object v3, p3, v0

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getMasterPlaylistBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;->mMasterPlaylistBody:Ljava/lang/String;

    return-object v0
.end method

.method public getStreamPlaylistMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy$HLSMasterPlaylistInformation;->mStreamPlaylistRelativeURLs:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
