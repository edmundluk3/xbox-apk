.class public abstract Lcom/microsoft/xbox/smartglass/Message;
.super Ljava/lang/Object;
.source "Message.java"


# instance fields
.field public final target:Lcom/microsoft/xbox/smartglass/MessageTarget;

.field public final type:Lcom/microsoft/xbox/smartglass/MessageType;


# direct methods
.method constructor <init>(III)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "titleId"    # I
    .param p3, "service"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/MessageType;->fromInt(I)Lcom/microsoft/xbox/smartglass/MessageType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/Message;->type:Lcom/microsoft/xbox/smartglass/MessageType;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-direct {v0, p2, p3}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/Message;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    .line 17
    return-void
.end method
