.class public Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "MultiplayerSessionModelManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;,
        Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;,
        Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;,
        Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;,
        Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field public static INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager; = null
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private static final MAX_MODEL_COUNT:I = 0xff

.field private static final SEARCH_SCID:Ljava/lang/String; = "SEARCH"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field editorialService:Lcom/microsoft/xbox/data/service/editorial/EditorialService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private isModifyingSession:Z

.field private lfgLanguageList:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

.field multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private sessions:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    .line 87
    const-class v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 102
    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->sessions:Landroid/support/v4/util/LruCache;

    .line 103
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;)V

    .line 104
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;Lcom/microsoft/xbox/service/model/UpdateType;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    .param p3, "x3"    # Lcom/microsoft/xbox/service/model/UpdateType;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->onUpdateLfgSessionCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;Lcom/microsoft/xbox/service/model/UpdateType;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->onGetLfgListSessionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->onGetLfgSessionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->lfgLanguageList:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    return-object p1
.end method

.method private clearSearchSessions()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->sessions:Landroid/support/v4/util/LruCache;

    const-string v1, "SEARCH"

    invoke-virtual {v0, v1}, Landroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    return-void
.end method

.method private getSessionHub(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;
    .locals 8
    .param p1, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;"
        }
    .end annotation

    .prologue
    .line 110
    .local p3, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 115
    if-nez p2, :cond_0

    if-eqz p3, :cond_3

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 116
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 117
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 118
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 119
    const-string v7, "SEARCH"

    .line 128
    .local v7, "sessionKey":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->sessions:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, v7}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    .line 129
    .local v0, "sessionHub":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;
    if-nez v0, :cond_2

    .line 130
    move-object v2, p1

    .line 131
    .local v2, "validServiceConfigId":Ljava/lang/String;
    const/4 v5, 0x0

    .line 133
    .local v5, "listType":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 135
    const/4 v2, 0x0

    .line 136
    sget-object v5, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 149
    :cond_1
    :goto_1
    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    .end local v0    # "sessionHub":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;
    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;-><init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 150
    .restart local v0    # "sessionHub":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->sessions:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, v7, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    .end local v2    # "validServiceConfigId":Ljava/lang/String;
    .end local v5    # "listType":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
    :cond_2
    return-object v0

    .line 121
    .end local v0    # "sessionHub":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;
    .end local v7    # "sessionKey":Ljava/lang/String;
    :cond_3
    const/16 v6, 0x11

    .line 122
    .local v6, "hashCode":I
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v6, v1, 0x20f

    .line 123
    mul-int/lit8 v1, v6, 0x1f

    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v3

    add-int v6, v1, v3

    .line 124
    mul-int/lit8 v1, v6, 0x1f

    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v3

    add-int v6, v1, v3

    .line 125
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "sessionKey":Ljava/lang/String;
    goto :goto_0

    .line 137
    .end local v6    # "hashCode":I
    .restart local v0    # "sessionHub":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;
    .restart local v2    # "validServiceConfigId":Ljava/lang/String;
    .restart local v5    # "listType":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
    :cond_4
    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 139
    const/4 v2, 0x0

    .line 140
    sget-object v5, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    goto :goto_1

    .line 141
    :cond_5
    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 145
    const/4 v2, 0x0

    .line 146
    sget-object v5, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    goto :goto_1
.end method

.method static synthetic lambda$createLfgSession$0(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)Lio/reactivex/SingleSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "scid"    # Ljava/lang/String;
    .param p2, "sessionName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "tags"    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "locale"    # Ljava/lang/String;
    .param p5, "session"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 330
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    .line 331
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;

    move-result-object v1

    const-string v2, "global(lfg)"

    .line 332
    invoke-static {p1, v2, p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;->sessionRef(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleType;->Search:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleType;

    .line 333
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;->type(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleType;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;

    move-result-object v1

    .line 334
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes$Builder;

    move-result-object v2

    .line 335
    invoke-virtual {v2, p3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes$Builder;->withSocialTags(Ljava/util/List;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes$Builder;

    move-result-object v2

    .line 336
    invoke-virtual {v2, p4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes$Builder;->locale(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes$Builder;

    move-result-object v2

    .line 337
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v2

    .line 334
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;->searchAttributes(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;

    move-result-object v1

    .line 338
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v1

    .line 330
    invoke-interface {v0, v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->createMutliplayerHandle(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private onGetLfgListSessionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "listType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;>;",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 364
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 365
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 366
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGetLfgListSessionsCompleted for listType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 369
    return-void
.end method

.method private onGetLfgSessionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 358
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 360
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->GetLfgSessions:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 361
    return-void
.end method

.method private declared-synchronized onUpdateLfgSessionCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;Lcom/microsoft/xbox/service/model/UpdateType;)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "updatedSession"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "type"    # Lcom/microsoft/xbox/service/model/UpdateType;

    .prologue
    .line 394
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->isModifyingSession:Z

    .line 395
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p3, v2, v3, p2}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;Ljava/lang/Object;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2, p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    monitor-exit p0

    return-void

    .line 394
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized approveLfgMember(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "handleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "memberIndex"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 372
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 373
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 375
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->isModifyingSession:Z

    if-nez v1, :cond_0

    .line 376
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->isModifyingSession:Z

    .line 377
    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;-><init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    .local v0, "task":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;->load(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 380
    .end local v0    # "task":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$ApproveLfgRequestTask;
    :cond_0
    monitor-exit p0

    return-void

    .line 372
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public createLfgSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/Date;Ljava/util/List;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;Ljava/lang/Long;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 7
    .param p1, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "needCount"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "startTime"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "visibilityPermission"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p7, "clubId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "locale"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/util/Date;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    .local p5, "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 295
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 296
    invoke-static {p8}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 297
    sget-object v3, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    const-string v4, "createLfgSession"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 300
    .local v1, "sessionName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 301
    .local v0, "sessionDescription":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 302
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v0

    .line 306
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v3

    .line 307
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;

    move-result-object v4

    .line 308
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;->withXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;

    move-result-object v4

    .line 309
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v4

    .line 307
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->withSelf(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v4

    .line 310
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v5

    .line 311
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v6

    if-eqz p7, :cond_1

    sget-object v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;->XboxLive:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    if-eq p6, v3, :cond_1

    sget-object v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;->Club:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;

    .line 312
    :goto_0
    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;->joinRestriction(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v6

    if-eqz p7, :cond_2

    sget-object v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;->XboxLive:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    if-eq p6, v3, :cond_2

    sget-object v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;->Club:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;

    .line 313
    :goto_1
    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;->readRestriction(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v3

    .line 314
    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;->description(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v3

    .line 315
    invoke-virtual {v3, p6}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;->searchHandleVisibility(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v3

    .line 316
    invoke-virtual {v3, p4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;->scheduledTime(Ljava/util/Date;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v6

    if-eqz p7, :cond_3

    .line 317
    invoke-virtual {p7}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;->clubId(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;

    move-result-object v3

    .line 318
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem;

    move-result-object v3

    .line 311
    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties$Builder;->system(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionPropertiesSystem;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties$Builder;

    move-result-object v3

    .line 320
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;

    move-result-object v3

    .line 310
    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->properties(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionProperties;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v4

    if-eqz p3, :cond_4

    .line 321
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lez v3, :cond_4

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;->with(I)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;

    move-result-object v3

    :goto_3
    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->roleTypes(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RoleInfo;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v3

    .line 322
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    move-result-object v2

    .line 324
    .local v2, "sessionTemplate":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    const-string v4, "global(lfg)"

    invoke-interface {v3, p1, v4, v1, v2}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->createOrUpdateMultiplayerSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)Lio/reactivex/Single;

    move-result-object v3

    invoke-static {p0, p1, v1, p5, p8}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/functions/Function;

    move-result-object v4

    .line 329
    invoke-virtual {v3, v4}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v3

    .line 324
    return-object v3

    .line 311
    .end local v2    # "sessionTemplate":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;->Followed:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;

    goto :goto_0

    .line 312
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;->Followed:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;

    goto :goto_1

    .line 317
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 321
    :cond_4
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public declared-synchronized declineLfgMember(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "handleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "memberIndex"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 383
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 384
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 386
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->isModifyingSession:Z

    if-nez v1, :cond_0

    .line 387
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->isModifyingSession:Z

    .line 388
    new-instance v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;-><init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    .local v0, "task":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;->load(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    .end local v0    # "task":Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$DeclineLfgRequestTask;
    :cond_0
    monitor-exit p0

    return-void

    .line 383
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getIsModifyingSession()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->isModifyingSession:Z

    return v0
.end method

.method public getLfgLanguages()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 166
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->lfgLanguageList:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    if-nez v2, :cond_0

    .line 167
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v1, "languagesList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;>;"
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0706d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "all"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-static {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->with(Ljava/util/List;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    move-result-object v0

    .line 171
    .local v0, "languages":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;
    new-instance v2, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;-><init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$FetchLfgLanguagesTask;->load(Z)V

    .line 174
    .end local v0    # "languages":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;
    .end local v1    # "languagesList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;>;"
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->lfgLanguageList:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    goto :goto_0
.end method

.method public getSessionsResult(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Ljava/util/List;
    .locals 4
    .param p1, "listType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 188
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSessionsResult "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3, v3}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionHub(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->getSessions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSessionsResult(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .param p1, "listType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    .local p2, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSessionsResult "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionHub(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->getSessions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSessionsResult(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)Ljava/util/List;
    .locals 1
    .param p1, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 183
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionHub(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->getSessions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized joinSession(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 4
    .param p1, "handleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 345
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 346
    invoke-static {}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->builder()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;

    move-result-object v2

    .line 347
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;->withXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;

    move-result-object v2

    .line 348
    invoke-virtual {v2, p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;->withDescription(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;

    move-result-object v2

    .line 349
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    move-result-object v2

    .line 346
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->withSelf(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;

    move-result-object v1

    .line 350
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession$Builder;->build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    move-result-object v0

    .line 352
    .local v0, "updatedSession":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    invoke-interface {v1, p1, v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->createOrUpdateMultiplayerSession(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)Lio/reactivex/Single;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit p0

    return-object v1

    .line 343
    .end local v0    # "updatedSession":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public loadLfgSessions(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2, "listType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 244
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 246
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v1, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessions(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadLfgSessions(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .param p2, "serviceConfigId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 237
    .local p4, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 239
    invoke-direct {p0, p2, p3, p4}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionHub(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V
    .locals 4
    .param p1, "forceRefresh"    # Z
    .param p2, "listType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 273
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadLfgSessionsAsync forceRefresh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " listType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 276
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v3, v3}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)V

    .line 277
    return-void
.end method

.method public loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;Ljava/util/List;)V
    .locals 3
    .param p1, "forceRefresh"    # Z
    .param p2, "listType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 280
    .local p3, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadLfgSessionsAsync forceRefresh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " listType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 283
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)V

    .line 284
    return-void
.end method

.method public loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)V
    .locals 1
    .param p1, "forceRefresh"    # Z
    .param p2, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 253
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)V

    .line 254
    return-void
.end method

.method public loadLfgSessionsAsync(ZLjava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)V
    .locals 3
    .param p1, "forceRefresh"    # Z
    .param p2, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 261
    .local p4, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadLfgSessionsAsync forceRefresh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " scid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 265
    if-eqz p1, :cond_1

    if-nez p3, :cond_0

    if-eqz p4, :cond_1

    .line 266
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->clearSearchSessions()V

    .line 269
    :cond_1
    invoke-direct {p0, p2, p3, p4}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionHub(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->loadAsync(Z)V

    .line 270
    return-void
.end method

.method public shouldRefresh(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Z
    .locals 4
    .param p1, "listType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 218
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "shouldRefresh "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v3, v3}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->shouldRefresh(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public shouldRefresh(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;Ljava/util/List;)Z
    .locals 3
    .param p1, "listType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 225
    .local p2, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "shouldRefresh "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->shouldRefresh(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public shouldRefresh(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;)Z
    .locals 1
    .param p1, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 205
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->shouldRefresh(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public shouldRefresh(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Z
    .locals 1
    .param p1, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 212
    .local p3, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 214
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionHub(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;)Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method
