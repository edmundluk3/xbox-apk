.class public Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
.super Ljava/lang/Object;
.source "NavigationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;,
        Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;,
        Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerHolder;,
        Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;,
        Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;,
        Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

.field authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final callAfterAnimation:Ljava/lang/Runnable;

.field private cannotNavigateTripwire:Z

.field private currentAnimation:Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

.field private goingBack:Z

.field homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final navigationListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final navigationParameters:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            ">;"
        }
    .end annotation
.end field

.field private final navigationStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field

.field private pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

.field private transitionAnimate:Z

.field private transitionLambda:Ljava/lang/Runnable;

.field tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v2, Ljava/util/Stack;

    invoke-direct {v2}, Ljava/util/Stack;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationParameters:Ljava/util/Stack;

    .line 67
    new-instance v2, Ljava/util/Stack;

    invoke-direct {v2}, Ljava/util/Stack;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    .line 68
    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->currentAnimation:Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    .line 69
    sget-object v2, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->NONE:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    .line 70
    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->transitionLambda:Ljava/lang/Runnable;

    .line 71
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->goingBack:Z

    .line 72
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->transitionAnimate:Z

    .line 73
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->cannotNavigateTripwire:Z

    .line 860
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->callAfterAnimation:Ljava/lang/Runnable;

    .line 88
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v2

    invoke-interface {v2, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V

    .line 89
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationListeners:Ljava/util/List;

    .line 90
    const-string v2, "You must access navigation manager on UI thread."

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v3, v4, :cond_0

    :goto_0
    invoke-static {v2, v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 91
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    const-string v1, "Create a new instance of navigation manager"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 95
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 96
    return-void

    :cond_0
    move v0, v1

    .line 90
    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;-><init>()V

    return-void
.end method

.method private OnAnimationEnd()V
    .locals 4

    .prologue
    .line 863
    sget-object v2, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;->$SwitchMap$com$microsoft$xbox$toolkit$ui$NavigationManager$NavigationManagerAnimationState:[I

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 890
    :cond_0
    :goto_0
    return-void

    .line 865
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 866
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_1

    .line 867
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->setAnimationBlocking(Z)V

    .line 870
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->NONE:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    .line 872
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 873
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onAnimateInCompleted()V

    goto :goto_0

    .line 878
    .end local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->transitionLambda:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 881
    const/4 v1, 0x0

    .line 882
    .local v1, "anim":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 883
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->goingBack:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getAnimateIn(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    move-result-object v1

    .line 886
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ANIMATING_IN:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->startAnimation(Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;)V

    goto :goto_0

    .line 863
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private PopScreensAndReplaceImmediate(ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 3
    .param p1, "popCount"    # I
    .param p2, "newScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p3, "animate"    # Z
    .param p4, "goingBack"    # Z
    .param p5, "isRestart"    # Z
    .param p6, "screenParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 593
    if-eqz p5, :cond_0

    .line 594
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p6, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$RestartRunner;-><init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;)V

    .line 681
    .local v0, "popAndReplaceRunnable":Ljava/lang/Runnable;
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;->$SwitchMap$com$microsoft$xbox$toolkit$ui$NavigationManager$NavigationManagerAnimationState:[I

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 699
    invoke-direct {p0, p4, v0, p3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->ReplaceOnAnimationEnd(ZLjava/lang/Runnable;Z)V

    .line 702
    :goto_1
    return-void

    .line 596
    .end local v0    # "popAndReplaceRunnable":Ljava/lang/Runnable;
    :cond_0
    invoke-static {p0, p6, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Ljava/lang/Runnable;

    move-result-object v0

    .restart local v0    # "popAndReplaceRunnable":Ljava/lang/Runnable;
    goto :goto_0

    .line 684
    :pswitch_0
    invoke-direct {p0, p4, v0, p3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->Transition(ZLjava/lang/Runnable;Z)V

    goto :goto_1

    .line 681
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private PopScreensAndReplaceWithPendingParams()V
    .locals 7

    .prologue
    .line 580
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .line 581
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->access$200(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)I

    move-result v1

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .line 582
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->access$300(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .line 583
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->access$400(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Z

    move-result v3

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .line 584
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->access$500(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Z

    move-result v4

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .line 585
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->access$600(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Z

    move-result v5

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .line 586
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->access$700(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v6

    move-object v0, p0

    .line 580
    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplaceImmediate(ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 588
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .line 589
    return-void
.end method

.method private ReplaceOnAnimationEnd(ZLjava/lang/Runnable;Z)V
    .locals 2
    .param p1, "goingBack"    # Z
    .param p2, "lambda"    # Ljava/lang/Runnable;
    .param p3, "animate"    # Z

    .prologue
    .line 852
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ANIMATING_OUT:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ANIMATING_IN:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 853
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ANIMATING_OUT:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    .line 855
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->transitionLambda:Ljava/lang/Runnable;

    .line 856
    iput-boolean p3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->transitionAnimate:Z

    .line 857
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->goingBack:Z

    .line 858
    return-void

    .line 852
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Size()I
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    return v0
.end method

.method private Transition(ZLjava/lang/Runnable;Z)V
    .locals 2
    .param p1, "goingBack"    # Z
    .param p2, "lambda"    # Ljava/lang/Runnable;
    .param p3, "animate"    # Z

    .prologue
    .line 841
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->transitionLambda:Ljava/lang/Runnable;

    .line 842
    iput-boolean p3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->transitionAnimate:Z

    .line 843
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->goingBack:Z

    .line 846
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->currentAnimation:Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    .line 848
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->currentAnimation:Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ANIMATING_OUT:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->startAnimation(Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;)V

    .line 849
    return-void

    .line 846
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getAnimateOut(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/util/Stack;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationParameters:Ljava/util/Stack;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$902(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->cannotNavigateTripwire:Z

    return p1
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->OnAnimationEnd()V

    return-void
.end method

.method private getHomeScreenClass(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)Ljava/lang/Class;
    .locals 2
    .param p1, "homeScreenPreference"    # Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 784
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;->$SwitchMap$com$microsoft$xbox$data$repository$homescreen$HomeScreenPreference:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 801
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected HomeScreenPreference: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->error(Ljava/lang/String;)V

    .line 802
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 786
    :pswitch_0
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ActivityFeedScreen;

    goto :goto_0

    .line 788
    :pswitch_1
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    goto :goto_0

    .line 790
    :pswitch_2
    const-class v0, Lcom/microsoft/xbox/xle/app/trending/TrendingPivotScreen;

    goto :goto_0

    .line 792
    :pswitch_3
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    goto :goto_0

    .line 794
    :pswitch_4
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;

    goto :goto_0

    .line 796
    :pswitch_5
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    goto :goto_0

    .line 798
    :pswitch_6
    const-class v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;

    goto :goto_0

    .line 784
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerHolder;->instance:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    return-object v0
.end method

.method static synthetic lambda$PopScreensAndReplaceImmediate$2(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 9
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p1, "screenParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .param p2, "popCount"    # I
    .param p3, "newScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v8, 0x0

    .line 597
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    .line 601
    .local v3, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 603
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->cannotNavigateTripwire:Z

    .line 604
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 605
    .local v0, "from":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putFromScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 606
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSourcePage(Ljava/lang/String;)V

    .line 608
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 609
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    .line 610
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    .line 611
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStop()V

    .line 615
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_2

    .line 616
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 617
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onDestroy()V

    .line 620
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->removeContentViewXLE(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 621
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationParameters:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 615
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 624
    :cond_2
    const/4 v4, 0x0

    .line 626
    .local v4, "to":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz p3, :cond_6

    .line 630
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isKeepPreviousScreen()Z

    move-result v5

    if-nez v5, :cond_3

    .line 631
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onTombstone()V

    .line 634
    :cond_3
    invoke-virtual {p3, v8}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setIsPivotPane(Z)V

    .line 635
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v5, p3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->addContentViewXLE(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 636
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationParameters:Ljava/util/Stack;

    invoke-virtual {v5, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 639
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreate()V

    .line 649
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 650
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    .line 651
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    .line 652
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    .line 653
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onAnimateInStarted()V

    .line 654
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v4

    .line 668
    :cond_5
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationListeners:Ljava/util/List;

    monitor-enter v6

    .line 669
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;

    .line 670
    .local v2, "listener":Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;
    invoke-interface {v2, v0, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;->onPageNavigated(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    goto :goto_2

    .line 672
    .end local v2    # "listener":Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 640
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 642
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->addContentViewXLE(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 644
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getIsTombstoned()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 645
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onRehydrate()V

    goto :goto_1

    .line 672
    :cond_7
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 675
    iput-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->cannotNavigateTripwire:Z

    .line 677
    .end local v0    # "from":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .end local v1    # "i":I
    .end local v4    # "to":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_8
    return-void
.end method

.method static synthetic lambda$navigateToHomeScreenOrOOBETutorial$3(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p1, "shouldShowTutorialExperience"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 737
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set welcome screen as home screen?: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    sget-object v1, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Tutorial:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->setActiveHomeScreenPreference(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V

    .line 741
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigateToHomeScreen()V

    .line 742
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 94
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPop(Ljava/lang/Class;)V

    return-void
.end method

.method private startAnimation(Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;)V
    .locals 2
    .param p1, "anim"    # Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "state"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    .prologue
    .line 893
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    .line 894
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->currentAnimation:Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    .line 896
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 897
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 898
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->setAnimationBlocking(Z)V

    .line 901
    :cond_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->transitionAnimate:Z

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 902
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->callAfterAnimation:Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;->setOnAnimationEndRunnable(Ljava/lang/Runnable;)V

    .line 903
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;->startAnimation()V

    .line 907
    :goto_0
    return-void

    .line 905
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->callAfterAnimation:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method


# virtual methods
.method public CountPopsToScreen(Ljava/lang/Class;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 232
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .line 233
    .local v0, "TOP_ELEM":I
    move v1, v0

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 234
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v3, v1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 235
    .local v2, "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 236
    sub-int v3, v0, v1

    .line 239
    .end local v2    # "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :goto_1
    return v3

    .line 233
    .restart local v2    # "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 239
    .end local v2    # "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_1
    const/4 v3, -0x1

    goto :goto_1
.end method

.method public GoBack()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 215
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreen()V

    .line 216
    return-void
.end method

.method public GotoScreenWithPop(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 10
    .param p1, "activityParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .param p3    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p2, "newTop":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    .local p3, "until":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 363
    const/4 v7, 0x0

    .line 364
    .local v7, "clsUntil":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    add-int/lit8 v8, v0, -0x1

    .line 365
    .local v8, "idxTop":I
    move v9, v8

    .line 366
    .local v9, "pos":I
    if-eqz p3, :cond_1

    .line 367
    :goto_0
    if-ltz v9, :cond_1

    .line 368
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v0, v9}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-ne p3, v0, :cond_0

    .line 369
    move-object v7, p3

    .line 371
    :cond_0
    add-int/lit8 v9, v9, -0x1

    goto :goto_0

    .line 374
    :cond_1
    if-eqz v7, :cond_4

    .line 376
    if-ne v7, p2, :cond_3

    .line 378
    if-ne v9, v8, :cond_2

    .line 379
    invoke-virtual {p0, p1, v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Z)V

    .line 391
    :goto_1
    return-void

    .line 381
    :cond_2
    sub-int v1, v8, v9

    const/4 v2, 0x0

    move-object v0, p0

    move v4, v3

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_1

    .line 385
    :cond_3
    sub-int v1, v8, v9

    move-object v0, p0

    move-object v2, p2

    move v4, v3

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_1

    .line 389
    :cond_4
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->Size()I

    move-result v1

    move-object v0, p0

    move-object v2, p2

    move v4, v3

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_1
.end method

.method public GotoScreenWithPop(Ljava/lang/Class;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 339
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->CountPopsToScreen(Ljava/lang/Class;)I

    move-result v1

    .line 340
    .local v1, "toPop":I
    if-lez v1, :cond_0

    .line 341
    const/4 v2, 0x0

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZ)V

    .line 349
    :goto_0
    return-void

    .line 342
    :cond_0
    if-gez v1, :cond_1

    .line 344
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->Size()I

    move-result v6

    move-object v5, p0

    move-object v7, p1

    move v8, v3

    move v9, v4

    move v10, v4

    invoke-virtual/range {v5 .. v10}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZ)V

    goto :goto_0

    .line 347
    :cond_1
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Z)V

    goto :goto_0
.end method

.method public GotoScreenWithPush(Ljava/lang/Class;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 466
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->CountPopsToScreen(Ljava/lang/Class;)I

    move-result v1

    .line 467
    .local v1, "toPop":I
    if-lez v1, :cond_0

    .line 468
    const/4 v2, 0x0

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZ)V

    .line 475
    :goto_0
    return-void

    .line 469
    :cond_0
    if-gez v1, :cond_1

    move-object v5, p0

    move v6, v4

    move-object v7, p1

    move v8, v3

    move v9, v4

    move v10, v4

    .line 471
    invoke-virtual/range {v5 .. v10}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZ)V

    goto :goto_0

    .line 473
    :cond_1
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Z)V

    goto :goto_0
.end method

.method public GotoScreenWithPush(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 12
    .param p2, "activityParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 487
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->CountPopsToScreen(Ljava/lang/Class;)I

    move-result v1

    .line 488
    .local v1, "toPop":I
    if-lez v1, :cond_0

    .line 489
    const/4 v2, 0x0

    move-object v0, p0

    move v5, v4

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 496
    :goto_0
    return-void

    .line 490
    :cond_0
    if-gez v1, :cond_1

    move-object v5, p0

    move v6, v4

    move-object v7, p1

    move v8, v3

    move v9, v4

    move v10, v4

    move-object v11, p2

    .line 492
    invoke-virtual/range {v5 .. v11}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 494
    :cond_1
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Z)V

    goto :goto_0
.end method

.method public IsScreenOnStack(Ljava/lang/Class;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 224
    .local v0, "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    const/4 v1, 0x1

    .line 228
    .end local v0    # "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public NavigateTo(Ljava/lang/Class;Z)V
    .locals 1
    .param p2, "addToStack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 192
    return-void
.end method

.method public NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 3
    .param p2, "addToStack"    # Z
    .param p3, "activityParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;Z",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            ")V"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    if-eqz p2, :cond_0

    .line 180
    :try_start_0
    invoke-virtual {p0, p1, p3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PushScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 187
    :goto_0
    return-void

    .line 182
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1, p1, p3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v1, "Failed to navigate"

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public OnBackButtonPressed()V
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onBackButtonPressed()V

    .line 198
    :cond_0
    return-void
.end method

.method public PopAllScreens()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 298
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->Size()I

    move-result v0

    if-lez v0, :cond_0

    .line 299
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->Size()I

    move-result v1

    const/4 v2, 0x0

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZ)V

    .line 301
    :cond_0
    return-void
.end method

.method public PopAllScreensAndReplace(Ljava/lang/Class;)V
    .locals 1
    .param p1    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 304
    .local p1, "target":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 305
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->Size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;)V

    .line 306
    return-void
.end method

.method public PopScreen()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 290
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreens(I)V

    .line 291
    return-void
.end method

.method public PopScreens(I)V
    .locals 1
    .param p1, "popCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 294
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;)V

    .line 295
    return-void
.end method

.method public PopScreensAndReplace(ILjava/lang/Class;)V
    .locals 1
    .param p1, "popCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 514
    .local p2, "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 515
    return-void
.end method

.method public PopScreensAndReplace(ILjava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 7
    .param p1, "popCount"    # I
    .param p3, "activityParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p2, "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v3, 0x1

    .line 509
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v4, v3

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 510
    return-void
.end method

.method public PopScreensAndReplace(ILjava/lang/Class;ZZ)V
    .locals 6
    .param p1, "popCount"    # I
    .param p3, "animate"    # Z
    .param p4, "isRestart"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;ZZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 518
    .local p2, "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v4, 0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZ)V

    .line 519
    return-void
.end method

.method public PopScreensAndReplace(ILjava/lang/Class;ZZZ)V
    .locals 7
    .param p1, "popCount"    # I
    .param p3, "animate"    # Z
    .param p4, "goingBack"    # Z
    .param p5, "isRestart"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;ZZZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 706
    .local p2, "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 707
    return-void
.end method

.method public PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 22
    .param p1, "popCount"    # I
    .param p2    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "animate"    # Z
    .param p4, "goingBack"    # Z
    .param p5, "isRestart"    # Z
    .param p6, "activityParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;ZZZ",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 523
    .local p2, "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const-string v3, "You must access navigation manager on UI thread."

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    sget-object v5, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v2, v5, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v3, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 524
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->cannotNavigateTripwire:Z

    if-eqz v2, :cond_1

    .line 525
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "NavigationManager: attempted to execute a recursive navigation in the OnStop/OnStart method.  This is forbidden."

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 523
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 530
    :cond_1
    if-eqz p2, :cond_2

    if-eqz p5, :cond_5

    .line 531
    :cond_2
    const/4 v4, 0x0

    .line 540
    .local v4, "newScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 541
    if-eqz p3, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isAnimateOnPop()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 p3, 0x1

    .line 545
    :cond_3
    :goto_2
    if-nez p6, :cond_8

    new-instance v8, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 547
    .local v8, "screenParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_3
    sget-object v3, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new screen "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v4, :cond_9

    const-string v2, "empty"

    :goto_4
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v21

    .line 550
    .local v21, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v21, :cond_4

    invoke-virtual/range {v21 .. v21}, Lcom/microsoft/xbox/xle/app/MainActivity;->isPaused()Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_4
    const/16 v18, 0x1

    .line 552
    .local v18, "appInBackground":Z
    :goto_5
    if-eqz v18, :cond_c

    if-eqz v4, :cond_c

    .line 553
    new-instance v19, Landroid/content/Intent;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-class v3, Lcom/microsoft/xbox/xle/app/MainActivity;

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 554
    .local v19, "bringAppToForeground":Landroid/content/Intent;
    const-string v2, "android.intent.action.MAIN"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 555
    const-string v2, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 556
    const/high16 v2, 0x10000000

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 558
    if-eqz v21, :cond_b

    .line 560
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivity(Landroid/content/Intent;)V

    move-object/from16 v2, p0

    move/from16 v3, p1

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    .line 561
    invoke-direct/range {v2 .. v8}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplaceImmediate(ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 577
    .end local v19    # "bringAppToForeground":Landroid/content/Intent;
    :goto_6
    return-void

    .line 533
    .end local v4    # "newScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .end local v8    # "screenParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v18    # "appInBackground":Z
    .end local v21    # "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :cond_5
    const/4 v2, 0x0

    :try_start_0
    new-array v2, v2, [Ljava/lang/Class;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 534
    .restart local v4    # "newScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz p3, :cond_6

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isAnimateOnPush()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_6

    const/16 p3, 0x1

    :goto_7
    goto/16 :goto_1

    :cond_6
    const/16 p3, 0x0

    goto :goto_7

    .line 536
    .end local v4    # "newScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :catch_0
    move-exception v20

    .line 537
    .local v20, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x13

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FIXME: Failed to create a screen of type "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-direct {v2, v6, v7, v3, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 541
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v4    # "newScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_7
    const/16 p3, 0x0

    goto/16 :goto_2

    :cond_8
    move-object/from16 v8, p6

    .line 545
    goto/16 :goto_3

    .line 547
    .restart local v8    # "screenParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_9
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 550
    .restart local v21    # "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :cond_a
    const/16 v18, 0x0

    goto/16 :goto_5

    .line 564
    .restart local v18    # "appInBackground":Z
    .restart local v19    # "bringAppToForeground":Landroid/content/Intent;
    :cond_b
    new-instance v9, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    const/16 v17, 0x0

    move-object/from16 v10, p0

    move/from16 v11, p1

    move-object v12, v4

    move/from16 v13, p3

    move/from16 v14, p4

    move/from16 v15, p5

    move-object/from16 v16, v8

    invoke-direct/range {v9 .. v17}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;-><init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .line 572
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/XLEApplication;->startActivity(Landroid/content/Intent;)V

    goto :goto_6

    .end local v19    # "bringAppToForeground":Landroid/content/Intent;
    :cond_c
    move-object/from16 v2, p0

    move/from16 v3, p1

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    .line 575
    invoke-direct/range {v2 .. v8}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplaceImmediate(ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto/16 :goto_6
.end method

.method public PopTillScreenThenPush(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 1
    .param p1    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 330
    .local p1, "target":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    .local p2, "newScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopTillScreenThenPush(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 331
    return-void
.end method

.method public PopTillScreenThenPush(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 11
    .param p1    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "target":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    .local p2, "newScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 315
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 316
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->CountPopsToScreen(Ljava/lang/Class;)I

    move-result v1

    .line 317
    .local v1, "toPop":I
    if-lez v1, :cond_0

    move-object v0, p0

    move-object v2, p2

    move v4, v3

    move-object v6, p3

    .line 318
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 327
    :goto_0
    return-void

    .line 319
    :cond_0
    if-gez v1, :cond_1

    .line 321
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "target screen does not exist "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, p0

    move-object v6, p2

    move v7, v3

    move v8, v5

    move v9, v5

    move-object v10, p3

    .line 322
    invoke-virtual/range {v4 .. v10}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    :cond_1
    move-object v4, p0

    move-object v6, p2

    move v7, v3

    move v8, v5

    move v9, v5

    move-object v10, p3

    .line 325
    invoke-virtual/range {v4 .. v10}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method public PushScreen(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 505
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PushScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 506
    return-void
.end method

.method public PushScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 7
    .param p2, "activityParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v1, 0x0

    .line 500
    const/4 v3, 0x1

    move-object v0, p0

    move-object v2, p1

    move v4, v1

    move v5, v1

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 501
    return-void
.end method

.method public RestartCurrentScreen(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Z)V
    .locals 8
    .param p1, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .param p2, "animate"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 269
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    const-string v3, "Restart current activity"

    invoke-static {v0, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v7

    .line 271
    .local v7, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v7, :cond_2

    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    sget-object v3, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ANIMATING_OUT:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    if-ne v0, v3, :cond_0

    .line 274
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->OnAnimationEnd()V

    .line 287
    :goto_0
    return-void

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    sget-object v3, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ANIMATING_IN:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    if-ne v0, v3, :cond_1

    .line 277
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->OnAnimationEnd()V

    .line 278
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .local v2, "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    move-object v0, p0

    move v3, p2

    move v4, v1

    move v5, v1

    move-object v6, p1

    .line 279
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 281
    .end local v2    # "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    :cond_1
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .restart local v2    # "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    move-object v0, p0

    move v3, p2

    move v4, v1

    move v5, v1

    move-object v6, p1

    .line 282
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 285
    .end local v2    # "newScreenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    const-string v1, "Attempted to restart current activity with no activity!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public RestartCurrentScreen(Z)V
    .locals 1
    .param p1, "animate"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 265
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Z)V

    .line 266
    return-void
.end method

.method public ShouldBackCloseApp()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 219
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->Size()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    sget-object v2, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->NONE:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public TEST_isAnimatingIn()Z
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ANIMATING_IN:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public TEST_isAnimatingOut()Z
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->ANIMATING_OUT:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addOnNavigatedListener(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 138
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationListeners:Ljava/util/List;

    monitor-enter v1

    .line 141
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    :cond_0
    monitor-exit v1

    .line 145
    return-void

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public bringAppToForeground()Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;
    .locals 5

    .prologue
    .line 710
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    .line 711
    .local v2, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->isPaused()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 713
    .local v0, "appInBackground":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 714
    new-instance v1, Landroid/content/Intent;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-class v4, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 715
    .local v1, "bringAppToForeground":Landroid/content/Intent;
    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 716
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 717
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 719
    if-eqz v2, :cond_2

    .line 721
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 722
    sget-object v3, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->BroughtToForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    .line 729
    .end local v1    # "bringAppToForeground":Landroid/content/Intent;
    :goto_1
    return-object v3

    .line 711
    .end local v0    # "appInBackground":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 724
    .restart local v0    # "appInBackground":Z
    .restart local v1    # "bringAppToForeground":Landroid/content/Intent;
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/XLEApplication;->startActivity(Landroid/content/Intent;)V

    .line 725
    sget-object v3, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->NewActivityStarted:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    goto :goto_1

    .line 729
    .end local v1    # "bringAppToForeground":Landroid/content/Intent;
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->AlreadyInForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    goto :goto_1
.end method

.method public countPopsToLowestScreen(Ljava/lang/Class;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 251
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 252
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, -0x1

    .line 257
    :goto_1
    return v1

    .line 251
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 257
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters(I)Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    return-object v0
.end method

.method public getActivityParameters(I)Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .locals 6
    .param p1, "depth"    # I

    .prologue
    .line 163
    sget-object v3, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getActivityParameters(depth: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "), this.navigationParameters: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationParameters:Ljava/util/Stack;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    if-ltz p1, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationParameters:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v3

    if-gt p1, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 165
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 167
    .local v1, "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationParameters:Ljava/util/Stack;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationParameters:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->size()I

    move-result v4

    sub-int/2addr v4, p1

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_1
    return-object v1

    .line 164
    .end local v1    # "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 168
    .restart local v1    # "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :catch_0
    move-exception v2

    .line 171
    .local v2, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget-object v3, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get ActivityParameters that is out of bound. depth: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationParameters:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "the stack is empty!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const/4 v0, 0x0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    goto :goto_0
.end method

.method public getCurrentActivityName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 121
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 122
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v1

    .line 125
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPreviousActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 131
    :cond_0
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    goto :goto_0
.end method

.method public gotoLowestScreenWithPop(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 10
    .param p1, "activityParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .param p3    # Ljava/lang/Class;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p2, "newTop":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    .local p3, "until":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 426
    const/4 v7, 0x0

    .line 427
    .local v7, "clsUntil":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    add-int/lit8 v8, v0, -0x1

    .line 428
    .local v8, "idxTop":I
    const/4 v9, 0x0

    .line 429
    .local v9, "pos":I
    if-eqz p3, :cond_1

    .line 430
    :goto_0
    if-gt v9, v8, :cond_1

    .line 431
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v0, v9}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-ne p3, v0, :cond_0

    .line 432
    move-object v7, p3

    .line 434
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 437
    :cond_1
    if-eqz v7, :cond_4

    .line 439
    if-ne v7, p2, :cond_3

    .line 441
    if-ne v9, v8, :cond_2

    .line 442
    invoke-virtual {p0, p1, v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Z)V

    .line 454
    :goto_1
    return-void

    .line 444
    :cond_2
    sub-int v1, v8, v9

    const/4 v2, 0x0

    move-object v0, p0

    move v4, v3

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_1

    .line 448
    :cond_3
    sub-int v1, v8, v9

    move-object v0, p0

    move-object v2, p2

    move v4, v3

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_1

    .line 452
    :cond_4
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->Size()I

    move-result v1

    move-object v0, p0

    move-object v2, p2

    move v4, v3

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_1
.end method

.method public gotoLowestScreenWithPop(Ljava/lang/Class;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 402
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->countPopsToLowestScreen(Ljava/lang/Class;)I

    move-result v1

    .line 403
    .local v1, "toPop":I
    if-lez v1, :cond_0

    .line 404
    const/4 v2, 0x0

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZ)V

    .line 412
    :goto_0
    return-void

    .line 405
    :cond_0
    if-gez v1, :cond_1

    .line 407
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->Size()I

    move-result v6

    move-object v5, p0

    move-object v7, p1

    move v8, v3

    move v9, v4

    move v10, v4

    invoke-virtual/range {v5 .. v10}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZ)V

    goto :goto_0

    .line 410
    :cond_1
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Z)V

    goto :goto_0
.end method

.method public isAnimating()Z
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->animationState:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;->NONE:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$NavigationManagerAnimationState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public navigateToHomeScreen()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 755
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    .line 756
    .local v1, "navigationManager":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v2}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->getActiveHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v0

    .line 758
    .local v0, "activeHomeScreen":Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
    sget-object v2, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "navigateToHomeScreen: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Clubs:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    if-ne v0, v2, :cond_1

    .line 761
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const-class v4, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreen;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 766
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getHomeScreenClass(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->gotoLowestScreenWithPop(Ljava/lang/Class;)V

    .line 767
    return-void

    .line 762
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;->Achievements:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    if-ne v0, v2, :cond_0

    .line 763
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const-class v4, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreen;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public navigateToHomeScreenOrOOBETutorial()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 733
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->isTutorialExperienceAllowed()Lio/reactivex/Single;

    move-result-object v0

    .line 734
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 735
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 736
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 743
    return-void
.end method

.method public onApplicationPause()V
    .locals 3

    .prologue
    .line 806
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 807
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 808
    .local v1, "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onApplicationPause()V

    .line 806
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 810
    .end local v1    # "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public onApplicationResume()V
    .locals 3

    .prologue
    .line 813
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->pendingNavigationParams:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    if-eqz v2, :cond_0

    .line 814
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplaceWithPendingParams()V

    .line 817
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 818
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationStack:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 819
    .local v1, "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onApplicationResume()V

    .line 817
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 821
    .end local v1    # "stackEntry":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_1
    return-void
.end method

.method public putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 2
    .param p2, "parameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 779
    .local p1, "newScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->getActiveHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getHomeScreenClass(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, p2, p1, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->gotoLowestScreenWithPop(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 780
    return-void
.end method

.method public removeOnNavigatedListener(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 148
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 150
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationListeners:Ljava/util/List;

    monitor-enter v1

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigationListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 152
    monitor-exit v1

    .line 153
    return-void

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
