.class public abstract Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PostLocator;
.super Ljava/lang/Object;
.source "ActivityFeedDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PostLocator"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PostLocator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PostLocator$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PostLocator$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/ActivityFeedDataTypes$PostLocator;
    .locals 1
    .param p0, "locator"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PostLocator;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/activityFeed/AutoValue_ActivityFeedDataTypes_PostLocator;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract locator()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
