.class public Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
.super Ljava/lang/Object;
.source "EpgClientStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/EpgClientStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProviderStorageInfo"
.end annotation


# static fields
.field private static final sFields:I = 0xa

.field private static final sSeparator:Ljava/lang/String; = "@@"


# instance fields
.field public lastFavoritesChannel:I

.field public lastTVChannel:I

.field public provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

.field public selectedFavoritesChannel:I

.field public selectedTVChannel:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    .locals 10
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 65
    new-instance v9, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    invoke-direct {v9}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;-><init>()V

    .line 67
    .local v9, "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    const-string v0, "@@"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 68
    .local v8, "fields":[Ljava/lang/String;
    array-length v0, v8

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    .line 69
    const-string v0, "EpgClientStorage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fromString: wrong number of fields: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const/4 v9, 0x0

    .line 80
    .end local v9    # "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    :goto_0
    return-object v9

    .line 73
    .restart local v9    # "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    aget-object v1, v8, v7

    const/4 v2, 0x1

    aget-object v2, v8, v2

    const/4 v3, 0x2

    aget-object v3, v8, v3

    const/4 v4, 0x3

    aget-object v4, v8, v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    move-result-object v4

    const/4 v5, 0x4

    aget-object v5, v8, v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x5

    aget-object v6, v8, v6

    invoke-static {v6}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;ZLcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;Z)V

    iput-object v0, v9, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    .line 75
    const/4 v0, 0x6

    aget-object v0, v8, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v9, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastTVChannel:I

    .line 76
    const/4 v0, 0x7

    aget-object v0, v8, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v9, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastFavoritesChannel:I

    .line 77
    const/16 v0, 0x8

    aget-object v0, v8, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v9, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedTVChannel:I

    .line 78
    const/16 v0, 0x9

    aget-object v0, v8, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v9, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedFavoritesChannel:I

    goto :goto_0
.end method


# virtual methods
.method public serialize()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->title_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_source:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-boolean v1, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->isPreferred:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->filter_preference:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastTVChannel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->lastFavoritesChannel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedTVChannel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->selectedFavoritesChannel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
