.class Lcom/microsoft/playready/networkdevice/TransmitterControlPoint$1;
.super Ljava/lang/Object;
.source "TransmitterControlPoint.java"

# interfaces
.implements Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->addTransmitterListener(Lcom/microsoft/playready/networkdevice/ITransmitterListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint$1;->this$0:Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call1(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    new-instance v2, Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint$1;->this$0:Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;

    invoke-static {v3}, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->access$000(Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;)Lcom/microsoft/playready/networkdevice/Native_Class4;

    move-result-object v3

    invoke-direct {v2, v3, p1, p2, p3}, Lcom/microsoft/playready/networkdevice/TransmitterInfo;-><init>(Lcom/microsoft/playready/networkdevice/Native_Class4;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .local v2, "transmitter":Lcom/microsoft/playready/networkdevice/TransmitterInfo;
    iget-object v4, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint$1;->this$0:Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;

    monitor-enter v4

    .line 53
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint$1;->this$0:Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;

    invoke-static {v3}, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->access$100(Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/playready/networkdevice/ITransmitterListener;

    .line 55
    .local v1, "listener":Lcom/microsoft/playready/networkdevice/ITransmitterListener;
    invoke-interface {v1, v2}, Lcom/microsoft/playready/networkdevice/ITransmitterListener;->onTransmitterAdded(Lcom/microsoft/playready/networkdevice/TransmitterInfo;)V

    goto :goto_0

    .line 57
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/microsoft/playready/networkdevice/ITransmitterListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    return-void
.end method

.method public call2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "udn"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    const/16 v4, 0x3a98

    const/16 v5, 0x2710

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/playready/networkdevice/TransmitterInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 70
    .local v0, "transmitter":Lcom/microsoft/playready/networkdevice/TransmitterInfo;
    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint$1;->this$0:Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;

    monitor-enter v2

    .line 72
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint$1;->this$0:Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;

    invoke-static {v1}, Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;->access$100(Lcom/microsoft/playready/networkdevice/TransmitterControlPoint;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/playready/networkdevice/ITransmitterListener;

    .line 74
    .local v7, "listener":Lcom/microsoft/playready/networkdevice/ITransmitterListener;
    invoke-interface {v7, v0}, Lcom/microsoft/playready/networkdevice/ITransmitterListener;->onTransmitterRemoved(Lcom/microsoft/playready/networkdevice/TransmitterInfo;)V

    goto :goto_0

    .line 76
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "listener":Lcom/microsoft/playready/networkdevice/ITransmitterListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 77
    return-void
.end method
