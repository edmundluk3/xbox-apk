.class public Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;
.super Landroid/widget/FrameLayout;
.source "XLELabeledImageView.java"


# instance fields
.field private labelView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mainImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f09035f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "heightInPixels"    # I

    .prologue
    const/4 v5, -0x2

    .line 47
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-direct {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->mainImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 48
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->mainImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 49
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v0, v3, p3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 50
    .local v0, "ilp":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->mainImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->mainImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    const v4, 0x7f0201a3

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageResource(I)V

    .line 54
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->mainImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->addView(Landroid/view/View;)V

    .line 56
    if-eqz p2, :cond_0

    .line 58
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-direct {v3, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->labelView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 59
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->labelView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v4, 0x7f0c0126

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setBackgroundResource(I)V

    .line 60
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f090257

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 61
    .local v2, "padding":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->labelView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v2, v2, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setPadding(IIII)V

    .line 63
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 64
    .local v1, "llp":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v3, 0x53

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 65
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->labelView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->labelView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->addView(Landroid/view/View;)V

    .line 69
    .end local v1    # "llp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v2    # "padding":I
    :cond_0
    return-void
.end method


# virtual methods
.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->labelView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/XLELabeledImageView;->labelView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    :cond_0
    return-void
.end method
