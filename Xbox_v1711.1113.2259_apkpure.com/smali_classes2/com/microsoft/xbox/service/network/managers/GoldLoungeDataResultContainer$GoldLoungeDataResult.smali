.class public Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult;
.super Ljava/lang/Object;
.source "GoldLoungeDataResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GoldLoungeDataResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
    }
.end annotation


# instance fields
.field public DealsWithGold:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation
.end field

.field public GamesWithGold:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult;
    .locals 3
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 110
    const-class v0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult;

    const-class v1, Ljava/util/Date;

    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterShortDateAlternateFormatJSONDeserializer;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterShortDateAlternateFormatJSONDeserializer;-><init>()V

    invoke-static {p0, v0, v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult;

    return-object v0
.end method


# virtual methods
.method public isGoldLoungeDataEmpty()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult;->DealsWithGold:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult;->GamesWithGold:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
