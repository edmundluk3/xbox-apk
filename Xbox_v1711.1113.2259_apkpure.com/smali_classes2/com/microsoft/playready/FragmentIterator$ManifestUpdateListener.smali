.class final Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;
.super Ljava/lang/Object;
.source "FragmentIterator.java"

# interfaces
.implements Lcom/microsoft/playready/IStreamUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/FragmentIterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ManifestUpdateListener"
.end annotation


# instance fields
.field mWeakIterator:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/microsoft/playready/FragmentIterator;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/playready/FragmentIterator;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/FragmentIterator;Lcom/microsoft/playready/FragmentIterator;)V
    .locals 1
    .param p2, "iterator"    # Lcom/microsoft/playready/FragmentIterator;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;->this$0:Lcom/microsoft/playready/FragmentIterator;

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;->mWeakIterator:Ljava/lang/ref/WeakReference;

    .line 137
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;->mWeakIterator:Ljava/lang/ref/WeakReference;

    .line 138
    return-void
.end method


# virtual methods
.method public onStreamUpdate(Lcom/microsoft/playready/MediaStream;)V
    .locals 2
    .param p1, "updatedStream"    # Lcom/microsoft/playready/MediaStream;

    .prologue
    .line 143
    iget-object v1, p0, Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;->mWeakIterator:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/FragmentIterator;

    .line 144
    .local v0, "iterator":Lcom/microsoft/playready/FragmentIterator;
    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0}, Lcom/microsoft/playready/FragmentIterator;->onManifestUpdate()V

    .line 148
    :cond_0
    return-void
.end method
