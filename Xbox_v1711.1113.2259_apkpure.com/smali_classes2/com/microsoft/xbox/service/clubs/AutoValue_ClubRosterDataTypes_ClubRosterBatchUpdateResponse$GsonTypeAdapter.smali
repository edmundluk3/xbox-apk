.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultResponses:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;"
        }
    .end annotation
.end field

.field private final responsesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;->defaultResponses:Lcom/google/common/collect/ImmutableList;

    .line 23
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;->responsesAdapter:Lcom/google/gson/TypeAdapter;

    .line 24
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    .locals 4
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v3, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v2, v3, :cond_0

    .line 45
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 46
    const/4 v2, 0x0

    .line 67
    :goto_0
    return-object v2

    .line 48
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;->defaultResponses:Lcom/google/common/collect/ImmutableList;

    .line 50
    .local v1, "responses":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 51
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v3, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v2, v3, :cond_1

    .line 53
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 56
    :cond_1
    const/4 v2, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_2
    :goto_2
    packed-switch v2, :pswitch_data_1

    .line 62
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 56
    :pswitch_0
    const-string v3, "responses"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    .line 58
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;->responsesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v2, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "responses":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    check-cast v1, Lcom/google/common/collect/ImmutableList;

    .line 59
    .restart local v1    # "responses":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    goto :goto_1

    .line 66
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 67
    new-instance v2, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse;

    invoke-direct {v2, v1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse;-><init>(Lcom/google/common/collect/ImmutableList;)V

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch -0x74d48bae
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultResponses(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "defaultResponses":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;->defaultResponses:Lcom/google/common/collect/ImmutableList;

    .line 27
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    if-nez p2, :cond_0

    .line 33
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 40
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 37
    const-string v0, "responses"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;->responsesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;->responses()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 39
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateResponse$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;)V

    return-void
.end method
