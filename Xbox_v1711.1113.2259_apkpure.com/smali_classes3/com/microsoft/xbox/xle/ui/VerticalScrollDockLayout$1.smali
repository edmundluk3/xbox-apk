.class Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;
.super Ljava/lang/Object;
.source "VerticalScrollDockLayout.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->setScrollContainerView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 11
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 232
    if-lez p3, :cond_0

    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$000(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)I

    move-result v2

    if-ne v2, v8, :cond_0

    .line 234
    invoke-virtual {p1, v7}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    if-nez v2, :cond_1

    .line 235
    .local v8, "atTop":Z
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$100(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_CLOSED:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    if-ne v2, v3, :cond_0

    if-eqz v8, :cond_0

    .line 237
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 238
    .local v0, "eventTime":J
    const/4 v4, 0x3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$200(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)F

    move-result v5

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$300(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)F

    move-result v6

    move-wide v2, v0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v9

    .line 239
    .local v9, "cancelTouch":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-virtual {v2, v9}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 240
    invoke-virtual {v9}, Landroid/view/MotionEvent;->recycle()V

    .line 241
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 242
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$200(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)F

    move-result v5

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$300(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)F

    move-result v6

    move-wide v2, v0

    move v4, v7

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v10

    .line 243
    .local v10, "newTouch":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-virtual {v2, v10}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 244
    invoke-virtual {v10}, Landroid/view/MotionEvent;->recycle()V

    .line 247
    .end local v0    # "eventTime":J
    .end local v8    # "atTop":Z
    .end local v9    # "cancelTouch":Landroid/view/MotionEvent;
    .end local v10    # "newTouch":Landroid/view/MotionEvent;
    :cond_0
    return-void

    :cond_1
    move v8, v7

    .line 234
    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    invoke-static {v0, p2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->access$002(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;I)I

    .line 252
    return-void
.end method
