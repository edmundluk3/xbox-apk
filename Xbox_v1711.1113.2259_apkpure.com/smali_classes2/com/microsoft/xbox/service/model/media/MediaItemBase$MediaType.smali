.class public final enum Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;
.super Ljava/lang/Enum;
.source "MediaItemBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/media/MediaItemBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum HUB:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum MOVIE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum MOVIE_TRAILER:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum MUSIC_ALBUM:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum MUSIC_ARTIST:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum MUSIC_MUSIC_VIDEO:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum MUSIC_PLAYLIST:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum MUSIC_TRACK:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum TV_EPISODE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum TV_SERIRES:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field public static final enum UNDEFINED:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

.field private static final intToTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 27
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "UNDEFINED"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->UNDEFINED:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "MOVIE"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MOVIE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 29
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "MUSIC_ALBUM"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_ALBUM:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "MUSIC_TRACK"

    invoke-direct {v2, v3, v8, v8}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_TRACK:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 31
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "TV_EPISODE"

    invoke-direct {v2, v3, v9, v9}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->TV_EPISODE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "MUSIC_MUSIC_VIDEO"

    const/4 v4, 0x5

    const/4 v5, 0x5

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_MUSIC_VIDEO:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 33
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "MUSIC_ARTIST"

    const/4 v4, 0x6

    const/4 v5, 0x6

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_ARTIST:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "MUSIC_PLAYLIST"

    const/4 v4, 0x7

    const/4 v5, 0x7

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_PLAYLIST:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 35
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "TV_SERIRES"

    const/16 v4, 0x8

    const/16 v5, 0x8

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->TV_SERIRES:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 36
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "HUB"

    const/16 v4, 0x9

    const/16 v5, 0x9

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->HUB:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 37
    new-instance v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    const-string v3, "MOVIE_TRAILER"

    const/16 v4, 0xa

    const/16 v5, 0xa

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MOVIE_TRAILER:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 25
    const/16 v2, 0xb

    new-array v2, v2, [Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->UNDEFINED:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MOVIE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_ALBUM:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_TRACK:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->TV_EPISODE:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_MUSIC_VIDEO:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_ARTIST:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MUSIC_PLAYLIST:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->TV_SERIRES:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->HUB:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->MOVIE_TRAILER:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->$VALUES:[Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 41
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->intToTypeMap:Ljava/util/HashMap;

    .line 44
    invoke-static {}, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->values()[Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 45
    sget-object v4, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->intToTypeMap:Ljava/util/HashMap;

    iget v5, v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->value:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "number"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->value:I

    .line 53
    return-void
.end method

.method public static getMediaTypeFromInt(I)Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 60
    sget-object v1, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->intToTypeMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 61
    .local v0, "mediaType":Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;
    if-nez v0, :cond_0

    .line 62
    sget-object v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->UNDEFINED:Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    .line 64
    .end local v0    # "mediaType":Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->$VALUES:[Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/microsoft/xbox/service/model/media/MediaItemBase$MediaType;->value:I

    return v0
.end method
