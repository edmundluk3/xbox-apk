.class Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PurchaseWebViewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FetchPurchaseAuthTokenAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private audienceUri:Ljava/lang/String;

.field private purchaseWebView:Landroid/webkit/WebView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0
    .param p2, "purchaseWebView"    # Landroid/webkit/WebView;
    .param p3, "audienceUri"    # Ljava/lang/String;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 266
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 267
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 268
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->purchaseWebView:Landroid/webkit/WebView;

    .line 269
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->audienceUri:Ljava/lang/String;

    .line 270
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 274
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 275
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 302
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->audienceUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->remove(Ljava/lang/Object;)V

    .line 303
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->audienceUri:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->audienceUri:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getXTokenString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadSafeFixedSizeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v1

    .line 304
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v1, "FetchPurchaseAuthTokenAsyncTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error in getting token for audience = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->audienceUri:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 296
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 4

    .prologue
    .line 280
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 281
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->purchaseWebView:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->audienceUri:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 282
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->purchaseWebView:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->audienceUri:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 292
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 260
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel$FetchPurchaseAuthTokenAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 286
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 287
    return-void
.end method
