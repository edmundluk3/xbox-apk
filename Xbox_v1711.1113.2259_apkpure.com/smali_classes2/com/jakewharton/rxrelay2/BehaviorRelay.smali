.class public final Lcom/jakewharton/rxrelay2/BehaviorRelay;
.super Lcom/jakewharton/rxrelay2/Relay;
.source "BehaviorRelay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/jakewharton/rxrelay2/Relay",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final EMPTY:[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

.field private static final EMPTY_ARRAY:[Ljava/lang/Object;


# instance fields
.field index:J

.field final readLock:Ljava/util/concurrent/locks/Lock;

.field private final subscribers:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<[",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field final value:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final writeLock:Ljava/util/concurrent/locks/Lock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->EMPTY_ARRAY:[Ljava/lang/Object;

    .line 61
    new-array v0, v1, [Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    sput-object v0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 90
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    invoke-direct {p0}, Lcom/jakewharton/rxrelay2/Relay;-><init>()V

    .line 91
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    .line 92
    .local v0, "lock":Ljava/util/concurrent/locks/ReadWriteLock;
    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    iput-object v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->readLock:Ljava/util/concurrent/locks/Lock;

    .line 93
    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    iput-object v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->writeLock:Ljava/util/concurrent/locks/Lock;

    .line 94
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v2, Lcom/jakewharton/rxrelay2/BehaviorRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    .line 95
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->value:Ljava/util/concurrent/atomic/AtomicReference;

    .line 96
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    .local p1, "defaultValue":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;-><init>()V

    .line 105
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "defaultValue == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->value:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V

    .line 107
    return-void
.end method

.method private add(Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    .local p1, "rs":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    const/4 v4, 0x0

    .line 202
    :cond_0
    iget-object v3, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    .line 203
    .local v0, "a":[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    array-length v2, v0

    .line 205
    .local v2, "len":I
    add-int/lit8 v3, v2, 0x1

    new-array v1, v3, [Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    .line 206
    .local v1, "b":[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 207
    aput-object p1, v1, v2

    .line 208
    iget-object v3, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 209
    return-void
.end method

.method public static create()Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;-><init>()V

    return-object v0
.end method

.method public static createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "defaultValue":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {v0, p0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private setCurrent(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 248
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    .local p1, "current":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->writeLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 250
    :try_start_0
    iget-wide v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->index:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->index:J

    .line 251
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->value:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->lazySet(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->writeLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 255
    return-void

    .line 253
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->writeLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "value == null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 125
    :cond_0
    invoke-direct {p0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->setCurrent(Ljava/lang/Object;)V

    .line 126
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 127
    .local v0, "bs":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    iget-wide v4, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->index:J

    invoke-virtual {v0, p1, v4, v5}, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->emitNext(Ljava/lang/Object;J)V

    .line 126
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 129
    .end local v0    # "bs":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    :cond_1
    return-void
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 145
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->value:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getValues()[Ljava/lang/Object;
    .locals 3

    .prologue
    .line 154
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    sget-object v0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->EMPTY_ARRAY:[Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 155
    .local v0, "a":[Ljava/lang/Object;, "[TT;"
    invoke-virtual {p0, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValues([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .line 156
    .local v1, "b":[Ljava/lang/Object;, "[TT;"
    sget-object v2, Lcom/jakewharton/rxrelay2/BehaviorRelay;->EMPTY_ARRAY:[Ljava/lang/Object;

    if-ne v1, v2, :cond_0

    .line 157
    const/4 v2, 0x0

    new-array v1, v2, [Ljava/lang/Object;

    .line 159
    .end local v1    # "b":[Ljava/lang/Object;, "[TT;"
    :cond_0
    return-object v1
.end method

.method public getValues([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)[TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    .local p1, "array":[Ljava/lang/Object;, "[TT;"
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 172
    iget-object v2, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->value:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    .line 173
    .local v1, "o":Ljava/lang/Object;, "TT;"
    if-nez v1, :cond_1

    .line 174
    array-length v2, p1

    if-eqz v2, :cond_0

    .line 175
    aput-object v5, p1, v3

    :cond_0
    move-object v0, p1

    .line 188
    .end local p1    # "array":[Ljava/lang/Object;, "[TT;"
    .local v0, "array":[Ljava/lang/Object;, "[TT;"
    :goto_0
    return-object v0

    .line 179
    .end local v0    # "array":[Ljava/lang/Object;, "[TT;"
    .restart local p1    # "array":[Ljava/lang/Object;, "[TT;"
    :cond_1
    array-length v2, p1

    if-eqz v2, :cond_3

    .line 180
    aput-object v1, p1, v3

    .line 181
    array-length v2, p1

    if-eq v2, v4, :cond_2

    .line 182
    aput-object v5, p1, v4

    :cond_2
    :goto_1
    move-object v0, p1

    .line 188
    .end local p1    # "array":[Ljava/lang/Object;, "[TT;"
    .restart local v0    # "array":[Ljava/lang/Object;, "[TT;"
    goto :goto_0

    .line 185
    .end local v0    # "array":[Ljava/lang/Object;, "[TT;"
    .restart local p1    # "array":[Ljava/lang/Object;, "[TT;"
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object p1, v2

    check-cast p1, [Ljava/lang/Object;

    .line 186
    aput-object v1, p1, v3

    goto :goto_1
.end method

.method public hasObservers()Z
    .locals 1

    .prologue
    .line 133
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    array-length v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValue()Z
    .locals 1

    .prologue
    .line 197
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->value:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method remove(Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    .local p1, "rs":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    const/4 v7, 0x0

    .line 217
    :cond_0
    iget-object v5, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    .line 218
    .local v0, "a":[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    sget-object v5, Lcom/jakewharton/rxrelay2/BehaviorRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    if-ne v0, v5, :cond_2

    .line 242
    :cond_1
    :goto_0
    return-void

    .line 221
    :cond_2
    array-length v4, v0

    .line 222
    .local v4, "len":I
    const/4 v3, -0x1

    .line 223
    .local v3, "j":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_3

    .line 224
    aget-object v5, v0, v2

    if-ne v5, p1, :cond_4

    .line 225
    move v3, v2

    .line 230
    :cond_3
    if-ltz v3, :cond_1

    .line 234
    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 235
    sget-object v1, Lcom/jakewharton/rxrelay2/BehaviorRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    .line 241
    .local v1, "b":[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    :goto_2
    iget-object v5, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    .line 223
    .end local v1    # "b":[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 237
    :cond_5
    add-int/lit8 v5, v4, -0x1

    new-array v1, v5, [Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    .line 238
    .restart local v1    # "b":[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "[Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    invoke-static {v0, v7, v1, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    add-int/lit8 v5, v3, 0x1

    sub-int v6, v4, v3

    add-int/lit8 v6, v6, -0x1

    invoke-static {v0, v5, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2
.end method

.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-TT;>;"
    new-instance v0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    invoke-direct {v0, p1, p0}, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;-><init>(Lio/reactivex/Observer;Lcom/jakewharton/rxrelay2/BehaviorRelay;)V

    .line 112
    .local v0, "bs":Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;, "Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable<TT;>;"
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 113
    invoke-direct {p0, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->add(Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;)V

    .line 114
    iget-boolean v1, v0, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->cancelled:Z

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {p0, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->remove(Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;)V

    .line 119
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;->emitFirst()V

    goto :goto_0
.end method

.method subscriberCount()I
    .locals 1

    .prologue
    .line 137
    .local p0, "this":Lcom/jakewharton/rxrelay2/BehaviorRelay;, "Lcom/jakewharton/rxrelay2/BehaviorRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/BehaviorRelay$BehaviorDisposable;

    array-length v0, v0

    return v0
.end method
