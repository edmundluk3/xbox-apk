.class public final Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes;
.super Ljava/lang/Object;
.source "HoverChatResultDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ParticipantsResult;,
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;,
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;,
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$PositionChangedResult;,
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$HideBadgeResult;,
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ShowBadgeResult;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
