.class public final enum Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;
.super Ljava/lang/Enum;
.source "ProfileRecentsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AuthorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

.field public static final enum Club:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

.field public static final enum PageUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

.field public static final enum TitleUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

.field public static final enum User:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 529
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    const-string v1, "User"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->User:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    const-string v1, "TitleUser"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->TitleUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    const-string v1, "PageUser"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->PageUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->Unknown:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    const-string v1, "Club"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->Club:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    .line 528
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->User:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->TitleUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->PageUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->Unknown:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->Club:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 528
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 528
    const-class v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;
    .locals 1

    .prologue
    .line 528
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    return-object v0
.end method
