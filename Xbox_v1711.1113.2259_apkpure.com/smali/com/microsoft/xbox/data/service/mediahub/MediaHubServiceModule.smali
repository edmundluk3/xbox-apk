.class public Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;
.super Ljava/lang/Object;
.source "MediaHubServiceModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule$Names;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideMediaHubEndpoint()Ljava/lang/String;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "MEDIA_HUB_ENDPOINT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 37
    const-string v0, "https://mediahub.xboxlive.com"

    return-object v0
.end method

.method provideMediaHubRetrofit(Ljava/lang/String;Lokhttp3/OkHttpClient;Lretrofit2/converter/gson/GsonConverterFactory;)Lretrofit2/Retrofit;
    .locals 2
    .param p1, "baseUrl"    # Ljava/lang/String;
        .annotation runtime Ljavax/inject/Named;
            value = "MEDIA_HUB_ENDPOINT"
        .end annotation
    .end param
    .param p2, "client"    # Lokhttp3/OkHttpClient;
        .annotation runtime Ljavax/inject/Named;
            value = "MEDIA_HUB_OK_HTTP"
        .end annotation
    .end param
    .param p3, "gsonConverterFactory"    # Lretrofit2/converter/gson/GsonConverterFactory;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "MEDIA_HUB_RETROFIT"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 67
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 68
    invoke-virtual {v0, p1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {v0, p3}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 70
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0, p2}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    .line 67
    return-object v0
.end method

.method providesMediaHubClient(Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;)Lokhttp3/OkHttpClient;
    .locals 1
    .param p1, "xTokenAuthenticator"    # Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;
    .param p2, "xTokenHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;
    .param p3, "xUserAgentHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;
    .param p4, "loggingInterceptor"    # Lokhttp3/logging/HttpLoggingInterceptor;
    .param p5, "localeHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;
    .param p6, "contentRestrictionsHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "MEDIA_HUB_OK_HTTP"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 51
    invoke-virtual {v0, p1}, Lokhttp3/OkHttpClient$Builder;->authenticator(Lokhttp3/Authenticator;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 52
    invoke-virtual {v0, p2}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p3}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 54
    invoke-virtual {v0, p4}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 55
    invoke-virtual {v0, p5}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p6}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 50
    return-object v0
.end method
