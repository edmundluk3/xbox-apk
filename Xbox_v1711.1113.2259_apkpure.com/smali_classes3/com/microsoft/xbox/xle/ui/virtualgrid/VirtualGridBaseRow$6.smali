.class Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;
.super Ljava/lang/Object;
.source "VirtualGridBaseRow.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->startAnimateBottomFrame(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

.field final synthetic val$finishAlpha:F

.field final synthetic val$finishHeight:F


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;FF)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    .prologue
    .line 298
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    iput p2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;->val$finishAlpha:F

    iput p3, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;->val$finishHeight:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 318
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setX(F)V

    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;->val$finishAlpha:F

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 312
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;->val$finishHeight:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 313
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow$6;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridBaseRow;->mBottomFrame:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 314
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 306
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 302
    return-void
.end method
