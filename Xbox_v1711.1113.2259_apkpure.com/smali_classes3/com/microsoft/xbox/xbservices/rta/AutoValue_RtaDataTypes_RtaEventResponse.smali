.class final Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;
.super Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;
.source "AutoValue_RtaDataTypes_RtaEventResponse.java"


# instance fields
.field private final payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

.field private final subscriptionId:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Ljava/lang/Integer;Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;)V
    .locals 0
    .param p1, "subscriptionId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "payload"    # Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->subscriptionId:Ljava/lang/Integer;

    .line 17
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;

    .line 47
    .local v0, "that":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->subscriptionId:Ljava/lang/Integer;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    if-nez v3, :cond_4

    .line 48
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;->payload()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 47
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->subscriptionId:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 48
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;->payload()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaEventResponse;
    :cond_5
    move v1, v2

    .line 50
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 55
    const/4 v0, 0x1

    .line 56
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->subscriptionId:Ljava/lang/Integer;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 58
    mul-int/2addr v0, v3

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    if-nez v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 60
    return v0

    .line 57
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->subscriptionId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    .line 59
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public payload()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    return-object v0
.end method

.method public subscriptionId()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->subscriptionId:Ljava/lang/Integer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RtaEventResponse{subscriptionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->subscriptionId:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", payload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/AutoValue_RtaDataTypes_RtaEventResponse;->payload:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaPayload;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
