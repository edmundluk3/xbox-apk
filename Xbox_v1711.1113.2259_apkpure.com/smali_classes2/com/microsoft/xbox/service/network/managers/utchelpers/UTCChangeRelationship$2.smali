.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$2;
.super Ljava/lang/Object;
.source "UTCChangeRelationship.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->trackChangeRelationshipAction(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$isFollowing:Z

.field final synthetic val$xuid:Ljava/lang/String;


# direct methods
.method constructor <init>(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$2;->val$isFollowing:Z

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$2;->val$xuid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    .prologue
    .line 90
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 91
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "FriendAdd"

    .line 93
    .local v1, "relationship":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$2;->val$isFollowing:Z

    if-eqz v2, :cond_0

    .line 94
    const-string v1, "ExistingFriend"

    .line 96
    :cond_0
    const-string v2, "RelationshipStatus"

    invoke-virtual {v0, v2, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 97
    const-string v2, "TargetXuid"

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$2;->val$xuid:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 99
    const-string v2, "Change Relationship - Action"

    invoke-static {v2, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 100
    return-void
.end method
