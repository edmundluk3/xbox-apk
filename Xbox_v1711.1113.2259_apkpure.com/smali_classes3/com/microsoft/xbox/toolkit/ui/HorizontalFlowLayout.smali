.class public Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;
.super Landroid/view/ViewGroup;
.source "HorizontalFlowLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    }
.end annotation


# instance fields
.field protected adapter:Landroid/widget/Adapter;

.field private dataSetObserver:Landroid/database/DataSetObserver;

.field protected rowHeights:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->rowHeights:Ljava/util/List;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->dataSetObserver:Landroid/database/DataSetObserver;

    .line 34
    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 138
    instance-of v0, p1, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->generateDefaultLayoutParams()Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;-><init>()V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 148
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 149
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .end local p1    # "p":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 151
    :goto_0
    return-object v0

    .restart local p1    # "p":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 143
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected getHorizontalPadding()I
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected getVerticalPadding()I
    .locals 2

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 13
    .param p1, "changed"    # Z
    .param p2, "leftPosition"    # I
    .param p3, "topPosition"    # I
    .param p4, "rightPosition"    # I
    .param p5, "bottomPosition"    # I

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getMeasuredWidth()I

    move-result v11

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getPaddingRight()I

    move-result v12

    sub-int v6, v11, v12

    .line 94
    .local v6, "maxInternalWidth":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getPaddingLeft()I

    move-result v9

    .line 95
    .local v9, "x":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getPaddingTop()I

    move-result v10

    .line 97
    .local v10, "y":I
    const/4 v8, 0x0

    .line 99
    .local v8, "rowIndex":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getChildCount()I

    move-result v7

    .line 101
    .local v7, "numChildren":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v7, :cond_3

    .line 102
    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 104
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_0

    .line 101
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 108
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 109
    .local v4, "childWidth":I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 110
    .local v2, "childHeight":I
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    .line 112
    .local v3, "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    add-int v11, v9, v4

    iget v12, v3, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->leftMargin:I

    add-int/2addr v11, v12

    if-le v11, v6, :cond_1

    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->rowHeights:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    if-ge v8, v11, :cond_1

    .line 113
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getPaddingLeft()I

    move-result v9

    .line 114
    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->rowHeights:Ljava/util/List;

    invoke-interface {v11, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    add-int/2addr v10, v11

    .line 115
    add-int/lit8 v8, v8, 0x1

    .line 118
    :cond_1
    iget-boolean v11, v3, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->centerVertical:Z

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->rowHeights:Ljava/util/List;

    .line 119
    invoke-interface {v11, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    sub-int/2addr v11, v2

    div-int/lit8 v11, v11, 0x2

    add-int v0, v10, v11

    .line 122
    .local v0, "_y":I
    :goto_2
    iget v11, v3, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->leftMargin:I

    add-int/2addr v9, v11

    .line 123
    iget v11, v3, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->topMargin:I

    add-int/2addr v0, v11

    .line 125
    add-int v11, v9, v4

    add-int v12, v0, v2

    invoke-virtual {v1, v9, v0, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 127
    iget v11, v3, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->rightMargin:I

    add-int/2addr v11, v4

    add-int/2addr v9, v11

    goto :goto_1

    .end local v0    # "_y":I
    :cond_2
    move v0, v10

    .line 119
    goto :goto_2

    .line 129
    .end local v1    # "child":Landroid/view/View;
    .end local v2    # "childHeight":I
    .end local v3    # "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .end local v4    # "childWidth":I
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 38
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->rowHeights:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->clear()V

    .line 40
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getHorizontalPadding()I

    move-result v15

    sub-int v10, v14, v15

    .line 42
    .local v10, "maxInternalWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getChildCount()I

    move-result v11

    .line 44
    .local v11, "numChildren":I
    const/4 v8, 0x0

    .line 45
    .local v8, "currentLineWidth":I
    const/4 v7, 0x0

    .line 46
    .local v7, "currentLineHeight":I
    const/4 v13, 0x0

    .line 47
    .local v13, "requiredWidth":I
    const/4 v12, 0x0

    .line 49
    .local v12, "requiredHeight":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v11, :cond_3

    .line 50
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 52
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v14

    const/16 v15, 0x8

    if-eq v14, v15, :cond_0

    .line 53
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v3, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->measureChild(Landroid/view/View;II)V

    .line 55
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;

    .line 57
    .local v5, "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    iget v15, v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->leftMargin:I

    add-int/2addr v14, v15

    iget v15, v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->rightMargin:I

    add-int v6, v14, v15

    .line 58
    .local v6, "childWidth":I
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    iget v15, v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->topMargin:I

    add-int/2addr v14, v15

    iget v15, v5, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;->bottomMargin:I

    add-int v4, v14, v15

    .line 60
    .local v4, "childHeight":I
    add-int v14, v8, v6

    if-le v14, v10, :cond_2

    .line 61
    invoke-static {v8, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 62
    add-int/2addr v12, v7

    .line 64
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->rowHeights:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    move v8, v6

    .line 67
    move v7, v4

    .line 74
    .end local v4    # "childHeight":I
    .end local v5    # "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .end local v6    # "childWidth":I
    :cond_0
    :goto_1
    add-int/lit8 v14, v11, -0x1

    if-ne v9, v14, :cond_1

    .line 75
    invoke-static {v8, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 76
    add-int/2addr v12, v7

    .line 77
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->rowHeights:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 69
    .restart local v4    # "childHeight":I
    .restart local v5    # "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .restart local v6    # "childWidth":I
    :cond_2
    add-int/2addr v8, v6

    .line 70
    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    goto :goto_1

    .line 82
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "childHeight":I
    .end local v5    # "childLayoutParams":Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout$FlowLayoutParams;
    .end local v6    # "childWidth":I
    :cond_3
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v14

    const/high16 v15, 0x40000000    # 2.0f

    if-ne v14, v15, :cond_4

    .line 83
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    .line 85
    :goto_2
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v15

    const/high16 v16, 0x40000000    # 2.0f

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 86
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    .line 81
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->setMeasuredDimension(II)V

    .line 88
    return-void

    .line 84
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getHorizontalPadding()I

    move-result v14

    add-int/2addr v14, v13

    goto :goto_2

    .line 87
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->getVerticalPadding()I

    move-result v15

    add-int/2addr v15, v12

    goto :goto_3
.end method

.method protected populateFromAdapter()V
    .locals 5

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->removeAllViewsInLayout()V

    .line 172
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->adapter:Landroid/widget/Adapter;

    if-eqz v3, :cond_0

    .line 173
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->adapter:Landroid/widget/Adapter;

    invoke-interface {v3}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 174
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->adapter:Landroid/widget/Adapter;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4, p0}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 175
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 176
    .local v1, "lays":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0, v2, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "i":I
    .end local v1    # "lays":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->requestLayout()V

    .line 181
    return-void
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/Adapter;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->adapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->adapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 160
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->adapter:Landroid/widget/Adapter;

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->adapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->adapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 166
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->populateFromAdapter()V

    .line 167
    return-void
.end method
