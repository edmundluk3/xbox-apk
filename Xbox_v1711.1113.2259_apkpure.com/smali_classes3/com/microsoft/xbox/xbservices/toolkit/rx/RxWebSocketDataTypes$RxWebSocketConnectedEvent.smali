.class public final enum Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;
.super Ljava/lang/Enum;
.source "RxWebSocketDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RxWebSocketConnectedEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;",
        ">;",
        "Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketEvent;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;->INSTANCE:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    sget-object v1, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;->INSTANCE:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;->$VALUES:[Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;->$VALUES:[Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketConnectedEvent;

    return-object v0
.end method
