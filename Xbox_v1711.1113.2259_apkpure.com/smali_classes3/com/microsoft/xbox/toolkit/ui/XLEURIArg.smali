.class public Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;
.super Ljava/lang/Object;
.source "XLEURIArg.java"


# instance fields
.field private final errorResourceId:I

.field private final loadingResourceId:I

.field private final uri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 19
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;-><init>(Ljava/lang/String;II)V

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "loadingResourceId"    # I
    .param p3, "errorResourceId"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->uri:Ljava/lang/String;

    .line 14
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->loadingResourceId:I

    .line 15
    iput p3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->errorResourceId:I

    .line 16
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 36
    if-nez p1, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v1

    .line 39
    :cond_1
    if-ne p1, p0, :cond_2

    move v1, v2

    .line 40
    goto :goto_0

    .line 42
    :cond_2
    instance-of v3, p1, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 45
    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;

    .line 46
    .local v0, "other":Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->loadingResourceId:I

    iget v4, v0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->loadingResourceId:I

    if-ne v3, v4, :cond_0

    .line 49
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->errorResourceId:I

    iget v4, v0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->errorResourceId:I

    if-ne v3, v4, :cond_0

    .line 52
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->uri:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->uri:Ljava/lang/String;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->uri:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->uri:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->uri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getErrorResourceId()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->errorResourceId:I

    return v0
.end method

.method public getLoadingResourceId()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->loadingResourceId:I

    return v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->uri:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x1

    .line 58
    .local v0, "hash":I
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->loadingResourceId:I

    add-int/lit8 v0, v1, 0xd

    .line 59
    mul-int/lit8 v1, v0, 0x11

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->errorResourceId:I

    add-int v0, v1, v2

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->uri:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 61
    mul-int/lit8 v1, v0, 0x17

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEURIArg;->uri:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 63
    :cond_0
    return v0
.end method
