.class public Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "FeedItemActionsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SocialActionModel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field private result:Lcom/microsoft/xbox/toolkit/AsyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;",
            ">;"
        }
    .end annotation
.end field

.field private final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V
    .locals 4
    .param p1, "actionType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .param p2, "fmtUrl"    # Ljava/lang/String;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 110
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->getQuery()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, p2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->url:Ljava/lang/String;

    .line 111
    new-instance v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel$ItemRootRunnable;-><init>(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    .line 112
    const-wide/32 v0, 0x1b7740

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->lifetime:J

    .line 113
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->url:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    return-object v0
.end method


# virtual methods
.method public getData()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->result:Lcom/microsoft/xbox/toolkit/AsyncResult;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->result:Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    goto :goto_0
.end method

.method public getResultList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$FeedItemAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->getData()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->getData()Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->actionType:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;->getActions(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 130
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->result:Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 131
    return-void
.end method
