.class public final Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;
.super Ljava/lang/Object;
.source "ActivityFeedFilterPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final activityFeedFilterPresenterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final interactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "activityFeedFilterPresenterMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;>;"
    .local p2, "interactorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;>;"
    .local p3, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-boolean v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->activityFeedFilterPresenterMembersInjector:Ldagger/MembersInjector;

    .line 30
    sget-boolean v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->interactorProvider:Ljavax/inject/Provider;

    .line 32
    sget-boolean v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 34
    return-void
.end method

.method public static create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "activityFeedFilterPresenterMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;>;"
    .local p1, "interactorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;>;"
    .local p2, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;-><init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;
    .locals 4

    .prologue
    .line 38
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->activityFeedFilterPresenterMembersInjector:Ldagger/MembersInjector;

    new-instance v3, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->interactorProvider:Ljavax/inject/Provider;

    .line 40
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->schedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    invoke-direct {v3, v0, v1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;-><init>(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    .line 38
    invoke-static {v2, v3}, Ldagger/internal/MembersInjectors;->injectMembers(Ldagger/MembersInjector;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter_Factory;->get()Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;

    move-result-object v0

    return-object v0
.end method
