.class public final Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;
.super Ljava/lang/Object;
.source "ClubWatchInteractor.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final initialLoadTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final refreshTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 1
    .param p1, "repository"    # Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    .line 49
    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;->refreshTransformer:Lio/reactivex/ObservableTransformer;

    .line 61
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 36
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 37
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    return-object v0
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 49
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 50
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 60
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    sget-object v0, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;->parameter()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p0, v0, v1}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;->load(J)Lio/reactivex/Single;

    move-result-object v0

    .line 39
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 40
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$13;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->inFlightInstance()Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    sget-object v0, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;->parameter()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p0, v0, v1}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;->load(J)Lio/reactivex/Single;

    move-result-object v0

    .line 52
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 53
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$7;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    invoke-static {}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->inFlightInstance()Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 51
    return-object v0
.end method


# virtual methods
.method public initialLoadTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadActionWithParameter",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public refreshTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshActionWithParameter",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;->refreshTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
