.class public Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.source "PeopleActivityFeedScreenViewModel.java"


# instance fields
.field filterRepository:Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private filtersActive:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 35
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->filteringEnabled:Z

    .line 37
    return-void
.end method


# virtual methods
.method protected getActivityFeedData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleActivityFeedData()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method protected getContinuationToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleActivityFeedsContinuationToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected loadActivityFeedData(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 10
    .param p1, "forceLoad"    # Z
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 58
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 59
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->currentFilters:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    invoke-virtual {v8, p1, v9, p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPeopleActivityFeed(ZLcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v7

    .line 60
    .local v7, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 61
    const-string v8, "PeopleActivityFeedScreenViewModel"

    const-string v9, "Unable to get my following activity data"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v8, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowingProfile(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 68
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v2

    .line 70
    .local v2, "followingData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_7

    .line 72
    new-instance v4, Ljava/util/Hashtable;

    invoke-direct {v4}, Ljava/util/Hashtable;-><init>()V

    .line 73
    .local v4, "lookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 74
    .local v1, "f":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v9, v1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v4, v9, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 77
    .end local v1    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleActivityFeedData()Ljava/util/List;

    move-result-object v0

    .line 78
    .local v0, "activityFeedData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_7

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v5

    .line 80
    .local v5, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v5, :cond_3

    .line 81
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 82
    .local v3, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 84
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPresenceData(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 89
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 90
    .restart local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    if-eqz v9, :cond_4

    .line 91
    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 92
    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 93
    .restart local v1    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v9, v1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setProfileStatus(Lcom/microsoft/xbox/service/model/UserStatus;)V

    .line 94
    iget-object v9, v1, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerRealName:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setRealName(Ljava/lang/String;)V

    .line 95
    iget-object v9, v1, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v9, :cond_4

    .line 96
    iget-object v9, v1, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/UserProfileData;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setProfilePictureURI(Ljava/lang/String;)V

    goto :goto_1

    .line 98
    .end local v1    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_5
    iget-object v9, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-static {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    if-eqz v5, :cond_4

    .line 99
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPresenceData()Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;

    move-result-object v6

    .line 100
    .local v6, "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    if-eqz v6, :cond_6

    .line 101
    iget-object v9, v6, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    invoke-static {v9}, Lcom/microsoft/xbox/service/model/UserStatus;->getStatusFromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setProfileStatus(Lcom/microsoft/xbox/service/model/UserStatus;)V

    .line 103
    :cond_6
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setRealName(Ljava/lang/String;)V

    goto :goto_1

    .line 110
    .end local v0    # "activityFeedData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    .end local v3    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .end local v4    # "lookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v5    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v6    # "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    :cond_7
    return-object v7
.end method

.method protected shouldReloadActivityFeedData()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshPeopleActivityFeed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshFollowingProfile()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showChangeFriendshipDialog(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 2
    .param p1, "personSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 114
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showChangeFriendshipDialog(Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 115
    return-void
.end method
