.class Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen$ActivityAlertAdapterProvider;
.super Ljava/lang/Object;
.source "ActivityAlertScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityAlertAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen$ActivityAlertAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen$1;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen$ActivityAlertAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/ActivityAlertScreen;)V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 46
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    .line 47
    .local v0, "activityAlertViewModel":Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 48
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getActivityAlertScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
