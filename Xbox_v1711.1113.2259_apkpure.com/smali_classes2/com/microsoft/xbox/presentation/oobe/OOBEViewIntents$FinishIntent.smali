.class public final enum Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;
.super Ljava/lang/Enum;
.source "OOBEViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FinishIntent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;",
        ">;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    .line 124
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    sget-object v1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;->$VALUES:[Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    .line 127
    const-class v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 124
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 124
    const-class v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;->$VALUES:[Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewIntents$FinishIntent;->TAG:Ljava/lang/String;

    return-object v0
.end method
