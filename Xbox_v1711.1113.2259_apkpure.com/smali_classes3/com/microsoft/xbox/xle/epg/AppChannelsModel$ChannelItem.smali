.class public Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;
.super Ljava/lang/Object;
.source "AppChannelsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/AppChannelsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChannelItem"
.end annotation


# static fields
.field public static final ATTR_EXTRA:Ljava/lang/String; = "extra"


# instance fields
.field private final mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

.field private mShow:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;

.field private mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

.field public final propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;)V
    .locals 1
    .param p1, "channel"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    .param p2, "show"    # Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;

    .prologue
    .line 811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    .line 812
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 813
    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShow:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;

    .line 814
    return-void
.end method


# virtual methods
.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 854
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->contentType:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 846
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->descritpion:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 858
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    iget v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->duration:I

    goto :goto_0
.end method

.method public getEpisodeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 866
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->episodeName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 826
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShow:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getImageURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 834
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShow:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;->image:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShow:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$Show;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->parentalRating:Ljava/lang/String;

    goto :goto_0
.end method

.method public getTuneToUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 862
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;->deepLink:Ljava/lang/String;

    goto :goto_0
.end method

.method public isExtraLoaded()Z
    .locals 1

    .prologue
    .line 838
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadExtra()V
    .locals 1

    .prologue
    .line 842
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->loadItemExtra(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)Z

    .line 843
    return-void
.end method

.method protected setExtra(Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;)V
    .locals 2
    .param p1, "showExtra"    # Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    .prologue
    .line 817
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mShowExtra:Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    .line 818
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    const-string v1, "extra"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->notify(Ljava/lang/String;)V

    .line 819
    return-void
.end method

.method public tuneToShow()Z
    .locals 1

    .prologue
    .line 822
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->tuneToShow(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)Z

    move-result v0

    return v0
.end method
