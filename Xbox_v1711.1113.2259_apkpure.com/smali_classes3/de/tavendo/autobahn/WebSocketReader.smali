.class public Lde/tavendo/autobahn/WebSocketReader;
.super Ljava/lang/Thread;
.source "WebSocketReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lde/tavendo/autobahn/WebSocketReader$ReaderState;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$de$tavendo$autobahn$WebSocketReader$ReaderState:[I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mApplicationBuffer:Ljava/nio/ByteBuffer;

.field private mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

.field private mInputStream:Ljava/io/InputStream;

.field private mInsideMessage:Z

.field private mMessageOpcode:I

.field private mMessagePayload:Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;

.field private final mNetworkBuffer:[B

.field private final mSocket:Ljava/net/Socket;

.field private mState:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

.field private volatile mStopped:Z

.field private mUTF8Validator:Lde/tavendo/autobahn/Utf8Validator;

.field private final mWebSocketConnectionHandler:Landroid/os/Handler;

.field private final mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;


# direct methods
.method static synthetic $SWITCH_TABLE$de$tavendo$autobahn$WebSocketReader$ReaderState()[I
    .locals 3

    .prologue
    .line 41
    sget-object v0, Lde/tavendo/autobahn/WebSocketReader;->$SWITCH_TABLE$de$tavendo$autobahn$WebSocketReader$ReaderState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->values()[Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->STATE_CLOSED:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->STATE_CLOSING:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->STATE_CONNECTING:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->STATE_OPEN:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lde/tavendo/autobahn/WebSocketReader;->$SWITCH_TABLE$de$tavendo$autobahn$WebSocketReader$ReaderState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lde/tavendo/autobahn/WebSocketReader;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Ljava/net/Socket;Lde/tavendo/autobahn/WebSocketOptions;Ljava/lang/String;)V
    .locals 2
    .param p1, "master"    # Landroid/os/Handler;
    .param p2, "socket"    # Ljava/net/Socket;
    .param p3, "options"    # Lde/tavendo/autobahn/WebSocketOptions;
    .param p4, "threadName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0, p4}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 56
    iput-boolean v0, p0, Lde/tavendo/autobahn/WebSocketReader;->mStopped:Z

    .line 65
    iput-boolean v0, p0, Lde/tavendo/autobahn/WebSocketReader;->mInsideMessage:Z

    .line 69
    new-instance v0, Lde/tavendo/autobahn/Utf8Validator;

    invoke-direct {v0}, Lde/tavendo/autobahn/Utf8Validator;-><init>()V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketReader;->mUTF8Validator:Lde/tavendo/autobahn/Utf8Validator;

    .line 83
    iput-object p1, p0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketConnectionHandler:Landroid/os/Handler;

    .line 85
    iput-object p2, p0, Lde/tavendo/autobahn/WebSocketReader;->mSocket:Ljava/net/Socket;

    .line 86
    iput-object p3, p0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    .line 88
    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketReader;->mNetworkBuffer:[B

    .line 89
    invoke-virtual {p3}, Lde/tavendo/autobahn/WebSocketOptions;->getMaxFramePayloadSize()I

    move-result v0

    add-int/lit8 v0, v0, 0xe

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    .line 90
    new-instance v0, Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;

    invoke-virtual {p3}, Lde/tavendo/autobahn/WebSocketOptions;->getMaxMessagePayloadSize()I

    move-result v1

    invoke-direct {v0, v1}, Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketReader;->mMessagePayload:Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    .line 93
    sget-object v0, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->STATE_CONNECTING:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketReader;->mState:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    .line 95
    sget-object v0, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    const-string v1, "WebSocket reader created."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    return-void
.end method

.method private consumeData()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 596
    invoke-static {}, Lde/tavendo/autobahn/WebSocketReader;->$SWITCH_TABLE$de$tavendo$autobahn$WebSocketReader$ReaderState()[I

    move-result-object v1

    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketReader;->mState:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    invoke-virtual {v2}, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 605
    :goto_0
    :pswitch_0
    return v0

    .line 599
    :pswitch_1
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketReader;->processData()Z

    move-result v0

    goto :goto_0

    .line 603
    :pswitch_2
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketReader;->processHandshake()Z

    move-result v0

    goto :goto_0

    .line 596
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private parseHTTPStatus()Landroid/util/Pair;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x20

    const/4 v13, 0x0

    .line 561
    const/4 v0, 0x4

    .local v0, "beg":I
    :goto_0
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v9

    if-lt v0, v9, :cond_3

    .line 565
    :cond_0
    add-int/lit8 v2, v0, 0x1

    .local v2, "end":I
    :goto_1
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v9

    if-lt v2, v9, :cond_4

    .line 569
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 570
    const/4 v6, 0x0

    .line 571
    .local v6, "statusCode":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    add-int v9, v0, v4

    if-lt v9, v2, :cond_5

    .line 577
    add-int/lit8 v2, v2, 0x1

    .line 579
    move v3, v2

    .local v3, "eol":I
    :goto_3
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->position()I

    move-result v9

    if-lt v3, v9, :cond_6

    .line 582
    :cond_2
    sub-int v8, v3, v2

    .line 583
    .local v8, "statusMessageLength":I
    new-array v5, v8, [B

    .line 584
    .local v5, "statusBuf":[B
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 585
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v5, v13, v8}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 586
    new-instance v7, Ljava/lang/String;

    const-string v9, "UTF-8"

    invoke-direct {v7, v5, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 587
    .local v7, "statusMessage":Ljava/lang/String;
    sget-object v9, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    const-string v10, "Status: %d (%s)"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v13

    const/4 v12, 0x1

    aput-object v7, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    new-instance v9, Landroid/util/Pair;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-direct {v9, v10, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v9

    .line 562
    .end local v2    # "end":I
    .end local v3    # "eol":I
    .end local v4    # "i":I
    .end local v5    # "statusBuf":[B
    .end local v6    # "statusCode":I
    .end local v7    # "statusMessage":Ljava/lang/String;
    .end local v8    # "statusMessageLength":I
    :cond_3
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v9

    if-eq v9, v10, :cond_0

    .line 561
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 566
    .restart local v2    # "end":I
    :cond_4
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v9

    if-eq v9, v10, :cond_1

    .line 565
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 572
    .restart local v4    # "i":I
    .restart local v6    # "statusCode":I
    :cond_5
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    add-int v10, v0, v4

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v9

    add-int/lit8 v1, v9, -0x30

    .line 573
    .local v1, "digit":I
    mul-int/lit8 v6, v6, 0xa

    .line 574
    add-int/2addr v6, v1

    .line 571
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 580
    .end local v1    # "digit":I
    .restart local v3    # "eol":I
    :cond_6
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v9

    const/16 v10, 0xd

    if-eq v9, v10, :cond_2

    .line 579
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method private processData()Z
    .locals 28
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    if-nez v25, :cond_1a

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/nio/ByteBuffer;->position()I

    move-result v25

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_19

    .line 135
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    .line 136
    .local v4, "b0":B
    and-int/lit16 v0, v4, 0x80

    move/from16 v25, v0

    if-eqz v25, :cond_0

    const/4 v7, 0x1

    .line 137
    .local v7, "fin":Z
    :goto_0
    and-int/lit8 v25, v4, 0x70

    shr-int/lit8 v22, v25, 0x4

    .line 138
    .local v22, "rsv":I
    and-int/lit8 v16, v4, 0xf

    .line 140
    .local v16, "opcode":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    invoke-virtual/range {v25 .. v26}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    .line 141
    .local v5, "b1":B
    and-int/lit16 v0, v5, 0x80

    move/from16 v25, v0

    if-eqz v25, :cond_1

    const/4 v14, 0x1

    .line 142
    .local v14, "masked":Z
    :goto_1
    and-int/lit8 v17, v5, 0x7f

    .line 146
    .local v17, "payload_len1":I
    if-eqz v22, :cond_2

    .line 147
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "RSV != 0 and no extension negotiated"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 136
    .end local v5    # "b1":B
    .end local v7    # "fin":Z
    .end local v14    # "masked":Z
    .end local v16    # "opcode":I
    .end local v17    # "payload_len1":I
    .end local v22    # "rsv":I
    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    .line 141
    .restart local v5    # "b1":B
    .restart local v7    # "fin":Z
    .restart local v16    # "opcode":I
    .restart local v22    # "rsv":I
    :cond_1
    const/4 v14, 0x0

    goto :goto_1

    .line 150
    .restart local v14    # "masked":Z
    .restart local v17    # "payload_len1":I
    :cond_2
    if-eqz v14, :cond_3

    .line 152
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "masked server frame"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 155
    :cond_3
    const/16 v25, 0x7

    move/from16 v0, v16

    move/from16 v1, v25

    if-le v0, v1, :cond_7

    .line 157
    if-nez v7, :cond_4

    .line 158
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "fragmented control frame"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 160
    :cond_4
    const/16 v25, 0x7d

    move/from16 v0, v17

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    .line 161
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "control frame with payload length > 125 octets"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 163
    :cond_5
    const/16 v25, 0x8

    move/from16 v0, v16

    move/from16 v1, v25

    if-eq v0, v1, :cond_6

    const/16 v25, 0x9

    move/from16 v0, v16

    move/from16 v1, v25

    if-eq v0, v1, :cond_6

    const/16 v25, 0xa

    move/from16 v0, v16

    move/from16 v1, v25

    if-eq v0, v1, :cond_6

    .line 164
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "control frame using reserved opcode "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 166
    :cond_6
    const/16 v25, 0x8

    move/from16 v0, v16

    move/from16 v1, v25

    if-ne v0, v1, :cond_a

    const/16 v25, 0x1

    move/from16 v0, v17

    move/from16 v1, v25

    if-ne v0, v1, :cond_a

    .line 167
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "received close control frame with payload len 1"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 171
    :cond_7
    if-eqz v16, :cond_8

    const/16 v25, 0x1

    move/from16 v0, v16

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    const/16 v25, 0x2

    move/from16 v0, v16

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    .line 172
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "data frame using reserved opcode "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 174
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mInsideMessage:Z

    move/from16 v25, v0

    if-nez v25, :cond_9

    if-nez v16, :cond_9

    .line 175
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "received continuation data frame outside fragmented message"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 177
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mInsideMessage:Z

    move/from16 v25, v0

    if-eqz v25, :cond_a

    if-eqz v16, :cond_a

    .line 178
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "received non-continuation data frame while inside fragmented message"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 182
    :cond_a
    if-eqz v14, :cond_b

    const/4 v13, 0x4

    .line 183
    .local v13, "mask_len":I
    :goto_2
    const/4 v9, 0x0

    .line 185
    .local v9, "header_len":I
    const/16 v25, 0x7e

    move/from16 v0, v17

    move/from16 v1, v25

    if-ge v0, v1, :cond_c

    .line 186
    add-int/lit8 v9, v13, 0x2

    .line 197
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/nio/ByteBuffer;->position()I

    move-result v25

    move/from16 v0, v25

    if-lt v0, v9, :cond_18

    .line 200
    const/4 v10, 0x2

    .line 201
    .local v10, "i":I
    const-wide/16 v18, 0x0

    .line 202
    .local v18, "payload_len":J
    const/16 v25, 0x7e

    move/from16 v0, v17

    move/from16 v1, v25

    if-ne v0, v1, :cond_10

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    shl-int/lit8 v25, v25, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v26

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    or-int v25, v25, v26

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 204
    const-wide/16 v26, 0x7e

    cmp-long v25, v18, v26

    if-gez v25, :cond_f

    .line 205
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "invalid data frame length (not using minimal length encoding)"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 182
    .end local v9    # "header_len":I
    .end local v10    # "i":I
    .end local v13    # "mask_len":I
    .end local v18    # "payload_len":J
    :cond_b
    const/4 v13, 0x0

    goto :goto_2

    .line 187
    .restart local v9    # "header_len":I
    .restart local v13    # "mask_len":I
    :cond_c
    const/16 v25, 0x7e

    move/from16 v0, v17

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    .line 188
    add-int/lit8 v9, v13, 0x4

    .line 189
    goto :goto_3

    :cond_d
    const/16 v25, 0x7f

    move/from16 v0, v17

    move/from16 v1, v25

    if-ne v0, v1, :cond_e

    .line 190
    add-int/lit8 v9, v13, 0xa

    .line 191
    goto :goto_3

    .line 193
    :cond_e
    new-instance v25, Ljava/lang/Exception;

    const-string v26, "logic error"

    invoke-direct/range {v25 .. v26}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v25

    .line 207
    .restart local v10    # "i":I
    .restart local v18    # "payload_len":J
    :cond_f
    add-int/lit8 v10, v10, 0x2

    .line 229
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketOptions;->getMaxFramePayloadSize()I

    move-result v25

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v26, v0

    cmp-long v25, v18, v26

    if-lez v25, :cond_14

    .line 230
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "frame payload too large"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 208
    :cond_10
    const/16 v25, 0x7f

    move/from16 v0, v17

    move/from16 v1, v25

    if-ne v0, v1, :cond_13

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    const/16 v26, 0x2

    invoke-virtual/range {v25 .. v26}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0x80

    move/from16 v25, v0

    if-eqz v25, :cond_11

    .line 210
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "invalid data frame length (> 2^63)"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 212
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    const/16 v26, 0x2

    invoke-virtual/range {v25 .. v26}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    shl-int/lit8 v25, v25, 0x38

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v26

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    shl-int/lit8 v26, v26, 0x30

    .line 212
    or-int v25, v25, v26

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v26, v0

    const/16 v27, 0x4

    invoke-virtual/range {v26 .. v27}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v26

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    shl-int/lit8 v26, v26, 0x28

    .line 212
    or-int v25, v25, v26

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v26, v0

    const/16 v27, 0x5

    invoke-virtual/range {v26 .. v27}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v26

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    shl-int/lit8 v26, v26, 0x20

    .line 212
    or-int v25, v25, v26

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v26, v0

    const/16 v27, 0x6

    invoke-virtual/range {v26 .. v27}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v26

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    shl-int/lit8 v26, v26, 0x18

    .line 212
    or-int v25, v25, v26

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v26, v0

    const/16 v27, 0x7

    invoke-virtual/range {v26 .. v27}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v26

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    shl-int/lit8 v26, v26, 0x10

    .line 212
    or-int v25, v25, v26

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v26, v0

    const/16 v27, 0x8

    invoke-virtual/range {v26 .. v27}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v26

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    shl-int/lit8 v26, v26, 0x8

    .line 212
    or-int v25, v25, v26

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v26, v0

    const/16 v27, 0x9

    invoke-virtual/range {v26 .. v27}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v26

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    .line 212
    or-int v25, v25, v26

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v18, v0

    .line 220
    const-wide/32 v26, 0x10000

    cmp-long v25, v18, v26

    if-gez v25, :cond_12

    .line 221
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "invalid data frame length (not using minimal length encoding)"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 223
    :cond_12
    add-int/lit8 v10, v10, 0x8

    .line 224
    goto/16 :goto_4

    .line 225
    :cond_13
    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v18, v0

    goto/16 :goto_4

    .line 234
    :cond_14
    new-instance v25, Lde/tavendo/autobahn/WebSocketFrameHeader;

    invoke-direct/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    .line 235
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/WebSocketFrameHeader;->setOpcode(I)V

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Lde/tavendo/autobahn/WebSocketFrameHeader;->setFin(Z)V

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/WebSocketFrameHeader;->setReserved(I)V

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketFrameHeader;->setPayloadLength(I)V

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Lde/tavendo/autobahn/WebSocketFrameHeader;->setHeaderLength(I)V

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getHeaderLength()I

    move-result v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getPayloadLength()I

    move-result v27

    add-int v26, v26, v27

    invoke-virtual/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketFrameHeader;->setTotalLen(I)V

    .line 242
    if-eqz v14, :cond_16

    .line 243
    const/16 v25, 0x4

    move/from16 v0, v25

    new-array v12, v0, [B

    .line 244
    .local v12, "mask":[B
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_5
    const/16 v25, 0x4

    move/from16 v0, v25

    if-lt v11, v0, :cond_15

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Lde/tavendo/autobahn/WebSocketFrameHeader;->setMask([B)V

    .line 249
    add-int/lit8 v10, v10, 0x4

    .line 255
    .end local v11    # "j":I
    .end local v12    # "mask":[B
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getPayloadLength()I

    move-result v25

    if-eqz v25, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/nio/ByteBuffer;->position()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getTotalLength()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_17

    const/16 v25, 0x0

    .line 417
    .end local v4    # "b0":B
    .end local v5    # "b1":B
    .end local v7    # "fin":Z
    .end local v9    # "header_len":I
    .end local v10    # "i":I
    .end local v13    # "mask_len":I
    .end local v14    # "masked":Z
    .end local v16    # "opcode":I
    .end local v17    # "payload_len1":I
    .end local v18    # "payload_len":J
    .end local v22    # "rsv":I
    :goto_7
    return v25

    .line 245
    .restart local v4    # "b0":B
    .restart local v5    # "b1":B
    .restart local v7    # "fin":Z
    .restart local v9    # "header_len":I
    .restart local v10    # "i":I
    .restart local v11    # "j":I
    .restart local v12    # "mask":[B
    .restart local v13    # "mask_len":I
    .restart local v14    # "masked":Z
    .restart local v16    # "opcode":I
    .restart local v17    # "payload_len1":I
    .restart local v18    # "payload_len":J
    .restart local v22    # "rsv":I
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    add-int v26, v10, v11

    invoke-virtual/range {v25 .. v26}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-byte v0, v0

    move/from16 v25, v0

    aput-byte v25, v12, v10

    .line 244
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 251
    .end local v11    # "j":I
    .end local v12    # "mask":[B
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketFrameHeader;->setMask([B)V

    goto :goto_6

    .line 255
    :cond_17
    const/16 v25, 0x1

    goto :goto_7

    .line 260
    .end local v10    # "i":I
    .end local v18    # "payload_len":J
    :cond_18
    const/16 v25, 0x0

    goto :goto_7

    .line 265
    .end local v4    # "b0":B
    .end local v5    # "b1":B
    .end local v7    # "fin":Z
    .end local v9    # "header_len":I
    .end local v13    # "mask_len":I
    .end local v14    # "masked":Z
    .end local v16    # "opcode":I
    .end local v17    # "payload_len1":I
    .end local v22    # "rsv":I
    :cond_19
    const/16 v25, 0x0

    goto :goto_7

    .line 275
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/nio/ByteBuffer;->position()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getTotalLength()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_2f

    .line 278
    const/4 v8, 0x0

    .line 279
    .local v8, "framePayload":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/nio/ByteBuffer;->position()I

    move-result v15

    .line 280
    .local v15, "oldPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getPayloadLength()I

    move-result v25

    if-lez v25, :cond_1b

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getPayloadLength()I

    move-result v25

    move/from16 v0, v25

    new-array v8, v0, [B

    .line 282
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getHeaderLength()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getPayloadLength()I

    move-result v27

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v8, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 285
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getTotalLength()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getOpcode()I

    move-result v25

    const/16 v26, 0x7

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_25

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getOpcode()I

    move-result v25

    const/16 v26, 0x8

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_22

    .line 294
    const/16 v6, 0x3ed

    .line 295
    .local v6, "code":I
    const/16 v21, 0x0

    .line 297
    .local v21, "reason":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getPayloadLength()I

    move-result v25

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_20

    .line 300
    const/16 v25, 0x0

    aget-byte v25, v8, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v25

    mul-int/lit16 v0, v0, 0x100

    move/from16 v25, v0

    const/16 v26, 0x1

    aget-byte v26, v8, v26

    move/from16 v0, v26

    and-int/lit16 v0, v0, 0xff

    move/from16 v26, v0

    add-int v6, v25, v26

    .line 301
    const/16 v25, 0x3e8

    move/from16 v0, v25

    if-lt v6, v0, :cond_1d

    .line 302
    const/16 v25, 0x3e8

    move/from16 v0, v25

    if-lt v6, v0, :cond_1c

    const/16 v25, 0xbb7

    move/from16 v0, v25

    if-gt v6, v0, :cond_1c

    .line 303
    const/16 v25, 0x3e8

    move/from16 v0, v25

    if-eq v6, v0, :cond_1c

    const/16 v25, 0x3e9

    move/from16 v0, v25

    if-eq v6, v0, :cond_1c

    const/16 v25, 0x3ea

    move/from16 v0, v25

    if-eq v6, v0, :cond_1c

    const/16 v25, 0x3eb

    move/from16 v0, v25

    if-eq v6, v0, :cond_1c

    const/16 v25, 0x3ef

    move/from16 v0, v25

    if-eq v6, v0, :cond_1c

    const/16 v25, 0x3f0

    move/from16 v0, v25

    if-eq v6, v0, :cond_1c

    const/16 v25, 0x3f1

    move/from16 v0, v25

    if-eq v6, v0, :cond_1c

    const/16 v25, 0x3f2

    move/from16 v0, v25

    if-eq v6, v0, :cond_1c

    const/16 v25, 0x3f3

    move/from16 v0, v25

    if-ne v6, v0, :cond_1d

    .line 304
    :cond_1c
    const/16 v25, 0x1388

    move/from16 v0, v25

    if-lt v6, v0, :cond_1e

    .line 306
    :cond_1d
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "invalid close code "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 310
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getPayloadLength()I

    move-result v25

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_20

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getPayloadLength()I

    move-result v25

    add-int/lit8 v25, v25, -0x2

    move/from16 v0, v25

    new-array v0, v0, [B

    move-object/from16 v20, v0

    .line 313
    .local v20, "ra":[B
    const/16 v25, 0x2

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getPayloadLength()I

    move-result v27

    add-int/lit8 v27, v27, -0x2

    move/from16 v0, v25

    move-object/from16 v1, v20

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-static {v8, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 315
    new-instance v24, Lde/tavendo/autobahn/Utf8Validator;

    invoke-direct/range {v24 .. v24}, Lde/tavendo/autobahn/Utf8Validator;-><init>()V

    .line 316
    .local v24, "val":Lde/tavendo/autobahn/Utf8Validator;
    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/Utf8Validator;->validate([B)Z

    .line 317
    invoke-virtual/range {v24 .. v24}, Lde/tavendo/autobahn/Utf8Validator;->isValid()Z

    move-result v25

    if-nez v25, :cond_1f

    .line 318
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "invalid close reasons (not UTF-8)"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 320
    :cond_1f
    new-instance v21, Ljava/lang/String;

    .end local v21    # "reason":Ljava/lang/String;
    const-string v25, "UTF-8"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 324
    .end local v20    # "ra":[B
    .end local v24    # "val":Lde/tavendo/autobahn/Utf8Validator;
    .restart local v21    # "reason":Ljava/lang/String;
    :cond_20
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v6, v1}, Lde/tavendo/autobahn/WebSocketReader;->onClose(ILjava/lang/String;)V

    .line 409
    .end local v6    # "code":I
    .end local v21    # "reason":Ljava/lang/String;
    :cond_21
    :goto_8
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/nio/ByteBuffer;->position()I

    move-result v25

    if-lez v25, :cond_2e

    const/16 v25, 0x1

    goto/16 :goto_7

    .line 326
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getOpcode()I

    move-result v25

    const/16 v26, 0x9

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_23

    .line 328
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lde/tavendo/autobahn/WebSocketReader;->onPing([B)V

    goto :goto_8

    .line 330
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getOpcode()I

    move-result v25

    const/16 v26, 0xa

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_24

    .line 332
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lde/tavendo/autobahn/WebSocketReader;->onPong([B)V

    goto :goto_8

    .line 337
    :cond_24
    new-instance v25, Ljava/lang/Exception;

    const-string v26, "logic error"

    invoke-direct/range {v25 .. v26}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v25

    .line 343
    :cond_25
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mInsideMessage:Z

    move/from16 v25, v0

    if-nez v25, :cond_26

    .line 345
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lde/tavendo/autobahn/WebSocketReader;->mInsideMessage:Z

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->getOpcode()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lde/tavendo/autobahn/WebSocketReader;->mMessageOpcode:I

    .line 347
    move-object/from16 v0, p0

    iget v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessageOpcode:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketOptions;->getValidateIncomingUtf8()Z

    move-result v25

    if-eqz v25, :cond_26

    .line 348
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mUTF8Validator:Lde/tavendo/autobahn/Utf8Validator;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/Utf8Validator;->reset()V

    .line 352
    :cond_26
    if-eqz v8, :cond_29

    .line 355
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessagePayload:Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;->size()I

    move-result v25

    array-length v0, v8

    move/from16 v26, v0

    add-int v25, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lde/tavendo/autobahn/WebSocketOptions;->getMaxMessagePayloadSize()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_27

    .line 356
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "message payload too large"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 360
    :cond_27
    move-object/from16 v0, p0

    iget v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessageOpcode:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketOptions;->getValidateIncomingUtf8()Z

    move-result v25

    if-eqz v25, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mUTF8Validator:Lde/tavendo/autobahn/Utf8Validator;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Lde/tavendo/autobahn/Utf8Validator;->validate([B)Z

    move-result v25

    if-nez v25, :cond_28

    .line 361
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "invalid UTF-8 in text message payload"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 365
    :cond_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessagePayload:Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;->write([B)V

    .line 369
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mFrameHeader:Lde/tavendo/autobahn/WebSocketFrameHeader;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketFrameHeader;->isFin()Z

    move-result v25

    if-eqz v25, :cond_21

    .line 371
    move-object/from16 v0, p0

    iget v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessageOpcode:I

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_2c

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketOptions;->getValidateIncomingUtf8()Z

    move-result v25

    if-eqz v25, :cond_2a

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mUTF8Validator:Lde/tavendo/autobahn/Utf8Validator;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/Utf8Validator;->isValid()Z

    move-result v25

    if-nez v25, :cond_2a

    .line 375
    new-instance v25, Lde/tavendo/autobahn/WebSocketException;

    const-string v26, "UTF-8 text message payload ended within Unicode code point"

    invoke-direct/range {v25 .. v26}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v25

    .line 379
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/WebSocketOptions;->getReceiveTextMessagesRaw()Z

    move-result v25

    if-eqz v25, :cond_2b

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessagePayload:Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;->toByteArray()[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/WebSocketReader;->onRawTextMessage([B)V

    .line 403
    :goto_9
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lde/tavendo/autobahn/WebSocketReader;->mInsideMessage:Z

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessagePayload:Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;->reset()V

    goto/16 :goto_8

    .line 387
    :cond_2b
    new-instance v23, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessagePayload:Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;->toByteArray()[B

    move-result-object v25

    const-string v26, "UTF-8"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 388
    .local v23, "s":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/WebSocketReader;->onTextMessage(Ljava/lang/String;)V

    goto :goto_9

    .line 391
    .end local v23    # "s":Ljava/lang/String;
    :cond_2c
    move-object/from16 v0, p0

    iget v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessageOpcode:I

    move/from16 v25, v0

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_2d

    .line 394
    move-object/from16 v0, p0

    iget-object v0, v0, Lde/tavendo/autobahn/WebSocketReader;->mMessagePayload:Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lde/tavendo/autobahn/NoCopyByteArrayOutputStream;->toByteArray()[B

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/WebSocketReader;->onBinaryMessage([B)V

    goto :goto_9

    .line 399
    :cond_2d
    new-instance v25, Ljava/lang/Exception;

    const-string v26, "logic error"

    invoke-direct/range {v25 .. v26}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v25

    .line 412
    :cond_2e
    const/16 v25, 0x0

    goto/16 :goto_7

    .line 417
    .end local v8    # "framePayload":[B
    .end local v15    # "oldPosition":I
    :cond_2f
    const/16 v25, 0x0

    goto/16 :goto_7
.end method

.method private processHandshake()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x54

    const/16 v10, 0xd

    const/16 v9, 0xa

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 509
    const/4 v2, 0x0

    .line 510
    .local v2, "res":Z
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int/lit8 v1, v5, -0x4

    .local v1, "pos":I
    :goto_0
    if-gez v1, :cond_0

    .line 555
    :goto_1
    return v2

    .line 511
    :cond_0
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v8, v1, 0x0

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-ne v5, v10, :cond_5

    .line 512
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v8, v1, 0x1

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-ne v5, v9, :cond_5

    .line 513
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v8, v1, 0x2

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-ne v5, v10, :cond_5

    .line 514
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v8, v1, 0x3

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-ne v5, v9, :cond_5

    .line 519
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 522
    .local v0, "oldPosition":I
    const/4 v3, 0x0

    .line 523
    .local v3, "serverError":Z
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    const/16 v8, 0x48

    if-ne v5, v8, :cond_1

    .line 524
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-ne v5, v11, :cond_1

    .line 525
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const/4 v8, 0x2

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    if-ne v5, v11, :cond_1

    .line 526
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    const/4 v8, 0x3

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    const/16 v8, 0x50

    if-ne v5, v8, :cond_1

    .line 528
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketReader;->parseHTTPStatus()Landroid/util/Pair;

    move-result-object v4

    .line 529
    .local v4, "status":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v8, 0x12c

    if-lt v5, v8, :cond_1

    .line 531
    new-instance v8, Lde/tavendo/autobahn/WebSocketMessage$ServerError;

    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v5, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-direct {v8, v9, v5}, Lde/tavendo/autobahn/WebSocketMessage$ServerError;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v8}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    .line 532
    const/4 v3, 0x1

    .line 536
    .end local v4    # "status":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
    :cond_1
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v8, v1, 0x4

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 537
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 538
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    .line 540
    if-nez v3, :cond_3

    .line 542
    iget-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    if-lez v5, :cond_2

    move v2, v6

    .line 544
    :goto_2
    sget-object v5, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->STATE_OPEN:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    iput-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mState:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    .line 551
    :goto_3
    if-eqz v3, :cond_4

    :goto_4
    invoke-virtual {p0, v7}, Lde/tavendo/autobahn/WebSocketReader;->onHandshake(Z)V

    goto/16 :goto_1

    :cond_2
    move v2, v7

    .line 542
    goto :goto_2

    .line 546
    :cond_3
    const/4 v2, 0x1

    .line 547
    sget-object v5, Lde/tavendo/autobahn/WebSocketReader$ReaderState;->STATE_CLOSED:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    iput-object v5, p0, Lde/tavendo/autobahn/WebSocketReader;->mState:Lde/tavendo/autobahn/WebSocketReader$ReaderState;

    .line 548
    iput-boolean v6, p0, Lde/tavendo/autobahn/WebSocketReader;->mStopped:Z

    goto :goto_3

    :cond_4
    move v7, v6

    .line 551
    goto :goto_4

    .line 510
    .end local v0    # "oldPosition":I
    .end local v3    # "serverError":Z
    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto/16 :goto_0
.end method


# virtual methods
.method protected notify(Ljava/lang/Object;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 118
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketConnectionHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 119
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 120
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketReader;->mWebSocketConnectionHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 121
    return-void
.end method

.method protected onBinaryMessage([B)V
    .locals 1
    .param p1, "payload"    # [B

    .prologue
    .line 500
    new-instance v0, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;

    invoke-direct {v0, p1}, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;-><init>([B)V

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    .line 501
    return-void
.end method

.method protected onClose(ILjava/lang/String;)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 439
    new-instance v0, Lde/tavendo/autobahn/WebSocketMessage$Close;

    invoke-direct {v0, p1, p2}, Lde/tavendo/autobahn/WebSocketMessage$Close;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    .line 440
    return-void
.end method

.method protected onHandshake(Z)V
    .locals 1
    .param p1, "success"    # Z

    .prologue
    .line 430
    new-instance v0, Lde/tavendo/autobahn/WebSocketMessage$ServerHandshake;

    invoke-direct {v0, p1}, Lde/tavendo/autobahn/WebSocketMessage$ServerHandshake;-><init>(Z)V

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    .line 431
    return-void
.end method

.method protected onPing([B)V
    .locals 1
    .param p1, "payload"    # [B

    .prologue
    .line 450
    new-instance v0, Lde/tavendo/autobahn/WebSocketMessage$Ping;

    invoke-direct {v0, p1}, Lde/tavendo/autobahn/WebSocketMessage$Ping;-><init>([B)V

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    .line 451
    return-void
.end method

.method protected onPong([B)V
    .locals 1
    .param p1, "payload"    # [B

    .prologue
    .line 461
    new-instance v0, Lde/tavendo/autobahn/WebSocketMessage$Pong;

    invoke-direct {v0, p1}, Lde/tavendo/autobahn/WebSocketMessage$Pong;-><init>([B)V

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    .line 462
    return-void
.end method

.method protected onRawTextMessage([B)V
    .locals 1
    .param p1, "payload"    # [B

    .prologue
    .line 489
    new-instance v0, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;

    invoke-direct {v0, p1}, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;-><init>([B)V

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    .line 490
    return-void
.end method

.method protected onTextMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "payload"    # Ljava/lang/String;

    .prologue
    .line 475
    new-instance v0, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;

    invoke-direct {v0, p1}, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    .line 476
    return-void
.end method

.method public quit()V
    .locals 2

    .prologue
    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lde/tavendo/autobahn/WebSocketReader;->mStopped:Z

    .line 106
    sget-object v0, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    const-string v1, "quit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    return-void
.end method

.method public run()V
    .locals 6

    .prologue
    .line 615
    monitor-enter p0

    .line 616
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 615
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619
    const/4 v2, 0x0

    .line 621
    .local v2, "inputStream":Ljava/io/InputStream;
    :try_start_1
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketReader;->mSocket:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    .line 627
    iput-object v2, p0, Lde/tavendo/autobahn/WebSocketReader;->mInputStream:Ljava/io/InputStream;

    .line 629
    sget-object v3, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    const-string v4, "WebSocker reader running."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 632
    :goto_0
    iget-boolean v3, p0, Lde/tavendo/autobahn/WebSocketReader;->mStopped:Z

    if-eqz v3, :cond_0

    .line 672
    sget-object v3, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    const-string v4, "WebSocket reader ended."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    :goto_1
    return-void

    .line 615
    .end local v2    # "inputStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 622
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 623
    .local v1, "e":Ljava/io/IOException;
    sget-object v3, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 635
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :try_start_3
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketReader;->mInputStream:Ljava/io/InputStream;

    iget-object v4, p0, Lde/tavendo/autobahn/WebSocketReader;->mNetworkBuffer:[B

    invoke-virtual {v3, v4}, Ljava/io/InputStream;->read([B)I

    move-result v0

    .line 636
    .local v0, "bytesRead":I
    if-lez v0, :cond_2

    .line 637
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketReader;->mApplicationBuffer:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lde/tavendo/autobahn/WebSocketReader;->mNetworkBuffer:[B

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 638
    :cond_1
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketReader;->consumeData()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 640
    :cond_2
    const/4 v3, -0x1

    if-ne v0, v3, :cond_3

    .line 641
    sget-object v3, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    const-string v4, "run() : ConnectionLost"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    new-instance v3, Lde/tavendo/autobahn/WebSocketMessage$ConnectionLost;

    invoke-direct {v3}, Lde/tavendo/autobahn/WebSocketMessage$ConnectionLost;-><init>()V

    invoke-virtual {p0, v3}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    .line 644
    const/4 v3, 0x1

    iput-boolean v3, p0, Lde/tavendo/autobahn/WebSocketReader;->mStopped:Z
    :try_end_3
    .catch Lde/tavendo/autobahn/WebSocketException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    .line 649
    .end local v0    # "bytesRead":I
    :catch_1
    move-exception v1

    .line 650
    .local v1, "e":Lde/tavendo/autobahn/WebSocketException;
    sget-object v3, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "run() : WebSocketException ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    new-instance v3, Lde/tavendo/autobahn/WebSocketMessage$ProtocolViolation;

    invoke-direct {v3, v1}, Lde/tavendo/autobahn/WebSocketMessage$ProtocolViolation;-><init>(Lde/tavendo/autobahn/WebSocketException;)V

    invoke-virtual {p0, v3}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    goto :goto_0

    .line 646
    .end local v1    # "e":Lde/tavendo/autobahn/WebSocketException;
    .restart local v0    # "bytesRead":I
    :cond_3
    :try_start_4
    sget-object v3, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    const-string v4, "WebSocketReader read() failed."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Lde/tavendo/autobahn/WebSocketException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    .line 654
    .end local v0    # "bytesRead":I
    :catch_2
    move-exception v1

    .line 655
    .local v1, "e":Ljava/net/SocketException;
    sget-object v3, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "run() : SocketException ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    new-instance v3, Lde/tavendo/autobahn/WebSocketMessage$ConnectionLost;

    invoke-direct {v3}, Lde/tavendo/autobahn/WebSocketMessage$ConnectionLost;-><init>()V

    invoke-virtual {p0, v3}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 659
    .end local v1    # "e":Ljava/net/SocketException;
    :catch_3
    move-exception v1

    .line 660
    .local v1, "e":Ljava/io/IOException;
    sget-object v3, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "run() : IOException ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    new-instance v3, Lde/tavendo/autobahn/WebSocketMessage$ConnectionLost;

    invoke-direct {v3}, Lde/tavendo/autobahn/WebSocketMessage$ConnectionLost;-><init>()V

    invoke-virtual {p0, v3}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 663
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 664
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lde/tavendo/autobahn/WebSocketReader;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "run() : Exception ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    new-instance v3, Lde/tavendo/autobahn/WebSocketMessage$Error;

    invoke-direct {v3, v1}, Lde/tavendo/autobahn/WebSocketMessage$Error;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {p0, v3}, Lde/tavendo/autobahn/WebSocketReader;->notify(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
