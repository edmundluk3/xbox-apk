.class public Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPage;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "SettingsNotificationsPage.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 38
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070bef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 19
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 21
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPage;->onCreateContentView()V

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPage;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 24
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f0301fa

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPage;->setContentView(I)V

    .line 34
    return-void
.end method
