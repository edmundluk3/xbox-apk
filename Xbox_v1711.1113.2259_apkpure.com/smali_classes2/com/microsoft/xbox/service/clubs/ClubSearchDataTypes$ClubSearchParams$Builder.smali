.class public abstract Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;
.super Ljava/lang/Object;
.source "ClubSearchDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
.end method

.method public abstract query(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract tags(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;"
        }
    .end annotation
.end method

.method public abstract titles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;"
        }
    .end annotation
.end method
