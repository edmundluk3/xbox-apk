.class final synthetic Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl$$Lambda$1;->arg$1:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl$$Lambda$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl$$Lambda$1;-><init>(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepositoryImpl$$Lambda$1;->arg$1:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterDataMapper;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    return-object v0
.end method
