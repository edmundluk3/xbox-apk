.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubBackgroundAchievementDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetUserClubsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$1;

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;)V

    return-void
.end method

.method static synthetic lambda$loadDataInBackground$0(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z
    .locals 1
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 147
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->hasTargetRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;->loadDataInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected loadDataInBackground()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 142
    .local v1, "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_1

    .line 143
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadClubs(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 145
    .local v0, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 149
    .end local v0    # "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    :goto_0
    return-object v2

    .line 147
    .restart local v0    # "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v2

    goto :goto_0

    .line 149
    .end local v0    # "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;->onError()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 123
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "clubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->access$400(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;Ljava/util/List;)V

    .line 161
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->access$300(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;)Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 156
    return-void
.end method
