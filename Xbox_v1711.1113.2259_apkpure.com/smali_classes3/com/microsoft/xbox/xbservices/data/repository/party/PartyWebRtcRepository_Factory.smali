.class public final Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;
.super Ljava/lang/Object;
.source "PartyWebRtcRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final loggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;"
        }
    .end annotation
.end field

.field private final peerConnectionClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;",
            ">;"
        }
    .end annotation
.end field

.field private final telemetryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "contextProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/Context;>;"
    .local p2, "loggerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;>;"
    .local p3, "peerConnectionClientProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;>;"
    .local p4, "telemetryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 31
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->loggerProvider:Ljavax/inject/Provider;

    .line 33
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->peerConnectionClientProvider:Ljavax/inject/Provider;

    .line 35
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->telemetryProvider:Ljavax/inject/Provider;

    .line 37
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "contextProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/Context;>;"
    .local p1, "loggerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;>;"
    .local p2, "peerConnectionClientProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;>;"
    .local p3, "telemetryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;
    .locals 5

    .prologue
    .line 41
    new-instance v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 42
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->loggerProvider:Ljavax/inject/Provider;

    .line 43
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->peerConnectionClientProvider:Ljavax/inject/Provider;

    .line 44
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->telemetryProvider:Ljavax/inject/Provider;

    .line 45
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V

    .line 41
    return-object v4
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository_Factory;->get()Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    move-result-object v0

    return-object v0
.end method
