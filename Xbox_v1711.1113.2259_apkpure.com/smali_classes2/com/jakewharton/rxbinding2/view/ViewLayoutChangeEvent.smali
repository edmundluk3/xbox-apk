.class public abstract Lcom/jakewharton/rxbinding2/view/ViewLayoutChangeEvent;
.super Ljava/lang/Object;
.source "ViewLayoutChangeEvent.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public static create(Landroid/view/View;IIIIIIII)Lcom/jakewharton/rxbinding2/view/ViewLayoutChangeEvent;
    .locals 10
    .param p0, "view"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .param p5, "oldLeft"    # I
    .param p6, "oldTop"    # I
    .param p7, "oldRight"    # I
    .param p8, "oldBottom"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 20
    new-instance v0, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewLayoutChangeEvent;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/jakewharton/rxbinding2/view/AutoValue_ViewLayoutChangeEvent;-><init>(Landroid/view/View;IIIIIIII)V

    return-object v0
.end method


# virtual methods
.method public abstract bottom()I
.end method

.method public abstract left()I
.end method

.method public abstract oldBottom()I
.end method

.method public abstract oldLeft()I
.end method

.method public abstract oldRight()I
.end method

.method public abstract oldTop()I
.end method

.method public abstract right()I
.end method

.method public abstract top()I
.end method

.method public abstract view()Landroid/view/View;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
