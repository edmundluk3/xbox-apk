.class public Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MultiplayerSessionModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GetLfgSessionsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
        ">;>;"
    }
.end annotation


# instance fields
.field private clubIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

.field private final lfgListType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

.field private final scid:Ljava/lang/String;

.field final synthetic this$1:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Ljava/util/List;Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "filters"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "lfgListType"    # Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 567
    .local p4, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->this$1:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 568
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->scid:Ljava/lang/String;

    .line 569
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    .line 570
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->clubIds:Ljava/util/List;

    .line 571
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->lfgListType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    .line 572
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 558
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->buildData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 580
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v10

    .line 581
    .local v10, "meModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    const/4 v6, 0x0

    .line 583
    .local v6, "sessionMembers":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionMembers;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    if-nez v3, :cond_0

    .line 584
    new-instance v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    .line 587
    :cond_0
    if-nez v10, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->lfgListType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    if-eqz v3, :cond_1

    .line 588
    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0xbbb

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v3

    .line 589
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->lfgListType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    sget-object v4, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    if-ne v3, v4, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->clubIds:Ljava/util/List;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 593
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadClubs(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 594
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/ProfileModel;->getClubs()Ljava/util/List;

    move-result-object v16

    .line 596
    .local v16, "userClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-static/range {v16 .. v16}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 597
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/ProfileModel;->getClubs()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->clubIds:Ljava/util/List;

    .line 598
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 599
    .local v8, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v12

    .line 600
    .local v12, "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-eq v4, v5, :cond_2

    .line 601
    invoke-static {v12}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 602
    invoke-virtual {v12}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 603
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->clubIds:Ljava/util/List;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 608
    .end local v8    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v12    # "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setHideFull(Z)V

    .line 619
    .end local v16    # "userClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :goto_1
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 622
    .local v17, "validSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    sget-object v3, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/SocialTagModel;->loadSystemTagsSync(Z)V

    .line 624
    new-instance v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleParameters;

    sget-object v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleType;->Search:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleType;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->scid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->clubIds:Ljava/util/List;

    invoke-direct/range {v2 .. v7}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleParameters;-><init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleType;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionMembers;Ljava/util/List;)V

    .line 631
    .local v2, "handleParameters":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleParameters;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->this$1:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->this$0:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    .line 632
    invoke-interface {v3, v2}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->getMultiplayerSessions(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleParameters;)Lio/reactivex/Single;

    move-result-object v3

    invoke-virtual {v3}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerQueryResponse;

    .line 634
    .local v11, "sessionsResponse":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerQueryResponse;
    if-eqz v11, :cond_c

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 635
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 637
    .local v15, "titlesToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 638
    .local v9, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    if-eqz v9, :cond_4

    .line 640
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 641
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v15, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 644
    :cond_5
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->membersCount()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 645
    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 609
    .end local v2    # "handleParameters":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleParameters;
    .end local v9    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v11    # "sessionsResponse":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerQueryResponse;
    .end local v15    # "titlesToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v17    # "validSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->lfgListType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    sget-object v4, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    if-ne v3, v4, :cond_7

    .line 610
    sget-object v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMoniker;->Following:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMoniker;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerPeople;->with(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMoniker;Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerPeople;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionMembers;->with(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerPeople;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionMembers;

    move-result-object v6

    .line 611
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setHideFull(Z)V

    goto/16 :goto_1

    .line 612
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->lfgListType:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    sget-object v4, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    if-ne v3, v4, :cond_8

    .line 613
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setMemberXuid(Ljava/lang/Long;)V

    .line 614
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setHideFull(Z)V

    goto/16 :goto_1

    .line 616
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->filters:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->setHideFull(Z)V

    goto/16 :goto_1

    .line 650
    .restart local v2    # "handleParameters":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleParameters;
    .restart local v11    # "sessionsResponse":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerQueryResponse;
    .restart local v15    # "titlesToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .restart local v17    # "validSessions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    :cond_9
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v15}, Lcom/microsoft/xbox/service/model/TitleHubModel;->load(ZLjava/util/Set;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 652
    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Long;

    .line 653
    .local v14, "titleId":Ljava/lang/Long;
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v4

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v13

    .line 654
    .local v13, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v13, :cond_a

    .line 655
    invoke-virtual {v13}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 656
    iget-wide v4, v13, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgressXboxoneAchievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    goto :goto_3

    .line 658
    :cond_b
    iget-wide v4, v13, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgress360Achievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    goto :goto_3

    .line 663
    .end local v13    # "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .end local v14    # "titleId":Ljava/lang/Long;
    .end local v15    # "titlesToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_c
    return-object v17
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 673
    const-wide/16 v0, 0x251c

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 668
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel$GetLfgSessionsRunner;->this$1:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$MultiplayerSessionModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 669
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 576
    return-void
.end method
