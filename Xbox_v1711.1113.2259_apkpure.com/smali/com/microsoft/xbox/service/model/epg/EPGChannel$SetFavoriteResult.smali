.class public final enum Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
.super Ljava/lang/Enum;
.source "EPGChannel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SetFavoriteResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

.field public static final enum errorCannotRemove:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

.field public static final enum errorCannotSet:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

.field public static final enum errorFullList:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

.field public static final enum resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 169
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    const-string/jumbo v1, "resultNoError"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    const-string v1, "errorCannotSet"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorCannotSet:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    const-string v1, "errorCannotRemove"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorCannotRemove:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    const-string v1, "errorFullList"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorFullList:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .line 168
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorCannotSet:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorCannotRemove:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorFullList:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->$VALUES:[Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 168
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 168
    const-class v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->$VALUES:[Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    return-object v0
.end method
