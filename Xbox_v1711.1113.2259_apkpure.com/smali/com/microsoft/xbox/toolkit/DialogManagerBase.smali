.class public abstract Lcom/microsoft/xbox/toolkit/DialogManagerBase;
.super Ljava/lang/Object;
.source "DialogManagerBase.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private appBarMenu:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

.field private blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

.field private cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

.field private final dialogShownListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;",
            ">;"
        }
    .end annotation
.end field

.field private dialogStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;",
            ">;"
        }
    .end annotation
.end field

.field private isEnabled:Z

.field private visibleToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogShownListeners:Ljava/util/List;

    .line 39
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 40
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private buildDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "promptText"    # Ljava/lang/String;
    .param p3, "okText"    # Ljava/lang/String;
    .param p4, "okHandler"    # Ljava/lang/Runnable;
    .param p5, "cancelText"    # Ljava/lang/String;
    .param p6, "cancelHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 467
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;-><init>(Landroid/content/Context;)V

    .line 468
    .local v0, "dialog":Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 469
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 470
    const/4 v2, -0x1

    invoke-static {p3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v3

    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/DialogManagerBase$$Lambda$2;->lambdaFactory$(Ljava/lang/Runnable;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 475
    invoke-static {p0, v0, p6}, Lcom/microsoft/xbox/toolkit/DialogManagerBase$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/DialogManagerBase;Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v1

    .line 482
    .local v1, "wrappedCancelHandler":Ljava/lang/Runnable;
    const/4 v2, -0x2

    invoke-static {p5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v3

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/DialogManagerBase$$Lambda$4;->lambdaFactory$(Ljava/lang/Runnable;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 486
    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 487
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->setCancelable(Z)V

    .line 493
    :goto_0
    return-object v0

    .line 490
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->setCancelable(Z)V

    .line 491
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/DialogManagerBase$$Lambda$5;->lambdaFactory$(Ljava/lang/Runnable;)Landroid/content/DialogInterface$OnCancelListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0
.end method

.method static synthetic lambda$buildDialog$1(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V
    .locals 0
    .param p0, "okHandler"    # Ljava/lang/Runnable;
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 471
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 472
    return-void
.end method

.method static synthetic lambda$buildDialog$2(Lcom/microsoft/xbox/toolkit/DialogManagerBase;Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/DialogManagerBase;
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;
    .param p2, "cancelHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 476
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 477
    if-eqz p2, :cond_0

    .line 478
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 480
    :cond_0
    return-void
.end method

.method static synthetic lambda$buildDialog$3(Ljava/lang/Runnable;Landroid/content/DialogInterface;I)V
    .locals 0
    .param p0, "wrappedCancelHandler"    # Ljava/lang/Runnable;
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 483
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 484
    return-void
.end method

.method static synthetic lambda$buildDialog$4(Ljava/lang/Runnable;Landroid/content/DialogInterface;)V
    .locals 0
    .param p0, "wrappedCancelHandler"    # Ljava/lang/Runnable;
    .param p1, "dialog1"    # Landroid/content/DialogInterface;

    .prologue
    .line 491
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$setCancelableBlocking$0(Lcom/microsoft/xbox/toolkit/DialogManagerBase;Ljava/lang/Runnable;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/DialogManagerBase;
    .param p1, "cancelRunnable"    # Ljava/lang/Runnable;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;->dismiss()V

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    .line 295
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 296
    return-void
.end method

.method private showDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 10
    .param p1, "type"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "promptText"    # Ljava/lang/String;
    .param p4, "okText"    # Ljava/lang/String;
    .param p5, "okHandler"    # Ljava/lang/Runnable;
    .param p6, "cancelText"    # Ljava/lang/String;
    .param p7, "cancelHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 182
    sget-object v0, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;->FATAL:Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;

    if-ne p1, v0, :cond_0

    .line 183
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->forceDismissAll()V

    .line 186
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    if-nez v0, :cond_2

    .line 187
    const-string v0, "Dialog manager isn\'t enabled"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 218
    :cond_1
    :goto_0
    return-void

    .line 191
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    const-string v0, "Application is finishing"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_4

    sget-object v0, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;->NORMAL:Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;

    if-ne p1, v0, :cond_4

    .line 197
    const-string v0, "Must dismiss other dialogs first."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    .line 201
    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->buildDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;

    move-result-object v7

    .line 202
    .local v7, "dialog":Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;
    invoke-virtual {v7, p1}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->setDialogType(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;)V

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v0, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->show()V

    .line 206
    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 209
    const/4 v0, -0x1

    invoke-virtual {v7, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v9

    .line 210
    .local v9, "positive":Landroid/widget/Button;
    if-eqz v9, :cond_5

    .line 211
    sget-object v0, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v9, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 214
    :cond_5
    const/4 v0, -0x2

    invoke-virtual {v7, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedAlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v8

    .line 215
    .local v8, "negative":Landroid/widget/Button;
    if-eqz v8, :cond_1

    .line 216
    sget-object v0, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v8, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method


# virtual methods
.method public addDialogShownListener(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 439
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 441
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogShownListeners:Ljava/util/List;

    monitor-enter v1

    .line 442
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogShownListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogShownListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 445
    :cond_0
    monitor-exit v1

    .line 446
    return-void

    .line 445
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 2
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 129
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "the dialog manager is disabled, nothing shows"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public dismissAppBar()V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->appBarMenu:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->appBarMenu:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->dismiss()V

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->appBarMenu:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    .line 428
    :cond_0
    return-void
.end method

.method public dismissBlocking()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 406
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;->dismiss()V

    .line 408
    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    if-eqz v0, :cond_1

    .line 412
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;->dismiss()V

    .line 413
    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    .line 415
    :cond_1
    return-void
.end method

.method public dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 1
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    .line 144
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 146
    :cond_0
    return-void
.end method

.method public dismissToast()V
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->visibleToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->visibleToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 367
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->visibleToast:Landroid/widget/Toast;

    .line 369
    :cond_0
    return-void
.end method

.method public dismissTopNonFatalAlert()V
    .locals 3

    .prologue
    .line 393
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;->getDialogType()Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;->FATAL:Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;

    if-eq v1, v2, :cond_0

    .line 394
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .line 395
    .local v0, "dialog":Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;
    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 397
    .end local v0    # "dialog":Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;
    :cond_0
    return-void
.end method

.method public forceDismissAlerts()V
    .locals 2

    .prologue
    .line 378
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 379
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .line 381
    .local v0, "dialog":Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;
    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;->quickDismiss()V

    goto :goto_0

    .line 383
    .end local v0    # "dialog":Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;
    :cond_0
    return-void
.end method

.method public forceDismissAll()V
    .locals 0

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dismissToast()V

    .line 353
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->forceDismissAlerts()V

    .line 354
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dismissBlocking()V

    .line 355
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dismissAppBar()V

    .line 356
    return-void
.end method

.method public getAppBarMenu()Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->appBarMenu:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    return-object v0
.end method

.method public getIsBlocking()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVisibleDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 4
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 458
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 459
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogShownListeners:Ljava/util/List;

    monitor-enter v2

    .line 460
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogShownListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;

    .line 461
    .local v0, "listener":Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;->onDialogShown(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0

    .line 463
    .end local v0    # "listener":Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 464
    return-void
.end method

.method public onAppBarDismissed()V
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->appBarMenu:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    .line 343
    return-void
.end method

.method public onDialogStopped(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 1
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    .line 151
    return-void
.end method

.method public removeDialogShownListener(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager$DialogShownListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 450
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 452
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogShownListeners:Ljava/util/List;

    monitor-enter v1

    .line 453
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogShownListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 454
    monitor-exit v1

    .line 455
    return-void

    .line 454
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setBlocking(ZLjava/lang/String;)V
    .locals 2
    .param p1, "visible"    # Z
    .param p2, "statusText"    # Ljava/lang/String;

    .prologue
    .line 261
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 262
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    if-eqz v0, :cond_1

    .line 263
    if-eqz p1, :cond_3

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    if-nez v0, :cond_0

    .line 265
    sget-object v0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->TAG:Ljava/lang/String;

    const-string v1, "blocking spinner null, create new one"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;->show(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 277
    :cond_1
    :goto_1
    return-void

    .line 261
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 272
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;->dismiss()V

    .line 274
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->blockingSpinner:Lcom/microsoft/xbox/toolkit/ui/BlockingScreen;

    goto :goto_1
.end method

.method public setCancelableBlocking(ZLjava/lang/String;Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "visible"    # Z
    .param p2, "statusText"    # Ljava/lang/String;
    .param p3, "cancelRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 286
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 287
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    if-eqz v0, :cond_1

    .line 288
    if-eqz p1, :cond_3

    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    if-nez v0, :cond_0

    .line 290
    sget-object v0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->TAG:Ljava/lang/String;

    const-string v1, "cancelable blocking dialog null, create new one"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    invoke-static {p0, p3}, Lcom/microsoft/xbox/toolkit/DialogManagerBase$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/DialogManagerBase;Ljava/lang/Runnable;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;->setCancelButtonAction(Landroid/view/View$OnClickListener;)V

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;->show(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 304
    :cond_1
    :goto_1
    return-void

    .line 286
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 299
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;->dismiss()V

    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->cancelableBlockingDialog:Lcom/microsoft/xbox/toolkit/ui/CancellableBlockingScreen;

    goto :goto_1
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    if-eq v0, p1, :cond_0

    .line 73
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    .line 75
    :cond_0
    return-void
.end method

.method protected shouldDismissAllBeforeOpeningADialog()Z
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x1

    return v0
.end method

.method public showAppBarMenu(Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;)V
    .locals 2
    .param p1, "appBar"    # Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    .prologue
    .line 313
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    if-eqz v1, :cond_1

    .line 314
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->appBarMenu:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    .line 315
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->appBarMenu:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 317
    .local v0, "window":Landroid/view/Window;
    if-eqz v0, :cond_0

    .line 318
    const v1, 0x7f080210

    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 321
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->appBarMenu:Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ExpandedAppBar;->show()V

    .line 323
    .end local v0    # "window":Landroid/view/Window;
    :cond_1
    return-void
.end method

.method public showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "promptText"    # Ljava/lang/String;
    .param p3, "okText"    # Ljava/lang/String;
    .param p4, "okHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 159
    sget-object v1, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;->FATAL:Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;

    const/4 v6, 0x0

    sget-object v7, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->showDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 160
    return-void
.end method

.method public showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "promptText"    # Ljava/lang/String;
    .param p3, "okText"    # Ljava/lang/String;
    .param p4, "okHandler"    # Ljava/lang/Runnable;
    .param p5, "cancelText"    # Ljava/lang/String;
    .param p6, "cancelHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 168
    sget-object v1, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;->FATAL:Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->showDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 169
    return-void
.end method

.method public showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    .locals 5
    .param p1, "dialog"    # Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->shouldDismissAllBeforeOpeningADialog()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->forceDismissAll()V

    .line 88
    :cond_0
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    if-eqz v3, :cond_6

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 91
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 92
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dialogStack:Ljava/util/Stack;

    invoke-virtual {v3, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    :try_start_0
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    .line 95
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    .end local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :goto_0
    return-void

    .line 96
    .restart local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :catch_0
    move-exception v1

    .line 97
    .local v1, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 98
    .local v2, "msg":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v3, "Adding window failed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 100
    :cond_1
    throw v1

    .line 103
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->TAG:Ljava/lang/String;

    invoke-static {v3, v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 106
    .end local v1    # "e":Ljava/lang/RuntimeException;
    .end local v2    # "msg":Ljava/lang/String;
    :cond_3
    if-nez v0, :cond_4

    .line 107
    sget-object v3, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->TAG:Ljava/lang/String;

    const-string v4, "XLEApplication.MainActivity == null!"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_4
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 109
    sget-object v3, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->TAG:Ljava/lang/String;

    const-string v4, "XLEApplication.MainActivity is finishing!"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_5
    sget-object v3, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->TAG:Ljava/lang/String;

    const-string v4, "XLEApplication.MainActivity is not alive!"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    .end local v0    # "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    :cond_6
    sget-object v3, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "the dialog manager is disabled, nothing shows"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "promptText"    # Ljava/lang/String;
    .param p3, "okText"    # Ljava/lang/String;
    .param p4, "okHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 173
    sget-object v1, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;->NON_FATAL:Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;

    const/4 v6, 0x0

    sget-object v7, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->showDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 174
    return-void
.end method

.method public showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "promptText"    # Ljava/lang/String;
    .param p3, "okText"    # Ljava/lang/String;
    .param p4, "okHandler"    # Ljava/lang/Runnable;
    .param p5, "cancelText"    # Ljava/lang/String;
    .param p6, "cancelHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 178
    sget-object v1, Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;->NORMAL:Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->showDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog$DialogType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 179
    return-void
.end method

.method public showToast(I)V
    .locals 1
    .param p1, "contentResId"    # I

    .prologue
    .line 228
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->showToast(II)V

    .line 229
    return-void
.end method

.method public showToast(II)V
    .locals 2
    .param p1, "contentResId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 233
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 234
    .local v0, "message":Ljava/lang/String;
    invoke-virtual {p0, v0, p2}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->showToast(Ljava/lang/String;I)V

    .line 235
    return-void
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 239
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->showToast(Ljava/lang/String;I)V

    .line 240
    return-void
.end method

.method public showToast(Ljava/lang/String;I)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->dismissToast()V

    .line 247
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 248
    .local v0, "mainActivity":Lcom/microsoft/xbox/xle/app/MainActivity;
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->isEnabled:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-static {v0, v1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->visibleToast:Landroid/widget/Toast;

    .line 250
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->visibleToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 252
    :cond_0
    return-void
.end method
