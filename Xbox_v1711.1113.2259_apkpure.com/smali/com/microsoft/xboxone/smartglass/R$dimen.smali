.class public final Lcom/microsoft/xboxone/smartglass/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xboxone/smartglass/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final AlbumDetailsactivityBodyScrollLeft:I = 0x7f090022

.field public static final GradientHeight:I = 0x7f090152

.field public static final GradientWidth:I = 0x7f090153

.field public static final TS_C7_5_DetailsTitle:I = 0x7f090023

.field public static final TS_SUI24L:I = 0x7f090154

.field public static final abc_action_bar_content_inset_material:I = 0x7f09012d

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f09012e

.field public static final abc_action_bar_default_height_material:I = 0x7f090001

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f09012f

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f090130

.field public static final abc_action_bar_elevation_material:I = 0x7f090155

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f090156

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f090157

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f090158

.field public static final abc_action_bar_progress_bar_size:I = 0x7f090002

.field public static final abc_action_bar_stacked_max_height:I = 0x7f090159

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f09015a

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f09015b

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f09015c

.field public static final abc_action_button_min_height_material:I = 0x7f09015d

.field public static final abc_action_button_min_width_material:I = 0x7f09015e

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f09015f

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f090000

.field public static final abc_button_inset_horizontal_material:I = 0x7f090160

.field public static final abc_button_inset_vertical_material:I = 0x7f090161

.field public static final abc_button_padding_horizontal_material:I = 0x7f090162

.field public static final abc_button_padding_vertical_material:I = 0x7f090163

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f090164

.field public static final abc_config_prefDialogWidth:I = 0x7f090005

.field public static final abc_control_corner_material:I = 0x7f090165

.field public static final abc_control_inset_material:I = 0x7f090166

.field public static final abc_control_padding_material:I = 0x7f090167

.field public static final abc_dialog_fixed_height_major:I = 0x7f090006

.field public static final abc_dialog_fixed_height_minor:I = 0x7f090007

.field public static final abc_dialog_fixed_width_major:I = 0x7f090008

.field public static final abc_dialog_fixed_width_minor:I = 0x7f090009

.field public static final abc_dialog_list_padding_bottom_no_buttons:I = 0x7f090168

.field public static final abc_dialog_list_padding_top_no_title:I = 0x7f090169

.field public static final abc_dialog_min_width_major:I = 0x7f09000a

.field public static final abc_dialog_min_width_minor:I = 0x7f09000b

.field public static final abc_dialog_padding_material:I = 0x7f09016a

.field public static final abc_dialog_padding_top_material:I = 0x7f09016b

.field public static final abc_dialog_title_divider_material:I = 0x7f09016c

.field public static final abc_disabled_alpha_material_dark:I = 0x7f09016d

.field public static final abc_disabled_alpha_material_light:I = 0x7f09016e

.field public static final abc_dropdownitem_icon_width:I = 0x7f09016f

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f090170

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f090171

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f090172

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f090173

.field public static final abc_edit_text_inset_top_material:I = 0x7f090174

.field public static final abc_floating_window_z:I = 0x7f090175

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f090176

.field public static final abc_panel_menu_list_width:I = 0x7f090177

.field public static final abc_progress_bar_height_material:I = 0x7f090178

.field public static final abc_search_view_preferred_height:I = 0x7f090179

.field public static final abc_search_view_preferred_width:I = 0x7f09017a

.field public static final abc_seekbar_track_background_height_material:I = 0x7f09017b

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f09017c

.field public static final abc_select_dialog_padding_start_material:I = 0x7f09017d

.field public static final abc_switch_padding:I = 0x7f090148

.field public static final abc_text_size_body_1_material:I = 0x7f09017e

.field public static final abc_text_size_body_2_material:I = 0x7f09017f

.field public static final abc_text_size_button_material:I = 0x7f090180

.field public static final abc_text_size_caption_material:I = 0x7f090181

.field public static final abc_text_size_display_1_material:I = 0x7f090182

.field public static final abc_text_size_display_2_material:I = 0x7f090183

.field public static final abc_text_size_display_3_material:I = 0x7f090184

.field public static final abc_text_size_display_4_material:I = 0x7f090185

.field public static final abc_text_size_headline_material:I = 0x7f090186

.field public static final abc_text_size_large_material:I = 0x7f090187

.field public static final abc_text_size_medium_material:I = 0x7f090188

.field public static final abc_text_size_menu_header_material:I = 0x7f090189

.field public static final abc_text_size_menu_material:I = 0x7f09018a

.field public static final abc_text_size_small_material:I = 0x7f09018b

.field public static final abc_text_size_subhead_material:I = 0x7f09018c

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f090003

.field public static final abc_text_size_title_material:I = 0x7f09018d

.field public static final abc_text_size_title_material_toolbar:I = 0x7f090004

.field public static final accountPickerMargin:I = 0x7f09018e

.field public static final achievementCompareProfileItemHeight:I = 0x7f09018f

.field public static final achievementDetailRewardIconSize:I = 0x7f090190

.field public static final achievementGameScoreMarginTop:I = 0x7f090191

.field public static final achievementPieChartMarginRight:I = 0x7f090192

.field public static final achievementPieChartMarginTop:I = 0x7f090193

.field public static final achievementProfileGameImageHeight:I = 0x7f090194

.field public static final achievementProfileGameImageWidth:I = 0x7f090195

.field public static final achievementProfileItemHeight:I = 0x7f090196

.field public static final achievementProfileItemWidth:I = 0x7f090197

.field public static final achievementProfileTitleWidth:I = 0x7f090198

.field public static final achievementStatItemHeight:I = 0x7f090199

.field public static final achievementsGameBoxArtHeight:I = 0x7f090024

.field public static final achievementsGameBoxArtWidth:I = 0x7f090025

.field public static final achievementsGameTileHeight:I = 0x7f090026

.field public static final achievementsGameTileMarginBottom:I = 0x7f090027

.field public static final achievementsGameTileMarginRight:I = 0x7f090028

.field public static final achievementsGameTileMarginTop:I = 0x7f090029

.field public static final achievementsGameTileWidth:I = 0x7f09002a

.field public static final achievementsPieChartHeight:I = 0x7f09019a

.field public static final achievementsPieChartWidth:I = 0x7f09019b

.field public static final achievementsRootRightMargin:I = 0x7f09019c

.field public static final activityFeedActionsListItemImageSize:I = 0x7f09019d

.field public static final activityFeedActionsListItemMarginLeft:I = 0x7f09019e

.field public static final activityFeedActionsListItemMarginRight:I = 0x7f09019f

.field public static final activityFeedActionsListItemPaddingBottom:I = 0x7f0901a0

.field public static final activityFeedActionsListItemPaddingTop:I = 0x7f0901a1

.field public static final activityFeedActionsScreenCommentsMarginBottom:I = 0x7f0901a2

.field public static final activityFeedActionsScreenEmptyStatePadding:I = 0x7f0901a3

.field public static final activityFeedActionsScreenItemMarginTop:I = 0x7f0901a4

.field public static final activityFeedActionsScreenMarginLeft:I = 0x7f0901a5

.field public static final activityFeedActionsScreenMarginRight:I = 0x7f0901a6

.field public static final activityFeedActionsScreenMarginTop:I = 0x7f0901a7

.field public static final activityFeedAlertImageSize:I = 0x7f0901a8

.field public static final activityFeedAlertPaddingBottom:I = 0x7f0901a9

.field public static final activityFeedCommentPostTextEntryFontSize:I = 0x7f0901aa

.field public static final activityFeedCommentPostTextEntryMargin:I = 0x7f0901ab

.field public static final activityFeedGamerTagDrawablePadding:I = 0x7f0901ac

.field public static final activityFeedSharedItemMarginLeft:I = 0x7f0901ad

.field public static final activityFeedSharedItemMarginRight:I = 0x7f0901ae

.field public static final activityFeedSharedItemMarginTop:I = 0x7f0901af

.field public static final activityFeedStatusPostMarginLeft:I = 0x7f0901b0

.field public static final activityFeedStatusPostMarginRight:I = 0x7f0901b1

.field public static final activityFeedStatusPostPaddingBottom:I = 0x7f0901b2

.field public static final activityFeedStatusPostPaddingLeft:I = 0x7f0901b3

.field public static final activityFeedStatusPostPaddingRight:I = 0x7f0901b4

.field public static final activityFeedStatusPostPaddingTop:I = 0x7f0901b5

.field public static final activityGoldAlertButtonHeight:I = 0x7f09002b

.field public static final activityGoldAlertButtonWidth:I = 0x7f09002c

.field public static final activityListItemDividerHeight:I = 0x7f09002d

.field public static final activityListItemTileHeight:I = 0x7f09002e

.field public static final activityListItemTileMarginRight:I = 0x7f09002f

.field public static final activityListItemTileMarginTop:I = 0x7f090030

.field public static final activityListItemTileWidth:I = 0x7f090031

.field public static final activityOverviewTileHeight:I = 0x7f090032

.field public static final activityOverviewTileWidth:I = 0x7f090033

.field public static final activityPlayButtonHeight:I = 0x7f090034

.field public static final activityPlayButtonWidth:I = 0x7f090035

.field public static final activityPurchaseTileHeight:I = 0x7f090036

.field public static final activityPurchaseTileMargin:I = 0x7f090037

.field public static final activityScreensHeaderMarginStandard:I = 0x7f0901b6

.field public static final activityScreensMarginStandard:I = 0x7f09001b

.field public static final albumDetailTrackListDurationWidth:I = 0x7f090038

.field public static final albumDetailTrackListIndexWidth:I = 0x7f090039

.field public static final albumdetailBuyButtom:I = 0x7f09003a

.field public static final albumdetailBuyHeight:I = 0x7f09003b

.field public static final albumdetailBuyTop:I = 0x7f09003c

.field public static final albumdetailBuyWidth:I = 0x7f09003d

.field public static final albumdetailImageTittleHeight:I = 0x7f09003e

.field public static final albumdetailImageTittleWidth:I = 0x7f09003f

.field public static final albumdetail_rankHeight:I = 0x7f090040

.field public static final albumdetail_rankTop:I = 0x7f090041

.field public static final albumdetail_rankWidth:I = 0x7f090042

.field public static final alertImageSize:I = 0x7f0901b7

.field public static final applicationBarButtonHeight:I = 0x7f090043

.field public static final applicationBarButtonMarginLeft:I = 0x7f090044

.field public static final applicationBarButtonMarginRight:I = 0x7f090045

.field public static final applicationBarButtonWidth:I = 0x7f090046

.field public static final applicationBarDotsHeight:I = 0x7f090047

.field public static final applicationBarDotsPaddingBottom:I = 0x7f090048

.field public static final applicationBarDotsPaddingLeft:I = 0x7f090049

.field public static final applicationBarDotsPaddingRight:I = 0x7f09004a

.field public static final applicationBarDotsPaddingTop:I = 0x7f09004b

.field public static final applicationBarDotsWidth:I = 0x7f09004c

.field public static final applicationBarHeight:I = 0x7f09004d

.field public static final applicationBarHeightWithMediaProgressBar:I = 0x7f0901b8

.field public static final applicationBarMarginBottom:I = 0x7f09004e

.field public static final applicationBarMarginTop:I = 0x7f09004f

.field public static final applicationBarMenuItemMarginRight:I = 0x7f090050

.field public static final applicationBarMenuItemPaddingBottom:I = 0x7f090051

.field public static final applicationBarMenuItemPaddingLeft:I = 0x7f090052

.field public static final applicationBarMenuItemPaddingTop:I = 0x7f090053

.field public static final applicationBarPageIndicatorMarginBottom:I = 0x7f090054

.field public static final artistTileMarginBottom:I = 0x7f090055

.field public static final artistTileMarginRight:I = 0x7f090056

.field public static final artistTileMarginTop:I = 0x7f090057

.field public static final artistdetailImageTittleHeight:I = 0x7f090058

.field public static final artistdetailImageTittleWidth:I = 0x7f090059

.field public static final artistdetail_rankHeight:I = 0x7f09005a

.field public static final artistdetail_rankTop:I = 0x7f09005b

.field public static final artistdetail_rankWidth:I = 0x7f09005c

.field public static final attachmentGamerTagBarHeight:I = 0x7f0901b9

.field public static final attachmentProgressIndicatorSize:I = 0x7f0901ba

.field public static final attachmentProgressViewHeight:I = 0x7f0901bb

.field public static final attainmentDetailMarginBotton:I = 0x7f0901bc

.field public static final attainmentDetailSectionMarginBotton:I = 0x7f0901bd

.field public static final bioTextMarginBottom:I = 0x7f09005d

.field public static final blockingDialogTextMarginTop:I = 0x7f09005e

.field public static final blockingDialogWidth:I = 0x7f09005f

.field public static final browseResetButtonSize:I = 0x7f0901be

.field public static final browseStopButtonHeight:I = 0x7f0901bf

.field public static final browseStopButtonWidth:I = 0x7f0901c0

.field public static final buttonBorder:I = 0x7f0901c1

.field public static final buttonHeight:I = 0x7f0901c2

.field public static final canvasButtonHeight:I = 0x7f090060

.field public static final canvasButtonMarginLeft:I = 0x7f090061

.field public static final canvasButtonWidth:I = 0x7f090062

.field public static final canvasCompanionBarPadding:I = 0x7f0901c3

.field public static final canvasConnectButtonRightMargin:I = 0x7f0901c4

.field public static final canvasEllipsisBottomMargin:I = 0x7f0901c5

.field public static final canvasEllipsisRightMargin:I = 0x7f0901c6

.field public static final canvasMenuBarHeight:I = 0x7f0901c7

.field public static final canvasMenuBarWidth:I = 0x7f0901c8

.field public static final canvasSplashButton1TopMargin:I = 0x7f0901c9

.field public static final canvasSplashButton2TopMargin:I = 0x7f0901ca

.field public static final canvasSplashButtonTextLeftMargin:I = 0x7f0901cb

.field public static final canvasSplashButtonTextSize:I = 0x7f0901cc

.field public static final canvasSplashCancelButtonLeftMargin:I = 0x7f0901cd

.field public static final canvasSplashDescriptionTextSize:I = 0x7f0901ce

.field public static final canvasSplashLeftMargin:I = 0x7f0901cf

.field public static final canvasSplashTitleTextSize:I = 0x7f0901d0

.field public static final cardview_compat_inset_shadow:I = 0x7f0901d1

.field public static final cardview_default_elevation:I = 0x7f0901d2

.field public static final cardview_default_radius:I = 0x7f0901d3

.field public static final changeFriendshipButtonWidth:I = 0x7f0901d4

.field public static final changeFriendshipDialogMargin:I = 0x7f0901d5

.field public static final changeFriendshipDialogUserNameSize:I = 0x7f0901d6

.field public static final changeFriendshipDialogWidth:I = 0x7f0901d7

.field public static final channelDownHeight:I = 0x7f09000c

.field public static final channelDownWidth:I = 0x7f09000d

.field public static final channelReturnHeight:I = 0x7f09000e

.field public static final channelReturnWidth:I = 0x7f09000f

.field public static final channelTileEdgeMargin:I = 0x7f0901d8

.field public static final channelUpHeight:I = 0x7f090010

.field public static final channelUpWidth:I = 0x7f090011

.field public static final chartStrokeWidth:I = 0x7f0901d9

.field public static final chatMessagesContainerMarginRight:I = 0x7f0901da

.field public static final chooseColorCellMargin:I = 0x7f0901db

.field public static final closeButtonMarginTop:I = 0x7f0901dc

.field public static final closeButtonSize:I = 0x7f0901dd

.field public static final closeButttonMarginLeft:I = 0x7f0901de

.field public static final club_card_category_header_margin:I = 0x7f0901df

.field public static final club_card_category_height:I = 0x7f0901e0

.field public static final club_card_category_margin:I = 0x7f0901e1

.field public static final club_card_category_see_all_right_margin:I = 0x7f0901e2

.field public static final club_card_height:I = 0x7f0901e3

.field public static final club_card_image_height:I = 0x7f0901e4

.field public static final club_card_internal_margin:I = 0x7f0901e5

.field public static final club_card_internal_top_bottom_margin:I = 0x7f0901e6

.field public static final club_card_margin:I = 0x7f0901e7

.field public static final club_card_padding:I = 0x7f0901e8

.field public static final club_card_width:I = 0x7f0901e9

.field public static final club_customize_gridview_adjustment:I = 0x7f09001c

.field public static final club_home_background_image_height:I = 0x7f090063

.field public static final club_home_display_image_size:I = 0x7f0901ea

.field public static final club_home_gridview_adjustment:I = 0x7f09001d

.field public static final club_home_horizontal_margin:I = 0x7f0901eb

.field public static final club_home_image_text_margin:I = 0x7f0901ec

.field public static final club_home_title_image_size:I = 0x7f0901ed

.field public static final club_home_vertical_margin_small:I = 0x7f0901ee

.field public static final club_invitation_height:I = 0x7f0901ef

.field public static final club_invitation_margin:I = 0x7f0901f0

.field public static final club_invitation_width:I = 0x7f0901f1

.field public static final club_search_input_margin:I = 0x7f0901f2

.field public static final club_settings_title_margin:I = 0x7f0901f3

.field public static final club_text_layout_height:I = 0x7f0901f4

.field public static final collectionfilterMarginLeft:I = 0x7f0901f5

.field public static final com_facebook_auth_dialog_corner_radius:I = 0x7f0901f6

.field public static final com_facebook_auth_dialog_corner_radius_oversized:I = 0x7f0901f7

.field public static final com_facebook_button_corner_radius:I = 0x7f0901f8

.field public static final com_facebook_button_login_corner_radius:I = 0x7f0901f9

.field public static final com_facebook_likeboxcountview_border_radius:I = 0x7f0901fa

.field public static final com_facebook_likeboxcountview_border_width:I = 0x7f0901fb

.field public static final com_facebook_likeboxcountview_caret_height:I = 0x7f0901fc

.field public static final com_facebook_likeboxcountview_caret_width:I = 0x7f0901fd

.field public static final com_facebook_likeboxcountview_text_padding:I = 0x7f0901fe

.field public static final com_facebook_likeboxcountview_text_size:I = 0x7f0901ff

.field public static final com_facebook_likeview_edge_padding:I = 0x7f090200

.field public static final com_facebook_likeview_internal_padding:I = 0x7f090201

.field public static final com_facebook_likeview_text_size:I = 0x7f090202

.field public static final com_facebook_profilepictureview_preset_size_large:I = 0x7f090203

.field public static final com_facebook_profilepictureview_preset_size_normal:I = 0x7f090204

.field public static final com_facebook_profilepictureview_preset_size_small:I = 0x7f090205

.field public static final com_facebook_share_button_compound_drawable_padding:I = 0x7f090206

.field public static final com_facebook_share_button_padding_bottom:I = 0x7f090207

.field public static final com_facebook_share_button_padding_left:I = 0x7f090208

.field public static final com_facebook_share_button_padding_right:I = 0x7f090209

.field public static final com_facebook_share_button_padding_top:I = 0x7f09020a

.field public static final com_facebook_share_button_text_size:I = 0x7f09020b

.field public static final com_facebook_tooltip_horizontal_padding:I = 0x7f09020c

.field public static final commonDialogCloseButtonPaddingBottom:I = 0x7f09020d

.field public static final commonDialogCloseButtonPaddingLeft:I = 0x7f09020e

.field public static final commonDialogCloseButtonPaddingRight:I = 0x7f09020f

.field public static final commonDialogCloseButtonPaddingTop:I = 0x7f090210

.field public static final compareGameListTextMarginTop:I = 0x7f090064

.field public static final compareGamerpicMargin:I = 0x7f090065

.field public static final compareGamerpicWidth:I = 0x7f090066

.field public static final compareHeaderFrameMargin:I = 0x7f090067

.field public static final compareItemBottonMargin:I = 0x7f090211

.field public static final comparisonbarHeight:I = 0x7f090212

.field public static final composeMessageTextMarginTop:I = 0x7f090213

.field public static final composeMessageTextTextSize:I = 0x7f090214

.field public static final composeReplyTextMarginTop:I = 0x7f090215

.field public static final composeboxMaxHeight:I = 0x7f090216

.field public static final composeboxMinHeight:I = 0x7f090068

.field public static final composeboxWidthWithAvatar:I = 0x7f090069

.field public static final connectionDialogCheckboxMarginLeft:I = 0x7f090217

.field public static final connectionDialogCheckboxTextMarginLeft:I = 0x7f090218

.field public static final connectionDialogCloseButtopMarginRight:I = 0x7f090219

.field public static final connectionDialogCloseButtopPaddingHorizontal:I = 0x7f09021a

.field public static final connectionDialogConnectButtonPaddingVertical:I = 0x7f09021b

.field public static final connectionDialogConsoleNamePaddingLeft:I = 0x7f09021c

.field public static final connectionDialogExpandedSectionPaddingBottom:I = 0x7f09021d

.field public static final connectionDialogExpandedSectionPaddingTop:I = 0x7f09021e

.field public static final connectionDialogFontRedline28:I = 0x7f09021f

.field public static final connectionDialogFontRedline36:I = 0x7f090220

.field public static final connectionDialogHeight:I = 0x7f090221

.field public static final connectionDialogIconSize:I = 0x7f090222

.field public static final connectionDialogMainSectionPaddingLeftRight:I = 0x7f090223

.field public static final connectionDialogMargin:I = 0x7f090224

.field public static final connectionDialogRowMainSectionHeight:I = 0x7f090225

.field public static final connectionDialogSwitchPanelMarginTop:I = 0x7f090226

.field public static final connectionDialogTitleMarginTop:I = 0x7f090227

.field public static final connectionDialogWidth:I = 0x7f090228

.field public static final connectionHeight:I = 0x7f09006a

.field public static final connectionLogoSize:I = 0x7f09006b

.field public static final consoleDesc:I = 0x7f09006c

.field public static final consoleDescMarginTop:I = 0x7f09006d

.field public static final consoleHelpBodyMarginLeft:I = 0x7f09006e

.field public static final consoleHelpBodyMarginRight:I = 0x7f09006f

.field public static final consoleHelpBodyMarginTop:I = 0x7f090070

.field public static final consoleInstructionMarginTop:I = 0x7f090071

.field public static final consoleNotConnectMarginTop:I = 0x7f090072

.field public static final consoleTitle:I = 0x7f090073

.field public static final consoleTryAgainMarginTop:I = 0x7f090074

.field public static final consoleTryAgiainHeight:I = 0x7f090075

.field public static final consoleTryAgiainWidth:I = 0x7f090076

.field public static final consoleXleButtonMarginButtom:I = 0x7f090077

.field public static final consoleXleButtonMarginTop:I = 0x7f090078

.field public static final controlButtonHeight:I = 0x7f090229

.field public static final controlButtonWidth:I = 0x7f09022a

.field public static final controlPickerTextMarginBottom:I = 0x7f09022b

.field public static final controlPickerTextMarginLeft:I = 0x7f09022c

.field public static final customizeImageHeight:I = 0x7f09022d

.field public static final customizeSectionMargin:I = 0x7f09022e

.field public static final customizeSectionMarginBottom:I = 0x7f09022f

.field public static final defaultPieChartSize:I = 0x7f090230

.field public static final defaultPivotMoreButtonAreaHeight:I = 0x7f090231

.field public static final defaultPivotPageMinWidth:I = 0x7f090232

.field public static final design_appbar_elevation:I = 0x7f090233

.field public static final design_bottom_navigation_active_item_max_width:I = 0x7f090234

.field public static final design_bottom_navigation_active_text_size:I = 0x7f090235

.field public static final design_bottom_navigation_height:I = 0x7f090236

.field public static final design_bottom_navigation_item_max_width:I = 0x7f090237

.field public static final design_bottom_navigation_margin:I = 0x7f090238

.field public static final design_bottom_navigation_text_size:I = 0x7f090239

.field public static final design_bottom_sheet_modal_elevation:I = 0x7f09023a

.field public static final design_bottom_sheet_peek_height_min:I = 0x7f09023b

.field public static final design_fab_border_width:I = 0x7f09023c

.field public static final design_fab_elevation:I = 0x7f09023d

.field public static final design_fab_image_size:I = 0x7f09023e

.field public static final design_fab_size_mini:I = 0x7f09023f

.field public static final design_fab_size_normal:I = 0x7f090240

.field public static final design_fab_translation_z_pressed:I = 0x7f090241

.field public static final design_navigation_elevation:I = 0x7f090242

.field public static final design_navigation_icon_padding:I = 0x7f090243

.field public static final design_navigation_icon_size:I = 0x7f090244

.field public static final design_navigation_max_width:I = 0x7f090131

.field public static final design_navigation_padding_bottom:I = 0x7f090245

.field public static final design_navigation_separator_vertical_padding:I = 0x7f090246

.field public static final design_snackbar_action_inline_max_width:I = 0x7f090132

.field public static final design_snackbar_background_corner_radius:I = 0x7f090133

.field public static final design_snackbar_elevation:I = 0x7f090247

.field public static final design_snackbar_extra_spacing_horizontal:I = 0x7f090134

.field public static final design_snackbar_max_width:I = 0x7f090135

.field public static final design_snackbar_min_width:I = 0x7f090136

.field public static final design_snackbar_padding_horizontal:I = 0x7f090248

.field public static final design_snackbar_padding_vertical:I = 0x7f090249

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f090137

.field public static final design_snackbar_text_size:I = 0x7f09024a

.field public static final design_tab_max_width:I = 0x7f09024b

.field public static final design_tab_scrollable_min_width:I = 0x7f090138

.field public static final design_tab_text_size:I = 0x7f09024c

.field public static final design_tab_text_size_2line:I = 0x7f09024d

.field public static final detailSquareArtHeightWithMargin:I = 0x7f090079

.field public static final detailSquareArtSize:I = 0x7f09007a

.field public static final detailSquareArtSizeSmall:I = 0x7f09007b

.field public static final detailsBoxArtHeight:I = 0x7f09007c

.field public static final detailsBoxArtMarginLeft:I = 0x7f09024e

.field public static final detailsBoxArtMarginRight:I = 0x7f09024f

.field public static final detailsBoxArtMarginTop:I = 0x7f09007d

.field public static final detailsBoxArtMaxWidth:I = 0x7f09007e

.field public static final detailsBoxArtWidth:I = 0x7f09007f

.field public static final detailsContentMarginBottom:I = 0x7f090250

.field public static final detailsContentMarginLeft:I = 0x7f090251

.field public static final detailsContentMarginRight:I = 0x7f090252

.field public static final detailsControlSpace:I = 0x7f090253

.field public static final detailsHeaderHeight:I = 0x7f090080

.field public static final detailsHorizontalControlGap:I = 0x7f090254

.field public static final detailsLastPivotPageRightMargin:I = 0x7f090255

.field public static final detailsListItemBottomMargin:I = 0x7f090256

.field public static final detailsListItemGap:I = 0x7f090257

.field public static final detailsListItemLeftMargin:I = 0x7f090258

.field public static final detailsListItemRightMargin:I = 0x7f090259

.field public static final detailsListItemTopMargin:I = 0x7f09025a

.field public static final detailsListRowBoxArtSize:I = 0x7f09025b

.field public static final detailsMarginBottomReleaseData:I = 0x7f09025c

.field public static final detailsMarginLeft:I = 0x7f090081

.field public static final detailsMarginTop:I = 0x7f090082

.field public static final detailsMarginTopMoreLess:I = 0x7f090083

.field public static final detailsPEGIRatingItemMargin:I = 0x7f09025d

.field public static final detailsPaddingBottomMoreLess:I = 0x7f090084

.field public static final detailsPaddingRightMoreLess:I = 0x7f09025e

.field public static final detailsPaddingTopMoreLess:I = 0x7f090085

.field public static final detailsPageTitleMarginLeft:I = 0x7f09025f

.field public static final detailsParentRatingMarginTop:I = 0x7f090086

.field public static final detailsParentRatingTileHeight:I = 0x7f090087

.field public static final detailsParentRatingTileMarginRight:I = 0x7f090260

.field public static final detailsParentRatingTileWidth:I = 0x7f090088

.field public static final detailsPivotPageLeftMargin:I = 0x7f090261

.field public static final detailsPivotPageMarginBottom:I = 0x7f090262

.field public static final detailsPlayButtonHeight:I = 0x7f090263

.field public static final detailsPlayButtonWidth:I = 0x7f090264

.field public static final detailsPortraitBoxArtMaxWidth:I = 0x7f090089

.field public static final detailsProviderItemImageMargin:I = 0x7f09008a

.field public static final detailsProvidersContainerWidth:I = 0x7f090265

.field public static final detailsProvidersSpace:I = 0x7f090266

.field public static final detailsRatingIconWidth:I = 0x7f090267

.field public static final detailsRatingSpace:I = 0x7f090268

.field public static final detailsRelatedVerticalSpacing:I = 0x7f090269

.field public static final detailsTVEpisodeDetailsPublishMaxWidth:I = 0x7f09008b

.field public static final detailsTitleImageHeight:I = 0x7f09008c

.field public static final detailsTitleImageMarginBottom:I = 0x7f09008d

.field public static final detailsTitleImageMarginRight:I = 0x7f09008e

.field public static final detailsTitleImageMarginTop:I = 0x7f09008f

.field public static final detailsTitleImageWidth:I = 0x7f090090

.field public static final detailsTvGridPaddingBottom:I = 0x7f090091

.field public static final detailsTvGridPaddingLeft:I = 0x7f090092

.field public static final detailsTvPaddingLeft:I = 0x7f090093

.field public static final detailsTvPaddingRight:I = 0x7f090094

.field public static final detailsTvSeriesBoxArtHeight:I = 0x7f090095

.field public static final detailsTvSeriesBoxArtWidth:I = 0x7f090096

.field public static final detailsTvStarPaddingTop:I = 0x7f090097

.field public static final detailsTvUserRateCountPaddingTop:I = 0x7f090098

.field public static final disConnect:I = 0x7f090099

.field public static final disabled_alpha_material_dark:I = 0x7f09026a

.field public static final disabled_alpha_material_light:I = 0x7f09026b

.field public static final discoverGridItemMarginBottom:I = 0x7f09009a

.field public static final discoverGridItemWidth:I = 0x7f09009b

.field public static final dodgeFastscrollPaddingRight:I = 0x7f09026c

.field public static final drawerGridSpacing:I = 0x7f09026d

.field public static final drawerHeaderTextMarginLeft:I = 0x7f09026e

.field public static final drawerMarginLeft:I = 0x7f09026f

.field public static final drawerMarginRight:I = 0x7f090270

.field public static final drawerMessagesBubbleMarginRight:I = 0x7f090271

.field public static final drawerMessagesBubbleSize:I = 0x7f090272

.field public static final drawerMessagesBubbleTextSize:I = 0x7f090273

.field public static final drawerPaddingLeft:I = 0x7f090274

.field public static final drawerProfileMarginBottom:I = 0x7f090275

.field public static final drawerProfileSize:I = 0x7f090276

.field public static final drawerRowHeight:I = 0x7f090277

.field public static final drawerRowIconMarginLeft:I = 0x7f090278

.field public static final drawerRowIconTextSize:I = 0x7f090279

.field public static final drawerRowPaddingBottom:I = 0x7f09027a

.field public static final drawerRowPaddingLeft:I = 0x7f09027b

.field public static final drawerRowPaddingTop:I = 0x7f09027c

.field public static final drawerRowTextMarginLeft:I = 0x7f09027d

.field public static final drawerRowTextSize:I = 0x7f09027e

.field public static final drawerRowWithProfileHeight:I = 0x7f09027f

.field public static final drawerWidth:I = 0x7f090280

.field public static final editProfileMarginBottom:I = 0x7f09009c

.field public static final editProfileMarginTop:I = 0x7f09009d

.field public static final editRealNameDialogContentWidth:I = 0x7f090281

.field public static final editViewFixLengthMarginTop:I = 0x7f09009e

.field public static final editorButtonSize:I = 0x7f09009f

.field public static final envelopeHeight:I = 0x7f0900a0

.field public static final envelopeWidth:I = 0x7f0900a1

.field public static final epg_anim_speed_per_ms:I = 0x7f090282

.field public static final epg_appchannel_focus_frame_padding_horizontal:I = 0x7f090283

.field public static final epg_appchannel_focus_frame_padding_vertical:I = 0x7f090284

.field public static final epg_appchannel_focus_frame_stroke_width:I = 0x7f090285

.field public static final epg_appchannel_item_connection_error_text_expanded_view_marginTop:I = 0x7f090286

.field public static final epg_appchannel_item_connection_error_text_expanded_view_paddingLeft:I = 0x7f090287

.field public static final epg_appchannel_item_connection_error_text_expanded_view_textSize:I = 0x7f090288

.field public static final epg_appchannel_item_connection_error_text_marginTop:I = 0x7f090289

.field public static final epg_appchannel_item_connection_error_text_paddingLeft:I = 0x7f09028a

.field public static final epg_appchannel_item_connection_error_text_textSize:I = 0x7f09028b

.field public static final epg_appchannel_item_description_text_textSize:I = 0x7f09028c

.field public static final epg_appchannel_item_subtitle_text_textSize:I = 0x7f09028d

.field public static final epg_appchannel_item_title_text_textSize:I = 0x7f09028e

.field public static final epg_appchannel_logo_padding:I = 0x7f09028f

.field public static final epg_appchannelrow_rightpanel_bottompanel_topMargin:I = 0x7f090290

.field public static final epg_channel_height:I = 0x7f090291

.field public static final epg_channel_width:I = 0x7f090292

.field public static final epg_detail_art_size:I = 0x7f090293

.field public static final epg_detail_channel_callsign_padding:I = 0x7f090294

.field public static final epg_detail_channel_number_padding:I = 0x7f090295

.field public static final epg_detail_info_and_tune_horizontal_padding_left:I = 0x7f090296

.field public static final epg_detail_info_and_tune_horizontal_padding_right:I = 0x7f090297

.field public static final epg_detail_info_and_tune_horizontal_padding_top:I = 0x7f090298

.field public static final epg_detail_info_and_tune_padding:I = 0x7f090299

.field public static final epg_detail_info_and_tune_size:I = 0x7f09029a

.field public static final epg_detail_padding:I = 0x7f09029b

.field public static final epg_detail_program_description_padding_bottom:I = 0x7f09029c

.field public static final epg_detail_program_description_padding_left:I = 0x7f09029d

.field public static final epg_detail_program_description_padding_right:I = 0x7f09029e

.field public static final epg_detail_program_description_padding_top:I = 0x7f09029f

.field public static final epg_detail_smallart_padding:I = 0x7f0902a0

.field public static final epg_detail_smallart_size:I = 0x7f0902a1

.field public static final epg_detail_smalllogo_height:I = 0x7f0902a2

.field public static final epg_detail_smalllogo_width:I = 0x7f0902a3

.field public static final epg_detail_space:I = 0x7f0902a4

.field public static final epg_detail_thumbnail:I = 0x7f0902a5

.field public static final epg_detail_thumbnail_padding:I = 0x7f0902a6

.field public static final epg_detail_thumbnail_padding_bottom:I = 0x7f0902a7

.field public static final epg_detail_thumbnail_padding_left:I = 0x7f0902a8

.field public static final epg_detail_thumbnail_padding_right:I = 0x7f0902a9

.field public static final epg_detail_thumbnail_padding_top:I = 0x7f0902aa

.field public static final epg_detail_thumbnail_spinner_padding_bottom:I = 0x7f0902ab

.field public static final epg_detail_thumbnail_spinner_padding_left:I = 0x7f0902ac

.field public static final epg_detail_thumbnail_spinner_padding_right:I = 0x7f0902ad

.field public static final epg_detail_thumbnail_spinner_padding_top:I = 0x7f0902ae

.field public static final epg_dialog_added_to_favorite_fail_marginBottom:I = 0x7f0902af

.field public static final epg_dialog_added_to_favorite_fail_marginLeft:I = 0x7f0902b0

.field public static final epg_dialog_added_to_favorite_fail_marginRight:I = 0x7f0902b1

.field public static final epg_dialog_added_to_favorite_fail_marginTop:I = 0x7f0902b2

.field public static final epg_dialog_added_to_favorite_fail_width:I = 0x7f0902b3

.field public static final epg_dialog_added_to_favorite_success_marginBottom:I = 0x7f0902b4

.field public static final epg_dialog_added_to_favorite_success_marginLeft:I = 0x7f0902b5

.field public static final epg_dialog_added_to_favorite_success_marginRight:I = 0x7f0902b6

.field public static final epg_dialog_added_to_favorite_success_marginTop:I = 0x7f0902b7

.field public static final epg_dialog_added_to_favorite_success_width:I = 0x7f0902b8

.field public static final epg_error_height:I = 0x7f0902b9

.field public static final epg_error_text_fontSize:I = 0x7f0902ba

.field public static final epg_error_text_style_compact_marginBottom:I = 0x7f0902bb

.field public static final epg_error_text_style_compact_marginLeft:I = 0x7f0902bc

.field public static final epg_error_text_style_compact_marginRight:I = 0x7f0902bd

.field public static final epg_error_text_style_compact_marginTop:I = 0x7f0902be

.field public static final epg_error_text_style_compact_stacked_paddingBottom:I = 0x7f0902bf

.field public static final epg_error_text_style_compact_stacked_paddingTop:I = 0x7f0902c0

.field public static final epg_favorite_button_channel_header_height:I = 0x7f0902c1

.field public static final epg_favorite_button_channel_header_textSize:I = 0x7f0902c2

.field public static final epg_favorite_button_channel_header_width:I = 0x7f0902c3

.field public static final epg_fullscreen_error_frame_paddingRight:I = 0x7f0902c4

.field public static final epg_fullscreen_error_frame_paddingTop:I = 0x7f0902c5

.field public static final epg_header_font_size:I = 0x7f0902c6

.field public static final epg_header_height:I = 0x7f0902c7

.field public static final epg_horizontal_margin:I = 0x7f0902c8

.field public static final epg_inline_details_appchannels_height:I = 0x7f0902c9

.field public static final epg_inline_details_height:I = 0x7f0902ca

.field public static final epg_inpage_loading_layout_height:I = 0x7f0902cb

.field public static final epg_loading_message_font_size:I = 0x7f0902cc

.field public static final epg_one_second:I = 0x7f0902cd

.field public static final epg_pivot_header_fontSize:I = 0x7f0902ce

.field public static final epg_program_view_text_textSize:I = 0x7f0902cf

.field public static final epg_provider_logo_height:I = 0x7f0902d0

.field public static final epg_provider_logo_width:I = 0x7f0902d1

.field public static final epg_recent_channel_callsign_textSize:I = 0x7f0902d2

.field public static final epg_recent_channels_episode_height:I = 0x7f0902d3

.field public static final epg_recent_channels_program_info_padding_bottom:I = 0x7f0902d4

.field public static final epg_recent_channels_program_info_padding_left:I = 0x7f0902d5

.field public static final epg_recent_channels_program_info_padding_right:I = 0x7f0902d6

.field public static final epg_recent_channels_program_info_padding_top:I = 0x7f0902d7

.field public static final epg_recent_channels_program_info_textSize:I = 0x7f0902d8

.field public static final epg_recent_channels_provider_width:I = 0x7f0902d9

.field public static final epg_recent_channels_text_callsign_marginBottom:I = 0x7f0902da

.field public static final epg_recent_channels_thumbnail_paddingBottom:I = 0x7f0902db

.field public static final epg_recent_channels_thumbnail_paddingLeft:I = 0x7f0902dc

.field public static final epg_recent_channels_thumbnail_paddingTop:I = 0x7f0902dd

.field public static final epg_recent_channels_thumbnail_size:I = 0x7f0902de

.field public static final epg_recent_channels_thumbnail_unavailable_textSize:I = 0x7f0902df

.field public static final epg_recent_channels_top_frame_margin_bottom:I = 0x7f0902e0

.field public static final epg_recent_channels_vertical_margin:I = 0x7f0902e1

.field public static final epg_recent_smalllogo_height:I = 0x7f0902e2

.field public static final epg_recent_smalllogo_width:I = 0x7f0902e3

.field public static final epg_scroll_accelerator_width:I = 0x7f0902e4

.field public static final epg_status_header_marginBottom:I = 0x7f0902e5

.field public static final epg_status_header_marginLeft:I = 0x7f0902e6

.field public static final epg_status_header_marginTop:I = 0x7f0902e7

.field public static final epg_status_header_textSize:I = 0x7f0902e8

.field public static final epg_status_message_fullscreen_header_textSize:I = 0x7f0902e9

.field public static final epg_status_message_fullscreen_horizontalRuleHeight:I = 0x7f0902ea

.field public static final epg_status_message_fullscreen_marginBottom:I = 0x7f0902eb

.field public static final epg_status_message_fullscreen_marginLeft:I = 0x7f0902ec

.field public static final epg_status_message_fullscreen_marginTop:I = 0x7f0902ed

.field public static final epg_status_message_fullscreen_marginTop_without_header:I = 0x7f0902ee

.field public static final epg_status_message_fullscreen_single_line_text_view_marginRight:I = 0x7f0902ef

.field public static final epg_status_message_fullscreen_single_line_text_view_marginTop:I = 0x7f0902f0

.field public static final epg_status_message_fullscreen_single_line_text_view_textSize:I = 0x7f0902f1

.field public static final epg_status_message_fullscreen_textSize:I = 0x7f0902f2

.field public static final epg_vertical_margin:I = 0x7f0902f3

.field public static final errorCaseTopPadding:I = 0x7f0902f4

.field public static final exo_media_button_height:I = 0x7f0902f5

.field public static final exo_media_button_width:I = 0x7f0902f6

.field public static final extrasListRowTileHeight:I = 0x7f0902f7

.field public static final familyItemIconSizeA:I = 0x7f0902f8

.field public static final familyItemIconSizeB:I = 0x7f0902f9

.field public static final familyItemIconSizeC:I = 0x7f0902fa

.field public static final familyItemIconSizeD:I = 0x7f0902fb

.field public static final familyItemTextSize:I = 0x7f0902fc

.field public static final familyPasscodeCancelButtonSize:I = 0x7f0902fd

.field public static final familyPasscodeIconMarginValue:I = 0x7f0902fe

.field public static final familyPasscodeItemMarginTop:I = 0x7f0902ff

.field public static final familyPasscodeItemMarginValue:I = 0x7f090300

.field public static final familyPasscodeItemSize:I = 0x7f090301

.field public static final familyPasscodetitleMarginTop:I = 0x7f090302

.field public static final findPhoneContactsButtonMargin:I = 0x7f09001e

.field public static final findPhoneContactsFramePadding:I = 0x7f090303

.field public static final fluidSize:I = 0x7f090304

.field public static final followingItemViewMarginRight:I = 0x7f090305

.field public static final followingListItemPadding:I = 0x7f090306

.field public static final followingPresenceDotSize:I = 0x7f090307

.field public static final followingProfileImageHeight:I = 0x7f090308

.field public static final followingProfileImageWidth:I = 0x7f090309

.field public static final followingProfileItemHeight:I = 0x7f09030a

.field public static final followingProfileListHeight:I = 0x7f09030b

.field public static final frameMarginTop:I = 0x7f09030c

.field public static final friendFinderConfirmDialogBodySize:I = 0x7f09030d

.field public static final friendFinderConfirmDialogErrorSize:I = 0x7f09030e

.field public static final friendFinderConfirmDialogTitleSize:I = 0x7f09030f

.field public static final friendListHeaderHeight:I = 0x7f0900a2

.field public static final friendListItemHeight:I = 0x7f090310

.field public static final friendSelectorIconHeight:I = 0x7f0900a3

.field public static final friendSelectorIconWidth:I = 0x7f0900a4

.field public static final friendsRightMarginSize:I = 0x7f090311

.field public static final friendsSpotlightMargin:I = 0x7f090312

.field public static final friendsWhoPlayListItemImageMarginRight:I = 0x7f090313

.field public static final friendsWhoPlayListItemImagePaddingBottom:I = 0x7f090314

.field public static final friendsWhoPlayListItemImagePaddingTop:I = 0x7f090315

.field public static final friendsWhoPlayListItemImageSize:I = 0x7f090316

.field public static final friendsWhoPlayListItemImageSizeActual:I = 0x7f090317

.field public static final gameHeaderGiconHeight:I = 0x7f0900a5

.field public static final gameHeaderGiconMarginBottom:I = 0x7f0900a6

.field public static final gameHeaderGiconMarginLeft:I = 0x7f0900a7

.field public static final gameHeaderGiconWidth:I = 0x7f0900a8

.field public static final gameNameMarginLeft:I = 0x7f090318

.field public static final gameProfileInfoButtonMargin:I = 0x7f090319

.field public static final gameProfileInfoButtonWidth:I = 0x7f09031a

.field public static final gameProfileInfoMarginBottom:I = 0x7f09031b

.field public static final gameclipsTitleTopMargin:I = 0x7f09031c

.field public static final genericButtonHeight:I = 0x7f09031d

.field public static final genericPageMargin:I = 0x7f09031e

.field public static final genericTitleBarHeight:I = 0x7f0900a9

.field public static final globalCloseButtonSize:I = 0x7f09031f

.field public static final globalListItemInnerMargin:I = 0x7f090320

.field public static final globalTileBottomMargin:I = 0x7f090321

.field public static final globalTileImageHeight:I = 0x7f090322

.field public static final globalTileInnerMargin:I = 0x7f090323

.field public static final groupMessagesHeaderFriendsNamesWidth:I = 0x7f09001f

.field public static final groupMessagesSummaryImageActualSize:I = 0x7f090324

.field public static final highlight_alpha_material_colored:I = 0x7f090325

.field public static final highlight_alpha_material_dark:I = 0x7f090326

.field public static final highlight_alpha_material_light:I = 0x7f090327

.field public static final hint_alpha_material_dark:I = 0x7f090328

.field public static final hint_alpha_material_light:I = 0x7f090329

.field public static final hint_pressed_alpha_material_dark:I = 0x7f09032a

.field public static final hint_pressed_alpha_material_light:I = 0x7f09032b

.field public static final homeFooterPaddingBottom:I = 0x7f09032c

.field public static final homeFooterPaddingTop:I = 0x7f09032d

.field public static final homeHeaderPaddingBottom:I = 0x7f09032e

.field public static final homeHeaderPaddingTop:I = 0x7f09032f

.field public static final homeScreenBatAppAchievementsContainerMarginTop:I = 0x7f0900aa

.field public static final homeScreenBatAppAchievementsMarginLeft:I = 0x7f0900ab

.field public static final homeScreenBatAppHeroStatMarginTop:I = 0x7f0900ac

.field public static final homeScreenBatAppHeroTextSize:I = 0x7f0900ad

.field public static final homeScreenBatAppStatsTextSize:I = 0x7f0900ae

.field public static final homeScreenBatAppTitleMarginTop:I = 0x7f0900af

.field public static final homeScreenBatAppTitleTextSize:I = 0x7f0900b0

.field public static final homeScreenBatAppTrophyIconSize:I = 0x7f0900b1

.field public static final homeScreenBatGameHeroStatMarginTop:I = 0x7f0900b2

.field public static final homeScreenBatGameHeroTextSize:I = 0x7f0900b3

.field public static final homeScreenBatGameMaxScorePaddingBottom:I = 0x7f0900b4

.field public static final homeScreenBatGameMaxScoreTextSize:I = 0x7f0900b5

.field public static final homeScreenBatGameProgressWheelSize:I = 0x7f0900b6

.field public static final homeScreenBatGameProgressWheelWidth:I = 0x7f0900b7

.field public static final homeScreenBatGameScoreIconMarginBottom:I = 0x7f090330

.field public static final homeScreenBatGameStatsContainerMarginLeft:I = 0x7f0900b8

.field public static final homeScreenBatGameStatsIconSize:I = 0x7f0900b9

.field public static final homeScreenBatGameStatsMarginVertical:I = 0x7f0900ba

.field public static final homeScreenBatGameStatsTextMarginLeft:I = 0x7f0900bb

.field public static final homeScreenBatGameStatsTextSize:I = 0x7f0900bc

.field public static final homeScreenBatGameTitleMarginTop:I = 0x7f0900bd

.field public static final homeScreenBatGameTitleTextSize:I = 0x7f0900be

.field public static final homeScreenBatGameTrophyIconMarginBottom:I = 0x7f090331

.field public static final homeScreenBatGameWheelMarginBottom:I = 0x7f0900bf

.field public static final homeScreenBatGameWheelTextSize:I = 0x7f0900c0

.field public static final homeScreenBatIconSize:I = 0x7f0900c1

.field public static final homeScreenBatNoStatsTitleMarginTop:I = 0x7f0900c2

.field public static final homeScreenBatNoStatsTitleTextSize:I = 0x7f0900c3

.field public static final homeScreenBatPromoText:I = 0x7f090019

.field public static final homeScreenBatTextMarginLeft:I = 0x7f0900c4

.field public static final homeScreenBatTextMarginRigth:I = 0x7f0900c5

.field public static final homeScreenCellImageSubstitute:I = 0x7f090332

.field public static final homeScreenCellTitleMarginBottom:I = 0x7f09014c

.field public static final homeScreenCellTitleMarginLeft:I = 0x7f09014d

.field public static final homeScreenCellTitleMarginRight:I = 0x7f09014e

.field public static final homeScreenCellTitleTextSize:I = 0x7f09014f

.field public static final homeScreenContainerPaddingEnd:I = 0x7f090333

.field public static final homeScreenHeaderMarginLeft:I = 0x7f090334

.field public static final homeScreenPopupCmdButtonFontSize:I = 0x7f090335

.field public static final homeScreenPopupCmdButtonSize:I = 0x7f090336

.field public static final homeScreenPopupCmdHeight:I = 0x7f090337

.field public static final homeScreenPopupCmdImageSize:I = 0x7f090338

.field public static final homeScreenPopupCmdTextMarginLeft:I = 0x7f090339

.field public static final homeScreenPopupCompanionRightSectionMarginLeft:I = 0x7f09033a

.field public static final homeScreenPopupCompanionTextMarginLeft:I = 0x7f09033b

.field public static final homeScreenPopupHeaderMarginLeft:I = 0x7f09033c

.field public static final homeScreenPopupImageHeight:I = 0x7f09033d

.field public static final homeScreenPopupImageSubstitute:I = 0x7f09033e

.field public static final homeScreenPopupMarginLeft:I = 0x7f09033f

.field public static final homeScreenPopupPaddingBase:I = 0x7f090340

.field public static final homeScreenPopupPaddingSide:I = 0x7f090341

.field public static final homeScreenPopupSmallImageSize:I = 0x7f090342

.field public static final homeScreenPopupTitleImageSize:I = 0x7f090343

.field public static final homeScreenRecentSnapMarginRight:I = 0x7f090344

.field public static final homeScreenRecentSnapMarginTop:I = 0x7f090345

.field public static final homeScreenRecentSnapTextSize:I = 0x7f090346

.field public static final homeScreenRecentsPromoText1MarginBottom:I = 0x7f090347

.field public static final homeScreenRecentsPromoText2:I = 0x7f09001a

.field public static final homeScreenSectionBatFullImageMarginHorizontal:I = 0x7f0900c6

.field public static final homeScreenSectionBatFullImageMarginRight:I = 0x7f0900c7

.field public static final homeScreenSectionBatFullImageMarginVertical:I = 0x7f090348

.field public static final homeScreenSectionBatIconMarginHorizontal:I = 0x7f090150

.field public static final homeScreenSectionBatIconMarginVertical:I = 0x7f090151

.field public static final homeScreenSectionBatPromoTextMarginVertical:I = 0x7f090349

.field public static final homeScreenSectionGridBottomPadding:I = 0x7f09034a

.field public static final homeScreenSectionGridMargin:I = 0x7f09034b

.field public static final homeScreenSectionPromoMarginLeftRight:I = 0x7f09034c

.field public static final homeScreenSectionPromoMarginTopBottom:I = 0x7f09034d

.field public static final homeScreenSectionTextMarginLeftRight:I = 0x7f09034e

.field public static final homeScreenTextAsImageSize:I = 0x7f09034f

.field public static final homeSectionPaddingEnd:I = 0x7f090350

.field public static final homeSectionPaddingStart:I = 0x7f090351

.field public static final hubRootTopMargin:I = 0x7f090352

.field public static final identityProgressPageMarginTop:I = 0x7f090353

.field public static final ieButtonsLeftPadding:I = 0x7f090354

.field public static final ieButtonsTopPadding:I = 0x7f090355

.field public static final ieMediaButtonTopMargin:I = 0x7f090356

.field public static final ieTextboxButtonWidth:I = 0x7f090357

.field public static final ieTextboxHeight:I = 0x7f090358

.field public static final ieTextboxPaddingRight:I = 0x7f090359

.field public static final ieTextboxTextSize:I = 0x7f09035a

.field public static final includedContentListItemHeight:I = 0x7f09035b

.field public static final invitePhoneContactsNameWidth:I = 0x7f0900c8

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f09035c

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f09035d

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f09035e

.field public static final labelImageViewHeight:I = 0x7f09035f

.field public static final leaderboardRow_ImageHeight:I = 0x7f090360

.field public static final leaderboardRow_ItemHeight:I = 0x7f090361

.field public static final leaderboardRow_LeftTextMargin:I = 0x7f090362

.field public static final leaderboardRow_RankHeight:I = 0x7f090363

.field public static final leaderboardRow_RankMargin:I = 0x7f090364

.field public static final leaderboard_FirstPlaceProfilePicSize:I = 0x7f090365

.field public static final leaderboard_ProfilePicMargin:I = 0x7f090366

.field public static final leaderboard_RibbonHeight:I = 0x7f090367

.field public static final leaderboard_RibbonWidth:I = 0x7f090368

.field public static final leaderboard_SecondPlaceProfilePicSize:I = 0x7f090369

.field public static final leaderboard_TrophyTopBottomMargin:I = 0x7f09036a

.field public static final leaderboard_topMargin:I = 0x7f09036b

.field public static final listMarginBottom:I = 0x7f0900c9

.field public static final listMarginTop:I = 0x7f0900ca

.field public static final listPivotPageMarginBottom:I = 0x7f09036c

.field public static final listRowContentMarginLeft:I = 0x7f0900cb

.field public static final listRowDividerHeight:I = 0x7f0900cc

.field public static final listRowGTextMarginLeft:I = 0x7f0900cd

.field public static final listRowGiconHeight:I = 0x7f0900ce

.field public static final listRowGiconMarginHeight:I = 0x7f0900cf

.field public static final listRowGiconMarginLeft:I = 0x7f0900d0

.field public static final listRowGiconMarginRight:I = 0x7f09036d

.field public static final listRowGiconMarginTop:I = 0x7f0900d1

.field public static final listRowGiconMarginWidth:I = 0x7f0900d2

.field public static final listRowGiconWidth:I = 0x7f0900d3

.field public static final listRowPrimaryTextMarginTop:I = 0x7f0900d4

.field public static final listRowTileHeight:I = 0x7f0900d5

.field public static final listRowTileMarginBottom:I = 0x7f0900d6

.field public static final listRowTileMarginLeft:I = 0x7f09036e

.field public static final listRowTileMarginTop:I = 0x7f0900d7

.field public static final listRowTileWidth:I = 0x7f0900d8

.field public static final loadingLogoBottomMargin:I = 0x7f0900d9

.field public static final loadingMoreFooterPaddingBottom:I = 0x7f0900da

.field public static final loadingMoreFooterPaddingTop:I = 0x7f0900db

.field public static final loadingRingBottomMargin:I = 0x7f0900dc

.field public static final marginFooter:I = 0x7f09036f

.field public static final marginLarge:I = 0x7f090370

.field public static final marginMedium:I = 0x7f090371

.field public static final marginSmall:I = 0x7f090372

.field public static final marginXLarge:I = 0x7f090373

.field public static final match_parent:I = 0x7f090374

.field public static final match_parent_fix:I = 0x7f090375

.field public static final maxAccountPickerHeight:I = 0x7f090376

.field public static final maxAccountPickerWidth:I = 0x7f090377

.field public static final mediaButtonTextSize:I = 0x7f090378

.field public static final mediaProgressBarHeight:I = 0x7f0900dd

.field public static final menuHeight:I = 0x7f090379

.field public static final menuMarginRight:I = 0x7f09037a

.field public static final menuMarginTopBottom:I = 0x7f09037b

.field public static final menuWidth:I = 0x7f09037c

.field public static final messageDetailsAttachmentMaxWidth:I = 0x7f09037d

.field public static final messageDetailsAttachmentMinWidth:I = 0x7f09037e

.field public static final messageDetailsAttachmentWidth:I = 0x7f09037f

.field public static final messageDetailsButtonWidth:I = 0x7f0900de

.field public static final messageboxMaxHeight:I = 0x7f0900df

.field public static final messagesContactIconMaxAcceptableHeight:I = 0x7f090380

.field public static final messagesContactIconMinAcceptableHeight:I = 0x7f090381

.field public static final messagesDetailArrowMargin:I = 0x7f090382

.field public static final messagesDetailGameTahMargin:I = 0x7f090383

.field public static final messagesDetailsArrowSize:I = 0x7f090384

.field public static final messagesDetailsDefaultTextSize:I = 0x7f090385

.field public static final messagesDetailsImageHeight:I = 0x7f090386

.field public static final messagesDetailsImageMarginLeft:I = 0x7f090387

.field public static final messagesDetailsImageWidth:I = 0x7f090388

.field public static final messagesDetailsMessageTextSize:I = 0x7f090389

.field public static final messagesDetailsMyContainerMarginBottom:I = 0x7f09038a

.field public static final messagesDetailsMyContainerMarginLeft:I = 0x7f09038b

.field public static final messagesDetailsMyContainerMarginRight:I = 0x7f09038c

.field public static final messagesDetailsMyContainerMarginTop:I = 0x7f09038d

.field public static final messagesDetailsMyRowTimeMarginBottom:I = 0x7f09038e

.field public static final messagesDetailsMyRowTimeMarginLeft:I = 0x7f09038f

.field public static final messagesDetailsMyRowTimeMarginRight:I = 0x7f090390

.field public static final messagesDetailsMyRowTimeMarginTop:I = 0x7f090391

.field public static final messagesDetailsMyRowTimePaddingRight:I = 0x7f090392

.field public static final messagesDetailsMySpeechArrowMarginRight:I = 0x7f090393

.field public static final messagesDetailsMySpeechArrowMarginTop:I = 0x7f090394

.field public static final messagesDetailsMyTextMarginBottom:I = 0x7f090395

.field public static final messagesDetailsMyTextMarginLeft:I = 0x7f090396

.field public static final messagesDetailsMyTextMarginRight:I = 0x7f090397

.field public static final messagesDetailsMyTextMarginTop:I = 0x7f090398

.field public static final messagesDetailsOtherAttachmentPaddingBottom:I = 0x7f090399

.field public static final messagesDetailsOtherAttachmentPaddingRight:I = 0x7f09039a

.field public static final messagesDetailsOtherAttachmentTextSize:I = 0x7f09039b

.field public static final messagesDetailsOtherContainerMarginBottom:I = 0x7f09039c

.field public static final messagesDetailsOtherContainerMarginLeft:I = 0x7f09039d

.field public static final messagesDetailsOtherContainerMarginRight:I = 0x7f09039e

.field public static final messagesDetailsOtherContainerMarginTop:I = 0x7f09039f

.field public static final messagesDetailsOtherIconMarginLeft:I = 0x7f0903a0

.field public static final messagesDetailsOtherNameDrawablePadding:I = 0x7f0903a1

.field public static final messagesDetailsOtherNameMarginBottom:I = 0x7f0903a2

.field public static final messagesDetailsOtherNameMarginLeft:I = 0x7f0903a3

.field public static final messagesDetailsOtherNamePaddingRight:I = 0x7f0903a4

.field public static final messagesDetailsOtherNamePaddingTop:I = 0x7f0903a5

.field public static final messagesDetailsOtherNameTextSize:I = 0x7f0903a6

.field public static final messagesDetailsOtherRealNameMarginLeft:I = 0x7f0903a7

.field public static final messagesDetailsOtherRealNameMarginRight:I = 0x7f0903a8

.field public static final messagesDetailsOtherRealNameMarginTop:I = 0x7f0903a9

.field public static final messagesDetailsOtherSpeechArrowMarginLeft:I = 0x7f0903aa

.field public static final messagesDetailsOtherSpeechArrowMarginTop:I = 0x7f0903ab

.field public static final messagesDetailsOtherTextMarginBottom:I = 0x7f0903ac

.field public static final messagesDetailsOtherTextMarginLeft:I = 0x7f0903ad

.field public static final messagesDetailsOtherTextMarginRight:I = 0x7f0903ae

.field public static final messagesDetailsOtherTextMarginTop:I = 0x7f0903af

.field public static final messagesDetailsOtherTextPaddingRight:I = 0x7f0903b0

.field public static final messagesDetailsOtherTimeMarginBottom:I = 0x7f0903b1

.field public static final messagesDetailsOtherTimeMarginLeft:I = 0x7f0903b2

.field public static final messagesDetailsOtherTimeMarginRight:I = 0x7f0903b3

.field public static final messagesDetailsOtherTimeMarginTop:I = 0x7f0903b4

.field public static final messagesDetailsOtherTimePaddingLeft:I = 0x7f0903b5

.field public static final messagesDetailsOtherTimePaddingRight:I = 0x7f0903b6

.field public static final messagesDetailsRowPaddingBottom:I = 0x7f0903b7

.field public static final messagesDetailsRowPaddingTop:I = 0x7f0903b8

.field public static final messagesMargin:I = 0x7f0903b9

.field public static final messagesProfileImageHeight:I = 0x7f0900e0

.field public static final messagesProfileImageWidth:I = 0x7f0900e1

.field public static final messagesRecipientCountTextSize:I = 0x7f0903ba

.field public static final messagesReplyComposeMessageContainerMarginLeft:I = 0x7f0903bb

.field public static final messagesReplyComposeMessageContainerMarginRight:I = 0x7f0903bc

.field public static final messagesReplyComposeMessageContainerMarginTop:I = 0x7f0903bd

.field public static final messagesReplyComposeMessageSendButtonLayoutWidth:I = 0x7f0903be

.field public static final messagesReplyComposeMessageSendButtonTextSize:I = 0x7f0903bf

.field public static final messagesSummaryDefaultTextSize:I = 0x7f0903c0

.field public static final messagesSummaryHeaderTopMargin:I = 0x7f0903c1

.field public static final messagesSummaryRowAttachmentIconMarginRight:I = 0x7f0903c2

.field public static final messagesSummaryRowAttachmentIconPaddingBottom:I = 0x7f0903c3

.field public static final messagesSummaryRowAttachmentIconPaddingTop:I = 0x7f0903c4

.field public static final messagesSummaryRowAttachmentIconTextSize:I = 0x7f0903c5

.field public static final messagesSummaryRowComposeMessageButtonMarginRight:I = 0x7f0903c6

.field public static final messagesSummaryRowImageActualSize:I = 0x7f0903c7

.field public static final messagesSummaryRowImageHeight:I = 0x7f0903c8

.field public static final messagesSummaryRowImageMarginBottom:I = 0x7f0903c9

.field public static final messagesSummaryRowImageMarginLeft:I = 0x7f0903ca

.field public static final messagesSummaryRowImageMarginTop:I = 0x7f0903cb

.field public static final messagesSummaryRowImageWidth:I = 0x7f0903cc

.field public static final messagesSummaryRowMessageTextMarginLeft:I = 0x7f0903cd

.field public static final messagesSummaryRowMessageTextMarginRight:I = 0x7f0903ce

.field public static final messagesSummaryRowMessageTextMarginTop:I = 0x7f0903cf

.field public static final messagesSummaryRowMessageTextSize:I = 0x7f0903d0

.field public static final messagesSummaryRowPaddingLeft:I = 0x7f0903d1

.field public static final messagesSummaryRowPaddingRight:I = 0x7f0903d2

.field public static final messagesSummaryRowRealNameMarginLeft:I = 0x7f0903d3

.field public static final messagesSummaryRowRealNameMarginRight:I = 0x7f0903d4

.field public static final messagesSummaryRowRealNameMarginTop:I = 0x7f0903d5

.field public static final messagesSummaryRowTimeMarginRight:I = 0x7f0903d6

.field public static final messagesSummaryRowTimeMarginTop:I = 0x7f0903d7

.field public static final messagesSummaryRowTimePaddingTop:I = 0x7f0903d8

.field public static final messagesSummaryRowTimeTextSize:I = 0x7f0903d9

.field public static final messagesSummaryRowUnreadIconMarginTop:I = 0x7f0903da

.field public static final messagesSummaryRowUnreadIconTextSize:I = 0x7f0903db

.field public static final messagesSummaryRowUserNameDrawablePadding:I = 0x7f0903dc

.field public static final messagesSummaryRowUserNameMarginLeft:I = 0x7f0903dd

.field public static final messagesSummaryRowUserNameMarginTop:I = 0x7f0903de

.field public static final metacriticGreenboxWidth:I = 0x7f0900e2

.field public static final metroTitleTopMargin:I = 0x7f0900e3

.field public static final mottoMarginBottom:I = 0x7f0900e4

.field public static final mottoMarginRight:I = 0x7f0900e5

.field public static final mruRowListTitleTextSize:I = 0x7f0903df

.field public static final mru_row_margin:I = 0x7f0903e0

.field public static final mru_row_margin_bottom:I = 0x7f0903e1

.field public static final mru_row_margin_top:I = 0x7f0903e2

.field public static final mru_row_recent_title_margin_left:I = 0x7f0903e3

.field public static final mru_row_snap_title_margin_left:I = 0x7f0903e4

.field public static final notification_action_icon_size:I = 0x7f0903e5

.field public static final notification_action_text_size:I = 0x7f0903e6

.field public static final notification_big_circle_margin:I = 0x7f0903e7

.field public static final notification_content_margin_start:I = 0x7f090149

.field public static final notification_large_icon_height:I = 0x7f0903e8

.field public static final notification_large_icon_width:I = 0x7f0903e9

.field public static final notification_main_column_padding_top:I = 0x7f09014a

.field public static final notification_media_narrow_margin:I = 0x7f09014b

.field public static final notification_right_icon_size:I = 0x7f0903ea

.field public static final notification_right_side_padding_top:I = 0x7f090147

.field public static final notification_small_icon_background_padding:I = 0x7f0903eb

.field public static final notification_small_icon_size_as_large:I = 0x7f0903ec

.field public static final notification_subtext_size:I = 0x7f0903ed

.field public static final notification_top_pad:I = 0x7f0903ee

.field public static final notification_top_pad_large_text:I = 0x7f0903ef

.field public static final nowPlayingAppControlsPadding:I = 0x7f0903f0

.field public static final nowPlayingBoxArtMarginRight:I = 0x7f0903f1

.field public static final nowPlayingButtonHeight:I = 0x7f0903f2

.field public static final nowPlayingButtonTextSize:I = 0x7f0903f3

.field public static final nowPlayingButtonWidth:I = 0x7f0903f4

.field public static final nowPlayingCollapsedButtonSize:I = 0x7f0900e6

.field public static final nowPlayingCollapsedImageMarginBottom:I = 0x7f0903f5

.field public static final nowPlayingCollapsedImageMarginLeft:I = 0x7f0903f6

.field public static final nowPlayingCollapsedImageMarginTop:I = 0x7f0903f7

.field public static final nowPlayingCollapsedImagePrimaryHeight:I = 0x7f0900e7

.field public static final nowPlayingCollapsedImagePrimaryWidth:I = 0x7f0903f8

.field public static final nowPlayingCollapsedImageSecondaryHeight:I = 0x7f0900e8

.field public static final nowPlayingCollapsedPrimaryTextLength:I = 0x7f0903f9

.field public static final nowPlayingCollapsedTextMarginLeft:I = 0x7f0900e9

.field public static final nowPlayingCollapsedTextMarginTop:I = 0x7f0900ea

.field public static final nowPlayingCollapsedTextSize:I = 0x7f0900eb

.field public static final nowPlayingCollapsedVolumeButtonPaddingLeft:I = 0x7f0900ec

.field public static final nowPlayingCollapsedVolumeButtonPaddingRight:I = 0x7f0900ed

.field public static final nowPlayingExpandedHeight:I = 0x7f0903fa

.field public static final nowPlayingHeight:I = 0x7f0903fb

.field public static final nowPlayingImageHeight:I = 0x7f0903fc

.field public static final nowPlayingImageWidth:I = 0x7f0903fd

.field public static final nowPlayingLogoSize:I = 0x7f0903fe

.field public static final nowPlayingMediaButtonMarginBottom:I = 0x7f0903ff

.field public static final nowPlayingMediaButtonMarginTop:I = 0x7f090400

.field public static final nowPlayingMediaControlsMarginTop:I = 0x7f090401

.field public static final nowPlayingNextMarginTop:I = 0x7f090402

.field public static final nowPlayingPadding:I = 0x7f090403

.field public static final nowPlayingTitleMarginBottom:I = 0x7f090404

.field public static final nowPlayingTitleMarginTop:I = 0x7f090405

.field public static final oneguide_pivot_right_padding:I = 0x7f0900ee

.field public static final oobeContentPaddingLeft:I = 0x7f090406

.field public static final oobeContentPaddingRight:I = 0x7f090407

.field public static final oobeDescriptionTextSize:I = 0x7f090408

.field public static final oobeDetailTileHeight:I = 0x7f090409

.field public static final oobeDetailTileWidth:I = 0x7f09040a

.field public static final oobeFooterBottomMargin:I = 0x7f09040b

.field public static final oobeFooterTopMargin:I = 0x7f09040c

.field public static final oobeHeaderBottomMargin:I = 0x7f09040d

.field public static final oobeHeaderMarginBottom:I = 0x7f09040e

.field public static final oobeHeaderMarginTop:I = 0x7f09040f

.field public static final oobeHeaderTextSize:I = 0x7f090410

.field public static final oobeHeaderTopMargin:I = 0x7f090411

.field public static final oobeListItemPaddingTop:I = 0x7f090412

.field public static final oobeMarginLeft:I = 0x7f090413

.field public static final oobeMarginRight:I = 0x7f090414

.field public static final oobeMarginRightErase:I = 0x7f090415

.field public static final oobeProgressHeight:I = 0x7f090416

.field public static final oobeProgressInnerMargin:I = 0x7f090417

.field public static final oobeProgressOuterMargin:I = 0x7f090418

.field public static final oobeSolidWidth:I = 0x7f090419

.field public static final oobeSubHeaderBottomMargin:I = 0x7f09041a

.field public static final oobeTileHeight:I = 0x7f09041b

.field public static final oobeTileWidth:I = 0x7f09041c

.field public static final oobeconsolesetupIconSize:I = 0x7f09041d

.field public static final paddingPopupMessageContentMaxWidth:I = 0x7f09041e

.field public static final paddingPopupMessageLarge:I = 0x7f09041f

.field public static final paddingPopupMessageMedium:I = 0x7f090420

.field public static final paddingPopupMessageParagraph:I = 0x7f090421

.field public static final paddingPopupMessageSmall:I = 0x7f090422

.field public static final pageIndicatorMargin:I = 0x7f0900ef

.field public static final pageIndicatorSize:I = 0x7f0900f0

.field public static final party_member_gamerpic_size:I = 0x7f090423

.field public static final party_squawker_ring_size_silent:I = 0x7f090424

.field public static final party_squawker_ring_size_talking:I = 0x7f090425

.field public static final peopleActivityFeedActionMarginBottom:I = 0x7f090426

.field public static final peopleActivityFeedActionMarginLeft:I = 0x7f090427

.field public static final peopleActivityFeedActionMarginRight:I = 0x7f090428

.field public static final peopleActivityFeedActionMarginTop:I = 0x7f090429

.field public static final peopleActivityFeedActionPaddingLeft:I = 0x7f09042a

.field public static final peopleActivityFeedActionTextSize:I = 0x7f09042b

.field public static final peopleActivityFeedAttachmentTextMarginBottom:I = 0x7f09042c

.field public static final peopleActivityFeedAttachmentTextMarginLeft:I = 0x7f09042d

.field public static final peopleActivityFeedAttachmentTextMarginRight:I = 0x7f09042e

.field public static final peopleActivityFeedAttachmentTextMarginTop:I = 0x7f09042f

.field public static final peopleActivityFeedAttachmentTextPadding:I = 0x7f090430

.field public static final peopleActivityFeedAttachmentTextSize:I = 0x7f090431

.field public static final peopleActivityFeedCommentCountMarginLeft:I = 0x7f090432

.field public static final peopleActivityFeedCommentCountMinWidth:I = 0x7f090433

.field public static final peopleActivityFeedCommentCountTextSize:I = 0x7f090434

.field public static final peopleActivityFeedCommentIconTextSize:I = 0x7f090435

.field public static final peopleActivityFeedCommentTextSize:I = 0x7f090436

.field public static final peopleActivityFeedContainerPaddingBottom:I = 0x7f090437

.field public static final peopleActivityFeedContainerPaddingLeft:I = 0x7f090438

.field public static final peopleActivityFeedContainerPaddingRight:I = 0x7f090439

.field public static final peopleActivityFeedContainerPaddingTop:I = 0x7f09043a

.field public static final peopleActivityFeedContentMarginBottom:I = 0x7f09043b

.field public static final peopleActivityFeedContentMarginTop:I = 0x7f09043c

.field public static final peopleActivityFeedGamerpicSize:I = 0x7f09043d

.field public static final peopleActivityFeedImageMarginLeft:I = 0x7f09043e

.field public static final peopleActivityFeedImageSize:I = 0x7f09043f

.field public static final peopleActivityFeedImageSizeActual:I = 0x7f090440

.field public static final peopleActivityFeedItemAchievementPaddingBottom:I = 0x7f090441

.field public static final peopleActivityFeedItemAchievementPaddingLeft:I = 0x7f090442

.field public static final peopleActivityFeedItemAchievementPaddingRight:I = 0x7f090443

.field public static final peopleActivityFeedItemAchievementPaddingTop:I = 0x7f090444

.field public static final peopleActivityFeedItemAchievementTextSize:I = 0x7f090445

.field public static final peopleActivityFeedItemActionMarginLeft:I = 0x7f090446

.field public static final peopleActivityFeedItemDateMarginRight:I = 0x7f090447

.field public static final peopleActivityFeedItemDateTextSize:I = 0x7f090448

.field public static final peopleActivityFeedItemGamerTagMarginLeft:I = 0x7f090449

.field public static final peopleActivityFeedItemGamerTagTextSize:I = 0x7f09044a

.field public static final peopleActivityFeedItemRealNameMarginLeft:I = 0x7f09044b

.field public static final peopleActivityFeedItemRealNameTextSize:I = 0x7f09044c

.field public static final peopleActivityFeedItemScoreTextSize:I = 0x7f09044d

.field public static final peopleActivityFeedLeaderboardHeight:I = 0x7f09044e

.field public static final peopleActivityFeedLeaderboardSubtitleText:I = 0x7f09044f

.field public static final peopleActivityFeedLeaderboardTitleText:I = 0x7f090450

.field public static final peopleActivityFeedLegacyAchievementItemImagePaddingBottom:I = 0x7f090451

.field public static final peopleActivityFeedLegacyAchievementItemImagePaddingRight:I = 0x7f090452

.field public static final peopleActivityFeedLegacyAchievementItemImageWidth:I = 0x7f090453

.field public static final peopleActivityFeedLikeControlIconSize:I = 0x7f090454

.field public static final peopleActivityFeedLikeControlPaddingLeft:I = 0x7f090455

.field public static final peopleActivityFeedLikeControlTextSize:I = 0x7f090456

.field public static final peopleActivityFeedMarginTop:I = 0x7f090457

.field public static final peopleActivityFeedMetadataMarginBottom:I = 0x7f090458

.field public static final peopleActivityFeedMoreActionButtonTextSize:I = 0x7f090459

.field public static final peopleActivityFeedReportButtonTextSize:I = 0x7f09045a

.field public static final peopleActivityFeedShareButtonMarginLeft:I = 0x7f09045b

.field public static final peopleActivityFeedShareButtonPaddingBottom:I = 0x7f09045c

.field public static final peopleActivityFeedShareButtonPaddingTop:I = 0x7f09045d

.field public static final peopleActivityFeedShareButtonTextSize:I = 0x7f09045e

.field public static final peopleActivityFeedTargetUserImageMarginRight:I = 0x7f09045f

.field public static final peopleActivityFeedTargetUserImageSize:I = 0x7f090460

.field public static final peopleActivityFeedUGCCaptionMarginBottom:I = 0x7f090461

.field public static final peopleActivityFeedUGCCaptionMarginLeft:I = 0x7f090462

.field public static final peopleActivityFeedUGCCaptionMarginRight:I = 0x7f090463

.field public static final peopleActivityFeedUGCCaptionText:I = 0x7f090464

.field public static final peopleActivityFeedUndoButtonPaddingLeft:I = 0x7f090465

.field public static final peopleActivityFeedUndoButtonPaddingRight:I = 0x7f090466

.field public static final peopleButtonHeight:I = 0x7f090467

.field public static final peopleButtonWidth:I = 0x7f090468

.field public static final peopleScreenMargin:I = 0x7f090469

.field public static final pinsGalleryGridSpacing:I = 0x7f09046a

.field public static final pivotHeaderSize:I = 0x7f09046b

.field public static final pivotTitleBottomMargin:I = 0x7f09046c

.field public static final pivotTitleTopMargin:I = 0x7f0900f1

.field public static final placeholderImageHeight:I = 0x7f09046d

.field public static final playMediaImageSize:I = 0x7f09046e

.field public static final popularWithFriendsFriendCountTextMarginTop:I = 0x7f09046f

.field public static final popularWithFriendsFriendCountTextSize:I = 0x7f090470

.field public static final popularWithFriendsFriendsPlayedTextMarginTop:I = 0x7f090471

.field public static final popularWithFriendsFriendsPlayedTextSize:I = 0x7f090472

.field public static final popularWithFriendsGameImageMarginRight:I = 0x7f090473

.field public static final popularWithFriendsGameImageSize:I = 0x7f090474

.field public static final popularWithFriendsGameTitleTextMarginTop:I = 0x7f090475

.field public static final popularWithFriendsGameTitleTextSize:I = 0x7f090476

.field public static final popularWithFriendsHeaderMarginBottom:I = 0x7f090477

.field public static final popularWithFriendsHeaderMarginTop:I = 0x7f090478

.field public static final popularWithFriendsHeaderTextSize:I = 0x7f090479

.field public static final popularWithFriendsListRowPaddingBottom:I = 0x7f09047a

.field public static final popularWithFriendsListRowPaddingTop:I = 0x7f09047b

.field public static final popularWithFriendsListRowWidth:I = 0x7f09047c

.field public static final popularWithFriendsListViewDividerHeight:I = 0x7f09047d

.field public static final popularWithFriendsListViewMarginTop:I = 0x7f09047e

.field public static final popularWithFriendsListViewWidth:I = 0x7f09047f

.field public static final popupOffsetFromTop:I = 0x7f090480

.field public static final powerFlyoutSeparatorHeight:I = 0x7f0900f2

.field public static final privacyButtonHeight:I = 0x7f0900f3

.field public static final privacyButtonMarginBottom:I = 0x7f0900f4

.field public static final privacyDescriptionMarginBottom:I = 0x7f0900f5

.field public static final privacyDescriptionMarginTop:I = 0x7f0900f6

.field public static final privacyHeaderMarginBottom:I = 0x7f0900f7

.field public static final privacyHeaderMarginTop:I = 0x7f0900f8

.field public static final prividerPickerTitleMarginBottom:I = 0x7f090481

.field public static final profileAccountTierIconSize:I = 0x7f090482

.field public static final profileBioLabelTextSize:I = 0x7f090483

.field public static final profileBioMarginLeft:I = 0x7f090484

.field public static final profileBioTextMarginTop:I = 0x7f090485

.field public static final profileBioValueTextSize:I = 0x7f090486

.field public static final profileControlMarginDefault:I = 0x7f090487

.field public static final profileEditBioButtonMarginTop:I = 0x7f090488

.field public static final profileFollowersLabelMarginTop:I = 0x7f090489

.field public static final profileFollowersTextMarginTop:I = 0x7f09048a

.field public static final profileFollowingLabelMarginTop:I = 0x7f09048b

.field public static final profileFollowingTextMarginTop:I = 0x7f09048c

.field public static final profileGamerTagMarginBottom:I = 0x7f09048d

.field public static final profileGutterWidth:I = 0x7f09048e

.field public static final profileHeaderAccountTierIconSize:I = 0x7f09048f

.field public static final profileHeaderMarginTop:I = 0x7f090490

.field public static final profileImageTwitchIconSize:I = 0x7f090491

.field public static final profileImageTwitchIconSizeActual:I = 0x7f090492

.field public static final profileLabelTextSize:I = 0x7f090493

.field public static final profileLocationHeaderMarginTop:I = 0x7f090494

.field public static final profileLocationLabelTextSize:I = 0x7f090495

.field public static final profileLocationTextMarginTop:I = 0x7f090496

.field public static final profileLocationValueTextSize:I = 0x7f090497

.field public static final profileMoreButtonMarginTop:I = 0x7f090498

.field public static final profileMsaPicSize:I = 0x7f090499

.field public static final profilePicMarginRight:I = 0x7f09049a

.field public static final profilePivotPageContentMarginTop:I = 0x7f09049b

.field public static final profilePivotPageHeaderTopMargin:I = 0x7f09049c

.field public static final profilePivotPageHorizontalSpacingSize:I = 0x7f09049d

.field public static final profileRealNameTextSize:I = 0x7f09049e

.field public static final profileRecentArtSize:I = 0x7f09049f

.field public static final profileRecentGutterWidth:I = 0x7f0904a0

.field public static final profileReputationLabelTopMargin:I = 0x7f0904a1

.field public static final profileReputationStrokeWidth:I = 0x7f0904a2

.field public static final profileReputationTextTopMargin:I = 0x7f0904a3

.field public static final profileReputationViewHeight:I = 0x7f0904a4

.field public static final profileReputationViewTopMargin:I = 0x7f0904a5

.field public static final profileReputationViewWidth:I = 0x7f0904a6

.field public static final profileRootTopMargin:I = 0x7f0904a7

.field public static final profileShowcaseImageHeight:I = 0x7f0904a8

.field public static final profileShowcasePlayButtonSize_large:I = 0x7f0904a9

.field public static final profileShowcasePlayButtonSize_small:I = 0x7f0904aa

.field public static final profileSocialButtonIconSize:I = 0x7f0904ab

.field public static final profileSocialButtonsTopMargin:I = 0x7f0904ac

.field public static final profileSquareArtSize:I = 0x7f0904ad

.field public static final profileTitleHeaderMarginTop:I = 0x7f0904ae

.field public static final profileTopGamesGameImageMarginRight:I = 0x7f0904af

.field public static final profileTopGamesGameImageSize:I = 0x7f0904b0

.field public static final profileTopGamesGameTitleTextMarginTop:I = 0x7f0904b1

.field public static final profileTopGamesGameTitleTextSize:I = 0x7f0904b2

.field public static final profileTopGamesLastPlayedTextMarginTop:I = 0x7f0904b3

.field public static final profileTopGamesLastPlayedTextSize:I = 0x7f0904b4

.field public static final profileTopGamesListRowPaddingBottom:I = 0x7f0904b5

.field public static final profileTopGamesListRowPaddingTop:I = 0x7f0904b6

.field public static final profileTopGamesListViewWidth:I = 0x7f0904b7

.field public static final profileTopGamesPlayCountTextMarginTop:I = 0x7f0904b8

.field public static final profileTopGamesPlayCountTextSize:I = 0x7f0904b9

.field public static final profileTopGamesScreenMarginLeft:I = 0x7f0904ba

.field public static final profileTopGamesScreenMarginRight:I = 0x7f0904bb

.field public static final profileValueTextSize:I = 0x7f0904bc

.field public static final profileWatermarkIntramargin:I = 0x7f0904bd

.field public static final profileWatermarkListTopMargin:I = 0x7f0904be

.field public static final profileWatermarkSize:I = 0x7f0904bf

.field public static final providerPickerRowImgMargin:I = 0x7f0904c0

.field public static final providerPickerRowImgSize:I = 0x7f0904c1

.field public static final providerPickerRowTextMarginLeft:I = 0x7f0904c2

.field public static final purchaseResultDialogWidth:I = 0x7f0904c3

.field public static final purchaseWebviewBottomMargin:I = 0x7f0904c4

.field public static final recentActivityTileEdgeMargin:I = 0x7f0904c5

.field public static final recentActivityTileGraphRightMargin:I = 0x7f0904c6

.field public static final recentActivityTileIconsRightMargin:I = 0x7f0904c7

.field public static final recentActivityTileImageSize:I = 0x7f0904c8

.field public static final recordThatButtonHeight:I = 0x7f0904c9

.field public static final recordThatButtonPadding:I = 0x7f0904ca

.field public static final recordThatButtonWidth:I = 0x7f0904cb

.field public static final recordThatCaptionMarginBottom:I = 0x7f0904cc

.field public static final recordThatCaptionMarginTop:I = 0x7f0904cd

.field public static final recordThatCaptionPaddingBottom:I = 0x7f0904ce

.field public static final recordThatCaptionTextSize:I = 0x7f0904cf

.field public static final recordThatComponentMarginRight:I = 0x7f0904d0

.field public static final recordThatExpandedComponentMarginRight:I = 0x7f0904d1

.field public static final recordThatRedDotPadding:I = 0x7f0904d2

.field public static final recordThatWhiteRingThickness:I = 0x7f0904d3

.field public static final remoteAppInputPadding:I = 0x7f0904d4

.field public static final remoteBButtonRightMargin:I = 0x7f0904d5

.field public static final remoteButtonSize:I = 0x7f0904d6

.field public static final remoteButtonSizeSmall:I = 0x7f0904d7

.field public static final remoteCloseButtonMarginRight:I = 0x7f0904d8

.field public static final remoteCloseButtonMarginTop:I = 0x7f0904d9

.field public static final remoteControllerButtonSpacing:I = 0x7f0904da

.field public static final remoteDPadBottomPadding:I = 0x7f0904db

.field public static final remoteDPadTextSize:I = 0x7f0904dc

.field public static final remoteDPadTopPadding:I = 0x7f0904dd

.field public static final remoteDpadMargin:I = 0x7f0904de

.field public static final remoteLineButtonBTranslationX:I = 0x7f0904df

.field public static final remoteLineButtonHorizontalMargin:I = 0x7f0904e0

.field public static final remoteLineButtonTotalWidth:I = 0x7f0904e1

.field public static final remoteLineButtonWidth:I = 0x7f0904e2

.field public static final remoteLineButtonXTranslationX:I = 0x7f0904e3

.field public static final remoteLineButtonYTranslationX:I = 0x7f0904e4

.field public static final remotePadding:I = 0x7f0904e5

.field public static final remotePivotButtonsHeight:I = 0x7f0904e6

.field public static final remotePivotTabUnderline:I = 0x7f0904e7

.field public static final remoteSwitcherBottomPadding:I = 0x7f0904e8

.field public static final remoteSwitcherRightMargin:I = 0x7f0904e9

.field public static final remoteSwitcherTextSize:I = 0x7f0904ea

.field public static final remoteSwitcherTopPadding:I = 0x7f0904eb

.field public static final remoteTitleBarPadding:I = 0x7f0904ec

.field public static final remoteTitleTextSize:I = 0x7f0904ed

.field public static final remoteTopFillerHeight:I = 0x7f0904ee

.field public static final remoteVolumeButtonMargin:I = 0x7f0904ef

.field public static final remoteVolumeLauncherBottomMargin:I = 0x7f0904f0

.field public static final remoteVolumeLauncherRightMargin:I = 0x7f0904f1

.field public static final remoteVolumeLauncherSize:I = 0x7f0904f2

.field public static final remoteVolumeLauncherTopMargin:I = 0x7f0904f3

.field public static final remoteVolumeMuteUnmuteTextSize:I = 0x7f0904f4

.field public static final remoteVolumeUpDownTextSize:I = 0x7f0904f5

.field public static final remotedpadHeight:I = 0x7f0904f6

.field public static final remotedpadWidth:I = 0x7f0904f7

.field public static final right_pane_width:I = 0x7f0904f8

.field public static final screenGutterLeft:I = 0x7f0904f9

.field public static final screenGutterLeftFirstPane:I = 0x7f0904fa

.field public static final screenGutterRight:I = 0x7f0904fb

.field public static final screenGutterRightLastPane:I = 0x7f0904fc

.field public static final screenGutterWidth:I = 0x7f0904fd

.field public static final screenHeaderMarginTop:I = 0x7f0904fe

.field public static final screensMarginLeft:I = 0x7f0904ff

.field public static final searchBarButtonHeight:I = 0x7f0900f9

.field public static final searchBarButtonMarginRight:I = 0x7f0900fa

.field public static final searchBarButtonMarginTop:I = 0x7f0900fb

.field public static final searchBarButtonWidth:I = 0x7f0900fc

.field public static final searchBarHeight:I = 0x7f0900fd

.field public static final searchBarHintPaddingLeft:I = 0x7f0900fe

.field public static final searchBarInputPadding:I = 0x7f0900ff

.field public static final searchBarSearchIconTextSize:I = 0x7f090500

.field public static final searchDialogAnimationDistanceY:I = 0x7f090501

.field public static final searchDialogBottomMargin:I = 0x7f090502

.field public static final searchDialogInternalEditTextPaddingLeft:I = 0x7f090503

.field public static final searchDialogInternalMarginLeft:I = 0x7f090504

.field public static final searchDialogInternalMarginRight:I = 0x7f090505

.field public static final searchDialogInternalMarginTop:I = 0x7f090506

.field public static final searchDialogQuickResultsMarginLeft:I = 0x7f090507

.field public static final searchDialogQuickResultsMarginRight:I = 0x7f090508

.field public static final searchDialogQuickResultsMarginTop:I = 0x7f090509

.field public static final searchDialogQuickResultsTextSize:I = 0x7f09050a

.field public static final searchFlyoutContentWidth:I = 0x7f09050b

.field public static final searchFlyoutPadding:I = 0x7f090100

.field public static final searchFlyoutSearchButtonWidth:I = 0x7f09050c

.field public static final searchFlyoutSoftTouchWidth:I = 0x7f090101

.field public static final searchFlyoutTextSize:I = 0x7f090102

.field public static final searchGamerEntryHeight:I = 0x7f090103

.field public static final searchResultImageHeightWithMargin:I = 0x7f090104

.field public static final searchResultImageWidth:I = 0x7f090105

.field public static final searchResultMediaTypeIconSize:I = 0x7f090106

.field public static final searchResultSGIconHeight:I = 0x7f090107

.field public static final searchResultSGIconWidth:I = 0x7f090108

.field public static final searchResultTextMarginLeft:I = 0x7f090109

.field public static final searchScreenMargin:I = 0x7f09050d

.field public static final settingsCheckboxTextSize:I = 0x7f09050e

.field public static final settingsEditRealNameTextSize:I = 0x7f09050f

.field public static final settingsEntryDescriptionTextSize:I = 0x7f090510

.field public static final settingsIconSize:I = 0x7f090511

.field public static final settingsRadioButtonTextSize:I = 0x7f090512

.field public static final settingsRealNameDescriptionTextSize:I = 0x7f090513

.field public static final settingsRealNameTextSize:I = 0x7f090514

.field public static final settingsScreenColumnSeparator:I = 0x7f090515

.field public static final settingsScreenPropertySeparator:I = 0x7f090516

.field public static final settingsScreenRadioSeparator:I = 0x7f090517

.field public static final settingsScreenSectionMarginBottom:I = 0x7f090518

.field public static final settingsScreenSectionMarginTop:I = 0x7f090519

.field public static final settingsScreenSeparatorThickness:I = 0x7f09051a

.field public static final settingsScreenVersionTextMarginTop:I = 0x7f09051b

.field public static final settingsSectionNameTextSize:I = 0x7f09051c

.field public static final settingsTitleBottomMargin:I = 0x7f09051d

.field public static final settingsTitleTopMargin:I = 0x7f09051e

.field public static final shadowtarHeight:I = 0x7f09010a

.field public static final shadowtarWidth:I = 0x7f09010b

.field public static final shareDecisionDialogItemSize:I = 0x7f09051f

.field public static final shareDecisionDialogLabelFontSize:I = 0x7f090520

.field public static final shareDecisionDialogPadding:I = 0x7f090521

.field public static final shareIdentityDialogReg:I = 0x7f090522

.field public static final showcaseListItemCommentMoreButtonPaddingLeft:I = 0x7f090523

.field public static final showcaseListItemCommentMoreButtonPaddingRight:I = 0x7f090524

.field public static final showcaseListItemCommentMoreButtonPaddingTop:I = 0x7f090525

.field public static final showcaseListItemCommentPaddingBottom:I = 0x7f090526

.field public static final showcaseListItemCommentPaddingLeft:I = 0x7f090527

.field public static final showcaseListItemCommentPaddingRight:I = 0x7f090528

.field public static final showcaseListItemCommentPaddingTop:I = 0x7f090529

.field public static final showcaseListItemCommentTextSize:I = 0x7f09052a

.field public static final showcaseListItemDurationPaddingLeft:I = 0x7f09052b

.field public static final showcaseListItemDurationPaddingRight:I = 0x7f09052c

.field public static final showcaseListItemDurationPaddingTop:I = 0x7f09052d

.field public static final showcaseListItemDurationTextSize:I = 0x7f09052e

.field public static final showcaseListItemMargin:I = 0x7f09052f

.field public static final showcaseListItemMediaTitlePaddingBottom:I = 0x7f090530

.field public static final showcaseListItemMediaTitlePaddingLeft:I = 0x7f090531

.field public static final showcaseListItemMediaTitlePaddingRight:I = 0x7f090532

.field public static final showcaseListItemMediaTitlePaddingTop:I = 0x7f090533

.field public static final showcaseListItemMediaTitleTextSize:I = 0x7f090534

.field public static final showcaseListItemMediaViewCountPaddingBottom:I = 0x7f090535

.field public static final showcaseListItemMediaViewCountPaddingLeft:I = 0x7f090536

.field public static final showcaseListItemMediaViewCountPaddingRight:I = 0x7f090537

.field public static final showcaseListItemMediaViewCountPaddingTop:I = 0x7f090538

.field public static final showcaseListItemMediaViewCountTextSize:I = 0x7f090539

.field public static final showcaseListItemPlayImageSize:I = 0x7f09053a

.field public static final showcaseListItemTitlePaddingBottom:I = 0x7f09053b

.field public static final showcaseListItemTitlePaddingLeft:I = 0x7f09053c

.field public static final showcaseListItemTitlePaddingRight:I = 0x7f09053d

.field public static final showcaseListItemTitlePaddingTop:I = 0x7f09053e

.field public static final showcaseListItemTitleTextSize:I = 0x7f09053f

.field public static final signinButtonMarginBottom:I = 0x7f090540

.field public static final signinLogoSize:I = 0x7f090541

.field public static final slideButtonWidth:I = 0x7f090542

.field public static final smallNumpadBranchButtonTextSize:I = 0x7f090012

.field public static final smartGlassControlPickerButtonSize:I = 0x7f09010c

.field public static final smartGlassControlPickerIconSize:I = 0x7f090543

.field public static final smartGlassControlPickerMargin:I = 0x7f090544

.field public static final smartGlassDrawablePadding:I = 0x7f090545

.field public static final smartGlassMarginBottom:I = 0x7f090546

.field public static final smartglassEnabledIconHeight:I = 0x7f09010d

.field public static final smartglassEnabledIconWidth:I = 0x7f09010e

.field public static final socialBarActionButtonWidth:I = 0x7f090547

.field public static final socialBarButtonWidth:I = 0x7f090548

.field public static final socialBarRowHeight:I = 0x7f090549

.field public static final socialButtonFullHeight:I = 0x7f09010f

.field public static final socialButtonHalfHeight:I = 0x7f090110

.field public static final socialButtonSpacing:I = 0x7f090111

.field public static final socialButtonWidth:I = 0x7f090112

.field public static final socialButtonsMarginBottom:I = 0x7f090113

.field public static final socialButtonsMarginRight:I = 0x7f090114

.field public static final socialMottoBubbleWidth:I = 0x7f090115

.field public static final socialRequestCountMargin:I = 0x7f090116

.field public static final socialRequestCountSize:I = 0x7f090117

.field public static final standardBottomButtonsBarMargin:I = 0x7f090020

.field public static final standardImageDataMargin:I = 0x7f09054a

.field public static final standardItemMargin:I = 0x7f09054b

.field public static final starRatingDialogMaxWidth:I = 0x7f09054c

.field public static final storeGoldItemDateMarginBottom:I = 0x7f09054d

.field public static final storeGoldItemDateMarginTop:I = 0x7f09054e

.field public static final storeGoldItemDateTextSize:I = 0x7f09054f

.field public static final storeGoldItemPriceIconTextSize:I = 0x7f090550

.field public static final storeGoldItemPriceMarginBottom:I = 0x7f090551

.field public static final storeGoldItemPriceMarginSides:I = 0x7f090552

.field public static final storeGoldItemPriceMarginTop:I = 0x7f090553

.field public static final storeGoldItemPriceTextSize:I = 0x7f090554

.field public static final storeGoldItemSubheaderTextSize:I = 0x7f090555

.field public static final storeGoldItemTitleMarginBottom:I = 0x7f090556

.field public static final storeGoldItemTitleMarginTop:I = 0x7f090557

.field public static final storeGoldItemTitleTextSize:I = 0x7f090558

.field public static final store_pivot_right_padding:I = 0x7f090021

.field public static final tabBarHeight:I = 0x7f090559

.field public static final tabIconMargin:I = 0x7f09055a

.field public static final tabIconWidth:I = 0x7f09055b

.field public static final tabTitleWidth:I = 0x7f09055c

.field public static final textInputButtonSpacing:I = 0x7f09055d

.field public static final textInputButtonWidth:I = 0x7f09055e

.field public static final textInputConfirmIconTextSize:I = 0x7f09055f

.field public static final textInputContainerWidth:I = 0x7f090560

.field public static final textInputHelpTextSize:I = 0x7f090561

.field public static final textInputMultiLineTextboxHeight:I = 0x7f090562

.field public static final textInputPadding:I = 0x7f090118

.field public static final textInputPromptMargin:I = 0x7f090563

.field public static final textInputPromptMarginBottom:I = 0x7f090564

.field public static final textInputTextPaddingLeft:I = 0x7f090565

.field public static final textInputTextSize:I = 0x7f090566

.field public static final textInputTextboxHeight:I = 0x7f090567

.field public static final textOnlyButtonHeightDefault:I = 0x7f090119

.field public static final textOnlyButtonWidthDefault:I = 0x7f09011a

.field public static final thirdPartyNoticesContentPaddingLeft:I = 0x7f090568

.field public static final thirdPartyNoticesContentPaddingRight:I = 0x7f090569

.field public static final thirdPartyNoticesContentTextSize:I = 0x7f09056a

.field public static final thirdPartyNoticesContentTopBottomMargin:I = 0x7f09056b

.field public static final thirdPartyNoticesHeaderBottomMargin:I = 0x7f09056c

.field public static final thirdPartyNoticesHeaderTextSize:I = 0x7f09056d

.field public static final thirdPartyNoticesHeaderTopMargin:I = 0x7f09056e

.field public static final titleBarButtonSize:I = 0x7f09056f

.field public static final titleBarHeight:I = 0x7f09011b

.field public static final titleBarLeft:I = 0x7f09011c

.field public static final titleBarRight:I = 0x7f09011d

.field public static final titleBarTop:I = 0x7f09011e

.field public static final titleProgressAchievementsItemMinHeight:I = 0x7f090570

.field public static final titleProgressChallengeImageHeight:I = 0x7f090571

.field public static final titleProgressRectImageWidth:I = 0x7f090572

.field public static final topLevelScreenMargin:I = 0x7f09011f

.field public static final topLevelScreenMarginFollowingLeft:I = 0x7f090120

.field public static final topLevelScreenMarginFollowingTop:I = 0x7f090573

.field public static final tvHorizontalListBoxArtHeight:I = 0x7f090574

.field public static final tvHorizontalListBoxArtWidth:I = 0x7f090575

.field public static final tvHorizontalListLiveArtHeight:I = 0x7f090576

.field public static final tvHorizontalListLiveArtWidth:I = 0x7f090577

.field public static final tvHorizontalListMultipleListSpacing:I = 0x7f090578

.field public static final tvHubPhoneStreamerControlBarHeight:I = 0x7f090579

.field public static final tvHubTabletBottomMargin:I = 0x7f09057a

.field public static final tvHubTabletPadding:I = 0x7f09057b

.field public static final tvHubTabletPaddingBottom:I = 0x7f09057c

.field public static final tvHubTabletPaddingLeft:I = 0x7f09057d

.field public static final tvHubTabletPaddingTop:I = 0x7f09057e

.field public static final tvHubTabletProfileSize:I = 0x7f09057f

.field public static final tvHubTabletTopMargin:I = 0x7f090580

.field public static final tvListingHeaderHeight:I = 0x7f090581

.field public static final tvListingHeaderMargin:I = 0x7f090582

.field public static final tvListingHeaderNowWidth:I = 0x7f090583

.field public static final tvListingTimelineHeight:I = 0x7f090584

.field public static final tvStreamerPipHeight:I = 0x7f090585

.field public static final tvStreamerPipWidth:I = 0x7f090586

.field public static final urcMarginSides:I = 0x7f090121

.field public static final urcMaxWidth:I = 0x7f090587

.field public static final urcPaddingSides:I = 0x7f090122

.field public static final userTile:I = 0x7f090588

.field public static final utilityBarConnectTextMarginRight:I = 0x7f090589

.field public static final utilityBarConnectTextSize:I = 0x7f09058a

.field public static final utilityBarConnectTextTranslationY:I = 0x7f09058b

.field public static final utilityBarTitleTextSize:I = 0x7f090123

.field public static final volumeDownHeight:I = 0x7f090013

.field public static final volumeDownWidth:I = 0x7f090014

.field public static final volumeMuteHeight:I = 0x7f090015

.field public static final volumeMuteWidth:I = 0x7f090016

.field public static final volumeUpHeight:I = 0x7f090017

.field public static final volumeUpWidth:I = 0x7f090018

.field public static final welcomeLogoWidth:I = 0x7f090124

.field public static final welcomeSignInButtonHeight:I = 0x7f09058c

.field public static final welcomeSignInButtonTextSize:I = 0x7f09058d

.field public static final welcomeSignInButtonWidth:I = 0x7f090125

.field public static final welcome_card_button_padding:I = 0x7f090139

.field public static final welcome_card_completion_indicator_icon_size:I = 0x7f09013a

.field public static final welcome_card_height:I = 0x7f09013b

.field public static final welcome_card_icon_size:I = 0x7f09013c

.field public static final welcome_card_inner_padding_from_top:I = 0x7f09013d

.field public static final welcome_card_inner_side_padding:I = 0x7f09013e

.field public static final welcome_card_inner_upper_padding:I = 0x7f09013f

.field public static final welcome_card_margin_sides:I = 0x7f090140

.field public static final welcome_card_margin_top:I = 0x7f090141

.field public static final welcome_card_screen_title_size:I = 0x7f090142

.field public static final welcome_card_spacing_between_items:I = 0x7f090143

.field public static final welcome_card_spacing_between_items_and_button:I = 0x7f090144

.field public static final welcome_card_sub_text_size:I = 0x7f090145

.field public static final welcome_card_text_size:I = 0x7f090146

.field public static final whatsnewContentPaddingLeft:I = 0x7f09058e

.field public static final whatsnewContentPaddingRight:I = 0x7f09058f

.field public static final whatsnewDetailTileHeight:I = 0x7f090126

.field public static final whatsnewDetailTileWidth:I = 0x7f090127

.field public static final whatsnewFooterBottomMargin:I = 0x7f090590

.field public static final whatsnewFooterTopMargin:I = 0x7f090591

.field public static final whatsnewHeaderBottomMargin:I = 0x7f090592

.field public static final whatsnewHeaderTextSize:I = 0x7f090593

.field public static final whatsnewHeaderTopMargin:I = 0x7f090594

.field public static final whatsnewListItemPaddingTop:I = 0x7f090595

.field public static final whatsnewSolidWidth:I = 0x7f090596

.field public static final whatsnewSubHeaderBottomMargin:I = 0x7f090597

.field public static final whatsnewSubHeaderTextSize:I = 0x7f090598

.field public static final whatsnewTileHeight:I = 0x7f090128

.field public static final whatsnewTileWidth:I = 0x7f090129

.field public static final widthHeightLongAspectRatio:I = 0x7f090599

.field public static final wrapOnTablet_matchOnPhone:I = 0x7f09059a

.field public static final wrap_content:I = 0x7f09059b

.field public static final wrap_content_fix:I = 0x7f09059c

.field public static final xbid_bg_stroke_width:I = 0x7f09059d

.field public static final xbid_body_image_diameter:I = 0x7f09059e

.field public static final xbid_body_padding_horizontal:I = 0x7f09059f

.field public static final xbid_body_padding_vertical:I = 0x7f0905a0

.field public static final xbid_bottom_bar_shadow_height:I = 0x7f0905a1

.field public static final xbid_button_border_width:I = 0x7f0905a2

.field public static final xbid_clickable_view_border_width:I = 0x7f0905a3

.field public static final xbid_clickable_view_padding:I = 0x7f0905a4

.field public static final xbid_header_image_diameter:I = 0x7f0905a5

.field public static final xbid_margin_body_standard:I = 0x7f0905a6

.field public static final xbid_margin_button:I = 0x7f0905a7

.field public static final xbid_margin_button_top:I = 0x7f0905a8

.field public static final xboxLogoHeight:I = 0x7f09012a

.field public static final xboxLogoWidth:I = 0x7f09012b

.field public static final xenonControlButtonHeight:I = 0x7f0905a9

.field public static final xenonControlButtonWidth:I = 0x7f0905aa

.field public static final youProfileFollowersMarginLeft:I = 0x7f0905ab

.field public static final youProfileMottoBubbleWidth:I = 0x7f09012c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
