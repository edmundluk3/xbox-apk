.class public Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "PopularWithFriendsScreenAdapter.java"


# instance fields
.field private header:Landroid/widget/TextView;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;

.field private popularGames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/PopularGame;",
            ">;"
        }
    .end annotation
.end field

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)V
    .locals 3
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    .line 31
    const v0, 0x7f0e0980

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 32
    const v0, 0x7f0e097f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->header:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f0e0981

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 35
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0301d6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->header:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->header:Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070a5e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 57
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 63
    return-void
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->isBusy()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->getPopularGames()Ljava/util/ArrayList;

    move-result-object v0

    .line 75
    .local v0, "newData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->popularGames:Ljava/util/ArrayList;

    if-eq v1, v0, :cond_1

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->clear()V

    .line 78
    if-eqz v0, :cond_0

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->addAll(Ljava/util/Collection;)V

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 82
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;->popularGames:Ljava/util/ArrayList;

    .line 84
    :cond_1
    return-void
.end method
