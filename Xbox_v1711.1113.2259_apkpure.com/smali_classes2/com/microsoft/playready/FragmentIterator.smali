.class public Lcom/microsoft/playready/FragmentIterator;
.super Ljava/lang/Object;
.source "FragmentIterator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/FragmentIterator$FragmentInfo;,
        Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;
    }
.end annotation


# instance fields
.field private m_ManifestListener:Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;

.field private m_ManifestUpdateFilter:Lcom/microsoft/playready/ManifestUpdateFilter;

.field private m_currentFragmentIndex:I

.field private m_currentFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

.field private m_currentQualityLevelIndex:I

.field private m_currentStreamIndex:I

.field private m_isValid:Z

.field private m_lockObj:Ljava/lang/Object;

.field private m_mediaRepresentation:Lcom/microsoft/playready/MediaRepresentation;

.field private m_mpProxy:Lcom/microsoft/playready/MediaProxy;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/MediaProxy;Lcom/microsoft/playready/ManifestUpdateFilter;Lcom/microsoft/playready/MediaRepresentation;J)V
    .locals 4
    .param p1, "mpProxy"    # Lcom/microsoft/playready/MediaProxy;
    .param p2, "updateFilter"    # Lcom/microsoft/playready/ManifestUpdateFilter;
    .param p3, "representation"    # Lcom/microsoft/playready/MediaRepresentation;
    .param p4, "startTimeMs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/playready/FragmentEOSException;,
            Lcom/microsoft/playready/FragmentBOSException;,
            Lcom/microsoft/playready/MediaException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    iput-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_mpProxy:Lcom/microsoft/playready/MediaProxy;

    .line 152
    iput v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    .line 153
    iput v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    .line 154
    iput v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentQualityLevelIndex:I

    .line 155
    iput-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestListener:Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;

    .line 156
    iput-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestUpdateFilter:Lcom/microsoft/playready/ManifestUpdateFilter;

    .line 157
    iput-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    .line 158
    iput-boolean v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_isValid:Z

    .line 159
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_lockObj:Ljava/lang/Object;

    .line 160
    iput-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_mediaRepresentation:Lcom/microsoft/playready/MediaRepresentation;

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_isValid:Z

    .line 169
    iput-object p1, p0, Lcom/microsoft/playready/FragmentIterator;->m_mpProxy:Lcom/microsoft/playready/MediaProxy;

    .line 170
    iput-object p3, p0, Lcom/microsoft/playready/FragmentIterator;->m_mediaRepresentation:Lcom/microsoft/playready/MediaRepresentation;

    .line 171
    invoke-virtual {p3}, Lcom/microsoft/playready/MediaRepresentation;->getParentStream()Lcom/microsoft/playready/MediaStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/playready/MediaStream;->getStreamIndex()I

    move-result v0

    iput v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    .line 172
    invoke-virtual {p3}, Lcom/microsoft/playready/MediaRepresentation;->getQualityLevelIndex()I

    move-result v0

    iput v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentQualityLevelIndex:I

    .line 173
    iget-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_mpProxy:Lcom/microsoft/playready/MediaProxy;

    iget v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    invoke-virtual {v0, v1, p4, p5}, Lcom/microsoft/playready/MediaProxy;->getFragmentIndexAtTime(IJ)I

    move-result v0

    iput v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    .line 174
    iget-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_mpProxy:Lcom/microsoft/playready/MediaProxy;

    iget v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/MediaProxy;->getFragmentInfo(II)Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    .line 176
    new-instance v0, Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;

    invoke-direct {v0, p0, p0}, Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;-><init>(Lcom/microsoft/playready/FragmentIterator;Lcom/microsoft/playready/FragmentIterator;)V

    iput-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestListener:Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;

    .line 177
    iput-object p2, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestUpdateFilter:Lcom/microsoft/playready/ManifestUpdateFilter;

    .line 179
    iget-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestUpdateFilter:Lcom/microsoft/playready/ManifestUpdateFilter;

    iget-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestListener:Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;

    invoke-virtual {p3}, Lcom/microsoft/playready/MediaRepresentation;->getParentStream()Lcom/microsoft/playready/MediaStream;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/ManifestUpdateFilter;->addStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;Lcom/microsoft/playready/MediaStream;)V

    .line 180
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 3

    .prologue
    .line 186
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestUpdateFilter:Lcom/microsoft/playready/ManifestUpdateFilter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestListener:Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestUpdateFilter:Lcom/microsoft/playready/ManifestUpdateFilter;

    iget-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_ManifestListener:Lcom/microsoft/playready/FragmentIterator$ManifestUpdateListener;

    iget v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/ManifestUpdateFilter;->removeStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 189
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method getFragmentIndex()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    return v0
.end method

.method public getFragmentInfo()Lcom/microsoft/playready/FragmentIterator$FragmentInfo;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    return-object v0
.end method

.method getMediaRepresentation()Lcom/microsoft/playready/MediaRepresentation;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_mediaRepresentation:Lcom/microsoft/playready/MediaRepresentation;

    return-object v0
.end method

.method getQLIndex()I
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentQualityLevelIndex:I

    return v0
.end method

.method getStreamIndex()I
    .locals 1

    .prologue
    .line 273
    iget v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/microsoft/playready/FragmentIterator;->m_isValid:Z

    return v0
.end method

.method public next()Z
    .locals 6

    .prologue
    .line 204
    const/4 v1, 0x1

    .line 205
    .local v1, "result":Z
    iget-object v3, p0, Lcom/microsoft/playready/FragmentIterator;->m_lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 207
    :try_start_0
    iget-boolean v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_isValid:Z

    if-nez v2, :cond_0

    .line 209
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v4, "This Iterator is no longer valid, please create a new one"

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 205
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 214
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_mpProxy:Lcom/microsoft/playready/MediaProxy;

    iget v4, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    iget v5, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/playready/MediaProxy;->getFragmentInfo(II)Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    .line 215
    iget v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I
    :try_end_1
    .catch Lcom/microsoft/playready/FragmentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :goto_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 223
    return v1

    .line 217
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Lcom/microsoft/playready/FragmentException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method onManifestUpdate()V
    .locals 6

    .prologue
    .line 252
    iget-object v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_lockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 256
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_mpProxy:Lcom/microsoft/playready/MediaProxy;

    iget v3, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    iget-object v4, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    invoke-static {v4}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->access$1(Lcom/microsoft/playready/FragmentIterator$FragmentInfo;)J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Lcom/microsoft/playready/MediaProxy;->getFragmentIndexAtTime(IJ)I

    move-result v1

    iput v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    .line 257
    iget-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_mpProxy:Lcom/microsoft/playready/MediaProxy;

    iget v3, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    iget v4, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    invoke-virtual {v1, v3, v4}, Lcom/microsoft/playready/MediaProxy;->getFragmentInfo(II)Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 264
    return-void

    .line 259
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/playready/FragmentIterator;->m_isValid:Z

    goto :goto_0

    .line 252
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public previous()Z
    .locals 6

    .prologue
    .line 228
    const/4 v1, 0x1

    .line 229
    .local v1, "result":Z
    iget-object v3, p0, Lcom/microsoft/playready/FragmentIterator;->m_lockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 231
    :try_start_0
    iget-boolean v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_isValid:Z

    if-nez v2, :cond_0

    .line 233
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v4, "This Iterator is no longer valid, please create a new one"

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 229
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 238
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_mpProxy:Lcom/microsoft/playready/MediaProxy;

    iget v4, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentStreamIndex:I

    iget v5, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/playready/MediaProxy;->getFragmentInfo(II)Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    .line 239
    iget v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/microsoft/playready/FragmentIterator;->m_currentFragmentIndex:I
    :try_end_1
    .catch Lcom/microsoft/playready/FragmentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229
    :goto_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 247
    return v1

    .line 241
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Lcom/microsoft/playready/FragmentException;
    const/4 v1, 0x0

    goto :goto_0
.end method
