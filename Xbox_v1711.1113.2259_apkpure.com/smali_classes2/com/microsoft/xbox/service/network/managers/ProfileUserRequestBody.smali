.class public Lcom/microsoft/xbox/service/network/managers/ProfileUserRequestBody;
.super Ljava/lang/Object;
.source "ProfileUserRequestBody.java"


# instance fields
.field public settings:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

.field public userIds:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)V
    .locals 0
    .param p2, "settings"    # [Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;",
            ")V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "userIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ProfileUserRequestBody;->userIds:Ljava/util/Collection;

    .line 16
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/ProfileUserRequestBody;->settings:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .line 17
    return-void
.end method
