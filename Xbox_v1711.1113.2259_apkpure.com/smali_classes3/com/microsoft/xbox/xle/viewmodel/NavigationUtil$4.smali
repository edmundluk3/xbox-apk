.class final Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;
.super Ljava/lang/Object;
.source "NavigationUtil.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

.field final synthetic val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

.field final synthetic val$holder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

.field final synthetic val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field final synthetic val$onPreClick:Ljava/lang/Runnable;

.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 501
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$holder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iput-object p5, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    iput-object p6, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$onPreClick:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 511
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$holder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->isViewAccessibilityFocused(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$holder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->parent:Landroid/view/View;

    .line 512
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->isViewAccessibilityFocused(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    .line 525
    :goto_0
    return-void

    .line 515
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$onPreClick:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->access$000(Ljava/lang/Runnable;)V

    .line 517
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->getAuthorType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->Club:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    if-ne v0, v1, :cond_2

    .line 518
    const-string v0, "Clubs - Activity Feed Navigate to Club"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackNavigateToClub(Ljava/lang/String;J)V

    .line 519
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V

    goto :goto_0

    .line 521
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_FRIEND_ADD:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->access$100(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    .line 522
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$4;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->targetUser:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    goto :goto_0
.end method
