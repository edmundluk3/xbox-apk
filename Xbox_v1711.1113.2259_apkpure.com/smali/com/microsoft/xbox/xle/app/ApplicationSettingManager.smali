.class public Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;
.super Ljava/lang/Object;
.source "ApplicationSettingManager.java"


# static fields
.field private static final ConnectedLocale:Ljava/lang/String; = "ConnectedLocale"

.field private static final CurrentSandboxId:Ljava/lang/String; = "CurrentSandboxID"

.field private static final DefaultSandboxId:Ljava/lang/String; = "DEFAULTSANDBOXID"

.field private static final EDSVersion:Ljava/lang/String; = "EDS"

.field private static final EXPERIMENT_SETTINGS:Ljava/lang/String; = "experimentsettings"

.field private static final GUID:Ljava/lang/String; = "GUID"

.field private static final HIDE_CLUB_DISCOVERY_BLOCK:Ljava/lang/String; = "HIDE_CLUB_DISCOVERY_BLOCK"

.field private static final HelpCompanionWhiteListUrls:Ljava/lang/String; = "HELPCOMPANION_ALLOWEDURLS"

.field private static final INVERT_H_SWIPE:Ljava/lang/String; = "INVERT_H_SWIPE"

.field private static final INVERT_V_SWIPE:Ljava/lang/String; = "INVERT_V_SWIPE"

.field private static final LEFT_HANDED_REMOTED:Ljava/lang/String; = "LEFT_HANDED_REMOTED"

.field private static final MESSAGESDB_CONVERSATIONS_METADATA:Ljava/lang/String; = "MESSAGESDB_CONVERSATIONS_METADATA"

.field private static final MESSAGESDB_CONVERSATION_LIST_RESULT:Ljava/lang/String; = "MESSAGESDB_CONVERSATION_LIST_RESULT"

.field private static final MESSAGESDB_RESET:Ljava/lang/String; = "MESSAGESDB_RESET"

.field private static final MESSAGESDB_SKYPE_CONVERSATION_MESSAGE_CACHE:Ljava/lang/String; = "MESSAGESDB_SKYPE_CONVERSATION_MESSAGE_CACHE"

.field private static final MePreferredColor:Ljava/lang/String; = "MePreferredColor"

.field private static final NOWPLAYING_SIMULATOR:Ljava/lang/String; = "NowPlayingSimulator"

.field private static final NetworkTestingEnabled:Ljava/lang/String; = "SMARTGLASSNETWORKTESTINGENABLED"

.field private static final NonAdultContentRatings:Ljava/lang/String; = "SMARTGLASSNONADULTCONTENTRATINGS"

.field private static final PINS_TUTORIAL:Ljava/lang/String; = "PINS_TUTORIAL"

.field private static final PhoneContactsUploadTimestamp:Ljava/lang/String; = "PHONECONTACTSUPLOADTIMESTAMP"

.field private static final PhoneNumber:Ljava/lang/String; = "PHONENUMBER"

.field private static final PostOOBEStatus:Ljava/lang/String; = "hasDonePostOOBE"

.field private static final PowerEnabled:Ljava/lang/String; = "SMARTGLASSPOWERENABLED"

.field private static final PremiumTVEnabled:Ljava/lang/String; = "SMARTGLASSGUIDEENABLED"

.field private static final PremiumUrcEnabled:Ljava/lang/String; = "SMARTGLASSURCENABLED"

.field private static final STAY_AWAKE:Ljava/lang/String; = "STAY_AWAKE"

.field private static final ShowSpecificContentRatingsEnabled:Ljava/lang/String; = "SMARTGLASSSHOWSPECIFICCONTENTRATINGS"

.field private static final StayAwakeDefaultSetting:Ljava/lang/String; = "StayAwakeDefaultSetting"

.field private static final TAG:Ljava/lang/String; = "ApplicationSettingManager"

.field private static final TreatMissingRatingAsAdult:Ljava/lang/String; = "SMARTGLASSTREATMISSINGRATINGASADULT"

.field private static final XBOX_SERVCIES_CONFIG:Ljava/lang/String; = "xboxservices"

.field private static final defaultTitleId:J = 0x138f999eL

.field private static final fileName:Ljava/lang/String; = "xlesetting"

.field private static final instance:Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

.field private static final lastWhatsNewDetailsShown:Ljava/lang/String; = "showWhatsNewDetailsVersionCode"

.field private static final lastestVersionChecked:Ljava/lang/String; = "lastestVersionChecked"

.field private static final rateAppEnabledSetting:Ljava/lang/String; = "rateAppEnabled"

.field private static final rateAppLaunchSetting:Ljava/lang/String; = "rateAppTrackLaunches"

.field private static final rateAppUri:Ljava/lang/String; = "rateAppUri"

.field private static final rateAppVersionSetting:Ljava/lang/String; = "rateAppLastVersion"

.field private static final soundStatus:Ljava/lang/String; = "hasSound"

.field private static final systemCheckStatus:Ljava/lang/String; = "hasSystemCheck"


# instance fields
.field private appConfig:Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;

.field private appGuid:Ljava/lang/String;

.field private buildNumber:Ljava/lang/String;

.field private connectedLocale:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private conversationListResult:Ljava/lang/String;

.field private conversationsMetadata:Ljava/lang/String;

.field private defaultSandboxId:Ljava/lang/String;

.field private experimentSettings:Ljava/lang/String;

.field private hasDonePostOOBE:Z

.field private hasSound:Z

.field private hasSystemCheck:Z

.field private helpWhiteListUrls:Ljava/lang/String;

.field private hideClubDiscoveryBlock:Z

.field private invertHSwipe:Z

.field private invertVSwipe:Z

.field private isMessagingDBReset:Z

.field private lastCheckedVersion:I

.field private lastWhatsNewDetailsVersion:I

.field private leftHandedRemote:Z

.field private mePreferredColor:I

.field private networkTestingEnabled:Z

.field private nonAdultContentRatings:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private nowPlayingSimulatorEnabled:Z

.field private phoneContactsUploadTimestamp:J

.field private phoneNumber:Ljava/lang/String;

.field private powerEnabled:Z

.field private preferences:Landroid/content/SharedPreferences;

.field private premiumTVEnabled:Z

.field private premiumUrcEnabled:Z

.field private showSpecificContentRatingsEnabled:Z

.field private skypeConversationMessageCache:Ljava/lang/String;

.field private stayAwake:Z

.field private stayAwakeSetting:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

.field private titleId:J

.field private treatMissingRatingAsAdult:Z

.field private useEDS31:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->instance:Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->connectedLocale:Ljava/lang/String;

    .line 125
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->appGuid:Ljava/lang/String;

    .line 126
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasSystemCheck:Z

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasSound:Z

    .line 128
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->defaultSandboxId:Ljava/lang/String;

    .line 129
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->premiumTVEnabled:Z

    .line 130
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->premiumUrcEnabled:Z

    .line 131
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->showSpecificContentRatingsEnabled:Z

    .line 132
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->treatMissingRatingAsAdult:Z

    .line 133
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->nonAdultContentRatings:Ljava/util/Set;

    .line 134
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasDonePostOOBE:Z

    .line 135
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->powerEnabled:Z

    .line 136
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->networkTestingEnabled:Z

    .line 137
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    .line 138
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->phoneNumber:Ljava/lang/String;

    .line 139
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->isMessagingDBReset:Z

    .line 140
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->conversationsMetadata:Ljava/lang/String;

    .line 141
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->conversationListResult:Ljava/lang/String;

    .line 142
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->skypeConversationMessageCache:Ljava/lang/String;

    .line 143
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hideClubDiscoveryBlock:Z

    .line 144
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->loadAllSettings()V

    .line 145
    return-void
.end method

.method private createAndSaveGUID()Ljava/lang/String;
    .locals 5

    .prologue
    .line 511
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v3, "xlesetting"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 512
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 513
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 514
    .local v1, "guid":Ljava/lang/String;
    const-string v2, "GUID"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 515
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 516
    return-object v1
.end method

.method private getAppConfig()Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->appConfig:Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;

    if-nez v0, :cond_1

    .line 236
    monitor-enter p0

    .line 237
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->appConfig:Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;

    if-nez v0, :cond_0

    .line 238
    new-instance v0, Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->appConfig:Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;

    .line 240
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->appConfig:Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;

    return-object v0

    .line 240
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;
    .locals 1

    .prologue
    .line 231
    sget-object v0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->instance:Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    return-object v0
.end method

.method private loadAllSettings()V
    .locals 12

    .prologue
    .line 148
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v8, "xlesetting"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 149
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "ConnectedLocale"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->connectedLocale:Ljava/lang/String;

    .line 150
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "hasSound"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasSound:Z

    .line 151
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "hasSystemCheck"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasSystemCheck:Z

    .line 152
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "EDS"

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->useEDS31:Z

    .line 153
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "NowPlayingSimulator"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->nowPlayingSimulatorEnabled:Z

    .line 154
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "STAY_AWAKE"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->stayAwake:Z

    .line 155
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "INVERT_H_SWIPE"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->invertHSwipe:Z

    .line 156
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "INVERT_V_SWIPE"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->invertVSwipe:Z

    .line 157
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "LEFT_HANDED_REMOTED"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->leftHandedRemote:Z

    .line 158
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "lastestVersionChecked"

    const/4 v9, -0x1

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->lastCheckedVersion:I

    .line 159
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string/jumbo v8, "showWhatsNewDetailsVersionCode"

    const/4 v9, -0x1

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->lastWhatsNewDetailsVersion:I

    .line 164
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "DEFAULTSANDBOXID"

    const-string v9, "RETAIL"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->defaultSandboxId:Ljava/lang/String;

    .line 166
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "hasDonePostOOBE"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasDonePostOOBE:Z

    .line 167
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getAppConfig()Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v9, "CurrentSandboxID"

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->defaultSandboxId:Ljava/lang/String;

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;->setSandbox(Ljava/lang/String;)V

    .line 168
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "SMARTGLASSGUIDEENABLED"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->premiumTVEnabled:Z

    .line 169
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "SMARTGLASSURCENABLED"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->premiumUrcEnabled:Z

    .line 170
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "SMARTGLASSSHOWSPECIFICCONTENTRATINGS"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->showSpecificContentRatingsEnabled:Z

    .line 171
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "SMARTGLASSTREATMISSINGRATINGASADULT"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->treatMissingRatingAsAdult:Z

    .line 172
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "SMARTGLASSNONADULTCONTENTRATINGS"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->parseNonAdultContentRatings(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->nonAdultContentRatings:Ljava/util/Set;

    .line 173
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "MePreferredColor"

    sget v9, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->mePreferredColor:I

    .line 174
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "SMARTGLASSPOWERENABLED"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->powerEnabled:Z

    .line 175
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "SMARTGLASSNETWORKTESTINGENABLED"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->networkTestingEnabled:Z

    .line 176
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "HIDE_CLUB_DISCOVERY_BLOCK"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hideClubDiscoveryBlock:Z

    .line 178
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "GUID"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->appGuid:Ljava/lang/String;

    .line 179
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->appGuid:Ljava/lang/String;

    if-nez v7, :cond_0

    .line 180
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->createAndSaveGUID()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->appGuid:Ljava/lang/String;

    .line 184
    :cond_0
    :try_start_0
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v8, "build_version_number.txt"

    invoke-virtual {v7, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 185
    .local v3, "stream":Ljava/io/InputStream;
    if-eqz v3, :cond_1

    .line 186
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->buildNumber:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 193
    .end local v3    # "stream":Ljava/io/InputStream;
    :cond_1
    :goto_0
    :try_start_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const-string/jumbo v9, "xboxservices"

    const-string v10, "raw"

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 194
    .restart local v3    # "stream":Ljava/io/InputStream;
    if-eqz v3, :cond_2

    .line 195
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    .line 196
    .local v6, "xboxConfigSettings":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 197
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 198
    .local v0, "config":Lorg/json/JSONObject;
    const-string v7, "TitleId"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 200
    .local v4, "titleId":J
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-lez v7, :cond_2

    .line 201
    iput-wide v4, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->titleId:J

    .line 206
    .end local v0    # "config":Lorg/json/JSONObject;
    .end local v4    # "titleId":J
    .end local v6    # "xboxConfigSettings":Ljava/lang/String;
    :cond_2
    iget-wide v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->titleId:J

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-nez v7, :cond_3

    .line 207
    const-string v7, "ApplicationSettingManager"

    const-string v8, "Failed to load title ID, using default value"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-wide/32 v8, 0x138f999e

    iput-wide v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->titleId:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 216
    .end local v3    # "stream":Ljava/io/InputStream;
    :cond_3
    :goto_1
    :try_start_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "StayAwakeDefaultSetting"

    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->OnlyRemote:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->name()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->stayAwakeSetting:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 222
    :goto_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "PHONENUMBER"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->phoneNumber:Ljava/lang/String;

    .line 223
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "PHONECONTACTSUPLOADTIMESTAMP"

    const-wide/16 v10, 0x0

    invoke-interface {v7, v8, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->phoneContactsUploadTimestamp:J

    .line 224
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "MESSAGESDB_RESET"

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->isMessagingDBReset:Z

    .line 225
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "experimentsettings"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->experimentSettings:Ljava/lang/String;

    .line 226
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "MESSAGESDB_CONVERSATION_LIST_RESULT"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->conversationListResult:Ljava/lang/String;

    .line 227
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v8, "MESSAGESDB_SKYPE_CONVERSATION_MESSAGE_CACHE"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->skypeConversationMessageCache:Ljava/lang/String;

    .line 228
    return-void

    .line 210
    :catch_0
    move-exception v1

    .line 211
    .local v1, "e":Ljava/lang/Exception;
    const-string v7, "ApplicationSettingManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to load xboxservices.config: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-wide/32 v8, 0x138f999e

    iput-wide v8, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->titleId:J

    goto :goto_1

    .line 217
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 219
    .local v2, "ex":Ljava/lang/IllegalArgumentException;
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->OnlyRemote:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->stayAwakeSetting:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    goto :goto_2

    .line 188
    .end local v2    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v7

    goto/16 :goto_0
.end method

.method private parseNonAdultContentRatings(Ljava/lang/String;)Ljava/util/Set;
    .locals 10
    .param p1, "ratings"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 478
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 479
    .local v0, "nonAdultRatings":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 480
    const-string v5, ";"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 481
    .local v2, "pairs":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 482
    array-length v7, v2

    move v5, v6

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v1, v2, v5

    .line 483
    .local v1, "pair":Ljava/lang/String;
    const-string v8, ":"

    invoke-virtual {v1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 484
    .local v3, "rating":[Ljava/lang/String;
    array-length v8, v3

    const/4 v9, 0x2

    if-eq v8, v9, :cond_1

    .line 485
    const-string v5, "ApplicationSettingManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NonAdultContentRatings format is wrong: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    .end local v1    # "pair":Ljava/lang/String;
    .end local v2    # "pairs":[Ljava/lang/String;
    .end local v3    # "rating":[Ljava/lang/String;
    :cond_0
    return-object v0

    .line 489
    .restart local v1    # "pair":Ljava/lang/String;
    .restart local v2    # "pairs":[Ljava/lang/String;
    .restart local v3    # "rating":[Ljava/lang/String;
    :cond_1
    new-instance v4, Landroid/util/Pair;

    aget-object v8, v3, v6

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v3, v9

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 490
    .local v4, "ratingPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 482
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getBuildNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->buildNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getConnectedLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->connectedLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getConversationListResult()Ljava/lang/String;
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->conversationListResult:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentSandboxId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getAppConfig()Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;->getSandbox()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultSandboxId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->defaultSandboxId:Ljava/lang/String;

    return-object v0
.end method

.method public getExperimentSettings()Ljava/lang/String;
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->experimentSettings:Ljava/lang/String;

    return-object v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->appGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getHasTvShowtimeInfo()Z
    .locals 1

    .prologue
    .line 498
    const/4 v0, 0x0

    return v0
.end method

.method public getHelpCompanionWhiteListUrls()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->helpWhiteListUrls:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->helpWhiteListUrls:Ljava/lang/String;

    const-string v1, "\\|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHideClubDiscoveryBlock()Z
    .locals 1

    .prologue
    .line 825
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hideClubDiscoveryBlock:Z

    return v0
.end method

.method public getLastCheckedVersion()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->lastCheckedVersion:I

    return v0
.end method

.method public getLastWhatNewDetailsVersion()I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->lastWhatsNewDetailsVersion:I

    return v0
.end method

.method public getMePreferredColor()I
    .locals 1

    .prologue
    .line 591
    iget v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->mePreferredColor:I

    return v0
.end method

.method public getMessagingConversationsMetadata()Ljava/lang/String;
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->conversationsMetadata:Ljava/lang/String;

    return-object v0
.end method

.method public getMessagingDBResetState()Z
    .locals 1

    .prologue
    .line 759
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->isMessagingDBReset:Z

    return v0
.end method

.method public getNetworkTestingEnabled()Z
    .locals 1

    .prologue
    .line 607
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->networkTestingEnabled:Z

    return v0
.end method

.method public getNonAdultContentRatings()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 474
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->nonAdultContentRatings:Ljava/util/Set;

    return-object v0
.end method

.method public getNowPlayingSimulatorEnabled()Z
    .locals 1

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->nowPlayingSimulatorEnabled:Z

    return v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPinsTutorialIsCleared()Z
    .locals 3

    .prologue
    .line 579
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "PINS_TUTORIAL"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getPostOOBEStatus()Z
    .locals 1

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasDonePostOOBE:Z

    return v0
.end method

.method public getPowerEnabled()Z
    .locals 1

    .prologue
    .line 595
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->powerEnabled:Z

    return v0
.end method

.method public getPremiumLiveTVEnabled()Z
    .locals 1

    .prologue
    .line 427
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->premiumTVEnabled:Z

    return v0
.end method

.method public getPremiumUrcEnabled()Z
    .locals 1

    .prologue
    .line 435
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->premiumUrcEnabled:Z

    return v0
.end method

.method public getRateAppEnable()Z
    .locals 4

    .prologue
    .line 691
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 696
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "rateAppEnabled"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 697
    .local v0, "enable":Z
    return v0
.end method

.method public getRateAppLaunchCount()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 641
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 644
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "rateAppTrackLaunches"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 645
    .local v0, "count":I
    return v0
.end method

.method public getRateAppUri()Ljava/lang/String;
    .locals 4

    .prologue
    .line 718
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 721
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "rateAppUri"

    const-string v3, "market://details?id="

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 722
    .local v0, "uri":Ljava/lang/String;
    return-object v0
.end method

.method public getRateAppVersion()I
    .locals 4

    .prologue
    .line 666
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 669
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "rateAppLastVersion"

    const v3, 0xa32fa28

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 670
    .local v0, "count":I
    return v0
.end method

.method public getShowSpecificContentRatingsEnabled()Z
    .locals 1

    .prologue
    .line 450
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->showSpecificContentRatingsEnabled:Z

    return v0
.end method

.method public getSkypeConversationMessageCache()Ljava/lang/String;
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->skypeConversationMessageCache:Ljava/lang/String;

    return-object v0
.end method

.method public getSoundStatus()Z
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasSound:Z

    return v0
.end method

.method public getStayAwakeSetting()Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->stayAwakeSetting:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    return-object v0
.end method

.method public getSystemCheckStatus()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasSystemCheck:Z

    return v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 277
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->titleId:J

    return-wide v0
.end method

.method public getTreatMissingRatingAsAdult()Z
    .locals 1

    .prologue
    .line 462
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->treatMissingRatingAsAdult:Z

    return v0
.end method

.method public getUseEDS31()Z
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->useEDS31:Z

    return v0
.end method

.method public getUserXuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 502
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getXuid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isInvertHSwipe()Z
    .locals 1

    .prologue
    .line 532
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->invertHSwipe:Z

    return v0
.end method

.method public isInvertVSwipe()Z
    .locals 1

    .prologue
    .line 544
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->invertVSwipe:Z

    return v0
.end method

.method public isLeftHandedRemote()Z
    .locals 1

    .prologue
    .line 556
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->leftHandedRemote:Z

    return v0
.end method

.method public isStayAwake()Z
    .locals 1

    .prologue
    .line 520
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->stayAwake:Z

    return v0
.end method

.method public reset()V
    .locals 4

    .prologue
    .line 246
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 247
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 248
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 249
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 250
    return-void
.end method

.method public saveConnectedLocale(Ljava/lang/String;)V
    .locals 4
    .param p1, "locale"    # Ljava/lang/String;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->connectedLocale:Ljava/lang/String;

    .line 254
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 255
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 256
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "ConnectedLocale"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 257
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 258
    return-void
.end method

.method public saveConversationListResult(Ljava/lang/String;)V
    .locals 4
    .param p1, "conversationListResult"    # Ljava/lang/String;

    .prologue
    .line 805
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->conversationListResult:Ljava/lang/String;

    .line 806
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 807
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 808
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "MESSAGESDB_CONVERSATION_LIST_RESULT"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 809
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 810
    return-void
.end method

.method public saveExperimentSettings(Ljava/lang/String;)V
    .locals 4
    .param p1, "settingsData"    # Ljava/lang/String;

    .prologue
    .line 792
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->experimentSettings:Ljava/lang/String;

    .line 793
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 794
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 795
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "experimentsettings"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 796
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 797
    return-void
.end method

.method public saveHideClubDiscoveryBlock(Z)V
    .locals 4
    .param p1, "hideClubDiscoveryBlock"    # Z

    .prologue
    .line 829
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hideClubDiscoveryBlock:Z

    .line 830
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 831
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 832
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "HIDE_CLUB_DISCOVERY_BLOCK"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 833
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 834
    return-void
.end method

.method public saveMessagingConversationsMetadata(Ljava/lang/String;)V
    .locals 4
    .param p1, "metadata"    # Ljava/lang/String;

    .prologue
    .line 780
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->conversationsMetadata:Ljava/lang/String;

    .line 781
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 782
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 783
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "MESSAGESDB_CONVERSATIONS_METADATA"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 784
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 785
    return-void
.end method

.method public saveMessagingDBResetState(Z)V
    .locals 4
    .param p1, "isReset"    # Z

    .prologue
    .line 768
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->isMessagingDBReset:Z

    .line 769
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 770
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 771
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "MESSAGESDB_RESET"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 772
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 773
    return-void
.end method

.method public saveNowPlayingSimulatorEnabled(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 293
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->nowPlayingSimulatorEnabled:Z

    .line 294
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 295
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 296
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "NowPlayingSimulator"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 297
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 298
    return-void
.end method

.method public saveSkypeConversationMessageCache(Ljava/lang/String;)V
    .locals 4
    .param p1, "skypeConversationMessageCache"    # Ljava/lang/String;

    .prologue
    .line 817
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->skypeConversationMessageCache:Ljava/lang/String;

    .line 818
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 819
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 820
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "MESSAGESDB_SKYPE_CONVERSATION_MESSAGE_CACHE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 821
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 822
    return-void
.end method

.method public saveSoundStatus(Z)V
    .locals 4
    .param p1, "status"    # Z

    .prologue
    .line 265
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasSound:Z

    .line 266
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 267
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 268
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "hasSound"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 269
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 270
    return-void
.end method

.method public saveSystemCheckStatus(Z)V
    .locals 4
    .param p1, "isFirst"    # Z

    .prologue
    .line 305
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasSystemCheck:Z

    .line 306
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 307
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 308
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "hasSystemCheck"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 309
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 310
    return-void
.end method

.method public saveUseEDS31(Z)V
    .locals 4
    .param p1, "status"    # Z

    .prologue
    .line 281
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->useEDS31:Z

    .line 282
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 283
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 284
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "EDS"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 285
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 286
    return-void
.end method

.method public setCurrentSandboxId(Ljava/lang/String;)V
    .locals 4
    .param p1, "sandboxId"    # Ljava/lang/String;

    .prologue
    .line 396
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 397
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getAppConfig()Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/idp/interop/XboxLiveAppConfig;->setSandbox(Ljava/lang/String;)V

    .line 398
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 399
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 400
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "CurrentSandboxID"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 401
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 403
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setDefaultSandboxId(Ljava/lang/String;)V
    .locals 4
    .param p1, "sandboxId"    # Ljava/lang/String;

    .prologue
    .line 367
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 368
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->defaultSandboxId:Ljava/lang/String;

    .line 369
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 370
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 371
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "DEFAULTSANDBOXID"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 372
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 374
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setHelpCompanionWhiteListUrls(Ljava/lang/String;)V
    .locals 4
    .param p1, "whiteListUrls"    # Ljava/lang/String;

    .prologue
    .line 382
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 383
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->helpWhiteListUrls:Ljava/lang/String;

    .line 384
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 385
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 386
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "HELPCOMPANION_ALLOWEDURLS"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 387
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 389
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setInvertHSwipe(Z)V
    .locals 4
    .param p1, "invertHSwipe"    # Z

    .prologue
    .line 536
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->invertHSwipe:Z

    .line 537
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 538
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 539
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "INVERT_H_SWIPE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 540
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 541
    return-void
.end method

.method public setInvertVSwipe(Z)V
    .locals 4
    .param p1, "invertVSwipe"    # Z

    .prologue
    .line 548
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->invertVSwipe:Z

    .line 549
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 550
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 551
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "INVERT_V_SWIPE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 552
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 553
    return-void
.end method

.method public setLastCheckedVersion(I)V
    .locals 4
    .param p1, "versionCode"    # I

    .prologue
    .line 321
    iput p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->lastCheckedVersion:I

    .line 322
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 323
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 324
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "lastestVersionChecked"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 325
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 326
    return-void
.end method

.method public setLastWhatsNewDetailsVersion(I)V
    .locals 4
    .param p1, "versionCode"    # I

    .prologue
    .line 333
    iput p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->lastWhatsNewDetailsVersion:I

    .line 334
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 335
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 336
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v1, "showWhatsNewDetailsVersionCode"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 337
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 338
    return-void
.end method

.method public setLeftHandedRemote(Z)V
    .locals 4
    .param p1, "leftHandedRemote"    # Z

    .prologue
    .line 560
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->leftHandedRemote:Z

    .line 561
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 562
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 563
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "LEFT_HANDED_REMOTED"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 564
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 565
    return-void
.end method

.method public setMePreferredColor(I)V
    .locals 4
    .param p1, "preferredColor"    # I

    .prologue
    .line 583
    iput p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->mePreferredColor:I

    .line 584
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 585
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 586
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "MePreferredColor"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 587
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 588
    return-void
.end method

.method public setNetworkTestingEnabled(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 611
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->networkTestingEnabled:Z

    .line 612
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 613
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 614
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SMARTGLASSNETWORKTESTINGENABLED"

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->networkTestingEnabled:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 615
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 616
    return-void
.end method

.method public setNonAdultContentRatings(Ljava/lang/String;)V
    .locals 4
    .param p1, "ratings"    # Ljava/lang/String;

    .prologue
    .line 466
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->parseNonAdultContentRatings(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->nonAdultContentRatings:Ljava/util/Set;

    .line 467
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 468
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 469
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SMARTGLASSNONADULTCONTENTRATINGS"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 470
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 471
    return-void
.end method

.method public setPhoneContactsUploadTimestamp()V
    .locals 4

    .prologue
    .line 740
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->phoneContactsUploadTimestamp:J

    .line 741
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 742
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 743
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "PHONECONTACTSUPLOADTIMESTAMP"

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->phoneContactsUploadTimestamp:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 744
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 745
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 4
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 726
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 727
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->phoneNumber:Ljava/lang/String;

    .line 728
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 729
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 730
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "PHONENUMBER"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 731
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 733
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method public setPinsTutorialIsCleared(Z)V
    .locals 4
    .param p1, "isCleared"    # Z

    .prologue
    .line 572
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 573
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 574
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "PINS_TUTORIAL"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 575
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 576
    return-void
.end method

.method public setPostOOBEStatus(Z)V
    .locals 4
    .param p1, "status"    # Z

    .prologue
    .line 341
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->hasDonePostOOBE:Z

    .line 342
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 343
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 344
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "hasDonePostOOBE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 345
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 346
    return-void
.end method

.method public setPowerEnabled(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    const/4 v3, 0x0

    .line 599
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->powerEnabled:Z

    .line 600
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 601
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 602
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SMARTGLASSPOWERENABLED"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 603
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 604
    return-void
.end method

.method public setPremiumLiveTVEnabled(Z)V
    .locals 4
    .param p1, "liveTVEnabled"    # Z

    .prologue
    .line 410
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->premiumTVEnabled:Z

    .line 411
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 412
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 413
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SMARTGLASSGUIDEENABLED"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 414
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 415
    return-void
.end method

.method public setPremiumUrcEnabled(Z)V
    .locals 4
    .param p1, "urcEnabled"    # Z

    .prologue
    .line 418
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->premiumUrcEnabled:Z

    .line 419
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 420
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 421
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SMARTGLASSURCENABLED"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 422
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 423
    return-void
.end method

.method public setRateAppEnable(Z)V
    .locals 4
    .param p1, "newValue"    # Z

    .prologue
    .line 679
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 680
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 681
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "rateAppEnabled"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 682
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 683
    return-void
.end method

.method public setRateAppLaunchCount(I)V
    .locals 4
    .param p1, "newValue"    # I

    .prologue
    .line 624
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 625
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 627
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const/16 v1, 0xa

    if-le p1, v1, :cond_0

    .line 629
    const/16 p1, 0xb

    .line 631
    :cond_0
    const-string v1, "rateAppTrackLaunches"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 632
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 633
    return-void
.end method

.method public setRateAppUri(Ljava/lang/String;)V
    .locals 4
    .param p1, "newValue"    # Ljava/lang/String;

    .prologue
    .line 706
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 707
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 708
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "rateAppUri"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 709
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 710
    return-void
.end method

.method public setRateAppVersion(I)V
    .locals 4
    .param p1, "newValue"    # I

    .prologue
    .line 654
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 655
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 656
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "rateAppLastVersion"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 657
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 658
    return-void
.end method

.method public setShowSpecificContentRatingsEnabled(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 442
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->showSpecificContentRatingsEnabled:Z

    .line 443
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 444
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 445
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SMARTGLASSSHOWSPECIFICCONTENTRATINGS"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 446
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 447
    return-void
.end method

.method public setStayAwake(Z)V
    .locals 4
    .param p1, "stayAwake"    # Z

    .prologue
    .line 524
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->stayAwake:Z

    .line 525
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 526
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 527
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "STAY_AWAKE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 528
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 529
    return-void
.end method

.method public setStayAwakeSettings(Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;)V
    .locals 4
    .param p1, "setting"    # Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    .prologue
    .line 357
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->stayAwakeSetting:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    .line 359
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 360
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 361
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "StayAwakeDefaultSetting"

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 362
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 363
    return-void
.end method

.method public setTreatMissingRatingAsAdult(Z)V
    .locals 4
    .param p1, "asAdult"    # Z

    .prologue
    .line 454
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->treatMissingRatingAsAdult:Z

    .line 455
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->context:Landroid/content/Context;

    const-string/jumbo v2, "xlesetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    .line 456
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 457
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "SMARTGLASSTREATMISSINGRATINGASADULT"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 458
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 459
    return-void
.end method

.method public shoulddUploadPhoneContacts()Z
    .locals 6

    .prologue
    .line 749
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 750
    .local v0, "ct":J
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->phoneContactsUploadTimestamp:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
