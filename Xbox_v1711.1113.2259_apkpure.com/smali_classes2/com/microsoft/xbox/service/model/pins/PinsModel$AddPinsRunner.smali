.class Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "PinsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/pins/PinsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddPinsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final pinItems:[Lcom/microsoft/xbox/service/model/pins/PinItem;

.field private serverPins:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V
    .locals 0
    .param p2, "pinItems"    # [Lcom/microsoft/xbox/service/model/pins/PinItem;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 357
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->pinItems:[Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 358
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 367
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getESServiceManager()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v2

    .line 368
    .local v2, "manager":Lcom/microsoft/xbox/service/network/managers/IESServiceManager;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 369
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->pinItems:[Lcom/microsoft/xbox/service/model/pins/PinItem;

    array-length v7, v6

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v0, v6, v5

    .line 370
    .local v0, "item":Lcom/microsoft/xbox/service/model/pins/PinItem;
    const/4 v8, 0x1

    invoke-static {v0, v8}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getEPListPinItem(Lcom/microsoft/xbox/service/model/pins/PinItem;Z)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v3

    .line 371
    .local v3, "newItem":Lcom/microsoft/xbox/service/model/pins/PinItem;
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 375
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/pins/PinItem;
    .end local v3    # "newItem":Lcom/microsoft/xbox/service/model/pins/PinItem;
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-interface {v2, v5}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->add([Lcom/microsoft/xbox/service/model/pins/PinItem;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 376
    .local v4, "ret":Ljava/lang/Boolean;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 378
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$800(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->buildData()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->serverPins:Ljava/util/List;

    .line 380
    :cond_1
    return-object v4
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 396
    const-wide/16 v0, 0xfbb

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 385
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 386
    .local v0, "ret":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$800(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->serverPins:Ljava/util/List;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getSender()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v5

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v6

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 391
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->pinItems:[Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-static {v1, p1, v2}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$900(Lcom/microsoft/xbox/service/model/pins/PinsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;[Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    .line 392
    return-void
.end method

.method public onPreExecute()V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$AddPinsRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$800(Lcom/microsoft/xbox/service/model/pins/PinsModel;)Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel$GetPinsRunner;->onPreExecute()V

    .line 363
    return-void
.end method
