.class final Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubPlayScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LoadPartiesAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private partyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$1;

    .prologue
    .line 390
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 396
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 10

    .prologue
    .line 410
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 411
    .local v4, "result":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    const/4 v3, 0x0

    .line 414
    .local v3, "response":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponse;
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    iget-object v5, v5, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    const-string v6, "7492BACA-C1B4-440D-A391-B7EF364A8D40"

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    iget-object v7, v7, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 416
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v8

    .line 414
    invoke-interface {v5, v6, v8, v9}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->getMultiplayerSessions(Ljava/lang/String;J)Lio/reactivex/Single;

    move-result-object v5

    .line 416
    invoke-virtual {v5}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponse;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    :goto_0
    if-eqz v3, :cond_0

    .line 422
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->partyList:Ljava/util/List;

    .line 423
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 426
    :cond_0
    return-object v4

    .line 417
    :catch_0
    move-exception v2

    .line 418
    .local v2, "ex":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Could not load club parties"

    invoke-static {v5, v6, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 405
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 401
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Z)Z

    .line 438
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->partyList:Ljava/util/List;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/util/List;)V

    .line 439
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 390
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Z)Z

    .line 432
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$LoadPartiesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;Z)Z

    .line 433
    return-void
.end method
