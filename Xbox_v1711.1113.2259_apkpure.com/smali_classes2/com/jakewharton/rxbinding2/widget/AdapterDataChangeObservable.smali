.class final Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;
.super Lcom/jakewharton/rxbinding2/InitialValueObservable;
.source "AdapterDataChangeObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable$ObserverDisposable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/widget/Adapter;",
        ">",
        "Lcom/jakewharton/rxbinding2/InitialValueObservable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final adapter:Landroid/widget/Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/widget/Adapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;, "Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable<TT;>;"
    .local p1, "adapter":Landroid/widget/Adapter;, "TT;"
    invoke-direct {p0}, Lcom/jakewharton/rxbinding2/InitialValueObservable;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;->adapter:Landroid/widget/Adapter;

    .line 16
    return-void
.end method


# virtual methods
.method protected getInitialValue()Landroid/widget/Adapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;, "Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;->adapter:Landroid/widget/Adapter;

    return-object v0
.end method

.method protected bridge synthetic getInitialValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    .local p0, "this":Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;, "Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable<TT;>;"
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;->getInitialValue()Landroid/widget/Adapter;

    move-result-object v0

    return-object v0
.end method

.method protected subscribeListener(Lio/reactivex/Observer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;, "Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable<TT;>;"
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-TT;>;"
    invoke-static {p1}, Lcom/jakewharton/rxbinding2/internal/Preconditions;->checkMainThread(Lio/reactivex/Observer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 25
    :goto_0
    return-void

    .line 22
    :cond_0
    new-instance v0, Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable$ObserverDisposable;

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;->adapter:Landroid/widget/Adapter;

    invoke-direct {v0, v1, p1}, Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable$ObserverDisposable;-><init>(Landroid/widget/Adapter;Lio/reactivex/Observer;)V

    .line 23
    .local v0, "disposableDataSetObserver":Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable$ObserverDisposable;, "Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable$ObserverDisposable<TT;>;"
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable;->adapter:Landroid/widget/Adapter;

    invoke-static {v0}, Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable$ObserverDisposable;->access$000(Lcom/jakewharton/rxbinding2/widget/AdapterDataChangeObservable$ObserverDisposable;)Landroid/database/DataSetObserver;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 24
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    goto :goto_0
.end method
