.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView$ChatBubble;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChatBubble"
.end annotation


# static fields
.field public static final Club:Ljava/lang/String; = "ChatBubble - Club View"

.field public static final Clubs:Ljava/lang/String; = "ChatBubble - Clubs View"

.field public static final Conversation:Ljava/lang/String; = "ChatBubble - Conversation View"

.field public static final Conversations:Ljava/lang/String; = "ChatBubble - Conversations View"

.field public static final Notification:Ljava/lang/String; = "ChatBubble - Notification View"

.field public static final Summary:Ljava/lang/String; = "ChatBubble - Summary View"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
