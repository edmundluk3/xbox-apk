.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$1;
.super Ljava/lang/Object;
.source "UTCGameHub.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/functions/GenericFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic eval(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$1;->eval(Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public eval(Ljava/lang/String;)Ljava/lang/Void;
    .locals 4
    .param p1, "pageName"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 71
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TitleId"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->access$000()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->access$102(Ljava/lang/String;)Ljava/lang/String;

    .line 74
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->access$202(Ljava/lang/String;)Ljava/lang/String;

    .line 76
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->access$100()Ljava/lang/String;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 78
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->access$100()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 81
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method
