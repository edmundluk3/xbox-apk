.class public final Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TitlesArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Landroid/support/v4/util/Pair",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final layoutParams:Landroid/view/ViewGroup$MarginLayoutParams;

.field private final titleNavigationListener:Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "titleNavigationListener"    # Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v3, -0x2

    .line 32
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 33
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 34
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 36
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->titleNavigationListener:Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;

    .line 37
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 38
    .local v0, "imageSize":I
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->layoutParams:Landroid/view/ViewGroup$MarginLayoutParams;

    .line 39
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->layoutParams:Landroid/view/ViewGroup$MarginLayoutParams;

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 40
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->layoutParams:Landroid/view/ViewGroup$MarginLayoutParams;

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 41
    return-void
.end method

.method static synthetic lambda$getView$0(Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;ILandroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->titleNavigationListener:Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;->navigateToTitle(I)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v2, 0x7f0201fa

    .line 47
    instance-of v1, p2, Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    move-object v0, p2

    .line 48
    check-cast v0, Landroid/widget/ImageView;

    .line 58
    .local v0, "titleImage":Landroid/widget/ImageView;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/util/Pair;

    iget-object v1, v1, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 56
    invoke-static {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 62
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/util/Pair;

    iget-object v1, v1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->titleNavigationListener:Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;

    if-eqz v1, :cond_0

    .line 64
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    :cond_0
    return-object v0

    .line 50
    .end local v0    # "titleImage":Landroid/widget/ImageView;
    :cond_1
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 51
    .restart local v0    # "titleImage":Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter;->layoutParams:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 53
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0
.end method
