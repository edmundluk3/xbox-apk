.class public Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "BeamListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ChannelViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
        ">;"
    }
.end annotation


# instance fields
.field private final image:Landroid/widget/ImageView;

.field private final subtitle:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

.field private final title:Landroid/widget/TextView;

.field private final viewerCount:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->this$0:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    .line 66
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 68
    const v0, 0x7f0e0ae6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->image:Landroid/widget/ImageView;

    .line 69
    const v0, 0x7f0e0ae7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->title:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0e0ae8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->subtitle:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0e0ae9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->viewerCount:Landroid/widget/TextView;

    .line 72
    return-void
.end method

.method private getContentDescription(Lcom/microsoft/xbox/domain/beam/BeamChannel;)Ljava/lang/String;
    .locals 2
    .param p1, "dataObject"    # Lcom/microsoft/xbox/domain/beam/BeamChannel;

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->title()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 95
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->subTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 96
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->viewerCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->getViewerCountContentDescription(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    return-object v0
.end method

.method private getViewerCountContentDescription(I)Ljava/lang/String;
    .locals 5
    .param p1, "viewers"    # I

    .prologue
    const/4 v2, 0x1

    .line 100
    if-ne p1, v2, :cond_0

    .line 101
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v1, 0x7f070608

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/XLEApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const v1, 0x7f07060c

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;Lcom/microsoft/xbox/domain/beam/BeamChannel;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "dataObject"    # Lcom/microsoft/xbox/domain/beam/BeamChannel;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->this$0:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->access$000(Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->this$0:Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;->access$000(Lcom/microsoft/xbox/presentation/beam/BeamListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 82
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/domain/beam/BeamChannel;)V
    .locals 3
    .param p1, "dataObject"    # Lcom/microsoft/xbox/domain/beam/BeamChannel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v2, 0x7f0201fb

    .line 76
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;Lcom/microsoft/xbox/domain/beam/BeamChannel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->imageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->title()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->subtitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->subTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->viewerCount:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/beam/BeamChannel;->viewerCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->itemView:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->getContentDescription(Lcom/microsoft/xbox/domain/beam/BeamChannel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 91
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 58
    check-cast p1, Lcom/microsoft/xbox/domain/beam/BeamChannel;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/beam/BeamListAdapter$ChannelViewHolder;->onBind(Lcom/microsoft/xbox/domain/beam/BeamChannel;)V

    return-void
.end method
