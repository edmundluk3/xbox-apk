.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ListSetting"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 985
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1007
    new-instance v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ListSetting$Builder;-><init>()V

    return-object v0
.end method

.method public static isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;)Z
    .locals 1
    .param p0, "setting"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 997
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/Gson;",
            "Lcom/google/gson/reflect/TypeToken",
            "<+",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<TT;>;>;)",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 1001
    .local p1, "typeToken":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<+Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting<TT;>;>;"
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ListSetting$GsonTypeAdapter;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ListSetting$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)V

    return-object v0
.end method


# virtual methods
.method public abstract allowedValues()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract canViewerAct()Z
.end method

.method public abstract canViewerChangeSetting()Z
.end method

.method public abstract toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting$Builder",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract value()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;"
        }
    .end annotation
.end method
