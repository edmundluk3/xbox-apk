.class public Lcom/microsoft/xbox/xle/app/adapter/StorePivotScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "StorePivotScreenAdapter.java"


# instance fields
.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 16
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 17
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StorePivotScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;

    .line 18
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method
