.class public Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubInvitationsScreenAdapter.java"


# instance fields
.field private invitationsHashCode:I

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)V
    .locals 6
    .param p1, "clubInvitationsScreenViewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 31
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    .line 34
    const v3, 0x7f0e0a95

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 35
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 37
    const v3, 0x7f0e06bd

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 38
    .local v2, "noContent":Landroid/widget/TextView;
    const v3, 0x7f070b62

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 40
    const v3, 0x7f0e0385

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 41
    .local v1, "declineAllButton":Lcom/microsoft/xbox/xle/ui/IconFontButton;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    const v3, 0x7f0e0386

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 44
    .local v0, "clubInvitationsRecyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    new-instance v5, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;)V

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    .line 62
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreAllClicked()V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 67
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isBusy()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 68
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 70
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_1

    .line 71
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getInviteList()Ljava/util/List;

    move-result-object v0

    .line 72
    .local v0, "inviteList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;>;"
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v1

    .line 73
    .local v1, "newHash":I
    iget v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->invitationsHashCode:I

    if-eq v1, v2, :cond_0

    .line 74
    iput v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->invitationsHashCode:I

    .line 76
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->clear()V

    .line 78
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 79
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->addAll(Ljava/util/Collection;)V

    .line 83
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter;->notifyDataSetChanged()V

    .line 85
    .end local v0    # "inviteList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;>;"
    .end local v1    # "newHash":I
    :cond_1
    return-void
.end method
