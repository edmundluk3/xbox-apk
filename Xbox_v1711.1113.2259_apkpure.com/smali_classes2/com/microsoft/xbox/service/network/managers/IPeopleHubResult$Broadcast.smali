.class public Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;
.super Ljava/lang/Object;
.source "IPeopleHubResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Broadcast"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field public final id:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final provider:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final started:Ljava/util/Date;

.field public final text:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final titleId:J

.field public final viewers:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;JJ)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "provider"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "started"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p7, "viewers"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 252
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 253
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 254
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 255
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p5, p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 256
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p7, p8}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 258
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->id:Ljava/lang/String;

    .line 259
    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->provider:Ljava/lang/String;

    .line 260
    invoke-virtual {p3}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->started:Ljava/util/Date;

    .line 261
    iput-object p4, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->text:Ljava/lang/String;

    .line 262
    iput-wide p5, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->titleId:J

    .line 263
    iput-wide p7, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->viewers:J

    .line 264
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 273
    if-ne p1, p0, :cond_1

    .line 279
    :cond_0
    :goto_0
    return v1

    .line 275
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    if-nez v3, :cond_2

    move v1, v2

    .line 276
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 278
    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;

    .line 279
    .local v0, "other":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->provider:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->provider:Ljava/lang/String;

    .line 280
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->started:Ljava/util/Date;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->started:Ljava/util/Date;

    .line 281
    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->text:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->text:Ljava/lang/String;

    .line 282
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->titleId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->titleId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->viewers:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->viewers:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getStarted()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->started:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 290
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    if-nez v0, :cond_0

    .line 291
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    .line 292
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    .line 293
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->provider:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    .line 294
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->started:Ljava/util/Date;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    .line 295
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->text:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    .line 296
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    .line 297
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->viewers:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    .line 300
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 305
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
