.class public Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "TrendingTagSelectorScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# instance fields
.field private titleId:J


# direct methods
.method public constructor <init>(J)V
    .locals 3
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 16
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 17
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;->titleId:J

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Do not use this constructor"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0706fd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 27
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;->onCreateContentView()V

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;->titleId:J

    invoke-direct {v0, p0, v2, v3}, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;J)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 30
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f030228

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreen;->setContentView(I)V

    .line 40
    return-void
.end method
