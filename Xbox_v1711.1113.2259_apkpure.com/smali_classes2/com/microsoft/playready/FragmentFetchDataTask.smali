.class public Lcom/microsoft/playready/FragmentFetchDataTask;
.super Ljava/util/concurrent/FutureTask;
.source "FragmentFetchDataTask.java"

# interfaces
.implements Lcom/microsoft/playready/IFragmentFetchDataTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<[B>;",
        "Lcom/microsoft/playready/IFragmentFetchDataTask;"
    }
.end annotation


# instance fields
.field private mFragmentFetchDataListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/IFragmentFetchDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private mFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

.field private mListenersNotified:Z

.field private mMediaRepresentation:Lcom/microsoft/playready/MediaRepresentation;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Callable;Lcom/microsoft/playready/FragmentIterator$FragmentInfo;Lcom/microsoft/playready/MediaRepresentation;)V
    .locals 2
    .param p2, "info"    # Lcom/microsoft/playready/FragmentIterator$FragmentInfo;
    .param p3, "representation"    # Lcom/microsoft/playready/MediaRepresentation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<[B>;",
            "Lcom/microsoft/playready/FragmentIterator$FragmentInfo;",
            "Lcom/microsoft/playready/MediaRepresentation;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<[B>;"
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 12
    iput-object v1, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mFragmentFetchDataListeners:Ljava/util/ArrayList;

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mListenersNotified:Z

    .line 14
    iput-object v1, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    .line 15
    iput-object v1, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mMediaRepresentation:Lcom/microsoft/playready/MediaRepresentation;

    .line 20
    iput-object p2, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    .line 21
    iput-object p3, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mMediaRepresentation:Lcom/microsoft/playready/MediaRepresentation;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mFragmentFetchDataListeners:Ljava/util/ArrayList;

    .line 23
    return-void
.end method


# virtual methods
.method public addFragmentFetchDataListener(Lcom/microsoft/playready/IFragmentFetchDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/IFragmentFetchDataListener;

    .prologue
    .line 42
    monitor-enter p0

    .line 44
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mListenersNotified:Z

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mFragmentFetchDataListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    :goto_0
    monitor-exit p0

    .line 53
    return-void

    .line 50
    :cond_0
    invoke-interface {p1, p0}, Lcom/microsoft/playready/IFragmentFetchDataListener;->onFragmentFetchDataComplete(Lcom/microsoft/playready/IFragmentFetchDataTask;)V

    goto :goto_0

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected done()V
    .locals 3

    .prologue
    .line 28
    monitor-enter p0

    .line 30
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mFragmentFetchDataListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 35
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mListenersNotified:Z

    .line 28
    monitor-exit p0

    .line 37
    return-void

    .line 30
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/IFragmentFetchDataListener;

    .line 32
    .local v0, "listener":Lcom/microsoft/playready/IFragmentFetchDataListener;
    invoke-interface {v0, p0}, Lcom/microsoft/playready/IFragmentFetchDataListener;->onFragmentFetchDataComplete(Lcom/microsoft/playready/IFragmentFetchDataTask;)V

    goto :goto_0

    .line 28
    .end local v0    # "listener":Lcom/microsoft/playready/IFragmentFetchDataListener;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public fragmentData()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    invoke-super {p0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public fragmentInfo()Lcom/microsoft/playready/FragmentIterator$FragmentInfo;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mFragmentInfo:Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    return-object v0
.end method

.method public fragmentStreamRepresentation()Lcom/microsoft/playready/MediaRepresentation;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mMediaRepresentation:Lcom/microsoft/playready/MediaRepresentation;

    return-object v0
.end method

.method public removeFragmentFetchDataListener(Lcom/microsoft/playready/IFragmentFetchDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/IFragmentFetchDataListener;

    .prologue
    .line 58
    monitor-enter p0

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/FragmentFetchDataTask;->mFragmentFetchDataListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 58
    monitor-exit p0

    .line 62
    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
