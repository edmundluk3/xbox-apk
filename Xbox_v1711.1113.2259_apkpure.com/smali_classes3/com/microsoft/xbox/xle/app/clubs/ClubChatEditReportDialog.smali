.class public Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ClubChatEditReportDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final backButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final editButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

.field private final reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private final rootView:Landroid/widget/RelativeLayout;

.field private final topicAuthorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final topicEditErrorMessage:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final topicMessageText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final topicTimeStampView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 42
    const v0, 0x7f08021b

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 44
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->layoutInflater:Landroid/view/LayoutInflater;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->layoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03006b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02cd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02cf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->topicMessageText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02d0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->topicAuthorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02d1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->topicTimeStampView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02d2

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->topicEditErrorMessage:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02d4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02d3

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->editButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e02ce

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->backButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 54
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->updateDialogUI()V

    .line 57
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->setDialogButtons()V

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->setContentView(Landroid/view/View;)V

    .line 60
    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->dismissClubChatEditReportDialog()V

    .line 90
    return-void
.end method

.method static synthetic lambda$setDialogButtons$0(Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->dismissSelf()V

    .line 65
    return-void
.end method

.method static synthetic lambda$setDialogButtons$1(Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;Landroid/view/View;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 68
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissClubChatEditReportDialog()V

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getClubId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;ZLjava/lang/Long;)V

    .line 70
    return-void
.end method

.method static synthetic lambda$setDialogButtons$2(Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 73
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->TAG:Ljava/lang/String;

    const-string v1, "clicked post button"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->showClubChatTopicDialog()V

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->dismissClubChatEditReportDialog()V

    .line 76
    return-void
.end method

.method static synthetic lambda$setDialogButtons$3(Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->dismissSelf()V

    .line 80
    return-void
.end method

.method private setDialogButtons()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->editButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->backButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->dismissSelf()V

    .line 86
    return-void
.end method

.method public setViewModel(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .line 119
    return-void
.end method

.method public updateDialogUI()V
    .locals 11

    .prologue
    const/16 v8, 0x22

    const/4 v6, 0x1

    const/16 v4, 0x8

    const/4 v5, 0x0

    .line 93
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getMessageOfTheDay()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    .line 95
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    if-eqz v3, :cond_3

    .line 96
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    iget-object v7, v7, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "messageActual":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->topicMessageText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 98
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->topicTimeStampView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    iget-object v7, v7, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->timeStamp:Ljava/util/Date;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getDurationSinceNowUptoDayOrShortDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 99
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->topicAuthorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070304

    new-array v9, v6, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    iget-object v10, v10, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->gamerTag:Ljava/lang/String;

    aput-object v10, v9, v5

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 100
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->messageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;

    iget-object v3, v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->xuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    :goto_0
    invoke-static {v7, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 108
    .end local v1    # "messageActual":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->canViewerSetChatTopic()Z

    move-result v0

    .line 109
    .local v0, "canViewerSetChatTopic":Z
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->editButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    if-eqz v0, :cond_0

    move v4, v5

    :cond_0
    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 111
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getWhoCanSetChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->getErrorRoleStringId(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)I

    move-result v2

    .line 112
    .local v2, "roleStringId":I
    if-eqz v2, :cond_1

    .line 113
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->topicEditErrorMessage:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-nez v0, :cond_4

    move v3, v6

    :goto_2
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f0702f6

    new-array v6, v6, [Ljava/lang/Object;

    sget-object v9, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v9, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v5

    invoke-virtual {v7, v8, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTrue(Landroid/widget/TextView;ZLjava/lang/CharSequence;)V

    .line 115
    :cond_1
    return-void

    .end local v0    # "canViewerSetChatTopic":Z
    .end local v2    # "roleStringId":I
    .restart local v1    # "messageActual":Ljava/lang/String;
    :cond_2
    move v3, v5

    .line 100
    goto :goto_0

    .line 103
    .end local v1    # "messageActual":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->topicAuthorTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 104
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->reportButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_1

    .restart local v0    # "canViewerSetChatTopic":Z
    .restart local v2    # "roleStringId":I
    :cond_4
    move v3, v5

    .line 113
    goto :goto_2
.end method
