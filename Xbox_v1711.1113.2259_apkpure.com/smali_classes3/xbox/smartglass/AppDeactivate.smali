.class public Lxbox/smartglass/AppDeactivate;
.super LMicrosoft/Telemetry/Data;
.source "AppDeactivate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxbox/smartglass/AppDeactivate$Schema;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LMicrosoft/Telemetry/Data",
        "<",
        "Lxbox/smartglass/CommonData;",
        ">;"
    }
.end annotation


# instance fields
.field private companionConnectedTime:J

.field private companionDisconnectedTime:J

.field private connectedTime:J

.field private disconnectedTime:J

.field private totalTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 304
    invoke-direct {p0}, LMicrosoft/Telemetry/Data;-><init>()V

    .line 306
    return-void
.end method

.method public static getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 299
    sget-object v0, Lxbox/smartglass/AppDeactivate$Schema;->schemaDef:Lcom/microsoft/bond/SchemaDef;

    return-object v0
.end method


# virtual methods
.method public clone()Lcom/microsoft/bond/BondSerializable;
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lxbox/smartglass/AppDeactivate;->clone()Lcom/microsoft/bond/BondSerializable;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Lcom/microsoft/bond/StructDef;)Lcom/microsoft/bond/BondMirror;
    .locals 1
    .param p1, "structDef"    # Lcom/microsoft/bond/StructDef;

    .prologue
    .line 283
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCompanionConnectedTime()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    return-wide v0
.end method

.method public final getCompanionDisconnectedTime()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    return-wide v0
.end method

.method public final getConnectedTime()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    return-wide v0
.end method

.method public final getDisconnectedTime()J
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    return-wide v0
.end method

.method public getField(Lcom/microsoft/bond/FieldDef;)Ljava/lang/Object;
    .locals 2
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;

    .prologue
    .line 238
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 250
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 240
    :sswitch_0
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 242
    :sswitch_1
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 244
    :sswitch_2
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 246
    :sswitch_3
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 248
    :sswitch_4
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->totalTime:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 238
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public getSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 291
    invoke-static {}, Lxbox/smartglass/AppDeactivate;->getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v0

    return-object v0
.end method

.method public final getTotalTime()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lxbox/smartglass/AppDeactivate;->totalTime:J

    return-wide v0
.end method

.method public marshal(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 0
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 473
    invoke-static {p0, p1}, Lcom/microsoft/bond/internal/Marshaler;->marshal(Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/ProtocolWriter;)V

    .line 474
    return-void
.end method

.method public memberwiseCompare(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 524
    if-nez p1, :cond_1

    .line 530
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 528
    check-cast v0, Lxbox/smartglass/AppDeactivate;

    .line 530
    .local v0, "that":Lxbox/smartglass/AppDeactivate;
    invoke-virtual {p0, v0}, Lxbox/smartglass/AppDeactivate;->memberwiseCompareQuick(Lxbox/smartglass/AppDeactivate;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lxbox/smartglass/AppDeactivate;->memberwiseCompareDeep(Lxbox/smartglass/AppDeactivate;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected memberwiseCompareDeep(Lxbox/smartglass/AppDeactivate;)Z
    .locals 2
    .param p1, "that"    # Lxbox/smartglass/AppDeactivate;

    .prologue
    .line 545
    const/4 v0, 0x1

    .line 546
    .local v0, "equals":Z
    if-eqz v0, :cond_0

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareDeep(LMicrosoft/Telemetry/Data;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 547
    :goto_0
    return v0

    .line 546
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected memberwiseCompareQuick(Lxbox/smartglass/AppDeactivate;)Z
    .locals 8
    .param p1, "that"    # Lxbox/smartglass/AppDeactivate;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 534
    const/4 v0, 0x1

    .line 535
    .local v0, "equals":Z
    if-eqz v0, :cond_0

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareQuick(LMicrosoft/Telemetry/Data;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 536
    :goto_0
    if-eqz v0, :cond_1

    iget-wide v4, p0, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    iget-wide v6, p1, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    move v0, v1

    .line 537
    :goto_1
    if-eqz v0, :cond_2

    iget-wide v4, p0, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    iget-wide v6, p1, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    move v0, v1

    .line 538
    :goto_2
    if-eqz v0, :cond_3

    iget-wide v4, p0, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    iget-wide v6, p1, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    move v0, v1

    .line 539
    :goto_3
    if-eqz v0, :cond_4

    iget-wide v4, p0, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    iget-wide v6, p1, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_4

    move v0, v1

    .line 540
    :goto_4
    if-eqz v0, :cond_5

    iget-wide v4, p0, Lxbox/smartglass/AppDeactivate;->totalTime:J

    iget-wide v6, p1, Lxbox/smartglass/AppDeactivate;->totalTime:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_5

    move v0, v1

    .line 541
    :goto_5
    return v0

    :cond_0
    move v0, v2

    .line 535
    goto :goto_0

    :cond_1
    move v0, v2

    .line 536
    goto :goto_1

    :cond_2
    move v0, v2

    .line 537
    goto :goto_2

    :cond_3
    move v0, v2

    .line 538
    goto :goto_3

    :cond_4
    move v0, v2

    .line 539
    goto :goto_4

    :cond_5
    move v0, v2

    .line 540
    goto :goto_5
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 342
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readBegin()V

    .line 343
    invoke-virtual {p0, p1}, Lxbox/smartglass/AppDeactivate;->readNested(Lcom/microsoft/bond/ProtocolReader;)V

    .line 344
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readEnd()V

    .line 345
    return-void
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 363
    return-void
.end method

.method public readNested(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 351
    sget-object v0, Lcom/microsoft/bond/ProtocolCapability;->TAGGED:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v0}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 352
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/AppDeactivate;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/AppDeactivate;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    invoke-static {p1}, Lcom/microsoft/bond/internal/ReadHelper;->skipPartialStruct(Lcom/microsoft/bond/ProtocolReader;)V

    goto :goto_0
.end method

.method protected readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z
    .locals 6
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 416
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 418
    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 465
    :goto_0
    return v2

    .line 435
    .local v0, "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :sswitch_0
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt64(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)J

    move-result-wide v4

    iput-wide v4, p0, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    .line 459
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldEnd()V

    .line 425
    .end local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldBegin()Lcom/microsoft/bond/ProtocolReader$FieldTag;

    move-result-object v0

    .line 427
    .restart local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP:Lcom/microsoft/bond/BondDataType;

    if-eq v3, v4, :cond_1

    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_3

    .line 429
    :cond_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_2

    .line 462
    .local v1, "isPartial":Z
    :goto_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    move v2, v1

    .line 465
    goto :goto_0

    .end local v1    # "isPartial":Z
    :cond_2
    move v1, v2

    .line 429
    goto :goto_2

    .line 433
    :cond_3
    iget v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->id:I

    sparse-switch v3, :sswitch_data_0

    .line 455
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-virtual {p1, v3}, Lcom/microsoft/bond/ProtocolReader;->skip(Lcom/microsoft/bond/BondDataType;)V

    goto :goto_1

    .line 439
    :sswitch_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt64(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)J

    move-result-wide v4

    iput-wide v4, p0, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    goto :goto_1

    .line 443
    :sswitch_2
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt64(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)J

    move-result-wide v4

    iput-wide v4, p0, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    goto :goto_1

    .line 447
    :sswitch_3
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt64(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)J

    move-result-wide v4

    iput-wide v4, p0, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    goto :goto_1

    .line 451
    :sswitch_4
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt64(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)J

    move-result-wide v4

    iput-wide v4, p0, Lxbox/smartglass/AppDeactivate;->totalTime:J

    goto :goto_1

    .line 433
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method protected readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V
    .locals 4
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 366
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 368
    .local v0, "canOmitFields":Z
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 369
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 371
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 372
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    .line 379
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 380
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    .line 387
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_5

    .line 388
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    .line 395
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_7

    .line 396
    :cond_6
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    .line 403
    :cond_7
    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_9

    .line 404
    :cond_8
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lxbox/smartglass/AppDeactivate;->totalTime:J

    .line 410
    :cond_9
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    .line 411
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 312
    const-string v0, "AppDeactivate"

    const-string v1, "xbox.smartglass.AppDeactivate"

    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/AppDeactivate;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    return-void
.end method

.method protected reset(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "qualifiedName"    # Ljava/lang/String;

    .prologue
    const-wide/16 v0, 0x0

    .line 316
    invoke-super {p0, p1, p2}, LMicrosoft/Telemetry/Data;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    .line 318
    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    .line 319
    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    .line 320
    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    .line 321
    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->totalTime:J

    .line 322
    return-void
.end method

.method public final setCompanionConnectedTime(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 55
    iput-wide p1, p0, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    .line 56
    return-void
.end method

.method public final setCompanionDisconnectedTime(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 69
    iput-wide p1, p0, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    .line 70
    return-void
.end method

.method public final setConnectedTime(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 83
    iput-wide p1, p0, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    .line 84
    return-void
.end method

.method public final setDisconnectedTime(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 97
    iput-wide p1, p0, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    .line 98
    return-void
.end method

.method public setField(Lcom/microsoft/bond/FieldDef;Ljava/lang/Object;)V
    .locals 2
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 259
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 276
    .end local p2    # "value":Ljava/lang/Object;
    :goto_0
    return-void

    .line 261
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_0
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    goto :goto_0

    .line 264
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_1
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    goto :goto_0

    .line 267
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_2
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    goto :goto_0

    .line 270
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_3
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    goto :goto_0

    .line 273
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_4
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lxbox/smartglass/AppDeactivate;->totalTime:J

    goto :goto_0

    .line 259
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch
.end method

.method public final setTotalTime(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 111
    iput-wide p1, p0, Lxbox/smartglass/AppDeactivate;->totalTime:J

    .line 112
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 328
    invoke-static {p1, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V

    .line 329
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    check-cast p2, Lcom/microsoft/bond/SchemaDef;

    .end local p2    # "schema":Lcom/microsoft/bond/BondSerializable;
    invoke-static {p1, p2, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/BondSerializable;)V

    .line 336
    return-void
.end method

.method public write(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 2
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 480
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeBegin()V

    .line 482
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->getFirstPassWriter()Lcom/microsoft/bond/ProtocolWriter;

    move-result-object v0

    .local v0, "firstPassWriter":Lcom/microsoft/bond/ProtocolWriter;
    if-eqz v0, :cond_0

    .line 484
    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/AppDeactivate;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 485
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/AppDeactivate;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 491
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeEnd()V

    .line 492
    return-void

    .line 489
    :cond_0
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/AppDeactivate;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    goto :goto_0
.end method

.method public writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V
    .locals 4
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 495
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 496
    .local v0, "canOmitFields":Z
    sget-object v1, Lxbox/smartglass/AppDeactivate$Schema;->metadata:Lcom/microsoft/bond/Metadata;

    invoke-virtual {p1, v1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructBegin(Lcom/microsoft/bond/BondSerializable;Z)V

    .line 497
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 499
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT64:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0xa

    invoke-static {}, Lxbox/smartglass/AppDeactivate$Schema;->access$000()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 500
    iget-wide v2, p0, Lxbox/smartglass/AppDeactivate;->companionConnectedTime:J

    invoke-virtual {p1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt64(J)V

    .line 501
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 503
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT64:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x14

    invoke-static {}, Lxbox/smartglass/AppDeactivate$Schema;->access$100()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 504
    iget-wide v2, p0, Lxbox/smartglass/AppDeactivate;->companionDisconnectedTime:J

    invoke-virtual {p1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt64(J)V

    .line 505
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 507
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT64:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x1e

    invoke-static {}, Lxbox/smartglass/AppDeactivate$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 508
    iget-wide v2, p0, Lxbox/smartglass/AppDeactivate;->connectedTime:J

    invoke-virtual {p1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt64(J)V

    .line 509
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 511
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT64:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x28

    invoke-static {}, Lxbox/smartglass/AppDeactivate$Schema;->access$300()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 512
    iget-wide v2, p0, Lxbox/smartglass/AppDeactivate;->disconnectedTime:J

    invoke-virtual {p1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt64(J)V

    .line 513
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 515
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT64:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x32

    invoke-static {}, Lxbox/smartglass/AppDeactivate$Schema;->access$400()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 516
    iget-wide v2, p0, Lxbox/smartglass/AppDeactivate;->totalTime:J

    invoke-virtual {p1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt64(J)V

    .line 517
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 519
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructEnd(Z)V

    .line 520
    return-void
.end method
