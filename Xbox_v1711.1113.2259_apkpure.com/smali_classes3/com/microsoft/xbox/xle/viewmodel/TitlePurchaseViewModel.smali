.class public abstract Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "TitlePurchaseViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;",
        ">",
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isInstalling:Z

.field private model:Lcom/microsoft/xbox/service/model/TitleModel;

.field private pushInstallTask:Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel",
            "<TT;>.PushInstallTask;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->isInstalling:Z

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->onPushInstallCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private onPushInstallCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 7
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    const/4 v5, 0x0

    .line 231
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->isInstalling:Z

    .line 232
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070c36

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 235
    .local v2, "dialogBodyText":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070c37

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070c9e

    .line 237
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v6, v5

    .line 235
    invoke-interface/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 239
    return-void

    .line 232
    .end local v2    # "dialogBodyText":Ljava/lang/String;
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070b6d

    .line 233
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public getDisplayListPrice()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMsrp()Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayPrice()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getListPrice()Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 286
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public getHasInstallRights()Z
    .locals 2

    .prologue
    .line 96
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getStoreCollectionItem()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    move-result-object v0

    .line 98
    .local v0, "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    if-eqz v0, :cond_0

    .line 99
    const/4 v1, 0x1

    .line 103
    .end local v0    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIsAdult()Z
    .locals 1

    .prologue
    .line 91
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public getIsFree()Z
    .locals 2

    .prologue
    .line 75
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getListPrice()Ljava/math/BigDecimal;

    move-result-object v0

    .line 76
    .local v0, "price":Ljava/math/BigDecimal;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getAvailabilityId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIsGamepassTitle()Z
    .locals 4

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    const/4 v1, 0x0

    .line 137
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v2, :cond_0

    .line 138
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getStoreCollectionItem()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    move-result-object v0

    .line 139
    .local v0, "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->satisfiedByProductId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->satisfiedByProductId:Ljava/lang/String;

    .line 141
    invoke-static {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->getSubscriptionFromAffirmationProductId(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Gamepass:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 144
    .end local v0    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    :cond_0
    return v1
.end method

.method public getIsPlayable()Z
    .locals 1

    .prologue
    .line 81
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsBundle()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBundlePrimaryItemId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsPurchasable()Z
    .locals 1

    .prologue
    .line 86
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->getHasInstallRights()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->getIsGamepassTitle()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->getIsTrial()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsTrial()Z
    .locals 3

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    const/4 v2, 0x0

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getStoreCollectionItem()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    move-result-object v0

    .line 109
    .local v0, "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 112
    .end local v0    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    :cond_0
    return v2
.end method

.method public getPurchaseButtonLabelText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getReleaseDate()Ljava/util/Date;

    move-result-object v0

    .line 66
    .local v0, "releaseDate":Ljava/util/Date;
    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070b01

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 70
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070aee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getPurchaseDate()Ljava/lang/String;
    .locals 8

    .prologue
    .line 116
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v4, :cond_1

    .line 117
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getStoreCollectionItem()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    move-result-object v1

    .line 118
    .local v1, "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    if-eqz v1, :cond_1

    .line 120
    iget-object v4, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->acquiredDate:Ljava/util/Date;

    if-eqz v4, :cond_1

    .line 121
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iget-object v6, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->acquiredDate:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 122
    .local v2, "timeDiff":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    .line 123
    const/4 v4, 0x0

    .line 133
    .end local v1    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    .end local v2    # "timeDiff":J
    :goto_0
    return-object v4

    .line 126
    .restart local v1    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    .restart local v2    # "timeDiff":J
    :cond_0
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v4}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 127
    .local v0, "dateFormat":Ljava/text/DateFormat;
    iget-object v4, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->acquiredDate:Ljava/util/Date;

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 133
    .end local v0    # "dateFormat":Ljava/text/DateFormat;
    .end local v1    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    .end local v2    # "timeDiff":J
    :cond_1
    const-string v4, ""

    goto :goto_0
.end method

.method public installTitle()V
    .locals 14

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getStoreCollectionItem()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    move-result-object v13

    .line 190
    .local v13, "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    const-string v0, "Unexpected: store collection item is null but the install button was enabled"

    invoke-static {v0, v13}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->getIsPlayable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->isInstalling:Z

    if-nez v0, :cond_0

    if-eqz v13, :cond_0

    .line 193
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    .line 194
    .local v7, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->getHomeConsoleId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 195
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070c3e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070c3d

    .line 196
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 197
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v6, v5

    .line 195
    invoke-interface/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 228
    .end local v7    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    :goto_0
    return-void

    .line 200
    .restart local v7    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->isInstalling:Z

    .line 202
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v10

    .line 203
    .local v10, "originatingPage":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v8

    .line 204
    .local v8, "oldParams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSourcePage()Ljava/lang/String;

    move-result-object v12

    .line 205
    .local v12, "sourcePage":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v0

    if-nez v0, :cond_2

    .line 206
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    invoke-virtual {v8, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 210
    :cond_2
    new-instance v11, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v11}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 211
    .local v11, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setPurchaseTitle(Ljava/lang/String;)V

    .line 212
    invoke-virtual {v11, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseItemConsumable(Z)V

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseProductId(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getAvailabilityId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseAvailabilityId(Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getSkuId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseSkuId(Ljava/lang/String;)V

    .line 218
    invoke-virtual {v11, v10}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseOriginatingPage(Ljava/lang/String;)V

    .line 219
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v9

    .line 220
    .local v9, "oldparams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 221
    invoke-virtual {v11, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putCodeRedeemable(Z)V

    .line 222
    invoke-static {v11}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->trackInstallToXbox(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 224
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;

    iget-object v1, v13, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->productId:Ljava/lang/String;

    iget-object v2, v13, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->skuId:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->pushInstallTask:Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;

    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->pushInstallTask:Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->load(Z)V

    goto :goto_0
.end method

.method protected loadDetailsInBackGround(Z)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 8
    .param p1, "forceLoad"    # Z

    .prologue
    .line 248
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 250
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 251
    .local v0, "loadAppDetailStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefresh()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 252
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 255
    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 256
    sget-boolean v3, Lcom/microsoft/xbox/toolkit/Build;->IncludePurchaseFlow:Z

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v3

    if-nez v3, :cond_2

    .line 257
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    if-nez v3, :cond_1

    .line 258
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v3

    :goto_0
    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 260
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 261
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadStoreCollectionItem(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 268
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v3

    const/16 v4, 0x2329

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsBundle()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 269
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getBundlePrimaryItemDetailModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v1

    .line 270
    .local v1, "primaryItemModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    if-eqz v1, :cond_3

    .line 271
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 272
    .local v2, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 273
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->setBundlePrimaryItemTitleId(J)V

    .line 281
    .end local v1    # "primaryItemModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .end local v2    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_3
    :goto_2
    return-object v0

    .line 258
    :cond_4
    const/4 v3, 0x0

    goto :goto_0

    .line 263
    :cond_5
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid eds id - cannot get purchase information TitleId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " BigCatProductId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 264
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 263
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 275
    .restart local v1    # "primaryItemModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .restart local v2    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_6
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to load the primary item of bundle: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 291
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    return-void
.end method

.method protected onStartOverride()V
    .locals 4

    .prologue
    .line 50
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onStartOverride()V

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 52
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public purchaseTitle()V
    .locals 7

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    const/4 v6, 0x0

    .line 148
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->getIsPurchasable()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 149
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v2

    .line 150
    .local v2, "originatingPage":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 151
    .local v0, "oldParams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSourcePage()Ljava/lang/String;

    move-result-object v4

    .line 152
    .local v4, "sourcePage":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v5

    if-nez v5, :cond_0

    .line 153
    new-instance v5, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 157
    :cond_0
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 158
    .local v3, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setPurchaseTitle(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseItemConsumable(Z)V

    .line 160
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseProductId(Ljava/lang/String;)V

    .line 161
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getAvailabilityId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseAvailabilityId(Ljava/lang/String;)V

    .line 162
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getSkuId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseSkuId(Ljava/lang/String;)V

    .line 165
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseOriginatingPage(Ljava/lang/String;)V

    .line 166
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 167
    .local v1, "oldparams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 169
    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putCodeRedeemable(Z)V

    .line 171
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 178
    const-string v5, "Purchase - Game"

    invoke-static {v5, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->track(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 181
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->increment()Ljava/lang/String;

    .line 183
    const-class v5, Lcom/microsoft/xbox/xle/app/activity/PurchaseWebViewActivity;

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6, v3}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 186
    .end local v0    # "oldParams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v1    # "oldparams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v2    # "originatingPage":Ljava/lang/String;
    .end local v3    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v4    # "sourcePage":Ljava/lang/String;
    :cond_1
    return-void

    .line 174
    .restart local v0    # "oldParams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .restart local v1    # "oldparams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .restart local v2    # "originatingPage":Ljava/lang/String;
    .restart local v3    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .restart local v4    # "sourcePage":Ljava/lang/String;
    :pswitch_0
    const-string v5, "Purchase - App"

    invoke-static {v5, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->track(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x2328
        :pswitch_0
    .end packed-switch
.end method

.method protected shouldRefresh()Z
    .locals 1

    .prologue
    .line 243
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>;"
    const/4 v0, 0x1

    return v0
.end method
