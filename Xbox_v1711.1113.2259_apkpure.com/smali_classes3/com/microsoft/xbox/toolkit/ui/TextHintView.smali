.class public Lcom/microsoft/xbox/toolkit/ui/TextHintView;
.super Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
.source "TextHintView.java"


# static fields
.field private static final DEFAULT_COUNT_DOWN_INTERVAL:I = 0x3e8


# instance fields
.field private counter:Landroid/os/CountDownTimer;

.field private currentIndex:I

.field private hints:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->currentIndex:I

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->currentIndex:I

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/TextHintView;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TextHintView;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->hints:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/ui/TextHintView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TextHintView;

    .prologue
    .line 12
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->currentIndex:I

    return v0
.end method

.method static synthetic access$108(Lcom/microsoft/xbox/toolkit/ui/TextHintView;)I
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TextHintView;

    .prologue
    .line 12
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->currentIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->currentIndex:I

    return v0
.end method

.method private createCountDownTimer()V
    .locals 6

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->counter:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->counter:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->counter:Landroid/os/CountDownTimer;

    .line 84
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TextHintView$1;

    const-wide/32 v2, 0x7fffffff

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/ui/TextHintView$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/TextHintView;JJ)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->counter:Landroid/os/CountDownTimer;

    .line 96
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 31
    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->TextHintView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 32
    .local v1, "typeArray":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 33
    .local v0, "sourceId":I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 35
    if-eq v0, v5, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->hints:[Ljava/lang/String;

    .line 39
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->hints:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->hints:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    :goto_1
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 41
    return-void

    :cond_0
    move v2, v4

    .line 35
    goto :goto_0

    :cond_1
    move v3, v4

    .line 39
    goto :goto_1
.end method


# virtual methods
.method public dimissCountDownTimer()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->counter:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->counter:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 62
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->counter:Landroid/os/CountDownTimer;

    .line 63
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->onAttachedToWindow()V

    .line 69
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->hints:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->hints:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->currentIndex:I

    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->createCountDownTimer()V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->counter:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 76
    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 45
    if-nez p1, :cond_1

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->hints:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->hints:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->currentIndex:I

    .line 48
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->createCountDownTimer()V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->counter:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 55
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 56
    return-void

    .line 52
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TextHintView;->dimissCountDownTimer()V

    goto :goto_0
.end method
