.class public final enum Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;
.super Ljava/lang/Enum;
.source "UserSearchServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/userSearch/IUserSearchService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/userSearch/IUserSearchService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

.field private static final USER_SEARCH_FILE:Ljava/lang/String; = "stubdata/UserSearch.json"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    .line 16
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/userSearch/UserSearchServiceStub;

    return-object v0
.end method


# virtual methods
.method public clubSearch(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "clubId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 38
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 42
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/UserSearch.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    return-object v1

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public liveSearch(Ljava/lang/String;)Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 25
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 28
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/UserSearch.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :goto_0
    return-object v1

    .line 29
    :catch_0
    move-exception v0

    .line 30
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method
