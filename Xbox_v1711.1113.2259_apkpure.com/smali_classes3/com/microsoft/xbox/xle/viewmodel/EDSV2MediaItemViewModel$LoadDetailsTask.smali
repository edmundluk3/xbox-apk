.class public Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "EDSV2MediaItemViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadDetailsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;

    .prologue
    .line 249
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 253
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 279
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->loadDetailsInBackGround(Z)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 249
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 274
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 249
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 258
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 259
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 269
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 270
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 249
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 263
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel<TT;>.LoadDetailsTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->isLoadingDetail:Z

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel$LoadDetailsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->updateAdapter()V

    .line 265
    return-void
.end method
