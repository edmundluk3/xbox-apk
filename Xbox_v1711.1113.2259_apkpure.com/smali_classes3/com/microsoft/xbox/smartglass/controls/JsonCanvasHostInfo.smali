.class Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;
.super Lorg/json/JSONObject;
.source "JsonCanvasHostInfo.java"


# instance fields
.field public final majorVersion:I

.field public final minorVersion:I

.field public final requestKey:Ljava/util/UUID;

.field public final version:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/UUID;)V
    .locals 4
    .param p1, "key"    # Ljava/util/UUID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 18
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 13
    const-string v0, "2.0"

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;->version:Ljava/lang/String;

    .line 14
    iput v3, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;->majorVersion:I

    .line 15
    iput v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;->minorVersion:I

    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;->requestKey:Ljava/util/UUID;

    .line 21
    const-string v0, "version"

    const-string v1, "2.0"

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 22
    const-string v0, "majorVersion"

    invoke-virtual {p0, v0, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 23
    const-string v0, "minorVersion"

    invoke-virtual {p0, v0, v2}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 24
    const-string v0, "requestKey"

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;->requestKey:Ljava/util/UUID;

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 25
    return-void
.end method
