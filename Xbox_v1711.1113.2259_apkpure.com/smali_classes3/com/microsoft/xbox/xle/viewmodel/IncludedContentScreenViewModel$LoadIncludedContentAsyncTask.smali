.class Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "IncludedContentScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadIncludedContentAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;)V
    .locals 0

    .prologue
    .line 132
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$1;

    .prologue
    .line 132
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 136
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefreshIncludedContent()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 166
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->forceLoad:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 167
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsBundle()Z

    move-result v1

    if-nez v1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->setShouldHideScreen(Z)V

    .line 174
    .end local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_0
    :goto_0
    return-object v0

    .line 171
    .restart local v0    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->forceLoad:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadIncludedContent(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 159
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 142
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->onLoadIncludedContentCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 144
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 154
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->onLoadIncludedContentCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 155
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 132
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 148
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<TT;>.LoadIncludedContentAsyncTask;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel$LoadIncludedContentAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;Z)Z

    .line 150
    return-void
.end method
