.class public final enum Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;
.super Ljava/lang/Enum;
.source "PartyDetailsViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TogglePartyJoinabilityIntent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;",
        ">;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    .line 54
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    sget-object v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;->$VALUES:[Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    .line 57
    const-class v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    const-class v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;->$VALUES:[Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;->TAG:Ljava/lang/String;

    return-object v0
.end method
