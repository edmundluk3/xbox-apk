.class public Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "TvMyChannelsScreen.java"


# static fields
.field private static final MY_PIVOT_ID:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->FAVORITES:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;->MY_PIVOT_ID:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const-string v0, "OneGuide Favorites"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 33
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;->onCreateContentView()V

    .line 34
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyChannelsScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 35
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f030245

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;->setContentView(I)V

    .line 40
    return-void
.end method

.method public onSetActive()V
    .locals 4

    .prologue
    .line 56
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onSetActive()V

    .line 58
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;->getRootView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0e0b63

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;

    .line 59
    .local v0, "pivot":Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;->getRootView()Landroid/view/View;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;->MY_PIVOT_ID:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->ordinal()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils;->setupPivotArrowButtons(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;Landroid/view/View;I)V

    .line 61
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/TvMyChannelsScreen;->getRootView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0e0b61

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 62
    .local v1, "pivotHeaderTitle":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07046b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->FAVORITES:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->setLastScreen(Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;)V

    .line 66
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method
