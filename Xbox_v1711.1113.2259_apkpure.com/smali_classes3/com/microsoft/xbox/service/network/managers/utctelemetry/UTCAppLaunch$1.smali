.class final Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;
.super Ljava/lang/Object;
.source "UTCAppLaunch.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch;->track(Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$launchIntent:Landroid/content/Intent;

.field final synthetic val$launchType:Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchType:Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    .line 46
    const/4 v6, 0x0

    .line 48
    .local v6, "isDeepLink":Z
    const-string v7, "UTCLOGGING"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "App Launch "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchType:Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    new-instance v1, Lxbox/smartglass/AppLaunch;

    invoke-direct {v1}, Lxbox/smartglass/AppLaunch;-><init>()V

    .line 51
    .local v1, "appLaunchEvent":Lxbox/smartglass/AppLaunch;
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchType:Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$LaunchType;->getValue()I

    move-result v7

    invoke-virtual {v1, v7}, Lxbox/smartglass/AppLaunch;->setLaunchType(I)V

    .line 53
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchIntent:Landroid/content/Intent;

    if-eqz v7, :cond_3

    .line 54
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchIntent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "action":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    const-string v7, ".MAIN"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 58
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchIntent:Landroid/content/Intent;

    const-string v9, "deepLinkId"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 59
    .local v4, "deepLinkId":Ljava/lang/String;
    if-nez v4, :cond_0

    const-string v4, ""

    .line 61
    :cond_0
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchIntent:Landroid/content/Intent;

    const-string v9, "deepLinkCaller"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 62
    .local v2, "deepLinkCaller":Ljava/lang/String;
    if-nez v2, :cond_1

    const-string v2, ""

    .line 64
    :cond_1
    const-string v5, ""

    .line 65
    .local v5, "deepLinkName":Ljava/lang/String;
    const-string v3, ""

    .line 66
    .local v3, "deepLinkContext":Ljava/lang/String;
    const/4 v7, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_0
    packed-switch v7, :pswitch_data_0

    .line 94
    move-object v5, v0

    .line 98
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 101
    const/4 v6, 0x1

    .line 103
    const-string v7, "deepLinkId"

    invoke-static {v7, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addPersistentValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 104
    const-string v7, "deepLinkName"

    invoke-static {v7, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addPersistentValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 105
    const-string v7, "deepLinkContext"

    invoke-static {v7, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addPersistentValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 106
    const-string v7, "deepLinkCaller"

    invoke-static {v7, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addPersistentValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 111
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "deepLinkCaller":Ljava/lang/String;
    .end local v3    # "deepLinkContext":Ljava/lang/String;
    .end local v4    # "deepLinkId":Ljava/lang/String;
    .end local v5    # "deepLinkName":Ljava/lang/String;
    :cond_3
    invoke-static {v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(I)Lxbox/smartglass/CommonData;

    move-result-object v7

    invoke-virtual {v1, v7}, Lxbox/smartglass/AppLaunch;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 113
    const-string v8, "UTCLOGGING"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "App Launch - Type: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lxbox/smartglass/AppLaunch;->getLaunchType()I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " AdditionalData: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Lxbox/smartglass/AppLaunch;->getBaseData()Lcom/microsoft/bond/BondSerializable;

    move-result-object v7

    check-cast v7, Lxbox/smartglass/CommonData;

    invoke-virtual {v7}, Lxbox/smartglass/CommonData;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 117
    if-eqz v6, :cond_4

    .line 118
    const-string v7, "Deeplink Launch"

    invoke-static {v7}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 120
    :cond_4
    return-void

    .line 66
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v2    # "deepLinkCaller":Ljava/lang/String;
    .restart local v3    # "deepLinkContext":Ljava/lang/String;
    .restart local v4    # "deepLinkId":Ljava/lang/String;
    .restart local v5    # "deepLinkName":Ljava/lang/String;
    :sswitch_0
    const-string v9, "com.microsoft.xbox.action.ACTION_VIEW_GAME_PROFILE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x0

    goto :goto_0

    :sswitch_1
    const-string v9, "com.microsoft.xbox.action.ACTION_VIEW_ACHIEVEMENTS"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v7, v8

    goto :goto_0

    :sswitch_2
    const-string v9, "com.microsoft.xbox.action.ACTION_VIEW_USER_PROFILE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x2

    goto :goto_0

    :sswitch_3
    const-string v9, "com.microsoft.xbox.action.ACTION_VIEW_SETTINGS"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x3

    goto/16 :goto_0

    :sswitch_4
    const-string v9, "com.microsoft.xbox.action.ACTION_FIND_PEOPLE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x4

    goto/16 :goto_0

    :sswitch_5
    const-string v9, "com.microsoft.xbox.action.ACTION_SIGNIN"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x5

    goto/16 :goto_0

    :sswitch_6
    const-string v9, "com.microsoft.xbox.action.ACTION_VIEW_PURCHASE_PAGE"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v7, 0x6

    goto/16 :goto_0

    .line 68
    :pswitch_0
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchIntent:Landroid/content/Intent;

    const-string v9, "com.microsoft.xbox.extra.TITLEID"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 69
    const-string v5, "DeepLink - GameHub"

    .line 70
    goto/16 :goto_1

    .line 72
    :pswitch_1
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchIntent:Landroid/content/Intent;

    const-string v9, "com.microsoft.xbox.extra.TITLEID"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 73
    const-string v5, "DeepLink - GameHub Achievements"

    .line 74
    goto/16 :goto_1

    .line 76
    :pswitch_2
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchIntent:Landroid/content/Intent;

    const-string v9, "com.microsoft.xbox.extra.XUID"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 77
    const-string v5, "DeepLink - User Profile"

    .line 78
    goto/16 :goto_1

    .line 80
    :pswitch_3
    const-string v5, "DeepLink - User Settings"

    .line 81
    goto/16 :goto_1

    .line 83
    :pswitch_4
    const-string v5, "DeepLink - Friend Suggestions"

    .line 84
    goto/16 :goto_1

    .line 86
    :pswitch_5
    const-string v5, "DeepLink - SignIn"

    .line 87
    goto/16 :goto_1

    .line 89
    :pswitch_6
    const-string v5, "DeepLink - Purchase"

    .line 90
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCAppLaunch$1;->val$launchIntent:Landroid/content/Intent;

    const-string v9, "com.microsoft.xbox.extra.BIGID"

    invoke-virtual {v7, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 91
    goto/16 :goto_1

    .line 66
    nop

    :sswitch_data_0
    .sparse-switch
        -0x74971f30 -> :sswitch_2
        -0x3d90d609 -> :sswitch_0
        -0x1c138521 -> :sswitch_1
        -0x693fa1f -> :sswitch_5
        0x14dbf0f6 -> :sswitch_4
        0x44b852b2 -> :sswitch_6
        0x5b9ffd5e -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
