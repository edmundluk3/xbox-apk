.class public final enum Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
.super Ljava/lang/Enum;
.source "EDSV3MediaType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum Album:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum AppActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum AvatarItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum DActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum DApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum DConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum DDurable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum DGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum DGameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum DNativeApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum DVideoActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum GameActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum MetroGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum MetroGameConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum MetroGameContent:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum MobileGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum Movie:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum MusicArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum MusicVideo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum TVEpisode:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum TVSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum TVSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum TVShow:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum Track:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum UnInitialize:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum VideoActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum VideoLayer:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum WebGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum WebVideo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum WebVideoCollection:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum Xbox360Game:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum Xbox360GameContent:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum Xbox360GameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxArcadeGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxBundle:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxGameConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxGameTrailer:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxGameTrial:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxGameVideo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxGamerTile:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxMarketplace:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxMobileConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxMobilePDLC:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxOriginalGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxTheme:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field public static final enum XboxXnaCommunityGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

.field private static final intToTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;",
            ">;"
        }
    .end annotation
.end field

.field private static final stringToTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final strVal:Ljava/lang/String;

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0x13

    const/16 v8, 0x12

    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 10
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "Xbox360Game"

    invoke-direct {v2, v3, v1, v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360Game:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 11
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxGameTrial"

    invoke-direct {v2, v3, v6, v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGameTrial:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 12
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "Xbox360GameContent"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4, v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360GameContent:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 13
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "Xbox360GameDemo"

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4, v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360GameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxTheme"

    const/4 v4, 0x4

    const/16 v5, 0x14

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxTheme:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 15
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxOriginalGame"

    const/16 v4, 0x15

    invoke-direct {v2, v3, v7, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxOriginalGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxGamerTile"

    const/4 v4, 0x6

    const/16 v5, 0x16

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGamerTile:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 17
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxArcadeGame"

    const/4 v4, 0x7

    const/16 v5, 0x17

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxArcadeGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxGameConsumable"

    const/16 v4, 0x8

    const/16 v5, 0x18

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGameConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 19
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxGameVideo"

    const/16 v4, 0x9

    const/16 v5, 0x1e

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGameVideo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxGameTrailer"

    const/16 v4, 0xa

    const/16 v5, 0x22

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGameTrailer:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 21
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxBundle"

    const/16 v4, 0xb

    const/16 v5, 0x24

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxBundle:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxXnaCommunityGame"

    const/16 v4, 0xc

    const/16 v5, 0x25

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxXnaCommunityGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 23
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxMarketplace"

    const/16 v4, 0xd

    const/16 v5, 0x2e

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxMarketplace:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 24
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "AvatarItem"

    const/16 v4, 0xe

    const/16 v5, 0x2f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->AvatarItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 25
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "WebGame"

    const/16 v4, 0xf

    const/16 v5, 0x39

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->WebGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "MobileGame"

    const/16 v4, 0x10

    const/16 v5, 0x3a

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MobileGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 27
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxMobilePDLC"

    const/16 v4, 0x11

    const/16 v5, 0x3b

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxMobilePDLC:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxMobileConsumable"

    const/16 v4, 0x3c

    invoke-direct {v2, v3, v8, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxMobileConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 29
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "XboxApp"

    const/16 v4, 0x3d

    invoke-direct {v2, v3, v9, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "MetroGame"

    const/16 v4, 0x14

    const/16 v5, 0x3e

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MetroGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 31
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "MetroGameContent"

    const/16 v4, 0x15

    const/16 v5, 0x3f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MetroGameContent:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "MetroGameConsumable"

    const/16 v4, 0x16

    const/16 v5, 0x40

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MetroGameConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 33
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "GameActivity"

    const/16 v4, 0x17

    const/16 v5, 0x42

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->GameActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "AppActivity"

    const/16 v4, 0x18

    const/16 v5, 0x43

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->AppActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 35
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "VideoLayer"

    const/16 v4, 0x19

    const/16 v5, 0x385

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->VideoLayer:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 36
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "VideoActivity"

    const/16 v4, 0x1a

    const/16 v5, 0x385

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->VideoActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 37
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "Movie"

    const/16 v4, 0x1b

    const/16 v5, 0x3e8

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Movie:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 38
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "TVShow"

    const/16 v4, 0x1c

    const/16 v5, 0x3ea

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->TVShow:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 39
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "TVEpisode"

    const/16 v4, 0x1d

    const/16 v5, 0x3eb

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->TVEpisode:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 40
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "TVSeries"

    const/16 v4, 0x1e

    const/16 v5, 0x3ec

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->TVSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 41
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "TVSeason"

    const/16 v4, 0x1f

    const/16 v5, 0x3ed

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->TVSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 42
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "Album"

    const/16 v4, 0x20

    const/16 v5, 0x3ee

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Album:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 43
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "Track"

    const/16 v4, 0x21

    const/16 v5, 0x3ef

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Track:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 44
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "MusicVideo"

    const/16 v4, 0x22

    const/16 v5, 0x3f0

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MusicVideo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 45
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "MusicArtist"

    const/16 v4, 0x23

    const/16 v5, 0x3f1

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MusicArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 46
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "WebVideo"

    const/16 v4, 0x24

    const/16 v5, 0x3f2

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->WebVideo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 47
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "WebVideoCollection"

    const/16 v4, 0x25

    const/16 v5, 0x3f3

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->WebVideoCollection:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 48
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "DGame"

    const/16 v4, 0x26

    const/16 v5, 0x2329

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 49
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "DConsumable"

    const/16 v4, 0x27

    const/16 v5, 0x232b

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 50
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "DGameDemo"

    const/16 v4, 0x28

    const/16 v5, 0x232a

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 51
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "DDurable"

    const/16 v4, 0x29

    const/16 v5, 0x232c

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DDurable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 52
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "DApp"

    const/16 v4, 0x2a

    const/16 v5, 0x2328

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 53
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "DActivity"

    const/16 v4, 0x2b

    const/16 v5, 0x232d

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 54
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "DNativeApp"

    const/16 v4, 0x2c

    const/16 v5, 0x232e

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DNativeApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 55
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "DVideoActivity"

    const/16 v4, 0x2d

    const/16 v5, 0x232f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DVideoActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 56
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "Unknown"

    const/16 v4, 0x2e

    invoke-direct {v2, v3, v4, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Unknown:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 57
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    const-string v3, "UnInitialize"

    const/16 v4, 0x2f

    const/4 v5, -0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->UnInitialize:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 9
    const/16 v2, 0x30

    new-array v2, v2, [Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360Game:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGameTrial:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v3, v2, v6

    const/4 v3, 0x2

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360GameContent:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360GameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxTheme:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxOriginalGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v3, v2, v7

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGamerTile:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxArcadeGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGameConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGameVideo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxGameTrailer:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxBundle:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxXnaCommunityGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxMarketplace:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->AvatarItem:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->WebGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MobileGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x11

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxMobilePDLC:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxMobileConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->XboxApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v3, v2, v9

    const/16 v3, 0x14

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MetroGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x15

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MetroGameContent:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x16

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MetroGameConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x17

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->GameActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x18

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->AppActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x19

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->VideoLayer:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->VideoActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Movie:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->TVShow:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->TVEpisode:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->TVSeries:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->TVSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x20

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Album:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x21

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Track:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x22

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MusicVideo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x23

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->MusicArtist:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x24

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->WebVideo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x25

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->WebVideoCollection:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x26

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x27

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x28

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGameDemo:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x29

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DDurable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DNativeApp:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DVideoActivity:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Unknown:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    const/16 v3, 0x2f

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->UnInitialize:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->$VALUES:[Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 61
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->intToTypeMap:Ljava/util/Map;

    .line 62
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->stringToTypeMap:Ljava/util/Map;

    .line 65
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->values()[Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 66
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->intToTypeMap:Ljava/util/Map;

    iget v5, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->value:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->stringToTypeMap:Ljava/util/Map;

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->strVal:Ljava/lang/String;

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput p3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->value:I

    .line 73
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->strVal:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    .locals 3
    .param p0, "i"    # I

    .prologue
    .line 81
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->intToTypeMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 82
    .local v0, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    if-nez v0, :cond_0

    .line 83
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Unknown:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 86
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :cond_0
    return-object v0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 90
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 91
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->stringToTypeMap:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 92
    .local v0, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    if-nez v0, :cond_0

    .line 93
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Unknown:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    .line 99
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Unknown:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->$VALUES:[Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->value:I

    return v0
.end method
