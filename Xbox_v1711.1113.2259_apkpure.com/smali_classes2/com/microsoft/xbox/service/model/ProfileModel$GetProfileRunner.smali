.class Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetProfileRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/ProfileData;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private loadEssentialsOnly:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Z)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "loadEssentialsOnly"    # Z

    .prologue
    .line 2219
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2220
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 2221
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->xuid:Ljava/lang/String;

    .line 2222
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->loadEssentialsOnly:Z

    .line 2223
    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    .prologue
    .line 2213
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->xuid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic lambda$buildData$0(Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;)V
    .locals 9
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;

    .prologue
    const/4 v5, 0x1

    .line 2289
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v6, v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPeopleHubPersonData(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    .line 2290
    .local v3, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2291
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$700(Lcom/microsoft/xbox/service/model/ProfileModel;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    move-result-object v0

    .line 2292
    .local v0, "meSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v0, :cond_4

    iget-object v6, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->socialManager:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubSocialManagerInfo;

    if-eqz v6, :cond_4

    .line 2293
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$800(Lcom/microsoft/xbox/service/model/ProfileModel;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 2294
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$900(Lcom/microsoft/xbox/service/model/ProfileModel;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 2295
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->socialManager:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubSocialManagerInfo;

    .line 2297
    .local v2, "socialManagerInfo":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubSocialManagerInfo;
    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubSocialManagerInfo;->titleIds:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2298
    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubSocialManagerInfo;->titleIds:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2299
    .local v4, "titleId":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$800(Lcom/microsoft/xbox/service/model/ProfileModel;)Ljava/util/List;

    move-result-object v7

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2303
    .end local v4    # "titleId":Ljava/lang/String;
    :cond_0
    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubSocialManagerInfo;->pages:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2304
    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubSocialManagerInfo;->pages:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPageInfo;

    .line 2305
    .local v1, "pageInfo":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPageInfo;
    iget-object v7, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPageInfo;->id:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 2306
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$900(Lcom/microsoft/xbox/service/model/ProfileModel;)Ljava/util/List;

    move-result-object v7

    iget-object v8, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPageInfo;->id:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2311
    .end local v1    # "pageInfo":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPageInfo;
    :cond_2
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$800(Lcom/microsoft/xbox/service/model/ProfileModel;)Ljava/util/List;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$900(Lcom/microsoft/xbox/service/model/ProfileModel;)Ljava/util/List;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v7

    if-nez v7, :cond_5

    :cond_3
    :goto_2
    invoke-static {v6, v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$1002(Lcom/microsoft/xbox/service/model/ProfileModel;Z)Z

    .line 2314
    .end local v0    # "meSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v2    # "socialManagerInfo":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubSocialManagerInfo;
    :cond_4
    return-void

    .line 2311
    .restart local v0    # "meSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .restart local v2    # "socialManagerInfo":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubSocialManagerInfo;
    :cond_5
    const/4 v5, 0x0

    goto :goto_2
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/ProfileData;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 2228
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v5

    .line 2230
    .local v5, "serviceManager":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2231
    .local v10, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v11, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->xuid:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2232
    new-instance v2, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;

    iget-boolean v11, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->loadEssentialsOnly:Z

    invoke-direct {v2, v10, v11}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;-><init>(Ljava/util/ArrayList;Z)V

    .line 2233
    .local v2, "profileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->getUserProfileRequestBody(Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v5, v11}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    move-result-object v4

    .line 2235
    .local v4, "response":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->xuid:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2236
    if-eqz v4, :cond_1

    iget-object v11, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    if-eqz v11, :cond_1

    iget-object v11, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_1

    .line 2237
    iget-object v11, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 2240
    .local v3, "profileUser":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :try_start_0
    sget-object v11, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->PreferredColor:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v3, v11}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->getSettingValue(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v9

    .line 2241
    .local v9, "url":Ljava/lang/String;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_0

    .line 2242
    iget-object v11, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/ProfileModel;->profileColorsRepository:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;

    invoke-virtual {v11, v9}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->getProfileColorForUrl(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v11

    invoke-virtual {v11}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    iput-object v11, v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 2243
    iget-object v11, v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-eqz v11, :cond_0

    iget-object v11, v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v11

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMePreferredColor()I

    move-result v12

    if-eq v11, v12, :cond_0

    .line 2244
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v11

    iget-object v12, v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setMePreferredColor(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 2252
    .end local v9    # "url":Ljava/lang/String;
    :cond_0
    :goto_0
    sget-object v11, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v12, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;

    invoke-direct {v12, p0, v5, v3}, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$1;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;)V

    invoke-interface {v11, v12}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 2288
    sget-object v11, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;)Ljava/lang/Runnable;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 2334
    .end local v3    # "profileUser":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :cond_1
    :goto_1
    const/4 v6, 0x0

    .line 2335
    .local v6, "shareRealName":Z
    const/4 v7, 0x0

    .line 2336
    .local v7, "shareRealNameStatus":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    const/4 v8, 0x0

    .line 2337
    .local v8, "sharingRealNameTransitively":Z
    const/4 v1, 0x0

    .line 2339
    .local v1, "privacyResult":Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    iget-object v11, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->xuid:Ljava/lang/String;

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->xuid:Ljava/lang/String;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v11

    if-nez v11, :cond_2

    .line 2341
    :try_start_1
    invoke-interface {v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfilePrivacySettings()Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 2347
    :cond_2
    :goto_2
    new-instance v11, Lcom/microsoft/xbox/service/model/ProfileData;

    invoke-direct {v11, v4, v1}, Lcom/microsoft/xbox/service/model/ProfileData;-><init>(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)V

    return-object v11

    .line 2318
    .end local v1    # "privacyResult":Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    .end local v6    # "shareRealName":Z
    .end local v7    # "shareRealNameStatus":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .end local v8    # "sharingRealNameTransitively":Z
    :cond_3
    if-eqz v4, :cond_1

    iget-object v11, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    if-eqz v11, :cond_1

    iget-object v11, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_1

    .line 2319
    iget-object v11, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 2322
    .restart local v3    # "profileUser":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :try_start_2
    sget-object v11, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->PreferredColor:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v3, v11}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->getSettingValue(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v9

    .line 2323
    .restart local v9    # "url":Ljava/lang/String;
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_1

    .line 2324
    iget-object v11, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/ProfileModel;->profileColorsRepository:Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;

    invoke-virtual {v11, v9}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->getProfileColorForUrl(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v11

    invoke-virtual {v11}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    iput-object v11, v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->colors:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 2326
    .end local v9    # "url":Ljava/lang/String;
    :catch_0
    move-exception v11

    goto :goto_1

    .line 2342
    .end local v3    # "profileUser":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    .restart local v1    # "privacyResult":Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    .restart local v6    # "shareRealName":Z
    .restart local v7    # "shareRealNameStatus":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .restart local v8    # "sharingRealNameTransitively":Z
    :catch_1
    move-exception v0

    .line 2343
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v11, Lcom/microsoft/xbox/service/model/ProfileModel;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "failed to get real name settings: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2247
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "privacyResult":Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    .end local v6    # "shareRealName":Z
    .end local v7    # "shareRealNameStatus":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .end local v8    # "sharingRealNameTransitively":Z
    .restart local v3    # "profileUser":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :catch_2
    move-exception v11

    goto/16 :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2213
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->buildData()Lcom/microsoft/xbox/service/model/ProfileData;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2361
    const-wide/16 v0, 0xbba

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/ProfileData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2356
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/ProfileData;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetProfileRunner;->loadEssentialsOnly:Z

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$600(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V

    .line 2357
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2352
    return-void
.end method
