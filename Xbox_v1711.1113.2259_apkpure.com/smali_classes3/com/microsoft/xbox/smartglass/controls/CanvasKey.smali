.class final enum Lcom/microsoft/xbox/smartglass/controls/CanvasKey;
.super Ljava/lang/Enum;
.source "CanvasKey.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/controls/CanvasKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Asterisk:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Back:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum BrowserBack:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum BrowserRefresh:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum BrowserStop:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Display:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Down:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum DvdMenu:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Escape:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum FastForward:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Guide:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Info:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Left:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum LeftTrigger:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum MediaCenter:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num0:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num1:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num2:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num3:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num4:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num5:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num6:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num7:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num8:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Num9:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum PadA:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum PadB:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum PadX:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum PadY:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Pause:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Play:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Pound:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Record:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Replay:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Return:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Rewind:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Right:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum RightTrigger:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Skip:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Start:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Stop:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Title:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Unknown:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum Up:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum VolumeDown:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum VolumeMute:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field public static final enum VolumeUp:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/controls/CanvasKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 10
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Unknown"

    invoke-direct {v2, v3, v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Unknown:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 11
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Return"

    invoke-direct {v2, v3, v5}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Return:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Pause"

    invoke-direct {v2, v3, v6}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Pause:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 13
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Escape"

    invoke-direct {v2, v3, v7}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Escape:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num0"

    invoke-direct {v2, v3, v8}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num0:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 15
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num1"

    const/4 v4, 0x5

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num1:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num2"

    const/4 v4, 0x6

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num2:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 17
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num3"

    const/4 v4, 0x7

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num3:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num4"

    const/16 v4, 0x8

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num4:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 19
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num5"

    const/16 v4, 0x9

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num5:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num6"

    const/16 v4, 0xa

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num6:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 21
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num7"

    const/16 v4, 0xb

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num7:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num8"

    const/16 v4, 0xc

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num8:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 23
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Num9"

    const/16 v4, 0xd

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num9:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 24
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "BrowserBack"

    const/16 v4, 0xe

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->BrowserBack:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 25
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "BrowserRefresh"

    const/16 v4, 0xf

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->BrowserRefresh:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "BrowserStop"

    const/16 v4, 0x10

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->BrowserStop:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 27
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "VolumeMute"

    const/16 v4, 0x11

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->VolumeMute:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "VolumeDown"

    const/16 v4, 0x12

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->VolumeDown:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 29
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "VolumeUp"

    const/16 v4, 0x13

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->VolumeUp:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Play"

    const/16 v4, 0x14

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Play:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 31
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "PadA"

    const/16 v4, 0x15

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->PadA:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "PadB"

    const/16 v4, 0x16

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->PadB:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 33
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "PadX"

    const/16 v4, 0x17

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->PadX:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "PadY"

    const/16 v4, 0x18

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->PadY:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 35
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "LeftTrigger"

    const/16 v4, 0x19

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->LeftTrigger:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 36
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "RightTrigger"

    const/16 v4, 0x1a

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->RightTrigger:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 37
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Guide"

    const/16 v4, 0x1b

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Guide:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 38
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Up"

    const/16 v4, 0x1c

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Up:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 39
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Down"

    const/16 v4, 0x1d

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Down:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 40
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Left"

    const/16 v4, 0x1e

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Left:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 41
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Right"

    const/16 v4, 0x1f

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Right:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 42
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Start"

    const/16 v4, 0x20

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Start:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 43
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Back"

    const/16 v4, 0x21

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Back:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 44
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "DvdMenu"

    const/16 v4, 0x22

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->DvdMenu:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 45
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Display"

    const/16 v4, 0x23

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Display:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 46
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Stop"

    const/16 v4, 0x24

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Stop:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 47
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Record"

    const/16 v4, 0x25

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Record:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 48
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "FastForward"

    const/16 v4, 0x26

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->FastForward:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 49
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Rewind"

    const/16 v4, 0x27

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Rewind:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 50
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Skip"

    const/16 v4, 0x28

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Skip:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 51
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Replay"

    const/16 v4, 0x29

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Replay:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 52
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Info"

    const/16 v4, 0x2a

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Info:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 53
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Asterisk"

    const/16 v4, 0x2b

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Asterisk:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 54
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Pound"

    const/16 v4, 0x2c

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Pound:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 55
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "Title"

    const/16 v4, 0x2d

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Title:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 56
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    const-string v3, "MediaCenter"

    const/16 v4, 0x2e

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->MediaCenter:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 9
    const/16 v2, 0x2f

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Unknown:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Return:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v3, v2, v5

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Pause:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Escape:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num0:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v3, v2, v8

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num1:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num2:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num3:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num4:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num5:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num6:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num7:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num8:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Num9:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->BrowserBack:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->BrowserRefresh:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->BrowserStop:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x11

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->VolumeMute:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x12

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->VolumeDown:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x13

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->VolumeUp:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x14

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Play:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x15

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->PadA:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x16

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->PadB:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x17

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->PadX:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x18

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->PadY:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x19

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->LeftTrigger:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->RightTrigger:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Guide:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Up:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Down:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Left:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Right:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x20

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Start:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x21

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Back:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x22

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->DvdMenu:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x23

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Display:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x24

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Stop:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x25

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Record:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x26

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->FastForward:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x27

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Rewind:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x28

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Skip:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x29

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Replay:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Info:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Asterisk:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Pound:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->Title:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->MediaCenter:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->$VALUES:[Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 58
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->_lookup:Landroid/util/SparseArray;

    .line 61
    invoke-static {}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->values()[Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    move-result-object v2

    array-length v3, v2

    .local v0, "key":Lcom/microsoft/xbox/smartglass/controls/CanvasKey;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 62
    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 61
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/controls/CanvasKey;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 67
    sget-object v1, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    .line 68
    .local v0, "key":Lcom/microsoft/xbox/smartglass/controls/CanvasKey;
    if-nez v0, :cond_0

    .line 69
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1

    .line 72
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/controls/CanvasKey;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/controls/CanvasKey;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->$VALUES:[Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    return-object v0
.end method
