.class abstract Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;
.super Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;
.source "$AutoValue_PartyDataChannelTypes_MemberSeenMessage.java"


# instance fields
.field private final mpsdMemberIndex:I

.field private final type:Ljava/lang/String;

.field private final xuid:J


# direct methods
.method constructor <init>(Ljava/lang/String;IJ)V
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "mpsdMemberIndex"    # I
    .param p3, "xuid"    # J

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;-><init>()V

    .line 18
    if-nez p1, :cond_0

    .line 19
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->type:Ljava/lang/String;

    .line 22
    iput p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->mpsdMemberIndex:I

    .line 23
    iput-wide p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->xuid:J

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    if-ne p1, p0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 56
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 57
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;

    .line 58
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->type:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;->type()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->mpsdMemberIndex:I

    .line 59
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;->mpsdMemberIndex()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->xuid:J

    .line 60
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;->xuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;
    :cond_3
    move v1, v2

    .line 62
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const v2, 0xf4243

    .line 67
    const/4 v0, 0x1

    .line 68
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 70
    mul-int/2addr v0, v2

    .line 71
    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->mpsdMemberIndex:I

    xor-int/2addr v0, v1

    .line 72
    mul-int/2addr v0, v2

    .line 73
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->xuid:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->xuid:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 74
    return v0
.end method

.method public mpsdMemberIndex()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->mpsdMemberIndex:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MemberSeenMessage{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mpsdMemberIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->mpsdMemberIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", xuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->xuid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->type:Ljava/lang/String;

    return-object v0
.end method

.method public xuid()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_MemberSeenMessage;->xuid:J

    return-wide v0
.end method
