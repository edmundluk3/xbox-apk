.class public Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ParentListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 18
    return-void
.end method

.method private getIconForType(I)Ljava/lang/String;
    .locals 2
    .param p1, "mediaType"    # I

    .prologue
    .line 52
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultIconStringRid(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 22
    const/4 v3, 0x0

    .line 23
    .local v3, "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;
    move-object v1, p2

    .line 24
    .local v1, "v":Landroid/view/View;
    if-nez v1, :cond_3

    .line 25
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 26
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f03019e

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 27
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;

    .end local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;
    invoke-direct {v3, v1}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;-><init>(Landroid/view/View;)V

    .line 28
    .restart local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;
    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 33
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 34
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_2

    .line 35
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->getNameView()Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 36
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->getNameView()Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    :cond_0
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->getIconView()Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 40
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->getIconView()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/adapter/ParentListAdapter;->getIconForType(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 43
    :cond_1
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->getTypeView()Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 44
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;->getTypeView()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemTypeName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    :cond_2
    return-object v1

    .line 30
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;
    check-cast v3, Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;

    .restart local v3    # "viewHolder":Lcom/microsoft/xbox/xle/app/adapter/ParentItemViewHolder;
    goto :goto_0
.end method
