.class public Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TagArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_MARGIN:I = 0xa

.field private static final DEFAULT_PADDING:I = 0x5

.field private static final DEFAULT_RADIUS:F = 40.0f

.field private static final TAG_LAYOUT:I = 0x7f030226
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field


# instance fields
.field private backgroundColor:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private cornerRadius:F

.field private hasCancel:Z

.field private horizontalPadding:I

.field private selectedColor:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

.field private tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

.field private textAppearanceId:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private verticalPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/util/List;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backgroundColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p3, "selectedColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    const/4 v3, 0x5

    const/4 v2, -0x2

    const/16 v1, 0xa

    .line 63
    const v0, 0x7f030226

    invoke-direct {p0, p1, v0, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->textAppearanceId:I

    .line 64
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 66
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->backgroundColor:I

    .line 67
    iput p3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->selectedColor:I

    .line 69
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 72
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->cornerRadius:F

    .line 73
    iput v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->verticalPadding:I

    .line 74
    iput v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->horizontalPadding:I

    .line 76
    invoke-virtual {p0, p4}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backgroundColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p3, "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 81
    return-void
.end method

.method static synthetic lambda$getView$0(Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;
    .param p2, "dataObject"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 125
    invoke-interface {p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;->onTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V

    .line 126
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->notifyDataSetChanged()V

    .line 127
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "parent"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 85
    move-object/from16 v6, p2

    .line 87
    .local v6, "itemView":Landroid/view/View;
    move-object/from16 v0, p2

    instance-of v11, v0, Landroid/widget/LinearLayout;

    if-eqz v11, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    instance-of v11, v11, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    if-nez v11, :cond_1

    .line 88
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 89
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v11, 0x7f030226

    const/4 v12, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v5, v11, v0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 92
    .end local v5    # "inflater":Landroid/view/LayoutInflater;
    :cond_1
    const v11, 0x7f0e0aa0

    invoke-virtual {v6, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    .line 93
    .local v10, "tagContainer":Landroid/widget/LinearLayout;
    const v11, 0x7f0e0aa2

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 94
    .local v8, "tag":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v11, 0x7f0e0aa3

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 95
    .local v3, "cancel":Landroid/widget/TextView;
    const v11, 0x7f0e0aa1

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 97
    .local v1, "achievementLogo":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-gez p1, :cond_2

    new-instance v4, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->getCount()I

    move-result v11

    invoke-direct {v4, v11}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$PlaceholderTag;-><init>(I)V

    .line 99
    .local v4, "dataObject":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    :goto_0
    if-nez v4, :cond_3

    .line 100
    const-string v11, "Null items shouldn\'t be added to this adapter"

    invoke-static {v11}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 101
    const/16 v11, 0x8

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    .line 140
    :goto_1
    return-object v6

    .line 97
    .end local v4    # "dataObject":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    move-object v4, v11

    goto :goto_0

    .line 105
    .restart local v4    # "dataObject":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    :cond_3
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    .line 106
    invoke-virtual {v6, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 108
    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    iget v11, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->horizontalPadding:I

    iget v12, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->verticalPadding:I

    iget v13, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->horizontalPadding:I

    iget v14, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->verticalPadding:I

    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 113
    iget v11, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->textAppearanceId:I

    const/4 v12, -0x1

    if-eq v11, v12, :cond_4

    .line 114
    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v11

    iget v12, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->textAppearanceId:I

    invoke-virtual {v8, v11, v12}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 117
    :cond_4
    iget v9, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->backgroundColor:I

    .line 119
    .local v9, "tagBackgroundColor":I
    invoke-interface {v4}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getDisplayText()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 120
    invoke-interface {v4}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getSymbol()Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 122
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

    .line 123
    .local v7, "listener":Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;
    if-eqz v7, :cond_5

    .line 124
    invoke-static {p0, v7, v4}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)Landroid/view/View$OnClickListener;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    invoke-interface {v7, v4}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;->isTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 130
    iget v9, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->selectedColor:I

    .line 134
    :cond_5
    iget-boolean v11, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->hasCancel:Z

    invoke-static {v3, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 136
    new-instance v2, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v2, v9}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    .line 137
    .local v2, "backgroundDrawable":Landroid/graphics/drawable/PaintDrawable;
    iget v11, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->cornerRadius:F

    invoke-virtual {v2, v11}, Landroid/graphics/drawable/PaintDrawable;->setCornerRadius(F)V

    .line 138
    invoke-virtual {v10, v2}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public setBackgroundColor(I)V
    .locals 0
    .param p1, "backgroundColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 165
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->backgroundColor:I

    .line 166
    return-void
.end method

.method public setCornerRadius(F)V
    .locals 0
    .param p1, "cornerRadius"    # F

    .prologue
    .line 156
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->cornerRadius:F

    .line 157
    return-void
.end method

.method public setHasCancel(Z)V
    .locals 0
    .param p1, "hasCancel"    # Z

    .prologue
    .line 169
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->hasCancel:Z

    .line 170
    return-void
.end method

.method public setSelectedColor(I)V
    .locals 0
    .param p1, "selectedColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 152
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->selectedColor:I

    .line 153
    return-void
.end method

.method public setTagLayoutParams(Landroid/widget/LinearLayout$LayoutParams;)V
    .locals 0
    .param p1, "tagLayoutParams"    # Landroid/widget/LinearLayout$LayoutParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 160
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 161
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 162
    return-void
.end method

.method public setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;)V
    .locals 0
    .param p1, "tagSelectedListener"    # Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 148
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;

    .line 149
    return-void
.end method

.method public setTextAppearanceId(I)V
    .locals 0
    .param p1, "textAppearanceId"    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 144
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->textAppearanceId:I

    .line 145
    return-void
.end method
