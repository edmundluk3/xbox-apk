.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicOfferInfo;
.super Ljava/lang/Object;
.source "EDSV2MusicOfferInfo.java"


# static fields
.field public static final EDSV2OFFERINSTANCEDISTRIBUTIONRIGHT_PREVIEW:I = 0x1

.field public static final EDSV2OFFERINSTANCEDISTRIBUTIONRIGHT_PURCHASE:I = 0x2

.field public static final EDSV2OFFERINSTANCEDISTRIBUTIONRIGHT_PURCHASESTREAM:I = 0x3

.field public static final EDSV2OFFERINSTANCEDISTRIBUTIONRIGHT_STREAM:I = 0x4

.field public static final EDSV2OFFERINSTANCEDISTRIBUTIONRIGHT_SUBSCRIPTION:I = 0x5

.field public static final EDSV2OFFERINSTANCEDISTRIBUTIONRIGHT_UNKNOWN:I


# instance fields
.field private expirationDate:Ljava/util/Date;

.field private fulfillmentTicket:Ljava/lang/String;

.field private mediaInstanceId:Ljava/lang/String;

.field private offerExpirationDate:Ljava/lang/String;

.field private right:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFulfillmentTicket()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicOfferInfo;->fulfillmentTicket:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaInstanceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicOfferInfo;->mediaInstanceId:Ljava/lang/String;

    return-object v0
.end method

.method public getRight()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicOfferInfo;->right:I

    return v0
.end method

.method public setFulfillmentTicket(Ljava/lang/String;)V
    .locals 0
    .param p1, "ticket"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicOfferInfo;->fulfillmentTicket:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setMediaInstanceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicOfferInfo;->mediaInstanceId:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setOfferExpirationDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicOfferInfo;->offerExpirationDate:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setRight(I)V
    .locals 0
    .param p1, "right"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicOfferInfo;->right:I

    .line 20
    return-void
.end method
