.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;
.super Ljava/lang/Object;
.source "EDSV2MediaType.java"


# static fields
.field public static final EDSV2AUDIENCE_GROUP_STRING:Ljava/lang/String; = "Group"

.field public static final EDSV2AUDIENCE_ONLYME_STRING:Ljava/lang/String; = "Onlyme"

.field public static final EDSV2INFLUENCEDBY_ALL_STRING:Ljava/lang/String; = "All"

.field public static final EDSV2INFLUENCEDBY_FRIENDS_STRING:Ljava/lang/String; = "Friends"

.field public static final EDSV2INFLUENCEDBY_ME_STRING:Ljava/lang/String; = "Me"

.field public static final EDSV2INFLUENCEDBY_US_STRING:Ljava/lang/String; = "Us"

.field public static final EDS_SUBSCRIPTION_LEVEL_GOLD:Ljava/lang/String; = "gold"

.field public static final EDS_SUBSCRIPTION_LEVEL_SILVER:Ljava/lang/String; = "silver"

.field public static final MEDIATYPE_ALBUM:I = 0x3ee

.field public static final MEDIATYPE_ALBUM_STRING:Ljava/lang/String; = "Album"

.field public static final MEDIATYPE_APPACTIVITY:I = 0x43

.field public static final MEDIATYPE_APPACTIVITY_STRING:Ljava/lang/String; = "AppActivity"

.field public static final MEDIATYPE_APPLICATION:Ljava/lang/String; = "Application"

.field public static final MEDIATYPE_AVATARITEM:I = 0x2f

.field public static final MEDIATYPE_AVATARITEM_STRING:Ljava/lang/String; = "AvatarItem"

.field public static final MEDIATYPE_DACTIVITY:I = 0x232d

.field public static final MEDIATYPE_DACTIVITY_STRING:Ljava/lang/String; = "DActivity"

.field public static final MEDIATYPE_DAPP:I = 0x2328

.field public static final MEDIATYPE_DAPP_STRING:Ljava/lang/String; = "DApp"

.field public static final MEDIATYPE_DCONSUMABLE:I = 0x232b

.field public static final MEDIATYPE_DCONSUMEABLE_STRING:Ljava/lang/String; = "DConsumable"

.field public static final MEDIATYPE_DDURABLE:I = 0x232c

.field public static final MEDIATYPE_DDURABLE_STRING:Ljava/lang/String; = "DDurable"

.field public static final MEDIATYPE_DGAME:I = 0x2329

.field public static final MEDIATYPE_DGAMEDEMO:I = 0x232a

.field public static final MEDIATYPE_DGAMEDEMO_STRING:Ljava/lang/String; = "DGameDemo"

.field public static final MEDIATYPE_DGAME_STRING:Ljava/lang/String; = "DGame"

.field public static final MEDIATYPE_DNATIVEAPP:I = 0x232e

.field public static final MEDIATYPE_DNATIVEAPP_STRING:Ljava/lang/String; = "DNativeApp"

.field public static final MEDIATYPE_DVIDEOACTIIVTY:I = 0x232f

.field public static final MEDIATYPE_DVIDEOACTIVITY_STRING:Ljava/lang/String; = "DVideoActivity"

.field public static final MEDIATYPE_GAMEACTIVITY:I = 0x42

.field public static final MEDIATYPE_GAMEACTIVITY_STRING:Ljava/lang/String; = "GameActivity"

.field public static final MEDIATYPE_GAMELAYER:I = 0x41

.field public static final MEDIATYPE_GAME_STRING:Ljava/lang/String; = "Game"

.field public static final MEDIATYPE_METROGAME:I = 0x3e

.field public static final MEDIATYPE_METROGAMECONSUMABLE:I = 0x40

.field public static final MEDIATYPE_METROGAMECONSUMABLE_STRING:Ljava/lang/String; = "MetroGameConsumable"

.field public static final MEDIATYPE_METROGAMECONTENT:I = 0x3f

.field public static final MEDIATYPE_METROGAMECONTENT_STRING:Ljava/lang/String; = "MetroGameContent"

.field public static final MEDIATYPE_METROGAME_STRING:Ljava/lang/String; = "MetroGame"

.field public static final MEDIATYPE_MOBILEGAME:I = 0x3a

.field public static final MEDIATYPE_MOBILEGAME_STRING:Ljava/lang/String; = "MobileGame"

.field public static final MEDIATYPE_MOVIE:I = 0x3e8

.field public static final MEDIATYPE_MOVIE_STRING:Ljava/lang/String; = "Movie"

.field public static final MEDIATYPE_MUSICARTIST:I = 0x3f1

.field public static final MEDIATYPE_MUSICARTIST_STRING:Ljava/lang/String; = "MusicArtist"

.field public static final MEDIATYPE_MUSICVIDEO:I = 0x3f0

.field public static final MEDIATYPE_MUSICVIDEO_STRING:Ljava/lang/String; = "MusicVideo"

.field public static final MEDIATYPE_TRACK:I = 0x3ef

.field public static final MEDIATYPE_TRACK_STRING:Ljava/lang/String; = "Track"

.field public static final MEDIATYPE_TVEPISODE:I = 0x3eb

.field public static final MEDIATYPE_TVEPISODE_STRING:Ljava/lang/String; = "TVEpisode"

.field public static final MEDIATYPE_TVSEASON:I = 0x3ed

.field public static final MEDIATYPE_TVSEASON_STRING:Ljava/lang/String; = "TVSeason"

.field public static final MEDIATYPE_TVSERIES:I = 0x3ec

.field public static final MEDIATYPE_TVSERIES_STRING:Ljava/lang/String; = "TVSeries"

.field public static final MEDIATYPE_TVSHOW:I = 0x3ea

.field public static final MEDIATYPE_TVSHOW_STRING:Ljava/lang/String; = "TVShow"

.field public static final MEDIATYPE_UNINITIALIZED:I = -0x1

.field public static final MEDIATYPE_UNKNOWN:I = 0x0

.field public static final MEDIATYPE_UNKNOWN_STRING:Ljava/lang/String; = "Unknown"

.field public static final MEDIATYPE_VIDEOACTIVITY:I = 0x385

.field public static final MEDIATYPE_VIDEOACTIVITY_STRING:Ljava/lang/String; = "VideoActivity"

.field public static final MEDIATYPE_WEBGAME:I = 0x39

.field public static final MEDIATYPE_WEBGAME_STRING:Ljava/lang/String; = "WebGame"

.field public static final MEDIATYPE_WEBVIDEO:I = 0x3f2

.field public static final MEDIATYPE_WEBVIDEOCOLLECTION:I = 0x3f3

.field public static final MEDIATYPE_WEBVIDEOCOLLECTION_STRING:Ljava/lang/String; = "WebVideoCollection"

.field public static final MEDIATYPE_WEBVIDEO_STRING:Ljava/lang/String; = "WebVideo"

.field public static final MEDIATYPE_XBOX360GAME:I = 0x1

.field public static final MEDIATYPE_XBOX360GAMECONTENT:I = 0x12

.field public static final MEDIATYPE_XBOX360GAMECONTENT_STRING:Ljava/lang/String; = "Xbox360GameContent"

.field public static final MEDIATYPE_XBOX360GAMEDEMO:I = 0x13

.field public static final MEDIATYPE_XBOX360GAMEDEMO_STRING:Ljava/lang/String; = "Xbox360GameDemo"

.field public static final MEDIATYPE_XBOX360GAME_STRING:Ljava/lang/String; = "Xbox360Game"

.field public static final MEDIATYPE_XBOXAPP:I = 0x3d

.field public static final MEDIATYPE_XBOXAPP_STRING:Ljava/lang/String; = "XboxApp"

.field public static final MEDIATYPE_XBOXARCADEGAME:I = 0x17

.field public static final MEDIATYPE_XBOXARCADEGAME_STRING:Ljava/lang/String; = "XboxArcadeGame"

.field public static final MEDIATYPE_XBOXBUNDLE:I = 0x24

.field public static final MEDIATYPE_XBOXBUNDLE_STRING:Ljava/lang/String; = "XboxBundle"

.field public static final MEDIATYPE_XBOXGAMECONSUMABLE:I = 0x18

.field public static final MEDIATYPE_XBOXGAMECONSUMABLE_STRING:Ljava/lang/String; = "XboxGameConsumable"

.field public static final MEDIATYPE_XBOXGAMERTILE:I = 0x16

.field public static final MEDIATYPE_XBOXGAMERTILE_STRING:Ljava/lang/String; = "XboxGamerTile"

.field public static final MEDIATYPE_XBOXGAMETRAILER:I = 0x22

.field public static final MEDIATYPE_XBOXGAMETRAILER_STRING:Ljava/lang/String; = "XboxGameTrailer"

.field public static final MEDIATYPE_XBOXGAMETRIAL:I = 0x5

.field public static final MEDIATYPE_XBOXGAMETRIAL_STRING:Ljava/lang/String; = "XboxGameTrial"

.field public static final MEDIATYPE_XBOXGAMEVIDEO:I = 0x1e

.field public static final MEDIATYPE_XBOXGAMEVIDEO_STRING:Ljava/lang/String; = "XboxGameVideo"

.field public static final MEDIATYPE_XBOXMARKETPLACE:I = 0x2e

.field public static final MEDIATYPE_XBOXMOBILECONSUMABLE:I = 0x3c

.field public static final MEDIATYPE_XBOXMOBILECONSUMABLE_STRING:Ljava/lang/String; = "XboxMobileConsumable"

.field public static final MEDIATYPE_XBOXMOBILEPDLC:I = 0x3b

.field public static final MEDIATYPE_XBOXMOBILEPDLC_STRING:Ljava/lang/String; = "XboxMobilePDLC"

.field public static final MEDIATYPE_XBOXORIGINALGAME:I = 0x15

.field public static final MEDIATYPE_XBOXORIGINALGAME_STRING:Ljava/lang/String; = "XboxOriginalGame"

.field public static final MEDIATYPE_XBOXTHEME:I = 0x14

.field public static final MEDIATYPE_XBOXTHEME_STRING:Ljava/lang/String; = "XboxTheme"

.field public static final MEDIATYPE_XBOXXNACOMMUNITYGAME:I = 0x25

.field public static final MEDIATYPE_XBOXXNACOMMUNITYGAME_STRING:Ljava/lang/String; = "XboxXnaCommunityGame"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMediaTypeString(I)Ljava/lang/String;
    .locals 2
    .param p0, "mediaType"    # I

    .prologue
    .line 130
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    .line 131
    .local v0, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
