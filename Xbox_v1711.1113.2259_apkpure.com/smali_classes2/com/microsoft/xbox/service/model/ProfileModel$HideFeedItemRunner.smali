.class Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HideFeedItemRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final hide:Z

.field private final itemRoot:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/ProfileModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;
    .param p3, "hide"    # Z

    .prologue
    .line 4607
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 4608
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 4609
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->itemRoot:Ljava/lang/String;

    .line 4610
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->hide:Z

    .line 4611
    return-void
.end method

.method private updateItemHiddenState()V
    .locals 4

    .prologue
    .line 4635
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$5300(Lcom/microsoft/xbox/service/model/ProfileModel;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 4636
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->itemRoot:Ljava/lang/String;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4637
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->hide:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->setHidden(Z)V

    .line 4641
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_1
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4619
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->INSTANCE:Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->itemRoot:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->hide:Z

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedsService;->hideActivityItem(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4602
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4631
    const-wide/16 v0, 0xc8c

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4624
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4625
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$HideFeedItemRunner;->updateItemHiddenState()V

    .line 4627
    :cond_0
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4615
    return-void
.end method
