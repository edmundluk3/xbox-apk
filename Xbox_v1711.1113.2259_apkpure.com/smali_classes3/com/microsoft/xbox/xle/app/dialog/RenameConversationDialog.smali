.class public Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "RenameConversationDialog.java"


# instance fields
.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private renameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private renameConversation:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .prologue
    .line 34
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 35
    const v0, 0x7f0301ea

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->setContentView(I)V

    .line 36
    const v0, 0x7f0e09c1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 37
    const v0, 0x7f0e09c2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 38
    const v0, 0x7f0e04cc

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 39
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .line 40
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->setRenameEditText()V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameConversation:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;Lcom/microsoft/xbox/toolkit/ui/XLEButton;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->setButtonUI(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Ljava/lang/String;)V

    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->hideSoftKeyBoard()V

    .line 142
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissRenameConversationDialog()V

    .line 143
    return-void
.end method

.method private hideSoftKeyBoard()V
    .locals 4

    .prologue
    .line 124
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 125
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 127
    .local v0, "currentFocus":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 128
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 130
    :cond_0
    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 102
    const-string v1, "RenameConversationDialog"

    const-string v2, "Rename button clicked"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameConversation:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 104
    .local v0, "newTopic":Landroid/text/Editable;
    if-eqz v0, :cond_0

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateGroupTopicName(Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->dismissSelf()V

    .line 108
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->dismissSelf()V

    .line 109
    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->dismissSelf()V

    return-void
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->dismissSelf()V

    return-void
.end method

.method static synthetic lambda$setRenameEditText$0(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameConversation:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->setButtonUI(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method static synthetic lambda$setRenameEditText$1(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameConversation:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setCursorVisible(Z)V

    return-void
.end method

.method private setButtonUI(Lcom/microsoft/xbox/toolkit/ui/XLEButton;Ljava/lang/String;)V
    .locals 2
    .param p1, "button"    # Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 89
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0142

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 92
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0151

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    goto :goto_0
.end method

.method private setRenameEditText()V
    .locals 3

    .prologue
    .line 45
    const v1, 0x7f0e09c0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 46
    .local v0, "clearMessageName":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    if-eqz v0, :cond_0

    .line 47
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    :cond_0
    const v1, 0x7f0e09bf

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameConversation:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameConversation:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getCurrentTopicName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameConversation:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameConversation:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->dismissSelf()V

    .line 138
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0151

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->renameButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public setVm(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .line 121
    return-void
.end method
