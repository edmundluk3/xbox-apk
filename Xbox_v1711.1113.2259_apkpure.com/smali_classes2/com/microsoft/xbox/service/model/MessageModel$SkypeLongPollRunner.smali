.class Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SkypeLongPollRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/MessageModel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final locationUrl:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final serviceManager:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/service/model/MessageModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/MessageModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "locationUrl"    # Ljava/lang/String;

    .prologue
    .line 1583
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1576
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSkypeGroupMessageService()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->serviceManager:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    .line 1584
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1586
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 1587
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->locationUrl:Ljava/lang/String;

    .line 1588
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1597
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->locationUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1598
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->getDefaultErrorCode()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 1602
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->serviceManager:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->locationUrl:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->skypeLongPoll(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeEvent;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1608
    :goto_0
    return-object v1

    .line 1603
    :catch_0
    move-exception v0

    .line 1607
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v2

    const-wide/16 v4, 0x3

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1608
    new-instance v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeNoEvent;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$SkypeRealTimeNoEvent;-><init>()V

    goto :goto_0

    .line 1610
    :cond_1
    throw v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1575
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->buildData()Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1622
    const-wide/16 v0, 0xbe9

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1617
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/groupMessaging/SkypeMessageDataTypes$ISkypeRealTimeEvent;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$SkypeLongPollRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1500(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1618
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1592
    return-void
.end method
