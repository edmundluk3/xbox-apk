.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEnforcement$1;
.super Ljava/lang/Object;
.source "UTCEnforcement.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEnforcement;->TrackClubEnforcement(Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$reason:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEnforcement$1;->val$reason:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 22
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEnforcement$1;->val$reason:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 25
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ReportReason"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEnforcement$1;->val$reason:Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ReportReasonCategory;->getTelemetryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    const-string v1, "Clubs - Report"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 28
    return-void
.end method
