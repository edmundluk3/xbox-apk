.class public Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;
.super Ljava/lang/Object;
.source "ViewPagerWithTabs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HeaderParams"
.end annotation


# instance fields
.field private final headerParams:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

.field private final subHeaderParams:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;)V
    .locals 0
    .param p1, "headerParams"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    .param p2, "subHeaderParams"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    .prologue
    .line 434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 435
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;->headerParams:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    .line 436
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;->subHeaderParams:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    .line 437
    return-void
.end method


# virtual methods
.method public getHeaderParams()Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;->headerParams:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    return-object v0
.end method

.method public getSubHeaderParams()Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;->subHeaderParams:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    return-object v0
.end method
