.class public abstract Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "MultiPurposeViewModelBase.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected createAdapter()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    if-nez v0, :cond_0

    .line 41
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "Not creating adapter because adapter provider is not set"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;->getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    goto :goto_0
.end method

.method public getAdapterProvider()Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    return-object v0
.end method

.method public abstract getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->createAdapter()V

    .line 24
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->createAdapter()V

    .line 29
    return-void
.end method

.method public setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 0
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 37
    return-void
.end method
