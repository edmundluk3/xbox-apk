.class Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChangeGamertagRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private gamertag:Ljava/lang/String;

.field private preview:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel;Ljava/lang/String;Z)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "gamertag"    # Ljava/lang/String;
    .param p4, "preview"    # Z

    .prologue
    .line 2626
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2627
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 2628
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->gamertag:Ljava/lang/String;

    .line 2629
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->preview:Z

    .line 2630
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2634
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->gamertag:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->preview:Z

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->changeGamertag(Ljava/lang/String;ZLjava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2621
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2643
    const-wide/16 v0, 0x232a

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2648
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->caller:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->gamertag:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$ChangeGamertagRunner;->preview:Z

    invoke-virtual {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->onChangeGamertagCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Z)V

    .line 2649
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2639
    return-void
.end method
