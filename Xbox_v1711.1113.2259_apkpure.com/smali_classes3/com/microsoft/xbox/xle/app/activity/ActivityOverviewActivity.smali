.class public Lcom/microsoft/xbox/xle/app/activity/ActivityOverviewActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ActivityOverviewActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "ActivityOverview"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 31
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityOverviewActivity;->onCreateContentView()V

    .line 33
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ActivityOverviewActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 34
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityOverviewActivity;->setContentView(I)V

    .line 39
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method
