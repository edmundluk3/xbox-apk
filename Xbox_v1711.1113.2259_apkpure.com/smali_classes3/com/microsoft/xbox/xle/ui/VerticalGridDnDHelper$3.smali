.class Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;
.super Ljava/lang/Object;
.source "VerticalGridDnDHelper.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .prologue
    .line 91
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 7

    .prologue
    .line 94
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$600(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 95
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$400(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getFirstVisiblePosition()I

    move-result v0

    .line 96
    .local v0, "fvp":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$600(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 97
    .local v2, "position":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$600(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Ljava/util/HashMap;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;

    .line 98
    .local v1, "off":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$400(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v4

    sub-int v6, v2, v0

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 99
    .local v3, "v":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 100
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v4, v3, v1}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$700(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;Landroid/view/View;Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;)V

    goto :goto_0

    .line 103
    .end local v1    # "off":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Offset;
    .end local v2    # "position":I
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$800(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Ljava/util/HashMap;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$600(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 104
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$3;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$600(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 106
    .end local v0    # "fvp":I
    :cond_2
    const/4 v4, 0x1

    return v4
.end method
