.class Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$2;
.super Ljava/lang/Object;
.source "AuthStateManagerImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/idp/interop/XsapiUser$SignOutCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->doSignOutXsapi()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$2;->this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(IILjava/lang/String;)V
    .locals 3
    .param p1, "httpStatusCode"    # I
    .param p2, "errorCode"    # I
    .param p3, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 202
    invoke-static {}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "signOut:onError - httpStatusCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", errorCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$2;->this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$100(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutError:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 204
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 189
    invoke-static {}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "signOutSilently:onSuccess"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->reset()V

    .line 195
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->reset()V

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$2;->this$0:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;

    invoke-static {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;->access$100(Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 198
    return-void
.end method
