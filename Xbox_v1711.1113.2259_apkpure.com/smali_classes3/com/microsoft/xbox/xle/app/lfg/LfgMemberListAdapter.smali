.class public Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "LfgMemberListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;",
        "Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private isHost:Z

.field private final onDeclineClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;",
            ">;"
        }
    .end annotation
.end field

.field private final onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "onItemClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;>;"
    .local p2, "onDeclineClicked":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 30
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->onDeclineClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->isHost:Z

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->isHost:Z

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->onItemClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->onDeclineClicked:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 51
    const v0, 0x7f03015e

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 46
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;->bindTo(Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;)V

    .line 47
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 41
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter$LfgMemberViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public setIsHost(Z)V
    .locals 0
    .param p1, "isHost"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgMemberListAdapter;->isHost:Z

    .line 36
    return-void
.end method
