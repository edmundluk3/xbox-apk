.class public Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TitleLeaderboardScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;",
        ">;"
    }
.end annotation


# instance fields
.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 22
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    .line 23
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 29
    if-nez p2, :cond_1

    .line 30
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 31
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f030230

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 33
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;

    invoke-direct {v0, p2}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;-><init>(Landroid/view/View;)V

    .line 34
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 39
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;

    .line 40
    .local v1, "user":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    if-eqz v1, :cond_0

    .line 41
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;->isTimeLeaderboard()Z

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;->updateContent(Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;Z)V

    .line 44
    :cond_0
    return-object p2

    .line 36
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;
    .end local v1    # "user":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$User;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;

    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenListAdapter$ListItem;
    goto :goto_0
.end method
