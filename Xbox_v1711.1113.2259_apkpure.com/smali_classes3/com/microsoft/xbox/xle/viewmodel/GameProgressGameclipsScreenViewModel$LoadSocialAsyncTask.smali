.class Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProgressGameclipsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadSocialAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private captureData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation
.end field

.field private filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 0
    .param p3, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 437
    .local p2, "gameClipData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 438
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->captureData:Ljava/util/ArrayList;

    .line 439
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 440
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 444
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 445
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 477
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 478
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->captureData:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameClipSocialInfo(Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 472
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 450
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 451
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$402(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Z)Z

    .line 452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Z)Z

    .line 453
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)V

    .line 454
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateAdapter()V

    .line 455
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 464
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$402(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Z)Z

    .line 465
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;Z)Z

    .line 466
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)V

    .line 467
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->updateAdapter()V

    .line 468
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 433
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel$LoadSocialAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 459
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 460
    return-void
.end method
