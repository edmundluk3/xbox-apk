.class public Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;
.source "GameOverviewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTIVITIES_SERVICE_MANAGER:Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private companion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

.field private loadCompanionTask:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->TAG:Ljava/lang/String;

    .line 33
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getActivitiesServiceManager()Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->ACTIVITIES_SERVICE_MANAGER:Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;-><init>()V

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 41
    return-void
.end method

.method static synthetic access$100()Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->ACTIVITIES_SERVICE_MANAGER:Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->onLoadCompanionTaskCompleted(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    return-void
.end method

.method private getDetailModelForLaunching()Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsBundle()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getBundlePrimaryItemDetailModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v0

    .line 146
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    goto :goto_0
.end method

.method private onLoadCompanionTaskCompleted(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 3
    .param p1, "companion"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 172
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadCompanionTaskCompleted: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_0

    const-string v0, " companion loaded"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->companion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 174
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->updateAdapter()V

    .line 175
    return-void

    .line 172
    :cond_0
    const-string v0, " no companion"

    goto :goto_0
.end method


# virtual methods
.method public getCoop()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getCoop()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getDefaultParentalRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v0

    return-object v0
.end method

.method public getDeveloper()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getDeveloper()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 64
    const v0, 0x7f070683

    return v0
.end method

.method public getMinRequirements()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMinRequirements()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMultiplayer()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMultiplayer()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getPublisher()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRatingDescriptors()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getRatingDescriptors()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRecRequirements()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getRecRequirements()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasDesktopSystemRequirements()Z
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getMinRequirements()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getRecRequirements()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public launchCompanion()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->companion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->companion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchNativeOrHtmlCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 139
    :cond_0
    return-void
.end method

.method public launchGame()V
    .locals 6

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDetailModelForLaunching()Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v0

    .line 106
    .local v0, "gameDetailModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 108
    const-string v2, "the provider is empty"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 113
    .local v1, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMediaType()I

    move-result v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMediaGroup()I

    move-result v4

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .line 114
    invoke-static {v1, v5}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v5

    .line 113
    invoke-static {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Ljava/lang/String;IILcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    goto :goto_0
.end method

.method public launchGameProfile()V
    .locals 2

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDetailModelForLaunching()Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v0

    .line 130
    .local v0, "gameDetailModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->navigateToGameProfile(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 133
    :cond_0
    return-void
.end method

.method public launchHelp()V
    .locals 6

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->getDetailModelForLaunching()Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    move-result-object v0

    .line 120
    .local v0, "gameDetailModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getTitleId()J

    move-result-wide v4

    .line 122
    .local v4, "titleId":J
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getProductId()Ljava/lang/String;

    move-result-object v2

    .line 123
    .local v2, "productId":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 124
    .local v1, "name":Ljava/lang/String;
    invoke-static {v4, v5, v2, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchHelp(JLjava/lang/String;Ljava/lang/String;)V

    .line 126
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "productId":Ljava/lang/String;
    .end local v4    # "titleId":J
    :cond_0
    return-void
.end method

.method public onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 52
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->loadCompanionTask:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->loadCompanionTask:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;->cancel()V

    .line 57
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->loadCompanionTask:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->loadCompanionTask:Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel$LoadCompanionTask;->load(Z)V

    .line 60
    :cond_1
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 46
    return-void
.end method

.method public setFilter(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;)V
    .locals 0
    .param p1, "platformType"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 151
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->setPlatformType(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;)V

    .line 153
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->updateViewModelState()V

    .line 154
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->updateAdapter()V

    .line 155
    return-void
.end method

.method public shouldShowCompanionButton()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->companion:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldShowGameHubAndHelpButton()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 95
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsBundle()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 96
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBundlePrimaryItemTitleId()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 96
    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 97
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
