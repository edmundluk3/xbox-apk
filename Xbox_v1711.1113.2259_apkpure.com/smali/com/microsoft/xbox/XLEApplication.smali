.class public Lcom/microsoft/xbox/XLEApplication;
.super Landroid/support/multidex/MultiDexApplication;
.source "XLEApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;
    }
.end annotation


# static fields
.field public static Accelerometer:Lcom/microsoft/xbox/toolkit/XLEAccelerometer;

.field public static AccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field public static ActivityManager:Landroid/app/ActivityManager;

.field public static final ApplicationReady:Lcom/microsoft/xbox/toolkit/Ready;

.field public static AssetManager:Landroid/content/res/AssetManager;

.field public static Instance:Lcom/microsoft/xbox/XLEApplication;

.field private static MainActivityInstance:Lcom/microsoft/xbox/xle/app/MainActivity;

.field public static PackageName:Ljava/lang/String;

.field public static Resources:Landroid/content/res/Resources;

.field private static final TAG:Ljava/lang/String;

.field public static VersionCode:I

.field public static VersionName:Ljava/lang/String;

.field private static final fixMultiDexEPGIterator:Lcom/microsoft/xbox/service/model/epg/EPGIterator;

.field private static final fixMultiDexEPGModel:Lcom/microsoft/xbox/service/model/epg/EPGModel;

.field private static final fixMultiDexServiceCommon:Lcom/microsoft/xbox/service/network/managers/ServiceCommon;


# instance fields
.field private component:Lcom/microsoft/xbox/XLEComponent;

.field private final reLinkerLogger:Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    const-class v0, Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/XLEApplication;->TAG:Ljava/lang/String;

    .line 39
    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->fixMultiDexEPGModel:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    .line 40
    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->fixMultiDexEPGIterator:Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    .line 41
    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->fixMultiDexServiceCommon:Lcom/microsoft/xbox/service/network/managers/ServiceCommon;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/XLEApplication;->ApplicationReady:Lcom/microsoft/xbox/toolkit/Ready;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/multidex/MultiDexApplication;-><init>()V

    .line 55
    new-instance v0, Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;-><init>(Lcom/microsoft/xbox/XLEApplication;Lcom/microsoft/xbox/XLEApplication$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/XLEApplication;->reLinkerLogger:Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;

    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 76
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->MainActivityInstance:Lcom/microsoft/xbox/xle/app/MainActivity;

    return-object v0
.end method

.method public static getVersionCode()I
    .locals 1

    .prologue
    .line 80
    sget v0, Lcom/microsoft/xbox/XLEApplication;->VersionCode:I

    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->getCurrentVersion(I)I

    move-result v0

    return v0
.end method

.method public static getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->VersionName:Ljava/lang/String;

    return-object v0
.end method

.method private initAnimations()V
    .locals 1

    .prologue
    .line 161
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->loadAnimations()V

    .line 162
    return-void
.end method

.method private initCll()V
    .locals 2

    .prologue
    .line 171
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->initInteropCll(Landroid/content/Context;)V

    .line 172
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->TAG:Ljava/lang/String;

    const-string v1, "Interop initialized"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method private initDependencyInjection()V
    .locals 2

    .prologue
    .line 111
    invoke-static {}, Lcom/microsoft/xbox/DaggerXLEComponent;->builder()Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/XLEAppModule;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/XLEAppModule;-><init>(Landroid/app/Application;)V

    .line 112
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->xLEAppModule(Lcom/microsoft/xbox/XLEAppModule;)Lcom/microsoft/xbox/DaggerXLEComponent$Builder;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lcom/microsoft/xbox/DaggerXLEComponent$Builder;->build()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/XLEApplication;->component:Lcom/microsoft/xbox/XLEComponent;

    .line 114
    return-void
.end method

.method private initGlobals()V
    .locals 4

    .prologue
    .line 136
    sput-object p0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    .line 137
    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 138
    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    .line 139
    const-string v1, "activity"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->ActivityManager:Landroid/app/ActivityManager;

    .line 140
    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    .line 141
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;-><init>(Lcom/microsoft/xbox/XLEApplication;)V

    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->Accelerometer:Lcom/microsoft/xbox/toolkit/XLEAccelerometer;

    .line 142
    const-string v1, "accessibility"

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->AccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 145
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    sput v1, Lcom/microsoft/xbox/XLEApplication;->VersionCode:I

    .line 146
    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v1, Lcom/microsoft/xbox/XLEApplication;->VersionName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_0
    return-void

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->TAG:Ljava/lang/String;

    const-string v2, "failed to get version code. default to 0"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private initInterop()V
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/idp/interop/Interop;->initializeInterop(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error initializing Interop.  Please fix."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    return-void
.end method

.method private initThreadManager()V
    .locals 1

    .prologue
    .line 153
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    .line 154
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->Handler:Landroid/os/Handler;

    .line 157
    sget-object v0, Lcom/microsoft/xbox/toolkit/XLEUnhandledExceptionHandler;->Instance:Lcom/microsoft/xbox/toolkit/XLEUnhandledExceptionHandler;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 158
    return-void
.end method

.method public static setMainActivity(Lcom/microsoft/xbox/xle/app/MainActivity;)V
    .locals 0
    .param p0, "mainActivity"    # Lcom/microsoft/xbox/xle/app/MainActivity;

    .prologue
    .line 88
    sput-object p0, Lcom/microsoft/xbox/XLEApplication;->MainActivityInstance:Lcom/microsoft/xbox/xle/app/MainActivity;

    .line 89
    return-void
.end method


# virtual methods
.method public getComponent()Lcom/microsoft/xbox/XLEComponent;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/XLEApplication;->component:Lcom/microsoft/xbox/XLEComponent;

    return-object v0
.end method

.method public initDependencyLibraries()V
    .locals 4

    .prologue
    .line 119
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/XLEApplication;->reLinkerLogger:Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;

    invoke-static {v1}, Lcom/getkeepsafe/relinker/ReLinker;->log(Lcom/getkeepsafe/relinker/ReLinker$Logger;)Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->recursively()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->force()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "gnustl_shared"

    invoke-virtual {v1, v2, v3}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->loadLibrary(Landroid/content/Context;Ljava/lang/String;)V

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/XLEApplication;->reLinkerLogger:Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;

    invoke-static {v1}, Lcom/getkeepsafe/relinker/ReLinker;->log(Lcom/getkeepsafe/relinker/ReLinker$Logger;)Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->recursively()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->force()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "vortex"

    invoke-virtual {v1, v2, v3}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->loadLibrary(Landroid/content/Context;Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/XLEApplication;->reLinkerLogger:Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;

    invoke-static {v1}, Lcom/getkeepsafe/relinker/ReLinker;->log(Lcom/getkeepsafe/relinker/ReLinker$Logger;)Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->recursively()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->force()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "xml2"

    invoke-virtual {v1, v2, v3}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->loadLibrary(Landroid/content/Context;Ljava/lang/String;)V

    .line 123
    invoke-static {}, Lcom/getkeepsafe/relinker/ReLinker;->recursively()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->force()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "epg-data-manager"

    invoke-virtual {v1, v2, v3}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->loadLibrary(Landroid/content/Context;Ljava/lang/String;)V

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/XLEApplication;->reLinkerLogger:Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;

    invoke-static {v1}, Lcom/getkeepsafe/relinker/ReLinker;->log(Lcom/getkeepsafe/relinker/ReLinker$Logger;)Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->recursively()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->force()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "xbidp"

    invoke-virtual {v1, v2, v3}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->loadLibrary(Landroid/content/Context;Ljava/lang/String;)V

    .line 126
    iget-object v1, p0, Lcom/microsoft/xbox/XLEApplication;->reLinkerLogger:Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;

    invoke-static {v1}, Lcom/getkeepsafe/relinker/ReLinker;->log(Lcom/getkeepsafe/relinker/ReLinker$Logger;)Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->recursively()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->force()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "nanoclient"

    invoke-virtual {v1, v2, v3}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->loadLibrary(Landroid/content/Context;Ljava/lang/String;)V

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/XLEApplication;->reLinkerLogger:Lcom/microsoft/xbox/XLEApplication$ReLinkerLogger;

    invoke-static {v1}, Lcom/getkeepsafe/relinker/ReLinker;->log(Lcom/getkeepsafe/relinker/ReLinker$Logger;)Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->recursively()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->force()Lcom/getkeepsafe/relinker/ReLinkerInstance;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "c++_shared"

    invoke-virtual {v1, v2, v3}, Lcom/getkeepsafe/relinker/ReLinkerInstance;->loadLibrary(Landroid/content/Context;Ljava/lang/String;)V

    .line 128
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->TAG:Ljava/lang/String;

    const-string v2, "Successfully loaded dependency libraries"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to load library "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isAspectRatioLong()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 189
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 190
    .local v1, "aspectRatioVal":Landroid/util/TypedValue;
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f090599

    invoke-virtual {v3, v4, v1, v2}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 191
    invoke-virtual {v1}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    .line 193
    .local v0, "aspectRatio":F
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenWidthHeightAspectRatio()F

    move-result v3

    cmpl-float v3, v3, v0

    if-ltz v3, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public killApp()V
    .locals 2

    .prologue
    .line 208
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->TAG:Ljava/lang/String;

    const-string v1, "killing the application"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 210
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 184
    invoke-super {p0, p1}, Landroid/support/multidex/MultiDexApplication;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 185
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->ensureDisplayLocale()V

    .line 186
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0}, Landroid/support/multidex/MultiDexApplication;->onCreate()V

    .line 94
    invoke-direct {p0}, Lcom/microsoft/xbox/XLEApplication;->initDependencyInjection()V

    .line 95
    invoke-virtual {p0}, Lcom/microsoft/xbox/XLEApplication;->initDependencyLibraries()V

    .line 96
    invoke-direct {p0}, Lcom/microsoft/xbox/XLEApplication;->initGlobals()V

    .line 97
    invoke-direct {p0}, Lcom/microsoft/xbox/XLEApplication;->initThreadManager()V

    .line 98
    invoke-direct {p0}, Lcom/microsoft/xbox/XLEApplication;->initAnimations()V

    .line 99
    invoke-direct {p0}, Lcom/microsoft/xbox/XLEApplication;->initInterop()V

    .line 100
    invoke-direct {p0}, Lcom/microsoft/xbox/XLEApplication;->initCll()V

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->setProvider(Lcom/microsoft/xbox/toolkit/IProjectSpecificDataProvider;)V

    .line 103
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->setManager(Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;)V

    .line 105
    const v0, 0x7f08019a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/XLEApplication;->setTheme(I)V

    .line 107
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->ApplicationReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 108
    return-void
.end method

.method public onTerminate()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Landroid/support/multidex/MultiDexApplication;->onTerminate()V

    .line 178
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/Interop;->deinitializeInterop()Z

    .line 179
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->TAG:Ljava/lang/String;

    const-string v1, "Interop deinitialized"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    return-void
.end method

.method public shouldShowStreamingButton()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 198
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 199
    .local v0, "branchSession":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v2

    .line 201
    .local v2, "streamer":Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->streamingCanBeEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->canStreamHdmi()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->canStreamTuner()Z
    :try_end_0
    .catch Lcom/microsoft/xbox/xle/epg/TvStreamerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v3, 0x1

    .line 203
    .end local v0    # "branchSession":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    .end local v2    # "streamer":Lcom/microsoft/xbox/xle/epg/TvStreamerModel;
    :cond_1
    :goto_0
    return v3

    .line 202
    :catch_0
    move-exception v1

    .line 203
    .local v1, "e":Lcom/microsoft/xbox/xle/epg/TvStreamerException;
    goto :goto_0
.end method
