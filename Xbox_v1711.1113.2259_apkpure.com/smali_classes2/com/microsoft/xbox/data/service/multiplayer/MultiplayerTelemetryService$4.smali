.class Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$4;
.super Ljava/lang/Object;
.source "MultiplayerTelemetryService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->removeMember(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

.field final synthetic val$xuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$4;->this$0:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iput-object p2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$4;->val$xuid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 145
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$4;->val$xuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 147
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 148
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TargetXuid"

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$4;->val$xuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 150
    const-string v1, "Party - Remove member from party"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 151
    return-void
.end method
