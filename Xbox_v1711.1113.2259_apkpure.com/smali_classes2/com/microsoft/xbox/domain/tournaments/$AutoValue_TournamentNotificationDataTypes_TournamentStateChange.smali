.class abstract Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;
.super Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
.source "$AutoValue_TournamentNotificationDataTypes_TournamentStateChange.java"


# instance fields
.field private final newState:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

.field private final recipientXuid:Ljava/lang/String;

.field private final teamName:Ljava/lang/String;

.field private final titleId:J

.field private final titleName:Ljava/lang/String;

.field private final tournamentImageUri:Ljava/lang/String;

.field private final tournamentName:Ljava/lang/String;

.field private final tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;J)V
    .locals 2
    .param p1, "newState"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;
    .param p2, "teamName"    # Ljava/lang/String;
    .param p3, "tournamentName"    # Ljava/lang/String;
    .param p4, "recipientXuid"    # Ljava/lang/String;
    .param p5, "titleName"    # Ljava/lang/String;
    .param p6, "tournamentImageUri"    # Ljava/lang/String;
    .param p7, "tournamentRef"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    .param p8, "titleId"    # J

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;-><init>()V

    .line 28
    if-nez p1, :cond_0

    .line 29
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null newState"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->newState:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    .line 32
    if-nez p2, :cond_1

    .line 33
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null teamName"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->teamName:Ljava/lang/String;

    .line 36
    if-nez p3, :cond_2

    .line 37
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null tournamentName"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentName:Ljava/lang/String;

    .line 40
    if-nez p4, :cond_3

    .line 41
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null recipientXuid"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->recipientXuid:Ljava/lang/String;

    .line 44
    if-nez p5, :cond_4

    .line 45
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null titleName"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleName:Ljava/lang/String;

    .line 48
    if-nez p6, :cond_5

    .line 49
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null tournamentImageUri"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentImageUri:Ljava/lang/String;

    .line 52
    if-nez p7, :cond_6

    .line 53
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null tournamentRef"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_6
    iput-object p7, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .line 56
    iput-wide p8, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleId:J

    .line 57
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    if-ne p1, p0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return v1

    .line 125
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 126
    check-cast v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;

    .line 127
    .local v0, "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->newState:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->newState()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->teamName:Ljava/lang/String;

    .line 128
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->teamName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentName:Ljava/lang/String;

    .line 129
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->tournamentName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->recipientXuid:Ljava/lang/String;

    .line 130
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->recipientXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleName:Ljava/lang/String;

    .line 131
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->titleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentImageUri:Ljava/lang/String;

    .line 132
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->tournamentImageUri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .line 133
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleId:J

    .line 134
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;->titleId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentStateChange;
    :cond_3
    move v1, v2

    .line 136
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const v2, 0xf4243

    .line 141
    const/4 v0, 0x1

    .line 142
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 143
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->newState:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 144
    mul-int/2addr v0, v2

    .line 145
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->teamName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 146
    mul-int/2addr v0, v2

    .line 147
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 148
    mul-int/2addr v0, v2

    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->recipientXuid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 150
    mul-int/2addr v0, v2

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 152
    mul-int/2addr v0, v2

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentImageUri:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 154
    mul-int/2addr v0, v2

    .line 155
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 156
    mul-int/2addr v0, v2

    .line 157
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleId:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleId:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 158
    return v0
.end method

.method public newState()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->newState:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    return-object v0
.end method

.method public recipientXuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->recipientXuid:Ljava/lang/String;

    return-object v0
.end method

.method public teamName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->teamName:Ljava/lang/String;

    return-object v0
.end method

.method public titleId()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleId:J

    return-wide v0
.end method

.method public titleName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TournamentStateChange{newState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->newState:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", teamName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->teamName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tournamentName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", recipientXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->recipientXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tournamentImageUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentImageUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tournamentRef="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->titleId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tournamentImageUri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentImageUri:Ljava/lang/String;

    return-object v0
.end method

.method public tournamentName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentName:Ljava/lang/String;

    return-object v0
.end method

.method public tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentStateChange;->tournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    return-object v0
.end method
