.class final enum Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;
.super Ljava/lang/Enum;
.source "InternetExplorerControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "BrowserCommandType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

.field public static final enum Back:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

.field public static final enum Favorite:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

.field public static final enum Refresh:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

.field public static final enum Stop:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

.field public static final enum ToggleWebHub:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

.field public static final enum Unknown:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 51
    new-instance v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v4, v4}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Unknown:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    const-string v1, "Back"

    invoke-direct {v0, v1, v5, v5}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Back:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    .line 53
    new-instance v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    const-string v1, "Refresh"

    invoke-direct {v0, v1, v6, v6}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Refresh:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    .line 54
    new-instance v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    const-string v1, "Stop"

    invoke-direct {v0, v1, v7, v7}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Stop:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    .line 55
    new-instance v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    const-string v1, "Favorite"

    invoke-direct {v0, v1, v8, v8}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Favorite:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    .line 56
    new-instance v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    const-string v1, "ToggleWebHub"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->ToggleWebHub:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    .line 49
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Unknown:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Back:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Refresh:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Stop:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->Favorite:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->ToggleWebHub:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->$VALUES:[Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    iput p3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->value:I

    .line 63
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->$VALUES:[Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$BrowserCommandType;->value:I

    return v0
.end method
