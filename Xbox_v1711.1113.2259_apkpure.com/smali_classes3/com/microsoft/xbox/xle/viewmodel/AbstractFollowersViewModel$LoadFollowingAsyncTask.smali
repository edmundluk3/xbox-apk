.class Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "AbstractFollowersViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadFollowingAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$1;

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 214
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshFollowingProfile()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowingProfile(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 220
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->onLoadFollowingCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 222
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->onLoadFollowingCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 233
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 211
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 226
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel$LoadFollowingAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/viewmodel/AbstractFollowersViewModel;->isLoadingFollowingData:Z

    .line 228
    return-void
.end method
