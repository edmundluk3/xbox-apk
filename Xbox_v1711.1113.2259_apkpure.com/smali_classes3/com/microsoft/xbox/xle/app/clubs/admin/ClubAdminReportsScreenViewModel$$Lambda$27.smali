.class final synthetic Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$27;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# static fields
.field private static final instance:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$27;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$27;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$27;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$27;->instance:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$27;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/Function;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$27;->instance:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$27;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
