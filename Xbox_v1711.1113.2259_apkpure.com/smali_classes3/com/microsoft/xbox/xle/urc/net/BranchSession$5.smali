.class Lcom/microsoft/xbox/xle/urc/net/BranchSession$5;
.super Ljava/lang/Object;
.source "BranchSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onNotifyChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

.field final synthetic val$blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

.field final synthetic val$channelType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 423
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$5;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$5;->val$channelType:Ljava/lang/String;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$5;->val$blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 427
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$5;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$400(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;

    .line 428
    .local v0, "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$5;->val$channelType:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$5;->val$blockExplicitContentPerShowInfo:Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;->onChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V

    goto :goto_0

    .line 430
    .end local v0    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;
    :cond_0
    return-void
.end method
