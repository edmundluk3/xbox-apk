.class public Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubSearchScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;
    }
.end annotation


# static fields
.field private static final RESULTS_FORMAT:Ljava/lang/String;

.field private static final SINGLE_RESULT:Ljava/lang/String;


# instance fields
.field private final criteriaDetail:Landroid/view/View;

.field private final criteriaText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final emptyCriteriaView:Landroid/view/View;

.field private final numResults:Landroid/widget/TextView;

.field private final recentGame1:Landroid/view/View;

.field private final recentGame2:Landroid/view/View;

.field private final recentGame3:Landroid/view/View;

.field private final recentsHeader:Landroid/view/View;

.field private final searchByTagsView:Landroid/view/View;

.field private final searchByTitlesView:Landroid/view/View;

.field private final searchInput:Landroid/widget/AutoCompleteTextView;

.field private final searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

.field private final searchResultList:Landroid/support/v7/widget/RecyclerView;

.field private final searchResultListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

.field private final searchTextAppearance:Landroid/text/style/TextAppearanceSpan;

.field private selectedTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedTagsCount:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final selectedTitlesCount:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private suggestionsHashCode:I

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

.field private final tagsHeader:Landroid/view/View;

.field private final tagsLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070334

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->SINGLE_RESULT:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070333

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->RESULTS_FORMAT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)V
    .locals 7
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v6, 0x7f0c0149

    const/4 v4, 0x2

    .line 80
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 76
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTags:Ljava/util/List;

    .line 82
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 84
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    .line 86
    const v2, 0x7f0e0a95

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 87
    const v2, 0x7f0e03c4

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/AutoCompleteTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInput:Landroid/widget/AutoCompleteTextView;

    .line 89
    const v2, 0x7f0e03bf

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchByTitlesView:Landroid/view/View;

    .line 90
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchByTitlesView:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const v2, 0x7f0e03c1

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTitlesCount:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 92
    const v2, 0x7f0e03c0

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 93
    .local v0, "searchByGamesIcon":Landroid/view/View;
    invoke-static {v0, v4}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 94
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTitlesCount:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v2, v4}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 96
    const v2, 0x7f0e03bc

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchByTagsView:Landroid/view/View;

    .line 97
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchByTagsView:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const v2, 0x7f0e03be

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTagsCount:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 99
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTagsCount:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v2, v4}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 100
    const v2, 0x7f0e03bd

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 101
    .local v1, "searchTagsIcon":Landroid/view/View;
    invoke-static {v1, v4}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 103
    const v2, 0x7f0e03c5

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->emptyCriteriaView:Landroid/view/View;

    .line 104
    const v2, 0x7f0e03c6

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentsHeader:Landroid/view/View;

    .line 105
    const v2, 0x7f0e03c7

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentGame1:Landroid/view/View;

    .line 106
    const v2, 0x7f0e03c8

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentGame2:Landroid/view/View;

    .line 107
    const v2, 0x7f0e03c9

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentGame3:Landroid/view/View;

    .line 108
    const v2, 0x7f0e03ca

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagsHeader:Landroid/view/View;

    .line 109
    const v2, 0x7f0e03cb

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagsLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    .line 110
    const v2, 0x7f0e03cc

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->criteriaDetail:Landroid/view/View;

    .line 111
    const v2, 0x7f0e03cd

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->criteriaText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 112
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f080221

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchTextAppearance:Landroid/text/style/TextAppearanceSpan;

    .line 113
    const v2, 0x7f0e03ce

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->numResults:Landroid/widget/TextView;

    .line 114
    const v2, 0x7f0e03ba

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultList:Landroid/support/v7/widget/RecyclerView;

    .line 116
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInput:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3}, Landroid/widget/AutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x109000a

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInput:Landroid/widget/AutoCompleteTextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInput:Landroid/widget/AutoCompleteTextView;

    sget-object v3, Lcom/microsoft/xbox/xle/app/XLEUtil;->COMMON_TYPEFACE:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInput:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$1;

    invoke-direct {v3, p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)V

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 139
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInput:Landroid/widget/AutoCompleteTextView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 156
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInput:Landroid/widget/AutoCompleteTextView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 170
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .line 171
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v4

    new-instance v5, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$2;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;)V

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .line 185
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultList:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 187
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 189
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 190
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 193
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$3;

    invoke-direct {v3, p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;)V

    .line 206
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagsLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 207
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTags:Ljava/util/List;

    return-object v0
.end method

.method private bindRecentTitle(Landroid/view/View;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)V
    .locals 4
    .param p1, "root"    # Landroid/view/View;
    .param p2, "titleData"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .prologue
    const v3, 0x7f020122

    .line 324
    if-nez p2, :cond_0

    .line 325
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 340
    :goto_0
    return-void

    .line 327
    :cond_0
    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 329
    const v2, 0x7f0e0ad7

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 330
    .local v0, "image":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 331
    const v2, 0x7f0e0ad8

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 333
    .local v1, "text":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    iget-object v2, p2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 338
    iget-object v2, p2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic lambda$bindRecentTitle$5(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;
    .param p1, "titleData"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectTitle(Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)V

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->navigateToTitlePicker()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->navigateToTagPicker()V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 141
    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    if-nez p2, :cond_1

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->setSearchTerm(Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    monitor-enter v1

    .line 145
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->clear()V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->notifyDataSetChanged()V

    .line 147
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 150
    const/4 v0, 0x1

    .line 153
    :goto_0
    return v0

    .line 147
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 153
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 157
    const/4 v1, 0x0

    .line 159
    .local v1, "selectedTerm":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    monitor-enter v3

    .line 160
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v2, p3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 161
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    if-eqz v1, :cond_0

    .line 164
    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverSearchSelect(Ljava/lang/String;)V

    .line 165
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->setSearchTerm(Ljava/lang/String;)V

    .line 166
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 168
    :cond_0
    return-void

    .line 161
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;
    .param p1, "cardModel"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    .prologue
    .line 173
    const-string v0, "Clubs - Discover Search Navigate to Club"

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackNavigateToClub(Ljava/lang/String;J)V

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V

    .line 175
    return-void
.end method

.method private setStyledText(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "textView"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .param p2, "searchFor"    # Ljava/lang/String;
    .param p3, "criteriaString"    # Ljava/lang/String;

    .prologue
    .line 376
    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 377
    .local v0, "styledTitle":Landroid/text/SpannableString;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchTextAppearance:Landroid/text/style/TextAppearanceSpan;

    const/4 v2, 0x0

    .line 380
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x21

    .line 377
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 383
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 384
    return-void
.end method

.method private updateCounts()V
    .locals 10

    .prologue
    const v9, 0x7f0701c9

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 268
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTitlesCount:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTitlesCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 269
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTagsCount:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTagsCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 271
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f0706ea

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 272
    .local v1, "searchByTags":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTagsCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v4, v9, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 273
    .local v3, "tagsSelected":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchByTagsView:Landroid/view/View;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 275
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const v5, 0x7f0701ce

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 276
    .local v2, "searchByTitles":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTitlesCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v4, v9, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 277
    .local v0, "gamesSelected":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchByTitlesView:Landroid/view/View;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 278
    return-void
.end method

.method private updateCriteriaDetail(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;I)V
    .locals 7
    .param p1, "params"    # Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    .param p2, "numberResults"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 351
    if-ltz p2, :cond_1

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 353
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->criteriaDetail:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 355
    if-eq p2, v4, :cond_2

    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->RESULTS_FORMAT:Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/Object;

    .line 356
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 359
    .local v2, "resultsText":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->numResults:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 363
    .local v0, "criteriaHeadings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->query()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 364
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->query()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTitleNames()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 368
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTagNames()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 370
    const-string v3, ","

    invoke-static {v3, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 372
    .local v1, "criteriaString":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->criteriaText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070332

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->setStyledText(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    return-void

    .end local v0    # "criteriaHeadings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "criteriaString":Ljava/lang/String;
    .end local v2    # "resultsText":Ljava/lang/String;
    :cond_1
    move v3, v5

    .line 351
    goto :goto_0

    .line 356
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->SINGLE_RESULT:Ljava/lang/String;

    goto :goto_1
.end method

.method private updateEmptySearchSection()V
    .locals 0

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->updateRecentsSection()V

    .line 282
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->updateTagsSection()V

    .line 283
    return-void
.end method

.method private updatePopulatedSearchSection(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;)V
    .locals 2
    .param p1, "searchParams"    # Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    .prologue
    .line 343
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getCurrentSearchResults()Ljava/util/List;

    move-result-object v0

    .line 345
    .local v0, "searchResults":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->updateCriteriaDetail(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;I)V

    .line 346
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->addAll(Ljava/util/Collection;)V

    .line 347
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->notifyDataSetChanged()V

    .line 348
    return-void
.end method

.method private updateRecentsSection()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 286
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getRecentlyPlayedTitles()Ljava/util/List;

    move-result-object v1

    .line 288
    .local v1, "recentTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 289
    .local v0, "numRecents":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentsHeader:Landroid/view/View;

    if-lez v0, :cond_3

    move v2, v3

    :goto_0
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 290
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentGame1:Landroid/view/View;

    if-lez v0, :cond_4

    move v2, v3

    :goto_1
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 291
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentGame2:Landroid/view/View;

    if-le v0, v3, :cond_5

    move v2, v3

    :goto_2
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 292
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentGame3:Landroid/view/View;

    if-le v0, v6, :cond_6

    move v2, v3

    :goto_3
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 294
    if-lez v0, :cond_0

    .line 295
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentGame1:Landroid/view/View;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-direct {p0, v5, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->bindRecentTitle(Landroid/view/View;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)V

    .line 298
    :cond_0
    if-le v0, v3, :cond_1

    .line 299
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentGame2:Landroid/view/View;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-direct {p0, v4, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->bindRecentTitle(Landroid/view/View;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)V

    .line 302
    :cond_1
    if-le v0, v6, :cond_2

    .line 303
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->recentGame3:Landroid/view/View;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    invoke-direct {p0, v3, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->bindRecentTitle(Landroid/view/View;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)V

    .line 305
    :cond_2
    return-void

    :cond_3
    move v2, v4

    .line 289
    goto :goto_0

    :cond_4
    move v2, v4

    .line 290
    goto :goto_1

    :cond_5
    move v2, v4

    .line 291
    goto :goto_2

    :cond_6
    move v2, v4

    .line 292
    goto :goto_3
.end method

.method private updateSuggestions()V
    .locals 6

    .prologue
    .line 244
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInput:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "currentTerm":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 247
    const/4 v3, 0x0

    iput v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->suggestionsHashCode:I

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getCurrentSuggestions()Ljava/util/List;

    move-result-object v2

    .line 251
    .local v2, "suggestionsForCurrentTerm":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v1

    .line 253
    .local v1, "newSuggestionsHashCode":I
    iget v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->suggestionsHashCode:I

    if-eq v3, v1, :cond_0

    .line 254
    iput v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->suggestionsHashCode:I

    .line 256
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    monitor-enter v4

    .line 257
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->clear()V

    .line 258
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->addAll(Ljava/util/Collection;)V

    .line 261
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInputAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$SearchInputAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v3

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchInput:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3, v0, v5}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    .line 262
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private updateTagsSection()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 308
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getRandomSystemTags()Ljava/util/List;

    move-result-object v1

    .line 309
    .local v1, "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 311
    .local v0, "numTags":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->clear()V

    .line 313
    if-lez v0, :cond_0

    .line 314
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagsHeader:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 315
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagsLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->setVisibility(I)V

    .line 316
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagArrayAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 321
    :goto_0
    return-void

    .line 318
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagsHeader:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 319
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->tagsLayout:Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/HorizontalFlowLayout;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 211
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isBusy()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->updateLoadingIndicator(Z)V

    .line 212
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 213
    const/4 v1, 0x0

    .line 214
    .local v1, "shouldForceListUpdate":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    sget-object v5, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v5, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isBusy()Z

    move-result v2

    if-nez v2, :cond_1

    .line 215
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->updateSuggestions()V

    .line 216
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->updateCounts()V

    .line 217
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->clear()V

    .line 219
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getCurrentSearchParams()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    move-result-object v0

    .line 221
    .local v0, "searchParams":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->criteriaDetail:Landroid/view/View;

    if-eqz v0, :cond_2

    move v2, v3

    :goto_0
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 222
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultList:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_3

    move v2, v3

    :goto_1
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 223
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->emptyCriteriaView:Landroid/view/View;

    if-nez v0, :cond_4

    :goto_2
    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 225
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTags:Ljava/util/List;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTags()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->unsortedEqual(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 226
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTags()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->selectedTags:Ljava/util/List;

    .line 227
    const/4 v1, 0x1

    .line 230
    :cond_0
    if-nez v0, :cond_5

    .line 231
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->updateEmptySearchSection()V

    .line 236
    :goto_3
    if-eqz v1, :cond_1

    .line 238
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultList:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->searchResultListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 241
    .end local v0    # "searchParams":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    :cond_1
    return-void

    .restart local v0    # "searchParams":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    :cond_2
    move v2, v4

    .line 221
    goto :goto_0

    :cond_3
    move v2, v4

    .line 222
    goto :goto_1

    :cond_4
    move v3, v4

    .line 223
    goto :goto_2

    .line 233
    :cond_5
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;->updatePopulatedSearchSection(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;)V

    goto :goto_3
.end method
