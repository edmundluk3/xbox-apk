.class public Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PartyMemberIconListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PartyMemberIconViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;",
        ">;"
    }
.end annotation


# instance fields
.field gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0867
    .end annotation
.end field

.field private memberColor:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field rootView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0865
    .end annotation
.end field

.field squawkerView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0866
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    .line 91
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 92
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 93
    return-void
.end method

.method private renderMuted(Z)V
    .locals 3
    .param p1, "isMuted"    # Z

    .prologue
    .line 150
    new-instance v1, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v2}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 152
    .local v1, "squawkerRing":Landroid/graphics/drawable/ShapeDrawable;
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    .line 153
    .local v0, "color":I
    :goto_0
    invoke-virtual {v1}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 154
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->squawkerView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 155
    return-void

    .line 152
    .end local v0    # "color":I
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->memberColor:I

    goto :goto_0
.end method

.method private renderTalking(Z)V
    .locals 2
    .param p1, "isTalking"    # Z

    .prologue
    .line 144
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->squawkerView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 145
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->access$000()I

    move-result v1

    :goto_0
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->squawkerView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    return-void

    .line 145
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->access$100()I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;)V
    .locals 4
    .param p1, "partyMember"    # Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v3, 0x7f020125

    .line 97
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->userSummary()Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    move-result-object v0

    .line 98
    .local v0, "userSummary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->colors()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v1

    const/high16 v2, -0x1000000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->memberColor:I

    .line 100
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->gamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->displayPicUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 102
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->partyMember()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->onUpdate(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V

    .line 103
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 76
    check-cast p1, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->onBind(Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;)V

    return-void
.end method

.method public onUpdate(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V
    .locals 6
    .param p1, "partyMember"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 108
    .local v0, "contentDescription":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isHost()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070a34

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$1;->$SwitchMap$com$microsoft$xbox$xbservices$domain$party$PartyMember$MemberChatState:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->currentState()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 136
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->isBroadcasting()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070a17

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->rootView:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 141
    return-void

    .line 117
    :pswitch_0
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070a20

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 120
    :pswitch_1
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070a1c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 123
    :pswitch_2
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->renderTalking(Z)V

    .line 124
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->renderMuted(Z)V

    goto :goto_0

    .line 127
    :pswitch_3
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->renderTalking(Z)V

    .line 128
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->renderMuted(Z)V

    goto :goto_0

    .line 131
    :pswitch_4
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->renderTalking(Z)V

    .line 132
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter$PartyMemberIconViewHolder;->renderMuted(Z)V

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
