.class final synthetic Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;

.field private final arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final arg$3:Lcom/microsoft/xbox/domain/party/PartyInteractor;

.field private final arg$4:Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;->arg$1:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;->arg$3:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    iput-object p4, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;->arg$4:Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)Lio/reactivex/ObservableTransformer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;-><init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;)V

    return-object v0
.end method


# virtual methods
.method public apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;->arg$1:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;->arg$3:Lcom/microsoft/xbox/domain/party/PartyInteractor;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter$$Lambda$1;->arg$4:Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;->lambda$new$13(Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/party/PartyInteractor;Lcom/microsoft/xbox/presentation/party/PartyDetailsNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
