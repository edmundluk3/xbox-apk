.class public Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "GameAchievementComparisonListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final VIEWTYPE_COMPARISON_ITEM:I = 0x0

.field public static final VIEWTYPE_COUNT:I = 0x3

.field public static final VIEWTYPE_STATISTICS_COMPARISON_ITEM:I = 0x1

.field private static meProfileImageURL:Ljava/lang/String;

.field private static youProfileImageURL:Ljava/lang/String;


# instance fields
.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .param p3, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 42
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .line 43
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->youProfileImageURL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->meProfileImageURL:Ljava/lang/String;

    return-object v0
.end method

.method public static getItemViewType(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)I
    .locals 1
    .param p0, "item"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    .prologue
    .line 84
    instance-of v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x1

    .line 87
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->getItemViewType(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    .line 50
    .local v1, "item":Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    instance-of v2, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;

    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->inflateView(Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    .line 53
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;

    .line 54
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 56
    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;

    .end local v1    # "item":Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->updateUI(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V

    .line 58
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->getNarratorContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 70
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;
    :goto_0
    return-object p2

    .line 61
    .restart local v1    # "item":Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->inflateView(Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    .line 62
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;

    .line 63
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 65
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->updateUI(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V

    .line 67
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$GameAchievementComparisonItemViewHolder;->getNarratorContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x3

    return v0
.end method
