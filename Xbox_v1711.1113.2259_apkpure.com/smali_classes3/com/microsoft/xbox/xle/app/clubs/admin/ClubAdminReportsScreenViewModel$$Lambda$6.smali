.class final synthetic Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Action;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

.field private final arg$2:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

.field private final arg$3:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

.field private final arg$4:J


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;->arg$1:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;->arg$2:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;->arg$3:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    iput-wide p4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;->arg$4:J

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;J)Lio/reactivex/functions/Action;
    .locals 7

    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;J)V

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;->arg$1:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;->arg$2:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;->arg$3:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;->arg$4:J

    invoke-static {v0, v1, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->lambda$deleteReport$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;J)V

    return-void
.end method
