.class Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;
.super Ljava/lang/Object;
.source "BranchSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onConfigReceived([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

.field final synthetic val$_devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 340
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->val$_devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 343
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->val$_devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$002(Lcom/microsoft/xbox/xle/urc/net/BranchSession;[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .line 345
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$102(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Z)Z

    .line 346
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->val$_devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    if-eqz v3, :cond_4

    .line 347
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->val$_devices:[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_4

    aget-object v0, v3, v2

    .line 348
    .local v0, "d":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    if-eqz v0, :cond_0

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    if-nez v5, :cond_1

    .line 347
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 353
    :cond_1
    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    const-string v6, "stb"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 357
    :cond_2
    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    const-string v6, "btn.vol_up"

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    const-string v6, "btn.vol_down"

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->buttons:Ljava/util/Map;

    const-string v6, "btn.vol_mute"

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 358
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$102(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Z)Z

    .line 364
    .end local v0    # "d":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$200(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;

    .line 365
    .local v1, "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$1;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$000(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;->onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V

    goto :goto_1

    .line 367
    .end local v1    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
    :cond_5
    return-void
.end method
