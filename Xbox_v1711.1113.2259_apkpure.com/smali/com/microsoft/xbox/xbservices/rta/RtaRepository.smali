.class public final Lcom/microsoft/xbox/xbservices/rta/RtaRepository;
.super Ljava/lang/Object;
.source "RtaRepository.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final RTA_SUBSCRIBE_FORMAT:Ljava/lang/String; = "[1, %s, \"%s\"]"

.field private static final RTA_SUB_PROTOCOL:Ljava/lang/String; = "rta.xboxlive.com.V2"

.field private static final RTA_UNSUBSCRIBE_FORMAT:Ljava/lang/String; = "[2,%s,%s]"

.field private static final RTA_WEBSOCKET_ENDPOINT:Ljava/lang/String; = "https://rta.xboxlive.com/connect"

.field private static final TAG:Ljava/lang/String;

.field private static final XTOKEN_OK_HTTP:Ljava/lang/String; = "XTOKEN_OK_HTTP"


# instance fields
.field private currentSequenceNumber:I

.field private knownSubscriptions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

.field private rtaConnectionObservable:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation
.end field

.field rtaDataMapper:Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private sequenceToSubscriptionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private sequenceToUnsubscribeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private subscriptionsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private webSocket:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lokhttp3/OkHttpClient;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V
    .locals 6
    .param p1, "httpClient"    # Lokhttp3/OkHttpClient;
        .annotation runtime Ljavax/inject/Named;
            value = "XTOKEN_OK_HTTP"
        .end annotation
    .end param
    .param p2, "logger"    # Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .param p3, "telemetryProvider"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->currentSequenceNumber:I

    .line 74
    new-instance v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    const-string v2, "https://rta.xboxlive.com/connect"

    const-string/jumbo v3, "rta.xboxlive.com.V2"

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;-><init>(Lokhttp3/OkHttpClient;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;)Lio/reactivex/functions/Action;

    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->doOnComplete(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->rtaConnectionObservable:Lio/reactivex/Observable;

    .line 95
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;)V
    .locals 0

    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->clearSubscriptionMaps()V

    return-void
.end method

.method private declared-synchronized clearSubscriptionMaps()V
    .locals 1

    .prologue
    .line 178
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    monitor-exit p0

    return-void

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized handleSubscribeResponse(Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;)V
    .locals 4
    .param p1, "rtaResponse"    # Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->errorType()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;->Success:Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;

    if-ne v0, v1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :goto_1
    monitor-exit p0

    return-void

    .line 136
    :cond_0
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RTA SUBSCRIBE failed, removing subscription for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;->sequenceNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 142
    :cond_1
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->TAG:Ljava/lang/String;

    const-string v1, "Received RTA SUBSCRIBE for unknown request!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized handleUnsubscribeResponse(Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaUnsubscribeResponse;)V
    .locals 4
    .param p1, "rtaResponse"    # Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaUnsubscribeResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaUnsubscribeResponse;->sequenceNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 150
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaUnsubscribeResponse;->errorCode()I

    move-result v1

    if-nez v1, :cond_1

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaUnsubscribeResponse;->sequenceNumber()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 153
    .local v0, "unsubscribedTarget":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    .end local v0    # "unsubscribedTarget":Ljava/lang/String;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 157
    :cond_1
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received error from RTA UNSUBSCRIBE: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaUnsubscribeResponse;->errorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 160
    :cond_2
    :try_start_2
    sget-object v1, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->TAG:Ljava/lang/String;

    const-string v2, "Received RTA UNSUBSCRIBE for unknown sequence ID. Ignoring."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 84
    const-class v0, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;)Z
    .locals 1
    .param p0, "event"    # Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;->text()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/rta/RtaRepository;
    .param p1, "event"    # Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->rtaDataMapper:Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocketDataTypes$RxWebSocketStringMessageEvent;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/rta/RtaDataMapper;->apply(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/rta/RtaRepository;
    .param p1, "rtaResponse"    # Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    instance-of v0, p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;

    if-eqz v0, :cond_1

    .line 89
    check-cast p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;

    .end local p1    # "rtaResponse":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->handleSubscribeResponse(Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;)V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 90
    .restart local p1    # "rtaResponse":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;
    :cond_1
    instance-of v0, p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaUnsubscribeResponse;

    if-eqz v0, :cond_0

    .line 91
    check-cast p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaUnsubscribeResponse;

    .end local p1    # "rtaResponse":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->handleUnsubscribeResponse(Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaUnsubscribeResponse;)V

    goto :goto_0
.end method

.method static synthetic lambda$subscribeToRtaEvents$4(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xbservices/rta/RtaRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "subscriptionTarget"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    invoke-interface {p2}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;->subscriptionId()Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized sendSubscribeMessage(Ljava/lang/String;)V
    .locals 5
    .param p1, "subscriptionTarget"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 165
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 166
    sget-object v1, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sendSubscribeMessage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const-string v1, "[1, %s, \"%s\"]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->currentSequenceNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "joinString":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToSubscriptionMap:Ljava/util/Map;

    iget v2, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->currentSequenceNumber:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->currentSequenceNumber:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->currentSequenceNumber:I

    .line 174
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->sendData(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    monitor-exit p0

    return-void

    .line 165
    .end local v0    # "joinString":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public isConnected()Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized subscribeToRtaEvents(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .param p1, "subscriptionTarget"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->knownSubscriptions:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sendSubscribeMessage(Ljava/lang/String;)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->rtaConnectionObservable:Lio/reactivex/Observable;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xbservices/rta/RtaRepository;Ljava/lang/String;)Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 116
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 115
    monitor-exit p0

    return-object v0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized unsubscribeFromAllRtaEvents()V
    .locals 7

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->subscriptionsMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 121
    .local v0, "kvPair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    const-string v3, "[2,%s,%s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->currentSequenceNumber:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 122
    .local v1, "unsubscribeString":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->sequenceToUnsubscribeMap:Ljava/util/Map;

    iget v4, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->currentSequenceNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget v3, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->currentSequenceNumber:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->currentSequenceNumber:I

    .line 125
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;->webSocket:Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xbservices/toolkit/rx/RxWebSocket;->sendData(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 120
    .end local v0    # "kvPair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v1    # "unsubscribeString":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 127
    :cond_0
    monitor-exit p0

    return-void
.end method
