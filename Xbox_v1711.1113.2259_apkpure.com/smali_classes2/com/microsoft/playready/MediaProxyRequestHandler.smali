.class Lcom/microsoft/playready/MediaProxyRequestHandler;
.super Ljava/lang/Object;
.source "MediaProxyRequestHandler.java"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/MediaProxyRequestHandler$ElementHandlerBase;,
        Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;,
        Lcom/microsoft/playready/MediaProxyRequestHandler$KeyHandler;,
        Lcom/microsoft/playready/MediaProxyRequestHandler$MasterPlaylistHandler;,
        Lcom/microsoft/playready/MediaProxyRequestHandler$SegmentHandler;,
        Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaProxyRequestHandler"


# instance fields
.field private final mElementHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final mElementMapLock:Ljava/lang/Object;

.field private final mRootUri:Ljava/net/URI;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/playready/MediaProxy;)V
    .locals 2
    .param p1, "rootUri"    # Ljava/lang/String;
    .param p2, "masterPlaylistUri"    # Ljava/lang/String;
    .param p3, "mediaProxy"    # Lcom/microsoft/playready/MediaProxy;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementHandlers:Ljava/util/Map;

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementMapLock:Ljava/lang/Object;

    .line 73
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mRootUri:Ljava/net/URI;

    .line 81
    new-instance v0, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p3, p0, v1}, Lcom/microsoft/playready/MediaProxyRequestHandler$StreamPlaylistHandler;-><init>(Lcom/microsoft/playready/MediaProxy;Lcom/microsoft/playready/MediaProxyRequestHandler;I)V

    invoke-virtual {p0, p2, v0}, Lcom/microsoft/playready/MediaProxyRequestHandler;->register(Ljava/lang/String;Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;)V

    .line 82
    return-void
.end method

.method private getPathFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 89
    move-object v1, p1

    .line 91
    .local v1, "path":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 92
    .local v2, "tmp":Ljava/net/URI;
    invoke-virtual {v2}, Ljava/net/URI;->getPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 99
    .end local v2    # "tmp":Ljava/net/URI;
    :goto_0
    return-object v1

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Ljava/net/URISyntaxException;
    const-string v3, "MediaProxyRequestHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to properly parse URI: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public getRootUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mRootUri:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 10
    .param p1, "request"    # Lorg/apache/http/HttpRequest;
    .param p2, "response"    # Lorg/apache/http/HttpResponse;
    .param p3, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 124
    .local v3, "method":Ljava/lang/String;
    const-string v7, "GET"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "HEAD"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "POST"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 125
    new-instance v7, Lorg/apache/http/MethodNotSupportedException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " method not supported"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 128
    :cond_0
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v5

    .line 130
    .local v5, "target":Ljava/lang/String;
    instance-of v7, p1, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v7, :cond_1

    move-object v7, p1

    .line 131
    check-cast v7, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {v7}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 132
    .local v2, "entity":Lorg/apache/http/HttpEntity;
    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    .line 136
    .end local v2    # "entity":Lorg/apache/http/HttpEntity;
    :cond_1
    invoke-static {v5}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 138
    .local v4, "path":Ljava/lang/String;
    :try_start_0
    iget-object v8, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementMapLock:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :try_start_1
    iget-object v7, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementHandlers:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 143
    iget-object v7, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementHandlers:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;

    invoke-interface {v7, p1, p2, p3}, Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;->HandleElementRequest(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    .line 138
    :goto_0
    monitor-exit v8

    .line 173
    :goto_1
    return-void

    .line 149
    :cond_2
    const/16 v7, 0x194

    invoke-interface {p2, v7}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 151
    new-instance v6, Lorg/apache/http/entity/EntityTemplate;

    new-instance v7, Lcom/microsoft/playready/MediaProxyRequestHandler$1;

    invoke-direct {v7, p0}, Lcom/microsoft/playready/MediaProxyRequestHandler$1;-><init>(Lcom/microsoft/playready/MediaProxyRequestHandler;)V

    invoke-direct {v6, v7}, Lorg/apache/http/entity/EntityTemplate;-><init>(Lorg/apache/http/entity/ContentProducer;)V

    .line 156
    .local v6, "template":Lorg/apache/http/entity/EntityTemplate;
    const-string v7, "text/html; charset=UTF-8"

    invoke-virtual {v6, v7}, Lorg/apache/http/entity/EntityTemplate;->setContentType(Ljava/lang/String;)V

    .line 157
    invoke-interface {p2, v6}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    .line 138
    .end local v6    # "template":Lorg/apache/http/entity/EntityTemplate;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v7
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 160
    :catch_0
    move-exception v1

    .line 168
    .local v1, "e":Ljava/lang/Exception;
    const/16 v7, 0x1f4

    invoke-interface {p2, v7}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 169
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    const-string v7, ""

    invoke-direct {v0, v7}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 170
    .local v0, "body":Lorg/apache/http/entity/AbstractHttpEntity;
    const-string v7, "text/html"

    invoke-virtual {v0, v7}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 171
    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_1
.end method

.method public register(Ljava/lang/String;Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;)V
    .locals 3
    .param p1, "handlerRelativeUri"    # Ljava/lang/String;
    .param p2, "handler"    # Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;

    .prologue
    .line 103
    iget-object v2, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementMapLock:Ljava/lang/Object;

    monitor-enter v2

    .line 104
    :try_start_0
    invoke-direct {p0, p1}, Lcom/microsoft/playready/MediaProxyRequestHandler;->getPathFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "handlerUri":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementHandlers:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    monitor-exit v2

    .line 107
    return-void

    .line 103
    .end local v0    # "handlerUri":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public requestUnregister(Ljava/lang/String;)V
    .locals 4
    .param p1, "handlerRelativeUri"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-object v3, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementMapLock:Ljava/lang/Object;

    monitor-enter v3

    .line 111
    :try_start_0
    invoke-direct {p0, p1}, Lcom/microsoft/playready/MediaProxyRequestHandler;->getPathFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "handlerUri":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementHandlers:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    iget-object v2, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementHandlers:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;

    .line 115
    .local v0, "handler":Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;
    iget-object v2, p0, Lcom/microsoft/playready/MediaProxyRequestHandler;->mElementHandlers:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    invoke-interface {v0}, Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;->Unregester()V

    .line 110
    .end local v0    # "handler":Lcom/microsoft/playready/MediaProxyRequestHandler$IMediaProxyElementHandler;
    :cond_0
    monitor-exit v3

    .line 119
    return-void

    .line 110
    .end local v1    # "handlerUri":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
