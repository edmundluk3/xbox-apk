.class Lcom/microsoft/xbox/xle/epg/TvStreamer$16;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupMediaPlayer(Lcom/microsoft/playready/PRMediaPlayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 688
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$16;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 692
    const-string v1, "TvStreamer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "new video width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$16;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$1400(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/playready/PRMediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/playready/PRMediaPlayer;->getAspectRatio()F

    move-result v0

    .line 694
    .local v0, "aspectRatio":F
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 695
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$16;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$16;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$1500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$16;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$1500(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-static {v1, v0, v2, v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$1600(Lcom/microsoft/xbox/xle/epg/TvStreamer;FII)V

    .line 697
    :cond_0
    return-void
.end method
