.class public final Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload;
.super Ljava/lang/Object;
.source "HoverChatHeadImageDownload.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$GroupMessageHoverChatHeadImageDownloader;,
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$MessageHoverChatHeadImageDownloader;,
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$ClubHoverChatHeadImageDownloader;,
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;,
        Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloader;
    }
.end annotation


# static fields
.field public static final NO_IMAGE_AVAILABLE:Ljava/lang/String; = "no_image_available"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
