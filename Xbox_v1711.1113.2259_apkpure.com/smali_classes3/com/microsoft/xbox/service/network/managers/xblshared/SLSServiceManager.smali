.class public Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;
.super Ljava/lang/Object;
.source "SLSServiceManager.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;


# static fields
.field private static final MAX_LEADERBOARD_RESULTS:I = 0x19

.field private static SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String; = null

.field private static SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String; = null

.field private static final SKYPE_REGISTRATION_TOKEN_HEADER:Ljava/lang/String; = "Set-RegistrationToken"

.field private static final TAG:Ljava/lang/String; = "SLSServiceManager"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    const-string v0, "ClientInfo"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    .line 143
    const-string v0, "os=Android; osVer=not defined; proc=not defined; lcid=%s; deviceType=1; country=%s; clientName=XboxAppAndroid; clientVer=1.0"

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private buildFeedPostMessage(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3604
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager$2;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager$2;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->buildJsonBody(Lcom/microsoft/xbox/toolkit/gson/GsonUtil$JsonBodyBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V
    .locals 1
    .param p0, "statusAndStream"    # Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    .prologue
    .line 3811
    if-eqz p0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 3812
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3816
    :cond_0
    :goto_0
    return-void

    .line 3814
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private createEntityPostBody(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "locator"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2917
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager$1;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->buildJsonBody(Lcom/microsoft/xbox/toolkit/gson/GsonUtil$JsonBodyBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getGameClipsMaxPageSize()I
    .locals 2

    .prologue
    .line 1956
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager$3;->$SwitchMap$com$microsoft$xbox$toolkit$network$NetworkStats$NetworkQuality:[I

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->getCurrentQuality()Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1966
    const/16 v0, 0xf

    :goto_0
    return v0

    .line 1958
    :pswitch_0
    const/16 v0, 0x1e

    goto :goto_0

    .line 1960
    :pswitch_1
    const/16 v0, 0x2d

    goto :goto_0

    .line 1962
    :pswitch_2
    const/16 v0, 0x3c

    goto :goto_0

    .line 1956
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getLocaleRegion(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "locale"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2783
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2786
    const-string v1, "-"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2788
    .local v0, "localeData":[Ljava/lang/String;
    array-length v1, v0

    if-le v1, v2, :cond_1

    .line 2790
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    return-object v1

    .line 2783
    .end local v0    # "localeData":[Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 2793
    .restart local v0    # "localeData":[Ljava/lang/String;
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x1389

    const-string v4, "Cannot find country or region code from legal locale"

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;)V

    throw v1
.end method

.method private getProfileStatisticsInfoInternal(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .locals 8
    .param p1, "requestBody"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1757
    const-string v5, "SLSServiceManager"

    const-string v6, "getting user statistics info"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1758
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_3

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1759
    const/4 v3, 0x0

    .line 1760
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1762
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1763
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1764
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "2"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1765
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1767
    const/4 v2, 0x0

    .line 1768
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1771
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {p2, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 1773
    const/16 v5, 0xc8

    iget v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 1774
    iget-object v5, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 1777
    :cond_0
    if-eqz v2, :cond_1

    .line 1778
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 1789
    :cond_1
    if-eqz v4, :cond_2

    .line 1790
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 1794
    :cond_2
    if-nez v3, :cond_6

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_1
    invoke-static {p2, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1795
    return-object v3

    .line 1758
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_3
    const/4 v5, 0x0

    goto :goto_0

    .line 1781
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 1782
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get user statistics info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 1784
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1789
    :catchall_0
    move-exception v5

    if-eqz v4, :cond_4

    .line 1790
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_4
    throw v5

    .line 1786
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_2
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbf6

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1794
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_1
.end method

.method private getSkypeRegistrationTokenFromResponseHeaders([Lorg/apache/http/Header;)Ljava/lang/String;
    .locals 5
    .param p1, "headers"    # [Lorg/apache/http/Header;

    .prologue
    .line 4184
    if-eqz p1, :cond_1

    .line 4185
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 4186
    .local v0, "header":Lorg/apache/http/Header;
    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Set-RegistrationToken"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4187
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 4192
    .end local v0    # "header":Lorg/apache/http/Header;
    :goto_1
    return-object v1

    .line 4185
    .restart local v0    # "header":Lorg/apache/http/Header;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4192
    .end local v0    # "header":Lorg/apache/http/Header;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getTenDaysAgoUtcString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1485
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1486
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v3, 0x5

    const/16 v4, -0xa

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 1487
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1488
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1489
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Z"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1493
    .local v2, "gmtTenDaysAgo":Ljava/lang/String;
    return-object v2
.end method


# virtual methods
.method public SearchGamertag(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .locals 10
    .param p1, "gamertag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 511
    const-string v6, "SLSServiceManager"

    const-string v9, "getting gamertag search info"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_2

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 513
    const/4 v3, 0x0

    .line 514
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getGamertagSearchUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 515
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 517
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 518
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "x-xbl-contract-version"

    const-string v8, "2"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    const/4 v2, 0x0

    .line 522
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 525
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 527
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 528
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 538
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    :cond_0
    if-eqz v4, :cond_1

    .line 539
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 543
    :cond_1
    if-nez v3, :cond_5

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_1
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 544
    return-object v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_2
    move v6, v8

    .line 512
    goto :goto_0

    .line 530
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 531
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get gamertag search info with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_4

    .line 533
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 538
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_3

    .line 539
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_3
    throw v6

    .line 535
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbc6

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 543
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    :cond_5
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_1
.end method

.method public addFriendToShareIdentitySetting(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 329
    const-string v5, "SLSServiceManager"

    const-string v8, "Adding friends to share identity"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 331
    const/4 v2, 0x0

    .line 332
    .local v2, "result":Z
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getAddFriendsToShareIdentityUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 333
    .local v4, "url":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 334
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 335
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "4"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    const/4 v1, 0x0

    .line 339
    .local v1, "response":Ljava/lang/String;
    const/4 v3, 0x0

    .line 341
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v0, p2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 343
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_2

    const/16 v5, 0xcc

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_2

    .line 345
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbe1

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    :catchall_0
    move-exception v5

    .line 350
    if-eqz v3, :cond_0

    :try_start_1
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_0

    .line 351
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 353
    :cond_0
    :goto_1
    throw v5

    .end local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v1    # "response":Ljava/lang/String;
    .end local v2    # "result":Z
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    move v5, v7

    .line 330
    goto :goto_0

    .line 347
    .restart local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v1    # "response":Ljava/lang/String;
    .restart local v2    # "result":Z
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x1

    .line 350
    if-eqz v3, :cond_3

    :try_start_2
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 351
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 355
    :cond_3
    :goto_2
    return v2

    .line 352
    :catch_0
    move-exception v6

    goto :goto_1

    :catch_1
    move-exception v5

    goto :goto_2
.end method

.method public addStaticSLSHeaders(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 3758
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 3759
    .local v1, "rand":Ljava/util/Random;
    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    .line 3760
    .local v2, "randomLong":J
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%d"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3761
    .local v0, "correlationId":Ljava/lang/String;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Android/%d %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getDeviceModel()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3763
    .local v4, "userAgentValue":Ljava/lang/String;
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-ClientCorrelationId"

    invoke-direct {v5, v6, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3764
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-UserAgent"

    invoke-direct {v5, v6, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3765
    return-void
.end method

.method public addUserToFavoriteList(Ljava/lang/String;)Z
    .locals 10
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2445
    const-string v5, "SLSServiceManager"

    const-string v8, "putting user to favorite list"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2446
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_3

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2447
    const/4 v2, 0x0

    .line 2448
    .local v2, "result":Z
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileFavoriteListUrl()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    const-string v9, "add"

    aput-object v9, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2449
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2451
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2452
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2453
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2455
    const/4 v3, 0x0

    .line 2458
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 2460
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_0

    const/16 v5, 0xcc

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v5, v6, :cond_1

    .line 2461
    :cond_0
    const/4 v2, 0x1

    .line 2472
    :cond_1
    if-eqz v3, :cond_2

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 2473
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2478
    :cond_2
    :goto_1
    if-nez v2, :cond_6

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2479
    return v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_3
    move v5, v7

    .line 2446
    goto :goto_0

    .line 2463
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2464
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to put user on favorite list with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2465
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 2466
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2471
    :catchall_0
    move-exception v5

    .line 2472
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 2473
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2475
    :cond_4
    :goto_3
    throw v5

    .line 2468
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xf9a

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2478
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2474
    :catch_1
    move-exception v6

    goto :goto_3

    :catch_2
    move-exception v5

    goto :goto_1
.end method

.method public addUserToFollowingList(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    .locals 15
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 2523
    const-string v10, "SLSServiceManager"

    const-string v13, "putting user to following list"

    invoke-static {v10, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    sget-object v13, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v10, v13, :cond_3

    move v10, v11

    :goto_0
    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2525
    new-instance v5, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;-><init>()V

    .line 2526
    .local v5, "result":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    invoke-virtual {v5, v12}, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->setAddFollowingRequestStatus(Z)V

    .line 2527
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateProfileFollowingListUrl()Ljava/lang/String;

    move-result-object v13

    new-array v11, v11, [Ljava/lang/Object;

    const-string v14, "add"

    aput-object v14, v11, v12

    invoke-static {v10, v13, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2528
    .local v8, "url":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2530
    .local v4, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2531
    new-instance v10, Lorg/apache/http/message/BasicHeader;

    const-string v11, "Content-type"

    const-string v12, "application/json"

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2532
    new-instance v10, Lorg/apache/http/message/BasicHeader;

    const-string v11, "x-xbl-contract-version"

    const-string v12, "1"

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2534
    const/4 v7, 0x0

    .line 2537
    .local v7, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v8, v4, v0}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v7

    .line 2539
    const/16 v10, 0xc8

    iget v11, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v10, v11, :cond_0

    const/16 v10, 0xcc

    iget v11, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v10, v11, :cond_1

    .line 2540
    :cond_0
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->setAddFollowingRequestStatus(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2564
    :cond_1
    if-eqz v7, :cond_2

    :try_start_1
    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v10, :cond_2

    .line 2565
    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 2570
    :cond_2
    :goto_1
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->getAddFollowingRequestStatus()Z

    move-result v10

    if-nez v10, :cond_8

    sget-object v10, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v8, v10}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    move-object v6, v5

    .line 2571
    .end local v5    # "result":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    .local v6, "result":Ljava/lang/Object;
    :goto_3
    return-object v6

    .end local v4    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v6    # "result":Ljava/lang/Object;
    .end local v7    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v8    # "url":Ljava/lang/String;
    :cond_3
    move v10, v12

    .line 2524
    goto :goto_0

    .line 2542
    .restart local v4    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v5    # "result":Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;
    .restart local v7    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v8    # "url":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 2543
    .local v3, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v10, "SLSServiceManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "failed to put user on following list with exception "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544
    instance-of v10, v3, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v10, :cond_7

    .line 2545
    move-object v0, v3

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    move-object v9, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2546
    .local v9, "xle":Lcom/microsoft/xbox/toolkit/XLEException;
    if-eqz v9, :cond_6

    .line 2548
    :try_start_3
    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/XLEException;->getUserObject()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-class v11, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;

    move-object v5, v0

    .line 2550
    iget v10, v5, Lcom/microsoft/xbox/service/network/managers/AddFollowingUserResponseContainer$AddFollowingUserResponse;->code:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/16 v11, 0x404

    if-ne v10, v11, :cond_6

    .line 2564
    if-eqz v7, :cond_4

    :try_start_4
    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v10, :cond_4

    .line 2565
    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_4
    :goto_4
    move-object v6, v5

    .line 2551
    .restart local v6    # "result":Ljava/lang/Object;
    goto :goto_3

    .line 2553
    .end local v6    # "result":Ljava/lang/Object;
    :catch_1
    move-exception v2

    .line 2554
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v10, "addUserToFollowingList"

    const-string v11, "Error in deserializing response to AddFollowingUserResponse.class"

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2555
    check-cast v3, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v3    # "ex":Ljava/lang/Exception;
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2563
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v9    # "xle":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v10

    .line 2564
    if-eqz v7, :cond_5

    :try_start_6
    iget-object v11, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v11, :cond_5

    .line 2565
    iget-object v11, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 2567
    :cond_5
    :goto_5
    throw v10

    .line 2558
    .restart local v3    # "ex":Ljava/lang/Exception;
    .restart local v9    # "xle":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_6
    :try_start_7
    check-cast v3, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v3    # "ex":Ljava/lang/Exception;
    throw v3

    .line 2560
    .end local v9    # "xle":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v3    # "ex":Ljava/lang/Exception;
    :cond_7
    new-instance v10, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v12, 0xbc3

    invoke-direct {v10, v12, v13, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v10
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2570
    .end local v3    # "ex":Ljava/lang/Exception;
    :cond_8
    sget-object v10, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2566
    :catch_2
    move-exception v11

    goto :goto_5

    .restart local v3    # "ex":Ljava/lang/Exception;
    .restart local v9    # "xle":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_3
    move-exception v10

    goto :goto_4

    .end local v3    # "ex":Ljava/lang/Exception;
    .end local v9    # "xle":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_4
    move-exception v10

    goto/16 :goto_1
.end method

.method public addUserToNeverList(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2363
    const-string v6, "SLSServiceManager"

    const-string v9, "putting user to never list"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2364
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_3

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2365
    const/4 v3, 0x0

    .line 2366
    .local v3, "result":Z
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileNeverListUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2367
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2369
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2370
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2371
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "x-xbl-contract-version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2373
    const/4 v2, 0x0

    .line 2374
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2377
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1, p2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 2379
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 2380
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 2381
    const/4 v3, 0x1

    .line 2384
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2385
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "response returned - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2397
    :cond_1
    if-eqz v4, :cond_2

    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    .line 2398
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2403
    :cond_2
    :goto_1
    if-nez v3, :cond_6

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2404
    return v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Z
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_3
    move v6, v8

    .line 2364
    goto :goto_0

    .line 2388
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "result":Z
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2389
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to put user to never list with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2390
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 2391
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2396
    :catchall_0
    move-exception v6

    .line 2397
    if-eqz v4, :cond_4

    :try_start_3
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 2398
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2400
    :cond_4
    :goto_3
    throw v6

    .line 2393
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xf9c

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2403
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2399
    :catch_1
    move-exception v7

    goto :goto_3

    :catch_2
    move-exception v6

    goto :goto_1
.end method

.method public changeGamertag(Ljava/lang/String;ZLjava/lang/String;)Z
    .locals 14
    .param p1, "gamertag"    # Ljava/lang/String;
    .param p2, "preview"    # Z
    .param p3, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4197
    const-string v9, "SLSServiceManager"

    const-string v10, "attempting to change gamertag"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4198
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v9, v10, :cond_2

    const/4 v9, 0x1

    :goto_0
    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4200
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getChangeGamertagUrl()Ljava/lang/String;

    move-result-object v7

    .line 4201
    .local v7, "url":Ljava/lang/String;
    const/4 v6, 0x0

    .line 4202
    .local v6, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v5, 0x0

    .line 4204
    .local v5, "result":Z
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4205
    .local v4, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "Content-type"

    const-string v11, "application/json"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4206
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4209
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;

    move/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v2, p1, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    .line 4210
    .local v2, "body":Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v4, v9}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v6

    .line 4212
    const/16 v9, 0xc8

    iget v10, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v9, v10, :cond_0

    .line 4213
    const/4 v5, 0x1

    .line 4232
    :cond_0
    if-eqz v6, :cond_1

    :try_start_1
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_1

    .line 4233
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 4238
    .end local v2    # "body":Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;
    :cond_1
    :goto_1
    sget-object v9, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v7, v9}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 4239
    return v5

    .line 4198
    .end local v4    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v5    # "result":Z
    .end local v6    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v7    # "url":Ljava/lang/String;
    :cond_2
    const/4 v9, 0x0

    goto :goto_0

    .line 4215
    .restart local v4    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v5    # "result":Z
    .restart local v6    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v7    # "url":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 4216
    .local v3, "ex":Ljava/lang/Exception;
    :try_start_2
    instance-of v9, v3, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v9, :cond_5

    .line 4217
    move-object v0, v3

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    move-object v8, v0

    .line 4219
    .local v8, "xleException":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v10

    const-wide/16 v12, 0x3fc

    cmp-long v9, v10, v12

    if-nez v9, :cond_3

    .line 4220
    const-string v9, "SLSServiceManager"

    const-string v10, "Expected error code response, user has no free gamertag changes available"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4221
    const/4 v5, 0x0

    .line 4232
    if-eqz v6, :cond_1

    :try_start_3
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_1

    .line 4233
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 4234
    :catch_1
    move-exception v9

    goto :goto_1

    .line 4223
    :cond_3
    :try_start_4
    const-string v9, "SLSServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "failed to change gamertag "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4224
    throw v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4231
    .end local v3    # "ex":Ljava/lang/Exception;
    .end local v8    # "xleException":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v9

    .line 4232
    if-eqz v6, :cond_4

    :try_start_5
    iget-object v10, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v10, :cond_4

    .line 4233
    iget-object v10, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 4235
    :cond_4
    :goto_2
    throw v9

    .line 4227
    .restart local v3    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_6
    const-string v9, "SLSServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "failed to change gamertag "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4228
    new-instance v9, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0x1

    invoke-direct {v9, v10, v11, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 4234
    .end local v3    # "ex":Ljava/lang/Exception;
    :catch_2
    move-exception v10

    goto :goto_2

    .restart local v2    # "body":Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;
    :catch_3
    move-exception v9

    goto/16 :goto_1
.end method

.method public checkGamertagAvailability(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 14
    .param p1, "gamertag"    # Ljava/lang/String;
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 4244
    const-string v10, "SLSServiceManager"

    const-string v11, "attempting to check gamertag availability"

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4245
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v10, v11, :cond_2

    :goto_0
    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4247
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getCheckGamertagAvailabilityUrl()Ljava/lang/String;

    move-result-object v7

    .line 4248
    .local v7, "url":Ljava/lang/String;
    const/4 v6, 0x0

    .line 4249
    .local v6, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v5, 0x0

    .line 4251
    .local v5, "result":Z
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4252
    .local v4, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "Content-type"

    const-string v11, "application/json"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4253
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4256
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;

    const/4 v9, 0x1

    move-object/from16 v0, p2

    invoke-direct {v2, p1, v9, v0}, Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    .line 4257
    .local v2, "body":Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v4, v9}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v6

    .line 4259
    const/16 v9, 0xc8

    iget v10, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v9, v10, :cond_0

    .line 4260
    const/4 v5, 0x1

    .line 4279
    :cond_0
    if-eqz v6, :cond_1

    :try_start_1
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_1

    .line 4280
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 4285
    .end local v2    # "body":Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;
    :cond_1
    :goto_1
    sget-object v9, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v7, v9}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 4286
    return v5

    .line 4245
    .end local v4    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v5    # "result":Z
    .end local v6    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v7    # "url":Ljava/lang/String;
    :cond_2
    const/4 v9, 0x0

    goto :goto_0

    .line 4262
    .restart local v4    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v5    # "result":Z
    .restart local v6    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v7    # "url":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 4263
    .local v3, "ex":Ljava/lang/Exception;
    :try_start_2
    instance-of v9, v3, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v9, :cond_5

    .line 4264
    move-object v0, v3

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    move-object v8, v0

    .line 4266
    .local v8, "xleException":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v10

    const-wide/16 v12, 0x16

    cmp-long v9, v10, v12

    if-nez v9, :cond_3

    .line 4267
    const-string v9, "SLSServiceManager"

    const-string v10, "Expected error code response, requested gamertag is not available"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4268
    const/4 v5, 0x0

    .line 4279
    if-eqz v6, :cond_1

    :try_start_3
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_1

    .line 4280
    iget-object v9, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 4281
    :catch_1
    move-exception v9

    goto :goto_1

    .line 4270
    :cond_3
    :try_start_4
    const-string v9, "SLSServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "failed to check gamertag availability"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4271
    throw v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4278
    .end local v3    # "ex":Ljava/lang/Exception;
    .end local v8    # "xleException":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v9

    .line 4279
    if-eqz v6, :cond_4

    :try_start_5
    iget-object v10, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v10, :cond_4

    .line 4280
    iget-object v10, v6, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 4282
    :cond_4
    :goto_2
    throw v9

    .line 4274
    .restart local v3    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_6
    const-string v9, "SLSServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "failed to check gamertag availability"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4275
    new-instance v9, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0x1

    invoke-direct {v9, v10, v11, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 4281
    .end local v3    # "ex":Ljava/lang/Exception;
    :catch_2
    move-exception v10

    goto :goto_2

    .restart local v2    # "body":Lcom/microsoft/xbox/service/network/managers/ChangeGamertagRequest;
    :catch_3
    move-exception v9

    goto/16 :goto_1
.end method

.method public deleteConversation(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "meXuid"    # Ljava/lang/String;
    .param p2, "targetXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 3131
    const-string v4, "SLSServiceManager"

    const-string v7, "deletes user\'s conversation"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3132
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v7, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3133
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    move v4, v5

    :goto_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3134
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v5

    :goto_2
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3135
    const/4 v2, 0x0

    .line 3136
    .local v2, "result":Z
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getConversationsDetailUrlFormat()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object p1, v8, v6

    aput-object p2, v8, v5

    invoke-static {v4, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3138
    .local v3, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3140
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3141
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3142
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "1"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3145
    :try_start_0
    invoke-static {v3, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->delete(Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 3155
    return v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    .end local v3    # "url":Ljava/lang/String;
    :cond_0
    move v4, v6

    .line 3132
    goto :goto_0

    :cond_1
    move v4, v6

    .line 3133
    goto :goto_1

    :cond_2
    move v4, v6

    .line 3134
    goto :goto_2

    .line 3146
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    .restart local v3    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3147
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to delete conversation "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3148
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_3

    .line 3149
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 3152
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbed

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
.end method

.method public deleteMessage(Ljava/lang/String;J)Z
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "messageId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2016
    const-string v4, "SLSServiceManager"

    const-string v7, "getting user\'s message detail info"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2017
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v7, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2018
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    move v4, v5

    :goto_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2019
    const-wide/16 v8, 0x0

    cmp-long v4, p2, v8

    if-ltz v4, :cond_2

    move v4, v5

    :goto_2
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2020
    const/4 v2, 0x0

    .line 2021
    .local v2, "result":Z
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMessageDetailUrlFormat()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object p1, v8, v6

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v8, v5

    invoke-static {v4, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2023
    .local v3, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2025
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2026
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2027
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "3"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2030
    :try_start_0
    invoke-static {v3, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->delete(Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2040
    if-nez v2, :cond_4

    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2041
    return v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    .end local v3    # "url":Ljava/lang/String;
    :cond_0
    move v4, v6

    .line 2017
    goto :goto_0

    :cond_1
    move v4, v6

    .line 2018
    goto :goto_1

    :cond_2
    move v4, v6

    .line 2019
    goto :goto_2

    .line 2031
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    .restart local v3    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2032
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to delete message "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2033
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_3

    .line 2034
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 2036
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbed

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4

    .line 2040
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3
.end method

.method public deleteSkypeConversation(Ljava/lang/String;)Z
    .locals 13
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2114
    const-string v5, "SLSServiceManager"

    const-string v8, "deletes user\'s conversation"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2115
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2116
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2117
    const/4 v2, 0x0

    .line 2118
    .local v2, "result":Z
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeSendMessageUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v9, v6, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2120
    .local v4, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 2122
    .local v1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-client-type"

    const-string v9, "XboxApp"

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2123
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept"

    const-string v9, "application/json"

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2124
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-contract-version"

    const-string v9, "3"

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2125
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-client-name"

    const-string v9, "Smartglass Android"

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2126
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    sget-object v8, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v10, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v7

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v6

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2128
    sget-object v5, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v5}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2129
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v8, "RegistrationToken"

    sget-object v9, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v9}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2135
    :goto_2
    const/4 v5, 0x0

    :try_start_0
    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->deleteWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 2136
    .local v3, "stream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    if-eqz v3, :cond_0

    .line 2137
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2138
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    iget-object v8, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 2141
    :cond_0
    if-eqz v3, :cond_6

    iget v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v8, 0xc8

    if-eq v5, v8, :cond_1

    iget v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v8, 0xcc

    if-ne v5, v8, :cond_5

    :cond_1
    move v2, v6

    .line 2151
    :goto_3
    return v2

    .end local v1    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    .end local v3    # "stream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 2115
    goto/16 :goto_0

    :cond_3
    move v5, v7

    .line 2116
    goto/16 :goto_1

    .line 2131
    .restart local v1    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    .restart local v4    # "url":Ljava/lang/String;
    :cond_4
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Cookie"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "skypetoken=%s"

    new-array v11, v6, [Ljava/lang/Object;

    sget-object v12, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v12}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getToken()Lio/reactivex/Single;

    move-result-object v12

    invoke-virtual {v12}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v11, v7

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .restart local v3    # "stream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_5
    move v2, v7

    .line 2141
    goto :goto_3

    :cond_6
    move v2, v7

    goto :goto_3

    .line 2142
    .end local v3    # "stream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 2143
    .local v0, "ex":Ljava/lang/Exception;
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to delete conversation "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2144
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_7

    .line 2145
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 2148
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_7
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbed

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
.end method

.method public deleteSkypeMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 14
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "messageId"    # Ljava/lang/String;
    .param p3, "clientMessageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2046
    const-string v7, "SLSServiceManager"

    const-string v8, "deleting skype message"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2047
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v8, :cond_5

    const/4 v7, 0x1

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2048
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    const/4 v7, 0x1

    :goto_1
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2049
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    const/4 v7, 0x1

    :goto_2
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2050
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    const/4 v7, 0x1

    :goto_3
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2051
    const/4 v4, 0x0

    .line 2052
    .local v4, "result":Z
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeDeleteMessageUrlFormat()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    const/4 v10, 0x1

    aput-object p2, v9, v10

    const/4 v10, 0x2

    aput-object p3, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2054
    .local v6, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 2056
    .local v1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-client-type"

    const-string v9, "XboxApp"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2057
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2058
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-contract-version"

    const-string v9, "3"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2059
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-client-name"

    const-string v9, "Smartglass Android"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2060
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    sget-object v8, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v10, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2062
    sget-object v7, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v7}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 2063
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "RegistrationToken"

    sget-object v9, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v9}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2068
    :goto_4
    const/4 v3, 0x0

    .line 2069
    .local v3, "response":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2072
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v7, 0x0

    :try_start_0
    invoke-static {v6, v1, v7}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->deleteWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 2074
    if-eqz v5, :cond_0

    .line 2075
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2076
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 2080
    :cond_0
    const/16 v7, 0xc8

    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v7, v8, :cond_2

    .line 2081
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 2082
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getSkypeRegistrationTokenFromResponseHeaders([Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v2

    .line 2083
    .local v2, "registrationToken":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2084
    sget-object v7, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v7, v2}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 2086
    :cond_1
    const/4 v4, 0x1

    .line 2089
    .end local v2    # "registrationToken":Ljava/lang/String;
    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 2090
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "response returned - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2102
    :cond_3
    if-eqz v5, :cond_4

    :try_start_1
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 2103
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2108
    :cond_4
    :goto_5
    if-nez v4, :cond_c

    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_6
    invoke-static {v6, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2109
    return v4

    .line 2047
    .end local v1    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .end local v3    # "response":Ljava/lang/String;
    .end local v4    # "result":Z
    .end local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v6    # "url":Ljava/lang/String;
    :cond_5
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 2048
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 2049
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 2050
    :cond_8
    const/4 v7, 0x0

    goto/16 :goto_3

    .line 2065
    .restart local v1    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .restart local v4    # "result":Z
    .restart local v6    # "url":Ljava/lang/String;
    :cond_9
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Cookie"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "skypetoken=%s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    sget-object v13, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v13}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getToken()Lio/reactivex/Single;

    move-result-object v13

    invoke-virtual {v13}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 2093
    .restart local v3    # "response":Ljava/lang/String;
    .restart local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 2094
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to send skype message with exception "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2095
    instance-of v7, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v7, :cond_b

    .line 2096
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2101
    :catchall_0
    move-exception v7

    .line 2102
    if-eqz v5, :cond_a

    :try_start_3
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_a

    .line 2103
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2105
    :cond_a
    :goto_7
    throw v7

    .line 2098
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_b
    :try_start_4
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbec

    invoke-direct {v7, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2108
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_c
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_6

    .line 2104
    :catch_1
    move-exception v8

    goto :goto_7

    :catch_2
    move-exception v7

    goto :goto_5
.end method

.method public editFirstName(Ljava/lang/String;)Z
    .locals 8
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 275
    const-string v4, "SLSServiceManager"

    const-string v5, "Edit first name"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v5, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 277
    const/4 v1, 0x0

    .line 278
    .local v1, "result":Z
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEditFirstNameSettingUrlFormat()Ljava/lang/String;

    move-result-object v3

    .line 279
    .local v3, "url":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 280
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 281
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "3"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    const/4 v2, 0x0

    .line 285
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v3, v0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 286
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_2

    const/16 v4, 0xc9

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_2

    .line 288
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbe1

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    :catchall_0
    move-exception v4

    .line 293
    if-eqz v2, :cond_0

    :try_start_1
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_0

    .line 294
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 296
    :cond_0
    :goto_1
    throw v4

    .line 276
    .end local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v1    # "result":Z
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 290
    .restart local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v1    # "result":Z
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x1

    .line 293
    if-eqz v2, :cond_3

    :try_start_2
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_3

    .line 294
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 298
    :cond_3
    :goto_2
    return v1

    .line 295
    :catch_0
    move-exception v5

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method public editLastName(Ljava/lang/String;)Z
    .locals 8
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 302
    const-string v4, "SLSServiceManager"

    const-string v5, "Edit last name"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v5, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 304
    const/4 v1, 0x0

    .line 305
    .local v1, "result":Z
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEditLastNameSettingUrlFormat()Ljava/lang/String;

    move-result-object v3

    .line 306
    .local v3, "url":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 307
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 308
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "3"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    const/4 v2, 0x0

    .line 312
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v3, v0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 313
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_2

    const/16 v4, 0xc9

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_2

    .line 315
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbe1

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    :catchall_0
    move-exception v4

    .line 320
    if-eqz v2, :cond_0

    :try_start_1
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_0

    .line 321
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 323
    :cond_0
    :goto_1
    throw v4

    .line 303
    .end local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v1    # "result":Z
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 317
    .restart local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v1    # "result":Z
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x1

    .line 320
    if-eqz v2, :cond_3

    :try_start_2
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_3

    .line 321
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 325
    :cond_3
    :goto_2
    return v1

    .line 322
    :catch_0
    move-exception v5

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method public genericDeleteWithUri(Ljava/lang/String;)Z
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1727
    const-string v3, "SLSServiceManager"

    const-string v6, "deleting item"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1728
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v3, v6, :cond_0

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1729
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1730
    const/4 v2, 0x0

    .line 1732
    .local v2, "result":Z
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1734
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1735
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Content-type"

    const-string v5, "application/json"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1738
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->delete(Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1748
    return v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    :cond_0
    move v3, v5

    .line 1728
    goto :goto_0

    :cond_1
    move v4, v5

    .line 1729
    goto :goto_1

    .line 1739
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    :catch_0
    move-exception v0

    .line 1740
    .local v0, "ex":Ljava/lang/Exception;
    const-string v3, "SLSServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to delete item "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1741
    instance-of v3, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v3, :cond_2

    .line 1742
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 1744
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_2
    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0xc88

    invoke-direct {v3, v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v3
.end method

.method public getAchievementDetailInfo(Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "achievementId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1972
    const-string v5, "SLSServiceManager"

    const-string v6, "getting user\'s title detail info"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1973
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 1974
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1975
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1977
    const/4 v2, 0x0

    .line 1978
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getAchievementDetailUrlFormat()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    const/4 v8, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1980
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1982
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1983
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1984
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "4"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1985
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1987
    const/4 v3, 0x0

    .line 1990
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1992
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 1993
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2004
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 2005
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2010
    :cond_1
    :goto_0
    if-nez v2, :cond_4

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_1
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2011
    return-object v2

    .line 1995
    :catch_0
    move-exception v0

    .line 1996
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get user\'s title detail info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1997
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_3

    .line 1998
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2003
    :catchall_0
    move-exception v5

    .line 2004
    if-eqz v3, :cond_2

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    .line 2005
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2007
    :cond_2
    :goto_2
    throw v5

    .line 2000
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbdc

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2010
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_1

    .line 2006
    :catch_1
    move-exception v6

    goto :goto_2

    :catch_2
    move-exception v5

    goto :goto_0
.end method

.method public getAchievements(JI)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 9
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "number"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x1

    .line 4452
    const-string v3, "SLSServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAchievements(xuid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4454
    invoke-static {v6, v7, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 4455
    int-to-long v4, p3

    invoke-static {v6, v7, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 4456
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 4458
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4459
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "x-xbl-contract-version"

    const-string v5, "2"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4460
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Content-type"

    const-string v5, "application/json"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4461
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4462
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 4463
    .local v1, "legalLocale":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 4464
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Accept-Language"

    invoke-direct {v3, v4, v1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4467
    :cond_0
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getGameProgressXboxOneUnlockedAchievementsUrlFormat()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 4469
    .local v2, "url":Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    .line 4468
    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    return-object v3
.end method

.method public getActivityAlertSummaryInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;
    .locals 9
    .param p1, "meXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2683
    const-string v5, "SLSServiceManager"

    const-string v8, "get the activity slert summary info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2684
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2685
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2686
    const/4 v3, 0x0

    .line 2687
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 2688
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getActivityAlertSummaryUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2690
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2692
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2693
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2694
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2697
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 2698
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 2699
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2711
    :cond_0
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    move v5, v7

    .line 2684
    goto :goto_0

    :cond_2
    move v5, v7

    .line 2685
    goto :goto_1

    .line 2702
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/AlertActivitySummaryResultContainer$ActivityAlertSummaryResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2703
    .local v0, "ex":Ljava/lang/Exception;
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get activity alert summary"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2704
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_3

    .line 2705
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 2708
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbba

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
.end method

.method public getBatchFeedItems(Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 13
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            max = 0x32L
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3770
    .local p1, "locators":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "SLSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBatchFeedItems: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3771
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 3772
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 3773
    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x32

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 3775
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getBatchPreviewUrl()Ljava/lang/String;

    move-result-object v12

    .line 3777
    .local v12, "url":Ljava/lang/String;
    const/4 v10, 0x0

    .line 3779
    .local v10, "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 3781
    .local v9, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3782
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Content-type"

    const-string v4, "application/json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3783
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3784
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "x-xbl-contract-version"

    const-string v4, "13"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3787
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/ActivityPreviewBatchRequest;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/service/network/managers/ActivityPreviewBatchRequest;-><init>(Ljava/util/List;)V

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v9, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v11

    .line 3790
    .local v11, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/16 v2, 0xc8

    :try_start_1
    iget v3, v11, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v2, v3, :cond_0

    .line 3791
    iget-object v2, v11, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJsonWithDateAdapter(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-object v10, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3794
    :cond_0
    :try_start_2
    iget-object v2, v11, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v2, :cond_1

    .line 3795
    iget-object v2, v11, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 3806
    :cond_1
    return-object v10

    .line 3794
    :catchall_0
    move-exception v2

    iget-object v3, v11, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v3, :cond_2

    .line 3795
    iget-object v3, v11, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v2
    :try_end_2
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 3798
    .end local v11    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v8

    .line 3799
    .local v8, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v2, "SLSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to get preview batch "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3800
    throw v8

    .line 3801
    .end local v8    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_1
    move-exception v8

    .line 3802
    .local v8, "ex":Ljava/lang/Exception;
    const-string v2, "SLSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to get preview batch "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3803
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x264e

    invoke-direct {v2, v4, v5, v8}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v2
.end method

.method public getClubActivityFeed(JLjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 11
    .param p1, "clubId"    # J
    .param p3, "continuationToken"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3717
    const-wide/16 v8, 0x0

    cmp-long v5, p1, v8

    if-lez v5, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 3718
    const-string v5, "SLSServiceManager"

    const-string v8, "getClubActivityFeed"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3720
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 3722
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getClubActivityFeedUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v9, v6, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v7

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3723
    .local v4, "url":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3724
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "&contToken=%s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p3, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3726
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3728
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3729
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3730
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3731
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "13"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3733
    const/4 v3, 0x0

    .line 3734
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 3737
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3738
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_1

    .line 3739
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 3748
    :cond_1
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    .line 3751
    if-nez v2, :cond_3

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_1
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3753
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 3717
    goto/16 :goto_0

    .line 3741
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3742
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get club activity info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3743
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3748
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v5

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    throw v5

    .line 3744
    :catch_1
    move-exception v0

    .line 3745
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get club activity info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3746
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xc85

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3751
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_1
.end method

.method public getCommentAlertInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    .locals 9
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 934
    const-string v5, "SLSServiceManager"

    const-string v8, "getting comment alert info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 936
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 937
    const/4 v2, 0x0

    .line 939
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getCommentsAlertInfoUrl()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 941
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 943
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 944
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 945
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 946
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 948
    const/4 v3, 0x0

    .line 951
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 953
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 954
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 966
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 967
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 972
    :cond_1
    :goto_2
    if-nez v2, :cond_6

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 973
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 935
    goto :goto_0

    :cond_3
    move v5, v7

    .line 936
    goto :goto_1

    .line 956
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$CommentAlertResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 957
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get comment alert info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 958
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 959
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 965
    :catchall_0
    move-exception v5

    .line 966
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 967
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 969
    :cond_4
    :goto_4
    throw v5

    .line 962
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbc0

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 972
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3

    .line 968
    :catch_1
    move-exception v6

    goto :goto_4

    :catch_2
    move-exception v5

    goto :goto_2
.end method

.method public getConversationDetail(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;
    .locals 11
    .param p1, "meXuid"    # Ljava/lang/String;
    .param p2, "targetXuid"    # Ljava/lang/String;
    .param p3, "startDate"    # Ljava/util/Date;
    .param p4, "endDate"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3083
    const-string v6, "SLSServiceManager"

    const-string v7, "get the user conversation details"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3084
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v7, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3085
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    const/4 v6, 0x1

    :goto_1
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3086
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3087
    const/4 v4, 0x0

    .line 3088
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 3089
    .local v2, "response":Ljava/lang/String;
    const/4 v3, 0x0

    .line 3091
    .local v3, "result":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getConversationsDetailUrlFormat()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    aput-object p2, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 3093
    .local v5, "url":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 3094
    if-nez p4, :cond_5

    .line 3095
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getConversationsDetailAfterFormat()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->dateToUrlFormat(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 3101
    :cond_0
    :goto_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3103
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3104
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3105
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "x-xbl-contract-version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3108
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 3109
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_1

    .line 3110
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 3112
    if-eqz v2, :cond_1

    .line 3113
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 3126
    :cond_1
    return-object v3

    .line 3084
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 3085
    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 3086
    :cond_4
    const/4 v6, 0x0

    goto :goto_2

    .line 3097
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationResult;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :cond_5
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getConversationsDetailBetweenFormat()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->dateToUrlFormat(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->dateToUrlFormat(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 3117
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :catch_0
    move-exception v0

    .line 3118
    .local v0, "ex":Ljava/lang/Exception;
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get conversation details"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3119
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_6

    .line 3120
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 3123
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbeb

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
.end method

.method public getConversationsList(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    .locals 10
    .param p1, "meXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2933
    const-string v6, "SLSServiceManager"

    const-string v9, "get the user conversations list"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2934
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_1

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2935
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    move v6, v7

    :goto_1
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2936
    const/4 v4, 0x0

    .line 2937
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 2938
    .local v2, "response":Ljava/lang/String;
    const/4 v3, 0x0

    .line 2939
    .local v3, "result":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getConversationsListUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2941
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2943
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2944
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2945
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "x-xbl-contract-version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2948
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 2949
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 2950
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 2952
    if-eqz v2, :cond_0

    .line 2953
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2966
    :cond_0
    return-object v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_1
    move v6, v8

    .line 2934
    goto :goto_0

    :cond_2
    move v6, v8

    .line 2935
    goto :goto_1

    .line 2957
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2958
    .local v0, "ex":Ljava/lang/Exception;
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get conversation list"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2959
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_3

    .line 2960
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 2963
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbeb

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
.end method

.method public getEntity(Ljava/lang/String;)Lcom/google/gson/JsonElement;
    .locals 10
    .param p1, "locator"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2874
    const-string v7, "SLSServiceManager"

    const-string v8, "getting entity"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2875
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v8, :cond_2

    const/4 v7, 0x1

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2877
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getPreviewUrl()Ljava/lang/String;

    move-result-object v6

    .line 2879
    .local v6, "url":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2881
    .local v4, "result":Lcom/google/gson/JsonElement;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2883
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2884
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Content-type"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2885
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2886
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-contract-version"

    const-string v9, "13"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2889
    :try_start_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->createEntityPostBody(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v1, v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 2891
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/16 v7, 0xc8

    :try_start_1
    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v7, v8, :cond_0

    .line 2892
    new-instance v2, Lcom/google/gson/JsonParser;

    invoke-direct {v2}, Lcom/google/gson/JsonParser;-><init>()V

    .line 2893
    .local v2, "parser":Lcom/google/gson/JsonParser;
    new-instance v3, Ljava/io/InputStreamReader;

    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-direct {v3, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2895
    .local v3, "reader":Ljava/io/InputStreamReader;
    :try_start_2
    invoke-virtual {v2, v3}, Lcom/google/gson/JsonParser;->parse(Ljava/io/Reader;)Lcom/google/gson/JsonElement;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 2897
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2901
    .end local v2    # "parser":Lcom/google/gson/JsonParser;
    .end local v3    # "reader":Ljava/io/InputStreamReader;
    :cond_0
    :try_start_4
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_1

    .line 2902
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 2913
    :cond_1
    return-object v4

    .line 2875
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v4    # "result":Lcom/google/gson/JsonElement;
    .end local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v6    # "url":Ljava/lang/String;
    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    .line 2897
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "parser":Lcom/google/gson/JsonParser;
    .restart local v3    # "reader":Ljava/io/InputStreamReader;
    .restart local v4    # "result":Lcom/google/gson/JsonElement;
    .restart local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v6    # "url":Ljava/lang/String;
    :catchall_0
    move-exception v7

    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    throw v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2901
    .end local v2    # "parser":Lcom/google/gson/JsonParser;
    .end local v3    # "reader":Ljava/io/InputStreamReader;
    :catchall_1
    move-exception v7

    :try_start_6
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_3

    .line 2902
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    :cond_3
    throw v7
    :try_end_6
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 2905
    .end local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 2906
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get entity "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2907
    throw v0

    .line 2908
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_1
    move-exception v0

    .line 2909
    .local v0, "ex":Ljava/lang/Exception;
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get entity "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2910
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbef

    invoke-direct {v7, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
.end method

.method public getFamilySettings(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    .locals 9
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2835
    const-string v5, "SLSServiceManager"

    const-string v8, "getting family settings"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2836
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2837
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getFamilyAccountUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2838
    .local v4, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2840
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    const/4 v3, 0x0

    .line 2842
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2844
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2845
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2846
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "3"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2849
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 2851
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 2852
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v6, Lcom/microsoft/xbox/service/network/managers/FamilySettings;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    check-cast v2, Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2863
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 2864
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2869
    :cond_1
    :goto_1
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 2836
    goto :goto_0

    .line 2854
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2855
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get family settings "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2856
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_4

    .line 2857
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2862
    :catchall_0
    move-exception v5

    .line 2863
    if-eqz v3, :cond_3

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 2864
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2866
    :cond_3
    :goto_2
    throw v5

    .line 2859
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbc8

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2865
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    goto :goto_2

    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/FamilySettings;
    :catch_2
    move-exception v5

    goto :goto_1
.end method

.method public getFeedItemActions(Ljava/lang/String;J)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    .locals 8
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "errorCode"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3193
    const-string v4, "SLSServiceManager"

    const-string v5, "getFeedItemActions"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3194
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v5, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3196
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3198
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3199
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3200
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3201
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "3"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3203
    const/4 v3, 0x0

    .line 3204
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 3207
    .local v2, "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    :try_start_0
    invoke-static {p1, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3208
    const/16 v4, 0xc8

    iget v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v4, v5, :cond_0

    .line 3209
    iget-object v4, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v5, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;

    const-class v6, Ljava/util/Date;

    new-instance v7, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;

    invoke-direct {v7}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;-><init>()V

    invoke-static {v4, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    check-cast v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3218
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    :cond_0
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    .line 3221
    return-object v2

    .line 3194
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 3211
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 3212
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to get FeedItemActionResult "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3213
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3218
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v4

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    throw v4

    .line 3214
    :catch_1
    move-exception v0

    .line 3215
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to get FeedItemActionResult "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3216
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-direct {v4, p2, p3, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getFollowerInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    .locals 11
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 701
    const-string v6, "SLSServiceManager"

    const-string v9, "getting followers info"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_3

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 703
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    move v6, v7

    :goto_1
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 704
    const/4 v3, 0x0

    .line 705
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getFollowersInfoUrlFormat()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    aput-object p1, v10, v8

    const/16 v8, 0x3e8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v10, v7

    invoke-static {v6, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 706
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 708
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 709
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Contract-Version"

    const-string v8, "2"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 711
    const/4 v2, 0x0

    .line 712
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 715
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 717
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 718
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 721
    :cond_0
    if-eqz v2, :cond_1

    .line 722
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 733
    :cond_1
    if-eqz v4, :cond_2

    .line 734
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 738
    :cond_2
    if-nez v3, :cond_7

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 739
    return-object v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_3
    move v6, v8

    .line 702
    goto :goto_0

    :cond_4
    move v6, v8

    .line 703
    goto :goto_1

    .line 725
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IFollowersResult$FollowersResult;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 726
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get followers info with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_6

    .line 728
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 733
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_5

    .line 734
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_5
    throw v6

    .line 730
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    :try_start_2
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbc2

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 738
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_7
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2
.end method

.method public getFollowersFromPeopleHub()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 626
    const-string v5, "SLSServiceManager"

    const-string v6, "getting followers info from people hub"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 628
    const/4 v2, 0x0

    .line 629
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMyFollowersFromPeopleHubUrl()Ljava/lang/String;

    move-result-object v4

    .line 630
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 632
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 633
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Contract-Version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 634
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    const/4 v3, 0x0

    .line 640
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 642
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 643
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v6, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    const-class v7, Ljava/util/Date;

    new-instance v8, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;

    invoke-direct {v8}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;-><init>()V

    invoke-static {v5, v6, v7, v8}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 655
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_0
    if-eqz v3, :cond_1

    .line 656
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 659
    :cond_1
    return-object v2

    .line 627
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 647
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 648
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get followers info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_4

    .line 650
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 655
    :catchall_0
    move-exception v5

    if-eqz v3, :cond_3

    .line 656
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_3
    throw v5

    .line 652
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbc2

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getFollowingFromPeopleHub(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 584
    const-string v6, "SLSServiceManager"

    const-string v9, "getting following info from people hub"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_2

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 586
    const/4 v3, 0x0

    .line 588
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 589
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMyFollowingSummaryUrlFormat()Ljava/lang/String;

    move-result-object v5

    .line 593
    .local v5, "url":Ljava/lang/String;
    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 595
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 596
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Contract-Version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600
    const/4 v2, 0x0

    .line 601
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 604
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 606
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 607
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    const-class v8, Ljava/util/Date;

    new-instance v9, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;

    invoke-direct {v9}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;-><init>()V

    invoke-static {v6, v7, v8, v9}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_0
    if-eqz v4, :cond_1

    .line 619
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 622
    :cond_1
    return-object v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_2
    move v6, v8

    .line 585
    goto :goto_0

    .line 591
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_3
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getFollowingSummaryUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "url":Ljava/lang/String;
    goto :goto_1

    .line 610
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 611
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get following info with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 613
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 618
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_4

    .line 619
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_4
    throw v6

    .line 615
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_2
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xc84

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getFollowingSummary()Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 548
    const-string v6, "SLSServiceManager"

    const-string v7, "getting following summary"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v7, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 550
    const/4 v3, 0x0

    .line 551
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMyFollowingSummaryUrlFormat()Ljava/lang/String;

    move-result-object v5

    .line 552
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 554
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 555
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Contract-Version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 556
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 557
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    const/4 v2, 0x0

    .line 560
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 563
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 565
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 566
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult;
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 576
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult;
    :cond_0
    if-eqz v4, :cond_1

    .line 577
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 580
    :cond_1
    return-object v3

    .line 549
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 568
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 569
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get followers info with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_4

    .line 571
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_3

    .line 577
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_3
    throw v6

    .line 573
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xc84

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getFriendFinderSettings()Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4151
    const-string v4, "SLSServiceManager"

    const-string v5, "getting FriendFinder settings"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4152
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v5, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4153
    const/4 v1, 0x0

    .line 4154
    .local v1, "result":Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getFriendFinderSettingsUrl()Ljava/lang/String;

    move-result-object v3

    .line 4156
    .local v3, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 4159
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 4161
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v4, v5, :cond_0

    .line 4162
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v5, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "result":Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    check-cast v1, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4173
    .restart local v1    # "result":Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    :cond_0
    if-eqz v2, :cond_1

    :try_start_1
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_1

    .line 4174
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 4179
    :cond_1
    :goto_1
    if-nez v1, :cond_5

    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 4180
    return-object v1

    .line 4152
    .end local v1    # "result":Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 4164
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 4165
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to get smartglass settings "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4166
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_4

    .line 4167
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4172
    :catchall_0
    move-exception v4

    .line 4173
    if-eqz v2, :cond_3

    :try_start_3
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 4174
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 4176
    :cond_3
    :goto_3
    throw v4

    .line 4169
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x1389

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4179
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v1    # "result":Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    :cond_5
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 4175
    .end local v1    # "result":Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    :catch_1
    move-exception v5

    goto :goto_3

    .restart local v1    # "result":Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
    :catch_2
    move-exception v4

    goto :goto_1
.end method

.method public getFriendsWhoEarnedAchievementInfo(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "achievementId"    # I
    .param p4, "startDate"    # Ljava/lang/String;
    .param p5, "endDate"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1205
    const-string v5, "SLSServiceManager"

    const-string v8, "getting friends who earned achievement info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1207
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1208
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v6

    :goto_2
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1209
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    move v5, v6

    :goto_3
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1210
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    move v5, v6

    :goto_4
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1211
    const/4 v2, 0x0

    .line 1212
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getFriendsWhoEarnedAchievementInfoUrlFormat()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v9, v6

    const/4 v6, 0x2

    aput-object p2, v9, v6

    const/4 v6, 0x3

    aput-object p4, v9, v6

    const/4 v6, 0x4

    aput-object p5, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1213
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1215
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1216
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1217
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1218
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Contract-Version"

    const-string v7, "5"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1220
    const/4 v3, 0x0

    .line 1223
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1225
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 1226
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1237
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 1238
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1243
    :cond_1
    :goto_5
    if-nez v2, :cond_9

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_6
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1244
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 1206
    goto/16 :goto_0

    :cond_3
    move v5, v7

    .line 1207
    goto/16 :goto_1

    :cond_4
    move v5, v7

    .line 1208
    goto/16 :goto_2

    :cond_5
    move v5, v7

    .line 1209
    goto/16 :goto_3

    :cond_6
    move v5, v7

    .line 1210
    goto/16 :goto_4

    .line 1228
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1229
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get friends achievement info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1230
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_8

    .line 1231
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1236
    :catchall_0
    move-exception v5

    .line 1237
    if-eqz v3, :cond_7

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_7

    .line 1238
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1240
    :cond_7
    :goto_7
    throw v5

    .line 1233
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_8
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbdb

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1243
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_9
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_6

    .line 1239
    :catch_1
    move-exception v6

    goto :goto_7

    :catch_2
    move-exception v5

    goto :goto_5
.end method

.method public getGameClipInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    .locals 12
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "gameClipId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0xbc0

    .line 775
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getGameClipInfo(xuid: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " scid: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " gameClipId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 778
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 780
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getMediaHubService()Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    move-result-object v3

    .line 783
    .local v3, "mediaHubService":Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    :try_start_0
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;->with(Ljava/util/List;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;

    move-result-object v4

    .line 784
    .local v4, "request":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;
    invoke-interface {v3, v4}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getGameClips(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;)Lretrofit2/Call;

    move-result-object v6

    invoke-interface {v6}, Lretrofit2/Call;->execute()Lretrofit2/Response;

    move-result-object v2

    .line 786
    .local v2, "gameclipResponse":Lretrofit2/Response;, "Lretrofit2/Response<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;>;"
    invoke-virtual {v2}, Lretrofit2/Response;->isSuccessful()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 787
    invoke-virtual {v2}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;->values()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 789
    .local v1, "gameClips":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 790
    new-instance v5, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;-><init>()V

    .line 791
    .local v5, "result":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    const/4 v6, 0x0

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;

    invoke-static {v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataMapper;->from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    move-result-object v6

    iput-object v6, v5, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 792
    return-object v5

    .line 796
    .end local v1    # "gameClips":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameClip;>;"
    .end local v5    # "result":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    :cond_0
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to get game clip: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lretrofit2/Response;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbc0

    invoke-direct {v6, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 799
    .end local v2    # "gameclipResponse":Lretrofit2/Response;, "Lretrofit2/Response<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipBatchResponse;>;"
    .end local v4    # "request":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;
    :catch_0
    move-exception v0

    .line 800
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "SLSServiceManager"

    const-string v7, "Failed getGameClipInfo"

    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 801
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-direct {v6, v10, v11, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
.end method

.method public getGameClipsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    .locals 12
    .param p1, "titleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "qualifier"    # Ljava/lang/String;
    .param p3, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0xbc1

    .line 1893
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getGameClipsInfo(titleId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " qualifier: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " xuid: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1895
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 1896
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1897
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1899
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getMediaHubService()Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    move-result-object v2

    .line 1900
    .local v2, "mediaHubService":Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    invoke-static {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v1

    .line 1904
    .local v1, "gameClipsFilter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    if-nez v1, :cond_0

    .line 1905
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getGameClipsMaxPageSize()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {p3, p1, v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->forUserAndTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    move-result-object v6

    invoke-interface {v2, v6}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;

    move-result-object v4

    .line 1931
    .local v4, "searchRequest":Lretrofit2/Call;
    :goto_0
    :try_start_0
    invoke-interface {v4}, Lretrofit2/Call;->execute()Lretrofit2/Response;

    move-result-object v5

    .line 1933
    .local v5, "searchResponse":Lretrofit2/Response;
    invoke-virtual {v5}, Lretrofit2/Response;->isSuccessful()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1934
    const/4 v3, 0x0

    .line 1936
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    invoke-virtual {v5}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;

    if-eqz v6, :cond_1

    .line 1937
    invoke-virtual {v5}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;

    invoke-static {v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataMapper;->from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$GameclipSearchResponse;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1944
    :goto_1
    return-object v3

    .line 1907
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    .end local v4    # "searchRequest":Lretrofit2/Call;
    .end local v5    # "searchResponse":Lretrofit2/Response;
    :cond_0
    sget-object v6, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager$3;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$GameProgressGameClipsFilter:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 1925
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getGameClipsMaxPageSize()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->forTitle(Ljava/lang/String;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    move-result-object v6

    invoke-interface {v2, v6}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;

    move-result-object v4

    .restart local v4    # "searchRequest":Lretrofit2/Call;
    goto :goto_0

    .line 1910
    .end local v4    # "searchRequest":Lretrofit2/Call;
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getGameClipsMaxPageSize()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->forTitle(Ljava/lang/String;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    move-result-object v6

    invoke-interface {v2, v6}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getGameClips(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;

    move-result-object v4

    .line 1911
    .restart local v4    # "searchRequest":Lretrofit2/Call;
    goto :goto_0

    .line 1915
    .end local v4    # "searchRequest":Lretrofit2/Call;
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getGameClipsMaxPageSize()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {p3, p1, v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->forUserAndTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    move-result-object v6

    invoke-interface {v2, v6}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getGameClips(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;

    move-result-object v4

    .line 1916
    .restart local v4    # "searchRequest":Lretrofit2/Call;
    goto :goto_0

    .line 1920
    .end local v4    # "searchRequest":Lretrofit2/Call;
    :pswitch_2
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getGameClipsMaxPageSize()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {p3, p1, v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;->forUserAndTitle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;

    move-result-object v6

    invoke-interface {v2, v6}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$SearchRequest;)Lretrofit2/Call;

    move-result-object v4

    .line 1921
    .restart local v4    # "searchRequest":Lretrofit2/Call;
    goto :goto_0

    .line 1938
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    .restart local v5    # "searchResponse":Lretrofit2/Response;
    :cond_1
    :try_start_1
    invoke-virtual {v5}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;

    if-eqz v6, :cond_2

    .line 1939
    invoke-virtual {v5}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;

    invoke-static {v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataMapper;->from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotSearchResponse;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;

    move-result-object v3

    goto :goto_1

    .line 1941
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected response body: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1949
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$ProfileShowcaseResult;
    .end local v5    # "searchResponse":Lretrofit2/Response;
    :catch_0
    move-exception v0

    .line 1950
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "SLSServiceManager"

    const-string v7, "getGameClipsInfo IOException"

    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1951
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-direct {v6, v10, v11, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6

    .line 1947
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v5    # "searchResponse":Lretrofit2/Response;
    :cond_3
    :try_start_2
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbc1

    invoke-direct {v6, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1907
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public getGameProgress360AchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .locals 11
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "titleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "continuationToken"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1585
    const-string v5, "SLSServiceManager"

    const-string v6, "getting game progress achievements info"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1586
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 1587
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1588
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1590
    const/4 v2, 0x0

    .line 1591
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getGameProgress360AllAchievementsInfoUrlFormat()Ljava/lang/String;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/Object;

    aput-object p1, v7, v8

    aput-object p2, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1592
    .local v4, "url":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1593
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s&continuationToken=%s"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v4, v7, v8

    aput-object p3, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1595
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1597
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1598
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1599
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Contract-Version"

    const-string v7, "4"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1600
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1602
    const/4 v3, 0x0

    .line 1605
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1607
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_1

    .line 1608
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1619
    :cond_1
    if-eqz v3, :cond_2

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 1620
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1625
    :cond_2
    :goto_0
    if-nez v2, :cond_5

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_1
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1626
    return-object v2

    .line 1610
    :catch_0
    move-exception v0

    .line 1611
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get friends 360 achievement info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1612
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_4

    .line 1613
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1618
    :catchall_0
    move-exception v5

    .line 1619
    if-eqz v3, :cond_3

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 1620
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1622
    :cond_3
    :goto_2
    throw v5

    .line 1615
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbdd

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1625
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_1

    .line 1621
    :catch_1
    move-exception v6

    goto :goto_2

    :catch_2
    move-exception v5

    goto :goto_0
.end method

.method public getGameProgress360EarnedAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .locals 11
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1631
    const-string v5, "SLSServiceManager"

    const-string v8, "getting game progress achievements info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1632
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_3

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1633
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1634
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    move v5, v6

    :goto_2
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1635
    const/4 v2, 0x0

    .line 1636
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getGameProgress360EarnedAchievementsInfoUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v9, v10, [Ljava/lang/Object;

    aput-object p1, v9, v7

    aput-object p2, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1637
    .local v4, "url":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1638
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s&continuationToken=%s"

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v4, v9, v7

    aput-object p3, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1640
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1642
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1643
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1644
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Contract-Version"

    const-string v7, "4"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1645
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1647
    const/4 v3, 0x0

    .line 1650
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1652
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_1

    .line 1653
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1664
    :cond_1
    if-eqz v3, :cond_2

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 1665
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1670
    :cond_2
    :goto_3
    if-nez v2, :cond_8

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1671
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_3
    move v5, v7

    .line 1632
    goto/16 :goto_0

    :cond_4
    move v5, v7

    .line 1633
    goto/16 :goto_1

    :cond_5
    move v5, v7

    .line 1634
    goto/16 :goto_2

    .line 1655
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1656
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get friends 360 earned achievement info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1657
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_7

    .line 1658
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1663
    :catchall_0
    move-exception v5

    .line 1664
    if-eqz v3, :cond_6

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_6

    .line 1665
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1667
    :cond_6
    :goto_5
    throw v5

    .line 1660
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_7
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbde

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1670
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_8
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 1666
    :catch_1
    move-exception v6

    goto :goto_5

    :catch_2
    move-exception v5

    goto :goto_3
.end method

.method public getGameProgressLimitedTimeChallengeInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 11
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1543
    const-string v5, "SLSServiceManager"

    const-string v8, "getting game progress limited time challenge info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1544
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1545
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1546
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v6

    :goto_2
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1547
    const/4 v2, 0x0

    .line 1548
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getGameProgressXboxOneAchievementsInfoUrlFormat()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    aput-object p1, v10, v7

    aput-object p2, v10, v6

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&types=Challenge&orderby=EndingSoon"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1549
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1551
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1552
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1553
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Contract-Version"

    const-string v7, "4"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1554
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1556
    const/4 v3, 0x0

    .line 1559
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1561
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 1562
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1573
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 1574
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1579
    :cond_1
    :goto_3
    if-nez v2, :cond_7

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1580
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 1544
    goto/16 :goto_0

    :cond_3
    move v5, v7

    .line 1545
    goto/16 :goto_1

    :cond_4
    move v5, v7

    .line 1546
    goto/16 :goto_2

    .line 1564
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1565
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get limited time challenge info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_6

    .line 1567
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1572
    :catchall_0
    move-exception v5

    .line 1573
    if-eqz v3, :cond_5

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_5

    .line 1574
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1576
    :cond_5
    :goto_5
    throw v5

    .line 1569
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbe0

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1579
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_7
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 1575
    :catch_1
    move-exception v6

    goto :goto_5

    :catch_2
    move-exception v5

    goto :goto_3
.end method

.method public getGameProgressXboxoneAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 11
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1498
    const-string v5, "SLSServiceManager"

    const-string v8, "getting game progress achievements info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1499
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_3

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1500
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1501
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    move v5, v6

    :goto_2
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1502
    const/4 v2, 0x0

    .line 1503
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getTitleProgressEarnedAchievementUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v9, v10, [Ljava/lang/Object;

    aput-object p1, v9, v7

    aput-object p2, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1504
    .local v4, "url":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1505
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s&continuationToken=%s"

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v4, v9, v7

    aput-object p3, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1507
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1509
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1510
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1511
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Contract-Version"

    const-string v7, "4"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1512
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1514
    const/4 v3, 0x0

    .line 1517
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1519
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_1

    .line 1520
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1531
    :cond_1
    if-eqz v3, :cond_2

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 1532
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1537
    :cond_2
    :goto_3
    if-nez v2, :cond_8

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1538
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_3
    move v5, v7

    .line 1499
    goto/16 :goto_0

    :cond_4
    move v5, v7

    .line 1500
    goto/16 :goto_1

    :cond_5
    move v5, v7

    .line 1501
    goto/16 :goto_2

    .line 1522
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1523
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get game progress info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1524
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_7

    .line 1525
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1530
    :catchall_0
    move-exception v5

    .line 1531
    if-eqz v3, :cond_6

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_6

    .line 1532
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1534
    :cond_6
    :goto_5
    throw v5

    .line 1527
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_7
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbdc

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1537
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_8
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 1533
    :catch_1
    move-exception v6

    goto :goto_5

    :catch_2
    move-exception v5

    goto :goto_3
.end method

.method public getGamertagSuggestions(Ljava/lang/String;I)Ljava/util/List;
    .locals 10
    .param p1, "seed"    # Ljava/lang/String;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4291
    const-string v6, "SLSServiceManager"

    const-string v7, "attempting to retrieve gamertag suggestions"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4292
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v7, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4294
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSuggestGamertagsUrl()Ljava/lang/String;

    move-result-object v5

    .line 4295
    .local v5, "url":Ljava/lang/String;
    const/4 v4, 0x0

    .line 4296
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v3, 0x0

    .line 4298
    .local v3, "response":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 4299
    .local v2, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4300
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4303
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, p2, v6, p1}, Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 4304
    .local v0, "body":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v2, v6}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 4306
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 4307
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "response":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4314
    .restart local v3    # "response":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;
    :cond_0
    if-eqz v4, :cond_1

    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_1

    .line 4315
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 4320
    :cond_1
    :goto_1
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 4321
    if-eqz v3, :cond_4

    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;->Gamertags:Ljava/util/List;

    :goto_2
    return-object v6

    .line 4292
    .end local v0    # "body":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;
    .end local v2    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v3    # "response":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 4309
    .restart local v2    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 4310
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "SLSServiceManager"

    const-string v7, "failed to retrieve gamertag suggestions"

    invoke-static {v6, v7, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4311
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x1

    invoke-direct {v6, v8, v9, v1}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4313
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 4314
    if-eqz v4, :cond_3

    :try_start_3
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_3

    .line 4315
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 4317
    :cond_3
    :goto_3
    throw v6

    .line 4321
    .restart local v0    # "body":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;
    .restart local v3    # "response":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;
    :cond_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    goto :goto_2

    .line 4316
    .end local v0    # "body":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;
    .end local v3    # "response":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;
    :catch_1
    move-exception v7

    goto :goto_3

    .restart local v0    # "body":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsRequest;
    .restart local v3    # "response":Lcom/microsoft/xbox/service/network/managers/SuggestGamertagsResponse;
    :catch_2
    move-exception v6

    goto :goto_1
.end method

.method public getLikeDataInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    .locals 11
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 879
    const-string v7, "SLSServiceManager"

    const-string v10, "getting like info"

    invoke-static {v7, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v10, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v10, :cond_0

    move v7, v8

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 881
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    :goto_1
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 882
    const/4 v3, 0x0

    .line 884
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 885
    const-string v7, "SLSServiceManager"

    const-string v8, "Empty post body for like count"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 929
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    .local v4, "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :goto_2
    return-object v4

    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :cond_0
    move v7, v9

    .line 880
    goto :goto_0

    :cond_1
    move v8, v9

    .line 881
    goto :goto_1

    .line 889
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSocialActionsSummariesBatchUrl()Ljava/lang/String;

    move-result-object v6

    .line 891
    .local v6, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 893
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 894
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Content-type"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 895
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-contract-version"

    const-string v9, "3"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 898
    const/4 v2, 0x0

    .line 899
    .local v2, "response":Ljava/lang/String;
    const/4 v5, 0x0

    .line 903
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v6, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 905
    const/16 v7, 0xc8

    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v7, v8, :cond_3

    .line 906
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 909
    :cond_3
    if-eqz v2, :cond_4

    .line 910
    const-class v7, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;

    invoke-static {v2, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 922
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :cond_4
    if-eqz v5, :cond_5

    :try_start_1
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_5

    .line 923
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 928
    :cond_5
    :goto_3
    if-nez v3, :cond_8

    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v6, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    move-object v4, v3

    .line 929
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    .restart local v4    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    goto :goto_2

    .line 913
    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :catch_0
    move-exception v0

    .line 914
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get like info with exception "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    instance-of v7, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v7, :cond_7

    .line 916
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 921
    :catchall_0
    move-exception v7

    .line 922
    if-eqz v5, :cond_6

    :try_start_3
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_6

    .line 923
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 925
    :cond_6
    :goto_5
    throw v7

    .line 918
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_7
    :try_start_4
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xd1c

    invoke-direct {v7, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 928
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :cond_8
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 924
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :catch_1
    move-exception v8

    goto :goto_5

    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :catch_2
    move-exception v7

    goto :goto_3
.end method

.method public getLiveTVSettings(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    .locals 9
    .param p1, "language"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2750
    const-string v4, "SLSServiceManager"

    const-string v7, "getting live tv settings"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2751
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v7, :cond_2

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2752
    const/4 v1, 0x0

    .line 2753
    .local v1, "result":Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSmartglassSettingsUrlFormat()Ljava/lang/String;

    move-result-object v7

    new-array v5, v5, [Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getLocaleRegion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-static {v4, v7, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2755
    .local v3, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2758
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 2760
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v4, v5, :cond_0

    .line 2761
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v5, Lcom/microsoft/xbox/xle/app/LiveTVSettings;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "result":Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    check-cast v1, Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2772
    .restart local v1    # "result":Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    :cond_0
    if-eqz v2, :cond_1

    :try_start_1
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_1

    .line 2773
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2778
    :cond_1
    :goto_1
    if-nez v1, :cond_5

    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2779
    return-object v1

    .end local v1    # "result":Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_2
    move v4, v6

    .line 2751
    goto :goto_0

    .line 2763
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2764
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to get live tv settings "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2765
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_4

    .line 2766
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2771
    :catchall_0
    move-exception v4

    .line 2772
    if-eqz v2, :cond_3

    :try_start_3
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 2773
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2775
    :cond_3
    :goto_3
    throw v4

    .line 2768
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x1389

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2778
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v1    # "result":Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    :cond_5
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2774
    .end local v1    # "result":Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    :catch_1
    move-exception v5

    goto :goto_3

    .restart local v1    # "result":Lcom/microsoft/xbox/xle/app/LiveTVSettings;
    :catch_2
    move-exception v4

    goto :goto_1
.end method

.method public getMeLikeInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .locals 11
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 978
    const-string v7, "SLSServiceManager"

    const-string v10, "getting me like info"

    invoke-static {v7, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v10, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v10, :cond_0

    move v7, v8

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 980
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    :goto_1
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 981
    const/4 v3, 0x0

    .line 983
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 984
    const-string v7, "SLSServiceManager"

    const-string v8, "Empty post body for me like info"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 1028
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .local v4, "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    :goto_2
    return-object v4

    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    :cond_0
    move v7, v9

    .line 979
    goto :goto_0

    :cond_1
    move v8, v9

    .line 980
    goto :goto_1

    .line 988
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMeLikeInfoUrl()Ljava/lang/String;

    move-result-object v6

    .line 990
    .local v6, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 992
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 993
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Content-type"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 994
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-contract-version"

    const-string v9, "3"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 997
    const/4 v2, 0x0

    .line 998
    .local v2, "response":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1002
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v6, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 1004
    const/16 v7, 0xc8

    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v7, v8, :cond_3

    .line 1005
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 1008
    :cond_3
    if-eqz v2, :cond_4

    .line 1009
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 1021
    :cond_4
    if-eqz v5, :cond_5

    :try_start_1
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_5

    .line 1022
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1027
    :cond_5
    :goto_3
    if-nez v3, :cond_8

    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v6, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    move-object v4, v3

    .line 1028
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .restart local v4    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    goto :goto_2

    .line 1012
    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    :catch_0
    move-exception v0

    .line 1013
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get me like info with exception "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    instance-of v7, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v7, :cond_7

    .line 1015
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1020
    :catchall_0
    move-exception v7

    .line 1021
    if-eqz v5, :cond_6

    :try_start_3
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_6

    .line 1022
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1024
    :cond_6
    :goto_5
    throw v7

    .line 1017
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_7
    :try_start_4
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xd1b

    invoke-direct {v7, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1027
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_8
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 1023
    :catch_1
    move-exception v8

    goto :goto_5

    :catch_2
    move-exception v7

    goto :goto_3
.end method

.method public getMeProfile()Lcom/microsoft/xbox/service/network/managers/UserProfile;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2798
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMeProfileUrl()Ljava/lang/String;

    move-result-object v4

    .line 2799
    .local v4, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2801
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/UserProfile;
    const/4 v3, 0x0

    .line 2803
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2805
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2806
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2807
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "3"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2810
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 2812
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 2813
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v6, Lcom/microsoft/xbox/service/network/managers/UserProfile;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/UserProfile;
    check-cast v2, Lcom/microsoft/xbox/service/network/managers/UserProfile;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2824
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/UserProfile;
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 2825
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2830
    :cond_1
    :goto_0
    return-object v2

    .line 2815
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/UserProfile;
    :catch_0
    move-exception v0

    .line 2816
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get current user profile "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2817
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_3

    .line 2818
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2823
    :catchall_0
    move-exception v5

    .line 2824
    if-eqz v3, :cond_2

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    .line 2825
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2827
    :cond_2
    :goto_1
    throw v5

    .line 2820
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbbb

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2826
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    goto :goto_1

    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/UserProfile;
    :catch_2
    move-exception v5

    goto :goto_0
.end method

.method public getMyShortCircuitProfile()Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4325
    const-string v5, "SLSServiceManager"

    const-string v6, "getting ShortCircuitProfile"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4326
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4327
    const/4 v2, 0x0

    .line 4329
    .local v2, "result":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getShortCircuitProfileUrlFormat()Ljava/lang/String;

    move-result-object v4

    .line 4331
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4332
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4334
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "PS-MSAAuthTicket"

    invoke-static {}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->getInstance()Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->getMsaTicket()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4335
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "PS-ApplicationId"

    const-string v7, "44445A65-4A71-4083-8C90-041A22856E69"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4336
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "PS-Scenario"

    const-string v7, "Test only"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4337
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-Type"

    const-string v7, "application/x-www-form-urlencoded"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4339
    const/4 v3, 0x0

    .line 4342
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 4344
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 4345
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;->parseJson(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 4356
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 4357
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 4362
    :cond_1
    :goto_1
    if-nez v2, :cond_5

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 4363
    return-object v2

    .line 4326
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 4347
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 4348
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get ShortCircuitProfile "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4349
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_4

    .line 4350
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4355
    :catchall_0
    move-exception v5

    .line 4356
    if-eqz v3, :cond_3

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 4357
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 4359
    :cond_3
    :goto_3
    throw v5

    .line 4352
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x1389

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4362
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 4358
    :catch_1
    move-exception v6

    goto :goto_3

    :catch_2
    move-exception v5

    goto :goto_1
.end method

.method public getNeverListInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1090
    const-string v6, "SLSServiceManager"

    const-string v9, "getting profile never list info"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1091
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_3

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1092
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    move v6, v7

    :goto_1
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1093
    const/4 v3, 0x0

    .line 1094
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileNeverListUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1095
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1097
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1098
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1099
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Contract-Version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1101
    const/4 v2, 0x0

    .line 1102
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1105
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 1107
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 1108
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 1111
    :cond_0
    if-eqz v2, :cond_1

    .line 1112
    const-class v6, Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;

    invoke-static {v2, v6}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1124
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    :cond_1
    if-eqz v4, :cond_2

    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    .line 1125
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1130
    :cond_2
    :goto_2
    if-nez v3, :cond_7

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1131
    return-object v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_3
    move v6, v8

    .line 1091
    goto :goto_0

    :cond_4
    move v6, v8

    .line 1092
    goto :goto_1

    .line 1115
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1116
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get profile never list info with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_6

    .line 1118
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1123
    :catchall_0
    move-exception v6

    .line 1124
    if-eqz v4, :cond_5

    :try_start_3
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_5

    .line 1125
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1127
    :cond_5
    :goto_4
    throw v6

    .line 1120
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    :try_start_4
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xc83

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1130
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    :cond_7
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3

    .line 1126
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    :catch_1
    move-exception v7

    goto :goto_4

    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/NeverListResultContainer$NeverListResult;
    :catch_2
    move-exception v6

    goto :goto_2
.end method

.method public getPageActivityFeedInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 10
    .param p1, "pageId"    # Ljava/lang/String;
    .param p2, "continuationToken"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3677
    const-string v5, "SLSServiceManager"

    const-string v8, "getting page activity feed info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3678
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3680
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getPageActivityFeedUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v9, v6, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3682
    .local v4, "url":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3683
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "&contToken=%s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p2, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3685
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3687
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3688
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3689
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3690
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "13"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3692
    const/4 v3, 0x0

    .line 3693
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 3696
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3697
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_1

    .line 3698
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 3707
    :cond_1
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    .line 3710
    if-nez v2, :cond_3

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_1
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3712
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 3678
    goto :goto_0

    .line 3700
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3701
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get page activity info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3702
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3707
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v5

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    throw v5

    .line 3703
    :catch_1
    move-exception v0

    .line 3704
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get page activity info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3705
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xc85

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3710
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_1
.end method

.method public getPeopleActivityFeed(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;Ljava/lang/String;IZZ)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 15
    .param p1, "xuid"    # Ljava/lang/String;
    .param p3, "filters"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .param p4, "continuationToken"    # Ljava/lang/String;
    .param p5, "numItems"    # I
    .param p6, "getLegacyFeed"    # Z
    .param p7, "includeSelf"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;",
            ">;",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            "Ljava/lang/String;",
            "IZZ)",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3413
    .local p2, "feedTypes":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;>;"
    const-string v10, "SLSServiceManager"

    const-string v11, "getting people activity feed"

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3414
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v10, v11, :cond_1

    const/4 v10, 0x1

    :goto_0
    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3415
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    const/4 v10, 0x1

    :goto_1
    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3416
    const/4 v6, 0x0

    .line 3417
    .local v6, "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 3418
    .local v4, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 3421
    .local v9, "urlBuilder":Ljava/lang/StringBuilder;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 3422
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getPeopleActivityFeedUrlFormat()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object p1, v12, v13

    const/4 v13, 0x1

    aput-object p4, v12, v13

    const/4 v13, 0x2

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3427
    :goto_2
    if-eqz p2, :cond_4

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_4

    .line 3428
    const-string v10, "&activityTypes="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3429
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v10

    if-ge v5, v10, :cond_4

    .line 3430
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3432
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ge v5, v10, :cond_0

    .line 3433
    const-string v10, ";"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3429
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 3414
    .end local v4    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v5    # "i":I
    .end local v6    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .end local v9    # "urlBuilder":Ljava/lang/StringBuilder;
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    .line 3415
    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    .line 3424
    .restart local v4    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v6    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .restart local v9    # "urlBuilder":Ljava/lang/StringBuilder;
    :cond_3
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getPeopleActivityFeedNoTokenUrlFormat()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object p1, v12, v13

    const/4 v13, 0x1

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 3438
    :cond_4
    if-eqz p3, :cond_f

    invoke-static {}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->defaultPrefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v10

    move-object/from16 v0, p3

    if-eq v0, v10, :cond_f

    const/4 v3, 0x1

    .line 3440
    .local v3, "hasFilters":Z
    :goto_4
    if-eqz v3, :cond_a

    .line 3441
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 3442
    .local v7, "scope":Ljava/lang/StringBuilder;
    const-string v10, "&scope=Pages;Recommendations"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3444
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFriends()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 3445
    const-string v10, ";friends"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3448
    :cond_5
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFavorites()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 3449
    const-string v10, ";favorites"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3452
    :cond_6
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showClubs()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 3453
    const-string v10, ";clubs"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3456
    :cond_7
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showGames()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 3457
    const-string v10, ";games"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3460
    :cond_8
    invoke-virtual/range {p3 .. p3}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showPopular()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 3461
    const-string v10, ";trending"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3464
    :cond_9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3467
    .end local v7    # "scope":Ljava/lang/StringBuilder;
    :cond_a
    const-string v10, "&includeSelf="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    if-nez p7, :cond_b

    if-eqz v3, :cond_10

    :cond_b
    const/4 v10, 0x1

    :goto_5
    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3468
    const-string v10, "&excludeTypes=Played"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3470
    if-nez p6, :cond_c

    .line 3471
    const-string v10, "&platform=XboxOne"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3474
    :cond_c
    const-string v10, "SLSServiceManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getPeopleActivityFeedUrl: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3477
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3478
    new-instance v10, Lorg/apache/http/message/BasicHeader;

    const-string v11, "Content-type"

    const-string v12, "application/json"

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3479
    new-instance v10, Lorg/apache/http/message/BasicHeader;

    const-string v11, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3480
    new-instance v10, Lorg/apache/http/message/BasicHeader;

    const-string v11, "x-xbl-contract-version"

    const-string v12, "13"

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3482
    const/4 v8, 0x0

    .line 3485
    .local v8, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v8

    .line 3487
    const/16 v10, 0xc8

    iget v11, v8, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v10, v11, :cond_d

    .line 3488
    iget-object v10, v8, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v10}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 3499
    :cond_d
    if-eqz v8, :cond_e

    :try_start_1
    iget-object v10, v8, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v10, :cond_e

    .line 3500
    iget-object v10, v8, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 3505
    :cond_e
    :goto_6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    if-nez v6, :cond_13

    sget-object v10, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_7
    invoke-static {v11, v10}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3506
    return-object v6

    .line 3438
    .end local v3    # "hasFilters":Z
    .end local v8    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 3467
    .restart local v3    # "hasFilters":Z
    :cond_10
    const/4 v10, 0x0

    goto/16 :goto_5

    .line 3490
    .restart local v8    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v2

    .line 3491
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v10, "SLSServiceManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "failed to get people activity feed with exception "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3492
    instance-of v10, v2, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v10, :cond_12

    .line 3493
    check-cast v2, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v2    # "ex":Ljava/lang/Exception;
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3498
    :catchall_0
    move-exception v10

    .line 3499
    if-eqz v8, :cond_11

    :try_start_3
    iget-object v11, v8, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v11, :cond_11

    .line 3500
    iget-object v11, v8, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 3502
    :cond_11
    :goto_8
    throw v10

    .line 3495
    .restart local v2    # "ex":Ljava/lang/Exception;
    :cond_12
    :try_start_4
    new-instance v10, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v12, 0xc86

    invoke-direct {v10, v12, v13, v2}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3505
    .end local v2    # "ex":Ljava/lang/Exception;
    :cond_13
    sget-object v10, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_7

    .line 3501
    :catch_1
    move-exception v11

    goto :goto_8

    :catch_2
    move-exception v10

    goto :goto_6
.end method

.method public getPeopleHubBatchSummaries(Ljava/util/ArrayList;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p1, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1395
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v10, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v10, :cond_1

    move v7, v8

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1396
    if-eqz p1, :cond_2

    :goto_1
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1398
    const/4 v3, 0x0

    .line 1399
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMyPeopleHubBatchSummaryUrlFormat()Ljava/lang/String;

    move-result-object v6

    .line 1401
    .local v6, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1402
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1403
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Content-type"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1404
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1405
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "X-XBL-Contract-Version"

    const-string v9, "1"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1407
    const/4 v5, 0x0

    .line 1409
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    new-instance v7, Lcom/microsoft/xbox/service/model/sls/PeopleHubBatchRequest;

    invoke-direct {v7, p1}, Lcom/microsoft/xbox/service/model/sls/PeopleHubBatchRequest;-><init>(Ljava/util/ArrayList;)V

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/sls/PeopleHubBatchRequest;->getPeopleHubBatchRequestBody(Lcom/microsoft/xbox/service/model/sls/PeopleHubBatchRequest;)Ljava/lang/String;

    move-result-object v2

    .line 1412
    .local v2, "requestBody":Ljava/lang/String;
    :try_start_0
    invoke-static {v6, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 1414
    const/16 v7, 0xc8

    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v7, v8, :cond_3

    .line 1415
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v8, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1416
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    if-eqz v3, :cond_3

    .line 1429
    if-eqz v5, :cond_0

    :try_start_1
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_0

    .line 1430
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_2
    move-object v4, v3

    .line 1436
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .local v4, "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :goto_3
    return-object v4

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "requestBody":Ljava/lang/String;
    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .end local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v6    # "url":Ljava/lang/String;
    :cond_1
    move v7, v9

    .line 1395
    goto :goto_0

    :cond_2
    move v8, v9

    .line 1396
    goto :goto_1

    .line 1429
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "requestBody":Ljava/lang/String;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .restart local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v6    # "url":Ljava/lang/String;
    :cond_3
    if-eqz v5, :cond_4

    :try_start_2
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 1430
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1435
    :cond_4
    :goto_4
    if-nez v3, :cond_7

    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_5
    invoke-static {v6, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    move-object v4, v3

    .line 1436
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .restart local v4    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    goto :goto_3

    .line 1420
    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_0
    move-exception v0

    .line 1421
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get batch people summary"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1422
    instance-of v7, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v7, :cond_6

    .line 1423
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1428
    :catchall_0
    move-exception v7

    .line 1429
    if-eqz v5, :cond_5

    :try_start_4
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_5

    .line 1430
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1432
    :cond_5
    :goto_6
    throw v7

    .line 1425
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    :try_start_5
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x1

    invoke-direct {v7, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1435
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_7
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_5

    .line 1431
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_1
    move-exception v8

    goto :goto_6

    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_2
    move-exception v7

    goto :goto_4

    :catch_3
    move-exception v7

    goto :goto_2
.end method

.method public getPeopleHubFriendFinderState()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3983
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v7, :cond_1

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3985
    const/4 v2, 0x0

    .line 3986
    .local v2, "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getPeopleHubFriendFinderStateUrlFormat()Ljava/lang/String;

    move-result-object v5

    .line 3988
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3989
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3990
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3991
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3992
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Contract-Version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3994
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Market"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3996
    const/4 v4, 0x0

    .line 3999
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 4001
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_2

    .line 4002
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    check-cast v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4003
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    if-eqz v2, :cond_2

    .line 4017
    if-eqz v4, :cond_0

    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_0

    .line 4018
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_1
    move-object v3, v2

    .line 4024
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .local v3, "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :goto_2
    return-object v3

    .line 3983
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v3    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 4017
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :cond_2
    if-eqz v4, :cond_3

    :try_start_2
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 4018
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 4023
    :cond_3
    :goto_3
    if-nez v2, :cond_6

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    move-object v3, v2

    .line 4024
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    goto :goto_2

    .line 4007
    .end local v3    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :catch_0
    move-exception v0

    .line 4008
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to people hub friends finder result"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4009
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 4010
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4016
    :catchall_0
    move-exception v6

    .line 4017
    if-eqz v4, :cond_4

    :try_start_4
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 4018
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 4020
    :cond_4
    :goto_5
    throw v6

    .line 4013
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_5
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x1

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4023
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :cond_6
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 4019
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :catch_1
    move-exception v7

    goto :goto_5

    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :catch_2
    move-exception v6

    goto :goto_3

    :catch_3
    move-exception v6

    goto :goto_1
.end method

.method public getPeopleHubPeoplePlayedTitle(Ljava/lang/String;J)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 10
    .param p1, "meXuid"    # Ljava/lang/String;
    .param p2, "gameTitleId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1346
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1347
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1348
    const/4 v2, 0x0

    .line 1350
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getGameProfileFriendsUrlFormat()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1352
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1353
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1354
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1355
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1356
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Contract-Version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1358
    const/4 v3, 0x0

    .line 1361
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1363
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_3

    .line 1364
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v6, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    const-class v7, Ljava/util/Date;

    new-instance v8, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;

    invoke-direct {v8}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;-><init>()V

    invoke-static {v5, v6, v7, v8}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1366
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    if-eqz v2, :cond_3

    .line 1380
    if-eqz v3, :cond_0

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_0

    .line 1381
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_2
    move-object v5, v2

    .line 1387
    :goto_3
    return-object v5

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    move v5, v7

    .line 1346
    goto/16 :goto_0

    :cond_2
    move v5, v7

    .line 1347
    goto :goto_1

    .line 1380
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :cond_3
    if-eqz v3, :cond_4

    :try_start_2
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_4

    .line 1381
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1386
    :cond_4
    :goto_4
    if-nez v2, :cond_7

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_5
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1387
    const/4 v5, 0x0

    goto :goto_3

    .line 1370
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_0
    move-exception v0

    .line 1371
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get game profile friends who play"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_6

    .line 1373
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1379
    :catchall_0
    move-exception v5

    .line 1380
    if-eqz v3, :cond_5

    :try_start_4
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_5

    .line 1381
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1383
    :cond_5
    :goto_6
    throw v5

    .line 1376
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    :try_start_5
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xb

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1386
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_7
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_5

    .line 1382
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_1
    move-exception v6

    goto :goto_6

    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_2
    move-exception v5

    goto :goto_4

    :catch_3
    move-exception v5

    goto :goto_2
.end method

.method public getPeopleHubPerson(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1442
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_1

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1443
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 1445
    const/4 v2, 0x0

    .line 1446
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getPeopleHubPersonUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1448
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1449
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1450
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1451
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1452
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Contract-Version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1454
    const/4 v4, 0x0

    .line 1457
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 1459
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_2

    .line 1460
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1461
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    if-eqz v2, :cond_2

    .line 1474
    if-eqz v4, :cond_0

    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_0

    .line 1475
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_1
    move-object v3, v2

    .line 1481
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :goto_2
    return-object v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_1
    move v6, v8

    .line 1442
    goto :goto_0

    .line 1474
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :cond_2
    if-eqz v4, :cond_3

    :try_start_2
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 1475
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1480
    :cond_3
    :goto_3
    if-nez v2, :cond_6

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    move-object v3, v2

    .line 1481
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    goto :goto_2

    .line 1465
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_0
    move-exception v0

    .line 1466
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get person summary"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1467
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 1468
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1473
    :catchall_0
    move-exception v6

    .line 1474
    if-eqz v4, :cond_4

    :try_start_4
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 1475
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1477
    :cond_4
    :goto_5
    throw v6

    .line 1470
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_5
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x1

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1480
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_6
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 1476
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_1
    move-exception v7

    goto :goto_5

    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_2
    move-exception v6

    goto :goto_3

    :catch_3
    move-exception v6

    goto :goto_1
.end method

.method public getPeopleHubRecommendations()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4105
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_1

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4106
    const/4 v2, 0x0

    .line 4108
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getPeopleHubRecommendationsUrlFormat()Ljava/lang/String;

    move-result-object v4

    .line 4110
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4111
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4112
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4113
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4114
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Market"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4115
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Contract-Version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4117
    const/4 v3, 0x0

    .line 4120
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 4122
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_2

    .line 4123
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v6, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    const-class v7, Ljava/util/Date;

    new-instance v8, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;

    invoke-direct {v8}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;-><init>()V

    invoke-static {v5, v6, v7, v8}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4125
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    if-eqz v2, :cond_2

    .line 4139
    if-eqz v3, :cond_0

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_0

    .line 4140
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_1
    move-object v5, v2

    .line 4146
    :goto_2
    return-object v5

    .line 4105
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 4139
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :cond_2
    if-eqz v3, :cond_3

    :try_start_2
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 4140
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 4145
    :cond_3
    :goto_3
    if-nez v2, :cond_6

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 4146
    const/4 v5, 0x0

    goto :goto_2

    .line 4129
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_0
    move-exception v0

    .line 4130
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get recommendations"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4131
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 4132
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4138
    :catchall_0
    move-exception v5

    .line 4139
    if-eqz v3, :cond_4

    :try_start_4
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 4140
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 4142
    :cond_4
    :goto_5
    throw v5

    .line 4135
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_5
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xb

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 4145
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_6
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 4141
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_1
    move-exception v6

    goto :goto_5

    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :catch_2
    move-exception v5

    goto :goto_3

    :catch_3
    move-exception v5

    goto :goto_1
.end method

.method public getPopularGamesWithFriends(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/PopularGame;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1249
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1250
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1252
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1254
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1255
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Content-type"

    const-string v9, "application/json"

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1256
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1257
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v8, "X-XBL-Contract-Version"

    const-string v9, "4"

    invoke-direct {v5, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1259
    const/4 v2, 0x0

    .line 1260
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    const/4 v3, 0x0

    .line 1263
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getPopularGamesWithFriendsUrlFormat()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getTenDaysAgoUtcString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1266
    .local v4, "url":Ljava/lang/String;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1268
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 1269
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5, p1}, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$PeopleActivitySummaryResult;->deserialize(Ljava/io/InputStream;Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1280
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 1281
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1286
    :cond_1
    :goto_2
    if-nez v2, :cond_6

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1287
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 1249
    goto :goto_0

    :cond_3
    move v5, v7

    .line 1250
    goto :goto_1

    .line 1271
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1272
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get popular games with friends with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1273
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 1274
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1279
    :catchall_0
    move-exception v5

    .line 1280
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 1281
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1283
    :cond_4
    :goto_4
    throw v5

    .line 1276
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x238c

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1286
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3

    .line 1282
    :catch_1
    move-exception v6

    goto :goto_4

    :catch_2
    move-exception v5

    goto :goto_2
.end method

.method public getPrivacySetting(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;)Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    .locals 14
    .param p1, "settingId"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 425
    const-string v9, "SLSServiceManager"

    const-string v12, "getting user privacy setting"

    invoke-static {v9, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    sget-object v12, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v9, v12, :cond_1

    move v9, v10

    :goto_0
    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 428
    const/4 v5, 0x0

    .line 429
    .local v5, "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileSettingUrlFormat()Ljava/lang/String;

    move-result-object v12

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;->name()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v10, v11

    invoke-static {v9, v12, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 430
    .local v8, "url":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 432
    .local v3, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 433
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "Content-type"

    const-string v11, "application/json"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "x-xbl-contract-version"

    const-string v11, "4"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    const/4 v4, 0x0

    .line 437
    .local v4, "response":Ljava/lang/String;
    const/4 v7, 0x0

    .line 440
    .local v7, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v8, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v7

    .line 442
    const/16 v9, 0xc8

    iget v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v9, v10, :cond_2

    .line 443
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    .line 445
    if-eqz v4, :cond_2

    .line 446
    const-class v9, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;

    invoke-static {v4, v9}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;

    move-object v5, v0

    .line 447
    if-eqz v5, :cond_2

    .line 448
    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;->setPrivacySettingId(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 467
    if-eqz v7, :cond_0

    :try_start_1
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_0

    .line 468
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_1
    move-object v6, v5

    .line 477
    .end local v5    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    .local v6, "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    :goto_2
    return-object v6

    .end local v3    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v4    # "response":Ljava/lang/String;
    .end local v6    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    .end local v7    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v8    # "url":Ljava/lang/String;
    :cond_1
    move v9, v11

    .line 426
    goto :goto_0

    .line 454
    .restart local v3    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v4    # "response":Ljava/lang/String;
    .restart local v5    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    .restart local v7    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v8    # "url":Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 455
    const-string v9, "SLSServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "response returned - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 467
    :cond_3
    if-eqz v7, :cond_4

    :try_start_3
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_4

    .line 468
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 473
    :cond_4
    :goto_3
    if-nez v5, :cond_8

    sget-object v9, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v8, v9}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 474
    if-eqz v5, :cond_5

    .line 475
    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;->setPrivacySettingId(Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingId;)V

    :cond_5
    move-object v6, v5

    .line 477
    .end local v5    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    .restart local v6    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    goto :goto_2

    .line 458
    .end local v6    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    .restart local v5    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySetting;
    :catch_0
    move-exception v2

    .line 459
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_4
    const-string v9, "SLSServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "failed to send attachement message with exception "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    instance-of v9, v2, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v9, :cond_7

    .line 461
    check-cast v2, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v2    # "ex":Ljava/lang/Exception;
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 466
    :catchall_0
    move-exception v9

    .line 467
    if-eqz v7, :cond_6

    :try_start_5
    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v10, :cond_6

    .line 468
    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 470
    :cond_6
    :goto_5
    throw v9

    .line 463
    .restart local v2    # "ex":Ljava/lang/Exception;
    :cond_7
    :try_start_6
    new-instance v9, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0x2166

    invoke-direct {v9, v10, v11, v2}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 473
    .end local v2    # "ex":Ljava/lang/Exception;
    :cond_8
    sget-object v9, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 469
    :catch_1
    move-exception v10

    goto :goto_5

    :catch_2
    move-exception v9

    goto :goto_3

    :catch_3
    move-exception v9

    goto :goto_1
.end method

.method public getProfileActivityFeedInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3356
    const-string v5, "SLSServiceManager"

    const-string v8, "getting profile activity feed info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3357
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_3

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3358
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3359
    const/4 v2, 0x0

    .line 3360
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileActivityFeedUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v9, v6, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3362
    .local v4, "url":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3363
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "&contToken=%s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p2, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3365
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3367
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3368
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3369
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3370
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "13"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3372
    const/4 v3, 0x0

    .line 3375
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3377
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_1

    .line 3378
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 3389
    :cond_1
    if-eqz v3, :cond_2

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 3390
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 3395
    :cond_2
    :goto_2
    if-nez v2, :cond_7

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3396
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_3
    move v5, v7

    .line 3357
    goto/16 :goto_0

    :cond_4
    move v5, v7

    .line 3358
    goto/16 :goto_1

    .line 3380
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3381
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get profile activity info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3382
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_6

    .line 3383
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3388
    :catchall_0
    move-exception v5

    .line 3389
    if-eqz v3, :cond_5

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_5

    .line 3390
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 3392
    :cond_5
    :goto_4
    throw v5

    .line 3385
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xc85

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3395
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_7
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3

    .line 3391
    :catch_1
    move-exception v6

    goto :goto_4

    :catch_2
    move-exception v5

    goto :goto_2
.end method

.method public getProfileStatisticsInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;
    .locals 1
    .param p1, "requestBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1753
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileStatisticsUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getProfileStatisticsInfoInternal(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$ProfileStatisticsResult;

    move-result-object v0

    return-object v0
.end method

.method public getProfileSummaryInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    .locals 9
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2642
    const-string v5, "SLSServiceManager"

    const-string v8, "getting profile summary info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2643
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2644
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2645
    const/4 v2, 0x0

    .line 2646
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileSummaryUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2648
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2650
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2651
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2652
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "2"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2654
    const/4 v3, 0x0

    .line 2657
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 2659
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 2660
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v6, Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2671
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 2672
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2677
    :cond_1
    :goto_2
    if-nez v2, :cond_6

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2678
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 2643
    goto :goto_0

    :cond_3
    move v5, v7

    .line 2644
    goto :goto_1

    .line 2662
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2663
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get profile summary info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2664
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 2665
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2670
    :catchall_0
    move-exception v5

    .line 2671
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 2672
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2674
    :cond_4
    :goto_4
    throw v5

    .line 2667
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbba

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2677
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    :cond_6
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3

    .line 2673
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    :catch_1
    move-exception v6

    goto :goto_4

    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;
    :catch_2
    move-exception v5

    goto :goto_2
.end method

.method public getProfileUsers(Ljava/util/Collection;[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    .locals 8
    .param p2, "settings"    # [Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;",
            ")",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3226
    .local p1, "xuids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const-string v5, "SLSServiceManager"

    const-string v6, "getProfileUsers"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3227
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_1

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3229
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3231
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3232
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3233
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3234
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "2"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3236
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUserProfileInfoUrl()Ljava/lang/String;

    move-result-object v4

    .line 3238
    .local v4, "url":Ljava/lang/String;
    const/4 v3, 0x0

    .line 3239
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 3242
    .local v2, "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    :try_start_0
    new-instance v5, Lcom/microsoft/xbox/service/network/managers/ProfileUserRequestBody;

    invoke-direct {v5, p1, p2}, Lcom/microsoft/xbox/service/network/managers/ProfileUserRequestBody;-><init>(Ljava/util/Collection;[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)V

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3243
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 3244
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v6, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    check-cast v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3253
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    :cond_0
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    .line 3256
    return-object v2

    .line 3227
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionResult$ProfileUsers;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 3246
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3247
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get FeedItemActionResult "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3248
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3253
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v5

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    throw v5

    .line 3249
    :catch_1
    move-exception v0

    .line 3250
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get FeedItemActionResult "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3251
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x232e

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getRecent360ProgressAndAchievementInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;
    .locals 12
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "titleId"    # Ljava/lang/String;
    .param p3, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1676
    const-string v5, "SLSServiceManager"

    const-string v8, "getting user\'s recent 360 app progress and achievement info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_4

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1678
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1679
    const/4 v2, 0x0

    .line 1680
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getRecentProgressAndAchievementInfoUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v9, v6, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1681
    .local v4, "url":Ljava/lang/String;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s%s"

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v4, v9, v7

    const-string v10, "?platforms=1,2,15,16,17&types=1,3&orderBy=unlockTime"

    aput-object v10, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1683
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1684
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s&continuationToken=%s"

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v4, v9, v7

    aput-object p3, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1687
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1688
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s&titleId=%s"

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v4, v9, v7

    aput-object p2, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1691
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1693
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1694
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1695
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "4"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1696
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1698
    const/4 v3, 0x0

    .line 1701
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1703
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_2

    .line 1704
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1715
    :cond_2
    if-eqz v3, :cond_3

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 1716
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1721
    :cond_3
    :goto_2
    if-nez v2, :cond_8

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1722
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_4
    move v5, v7

    .line 1677
    goto/16 :goto_0

    :cond_5
    move v5, v7

    .line 1678
    goto/16 :goto_1

    .line 1706
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1707
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get user\'s recent 360 app progress and achievement info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1708
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_7

    .line 1709
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1714
    :catchall_0
    move-exception v5

    .line 1715
    if-eqz v3, :cond_6

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_6

    .line 1716
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1718
    :cond_6
    :goto_4
    throw v5

    .line 1711
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_7
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbd8

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1721
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_8
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3

    .line 1717
    :catch_1
    move-exception v6

    goto :goto_4

    :catch_2
    move-exception v5

    goto :goto_2
.end method

.method public getRecentsFromPeopleHub()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 663
    const-string v6, "SLSServiceManager"

    const-string v7, "getting recents info from people hub"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v7, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 665
    const/4 v3, 0x0

    .line 666
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMyRecentsFromPeopleHubUrl()Ljava/lang/String;

    move-result-object v5

    .line 667
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 669
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 670
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Contract-Version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 674
    const/4 v2, 0x0

    .line 675
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 678
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 680
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 681
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    const-class v8, Ljava/util/Date;

    new-instance v9, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;

    invoke-direct {v9}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;-><init>()V

    invoke-static {v6, v7, v8, v9}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 692
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    :cond_0
    if-eqz v4, :cond_1

    .line 693
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 696
    :cond_1
    return-object v3

    .line 664
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 684
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 685
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get recents info with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_4

    .line 687
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 692
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_3

    .line 693
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_3
    throw v6

    .line 689
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xfbd

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getScreenshotInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;
    .locals 12
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "screenshotId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0xbc0

    .line 743
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getScreenshotInfo(xuid: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " scid: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " screenshotId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 746
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 748
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getMediaHubService()Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    move-result-object v1

    .line 751
    .local v1, "mediaHubService":Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    :try_start_0
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;->with(Ljava/util/List;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;

    move-result-object v2

    .line 752
    .local v2, "request":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;
    invoke-interface {v1, v2}, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;->getScreenshots(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;)Lretrofit2/Call;

    move-result-object v6

    invoke-interface {v6}, Lretrofit2/Call;->execute()Lretrofit2/Response;

    move-result-object v4

    .line 754
    .local v4, "screenshotBatchResponse":Lretrofit2/Response;, "Lretrofit2/Response<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;>;"
    invoke-virtual {v4}, Lretrofit2/Response;->isSuccessful()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 755
    invoke-virtual {v4}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;->values()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 757
    .local v5, "screenshots":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;"
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 758
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;-><init>()V

    .line 759
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;
    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    invoke-static {v6}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataMapper;->from(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    move-result-object v6

    iput-object v6, v3, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;->screenshot:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .line 760
    return-object v3

    .line 764
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/CaptureContainer$ScreenshotResult;
    .end local v5    # "screenshots":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;"
    :cond_0
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to get screenshot: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lretrofit2/Response;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbc0

    invoke-direct {v6, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 767
    .end local v2    # "request":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$BatchRequest;
    .end local v4    # "screenshotBatchResponse":Lretrofit2/Response;, "Lretrofit2/Response<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ScreenshotBatchResponse;>;"
    :catch_0
    move-exception v0

    .line 768
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "SLSServiceManager"

    const-string v7, "Failed getScreenshotInfo"

    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 769
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-direct {v6, v10, v11, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
.end method

.method public getSkypeConversationMessages(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .locals 14
    .param p1, "skypeId"    # Ljava/lang/String;
    .param p2, "syncState"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3030
    const-string v7, "SLSServiceManager"

    const-string v8, "get user messages for conversation"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3031
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    const/4 v7, 0x1

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3032
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v8, :cond_4

    const/4 v7, 0x1

    :goto_1
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3033
    const/4 v5, 0x0

    .line 3034
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v3, 0x0

    .line 3035
    .local v3, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 3036
    .local v4, "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeConversationMessagesUrlFormat()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object p1, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 3038
    .local v6, "url":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 3039
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "&syncState=%s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p2, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 3042
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 3044
    .local v1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-client-type"

    const-string v9, "XboxApp"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3045
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3046
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-contract-version"

    const-string v9, "3"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3047
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-client-name"

    const-string v9, "Smartglass Android"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3048
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    sget-object v8, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v10, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3050
    sget-object v7, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v7}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 3051
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "RegistrationToken"

    sget-object v9, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v9}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3057
    :goto_2
    :try_start_0
    invoke-static {v6, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->getStreamAndStatusWithBadRequestResponseBody(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 3058
    const/16 v7, 0xc8

    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v7, v8, :cond_2

    .line 3059
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 3060
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getSkypeRegistrationTokenFromResponseHeaders([Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v2

    .line 3061
    .local v2, "registrationToken":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 3062
    sget-object v7, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v7, v2}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 3065
    :cond_1
    if-eqz v3, :cond_2

    .line 3066
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 3078
    .end local v2    # "registrationToken":Ljava/lang/String;
    :cond_2
    return-object v4

    .line 3031
    .end local v1    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .end local v3    # "response":Ljava/lang/String;
    .end local v4    # "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v6    # "url":Ljava/lang/String;
    :cond_3
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 3032
    :cond_4
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 3053
    .restart local v1    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .restart local v3    # "response":Ljava/lang/String;
    .restart local v4    # "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .restart local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v6    # "url":Ljava/lang/String;
    :cond_5
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Cookie"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "skypetoken=%s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    sget-object v13, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v13}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getToken()Lio/reactivex/Single;

    move-result-object v13

    invoke-virtual {v13}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3070
    :catch_0
    move-exception v0

    .line 3071
    .local v0, "ex":Ljava/lang/Exception;
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get skype conversation messages"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3072
    instance-of v7, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v7, :cond_6

    .line 3073
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 3075
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbeb

    invoke-direct {v7, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
.end method

.method public getSkypeConversationsList()Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2971
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getSkypeConversationsList(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;

    move-result-object v0

    return-object v0
.end method

.method public getSkypeConversationsList(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    .locals 14
    .param p1, "paginationUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2976
    const-string v7, "SLSServiceManager"

    const-string v8, "get the user conversations list from skype"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2977
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v8, :cond_3

    const/4 v7, 0x1

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2978
    const/4 v5, 0x0

    .line 2979
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v3, 0x0

    .line 2980
    .local v3, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2981
    .local v4, "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeConversationsListUrlFormat()Ljava/lang/String;

    move-result-object v6

    .line 2983
    .local v6, "url":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v1

    .line 2985
    .local v1, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-client-type"

    const-string v9, "XboxOne"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2986
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2987
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-contract-version"

    const-string v9, "3"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2988
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-client-name"

    const-string v9, "Smartglass Android"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2989
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    sget-object v8, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v10, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2991
    sget-object v7, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v7}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2992
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "RegistrationToken"

    sget-object v9, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v9}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2998
    :goto_2
    const/4 v7, 0x0

    :try_start_0
    invoke-static {v6, v1, v7}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->getStreamAndStatusWithLastRedirectUri(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 2999
    const/16 v7, 0xc8

    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v7, v8, :cond_1

    .line 3000
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 3001
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getSkypeRegistrationTokenFromResponseHeaders([Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v2

    .line 3002
    .local v2, "registrationToken":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 3003
    sget-object v7, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v7, v2}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 3006
    :cond_0
    if-eqz v3, :cond_1

    .line 3007
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;

    move-result-object v4

    .line 3011
    .end local v2    # "registrationToken":Ljava/lang/String;
    :cond_1
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 3012
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3024
    :cond_2
    return-object v4

    .line 2977
    .end local v1    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .end local v3    # "response":Ljava/lang/String;
    .end local v4    # "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    .end local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v6    # "url":Ljava/lang/String;
    :cond_3
    const/4 v7, 0x0

    goto/16 :goto_0

    .restart local v3    # "response":Ljava/lang/String;
    .restart local v4    # "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    .restart local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_4
    move-object v6, p1

    .line 2981
    goto/16 :goto_1

    .line 2994
    .restart local v1    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .restart local v6    # "url":Ljava/lang/String;
    :cond_5
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Cookie"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "skypetoken=%s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    sget-object v13, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v13}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getToken()Lio/reactivex/Single;

    move-result-object v13

    invoke-virtual {v13}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3015
    :catch_0
    move-exception v0

    .line 3016
    .local v0, "ex":Ljava/lang/Exception;
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get skype conversation list"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3017
    instance-of v7, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v7, :cond_6

    .line 3018
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 3021
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbeb

    invoke-direct {v7, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
.end method

.method public getSkypeToken()Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2156
    const-string v4, "SLSServiceManager"

    const-string v5, "getting Skype token"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2158
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeTokenExchangeUrl()Ljava/lang/String;

    move-result-object v3

    .line 2159
    .local v3, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2161
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Accept"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2162
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "X-Skype-Caller"

    const-string v6, "Smartglass Android"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2163
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "X-Skype-Request-Id"

    const-string v6, "sandroid"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2164
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v7, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2166
    const/4 v2, 0x0

    .line 2169
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    const-string v4, ""

    invoke-static {v3, v1, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 2171
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v4, v5, :cond_2

    .line 2174
    :try_start_1
    const-string v4, "SLSServiceManager"

    const-string v5, "Successfully retrieved skype token"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2175
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v5, Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2193
    if-eqz v2, :cond_0

    :try_start_2
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_0

    .line 2194
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 2175
    :cond_0
    :goto_0
    return-object v4

    .line 2176
    :catch_0
    move-exception v0

    .line 2177
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x3fe

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2184
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 2185
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_4
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-nez v4, :cond_3

    .line 2186
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to get user token "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2187
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x3f4    # 5.0E-321

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2192
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 2193
    if-eqz v2, :cond_1

    :try_start_5
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 2194
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 2196
    :cond_1
    :goto_1
    throw v4

    .line 2181
    :cond_2
    :try_start_6
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user token endpoint returned error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2182
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x3f4    # 5.0E-321

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2189
    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_7
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "e":Ljava/lang/Exception;
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2195
    :catch_2
    move-exception v5

    goto :goto_1

    :catch_3
    move-exception v5

    goto :goto_0
.end method

.method public getSmartglassSettings()Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2716
    const-string v4, "SLSServiceManager"

    const-string v5, "getting smartglass settings"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2717
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v5, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2718
    const/4 v1, 0x0

    .line 2719
    .local v1, "result":Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSmartglassSettingsUrl()Ljava/lang/String;

    move-result-object v3

    .line 2721
    .local v3, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2724
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 2726
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v4, v5, :cond_0

    .line 2727
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v5, Lcom/microsoft/xbox/xle/app/SmartglassSettings;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "result":Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    check-cast v1, Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2738
    .restart local v1    # "result":Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    :cond_0
    if-eqz v2, :cond_1

    :try_start_1
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_1

    .line 2739
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2744
    :cond_1
    :goto_1
    if-nez v1, :cond_5

    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2745
    return-object v1

    .line 2717
    .end local v1    # "result":Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 2729
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2730
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to get smartglass settings "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2731
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_4

    .line 2732
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2737
    :catchall_0
    move-exception v4

    .line 2738
    if-eqz v2, :cond_3

    :try_start_3
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 2739
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2741
    :cond_3
    :goto_3
    throw v4

    .line 2734
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x1389

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2744
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v1    # "result":Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    :cond_5
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2740
    .end local v1    # "result":Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    :catch_1
    move-exception v5

    goto :goto_3

    .restart local v1    # "result":Lcom/microsoft/xbox/xle/app/SmartglassSettings;
    :catch_2
    move-exception v4

    goto :goto_1
.end method

.method public getSocialTitleVips(Ljava/lang/String;J)Ljava/util/ArrayList;
    .locals 12
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "gameTitleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1292
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v10, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v10, :cond_1

    move v7, v8

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1293
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    move v7, v8

    :goto_1
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1295
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1297
    .local v3, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1298
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v10, "Content-type"

    const-string v11, "application/json"

    invoke-direct {v7, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1299
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v10, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v7, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1300
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v10, "X-XBL-Contract-Version"

    const-string v11, "1"

    invoke-direct {v7, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1302
    const/4 v4, 0x0

    .line 1303
    .local v4, "result":[Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;
    const/4 v5, 0x0

    .line 1306
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getGameProfileVipsUrlFormat()Ljava/lang/String;

    move-result-object v10

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v8, v9

    invoke-static {v7, v10, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1309
    .local v6, "url":Ljava/lang/String;
    :try_start_0
    invoke-static {v6, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 1311
    const/16 v7, 0xc8

    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v7, v8, :cond_3

    .line 1312
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v8, [Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, [Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;

    move-object v4, v0

    .line 1314
    if-eqz v4, :cond_3

    .line 1315
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1331
    if-eqz v5, :cond_0

    :try_start_1
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_0

    .line 1332
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 1338
    :cond_0
    :goto_2
    return-object v7

    .end local v3    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v4    # "result":[Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;
    .end local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v6    # "url":Ljava/lang/String;
    :cond_1
    move v7, v9

    .line 1292
    goto/16 :goto_0

    :cond_2
    move v7, v9

    .line 1293
    goto :goto_1

    .line 1331
    .restart local v3    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v4    # "result":[Lcom/microsoft/xbox/service/model/ISocialVipsData$SocialVipData;
    .restart local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v6    # "url":Ljava/lang/String;
    :cond_3
    if-eqz v5, :cond_4

    :try_start_2
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 1332
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 1337
    :cond_4
    :goto_3
    if-nez v4, :cond_7

    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v6, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1338
    const/4 v7, 0x0

    goto :goto_2

    .line 1318
    :catch_0
    move-exception v2

    .line 1319
    .local v2, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_3
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get suggested players"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321
    const-wide/16 v8, 0x15

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-nez v7, :cond_5

    .line 1322
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1331
    if-eqz v5, :cond_0

    :try_start_4
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_0

    .line 1332
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 1333
    :catch_1
    move-exception v8

    goto :goto_2

    .line 1324
    :cond_5
    :try_start_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1330
    .end local v2    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v7

    .line 1331
    if-eqz v5, :cond_6

    :try_start_6
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_6

    .line 1332
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1334
    :cond_6
    :goto_5
    throw v7

    .line 1326
    :catch_2
    move-exception v2

    .line 1328
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_7
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xb

    invoke-direct {v7, v8, v9, v2}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1337
    .end local v2    # "ex":Ljava/lang/Exception;
    :cond_7
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 1333
    :catch_3
    move-exception v8

    goto :goto_5

    :catch_4
    move-exception v7

    goto :goto_3

    :catch_5
    move-exception v8

    goto :goto_2
.end method

.method public getTitleActivityFeedInfo(JLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 11
    .param p1, "titleId"    # J
    .param p3, "startDateTime"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "continuationToken"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3628
    const-string v5, "SLSServiceManager"

    const-string v8, "getting title activity feed info"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3629
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3630
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 3632
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getTitleActivityFeedUrlFormat()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v7

    aput-object p3, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3634
    .local v4, "url":Ljava/lang/String;
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3635
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "&contToken=%s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p4, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3637
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3639
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3640
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3641
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3642
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "13"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3644
    const/4 v3, 0x0

    .line 3645
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 3648
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3649
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_1

    .line 3650
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 3659
    :cond_1
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    .line 3662
    if-nez v2, :cond_3

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_1
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3664
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 3629
    goto/16 :goto_0

    .line 3652
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3653
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get title activity info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3654
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3659
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v5

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    throw v5

    .line 3655
    :catch_1
    move-exception v0

    .line 3656
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get title activity info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3657
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xc85

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3662
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_1
.end method

.method public getTitleFollowingState(Ljava/lang/String;)Ljava/util/Set;
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 3820
    const-string v6, "SLSServiceManager"

    const-string v9, "getting title following state"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3821
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_2

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3823
    const/4 v4, 0x0

    .line 3826
    .local v4, "titleSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 3827
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMyTitleFollowingUrlFormat()Ljava/lang/String;

    move-result-object v5

    .line 3832
    .local v5, "url":Ljava/lang/String;
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3833
    .local v2, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3834
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3835
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Contract-Version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3837
    const/4 v3, 0x0

    .line 3840
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3842
    const/16 v6, 0xc8

    iget v7, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 3843
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/model/UserTitlesFollowingData;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UserTitlesFollowingData;

    .line 3844
    .local v0, "data":Lcom/microsoft/xbox/service/model/UserTitlesFollowingData;
    if-eqz v0, :cond_0

    .line 3845
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UserTitlesFollowingData;->getHashSet()Ljava/util/Set;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 3857
    .end local v0    # "data":Lcom/microsoft/xbox/service/model/UserTitlesFollowingData;
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_1

    .line 3858
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 3863
    :cond_1
    :goto_2
    if-nez v4, :cond_6

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3865
    return-object v4

    .end local v2    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "titleSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v5    # "url":Ljava/lang/String;
    :cond_2
    move v6, v8

    .line 3821
    goto :goto_0

    .line 3829
    .restart local v4    # "titleSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_3
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getTitleFollowingUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "url":Ljava/lang/String;
    goto :goto_1

    .line 3848
    .restart local v2    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v1

    .line 3849
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get title flowing titles with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3850
    instance-of v6, v1, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 3851
    check-cast v1, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v1    # "ex":Ljava/lang/Exception;
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3856
    :catchall_0
    move-exception v6

    .line 3857
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v7, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 3858
    iget-object v7, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 3860
    :cond_4
    :goto_4
    throw v6

    .line 3853
    .restart local v1    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x247c

    invoke-direct {v6, v8, v9, v1}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3863
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3

    .line 3859
    :catch_1
    move-exception v7

    goto :goto_4

    :catch_2
    move-exception v6

    goto :goto_2
.end method

.method public getTitleLeaderBoardInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    .locals 9
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "statName"    # Ljava/lang/String;
    .param p4, "skipToUser"    # Z
    .param p5, "sortOrder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1800
    const-string v5, "SLSServiceManager"

    const-string v6, "getting leaderboard info"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1801
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1802
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1804
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1805
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1806
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1808
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5, p4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getLeaderBoardStatUrlFormat(Z)Ljava/lang/String;

    move-result-object v4

    .line 1809
    .local v4, "url":Ljava/lang/String;
    if-eqz p4, :cond_3

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x2

    aput-object p3, v6, v7

    const/4 v7, 0x3

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    aput-object p5, v6, v7

    const/4 v7, 0x5

    aput-object p1, v6, v7

    invoke-static {v5, v4, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1811
    :goto_1
    const/4 v2, 0x0

    .line 1812
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    const/4 v3, 0x0

    .line 1815
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1817
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 1818
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1829
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 1830
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1835
    :cond_1
    :goto_2
    return-object v2

    .line 1801
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 1809
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v4    # "url":Ljava/lang/String;
    :cond_3
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x2

    aput-object p3, v6, v7

    const/4 v7, 0x3

    const/16 v8, 0x19

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    aput-object p5, v6, v7

    invoke-static {v5, v4, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 1820
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/TitleLeaderBoard$TitleLeaderBoardResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 1821
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get leaderboard info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1822
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 1823
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1828
    :catchall_0
    move-exception v5

    .line 1829
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 1830
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1832
    :cond_4
    :goto_3
    throw v5

    .line 1825
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x2134

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1831
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    goto :goto_3

    :catch_2
    move-exception v5

    goto :goto_2
.end method

.method public getTitleProgressInfo(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;
    .locals 12
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .local p2, "titleIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1840
    const-string v7, "SLSServiceManager"

    const-string v10, "getting user\'s title progress info"

    invoke-static {v7, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v10, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v10, :cond_1

    move v7, v8

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1842
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    move v7, v8

    :goto_1
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1843
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 1844
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_3

    move v7, v8

    :goto_2
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1845
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1846
    .local v0, "commaSeparatedTitleIds":Ljava/lang/StringBuilder;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1847
    .local v3, "id":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-lez v10, :cond_0

    .line 1848
    const-string v10, ","

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1851
    :cond_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .end local v0    # "commaSeparatedTitleIds":Ljava/lang/StringBuilder;
    .end local v3    # "id":Ljava/lang/String;
    :cond_1
    move v7, v9

    .line 1841
    goto :goto_0

    :cond_2
    move v7, v9

    .line 1842
    goto :goto_1

    :cond_3
    move v7, v9

    .line 1844
    goto :goto_2

    .line 1854
    .restart local v0    # "commaSeparatedTitleIds":Ljava/lang/StringBuilder;
    :cond_4
    const/4 v4, 0x0

    .line 1855
    .local v4, "result":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getTitleProgressUrlFormat()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    aput-object p1, v11, v9

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v11, v8

    invoke-static {v7, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1857
    .local v6, "url":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1859
    .local v2, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1860
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Content-type"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1861
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-contract-version"

    const-string v9, "4"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1862
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1864
    const/4 v5, 0x0

    .line 1867
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v6, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 1869
    const/16 v7, 0xc8

    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v7, v8, :cond_5

    .line 1870
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v7}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 1881
    :cond_5
    if-eqz v5, :cond_6

    :try_start_1
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_6

    .line 1882
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1887
    :cond_6
    :goto_4
    if-nez v4, :cond_9

    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_5
    invoke-static {v6, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1888
    return-object v4

    .line 1872
    :catch_0
    move-exception v1

    .line 1873
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to get user\'s title progress info with exception "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1874
    instance-of v7, v1, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v7, :cond_8

    .line 1875
    check-cast v1, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v1    # "ex":Ljava/lang/Exception;
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1880
    :catchall_0
    move-exception v7

    .line 1881
    if-eqz v5, :cond_7

    :try_start_3
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_7

    .line 1882
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1884
    :cond_7
    :goto_6
    throw v7

    .line 1877
    .restart local v1    # "ex":Ljava/lang/Exception;
    :cond_8
    :try_start_4
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbd8

    invoke-direct {v7, v8, v9, v1}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1887
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_9
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_5

    .line 1883
    :catch_1
    move-exception v8

    goto :goto_6

    :catch_2
    move-exception v7

    goto :goto_4
.end method

.method public getUnsharedActivityFeed(Ljava/lang/String;I)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "numItems"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3519
    const-string v5, "SLSServiceManager"

    const-string v8, "getUnsharedActivityFeed"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3520
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3521
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3523
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUnsharedActivityFeedUrlFormat()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object p1, v9, v7

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3525
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3527
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3528
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3529
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3530
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3531
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "13"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3533
    const/4 v2, 0x0

    .line 3535
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    const/4 v3, 0x0

    .line 3538
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3539
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 3540
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 3551
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 3552
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 3557
    :cond_1
    :goto_2
    if-nez v2, :cond_6

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3558
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 3520
    goto/16 :goto_0

    :cond_3
    move v5, v7

    .line 3521
    goto/16 :goto_1

    .line 3542
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3543
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get unshared feed with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3544
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 3545
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3550
    :catchall_0
    move-exception v5

    .line 3551
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 3552
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 3554
    :cond_4
    :goto_4
    throw v5

    .line 3547
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xc86

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3557
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3

    .line 3553
    :catch_1
    move-exception v6

    goto :goto_4

    :catch_2
    move-exception v5

    goto :goto_2
.end method

.method public getUserFollowingPages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    .locals 11
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 3905
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_2

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3906
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v5, "me"

    .line 3907
    .local v5, "xuidStr":Ljava/lang/String;
    :goto_1
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUserPagesUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v5, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3909
    .local v4, "url":Ljava/lang/String;
    const/4 v2, 0x0

    .line 3910
    .local v2, "result":Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3911
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3912
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3913
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "X-XBL-Contract-Version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3916
    const/4 v3, 0x0

    .line 3919
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3921
    const/16 v6, 0xc8

    iget v7, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 3922
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    check-cast v2, Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3933
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_1

    .line 3934
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 3939
    :cond_1
    :goto_2
    if-nez v2, :cond_6

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_3
    invoke-static {v4, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3941
    return-object v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    .end local v5    # "xuidStr":Ljava/lang/String;
    :cond_2
    move v6, v8

    .line 3905
    goto :goto_0

    .line 3906
    :cond_3
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "xuid(%s)"

    new-array v10, v7, [Ljava/lang/Object;

    aput-object p1, v10, v8

    invoke-static {v6, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 3924
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    .restart local v5    # "xuidStr":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3925
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get user following pages with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3926
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 3927
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3932
    :catchall_0
    move-exception v6

    .line 3933
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v7, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 3934
    iget-object v7, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 3936
    :cond_4
    :goto_4
    throw v6

    .line 3929
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x247c

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3939
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    :cond_6
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_3

    .line 3935
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    :catch_1
    move-exception v7

    goto :goto_4

    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/usertitles/FollowingPages;
    :catch_2
    move-exception v6

    goto :goto_2
.end method

.method public getUserListPresenceInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .locals 8
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1136
    const-string v5, "SLSServiceManager"

    const-string v6, "getting presence info for list of users"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1137
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1138
    const/4 v2, 0x0

    .line 1139
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUserPresenceInfoUrl()Ljava/lang/String;

    move-result-object v4

    .line 1140
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1142
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1143
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1144
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-XBL-Contract-Version"

    const-string v7, "3"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1146
    const/4 v3, 0x0

    .line 1149
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 1151
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 1152
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;->deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1163
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 1164
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1169
    :cond_1
    :goto_1
    if-nez v2, :cond_5

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 1170
    return-object v2

    .line 1137
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 1154
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1155
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to get user list presence info with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_4

    .line 1157
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1162
    :catchall_0
    move-exception v5

    .line 1163
    if-eqz v3, :cond_3

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 1164
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1166
    :cond_3
    :goto_3
    throw v5

    .line 1159
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbc5

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1169
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 1165
    :catch_1
    move-exception v6

    goto :goto_3

    :catch_2
    move-exception v5

    goto :goto_1
.end method

.method public getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .locals 10
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 167
    const-string v6, "SLSServiceManager"

    const-string v7, "getting user info"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v7, :cond_3

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 169
    const/4 v3, 0x0

    .line 170
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUserProfileInfoUrl()Ljava/lang/String;

    move-result-object v5

    .line 171
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 173
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 174
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "x-xbl-contract-version"

    const-string v8, "2"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    const/4 v2, 0x0

    .line 178
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 181
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 183
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 184
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 187
    :cond_0
    if-eqz v2, :cond_1

    .line 188
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 199
    :cond_1
    if-eqz v4, :cond_2

    .line 200
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 204
    :cond_2
    if-nez v3, :cond_6

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_1
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 205
    return-object v3

    .line 168
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_3
    const/4 v6, 0x0

    goto :goto_0

    .line 191
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 192
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get user info with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 194
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_4

    .line 200
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_4
    throw v6

    .line 196
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_2
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbba

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 204
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_1
.end method

.method public getUserProfilePrivacySettings()Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 387
    const-string v6, "SLSServiceManager"

    const-string v8, "getting user profile privacy settings"

    invoke-static {v6, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v8, :cond_2

    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 390
    const/4 v3, 0x0

    .line 391
    .local v3, "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUserProfileSettingUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v6, v8, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 392
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 394
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 395
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "x-xbl-contract-version"

    const-string v8, "4"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    const/4 v2, 0x0

    .line 399
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 402
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 404
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 405
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v7, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    check-cast v3, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    .restart local v3    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    :cond_0
    if-eqz v4, :cond_1

    .line 416
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 420
    :cond_1
    return-object v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_2
    move v6, v7

    .line 388
    goto :goto_0

    .line 407
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 408
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to get user settings"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_4

    .line 410
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_3

    .line 416
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_3
    throw v6

    .line 412
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x2166

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getXTokenPrivileges()[I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "privileges":[I
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v1

    if-nez v1, :cond_0

    .line 160
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getPrivileges()[I

    move-result-object v0

    .line 162
    :cond_0
    return-object v0
.end method

.method public getXuidString()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "xuid":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->isUsingStub()Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    const-string v1, "https://xboxlive.com"

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getXuidFromToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 153
    :cond_0
    return-object v0
.end method

.method public markConversationRead(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "meXuid"    # Ljava/lang/String;
    .param p2, "targetXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3160
    const-string v5, "SLSServiceManager"

    const-string v8, "mark the user conversation as read"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3161
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3162
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3163
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_2
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3164
    const/4 v3, 0x0

    .line 3165
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 3166
    .local v2, "response":Ljava/lang/String;
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getConversationsDetailUrlFormat()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    aput-object p1, v9, v7

    aput-object p2, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3168
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3170
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3171
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3172
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3175
    const/4 v5, 0x0

    :try_start_0
    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3176
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 3177
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 3189
    :cond_0
    return-void

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    move v5, v7

    .line 3161
    goto :goto_0

    :cond_2
    move v5, v7

    .line 3162
    goto :goto_1

    :cond_3
    move v5, v7

    .line 3163
    goto :goto_2

    .line 3180
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 3181
    .local v0, "ex":Ljava/lang/Exception;
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to mark conversation as read"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3182
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_4

    .line 3183
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 3186
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbeb

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
.end method

.method public postComment(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    .locals 13
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "itemRoot"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 3261
    const-string v7, "SLSServiceManager"

    const-string v10, "postComment"

    invoke-static {v7, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3262
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    sget-object v10, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v7, v10, :cond_0

    move v7, v8

    :goto_0
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3263
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    move v7, v8

    :goto_1
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3265
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "%s/%s/comments"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getCommentsServiceUrl()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v9

    aput-object p2, v11, v8

    invoke-static {v7, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 3267
    .local v6, "url":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3269
    .local v3, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3270
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Content-type"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3271
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3272
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Accept"

    const-string v9, "application/json"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3273
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "x-xbl-contract-version"

    const-string v9, "3"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3275
    const/4 v5, 0x0

    .line 3276
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v4, 0x0

    .line 3279
    .local v4, "result":Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    :try_start_0
    new-instance v7, Lcom/microsoft/xbox/service/network/managers/PostCommentRequestBody;

    move-object/from16 v0, p3

    invoke-direct {v7, p1, v0}, Lcom/microsoft/xbox/service/network/managers/PostCommentRequestBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v3, v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 3280
    iget-object v7, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v8, Lcom/microsoft/xbox/service/network/managers/PostCommentResult;

    const-class v9, Ljava/util/Date;

    new-instance v10, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;

    invoke-direct {v10}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;-><init>()V

    invoke-static {v7, v8, v9, v10}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/PostCommentResult;

    move-object v4, v0

    .line 3281
    const/16 v7, 0xc8

    iget v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v7, v8, :cond_2

    .line 3282
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x232f

    invoke-direct {v7, v8, v9}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v7
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3284
    :catch_0
    move-exception v2

    .line 3285
    .local v2, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to post comment "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3286
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v6, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3287
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3293
    .end local v2    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v7

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    throw v7

    .end local v3    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    .end local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v6    # "url":Ljava/lang/String;
    :cond_0
    move v7, v9

    .line 3262
    goto/16 :goto_0

    :cond_1
    move v7, v9

    .line 3263
    goto/16 :goto_1

    .line 3293
    .restart local v3    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v4    # "result":Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    .restart local v5    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v6    # "url":Ljava/lang/String;
    :cond_2
    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    .line 3295
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v6, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3297
    return-object v4

    .line 3288
    :catch_1
    move-exception v2

    .line 3289
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v7, "SLSServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "failed to post comment "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3290
    sget-object v7, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v6, v7}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3291
    new-instance v7, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0x232f

    invoke-direct {v7, v8, v9, v2}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public postTextToFeed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x232a

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 3570
    const-string v4, "SLSServiceManager"

    const-string v7, "postTextToFeed"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3571
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v7, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3572
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    move v4, v5

    :goto_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3574
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getActivitiyFeedTextPostUrlFormat()Ljava/lang/String;

    move-result-object v7

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-static {v4, v7, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3576
    .local v3, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3578
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3579
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3580
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3581
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Accept"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3582
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "13"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3585
    :try_start_0
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->buildFeedPostMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 3586
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/StreamUtil;->consumeAndClose(Ljava/io/InputStream;)V

    .line 3587
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_2

    .line 3588
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x232a

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 3590
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 3591
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to update status "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3592
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3593
    throw v0

    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v3    # "url":Ljava/lang/String;
    :cond_0
    move v4, v6

    .line 3571
    goto/16 :goto_0

    :cond_1
    move v4, v6

    .line 3572
    goto/16 :goto_1

    .line 3594
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v3    # "url":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 3595
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to update status "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3596
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3597
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-direct {v4, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4

    .line 3599
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_2
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3600
    return-void
.end method

.method public removeFriendFromShareIdentitySetting(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 246
    const-string v5, "SLSServiceManager"

    const-string v8, "Removing friends to share identity"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 248
    const/4 v2, 0x0

    .line 249
    .local v2, "result":Z
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getRemoveUsersFromShareIdentityUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 250
    .local v4, "url":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 251
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 252
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "4"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    const/4 v1, 0x0

    .line 255
    .local v1, "response":Ljava/lang/String;
    const/4 v3, 0x0

    .line 257
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v0, p2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 259
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_2

    const/16 v5, 0xcc

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_2

    .line 261
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbe1

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    :catchall_0
    move-exception v5

    .line 266
    if-eqz v3, :cond_0

    :try_start_1
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_0

    .line 267
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 269
    :cond_0
    :goto_1
    throw v5

    .end local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v1    # "response":Ljava/lang/String;
    .end local v2    # "result":Z
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    move v5, v7

    .line 247
    goto :goto_0

    .line 263
    .restart local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v1    # "response":Ljava/lang/String;
    .restart local v2    # "result":Z
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x1

    .line 266
    if-eqz v3, :cond_3

    :try_start_2
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 267
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 271
    :cond_3
    :goto_2
    return v2

    .line 268
    :catch_0
    move-exception v6

    goto :goto_1

    :catch_1
    move-exception v5

    goto :goto_2
.end method

.method public removeUserFromFavoriteList(Ljava/lang/String;)Z
    .locals 10
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2484
    const-string v5, "SLSServiceManager"

    const-string v8, "removing user from favorite list"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2485
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_3

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2486
    const/4 v2, 0x0

    .line 2487
    .local v2, "result":Z
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileFavoriteListUrl()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    const-string v9, "remove"

    aput-object v9, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2488
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2490
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2491
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2492
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2494
    const/4 v3, 0x0

    .line 2497
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 2499
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_0

    const/16 v5, 0xcc

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v5, v6, :cond_1

    .line 2500
    :cond_0
    const/4 v2, 0x1

    .line 2511
    :cond_1
    if-eqz v3, :cond_2

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 2512
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2517
    :cond_2
    :goto_1
    if-nez v2, :cond_6

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2518
    return v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_3
    move v5, v7

    .line 2485
    goto :goto_0

    .line 2502
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2503
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to remove user from favorite list with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2504
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 2505
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2510
    :catchall_0
    move-exception v5

    .line 2511
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 2512
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2514
    :cond_4
    :goto_3
    throw v5

    .line 2507
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xf9b

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2517
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2513
    :catch_1
    move-exception v6

    goto :goto_3

    :catch_2
    move-exception v5

    goto :goto_1
.end method

.method public removeUserFromFollowingList(Ljava/lang/String;)Z
    .locals 10
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2576
    const-string v5, "SLSServiceManager"

    const-string v8, "removing user from following list"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2577
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_3

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2578
    const/4 v2, 0x0

    .line 2579
    .local v2, "result":Z
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateProfileFollowingListUrl()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    const-string v9, "remove"

    aput-object v9, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2580
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2582
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2583
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2584
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2586
    const/4 v3, 0x0

    .line 2589
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 2591
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_0

    const/16 v5, 0xcc

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v5, v6, :cond_1

    .line 2592
    :cond_0
    const/4 v2, 0x1

    .line 2603
    :cond_1
    if-eqz v3, :cond_2

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 2604
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2609
    :cond_2
    :goto_1
    if-nez v2, :cond_6

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2610
    return v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_3
    move v5, v7

    .line 2577
    goto :goto_0

    .line 2594
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2595
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to remove user from following list with exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2596
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_5

    .line 2597
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2602
    :catchall_0
    move-exception v5

    .line 2603
    if-eqz v3, :cond_4

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_4

    .line 2604
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2606
    :cond_4
    :goto_3
    throw v5

    .line 2599
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbc4

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2609
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2605
    :catch_1
    move-exception v6

    goto :goto_3

    :catch_2
    move-exception v5

    goto :goto_1
.end method

.method public removeUserFromNeverList(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2409
    const-string v6, "SLSServiceManager"

    const-string v9, "removing user from never list"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_1

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2411
    const/4 v3, 0x0

    .line 2412
    .local v3, "result":Z
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getProfileNeverListUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2413
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2415
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2416
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2417
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "x-xbl-contract-version"

    const-string v8, "1"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2419
    const/4 v2, 0x0

    .line 2420
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2423
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1, p2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->delete(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 2433
    if-eqz v4, :cond_0

    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_0

    .line 2434
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2439
    :cond_0
    :goto_1
    if-nez v3, :cond_4

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2440
    return v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Z
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_1
    move v6, v8

    .line 2410
    goto :goto_0

    .line 2424
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "result":Z
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2425
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to remove user from never list with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_3

    .line 2427
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2432
    :catchall_0
    move-exception v6

    .line 2433
    if-eqz v4, :cond_2

    :try_start_3
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_2

    .line 2434
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2436
    :cond_2
    :goto_3
    throw v6

    .line 2429
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    :try_start_4
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xf9d

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2439
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2435
    :catch_1
    move-exception v7

    goto :goto_3

    :catch_2
    move-exception v6

    goto :goto_1
.end method

.method public removeUserFromRecommendationList(Ljava/lang/String;)Z
    .locals 8
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2615
    const-string v4, "SLSServiceManager"

    const-string v7, "removing user from recommendation list"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2616
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v7, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2617
    const/4 v2, 0x0

    .line 2618
    .local v2, "result":Z
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->removeUserFromRecommendationListUrlFormat()Ljava/lang/String;

    move-result-object v7

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-static {v4, v7, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2619
    .local v3, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2621
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2622
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2623
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "1"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2626
    :try_start_0
    invoke-static {v3, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->delete(Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2636
    if-nez v2, :cond_2

    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_1
    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2637
    return v2

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    .end local v3    # "url":Ljava/lang/String;
    :cond_0
    move v4, v6

    .line 2616
    goto :goto_0

    .line 2627
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    .restart local v3    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2628
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to remove user from recommendation list with exception "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2629
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_1

    .line 2630
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0

    .line 2632
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_1
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbc4

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4

    .line 2636
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_2
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_1
.end method

.method public resetNewCommentAlert(Ljava/lang/String;)V
    .locals 12
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0xbe1

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 842
    const-string v4, "SLSServiceManager"

    const-string v7, "resetNewCommentAlert"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v7, :cond_1

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 844
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v5

    :goto_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 846
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->resetNewCommentAlertUrl()Ljava/lang/String;

    move-result-object v7

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-static {v4, v7, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 848
    .local v3, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 850
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 851
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 852
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Accept"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 853
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "3"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 854
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 858
    :try_start_0
    invoke-static {v3, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 859
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/StreamUtil;->consumeAndClose(Ljava/io/InputStream;)V

    .line 860
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v4, v5, :cond_0

    const/16 v4, 0xcc

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_3

    .line 862
    :cond_0
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbe1

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 864
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 865
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to reset new comment alert "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 867
    throw v0

    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v3    # "url":Ljava/lang/String;
    :cond_1
    move v4, v6

    .line 843
    goto/16 :goto_0

    :cond_2
    move v4, v6

    .line 844
    goto/16 :goto_1

    .line 868
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v3    # "url":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 869
    .local v0, "ex":Ljava/lang/Exception;
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to reset new commnent alert "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 872
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-direct {v4, v10, v11, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4

    .line 874
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->resetNewFollowerAlertUrl()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 875
    return-void
.end method

.method public resetNewFollowerAlert(Ljava/lang/String;)V
    .locals 10
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0xbe1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 807
    const-string v3, "SLSServiceManager"

    const-string v6, "resetNewFollowerAlert"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v3, v6, :cond_1

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 809
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 811
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 813
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 814
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Content-type"

    const-string v5, "application/json"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 815
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Accept"

    const-string v5, "application/json"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 816
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "x-xbl-contract-version"

    const-string v5, "1"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 817
    new-instance v3, Lorg/apache/http/message/BasicHeader;

    const-string v4, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 821
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->resetNewFollowerAlertUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 822
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    iget-object v3, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/StreamUtil;->consumeAndClose(Ljava/io/InputStream;)V

    .line 823
    const/16 v3, 0xc8

    iget v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v3, v4, :cond_0

    const/16 v3, 0xcc

    iget v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v3, v4, :cond_3

    .line 825
    :cond_0
    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0xbe1

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v3
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 827
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v0

    .line 828
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v3, "SLSServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to reset new follower alert "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->resetNewFollowerAlertUrl()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 830
    throw v0

    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :cond_1
    move v3, v5

    .line 808
    goto/16 :goto_0

    :cond_2
    move v4, v5

    .line 809
    goto/16 :goto_1

    .line 831
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    :catch_1
    move-exception v0

    .line 832
    .local v0, "ex":Ljava/lang/Exception;
    const-string v3, "SLSServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed to reset new follower alert "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->resetNewFollowerAlertUrl()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 835
    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-direct {v3, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v3

    .line 837
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->resetNewFollowerAlertUrl()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v3, v4}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 838
    return-void
.end method

.method public sendMeLike(Ljava/lang/String;Z)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .locals 12
    .param p1, "urlPath"    # Ljava/lang/String;
    .param p2, "currLikeState"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1033
    const-string v8, "SLSServiceManager"

    const-string v11, "sending like action"

    invoke-static {v8, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1034
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    sget-object v11, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v8, v11, :cond_0

    move v8, v9

    :goto_0
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1035
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    :goto_1
    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1036
    const/4 v3, 0x0

    .line 1038
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1039
    const-string v8, "SLSServiceManager"

    const-string v9, "Empty post body for send like action"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v3

    .line 1085
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .local v4, "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :goto_2
    return-object v4

    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :cond_0
    move v8, v10

    .line 1034
    goto :goto_0

    :cond_1
    move v9, v10

    .line 1035
    goto :goto_1

    .line 1043
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSendLikeUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1045
    .local v7, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1047
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1048
    new-instance v8, Lorg/apache/http/message/BasicHeader;

    const-string v9, "Content-type"

    const-string v10, "application/json"

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1050
    const/4 v2, 0x0

    .line 1051
    .local v2, "response":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1054
    .local v5, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v6, 0x0

    .line 1055
    .local v6, "unlikeStatus":I
    if-eqz p2, :cond_6

    .line 1056
    const/4 v8, 0x0

    :try_start_0
    invoke-static {v7, v1, v8}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v5

    .line 1061
    :goto_3
    if-eqz v5, :cond_3

    const/16 v8, 0xc8

    iget v9, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v8, v9, :cond_3

    .line 1062
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 1065
    :cond_3
    if-eqz v2, :cond_4

    .line 1066
    const-class v8, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    invoke-static {v2, v8}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1078
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :cond_4
    if-eqz v5, :cond_5

    :try_start_1
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v8, :cond_5

    .line 1079
    iget-object v8, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1084
    :cond_5
    :goto_4
    if-nez v3, :cond_9

    sget-object v8, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_5
    invoke-static {v7, v8}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    move-object v4, v3

    .line 1085
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .restart local v4    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    goto :goto_2

    .line 1058
    .end local v4    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :cond_6
    :try_start_2
    invoke-static {v7, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->deleteWithStatus(Ljava/lang/String;Ljava/util/List;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    goto :goto_3

    .line 1069
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :catch_0
    move-exception v0

    .line 1070
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v8, "SLSServiceManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "failed to send like action with exception "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    instance-of v8, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v8, :cond_8

    .line 1072
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1077
    :catchall_0
    move-exception v8

    .line 1078
    if-eqz v5, :cond_7

    :try_start_4
    iget-object v9, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_7

    .line 1079
    iget-object v9, v5, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1081
    :cond_7
    :goto_6
    throw v8

    .line 1074
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_8
    :try_start_5
    new-instance v8, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0xd1d

    invoke-direct {v8, v10, v11, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1084
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :cond_9
    sget-object v8, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_5

    .line 1080
    .end local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :catch_1
    move-exception v9

    goto :goto_6

    .restart local v3    # "result":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :catch_2
    move-exception v8

    goto :goto_4
.end method

.method public sendMessage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2202
    const-string v6, "SLSServiceManager"

    const-string v9, "sending message"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2203
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_3

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2204
    const/4 v3, 0x0

    .line 2205
    .local v3, "result":Z
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->sendMessageUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2206
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2208
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2209
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2210
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "x-xbl-contract-version"

    const-string v8, "2"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2212
    const/4 v2, 0x0

    .line 2213
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2216
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1, p2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 2218
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 2219
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 2220
    const/4 v3, 0x1

    .line 2223
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2224
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "response returned - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2236
    :cond_1
    if-eqz v4, :cond_2

    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    .line 2237
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2242
    :cond_2
    :goto_1
    if-nez v3, :cond_6

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2243
    return v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Z
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_3
    move v6, v8

    .line 2203
    goto :goto_0

    .line 2227
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "result":Z
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2228
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to semd message with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2229
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 2230
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2235
    :catchall_0
    move-exception v6

    .line 2236
    if-eqz v4, :cond_4

    :try_start_3
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 2237
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2239
    :cond_4
    :goto_3
    throw v6

    .line 2232
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbec

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2242
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2238
    :catch_1
    move-exception v7

    goto :goto_3

    :catch_2
    move-exception v6

    goto :goto_1
.end method

.method public sendMessageWithAttachement(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2248
    const-string v6, "SLSServiceManager"

    const-string v9, "sending message with attachement"

    invoke-static {v6, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2249
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v6, v9, :cond_3

    move v6, v7

    :goto_0
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2250
    const/4 v3, 0x0

    .line 2251
    .local v3, "result":Z
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->sendMessageWithAttachementUrlFormat()Ljava/lang/String;

    move-result-object v9

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v9, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2252
    .local v5, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2254
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 2255
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Content-type"

    const-string v8, "application/json"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2256
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "x-xbl-contract-version"

    const-string v8, "3"

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2257
    new-instance v6, Lorg/apache/http/message/BasicHeader;

    const-string v7, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2259
    const/4 v2, 0x0

    .line 2260
    .local v2, "response":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2263
    .local v4, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v5, v1, p2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v4

    .line 2265
    const/16 v6, 0xc8

    iget v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v6, v7, :cond_0

    .line 2266
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 2267
    const/4 v3, 0x1

    .line 2270
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2271
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "response returned - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2283
    :cond_1
    if-eqz v4, :cond_2

    :try_start_1
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_2

    .line 2284
    iget-object v6, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2289
    :cond_2
    :goto_1
    if-nez v3, :cond_6

    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v5, v6}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2290
    return v3

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "response":Ljava/lang/String;
    .end local v3    # "result":Z
    .end local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v5    # "url":Ljava/lang/String;
    :cond_3
    move v6, v8

    .line 2249
    goto/16 :goto_0

    .line 2274
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "response":Ljava/lang/String;
    .restart local v3    # "result":Z
    .restart local v4    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v5    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2275
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "SLSServiceManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to send attachement message with exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2276
    instance-of v6, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v6, :cond_5

    .line 2277
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2282
    :catchall_0
    move-exception v6

    .line 2283
    if-eqz v4, :cond_4

    :try_start_3
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v7, :cond_4

    .line 2284
    iget-object v7, v4, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2286
    :cond_4
    :goto_3
    throw v6

    .line 2279
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    :try_start_4
    new-instance v6, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v8, 0xbec

    invoke-direct {v6, v8, v9, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2289
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_6
    sget-object v6, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 2285
    :catch_1
    move-exception v7

    goto :goto_3

    :catch_2
    move-exception v6

    goto :goto_1
.end method

.method public sendShortCircuitProfile(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .locals 8
    .param p1, "request"    # Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4367
    const-string v5, "SLSServiceManager"

    const-string v6, "Sending ShortCircuitProfile message"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4368
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4369
    const/4 v2, 0x0

    .line 4371
    .local v2, "result":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getShortCircuitProfileUrlFormat()Ljava/lang/String;

    move-result-object v4

    .line 4373
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4374
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4375
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "PS-MSAAuthTicket"

    invoke-static {}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->getInstance()Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->getMsaTicket()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4376
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "PS-ApplicationId"

    const-string v7, "44445A65-4A71-4083-8C90-041A22856E69"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4377
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "PS-Scenario"

    const-string v7, "Test only"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4378
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-Type"

    const-string v7, "application/x-www-form-urlencoded"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4380
    const/4 v3, 0x0

    .line 4383
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileRequest;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 4385
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 4386
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;->parseJson(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 4397
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 4398
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 4403
    :cond_1
    :goto_1
    if-nez v2, :cond_5

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 4404
    return-object v2

    .line 4368
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 4388
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$ShortCircuitProfileResponse;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 4389
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to send ShortCircuitProfile message "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4390
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_4

    .line 4391
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4396
    :catchall_0
    move-exception v5

    .line 4397
    if-eqz v3, :cond_3

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 4398
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 4400
    :cond_3
    :goto_3
    throw v5

    .line 4393
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x1389

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4403
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 4399
    :catch_1
    move-exception v6

    goto :goto_3

    :catch_2
    move-exception v5

    goto :goto_1
.end method

.method public sendSkypeMessage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 16
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2296
    const-string v9, "SLSServiceManager"

    const-string v10, "sending skype message"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2297
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v9, v10, :cond_5

    const/4 v9, 0x1

    :goto_0
    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2298
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    const/4 v9, 0x1

    :goto_1
    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 2299
    const/4 v6, 0x0

    .line 2300
    .local v6, "result":Z
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSkypeSendMessageUrlFormat()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p1, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2302
    .local v8, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupService;->getHeaders()Ljava/util/List;

    move-result-object v3

    .line 2304
    .local v3, "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "x-xbl-client-type"

    const-string v11, "XboxApp"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2305
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "Accept"

    const-string v11, "application/json"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2306
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "x-xbl-contract-version"

    const-string v11, "3"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2307
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "x-xbl-client-name"

    const-string v11, "Smartglass Android"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2308
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    sget-object v10, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_HEADER:Ljava/lang/String;

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v12, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->SKYPE_CLIENT_INFO_FORMAT:Ljava/lang/String;

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v15

    invoke-virtual {v15}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v15

    invoke-virtual {v15}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2310
    sget-object v9, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v9}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 2311
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "RegistrationToken"

    sget-object v11, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v11}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getRegistrationToken()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2316
    :goto_2
    const/4 v5, 0x0

    .line 2317
    .local v5, "response":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2320
    .local v7, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v9, 0x0

    :try_start_0
    move-object/from16 v0, p2

    invoke-static {v8, v3, v0, v9}, Lcom/microsoft/xbox/service/groupMessaging/SkypeService;->postStreamAndStatusWithRedirect(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v7

    .line 2322
    if-eqz v7, :cond_0

    .line 2323
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 2324
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v9

    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->lastUri:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->updateSkypeHostname(Ljava/lang/String;)V

    .line 2328
    :cond_0
    const/16 v9, 0xc9

    iget v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v9, v10, :cond_2

    .line 2329
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v5

    .line 2330
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->headers:[Lorg/apache/http/Header;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->getSkypeRegistrationTokenFromResponseHeaders([Lorg/apache/http/Header;)Ljava/lang/String;

    move-result-object v4

    .line 2331
    .local v4, "registrationToken":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 2332
    sget-object v9, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v9, v4}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 2335
    :cond_1
    const/4 v6, 0x1

    .line 2338
    .end local v4    # "registrationToken":Ljava/lang/String;
    :cond_2
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 2339
    const-string v9, "SLSServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "response returned - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2351
    :cond_3
    if-eqz v7, :cond_4

    :try_start_1
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_4

    .line 2352
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2357
    :cond_4
    :goto_3
    if-nez v6, :cond_a

    sget-object v9, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_4
    invoke-static {v8, v9}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 2358
    return v6

    .line 2297
    .end local v3    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .end local v5    # "response":Ljava/lang/String;
    .end local v6    # "result":Z
    .end local v7    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v8    # "url":Ljava/lang/String;
    :cond_5
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 2298
    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 2313
    .restart local v3    # "headers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/Header;>;"
    .restart local v6    # "result":Z
    .restart local v8    # "url":Ljava/lang/String;
    :cond_7
    new-instance v9, Lorg/apache/http/message/BasicHeader;

    const-string v10, "Cookie"

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "skypetoken=%s"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    sget-object v15, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v15}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getToken()Lio/reactivex/Single;

    move-result-object v15

    invoke-virtual {v15}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2342
    .restart local v5    # "response":Ljava/lang/String;
    .restart local v7    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :catch_0
    move-exception v2

    .line 2343
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v9, "SLSServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "failed to send skype message with exception "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344
    instance-of v9, v2, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v9, :cond_9

    .line 2345
    check-cast v2, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v2    # "ex":Ljava/lang/Exception;
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2350
    :catchall_0
    move-exception v9

    .line 2351
    if-eqz v7, :cond_8

    :try_start_3
    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v10, :cond_8

    .line 2352
    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2354
    :cond_8
    :goto_5
    throw v9

    .line 2347
    .restart local v2    # "ex":Ljava/lang/Exception;
    :cond_9
    :try_start_4
    new-instance v9, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0xbec

    invoke-direct {v9, v10, v11, v2}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2357
    .end local v2    # "ex":Ljava/lang/Exception;
    :cond_a
    sget-object v9, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_4

    .line 2353
    :catch_1
    move-exception v10

    goto :goto_5

    :catch_2
    move-exception v9

    goto :goto_3
.end method

.method public setFollowingPageState(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .param p3, "follow"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 3946
    const-string v4, "SLSServiceManager"

    const-string v7, "change page following state"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3947
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v7, :cond_1

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3948
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3950
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUserPagesUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v5, v5, [Ljava/lang/Object;

    const-string v9, "me"

    aput-object v9, v5, v6

    invoke-static {v7, v8, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3952
    .local v3, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3953
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3954
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3955
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "X-XBL-Contract-Version"

    const-string v6, "1"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3957
    const/4 v2, 0x0

    .line 3960
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    if-eqz p3, :cond_2

    .line 3961
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v3, v1, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 3974
    :goto_1
    if-eqz v2, :cond_0

    :try_start_1
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_0

    .line 3975
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 3979
    :cond_0
    :goto_2
    return-void

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_1
    move v4, v6

    .line 3947
    goto :goto_0

    .line 3963
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-static {v3, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->delete(Ljava/lang/String;Ljava/util/List;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3965
    :catch_0
    move-exception v0

    .line 3966
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to set page following state "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3967
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_4

    .line 3968
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3973
    :catchall_0
    move-exception v4

    .line 3974
    if-eqz v2, :cond_3

    :try_start_4
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 3975
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 3977
    :cond_3
    :goto_3
    throw v4

    .line 3970
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_5
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x2454

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3976
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    goto :goto_3

    :catch_2
    move-exception v4

    goto :goto_2
.end method

.method public setFollowingState(JZ)V
    .locals 9
    .param p1, "titleId"    # J
    .param p3, "follow"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 3869
    const-string v4, "SLSServiceManager"

    const-string v5, "change title following state"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3870
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v5, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3872
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getMyTitleFollowingUrlFormat()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3874
    .local v3, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3875
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3876
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3877
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "X-XBL-Contract-Version"

    const-string v6, "1"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3879
    const/4 v2, 0x0

    .line 3882
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    if-eqz p3, :cond_2

    .line 3883
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v3, v1, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 3896
    :goto_1
    if-eqz v2, :cond_0

    :try_start_1
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_0

    .line 3897
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 3901
    :cond_0
    :goto_2
    return-void

    .line 3870
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 3885
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-static {v3, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->delete(Ljava/lang/String;Ljava/util/List;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 3887
    :catch_0
    move-exception v0

    .line 3888
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to get title flowing titles with exception "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3889
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_4

    .line 3890
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3895
    :catchall_0
    move-exception v4

    .line 3896
    if-eqz v2, :cond_3

    :try_start_4
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 3897
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 3899
    :cond_3
    :goto_3
    throw v4

    .line 3892
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_5
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x2454

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3898
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    goto :goto_3

    :catch_2
    move-exception v4

    goto :goto_2
.end method

.method public setFriendFinderOptInStatus(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Z
    .locals 10
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;
    .param p2, "optInStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 4065
    const-string v4, "SLSServiceManager"

    const-string v7, "Set opt in"

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4066
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v7, :cond_2

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4067
    const/4 v1, 0x0

    .line 4068
    .local v1, "result":Z
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSetFriendFinderOptInStatusUrlFormat()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;->name()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v5

    invoke-static {v4, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 4071
    .local v3, "url":Ljava/lang/String;
    sget-object v4, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne p2, v4, :cond_0

    .line 4072
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&waitForUpdate=true"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4075
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4076
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4077
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4078
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "1"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4079
    const/4 v2, 0x0

    .line 4081
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v3, v0, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStreamWithStatus(Ljava/lang/String;Ljava/util/List;[B)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 4082
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_3

    const/16 v4, 0xcc

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_3

    .line 4084
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xb

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4088
    :catchall_0
    move-exception v4

    .line 4089
    if-eqz v2, :cond_1

    :try_start_1
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 4090
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 4092
    :cond_1
    :goto_1
    throw v4

    .end local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v1    # "result":Z
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_2
    move v4, v6

    .line 4066
    goto :goto_0

    .line 4086
    .restart local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v1    # "result":Z
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x1

    .line 4089
    if-eqz v2, :cond_4

    :try_start_2
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_4

    .line 4090
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 4094
    :cond_4
    :goto_2
    return v1

    .line 4091
    :catch_0
    move-exception v5

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method public setPrivacySettings(Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)Z
    .locals 8
    .param p1, "settings"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 482
    const-string v4, "SLSServiceManager"

    const-string v5, "set privacy setting"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v5, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 484
    const/4 v1, 0x0

    .line 485
    .local v1, "result":Z
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUserProfileSettingUrlFormat()Ljava/lang/String;

    move-result-object v3

    .line 487
    .local v3, "url":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 488
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 489
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 490
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "x-xbl-contract-version"

    const-string v6, "4"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 491
    const/4 v2, 0x0

    .line 493
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;->getPrivacySettingRequestBody(Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v2

    .line 494
    const/16 v4, 0xc8

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_2

    const/16 v4, 0xc9

    iget v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v4, v5, :cond_2

    .line 496
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xb

    invoke-direct {v4, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    :catchall_0
    move-exception v4

    .line 501
    if-eqz v2, :cond_0

    :try_start_1
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_0

    .line 502
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 504
    :cond_0
    :goto_1
    throw v4

    .line 483
    .end local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v1    # "result":Z
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 498
    .restart local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v1    # "result":Z
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x1

    .line 501
    if-eqz v2, :cond_3

    :try_start_2
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_3

    .line 502
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 506
    :cond_3
    :goto_2
    return v1

    .line 503
    :catch_0
    move-exception v5

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method public setUserProfileShareIdentitySetting(Ljava/lang/String;)Z
    .locals 8
    .param p1, "postBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 359
    const-string v5, "SLSServiceManager"

    const-string v6, "Setting user profile share identity setting"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_1

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 361
    const/4 v2, 0x0

    .line 362
    .local v2, "result":Z
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUserProfileSettingUrlFormat()Ljava/lang/String;

    move-result-object v4

    .line 363
    .local v4, "url":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 364
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 365
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "4"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    const/4 v1, 0x0

    .line 369
    .local v1, "response":Ljava/lang/String;
    const/4 v3, 0x0

    .line 371
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v0, p1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 372
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_2

    .line 374
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbe1

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 378
    :catchall_0
    move-exception v5

    if-eqz v3, :cond_0

    .line 379
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_0
    throw v5

    .line 360
    .end local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v1    # "response":Ljava/lang/String;
    .end local v2    # "result":Z
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 376
    .restart local v0    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v1    # "response":Ljava/lang/String;
    .restart local v2    # "result":Z
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x1

    .line 378
    if-eqz v3, :cond_3

    .line 379
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 383
    :cond_3
    return v2
.end method

.method public shareToFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    .locals 11
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "itemLocator"    # Ljava/lang/String;
    .param p3, "caption"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 3302
    const-string v5, "SLSServiceManager"

    const-string v8, "shareToFeed"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 3303
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3304
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v6

    :goto_1
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 3306
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s/%s/shares"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getCommentsServiceUrl()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    aput-object p2, v9, v6

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3308
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3310
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 3311
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3312
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept-Language"

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3313
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3314
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "3"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3316
    const/4 v3, 0x0

    .line 3320
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    new-instance v5, Lcom/microsoft/xbox/service/network/managers/PostCommentRequestBody;

    if-nez p3, :cond_0

    const-string p3, ""

    .end local p3    # "caption":Ljava/lang/String;
    :cond_0
    invoke-direct {v5, p1, p3}, Lcom/microsoft/xbox/service/network/managers/PostCommentRequestBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 3321
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v6, Lcom/microsoft/xbox/service/network/managers/PostCommentResult;

    const-class v7, Ljava/util/Date;

    new-instance v8, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;

    invoke-direct {v8}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;-><init>()V

    invoke-static {v5, v6, v7, v8}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/PostCommentResult;

    .line 3322
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_3

    .line 3323
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x2330

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3325
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    :catch_0
    move-exception v0

    .line 3326
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to share to feed "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3327
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3328
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3334
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v5

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    throw v5

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    .restart local p3    # "caption":Ljava/lang/String;
    :cond_1
    move v5, v7

    .line 3303
    goto/16 :goto_0

    :cond_2
    move v5, v7

    .line 3304
    goto/16 :goto_1

    .line 3334
    .end local p3    # "caption":Ljava/lang/String;
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :cond_3
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    .line 3337
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3339
    return-object v2

    .line 3329
    .end local v2    # "result":Lcom/microsoft/xbox/service/network/managers/PostCommentResult;
    :catch_1
    move-exception v0

    .line 3330
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to share to feed "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 3331
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 3332
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x2330

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public updatePhoneContacts(Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    .locals 8
    .param p1, "request"    # Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4408
    const-string v5, "SLSServiceManager"

    const-string v6, "Uploading phone contacts"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4409
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v6, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4410
    const/4 v2, 0x0

    .line 4412
    .local v2, "result":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUploadingPhoneContactsUrlFormat()Ljava/lang/String;

    move-result-object v4

    .line 4414
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4415
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4417
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-TicketToken"

    invoke-static {}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->getInstance()Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->getMsaTicket()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4418
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-AppId"

    const-string v7, "44445A65-4A71-4083-8C90-041A22856E69"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4419
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-Scenario"

    const-string v7, "Test only"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4420
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-Type"

    const-string v7, "application/x-www-form-urlencoded"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4422
    const/4 v3, 0x0

    .line 4425
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsRequest;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 4427
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-ne v5, v6, :cond_0

    .line 4428
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;->parseJson(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 4439
    :cond_0
    if-eqz v3, :cond_1

    :try_start_1
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_1

    .line 4440
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 4445
    :cond_1
    :goto_1
    if-nez v2, :cond_5

    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    :goto_2
    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 4446
    return-object v2

    .line 4409
    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 4430
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Lcom/microsoft/xbox/service/model/friendfinder/ShortCircuitProfileMessage$UploadPhoneContactsResponse;
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 4431
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to uploading phone contacts "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4432
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_4

    .line 4433
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4438
    :catchall_0
    move-exception v5

    .line 4439
    if-eqz v3, :cond_3

    :try_start_3
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_3

    .line 4440
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 4442
    :cond_3
    :goto_3
    throw v5

    .line 4435
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x1389

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4445
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_5
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    goto :goto_2

    .line 4441
    :catch_1
    move-exception v6

    goto :goto_3

    :catch_2
    move-exception v5

    goto :goto_1
.end method

.method public updateThirdPartyToken(Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;Ljava/lang/String;)Z
    .locals 10
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;
    .param p2, "token"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 4029
    const-string v5, "SLSServiceManager"

    const-string v8, "Update thirdparty token"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 4030
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 4031
    const/4 v2, 0x0

    .line 4032
    .local v2, "result":Z
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUpdateThirdPartyTokenUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/friendfinder/LinkedAccountHelpers$LinkedAccountType;->name()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 4034
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4035
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 4036
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4037
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "1"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4038
    const/4 v3, 0x0

    .line 4040
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    new-instance v5, Lcom/microsoft/xbox/service/model/friendfinder/UpdateThirdPartyTokenRequest;

    invoke-direct {v5, p2}, Lcom/microsoft/xbox/service/model/friendfinder/UpdateThirdPartyTokenRequest;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/friendfinder/UpdateThirdPartyTokenRequest;->getUpdateThirdPartyTokenRequestBody(Lcom/microsoft/xbox/service/model/friendfinder/UpdateThirdPartyTokenRequest;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-static {v4, v1, v5, v6, v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 4041
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_2

    const/16 v5, 0xcc

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_2

    .line 4043
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xb

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4046
    :catch_0
    move-exception v0

    .line 4047
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to update third party token"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4048
    instance-of v5, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v5, :cond_4

    .line 4049
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4054
    :catchall_0
    move-exception v5

    .line 4055
    if-eqz v3, :cond_0

    :try_start_2
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v6, :cond_0

    .line 4056
    iget-object v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 4058
    :cond_0
    :goto_1
    throw v5

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_1
    move v5, v7

    .line 4030
    goto/16 :goto_0

    .line 4045
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x1

    .line 4055
    if-eqz v3, :cond_3

    :try_start_3
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_3

    .line 4056
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 4060
    :cond_3
    :goto_2
    return v2

    .line 4051
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_4
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x1

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4057
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    goto :goto_1

    :catch_2
    move-exception v5

    goto :goto_2
.end method

.method public updateUserProfileInfo(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;)Z
    .locals 10
    .param p1, "setting"    # Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 210
    const-string v5, "SLSServiceManager"

    const-string v8, "updateUserProfileInfo"

    invoke-static {v5, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v5, v8, :cond_0

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 213
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUpdateProfileSettingUrlFormat()Ljava/lang/String;

    move-result-object v8

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->name()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-static {v5, v8, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 215
    .local v4, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 216
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 217
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Content-type"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "x-xbl-contract-version"

    const-string v7, "2"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    const/4 v3, 0x0

    .line 221
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const/4 v2, 0x0

    .line 224
    .local v2, "result":Z
    :try_start_0
    new-instance v5, Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody;

    invoke-direct {v5, p1, p2}, Lcom/microsoft/xbox/service/network/managers/UpdateProfileRequestBody;-><init>(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;)V

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 225
    const/16 v5, 0xc9

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    if-eq v5, v6, :cond_1

    .line 226
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbba

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to update profile "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 232
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v5

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    throw v5

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "result":Z
    .end local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v4    # "url":Ljava/lang/String;
    :cond_0
    move v5, v7

    .line 211
    goto :goto_0

    .line 228
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "result":Z
    .restart local v3    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v4    # "url":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x1

    .line 238
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->closeStream(Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;)V

    .line 241
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 242
    return v2

    .line 233
    :catch_1
    move-exception v0

    .line 234
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SLSServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to update profile "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 236
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xbba

    invoke-direct {v5, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public uploadUserPresenceHeartBeat(Ljava/lang/String;)V
    .locals 8
    .param p1, "xuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1175
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    sget-object v7, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v4, v7, :cond_1

    move v4, v5

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 1176
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getUserPresenceHeartBeatUrl()Ljava/lang/String;

    move-result-object v7

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-static {v4, v7, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1177
    .local v3, "url":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1179
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/SLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 1180
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "Content-type"

    const-string v6, "application/json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1181
    new-instance v4, Lorg/apache/http/message/BasicHeader;

    const-string v5, "X-XBL-Contract-Version"

    const-string v6, "3"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1183
    const/4 v2, 0x0

    .line 1186
    .local v2, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    const-string v4, "{\"state\":\"active\"}"

    invoke-static {v3, v1, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1196
    if-eqz v2, :cond_0

    :try_start_1
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v4, :cond_0

    .line 1197
    iget-object v4, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1201
    :cond_0
    :goto_1
    return-void

    .end local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .end local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .end local v3    # "url":Ljava/lang/String;
    :cond_1
    move v4, v6

    .line 1175
    goto :goto_0

    .line 1187
    .restart local v1    # "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    .restart local v2    # "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .restart local v3    # "url":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1188
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "SLSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to upload user presence heart beat "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v4, :cond_3

    .line 1190
    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v0    # "ex":Ljava/lang/Exception;
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1195
    :catchall_0
    move-exception v4

    .line 1196
    if-eqz v2, :cond_2

    :try_start_3
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v5, :cond_2

    .line 1197
    iget-object v5, v2, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1199
    :cond_2
    :goto_2
    throw v4

    .line 1192
    .restart local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    :try_start_4
    new-instance v4, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0x26de

    invoke-direct {v4, v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1198
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    goto :goto_2

    :catch_2
    move-exception v4

    goto :goto_1
.end method
