.class public final Lcom/microsoft/xbox/service/model/UpdateData;
.super Ljava/lang/Object;
.source "UpdateData.java"


# instance fields
.field private final context:Ljava/lang/Object;

.field private final extra:Landroid/os/Bundle;

.field private final isFinal:Z

.field private final updateType:Lcom/microsoft/xbox/service/model/UpdateType;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V
    .locals 1
    .param p1, "updateType"    # Lcom/microsoft/xbox/service/model/UpdateType;
    .param p2, "isFinal"    # Z

    .prologue
    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;Ljava/lang/Object;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V
    .locals 1
    .param p1, "updateType"    # Lcom/microsoft/xbox/service/model/UpdateType;
    .param p2, "isFinal"    # Z
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;Ljava/lang/Object;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;Ljava/lang/Object;)V
    .locals 0
    .param p1, "updateType"    # Lcom/microsoft/xbox/service/model/UpdateType;
    .param p2, "isFinal"    # Z
    .param p3, "extra"    # Landroid/os/Bundle;
    .param p4, "context"    # Ljava/lang/Object;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/UpdateData;->updateType:Lcom/microsoft/xbox/service/model/UpdateType;

    .line 22
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/UpdateData;->isFinal:Z

    .line 23
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/UpdateData;->extra:Landroid/os/Bundle;

    .line 24
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/UpdateData;->context:Ljava/lang/Object;

    .line 25
    return-void
.end method


# virtual methods
.method public getContext()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/UpdateData;->context:Ljava/lang/Object;

    return-object v0
.end method

.method public getExtra()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/UpdateData;->extra:Landroid/os/Bundle;

    return-object v0
.end method

.method public getIsFinal()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/UpdateData;->isFinal:Z

    return v0
.end method

.method public getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/UpdateData;->updateType:Lcom/microsoft/xbox/service/model/UpdateType;

    return-object v0
.end method
