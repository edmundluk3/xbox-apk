.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;
.super Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;
.source "ClubBackgroundChangeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenshotViewHolder"
.end annotation


# instance fields
.field private final date:Landroid/widget/TextView;

.field private final imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final subtitle:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

.field private final title:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;Landroid/view/ViewGroup;)V
    .locals 3
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    .line 106
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03007b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;Landroid/view/View;)V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e02b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->title:Landroid/widget/TextView;

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0320

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->subtitle:Landroid/widget/TextView;

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0321

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->date:Landroid/widget/TextView;

    .line 111
    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "backgroundImageItem"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;)V
    .locals 5
    .param p1, "backgroundImageItem"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 114
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;->getThumbnailImageURI()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020122

    const v4, 0x7f020115

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->title:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->subtitle:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;->getSubTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;->getDate()Ljava/util/Date;

    move-result-object v0

    .line 119
    .local v0, "date":Ljava/util/Date;
    if-eqz v0, :cond_0

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    return-void

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->date:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 98
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$ScreenshotViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;)V

    return-void
.end method
