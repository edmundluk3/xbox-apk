.class public Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "AppDetailHeaderScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 13
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAppDetailHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 14
    return-void
.end method


# virtual methods
.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    .line 34
    .local v0, "midd":Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->MediaItemType:Ljava/lang/String;

    goto :goto_0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 23
    const v0, 0x7f070683

    return v0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 18
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAppDetailHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 19
    return-void
.end method
