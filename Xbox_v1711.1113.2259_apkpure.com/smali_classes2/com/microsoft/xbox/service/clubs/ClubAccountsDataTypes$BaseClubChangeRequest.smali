.class public abstract Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$BaseClubChangeRequest;
.super Ljava/lang/Object;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BaseClubChangeRequest"
.end annotation


# instance fields
.field private final method:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;)V
    .locals 0
    .param p1, "method"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 264
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 265
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$BaseClubChangeRequest;->method:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    .line 266
    return-void
.end method
