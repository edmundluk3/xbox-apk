.class public Lde/tavendo/autobahn/WebSocketMessage$Close;
.super Lde/tavendo/autobahn/WebSocketMessage$Message;
.source "WebSocketMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lde/tavendo/autobahn/WebSocketMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Close"
.end annotation


# instance fields
.field private mCode:I

.field private mReason:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketMessage$Message;-><init>()V

    .line 168
    const/16 v0, 0x3f3

    iput v0, p0, Lde/tavendo/autobahn/WebSocketMessage$Close;->mCode:I

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketMessage$Close;->mReason:Ljava/lang/String;

    .line 170
    return-void
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 172
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketMessage$Message;-><init>()V

    .line 173
    iput p1, p0, Lde/tavendo/autobahn/WebSocketMessage$Close;->mCode:I

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketMessage$Close;->mReason:Ljava/lang/String;

    .line 175
    return-void
.end method

.method constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 177
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketMessage$Message;-><init>()V

    .line 178
    iput p1, p0, Lde/tavendo/autobahn/WebSocketMessage$Close;->mCode:I

    .line 179
    iput-object p2, p0, Lde/tavendo/autobahn/WebSocketMessage$Close;->mReason:Ljava/lang/String;

    .line 180
    return-void
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lde/tavendo/autobahn/WebSocketMessage$Close;->mCode:I

    return v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketMessage$Close;->mReason:Ljava/lang/String;

    return-object v0
.end method
