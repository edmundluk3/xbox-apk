.class final Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse;
.super Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;
.source "AutoValue_UserPostsDataTypes_PostResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostResponse$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .param p1, "postAuthor"    # Ljava/lang/String;
    .param p2, "postDate"    # Ljava/util/Date;
    .param p3, "postId"    # Ljava/lang/String;
    .param p4, "postText"    # Ljava/lang/String;
    .param p5, "postType"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;
    .param p6, "postTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    .param p7, "postUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p8, "timelines":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;>;"
    invoke-direct/range {p0 .. p8}, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostResponse;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostType;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V

    .line 21
    return-void
.end method
