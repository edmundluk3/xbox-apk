.class Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PartyAndLfgListViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HeaderViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final countText:Landroid/widget/TextView;

.field private final headerText:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;Landroid/view/View;)V
    .locals 1
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    .line 345
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 347
    const v0, 0x7f0e06be

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;->headerText:Landroid/widget/TextView;

    .line 348
    const v0, 0x7f0e06bf

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;->countText:Landroid/widget/TextView;

    .line 349
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;)V
    .locals 3
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 353
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 355
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;->headerText:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 356
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->count:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 357
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;->count:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 358
    .local v0, "countText":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;->countText:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 362
    .end local v0    # "countText":Ljava/lang/String;
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;->countText:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 340
    check-cast p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$HeaderListItem;)V

    return-void
.end method
