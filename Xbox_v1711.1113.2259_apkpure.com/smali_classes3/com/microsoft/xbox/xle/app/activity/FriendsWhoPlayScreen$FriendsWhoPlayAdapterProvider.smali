.class Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$FriendsWhoPlayAdapterProvider;
.super Ljava/lang/Object;
.source "FriendsWhoPlayScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FriendsWhoPlayAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$FriendsWhoPlayAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$1;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$FriendsWhoPlayAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;)V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 62
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    .line 63
    .local v0, "friendsWhoPlayViewModel":Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getFriendsWhoPlayScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
