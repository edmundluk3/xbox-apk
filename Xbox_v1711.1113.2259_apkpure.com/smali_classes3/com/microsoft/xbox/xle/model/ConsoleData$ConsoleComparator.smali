.class public Lcom/microsoft/xbox/xle/model/ConsoleData$ConsoleComparator;
.super Ljava/lang/Object;
.source "ConsoleData.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/model/ConsoleData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConsoleComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/xle/model/ConsoleData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/xle/model/ConsoleData;Lcom/microsoft/xbox/xle/model/ConsoleData;)I
    .locals 4
    .param p1, "c1"    # Lcom/microsoft/xbox/xle/model/ConsoleData;
    .param p2, "c2"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 160
    iget-wide v0, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    iget-wide v2, p2, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 161
    const/4 v0, -0x1

    .line 165
    :goto_0
    return v0

    .line 162
    :cond_0
    iget-wide v0, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    iget-wide v2, p2, Lcom/microsoft/xbox/xle/model/ConsoleData;->lastConnected:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 163
    const/4 v0, 0x1

    goto :goto_0

    .line 165
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/xle/model/ConsoleData;->friendlyName:Ljava/lang/String;

    iget-object v1, p2, Lcom/microsoft/xbox/xle/model/ConsoleData;->friendlyName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 157
    check-cast p1, Lcom/microsoft/xbox/xle/model/ConsoleData;

    check-cast p2, Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/model/ConsoleData$ConsoleComparator;->compare(Lcom/microsoft/xbox/xle/model/ConsoleData;Lcom/microsoft/xbox/xle/model/ConsoleData;)I

    move-result v0

    return v0
.end method
