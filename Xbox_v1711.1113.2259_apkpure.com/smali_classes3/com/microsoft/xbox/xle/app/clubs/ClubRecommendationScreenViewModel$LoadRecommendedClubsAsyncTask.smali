.class Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubRecommendationScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRecommendedClubsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final criteria:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "criteria"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 118
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->criteria:Ljava/lang/String;

    .line 119
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->loadDataInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected loadDataInBackground()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->criteria:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->loadRecommendedClubs(Ljava/util/List;I)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->onError()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 128
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 113
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "loadedClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;Ljava/util/List;)V

    .line 148
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel$LoadRecommendedClubsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 143
    return-void
.end method
