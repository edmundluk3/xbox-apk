.class public abstract Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.super Ljava/lang/Object;
.source "AdapterBase.java"


# static fields
.field public static ALLOCATION_TAG:Ljava/lang/String;

.field private static adapterCounter:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected isActive:Z

.field private isStarted:Z

.field protected final rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private screenModules:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "ADAPTERBASE"

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->ALLOCATION_TAG:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->adapterCounter:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->isActive:Z

    .line 51
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->isStarted:Z

    .line 55
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;->getInstance()Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->ALLOCATION_TAG:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;->debugIncrement(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;->getInstance()Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->ALLOCATION_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;->debugPrintOverallocated(Ljava/lang/String;)V

    .line 74
    return-void
.end method


# virtual methods
.method protected dismissKeyboard()V
    .locals 0

    .prologue
    .line 192
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dismissKeyboard()V

    .line 193
    return-void
.end method

.method public finalize()V
    .locals 3

    .prologue
    .line 79
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;->getInstance()Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->ALLOCATION_TAG:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;->debugDecrement(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;->getInstance()Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->ALLOCATION_TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/XLEAllocationTracker;->debugPrintOverallocated(Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method protected findAndInitializeModuleById(ILcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 239
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 240
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    if-eqz v2, :cond_0

    .line 241
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 242
    .local v0, "module":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->setViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 243
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    .end local v0    # "module":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    return-void
.end method

.method public findViewById(I)Landroid/view/View;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 224
    const/4 v1, 0x0

    .line 225
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v2, :cond_0

    .line 226
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 231
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 233
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_1

    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-object v1

    .restart local v1    # "view":Landroid/view/View;
    :cond_1
    if-eqz v0, :cond_2

    .line 235
    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public forceUpdateViewImmediately()V
    .locals 3

    .prologue
    .line 116
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 118
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->updateViewOverride()V

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 120
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->updateView()V

    goto :goto_0

    .line 122
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    return-void
.end method

.method public getAnimateIn(Z)Ljava/util/ArrayList;
    .locals 1
    .param p1, "goingBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAnimateOut(Z)Ljava/util/ArrayList;
    .locals 1
    .param p1, "goingBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getIsStarted()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->isStarted:Z

    return v0
.end method

.method protected getTestMenuButtons()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/appbar/AppBarMenuButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    const/4 v0, 0x0

    return-object v0
.end method

.method public invalidateView()V
    .locals 3

    .prologue
    .line 103
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->isAnimating()Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->invalidateViewOverride()V

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 106
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->invalidateView()V

    goto :goto_0

    .line 109
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    return-void
.end method

.method protected invalidateViewOverride()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public onAnimateInCompleted()V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method protected onAppBarButtonsAdded()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method protected onAppBarUpdated()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 186
    return-void
.end method

.method public onApplicationPause()V
    .locals 3

    .prologue
    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 140
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->onApplicationPause()V

    goto :goto_0

    .line 142
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    return-void
.end method

.method public onApplicationResume()V
    .locals 3

    .prologue
    .line 145
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 146
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->onApplicationResume()V

    goto :goto_0

    .line 148
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 157
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 158
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->onDestroy()V

    goto :goto_0

    .line 161
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 162
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 134
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->onPause()V

    goto :goto_0

    .line 136
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 152
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->onResume()V

    goto :goto_0

    .line 154
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    return-void
.end method

.method public onSetActive()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->isActive:Z

    .line 203
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 204
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->isStarted:Z

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->updateView()V

    .line 208
    :cond_0
    return-void
.end method

.method public onSetInactive()V
    .locals 1
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->isActive:Z

    .line 214
    return-void
.end method

.method public onStart()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 166
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->isStarted:Z

    .line 167
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->dismissKeyboard()V

    .line 168
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 169
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->onStart()V

    goto :goto_0

    .line 171
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    .line 175
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->isStarted:Z

    .line 176
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->dismissKeyboard()V

    .line 177
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 178
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->onStop()V

    goto :goto_0

    .line 181
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 182
    return-void
.end method

.method protected setAppBarButtonClickListener(ILandroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 260
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setButtonClickListener(ILandroid/view/View$OnClickListener;)V

    .line 261
    return-void
.end method

.method protected setAppBarButtonEnabled(IZ)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "isEnabled"    # Z

    .prologue
    .line 256
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setButtonEnabled(IZ)V

    .line 257
    return-void
.end method

.method protected setAppBarButtonVisibility(II)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "visibility"    # I

    .prologue
    .line 264
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->setButtonVisibility(II)V

    .line 265
    return-void
.end method

.method protected setBlocking(ZLjava/lang/String;)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "blockingText"    # Ljava/lang/String;

    .prologue
    .line 248
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/DialogManager;->setBlocking(ZLjava/lang/String;)V

    .line 249
    return-void
.end method

.method protected setCancelableBlocking(ZLjava/lang/String;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "blockingText"    # Ljava/lang/String;
    .param p3, "cancelRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 252
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/DialogManager;->setCancelableBlocking(ZLjava/lang/String;Ljava/lang/Runnable;)V

    .line 253
    return-void
.end method

.method public setScreenState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 269
    return-void
.end method

.method protected showKeyboard(Landroid/view/View;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "delayMS"    # I

    .prologue
    .line 196
    invoke-static {p1, p2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showKeyboard(Landroid/view/View;I)V

    .line 197
    return-void
.end method

.method public updateView()V
    .locals 3

    .prologue
    .line 91
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->isAnimating()Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->updateViewOverride()V

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->screenModules:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;

    .line 94
    .local v0, "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->updateView()V

    goto :goto_0

    .line 97
    .end local v0    # "miniAdapter":Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
    :cond_0
    return-void
.end method

.method protected abstract updateViewOverride()V
.end method
