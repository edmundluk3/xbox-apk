.class public final Lcom/jakewharton/rxrelay2/PublishRelay;
.super Lcom/jakewharton/rxrelay2/Relay;
.source "PublishRelay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/jakewharton/rxrelay2/Relay",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final EMPTY:[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;


# instance fields
.field private final subscribers:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<[",
            "Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    sput-object v0, Lcom/jakewharton/rxrelay2/PublishRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 60
    .local p0, "this":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<TT;>;"
    invoke-direct {p0}, Lcom/jakewharton/rxrelay2/Relay;-><init>()V

    .line 61
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, Lcom/jakewharton/rxrelay2/PublishRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    .line 62
    return-void
.end method

.method private add(Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<TT;>;"
    .local p1, "ps":Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    const/4 v4, 0x0

    .line 84
    :cond_0
    iget-object v3, p0, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    .line 85
    .local v0, "a":[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    array-length v2, v0

    .line 87
    .local v2, "n":I
    add-int/lit8 v3, v2, 0x1

    new-array v1, v3, [Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    .line 88
    .local v1, "b":[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    aput-object p1, v1, v2

    .line 91
    iget-object v3, p0, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 92
    return-void
.end method

.method public static create()Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-direct {v0}, Lcom/jakewharton/rxrelay2/PublishRelay;-><init>()V

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 139
    .local p0, "this":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "value == null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 141
    .local v0, "s":Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;->onNext(Ljava/lang/Object;)V

    .line 140
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 143
    .end local v0    # "s":Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    :cond_1
    return-void
.end method

.method public hasObservers()Z
    .locals 1

    .prologue
    .line 147
    .local p0, "this":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    array-length v0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method remove(Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<TT;>;"
    .local p1, "ps":Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    const/4 v7, 0x0

    .line 104
    :cond_0
    iget-object v5, p0, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    .line 105
    .local v0, "a":[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    sget-object v5, Lcom/jakewharton/rxrelay2/PublishRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    if-ne v0, v5, :cond_2

    .line 132
    :cond_1
    :goto_0
    return-void

    .line 109
    :cond_2
    array-length v4, v0

    .line 110
    .local v4, "n":I
    const/4 v3, -0x1

    .line 111
    .local v3, "j":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_3

    .line 112
    aget-object v5, v0, v2

    if-ne v5, p1, :cond_4

    .line 113
    move v3, v2

    .line 118
    :cond_3
    if-ltz v3, :cond_1

    .line 124
    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    .line 125
    sget-object v1, Lcom/jakewharton/rxrelay2/PublishRelay;->EMPTY:[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    .line 131
    .local v1, "b":[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    :goto_2
    iget-object v5, p0, Lcom/jakewharton/rxrelay2/PublishRelay;->subscribers:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    .line 111
    .end local v1    # "b":[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 127
    :cond_5
    add-int/lit8 v5, v4, -0x1

    new-array v1, v5, [Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    .line 128
    .restart local v1    # "b":[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "[Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    invoke-static {v0, v7, v1, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129
    add-int/lit8 v5, v3, 0x1

    sub-int v6, v4, v3

    add-int/lit8 v6, v6, -0x1

    invoke-static {v0, v5, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2
.end method

.method public subscribeActual(Lio/reactivex/Observer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<TT;>;"
    .local p1, "t":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-TT;>;"
    new-instance v0, Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;

    invoke-direct {v0, p1, p0}, Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;-><init>(Lio/reactivex/Observer;Lcom/jakewharton/rxrelay2/PublishRelay;)V

    .line 68
    .local v0, "ps":Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;, "Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable<TT;>;"
    invoke-interface {p1, v0}, Lio/reactivex/Observer;->onSubscribe(Lio/reactivex/disposables/Disposable;)V

    .line 69
    invoke-direct {p0, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->add(Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;)V

    .line 72
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;->isDisposed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    invoke-virtual {p0, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->remove(Lcom/jakewharton/rxrelay2/PublishRelay$PublishDisposable;)V

    .line 75
    :cond_0
    return-void
.end method
