.class Lcom/microsoft/playready/networkdevice/ReceiverSession$CloseRunnable;
.super Ljava/lang/Object;
.source "ReceiverSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/networkdevice/ReceiverSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CloseRunnable"
.end annotation


# instance fields
.field private mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

.field private final mExpirationTimers:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/playready/networkdevice/ReceiverSession;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/networkdevice/ReceiverSession;Ljava/util/Hashtable;Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V
    .locals 0
    .param p3, "drmProxy"    # Lcom/microsoft/playready/networkdevice/PrndDrmProxy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;",
            ">;",
            "Lcom/microsoft/playready/networkdevice/PrndDrmProxy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 344
    .local p2, "expirationTimers":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Lcom/microsoft/playready/networkdevice/ReceiverSession$ExpirationTimerTask;>;"
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$CloseRunnable;->this$0:Lcom/microsoft/playready/networkdevice/ReceiverSession;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    iput-object p2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$CloseRunnable;->mExpirationTimers:Ljava/util/Hashtable;

    .line 346
    iput-object p3, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$CloseRunnable;->mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    .line 347
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 354
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$CloseRunnable;->mExpirationTimers:Ljava/util/Hashtable;

    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/ReceiverSession$CloseRunnable;->mDrmProxy:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-static {v1, v2}, Lcom/microsoft/playready/networkdevice/ReceiverSession;->access$000(Ljava/util/Hashtable;Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :goto_0
    return-void

    .line 356
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
