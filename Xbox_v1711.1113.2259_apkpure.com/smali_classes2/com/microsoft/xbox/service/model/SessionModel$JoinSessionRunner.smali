.class Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "SessionModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/SessionModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "JoinSessionRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private ipAddress:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/SessionModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/SessionModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 692
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;->this$0:Lcom/microsoft/xbox/service/model/SessionModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 693
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;->ipAddress:Ljava/lang/String;

    .line 694
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 689
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;->buildData()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/lang/Void;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 702
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;->this$0:Lcom/microsoft/xbox/service/model/SessionModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->access$300(Lcom/microsoft/xbox/service/model/SessionModel;)Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;->ipAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->joinSession(Ljava/lang/String;)V

    .line 703
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 708
    const-wide/16 v0, 0x7d1

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 713
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;->this$0:Lcom/microsoft/xbox/service/model/SessionModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/SessionModel;->access$400(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 714
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 698
    return-void
.end method
