.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$16;
.super Ljava/lang/Object;
.source "UTCMessaging.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackReport(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$message:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$16;->val$message:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 415
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$16;->val$message:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 417
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 418
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ConversationId"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$16;->val$message:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 419
    const-string v1, "MessageId"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$16;->val$message:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 421
    const-string v1, "Messaging - Report"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 422
    return-void
.end method
