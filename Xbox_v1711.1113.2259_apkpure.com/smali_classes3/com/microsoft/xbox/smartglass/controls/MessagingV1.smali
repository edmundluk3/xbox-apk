.class Lcom/microsoft/xbox/smartglass/controls/MessagingV1;
.super Lcom/microsoft/xbox/smartglass/controls/Messaging;
.source "MessagingV1.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 0
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Messaging;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 10
    return-void
.end method


# virtual methods
.method protected sendMessage(ILjava/lang/String;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/MessagingV1;->getDefaultTarget()Lcom/microsoft/xbox/smartglass/MessageTarget;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/smartglass/controls/MessagingV1;->sendMessage(ILjava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 15
    return-void
.end method
