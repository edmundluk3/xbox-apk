.class Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "ClubChatScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FormatMessageFromLocalAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private containsLinks:Z

.field private directMentionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private formattedMessage:Ljava/lang/String;

.field private final message:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 924
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .line 925
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 919
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->containsLinks:Z

    .line 926
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 928
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->message:Ljava/lang/String;

    .line 930
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->directMentionSet:Ljava/util/Set;

    .line 931
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 917
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->doInBackground()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/lang/Void;
    .locals 15

    .prologue
    .line 939
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->message:Ljava/lang/String;

    invoke-static {v10}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->xmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    .line 941
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 942
    .local v8, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 943
    .local v0, "end":I
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$000()Ljava/util/regex/Pattern;

    move-result-object v10

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 945
    .local v6, "resolvingMatcher":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 946
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->start()I

    move-result v7

    .line 947
    .local v7, "resolvingStart":I
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    .line 949
    .local v5, "resolvingEnd":I
    const/4 v10, 0x1

    invoke-virtual {v6, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 951
    .local v4, "potentialDirectMention":Ljava/lang/String;
    const/4 v3, 0x0

    .line 952
    .local v3, "longestMatchedItem":Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    invoke-static {v10}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;

    .line 953
    .local v2, "item":Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
    iget-object v11, v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_0

    iget-object v11, v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    .line 954
    invoke-virtual {v4, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    if-eqz v3, :cond_1

    iget-object v11, v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    .line 955
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    iget-object v12, v2, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v11, v12, :cond_0

    .line 956
    :cond_1
    move-object v3, v2

    goto :goto_1

    .line 961
    .end local v2    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
    :cond_2
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v10, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 963
    if-eqz v3, :cond_4

    .line 964
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->directMentionSet:Ljava/util/Set;

    iget-object v11, v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->xuid:Ljava/lang/String;

    invoke-interface {v10, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 967
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "<at id=\"%1$s\">@%2$s</at>"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->xuid:Ljava/lang/String;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    iget-object v14, v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 969
    .local v1, "formattedDirectMention":Ljava/lang/String;
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 972
    iget-object v10, v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 973
    iget-object v10, v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 980
    .end local v1    # "formattedDirectMention":Ljava/lang/String;
    :cond_3
    :goto_2
    move v0, v5

    .line 981
    goto/16 :goto_0

    .line 977
    :cond_4
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 983
    .end local v3    # "longestMatchedItem":Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
    .end local v4    # "potentialDirectMention":Ljava/lang/String;
    .end local v5    # "resolvingEnd":I
    .end local v7    # "resolvingStart":I
    :cond_5
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-eq v0, v10, :cond_6

    .line 984
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v10, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 987
    :cond_6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    .line 989
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-static {v10}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->reformatUrlsInMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 990
    .local v9, "urlFormattedMessage":Ljava/lang/String;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 991
    iput-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    .line 992
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->containsLinks:Z

    .line 995
    :cond_7
    const/4 v10, 0x0

    return-object v10
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 917
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 1000
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->directMentionSet:Ljava/util/Set;

    .line 1001
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->containsLinks:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->message:Ljava/lang/String;

    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->directMentionSet:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->containsLinks:Z

    .line 1000
    invoke-static {v1, v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Ljava/lang/String;Ljava/util/List;Z)V

    .line 1004
    return-void

    .line 1001
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromLocalAsyncTask;->formattedMessage:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 935
    return-void
.end method
