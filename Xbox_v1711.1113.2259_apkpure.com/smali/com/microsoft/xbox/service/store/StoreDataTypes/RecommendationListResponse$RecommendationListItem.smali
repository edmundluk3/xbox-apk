.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;
.super Ljava/lang/Object;
.source "RecommendationListResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecommendationListItem"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field public final id:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Id"
    .end annotation
.end field

.field public final itemType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ItemType"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 128
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 130
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->id:Ljava/lang/String;

    .line 131
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->itemType:Ljava/lang/String;

    .line 132
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 136
    if-ne p1, p0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return v1

    .line 138
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 139
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 141
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;

    .line 142
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->id:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->itemType:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->itemType:Ljava/lang/String;

    .line 143
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 150
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 151
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->hashCode:I

    .line 152
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->hashCode:I

    .line 153
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->itemType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->hashCode:I

    .line 155
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
