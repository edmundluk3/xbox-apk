.class Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;
.super Ljava/lang/Object;
.source "TagPickerDialog.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClear()V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$500(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    .line 356
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/CharSequence;)V
    .locals 7
    .param p1, "newText"    # Ljava/lang/CharSequence;

    .prologue
    .line 315
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 316
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$500(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    .line 342
    :goto_0
    return-void

    .line 319
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 321
    .local v0, "matchedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$600(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    .line 322
    .local v2, "vm":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;->getTags()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    .line 323
    .local v1, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    invoke-interface {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .line 324
    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 325
    invoke-interface {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getDisplayText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 326
    invoke-interface {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 327
    :cond_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 332
    .end local v1    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    .end local v2    # "vm":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    :cond_4
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 333
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 336
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$800(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->clear()V

    .line 337
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$800(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 338
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$800(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 339
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/CharSequence;

    .prologue
    .line 346
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$900(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$1000(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    .line 349
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$4;->this$0:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->access$400(Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;)V

    .line 351
    :cond_0
    return-void
.end method
