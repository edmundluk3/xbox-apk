.class Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "SettingsGeneralPageViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetFollowingSummaryAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;

    .prologue
    .line 505
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileSummaryData()Lcom/microsoft/xbox/service/network/managers/ProfileSummaryResultContainer$ProfileSummaryResult;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 536
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 537
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 538
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowingSummary(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 539
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_1

    .line 545
    :cond_0
    :goto_0
    return-object v0

    .line 542
    :cond_1
    const-string v1, "SettingsActivityViewModel"

    const-string v2, "Failed to get followers summary."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 515
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$400(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 516
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 531
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$400(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 532
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 505
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 520
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 521
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$GetFollowingSummaryAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$502(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z

    .line 522
    return-void
.end method
