.class public Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;
.source "SearchScreenAdapter.java"


# instance fields
.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private listAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;",
            ">;"
        }
    .end annotation
.end field

.field private listView:Landroid/widget/AbsListView;

.field private searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

.field private suggestedTerms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;",
            ">;"
        }
    .end annotation
.end field

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;-><init>()V

    .line 36
    const v1, 0x7f0e09d0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->screenBody:Landroid/view/View;

    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    .line 38
    const v1, 0x7f0e09d4

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 40
    const v1, 0x7f0e0100

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SearchBarView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .line 41
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    const v2, 0x7f0e09c9

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 42
    .local v0, "searchInputEditView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0705a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 44
    const v1, 0x7f0e09d3

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f03016c

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/StoreAutoSuggestItemListAdapter;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listAdapter:Landroid/widget/ArrayAdapter;

    .line 48
    const v1, 0x7f0e09d5

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AbsListView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listView:Landroid/widget/AbsListView;

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listView:Landroid/widget/AbsListView;

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->setXLEAdapterViewBase(Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;)V

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listView:Landroid/widget/AbsListView;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listView:Landroid/widget/AbsListView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->confirm()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;
    .param p2, "parent"    # Landroid/widget/AdapterView;
    .param p3, "view"    # Landroid/view/View;
    .param p4, "position"    # I
    .param p5, "id"    # J

    .prologue
    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;

    .line 53
    .local v0, "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;
    if-eqz v0, :cond_0

    .line 54
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Auto Suggest"

    const-string v3, "Search"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->navigateToSearchResultDetails(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;)V

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method protected dismissKeyboard()V
    .locals 1

    .prologue
    .line 110
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 111
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hideKeyboard()V

    .line 113
    :cond_0
    return-void
.end method

.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->onStart()V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnSearchBarListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;)V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnShowOrDismissKeyboardListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onSetActive()V

    .line 103
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithXLEAdapterView;->onStop()V

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnSearchBarListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;)V

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setOnShowOrDismissKeyboardListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;)V

    .line 122
    :cond_0
    return-void
.end method

.method protected showKeyboard(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showKeyboard(Landroid/view/View;I)V

    .line 107
    return-void
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->updateLoadingIndicator(Z)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->getSearchTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->getSearchTag()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->getSearchTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->searchBar:Lcom/microsoft/xbox/xle/ui/SearchBarView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->getSearchTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->setSearchTag(Ljava/lang/String;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->suggestedTerms:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->getSuggested()Ljava/util/List;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->getSuggested()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->suggestedTerms:Ljava/util/List;

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->suggestedTerms:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listAdapter:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->suggestedTerms:Ljava/util/List;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->listView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;->onDataUpdated()V

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 143
    return-void
.end method
