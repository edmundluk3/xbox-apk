.class Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ClubModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetClubDataRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

.field private clubIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private requestFilters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

.field private xuid:J


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 164
    invoke-direct {p0, p1, v0, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;Ljava/util/List;)V

    .line 165
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;J)V
    .locals 2
    .param p2, "xuid"    # J

    .prologue
    .line 176
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 157
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubHubService()Lcom/microsoft/xbox/service/clubs/IClubHubService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    .line 177
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->xuid:J

    .line 178
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 168
    .local p2, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;Ljava/util/List;)V

    .line 169
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p2, "clubIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p3, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 157
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubHubService()Lcom/microsoft/xbox/service/clubs/IClubHubService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    .line 172
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubIds:Ljava/util/List;

    .line 173
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->requestFilters:Ljava/util/List;

    .line 174
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->buildData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubIds:Ljava/util/List;

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->xuid:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    invoke-interface {v1}, Lcom/microsoft/xbox/service/clubs/IClubHubService;->getRecommendedClubs()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    move-result-object v0

    .line 194
    .local v0, "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    :goto_0
    if-eqz v0, :cond_3

    .line 195
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;->clubs()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    .line 197
    :goto_1
    return-object v1

    .line 186
    .end local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubIds:Ljava/util/List;

    if-nez v1, :cond_1

    .line 187
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->xuid:J

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/IClubHubService;->getUserClubs(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    move-result-object v0

    .restart local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    goto :goto_0

    .line 188
    .end local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->requestFilters:Ljava/util/List;

    if-nez v1, :cond_2

    .line 189
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubIds:Ljava/util/List;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/clubs/IClubHubService;->getClubs(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    move-result-object v0

    .restart local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    goto :goto_0

    .line 191
    .end local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->clubIds:Ljava/util/List;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->requestFilters:Ljava/util/List;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/IClubHubService;->getClubs(Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    move-result-object v0

    .restart local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    goto :goto_0

    .line 197
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 212
    const-wide/16 v0, 0x2580

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager$GetClubDataRunner;->requestFilters:Ljava/util/List;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->access$200(Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/util/List;)V

    .line 208
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 203
    return-void
.end method
