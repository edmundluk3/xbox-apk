.class public Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "TitleHubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleHubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserTitleFollowingModel"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;,
        Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$GetTitleFollowingStateRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/util/Set",
        "<",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation


# instance fields
.field private result:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    return-void
.end method


# virtual methods
.method public getResult()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->result:Ljava/util/Set;

    return-object v0
.end method

.method public load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 232
    new-instance v0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$GetTitleFollowingStateRunner;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$GetTitleFollowingStateRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;)V

    .line 233
    .local v0, "runner":Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$GetTitleFollowingStateRunner;
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    return-object v1
.end method

.method public shouldRefresh()Z
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->result:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->lastRefreshTime:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->shouldRefresh(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggle(J)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "titleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 245
    new-instance v6, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->result:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v6, p0, p1, p2, v0}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;-><init>(Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;JZ)V

    .line 248
    .local v6, "runner":Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;
    invoke-static {v6}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->access$100(Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;)Z

    move-result v0

    invoke-static {p1, p2, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackFollowToggleAction(JZ)V

    .line 250
    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    .line 251
    .local v5, "loadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0

    .line 245
    .end local v5    # "loadingStatus":Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;
    .end local v6    # "runner":Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateFollowState(Lcom/microsoft/xbox/toolkit/AsyncResult;J)V
    .locals 4
    .param p2, "titleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 255
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 256
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->result:Ljava/util/Set;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 257
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->result:Ljava/util/Set;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 262
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 263
    .local v0, "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->clearShouldRefreshFollowedTitles()V

    .line 267
    .end local v0    # "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_0
    return-void

    .line 259
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->result:Ljava/util/Set;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 237
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/Set<Ljava/lang/Long;>;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 238
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 239
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 240
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->result:Ljava/util/Set;

    .line 242
    :cond_0
    return-void

    .line 238
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
