.class public Lde/tavendo/autobahn/WebSocketConnection$SocketThread;
.super Ljava/lang/Thread;
.source "WebSocketConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lde/tavendo/autobahn/WebSocketConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SocketThread"
.end annotation


# static fields
.field private static final WS_CONNECTOR:Ljava/lang/String; = "WebSocketConnector"


# instance fields
.field private mFailureMessage:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mSocket:Ljava/net/Socket;

.field private final mWebSocketURI:Ljava/net/URI;


# direct methods
.method public constructor <init>(Ljava/net/URI;Lde/tavendo/autobahn/WebSocketOptions;)V
    .locals 1
    .param p1, "uri"    # Ljava/net/URI;
    .param p2, "options"    # Lde/tavendo/autobahn/WebSocketOptions;

    .prologue
    const/4 v0, 0x0

    .line 444
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 437
    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mSocket:Ljava/net/Socket;

    .line 438
    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mFailureMessage:Ljava/lang/String;

    .line 445
    const-string v0, "WebSocketConnector"

    invoke-virtual {p0, v0}, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->setName(Ljava/lang/String;)V

    .line 447
    iput-object p1, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mWebSocketURI:Ljava/net/URI;

    .line 448
    return-void
.end method


# virtual methods
.method public getFailureMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mFailureMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getSocket()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mSocket:Ljava/net/Socket;

    return-object v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 454
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 455
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mHandler:Landroid/os/Handler;

    .line 456
    monitor-enter p0

    .line 457
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 456
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 461
    invoke-static {}, Lde/tavendo/autobahn/WebSocketConnection;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SocketThread exited."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    return-void

    .line 456
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public startConnection()V
    .locals 6

    .prologue
    .line 468
    :try_start_0
    iget-object v4, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mWebSocketURI:Ljava/net/URI;

    invoke-virtual {v4}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 469
    .local v2, "host":Ljava/lang/String;
    iget-object v4, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mWebSocketURI:Ljava/net/URI;

    invoke-virtual {v4}, Ljava/net/URI;->getPort()I

    move-result v3

    .line 471
    .local v3, "port":I
    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 472
    iget-object v4, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mWebSocketURI:Ljava/net/URI;

    invoke-virtual {v4}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "wss"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 473
    const/16 v3, 0x1bb

    .line 479
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 480
    .local v1, "factory":Ljavax/net/SocketFactory;
    iget-object v4, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mWebSocketURI:Ljava/net/URI;

    invoke-virtual {v4}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "wss"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 481
    invoke-static {}, Landroid/net/SSLCertificateSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v1

    .line 487
    :goto_1
    invoke-virtual {v1, v2, v3}, Ljavax/net/SocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v4

    iput-object v4, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mSocket:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 492
    .end local v1    # "factory":Ljavax/net/SocketFactory;
    .end local v2    # "host":Ljava/lang/String;
    .end local v3    # "port":I
    :goto_2
    monitor-enter p0

    .line 493
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 492
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 495
    return-void

    .line 475
    .restart local v2    # "host":Ljava/lang/String;
    .restart local v3    # "port":I
    :cond_1
    const/16 v3, 0x50

    goto :goto_0

    .line 483
    .restart local v1    # "factory":Ljavax/net/SocketFactory;
    :cond_2
    :try_start_2
    invoke-static {}, Ljavax/net/SocketFactory;->getDefault()Ljavax/net/SocketFactory;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v1

    goto :goto_1

    .line 488
    .end local v1    # "factory":Ljavax/net/SocketFactory;
    .end local v2    # "host":Ljava/lang/String;
    .end local v3    # "port":I
    :catch_0
    move-exception v0

    .line 489
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mFailureMessage:Ljava/lang/String;

    goto :goto_2

    .line 492
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4
.end method

.method public stopConnection()V
    .locals 2

    .prologue
    .line 499
    :try_start_0
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mSocket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 500
    const/4 v1, 0x0

    iput-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mSocket:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 504
    :goto_0
    return-void

    .line 501
    :catch_0
    move-exception v0

    .line 502
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->mFailureMessage:Ljava/lang/String;

    goto :goto_0
.end method
