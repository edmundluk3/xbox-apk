.class public interface abstract Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;
.super Ljava/lang/Object;
.source "WebComponentContainer.java"


# virtual methods
.method public abstract addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V
.end method

.method public abstract completeRequest(I)V
.end method

.method public abstract completeRequest(ILorg/json/JSONObject;)V
.end method

.method public abstract failRequest(ILjava/lang/String;)V
.end method

.method public abstract failRequest(ILorg/json/JSONObject;)V
.end method

.method public abstract getVersion()Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;
.end method

.method public abstract raiseError(Ljava/lang/String;)V
.end method

.method public abstract raiseEvent(Ljava/lang/String;)V
.end method

.method public abstract raiseEvent(Ljava/lang/String;Ljava/lang/String;)V
.end method
