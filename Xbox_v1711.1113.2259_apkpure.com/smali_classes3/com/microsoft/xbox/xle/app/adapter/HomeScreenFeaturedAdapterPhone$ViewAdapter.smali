.class Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "HomeScreenFeaturedAdapterPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewAdapter"
.end annotation


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;Landroid/content/Context;)V
    .locals 1
    .param p2, "ctx"    # Landroid/content/Context;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 126
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 127
    return-void
.end method

.method private bindCell(Landroid/view/View;I)V
    .locals 11
    .param p1, "cell"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x2

    .line 152
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->access$200(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;)I

    move-result v3

    .line 154
    .local v3, "height":I
    const v8, 0x7f0e06d6

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 156
    .local v4, "image":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->getItem(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v8

    invoke-static {p2, v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->computeAspectRatio(ILcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)F

    move-result v0

    .line 157
    .local v0, "ar":F
    int-to-float v8, v3

    div-float/2addr v8, v0

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 159
    .local v7, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 160
    .local v5, "lpCell":Landroid/view/ViewGroup$LayoutParams;
    iput v9, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 161
    iput v9, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 162
    invoke-virtual {p1, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 164
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 165
    .local v6, "lpImg":Landroid/widget/RelativeLayout$LayoutParams;
    iput v3, v6, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 166
    iput v7, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 167
    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->getItem(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 170
    .local v1, "data":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v8

    invoke-static {v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v2

    .line 172
    .local v2, "defaultRid":I
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 173
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 174
    new-instance v8, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter$1;

    invoke-direct {v8, p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;I)V

    invoke-virtual {p1, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 186
    .end local v2    # "defaultRid":I
    :goto_0
    return-void

    .line 182
    :cond_0
    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 183
    invoke-virtual {p1, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    invoke-virtual {p1, v10}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->barCount:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->access$000(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;)I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;->access$100(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->getItem(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 141
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 146
    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03013e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 147
    .local v0, "cell":Landroid/view/View;
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenFeaturedAdapterPhone$ViewAdapter;->bindCell(Landroid/view/View;I)V

    .line 148
    return-object v0

    .end local v0    # "cell":Landroid/view/View;
    :cond_0
    move-object v0, p2

    .line 146
    goto :goto_0
.end method
