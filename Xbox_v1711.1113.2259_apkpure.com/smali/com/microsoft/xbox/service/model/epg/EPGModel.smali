.class public Lcom/microsoft/xbox/service/model/epg/EPGModel;
.super Ljava/lang/Object;
.source "EPGModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
.implements Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;,
        Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;",
        "Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;",
        "Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EPGModel"

.field private static sActiveListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

.field private static sAllProvidersListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

.field private static sProviders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/epg/EPGProvider;",
            ">;"
        }
    .end annotation
.end field

.field private static sSortedProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGProvider;",
            ">;"
        }
    .end annotation
.end field

.field private static sTunerData:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sSortedProviders:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveListeners:Ljava/util/ArrayList;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sAllProvidersListeners:Ljava/util/ArrayList;

    .line 78
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    return-object v0
.end method

.method public static addListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;

    .prologue
    .line 572
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    return-void
.end method

.method public static addListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;

    .prologue
    .line 597
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sAllProvidersListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    return-void
.end method

.method public static addProvider(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;Ljava/lang/String;)V
    .locals 3
    .param p0, "provider"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    .param p1, "locale"    # Ljava/lang/String;

    .prologue
    .line 105
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const-string v0, "EPGModel"

    const-string v1, "addProvider: Attempted to add duplicate provider!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    const-string v0, "EPGModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addProvider: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    new-instance v2, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;-><init>(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static chooseDefaultProvider()V
    .locals 2

    .prologue
    .line 187
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->setActiveProvider(Ljava/lang/String;)V

    .line 194
    :goto_0
    return-void

    .line 193
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sSortedProviders:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->setActiveProvider(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static createSortedProviderList()V
    .locals 2

    .prologue
    .line 180
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sSortedProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 181
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sSortedProviders:Ljava/util/ArrayList;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 182
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sSortedProviders:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 183
    return-void
.end method

.method public static getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    return-object v0
.end method

.method public static getDescriptor()Ljava/lang/String;
    .locals 5

    .prologue
    .line 197
    sget-object v3, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 198
    const-string v3, ""

    .line 221
    .local v0, "index":I
    .local v2, "str":Ljava/lang/StringBuilder;
    :goto_0
    return-object v3

    .line 201
    .end local v0    # "index":I
    .end local v2    # "str":Ljava/lang/StringBuilder;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    .restart local v2    # "str":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 203
    .restart local v0    # "index":I
    sget-object v3, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 204
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-nez v1, :cond_2

    .line 205
    const-string v4, "Found null provider in provider list!"

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 206
    const-string v4, "null"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    :goto_2
    sget-object v4, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_1

    .line 216
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 219
    goto :goto_1

    .line 209
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v4

    if-ne v1, v4, :cond_3

    .line 210
    const-string v4, "***"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    :cond_3
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 221
    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .locals 4
    .param p0, "headend"    # Ljava/lang/String;

    .prologue
    .line 85
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 86
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-nez v0, :cond_0

    .line 87
    const-string v1, "EPGModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getProvider: Provider not found for headend "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_0
    return-object v0
.end method

.method public static getProviders()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/epg/EPGProvider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    return-object v0
.end method

.method private static getRootDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 689
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSortedProviders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGProvider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sSortedProviders:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static getTempDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 693
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasProviders()Z
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static loadStoredProviders()V
    .locals 9

    .prologue
    .line 115
    sget-object v4, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 145
    .local v1, "settings":Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;
    .local v2, "somethingChanged":Z
    .local v3, "store":Lcom/microsoft/xbox/xle/epg/EpgClientStorage;
    :cond_0
    :goto_0
    return-void

    .line 119
    .end local v1    # "settings":Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;
    .end local v2    # "somethingChanged":Z
    .end local v3    # "store":Lcom/microsoft/xbox/xle/epg/EpgClientStorage;
    :cond_1
    const-string v4, "EPGModel"

    const-string v5, "Loading stored providers"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const/4 v2, 0x0

    .line 124
    .restart local v2    # "somethingChanged":Z
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getInstance()Lcom/microsoft/xbox/xle/epg/EpgClientStorage;

    move-result-object v3

    .line 125
    .restart local v3    # "store":Lcom/microsoft/xbox/xle/epg/EpgClientStorage;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    .line 126
    .restart local v1    # "settings":Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getStoredProviders()[Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;

    move-result-object v6

    array-length v7, v6

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_2

    aget-object v0, v6, v5

    .line 127
    .local v0, "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    iget-object v4, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getConnectedLocale()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->addProvider(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;Ljava/lang/String;)V

    .line 128
    sget-object v4, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    iget-object v8, v0, Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;->provider:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    iget-object v8, v8, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v4

    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->start(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;)V

    .line 129
    const/4 v2, 0x1

    .line 126
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 132
    .end local v0    # "providerStore":Lcom/microsoft/xbox/xle/epg/EpgClientStorage$ProviderStorageInfo;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->createSortedProviderList()V

    .line 135
    sget-object v4, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getLastProvider()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 136
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->getLastProvider()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->setActiveProvider(Ljava/lang/String;)V

    .line 142
    :goto_2
    if-eqz v2, :cond_0

    .line 143
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->notifyProviderListChanged()V

    goto :goto_0

    .line 138
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->chooseDefaultProvider()V

    goto :goto_2
.end method

.method private static native nativeRunTests(Ljava/lang/Object;)V
.end method

.method private static notifyActiveProviderChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 3
    .param p0, "newProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 608
    if-eqz p0, :cond_0

    .line 609
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->setActive()V

    .line 611
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;

    .line 612
    .local v0, "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
    invoke-interface {v0, p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;->onActiveProviderChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    goto :goto_0

    .line 614
    .end local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sAllProvidersListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;

    .line 615
    .local v0, "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    invoke-interface {v0, p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;->onActiveProviderChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    goto :goto_1

    .line 617
    .end local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    :cond_2
    return-void
.end method

.method private static notifyDataChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;II)V
    .locals 3
    .param p0, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .param p1, "startChannel"    # I
    .param p2, "endChannel"    # I

    .prologue
    .line 637
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    if-ne p0, v1, :cond_0

    .line 638
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;

    .line 639
    .local v0, "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;->onDataChanged(II)V

    goto :goto_0

    .line 642
    .end local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sAllProvidersListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;

    .line 643
    .local v0, "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    invoke-interface {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;->onDataChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;II)V

    goto :goto_1

    .line 645
    .end local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    :cond_1
    return-void
.end method

.method private static notifyFavoritesError(Lcom/microsoft/xbox/service/model/epg/EPGProvider;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
    .locals 3
    .param p0, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .param p1, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .prologue
    .line 648
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    if-ne p0, v1, :cond_0

    .line 649
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;

    .line 650
    .local v0, "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;->onFavoritesError(Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V

    goto :goto_0

    .line 653
    .end local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sAllProvidersListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;

    .line 654
    .local v0, "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    invoke-interface {v0, p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;->onFavoritesError(Lcom/microsoft/xbox/service/model/epg/EPGProvider;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V

    goto :goto_1

    .line 656
    .end local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    :cond_1
    return-void
.end method

.method private static notifyFetchingStatusChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 3
    .param p0, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 626
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    if-ne p0, v1, :cond_0

    .line 627
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;

    .line 628
    .local v0, "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
    invoke-interface {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;->onFetchingStatusChanged()V

    goto :goto_0

    .line 631
    .end local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sAllProvidersListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;

    .line 632
    .local v0, "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    invoke-interface {v0, p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;->onFetchingStatusChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    goto :goto_1

    .line 634
    .end local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    :cond_1
    return-void
.end method

.method private static notifyProviderListChanged()V
    .locals 3

    .prologue
    .line 620
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sAllProvidersListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;

    .line 621
    .restart local v0    # "listener":Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;
    invoke-interface {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;->onProviderListChanged()V

    goto :goto_0

    .line 623
    :cond_0
    return-void
.end method

.method public static onCreate()V
    .locals 4

    .prologue
    .line 247
    const-string v2, "EPGModel"

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->connectToEpgModel()V

    .line 251
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->loadStoredProviders()V

    .line 253
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    .line 254
    .local v1, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v1, :cond_1

    .line 255
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 256
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;)V

    .line 261
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 262
    .local v0, "currentProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 263
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 265
    :cond_0
    return-void

    .line 258
    .end local v0    # "currentProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    const-string v2, "EPGModel"

    const-string v3, "Branch session has not been initialized yet"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static onDestroy()V
    .locals 4

    .prologue
    .line 268
    const-string v2, "EPGModel"

    const-string v3, "onDestroy"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/EpgClientStorage;->disconnectFromEpgModel()V

    .line 271
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    .line 272
    .local v1, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v1, :cond_1

    .line 274
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 275
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITunerListener;)V

    .line 280
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 281
    .local v0, "currentProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 282
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 284
    :cond_0
    return-void

    .line 277
    .end local v0    # "currentProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    const-string v2, "EPGModel"

    const-string v3, "Branch session has not been initialized yet"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static onNativeChannelLoadComplete(Ljava/lang/String;I)V
    .locals 7
    .param p0, "headend"    # Ljava/lang/String;
    .param p1, "result"    # I

    .prologue
    const/4 v3, 0x0

    .line 449
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0x2710

    if-ge v2, v4, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 450
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 451
    const-string v2, "EPGModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onNativeChannelLoadComplete called with invalid headend: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x32

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {p0, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v3

    .line 449
    goto :goto_0

    .line 455
    :cond_2
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    .line 456
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v1, :cond_0

    .line 460
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->onNativeChannelLoadComplete(I)V

    .line 462
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sTunerData:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    if-eqz v2, :cond_0

    .line 467
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sTunerData:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    array-length v4, v2

    :goto_2
    if-ge v3, v4, :cond_0

    aget-object v0, v2, v3

    .line 468
    .local v0, "d":Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;
    iget-object v5, v0, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->headendId:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_3

    .line 469
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->hasLoadedChannels()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 470
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->storeTunerChannels(Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;)V

    goto :goto_1

    .line 467
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private static onNativeFetchComplete(Ljava/lang/String;I)V
    .locals 6
    .param p0, "headend"    # Ljava/lang/String;
    .param p1, "result"    # I

    .prologue
    const/4 v2, 0x0

    .line 498
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v3, 0x2710

    if-ge v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 499
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 500
    const-string v1, "EPGModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNativeFetchComplete called with invalid headend: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x32

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 498
    goto :goto_0

    .line 503
    :cond_2
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 504
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v0, :cond_0

    .line 508
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->onNativeFetchComplete(I)V

    goto :goto_1
.end method

.method private static onNativeFetchListServiceComplete(Ljava/lang/String;I)V
    .locals 6
    .param p0, "headend"    # Ljava/lang/String;
    .param p1, "result"    # I

    .prologue
    const/4 v2, 0x0

    .line 528
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v3, 0x2710

    if-ge v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 529
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 530
    const-string v1, "EPGModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNativeFetchListServiceComplete called with invalid headend: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x32

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 528
    goto :goto_0

    .line 533
    :cond_2
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 534
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v0, :cond_0

    .line 538
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->onNativeFetchListServiceComplete(I)V

    goto :goto_1
.end method

.method private static onNativeFetchProgress(Ljava/lang/String;I)V
    .locals 6
    .param p0, "headend"    # Ljava/lang/String;
    .param p1, "percent"    # I

    .prologue
    const/4 v2, 0x0

    .line 513
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v3, 0x2710

    if-ge v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 514
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 515
    const-string v1, "EPGModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNativeFetchProgress called with invalid headend: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x32

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (percent = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 513
    goto :goto_0

    .line 518
    :cond_2
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 519
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v0, :cond_0

    .line 523
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->onNativeFetchProgress(I)V

    goto :goto_1
.end method

.method private static onNativeMetadataLoadComplete(Ljava/lang/String;I)V
    .locals 6
    .param p0, "headend"    # Ljava/lang/String;
    .param p1, "result"    # I

    .prologue
    const/4 v2, 0x0

    .line 479
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v3, 0x2710

    if-ge v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 480
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 481
    const-string v1, "EPGModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNativeMetadataLoadComplete called with invalid headend: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x32

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 479
    goto :goto_0

    .line 484
    :cond_2
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 485
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v0, :cond_0

    .line 489
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->onNativeMetadataLoadComplete(I)V

    .line 491
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    if-ne v0, v1, :cond_0

    .line 492
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->applyBranding()V

    goto :goto_1
.end method

.method private static onNativeSendComplete(Ljava/lang/String;IZ)V
    .locals 6
    .param p0, "headend"    # Ljava/lang/String;
    .param p1, "result"    # I
    .param p2, "attemptedFavoriteValue"    # Z

    .prologue
    const/4 v2, 0x0

    .line 543
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v3, 0x2710

    if-ge v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 544
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 545
    const-string v1, "EPGModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNativeSendComplete called with invalid headend: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x32

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 543
    goto :goto_0

    .line 548
    :cond_2
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 549
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v0, :cond_0

    .line 553
    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->onNativeSendComplete(IZ)V

    goto :goto_1
.end method

.method public static purgeOldSchedules()V
    .locals 3

    .prologue
    .line 237
    const-string v1, "EPGModel"

    const-string v2, "purgeOldSchedules"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 239
    .restart local v0    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->purgeOldSchedules()V

    goto :goto_0

    .line 241
    :cond_0
    return-void
.end method

.method public static removeListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;

    .prologue
    .line 576
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 577
    return-void
.end method

.method public static removeListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;)V
    .locals 1
    .param p0, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;

    .prologue
    .line 601
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sAllProvidersListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 602
    return-void
.end method

.method public static reset()V
    .locals 4

    .prologue
    .line 225
    const-string v1, "EPGModel"

    const-string/jumbo v2, "reset"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 227
    .restart local v0    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->stop(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;)V

    .line 228
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->destroy()V

    goto :goto_0

    .line 230
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 231
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->setActiveProvider(Ljava/lang/String;)V

    .line 232
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->notifyProviderListChanged()V

    .line 233
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->resetUserId()V

    .line 234
    return-void
.end method

.method public static runAllNativeTests(Ljava/lang/Object;)V
    .locals 0
    .param p0, "callbackObject"    # Ljava/lang/Object;

    .prologue
    .line 700
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->nativeRunTests(Ljava/lang/Object;)V

    .line 701
    return-void
.end method

.method public static setActiveProvider(Ljava/lang/String;)V
    .locals 4
    .param p0, "headendId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 152
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    const-string v0, "EPGModel"

    const-string/jumbo v1, "setActiveProvider: set to null (headend null)"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    if-nez v0, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    sput-object v3, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 176
    :goto_1
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->notifyActiveProviderChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    goto :goto_0

    .line 159
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 160
    const-string v0, "EPGModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setActiveProvider: set to null (headend not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    if-eqz v0, :cond_0

    .line 165
    sput-object v3, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    goto :goto_1

    .line 168
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 169
    const-string v0, "EPGModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setActiveProvider: no change (headend: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 172
    :cond_4
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    sput-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 173
    const-string v0, "EPGModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setActiveProvider: set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static testSetHeadend(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 1
    .param p0, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 704
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V

    .line 705
    return-void
.end method


# virtual methods
.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 0
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 297
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    .line 292
    return-void
.end method

.method public onDataChanged(Ljava/lang/String;II)V
    .locals 1
    .param p1, "headend"    # Ljava/lang/String;
    .param p2, "startChannel"    # I
    .param p3, "endChannel"    # I

    .prologue
    .line 410
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-static {v0, p2, p3}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->notifyDataChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;II)V

    .line 411
    return-void
.end method

.method public onFavoritesError(Ljava/lang/String;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
    .locals 1
    .param p1, "headend"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .prologue
    .line 415
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-static {v0, p2}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->notifyFavoritesError(Lcom/microsoft/xbox/service/model/epg/EPGProvider;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V

    .line 416
    return-void
.end method

.method public onFetchingStatusChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "headend"    # Ljava/lang/String;

    .prologue
    .line 405
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->notifyFetchingStatusChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V

    .line 406
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 14
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 302
    if-nez p1, :cond_1

    .line 303
    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    .line 304
    const-string v8, "EPGModel"

    const-string v9, "onHeadendChanged: HeadendInfo null, removing all providers"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sSortedProviders:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 306
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->reset()V

    .line 309
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v8

    const-string v9, "Headend Count"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;I)V

    .line 383
    :goto_0
    return-void

    .line 314
    :cond_1
    const/4 v6, 0x0

    .line 315
    .local v6, "removedActiveProvider":Z
    const/4 v7, 0x0

    .line 318
    .local v7, "somethingChanged":Z
    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-interface {v8, v9}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 319
    .local v5, "providerList":[Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v8, v5

    if-ge v2, v8, :cond_6

    .line 320
    aget-object v8, v5, v2

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v0

    .line 321
    .local v0, "existing_headend":Ljava/lang/String;
    const/4 v1, 0x0

    .line 322
    .local v1, "found":Z
    iget-object v9, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v10, v9

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v10, :cond_2

    aget-object v3, v9, v8

    .line 323
    .local v3, "new_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    iget-object v11, v3, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v11, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v11

    if-nez v11, :cond_5

    .line 324
    const/4 v1, 0x1

    .line 326
    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v8, v3}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->update(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;)V

    .line 330
    .end local v3    # "new_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    :cond_2
    if-nez v1, :cond_4

    .line 332
    const-string v8, "EPGModel"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onHeadendChanged: Removing provider "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v5, v2

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    aget-object v8, v5, v2

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v8

    sget-object v9, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->stop(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;)V

    .line 334
    aget-object v8, v5, v2

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->destroy()V

    .line 335
    aget-object v8, v5, v2

    sget-object v9, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    if-ne v8, v9, :cond_3

    .line 336
    const/4 v8, 0x0

    sput-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 337
    const/4 v6, 0x1

    .line 339
    :cond_3
    const/4 v7, 0x1

    .line 340
    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 322
    .restart local v3    # "new_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 345
    .end local v0    # "existing_headend":Ljava/lang/String;
    .end local v1    # "found":Z
    .end local v3    # "new_provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    :cond_6
    iget-object v10, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v11, v10

    const/4 v8, 0x0

    move v9, v8

    :goto_3
    if-ge v9, v11, :cond_8

    aget-object v4, v10, v9

    .line 346
    .local v4, "provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    iget-object v12, v4, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v8, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 347
    const-string v8, "EPGModel"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "onHeadendChanged: Adding provider "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v4, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->provider_name:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v8, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v8, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->headend_locale:Ljava/lang/String;

    invoke-static {v4, v8}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->addProvider(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;Ljava/lang/String;)V

    .line 349
    const/4 v7, 0x1

    .line 350
    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    iget-object v12, v4, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v8, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v8

    sget-object v12, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sInstance:Lcom/microsoft/xbox/service/model/epg/EPGModel;

    invoke-virtual {v8, v12}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->start(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;)V

    .line 345
    :cond_7
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_3

    .line 355
    .end local v4    # "provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    :cond_8
    if-eqz v7, :cond_9

    .line 356
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->createSortedProviderList()V

    .line 360
    :cond_9
    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sActiveProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    if-nez v8, :cond_e

    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_e

    .line 361
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->chooseDefaultProvider()V

    .line 367
    :cond_a
    :goto_4
    if-eqz v7, :cond_b

    .line 368
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->notifyProviderListChanged()V

    .line 372
    :cond_b
    const/4 v8, 0x0

    sput-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sTunerData:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    .line 373
    sget-object v8, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_d

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 374
    .local v4, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderSource()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;->tuner:Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderSource;

    if-ne v9, v10, :cond_c

    .line 375
    const-string v8, "EPGModel"

    const-string v9, "onHeadendChanged: Requesting tuner channels"

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestTunerChannels()Z

    .line 382
    .end local v4    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_d
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v8

    const-string v9, "Headend Count"

    sget-object v10, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 362
    :cond_e
    if-eqz v6, :cond_a

    .line 363
    const/4 v8, 0x0

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->setActiveProvider(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 7
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 387
    if-nez p1, :cond_1

    .line 398
    :cond_0
    return-void

    .line 391
    :cond_1
    iget-object v3, p1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->providers:[Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 392
    .local v1, "provider":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;
    sget-object v5, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    iget-object v6, v1, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;->headend_id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 393
    .local v0, "epgProvider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    const-string v5, "Provider not found while updating settings"

    invoke-static {v5, v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 394
    if-eqz v0, :cond_2

    .line 395
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->update(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$ProviderInfo;)V

    .line 391
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onTunerChannelsReceived([Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;)V
    .locals 8
    .param p1, "data"    # [Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    .prologue
    .line 425
    sput-object p1, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sTunerData:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    .line 427
    if-nez p1, :cond_1

    .line 442
    :cond_0
    return-void

    .line 432
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sTunerData:[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 433
    .local v0, "d":Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;
    sget-object v5, Lcom/microsoft/xbox/service/model/epg/EPGModel;->sProviders:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 434
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    iget-object v6, v0, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->headendId:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    .line 435
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->hasLoadedChannels()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 436
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->storeTunerChannels(Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;)V

    .line 432
    .end local v1    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 663
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_0

    .line 667
    const-string v0, "EPGModel"

    const-string v1, "Received ProfileData notification"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 672
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 673
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel$1;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
