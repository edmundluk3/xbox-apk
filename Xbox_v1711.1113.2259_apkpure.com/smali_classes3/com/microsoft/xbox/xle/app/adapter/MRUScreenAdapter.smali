.class public Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullPromoHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$SnapHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsRecentHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullNoStatsHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameRecentHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullGameHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private final adapter:Landroid/support/v7/widget/RecyclerView$Adapter;

.field private final inflater:Landroid/view/LayoutInflater;

.field private final nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

.field private npState:Lcom/microsoft/xbox/xle/viewmodel/NPState;

.field private final pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Landroid/support/v7/widget/RecyclerView;)V
    .locals 1
    .param p1, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p2, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .param p3, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 64
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->adapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    .line 50
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .line 51
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->adapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    invoke-virtual {p3, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/NPState;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->npState:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    return-object v0
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getNPState()Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v0

    .line 58
    .local v0, "npStateNew":Lcom/microsoft/xbox/xle/viewmodel/NPState;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->npState:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->npState:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 59
    :cond_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->npState:Lcom/microsoft/xbox/xle/viewmodel/NPState;

    .line 60
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;->adapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 62
    :cond_1
    return-void
.end method
