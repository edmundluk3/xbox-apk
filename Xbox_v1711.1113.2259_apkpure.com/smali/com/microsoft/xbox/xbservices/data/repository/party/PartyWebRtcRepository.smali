.class public Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;
.super Ljava/lang/Object;
.source "PartyWebRtcRepository.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final userTalkingThreshold:I = 0x64


# instance fields
.field private audioManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

.field private closeConnectionSubject:Lio/reactivex/subjects/PublishSubject;

.field private context:Landroid/content/Context;

.field private hasSentClientParameters:Z

.field private isDataChannelListening:Z

.field private localOffer:Lorg/webrtc/SessionDescription;

.field private localPayloadType:I

.field private localSsrc:J

.field private logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

.field private mpsdIndexToSsrcMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

.field private serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

.field private shouldRefreshRemoteOffer:Z

.field private telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

.field private webRtcEventSubject:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$PartyWebRtcEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/inject/Named;
            value = "app_context"
        .end annotation
    .end param
    .param p2, "logger"    # Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .param p3, "peerConnectionClient"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p4, "telemetryProvider"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->isDataChannelListening:Z

    .line 79
    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->hasSentClientParameters:Z

    .line 80
    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->shouldRefreshRemoteOffer:Z

    .line 112
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->context:Landroid/content/Context;

    .line 113
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    .line 114
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    .line 115
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->mpsdIndexToSsrcMap:Ljava/util/Map;

    .line 117
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->webRtcEventSubject:Lio/reactivex/subjects/PublishSubject;

    .line 118
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;Ljava/util/Set;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;
    .param p1, "x1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
    .param p2, "x2"    # Ljava/util/Set;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->onAudioManagerDevicesChanged(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;Ljava/util/Set;)V

    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Lio/reactivex/subjects/PublishSubject;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->webRtcEventSubject:Lio/reactivex/subjects/PublishSubject;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->mpsdIndexToSsrcMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->shouldRefreshRemoteOffer:Z

    return p1
.end method

.method public static fixSDP(Lorg/webrtc/SessionDescription;)Lorg/webrtc/SessionDescription;
    .locals 10
    .param p0, "sdp"    # Lorg/webrtc/SessionDescription;

    .prologue
    const/4 v6, 0x0

    .line 547
    iget-object v7, p0, Lorg/webrtc/SessionDescription;->description:Ljava/lang/String;

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 548
    .local v5, "sdpParts":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 549
    .local v1, "newSdp":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 551
    .local v3, "payloadType":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v7, v5

    if-ge v0, v7, :cond_0

    .line 552
    aget-object v7, v5, v0

    const-string v8, "a=rtpmap:"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    aget-object v7, v5, v0

    const-string v8, "opus"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 553
    aget-object v7, v5, v0

    const-string v8, "a=rtpmap:"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    aget-object v3, v7, v6

    .line 558
    :cond_0
    if-eqz v3, :cond_3

    .line 559
    const/4 v0, 0x0

    :goto_1
    array-length v7, v5

    if-ge v0, v7, :cond_3

    .line 560
    aget-object v7, v5, v0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "a=fmtp:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 561
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "a=fmtp:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " stereo=0;sprop-stereo=0;cbr=0;sprop-maxcapturerate=24000;maxplaybackrate=24000;minptime=20;ptime=20;maxptime=21;useinbandfec=1;usedtx=1;maxaveragebitrate=24000"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v0

    .line 559
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 551
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 566
    :cond_3
    array-length v7, v5

    :goto_2
    if-ge v6, v7, :cond_4

    aget-object v4, v5, v6

    .line 567
    .local v4, "sdpPart":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    const-string v8, "\r\n"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 571
    .end local v4    # "sdpPart":Ljava/lang/String;
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 572
    .local v2, "newSdpString":Ljava/lang/String;
    new-instance v6, Lorg/webrtc/SessionDescription;

    iget-object v7, p0, Lorg/webrtc/SessionDescription;->type:Lorg/webrtc/SessionDescription$Type;

    invoke-direct {v6, v7, v2}, Lorg/webrtc/SessionDescription;-><init>(Lorg/webrtc/SessionDescription$Type;Ljava/lang/String;)V

    return-object v6
.end method

.method private onAudioManagerDevicesChanged(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;Ljava/util/Set;)V
    .locals 6
    .param p1, "device"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 299
    .local p2, "availableDevices":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;>;"
    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatAudioDeviceChange:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    .line 300
    .local v1, "event":Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;
    const-string v2, "AudioDevice"

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->getTelemetryName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    .line 302
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->audioManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->getBluetoothDeviceName()Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "bluetoothName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 304
    const-string v2, "BluetoothName"

    invoke-virtual {v1, v2, v0}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    .line 306
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 308
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onAudioManagerDevicesChanged: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", selected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    return-void
.end method

.method private onConnectedToRoomInternal()V
    .locals 3

    .prologue
    .line 313
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "onConnectedToRoomInternal"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->createPeerConnection()V

    .line 319
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    const/4 v1, 0x1

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->enableStatsEvents(ZI)V

    .line 323
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->createOffer()V

    .line 324
    return-void
.end method

.method private parseSdp(Lorg/webrtc/SessionDescription;)Ljava/util/List;
    .locals 9
    .param p1, "sdp"    # Lorg/webrtc/SessionDescription;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/webrtc/SessionDescription;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 517
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 518
    .local v2, "parsedSdp":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 519
    .local v4, "sdpList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;>;"
    iget-object v6, p1, Lorg/webrtc/SessionDescription;->description:Ljava/lang/String;

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 521
    .local v3, "sdpLines":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 523
    .local v0, "currentSectionId":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v3

    if-ge v1, v6, :cond_2

    .line 524
    aget-object v6, v3, v1

    const-string v7, "m="

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 525
    aget-object v6, v3, v1

    const-string v7, "m="

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v5, v6, v7

    .line 526
    .local v5, "sectionName":Ljava/lang/String;
    move-object v0, v5

    .line 527
    new-instance v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;

    invoke-direct {v6, p0, v5}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;Ljava/lang/String;)V

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    .end local v5    # "sectionName":Ljava/lang/String;
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 532
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;

    iget-object v6, v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;->lines:Ljava/util/List;

    aget-object v7, v3, v1

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    aget-object v6, v3, v1

    const-string v7, "a=mid:"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 535
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;

    aget-object v7, v3, v1

    const-string v8, "a=mid:"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;->bundleId:Ljava/lang/String;

    .line 523
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 540
    :cond_2
    return-object v4
.end method

.method private sendOfferSdp(Lorg/webrtc/SessionDescription;)V
    .locals 8
    .param p1, "sdp"    # Lorg/webrtc/SessionDescription;

    .prologue
    const/4 v4, 0x0

    .line 489
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v5, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string/jumbo v6, "sendOfferSdp"

    invoke-interface {v3, v5, v6}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    iget-object v3, p1, Lorg/webrtc/SessionDescription;->description:Ljava/lang/String;

    const-string v5, "\r\n"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 492
    .local v1, "sdpParts":[Ljava/lang/String;
    array-length v5, v1

    move v3, v4

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v0, v1, v3

    .line 493
    .local v0, "sdpPart":Ljava/lang/String;
    const-string v6, "a=ssrc:"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 494
    const-string v6, "a=ssrc:"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 495
    .local v2, "ssrcParts":[Ljava/lang/String;
    aget-object v6, v2, v4

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localSsrc:J

    .line 492
    .end local v2    # "ssrcParts":[Ljava/lang/String;
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 496
    :cond_1
    const-string v6, "a=rtpmap:"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "opus"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 497
    const-string v6, "a=rtpmap:"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 498
    .restart local v2    # "ssrcParts":[Ljava/lang/String;
    aget-object v6, v2, v4

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localPayloadType:I

    goto :goto_1

    .line 502
    .end local v0    # "sdpPart":Ljava/lang/String;
    .end local v2    # "ssrcParts":[Ljava/lang/String;
    :cond_2
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localOffer:Lorg/webrtc/SessionDescription;

    .line 503
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->webRtcEventSubject:Lio/reactivex/subjects/PublishSubject;

    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;->with(Lorg/webrtc/SessionDescription;)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 504
    return-void
.end method

.method private setRemoteDescription()V
    .locals 9

    .prologue
    .line 130
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localOffer:Lorg/webrtc/SessionDescription;

    if-eqz v6, :cond_4

    .line 131
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localOffer:Lorg/webrtc/SessionDescription;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->parseSdp(Lorg/webrtc/SessionDescription;)Ljava/util/List;

    move-result-object v3

    .line 132
    .local v3, "parsedSdp":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;>;"
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 134
    .local v5, "sdpBuffer":Ljava/lang/StringBuffer;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 135
    .local v0, "bundleList":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 136
    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 137
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;

    iget-object v6, v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;->bundleId:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 135
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 140
    :cond_0
    const-string/jumbo v6, "v=0\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 141
    const-string v6, "o=- 1616430012261898126 2 IN IP4 127.0.0.1\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 142
    const-string/jumbo v6, "s=-\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 143
    const-string/jumbo v6, "t=0 0\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=group:BUNDLE"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 145
    const-string v6, "a=msid-semantic: WMS *\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 147
    const/4 v2, 0x0

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 148
    const-string v7, "application"

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;

    iget-object v6, v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;->type:Ljava/lang/String;

    invoke-static {v7, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 149
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "m=application "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidatePort()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " DTLS/SCTP 5000\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 150
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "c=IN IP4 "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidateIpv4()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 151
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=candidate:1 1 udp 1 "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidateIpv4()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidatePort()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " typ host generation 0 network-id 1\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 152
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=ice-ufrag:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->iceUfrag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 153
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=ice-pwd:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->icePwd()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 154
    const-string v6, "a=ice-options:trickle\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 155
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=fingerprint:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->dtlsAlgorithm()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->dtlsThumbprint()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 156
    const-string v6, "a=setup:active\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 157
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=mid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;

    iget-object v6, v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;->bundleId:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 158
    const-string v6, "a=sctpmap:5000 webrtc-datachannel 1024\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 147
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 159
    :cond_2
    const-string v7, "audio"

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;

    iget-object v6, v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;->type:Ljava/lang/String;

    invoke-static {v7, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 160
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "m=audio "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidatePort()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " UDP/TLS/RTP/SAVPF "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localPayloadType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 161
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "c=IN IP4 "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidateIpv4()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 162
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=rtcp:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidatePort()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " IN IP4 "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidateIpv4()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=candidate:1 1 udp 1 "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidateIpv4()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->candidatePort()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " typ host generation 0 network-id 1\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 164
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=ice-ufrag:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->iceUfrag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 165
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=ice-pwd:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->icePwd()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 166
    const-string v6, "a=ice-options:trickle\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=fingerprint:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->dtlsAlgorithm()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;->dtlsThumbprint()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 168
    const-string v6, "a=setup:active\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 169
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=mid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;

    iget-object v6, v6, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;->bundleId:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 170
    const-string v6, "a=sendrecv\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 171
    const-string v6, "a=rtcp-mux\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 172
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=rtpmap:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localPayloadType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " opus/48000/2\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 173
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=rtcp-fb:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localPayloadType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " transport-cc\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 174
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "a=fmtp:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localPayloadType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " stereo=0;sprop-stereo=0;cbr=0;sprop-maxcapturerate=24000;maxplaybackrate=24000;minptime=20;ptime=20;maxptime=21;useinbandfec=1;usedtx=1;maxaveragebitrate=24000\r\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 176
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->mpsdIndexToSsrcMap:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 177
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "a=ssrc:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cname:cname_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "a=ssrc:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " msid:mslabel_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " msid_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 179
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "a=ssrc:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mslabel:mslabel_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 180
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "a=ssrc:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " label:msid_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 185
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_3
    new-instance v4, Lorg/webrtc/SessionDescription;

    sget-object v6, Lorg/webrtc/SessionDescription$Type;->ANSWER:Lorg/webrtc/SessionDescription$Type;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lorg/webrtc/SessionDescription;-><init>(Lorg/webrtc/SessionDescription$Type;Ljava/lang/String;)V

    .line 186
    .local v4, "remoteSdp":Lorg/webrtc/SessionDescription;
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->onRemoteDescription(Lorg/webrtc/SessionDescription;)V

    .line 190
    .end local v0    # "bundleList":Ljava/lang/StringBuffer;
    .end local v2    # "i":I
    .end local v3    # "parsedSdp":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$SdpSection;>;"
    .end local v4    # "remoteSdp":Lorg/webrtc/SessionDescription;
    .end local v5    # "sdpBuffer":Ljava/lang/StringBuffer;
    :goto_3
    return-void

    .line 188
    :cond_4
    iget-object v6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v7, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v8, "Attempted to setRemoteDescription with no local offer SDP"

    invoke-interface {v6, v7, v8}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private startCall()V
    .locals 3

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->onConnectedToRoomInternal()V

    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->create(Landroid/content/Context;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->audioManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "Starting the audio manager..."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->audioManager:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$1;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;->start(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioManagerEvents;)V

    .line 292
    return-void
.end method


# virtual methods
.method public close()Lio/reactivex/Observable;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "closing WebRTC connection"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    iput-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->isDataChannelListening:Z

    .line 259
    iput-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->hasSentClientParameters:Z

    .line 260
    iput-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->shouldRefreshRemoteOffer:Z

    .line 261
    iput-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localOffer:Lorg/webrtc/SessionDescription;

    .line 262
    iput-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->mpsdIndexToSsrcMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->closeConnectionSubject:Lio/reactivex/subjects/PublishSubject;

    if-nez v0, :cond_0

    .line 266
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->closeConnectionSubject:Lio/reactivex/subjects/PublishSubject;

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->close()V

    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->closeConnectionSubject:Lio/reactivex/subjects/PublishSubject;

    return-object v0
.end method

.method public create()V
    .locals 15

    .prologue
    .line 219
    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->isOnUIThread()V

    .line 221
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    const/4 v1, 0x1

    const/16 v2, 0x64

    const/4 v3, -0x1

    const-string v4, ""

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;-><init>(ZIILjava/lang/String;ZI)V

    .line 230
    .local v0, "dataChannelParameters":Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;
    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/16 v5, 0x20

    const-string v6, "OPUS"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v14, v0

    invoke-direct/range {v1 .. v14}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;-><init>(ZZZILjava/lang/String;ZZZZZZZLcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;)V

    .line 247
    .local v1, "peerConnectionParameters":Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->context:Landroid/content/Context;

    invoke-virtual {v2, v3, v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->createPeerConnectionFactory(Landroid/content/Context;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionEvents;)V

    .line 252
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->startCall()V

    .line 253
    return-void
.end method

.method public getWebRtcEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$PartyWebRtcEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->webRtcEventSubject:Lio/reactivex/subjects/PublishSubject;

    return-object v0
.end method

.method public onIceCandidate(Lorg/webrtc/IceCandidate;)V
    .locals 4
    .param p1, "candidate"    # Lorg/webrtc/IceCandidate;

    .prologue
    .line 423
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onIceCandidate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lorg/webrtc/IceCandidate;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    return-void
.end method

.method public onIceCandidatesRemoved([Lorg/webrtc/IceCandidate;)V
    .locals 0
    .param p1, "candidates"    # [Lorg/webrtc/IceCandidate;

    .prologue
    .line 428
    return-void
.end method

.method public onIceConnected()V
    .locals 3

    .prologue
    .line 432
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "onIceConnected"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->webRtcEventSubject:Lio/reactivex/subjects/PublishSubject;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcConnected;->INSTANCE:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcConnected;

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 434
    return-void
.end method

.method public onIceDisconnected()V
    .locals 3

    .prologue
    .line 438
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "onIceDisconnected"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    return-void
.end method

.method public onLocalDescription(Lorg/webrtc/SessionDescription;)V
    .locals 3
    .param p1, "sdp"    # Lorg/webrtc/SessionDescription;

    .prologue
    .line 407
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "onLocalDescription"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->shouldRefreshRemoteOffer:Z

    if-eqz v0, :cond_0

    .line 413
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->shouldRefreshRemoteOffer:Z

    .line 414
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->setRemoteDescription()V

    .line 419
    :goto_0
    return-void

    .line 417
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->sendOfferSdp(Lorg/webrtc/SessionDescription;)V

    goto :goto_0
.end method

.method public onPeerConnectionClosed()V
    .locals 3

    .prologue
    .line 443
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "onPeerConnectionClosed"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->closeConnectionSubject:Lio/reactivex/subjects/PublishSubject;

    if-eqz v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->closeConnectionSubject:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/PublishSubject;->onComplete()V

    .line 447
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->closeConnectionSubject:Lio/reactivex/subjects/PublishSubject;

    .line 449
    :cond_0
    return-void
.end method

.method public onPeerConnectionError(Ljava/lang/String;)V
    .locals 4
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 481
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPeerConnectionError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->webRtcEventSubject:Lio/reactivex/subjects/PublishSubject;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcDisconnected;->INSTANCE:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcDisconnected;

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 483
    return-void
.end method

.method public onPeerConnectionStatsReady([Lorg/webrtc/StatsReport;)V
    .locals 14
    .param p1, "reports"    # [Lorg/webrtc/StatsReport;

    .prologue
    const/16 v13, 0x64

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 453
    array-length v8, p1

    move v7, v6

    :goto_0
    if-ge v7, v8, :cond_7

    aget-object v1, p1, v7

    .line 454
    .local v1, "report":Lorg/webrtc/StatsReport;
    iget-object v4, v1, Lorg/webrtc/StatsReport;->id:Ljava/lang/String;

    const-string/jumbo v9, "ssrc"

    invoke-virtual {v4, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v1, Lorg/webrtc/StatsReport;->id:Ljava/lang/String;

    const-string/jumbo v9, "send"

    invoke-virtual {v4, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 455
    iget-object v9, v1, Lorg/webrtc/StatsReport;->values:[Lorg/webrtc/StatsReport$Value;

    array-length v10, v9

    move v4, v6

    :goto_1
    if-ge v4, v10, :cond_0

    aget-object v3, v9, v4

    .line 456
    .local v3, "value":Lorg/webrtc/StatsReport$Value;
    iget-object v11, v3, Lorg/webrtc/StatsReport$Value;->name:Ljava/lang/String;

    const-string v12, "audioInputLevel"

    invoke-static {v11, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 457
    iget-object v9, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->webRtcEventSubject:Lio/reactivex/subjects/PublishSubject;

    const-string/jumbo v10, "self"

    iget-object v4, v3, Lorg/webrtc/StatsReport$Value;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v4, v13, :cond_1

    move v4, v5

    :goto_2
    invoke-static {v10, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;->with(Ljava/lang/String;Z)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;

    move-result-object v4

    invoke-virtual {v9, v4}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 453
    .end local v3    # "value":Lorg/webrtc/StatsReport$Value;
    :cond_0
    :goto_3
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_0

    .restart local v3    # "value":Lorg/webrtc/StatsReport$Value;
    :cond_1
    move v4, v6

    .line 457
    goto :goto_2

    .line 455
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 461
    .end local v3    # "value":Lorg/webrtc/StatsReport$Value;
    :cond_3
    iget-object v4, v1, Lorg/webrtc/StatsReport;->id:Ljava/lang/String;

    const-string/jumbo v9, "ssrc"

    invoke-virtual {v4, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v1, Lorg/webrtc/StatsReport;->id:Ljava/lang/String;

    const-string v9, "recv"

    invoke-virtual {v4, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 462
    iget-object v9, v1, Lorg/webrtc/StatsReport;->values:[Lorg/webrtc/StatsReport$Value;

    array-length v10, v9

    move v4, v6

    :goto_4
    if-ge v4, v10, :cond_0

    aget-object v3, v9, v4

    .line 463
    .restart local v3    # "value":Lorg/webrtc/StatsReport$Value;
    iget-object v11, v3, Lorg/webrtc/StatsReport$Value;->name:Ljava/lang/String;

    const-string v12, "audioOutputLevel"

    invoke-static {v11, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 464
    iget-object v4, v1, Lorg/webrtc/StatsReport;->id:Ljava/lang/String;

    const-string/jumbo v9, "ssrc_"

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    iget-object v10, v1, Lorg/webrtc/StatsReport;->id:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const-string v11, "_recv"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {v4, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 465
    .local v2, "ssrcId":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->mpsdIndexToSsrcMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 466
    .local v0, "clientMapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 467
    iget-object v9, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->webRtcEventSubject:Lio/reactivex/subjects/PublishSubject;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v4, v3, Lorg/webrtc/StatsReport$Value;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v4, v13, :cond_5

    move v4, v5

    :goto_5
    invoke-static {v10, v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;->with(Ljava/lang/String;Z)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;

    move-result-object v4

    invoke-virtual {v9, v4}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_5
    move v4, v6

    goto :goto_5

    .line 462
    .end local v0    # "clientMapping":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v2    # "ssrcId":Ljava/lang/String;
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 477
    .end local v1    # "report":Lorg/webrtc/StatsReport;
    .end local v3    # "value":Lorg/webrtc/StatsReport$Value;
    :cond_7
    return-void
.end method

.method public onRemoteDescription(Lorg/webrtc/SessionDescription;)V
    .locals 4
    .param p1, "sdp"    # Lorg/webrtc/SessionDescription;

    .prologue
    const/4 v3, 0x1

    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "onRemoteDescription"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->isDataChannelListening:Z

    if-nez v0, :cond_1

    .line 334
    iput-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->isDataChannelListening:Z

    .line 336
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->getDataChannel()Lorg/webrtc/DataChannel;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$2;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;)V

    invoke-virtual {v0, v1}, Lorg/webrtc/DataChannel;->registerObserver(Lorg/webrtc/DataChannel$Observer;)V

    .line 387
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    if-nez v0, :cond_2

    .line 388
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "Received remote SDP for non-initilized peer connection."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :goto_1
    return-void

    .line 380
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->hasSentClientParameters:Z

    if-nez v0, :cond_0

    .line 381
    iput-boolean v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->hasSentClientParameters:Z

    .line 382
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "Sending clientParametersMessage"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-wide v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localSsrc:J

    iget v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->localPayloadType:I

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;->with(JI)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->sendDataChannelMessage(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;)Z

    goto :goto_0

    .line 392
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->setRemoteDescription(Lorg/webrtc/SessionDescription;)V

    goto :goto_1
.end method

.method public onRemoteIceCandidate(Lorg/webrtc/IceCandidate;)V
    .locals 3
    .param p1, "candidate"    # Lorg/webrtc/IceCandidate;

    .prologue
    .line 396
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "onRemoteIceCandidate"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    if-nez v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v2, "Received ICE candidate for a non-initialized peer connection."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :goto_0
    return-void

    .line 402
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->addRemoteIceCandidate(Lorg/webrtc/IceCandidate;)V

    goto :goto_0
.end method

.method public sendDataChannelMessage(Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;)Z
    .locals 7
    .param p1, "message"    # Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;

    .prologue
    const/4 v2, 0x0

    .line 201
    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 202
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v0, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 204
    .local v1, "jsonMessage":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sending dataChannelMessage: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    sget-object v5, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;->PartyChatSendDataChannelMessage:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryDiagnostics;)V

    const-string v5, "RtcDataMessage"

    .line 206
    invoke-virtual {v4, v5, p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;->addValue(Ljava/lang/String;Ljava/lang/Object;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    move-result-object v4

    .line 205
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 207
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->getDataChannel()Lorg/webrtc/DataChannel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 208
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->getDataChannel()Lorg/webrtc/DataChannel;

    move-result-object v3

    new-instance v4, Lorg/webrtc/DataChannel$Buffer;

    sget-object v5, Lcom/microsoft/xbox/xbservices/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    .line 209
    invoke-virtual {v5, v1}, Ljava/nio/charset/Charset;->encode(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lorg/webrtc/DataChannel$Buffer;-><init>(Ljava/nio/ByteBuffer;Z)V

    .line 208
    invoke-virtual {v3, v4}, Lorg/webrtc/DataChannel;->send(Lorg/webrtc/DataChannel$Buffer;)Z

    .line 210
    const/4 v2, 0x1

    .line 214
    :goto_0
    return v2

    .line 212
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->telemetryProvider:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    new-instance v4, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    sget-object v5, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendDataChannelMessageNoPeerConnection:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-direct {v4, v5}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V

    .line 213
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->logger:Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    sget-object v4, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->TAG:Ljava/lang/String;

    const-string v5, "Attempted to send dataChannel message when peerConnectionClient is not available."

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAudioEnabled(Z)V
    .locals 1
    .param p1, "isEnabled"    # Z

    .prologue
    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->setAudioEnabled(Z)V

    .line 194
    return-void
.end method

.method public setRemoteAudioEnabled(Z)V
    .locals 1
    .param p1, "isEnabled"    # Z

    .prologue
    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->peerConnectionClient:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->setRemoteAudioEnabled(Z)V

    .line 198
    return-void
.end method

.method public setRemoteDescription(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;)V
    .locals 0
    .param p1, "serverInfo"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->serverInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ServerWebRtcInfo;

    .line 126
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository;->setRemoteDescription()V

    .line 127
    return-void
.end method
