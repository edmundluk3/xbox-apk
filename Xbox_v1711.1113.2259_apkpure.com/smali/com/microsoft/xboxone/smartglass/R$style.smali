.class public final Lcom/microsoft/xboxone/smartglass/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xboxone/smartglass/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AlertDialog_AppCompat:I = 0x7f0800b8

.field public static final AlertDialog_AppCompat_Light:I = 0x7f0800b9

.field public static final Animation_AppCompat_Dialog:I = 0x7f0800ba

.field public static final Animation_AppCompat_DropDownUp:I = 0x7f0800bb

.field public static final Animation_Catalyst_RedBox:I = 0x7f0800bc

.field public static final Animation_Design_BottomSheetDialog:I = 0x7f0800bd

.field public static final AppTheme:I = 0x7f0800be

.field public static final Base:I = 0x7f0800bf

.field public static final BaseBlackButton:I = 0x7f0800ee

.field public static final Base_AlertDialog_AppCompat:I = 0x7f0800c0

.field public static final Base_AlertDialog_AppCompat_Light:I = 0x7f0800c1

.field public static final Base_Animation_AppCompat_Dialog:I = 0x7f0800c2

.field public static final Base_Animation_AppCompat_DropDownUp:I = 0x7f0800c3

.field public static final Base_CardView:I = 0x7f0800c4

.field public static final Base_DialogWindowTitleBackground_AppCompat:I = 0x7f0800c6

.field public static final Base_DialogWindowTitle_AppCompat:I = 0x7f0800c5

.field public static final Base_TextAppearance_AppCompat:I = 0x7f080056

.field public static final Base_TextAppearance_AppCompat_Body1:I = 0x7f080057

.field public static final Base_TextAppearance_AppCompat_Body2:I = 0x7f080058

.field public static final Base_TextAppearance_AppCompat_Button:I = 0x7f08003e

.field public static final Base_TextAppearance_AppCompat_Caption:I = 0x7f080059

.field public static final Base_TextAppearance_AppCompat_Display1:I = 0x7f08005a

.field public static final Base_TextAppearance_AppCompat_Display2:I = 0x7f08005b

.field public static final Base_TextAppearance_AppCompat_Display3:I = 0x7f08005c

.field public static final Base_TextAppearance_AppCompat_Display4:I = 0x7f08005d

.field public static final Base_TextAppearance_AppCompat_Headline:I = 0x7f08005e

.field public static final Base_TextAppearance_AppCompat_Inverse:I = 0x7f080021

.field public static final Base_TextAppearance_AppCompat_Large:I = 0x7f08005f

.field public static final Base_TextAppearance_AppCompat_Large_Inverse:I = 0x7f080022

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f080060

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f080061

.field public static final Base_TextAppearance_AppCompat_Medium:I = 0x7f080062

.field public static final Base_TextAppearance_AppCompat_Medium_Inverse:I = 0x7f080023

.field public static final Base_TextAppearance_AppCompat_Menu:I = 0x7f080063

.field public static final Base_TextAppearance_AppCompat_SearchResult:I = 0x7f0800c7

.field public static final Base_TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f080064

.field public static final Base_TextAppearance_AppCompat_SearchResult_Title:I = 0x7f080065

.field public static final Base_TextAppearance_AppCompat_Small:I = 0x7f080066

.field public static final Base_TextAppearance_AppCompat_Small_Inverse:I = 0x7f080024

.field public static final Base_TextAppearance_AppCompat_Subhead:I = 0x7f080067

.field public static final Base_TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f080025

.field public static final Base_TextAppearance_AppCompat_Title:I = 0x7f080068

.field public static final Base_TextAppearance_AppCompat_Title_Inverse:I = 0x7f080026

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f0800ac

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f080069

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f08006a

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f08006b

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f08006c

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f08006d

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f08006e

.field public static final Base_TextAppearance_AppCompat_Widget_Button:I = 0x7f08006f

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I = 0x7f0800b4

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Colored:I = 0x7f0800b5

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Inverse:I = 0x7f0800ad

.field public static final Base_TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f0800c8

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Header:I = 0x7f080070

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f080071

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f080072

.field public static final Base_TextAppearance_AppCompat_Widget_Switch:I = 0x7f080073

.field public static final Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f080074

.field public static final Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f0800c9

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f080075

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f080076

.field public static final Base_ThemeOverlay_AppCompat:I = 0x7f0800ce

.field public static final Base_ThemeOverlay_AppCompat_ActionBar:I = 0x7f0800cf

.field public static final Base_ThemeOverlay_AppCompat_Dark:I = 0x7f0800d0

.field public static final Base_ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f0800d1

.field public static final Base_ThemeOverlay_AppCompat_Dialog:I = 0x7f08002d

.field public static final Base_ThemeOverlay_AppCompat_Dialog_Alert:I = 0x7f08002e

.field public static final Base_ThemeOverlay_AppCompat_Light:I = 0x7f0800d2

.field public static final Base_Theme_AppCompat:I = 0x7f080077

.field public static final Base_Theme_AppCompat_CompactMenu:I = 0x7f0800ca

.field public static final Base_Theme_AppCompat_Dialog:I = 0x7f080027

.field public static final Base_Theme_AppCompat_DialogWhenLarge:I = 0x7f080005

.field public static final Base_Theme_AppCompat_Dialog_Alert:I = 0x7f080028

.field public static final Base_Theme_AppCompat_Dialog_FixedSize:I = 0x7f0800cb

.field public static final Base_Theme_AppCompat_Dialog_MinWidth:I = 0x7f080029

.field public static final Base_Theme_AppCompat_Light:I = 0x7f080078

.field public static final Base_Theme_AppCompat_Light_DarkActionBar:I = 0x7f0800cc

.field public static final Base_Theme_AppCompat_Light_Dialog:I = 0x7f08002a

.field public static final Base_Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f080006

.field public static final Base_Theme_AppCompat_Light_Dialog_Alert:I = 0x7f08002b

.field public static final Base_Theme_AppCompat_Light_Dialog_FixedSize:I = 0x7f0800cd

.field public static final Base_Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f08002c

.field public static final Base_V11_ThemeOverlay_AppCompat_Dialog:I = 0x7f080031

.field public static final Base_V11_Theme_AppCompat_Dialog:I = 0x7f08002f

.field public static final Base_V11_Theme_AppCompat_Light_Dialog:I = 0x7f080030

.field public static final Base_V12_Widget_AppCompat_AutoCompleteTextView:I = 0x7f08003a

.field public static final Base_V12_Widget_AppCompat_EditText:I = 0x7f08003b

.field public static final Base_V21_ThemeOverlay_AppCompat_Dialog:I = 0x7f08007d

.field public static final Base_V21_Theme_AppCompat:I = 0x7f080079

.field public static final Base_V21_Theme_AppCompat_Dialog:I = 0x7f08007a

.field public static final Base_V21_Theme_AppCompat_Light:I = 0x7f08007b

.field public static final Base_V21_Theme_AppCompat_Light_Dialog:I = 0x7f08007c

.field public static final Base_V22_Theme_AppCompat:I = 0x7f0800aa

.field public static final Base_V22_Theme_AppCompat_Light:I = 0x7f0800ab

.field public static final Base_V23_Theme_AppCompat:I = 0x7f0800ae

.field public static final Base_V23_Theme_AppCompat_Light:I = 0x7f0800af

.field public static final Base_V7_ThemeOverlay_AppCompat_Dialog:I = 0x7f0800d7

.field public static final Base_V7_Theme_AppCompat:I = 0x7f0800d3

.field public static final Base_V7_Theme_AppCompat_Dialog:I = 0x7f0800d4

.field public static final Base_V7_Theme_AppCompat_Light:I = 0x7f0800d5

.field public static final Base_V7_Theme_AppCompat_Light_Dialog:I = 0x7f0800d6

.field public static final Base_V7_Widget_AppCompat_AutoCompleteTextView:I = 0x7f0800d8

.field public static final Base_V7_Widget_AppCompat_EditText:I = 0x7f0800d9

.field public static final Base_Widget_AppCompat_ActionBar:I = 0x7f0800da

.field public static final Base_Widget_AppCompat_ActionBar_Solid:I = 0x7f0800db

.field public static final Base_Widget_AppCompat_ActionBar_TabBar:I = 0x7f0800dc

.field public static final Base_Widget_AppCompat_ActionBar_TabText:I = 0x7f08007e

.field public static final Base_Widget_AppCompat_ActionBar_TabView:I = 0x7f08007f

.field public static final Base_Widget_AppCompat_ActionButton:I = 0x7f080080

.field public static final Base_Widget_AppCompat_ActionButton_CloseMode:I = 0x7f080081

.field public static final Base_Widget_AppCompat_ActionButton_Overflow:I = 0x7f080082

.field public static final Base_Widget_AppCompat_ActionMode:I = 0x7f0800dd

.field public static final Base_Widget_AppCompat_ActivityChooserView:I = 0x7f0800de

.field public static final Base_Widget_AppCompat_AutoCompleteTextView:I = 0x7f08003c

.field public static final Base_Widget_AppCompat_Button:I = 0x7f080083

.field public static final Base_Widget_AppCompat_ButtonBar:I = 0x7f080087

.field public static final Base_Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f0800e0

.field public static final Base_Widget_AppCompat_Button_Borderless:I = 0x7f080084

.field public static final Base_Widget_AppCompat_Button_Borderless_Colored:I = 0x7f080085

.field public static final Base_Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f0800df

.field public static final Base_Widget_AppCompat_Button_Colored:I = 0x7f0800b0

.field public static final Base_Widget_AppCompat_Button_Small:I = 0x7f080086

.field public static final Base_Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f080088

.field public static final Base_Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f080089

.field public static final Base_Widget_AppCompat_CompoundButton_Switch:I = 0x7f0800e1

.field public static final Base_Widget_AppCompat_DrawerArrowToggle:I = 0x7f080003

.field public static final Base_Widget_AppCompat_DrawerArrowToggle_Common:I = 0x7f0800e2

.field public static final Base_Widget_AppCompat_DropDownItem_Spinner:I = 0x7f08008a

.field public static final Base_Widget_AppCompat_EditText:I = 0x7f08003d

.field public static final Base_Widget_AppCompat_ImageButton:I = 0x7f08008b

.field public static final Base_Widget_AppCompat_Light_ActionBar:I = 0x7f0800e3

.field public static final Base_Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f0800e4

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f0800e5

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f08008c

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f08008d

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f08008e

.field public static final Base_Widget_AppCompat_Light_PopupMenu:I = 0x7f08008f

.field public static final Base_Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f080090

.field public static final Base_Widget_AppCompat_ListMenuView:I = 0x7f0800e6

.field public static final Base_Widget_AppCompat_ListPopupWindow:I = 0x7f080091

.field public static final Base_Widget_AppCompat_ListView:I = 0x7f080092

.field public static final Base_Widget_AppCompat_ListView_DropDown:I = 0x7f080093

.field public static final Base_Widget_AppCompat_ListView_Menu:I = 0x7f080094

.field public static final Base_Widget_AppCompat_PopupMenu:I = 0x7f080095

.field public static final Base_Widget_AppCompat_PopupMenu_Overflow:I = 0x7f080096

.field public static final Base_Widget_AppCompat_PopupWindow:I = 0x7f0800e7

.field public static final Base_Widget_AppCompat_ProgressBar:I = 0x7f080032

.field public static final Base_Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f080033

.field public static final Base_Widget_AppCompat_RatingBar:I = 0x7f080097

.field public static final Base_Widget_AppCompat_RatingBar_Indicator:I = 0x7f0800b1

.field public static final Base_Widget_AppCompat_RatingBar_Small:I = 0x7f0800b2

.field public static final Base_Widget_AppCompat_SearchView:I = 0x7f0800e8

.field public static final Base_Widget_AppCompat_SearchView_ActionBar:I = 0x7f0800e9

.field public static final Base_Widget_AppCompat_SeekBar:I = 0x7f080098

.field public static final Base_Widget_AppCompat_SeekBar_Discrete:I = 0x7f0800ea

.field public static final Base_Widget_AppCompat_Spinner:I = 0x7f080099

.field public static final Base_Widget_AppCompat_Spinner_Underlined:I = 0x7f080007

.field public static final Base_Widget_AppCompat_TextView_SpinnerItem:I = 0x7f08009a

.field public static final Base_Widget_AppCompat_Toolbar:I = 0x7f0800eb

.field public static final Base_Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f08009b

.field public static final Base_Widget_Design_AppBarLayout:I = 0x7f0800ec

.field public static final Base_Widget_Design_TabLayout:I = 0x7f0800ed

.field public static final Body:I = 0x7f0800ef

.field public static final Body_BottomButton:I = 0x7f0800f0

.field public static final Body_GamerTag:I = 0x7f0800f1

.field public static final Body_SectionText:I = 0x7f0800f2

.field public static final Body_SectionTitle:I = 0x7f0800f3

.field public static final Body_SuggestionText:I = 0x7f0800f4

.field public static final Body_TextInput:I = 0x7f0800f5

.field public static final Body_TextInputComment:I = 0x7f0800f6

.field public static final Body_Title:I = 0x7f0800f7

.field public static final Body_TitleComment:I = 0x7f0800f8

.field public static final Body_UserName:I = 0x7f0800f9

.field public static final ButtonSubText_grey:I = 0x7f0800fa

.field public static final C7_5_DetailsTitle:I = 0x7f08000f

.field public static final CalendarDatePickerDialog:I = 0x7f0800fb

.field public static final CalendarDatePickerStyle:I = 0x7f0800fc

.field public static final CardView:I = 0x7f0800b3

.field public static final CardView_Dark:I = 0x7f0800fd

.field public static final CardView_Light:I = 0x7f0800fe

.field public static final DialogAnimationFade:I = 0x7f0800ff

.field public static final DialogAnimationSlide:I = 0x7f080100

.field public static final Error:I = 0x7f080101

.field public static final Error_Button:I = 0x7f080102

.field public static final Error_Greeting:I = 0x7f080103

.field public static final Error_Normal:I = 0x7f080104

.field public static final ExoMediaButton:I = 0x7f080034

.field public static final ExoMediaButton_FastForward:I = 0x7f080105

.field public static final ExoMediaButton_Next:I = 0x7f080106

.field public static final ExoMediaButton_Pause:I = 0x7f080107

.field public static final ExoMediaButton_Play:I = 0x7f080108

.field public static final ExoMediaButton_Previous:I = 0x7f080109

.field public static final ExoMediaButton_Rewind:I = 0x7f08010a

.field public static final Header:I = 0x7f08010b

.field public static final Header_Email:I = 0x7f08010c

.field public static final Header_UserName:I = 0x7f08010d

.field public static final InAppNotification:I = 0x7f08010e

.field public static final MessengerButton:I = 0x7f08010f

.field public static final MessengerButtonText:I = 0x7f080116

.field public static final MessengerButtonText_Blue:I = 0x7f080117

.field public static final MessengerButtonText_Blue_Large:I = 0x7f080118

.field public static final MessengerButtonText_Blue_Small:I = 0x7f080119

.field public static final MessengerButtonText_White:I = 0x7f08011a

.field public static final MessengerButtonText_White_Large:I = 0x7f08011b

.field public static final MessengerButtonText_White_Small:I = 0x7f08011c

.field public static final MessengerButton_Blue:I = 0x7f080110

.field public static final MessengerButton_Blue_Large:I = 0x7f080111

.field public static final MessengerButton_Blue_Small:I = 0x7f080112

.field public static final MessengerButton_White:I = 0x7f080113

.field public static final MessengerButton_White_Large:I = 0x7f080114

.field public static final MessengerButton_White_Small:I = 0x7f080115

.field public static final MoreTitleText:I = 0x7f08011d

.field public static final NumpadBranchButton:I = 0x7f08011e

.field public static final OnlineidUiTheme:I = 0x7f08011f

.field public static final Platform_AppCompat:I = 0x7f080035

.field public static final Platform_AppCompat_Light:I = 0x7f080036

.field public static final Platform_ThemeOverlay_AppCompat:I = 0x7f08009c

.field public static final Platform_ThemeOverlay_AppCompat_Dark:I = 0x7f08009d

.field public static final Platform_ThemeOverlay_AppCompat_Light:I = 0x7f08009e

.field public static final Platform_V11_AppCompat:I = 0x7f080037

.field public static final Platform_V11_AppCompat_Light:I = 0x7f080038

.field public static final Platform_V14_AppCompat:I = 0x7f08003f

.field public static final Platform_V14_AppCompat_Light:I = 0x7f080040

.field public static final Platform_V21_AppCompat:I = 0x7f08009f

.field public static final Platform_V21_AppCompat_Light:I = 0x7f0800a0

.field public static final Platform_V25_AppCompat:I = 0x7f0800b6

.field public static final Platform_V25_AppCompat_Light:I = 0x7f0800b7

.field public static final Platform_Widget_AppCompat_Spinner:I = 0x7f080039

.field public static final PlaybackBranchButton:I = 0x7f080120

.field public static final PowerOnOffText:I = 0x7f080121

.field public static final PowerText:I = 0x7f080122

.field public static final RadioButtonSubText:I = 0x7f080123

.field public static final RadioButtonText:I = 0x7f080124

.field public static final RtlOverlay_DialogWindowTitle_AppCompat:I = 0x7f080048

.field public static final RtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I = 0x7f080049

.field public static final RtlOverlay_Widget_AppCompat_DialogTitle_Icon:I = 0x7f08004a

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem:I = 0x7f08004b

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I = 0x7f08004c

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I = 0x7f08004d

.field public static final RtlOverlay_Widget_AppCompat_SearchView_MagIcon:I = 0x7f080053

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown:I = 0x7f08004e

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1:I = 0x7f08004f

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2:I = 0x7f080050

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Query:I = 0x7f080051

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Text:I = 0x7f080052

.field public static final RtlUnderlay_Widget_AppCompat_ActionButton:I = 0x7f080054

.field public static final RtlUnderlay_Widget_AppCompat_ActionButton_Overflow:I = 0x7f080055

.field public static final SG:I = 0x7f080125

.field public static final SG_ActionBar:I = 0x7f080126

.field public static final SG_ActionButton:I = 0x7f080127

.field public static final SG_XLEIconButton:I = 0x7f080128

.field public static final SUI_B_base:I = 0x7f080129

.field public static final SUI_B_black:I = 0x7f08012a

.field public static final SUI_B_white:I = 0x7f08012b

.field public static final SUI_L_base:I = 0x7f08012c

.field public static final SUI_L_black:I = 0x7f08012d

.field public static final SUI_L_white:I = 0x7f08012e

.field public static final SUI_SB_base:I = 0x7f08012f

.field public static final SUI_SB_black:I = 0x7f080130

.field public static final SUI_SB_white:I = 0x7f080131

.field public static final SUI_SL_base:I = 0x7f080132

.field public static final SUI_SL_black:I = 0x7f080133

.field public static final SUI_SL_white:I = 0x7f080134

.field public static final SUI_base:I = 0x7f080135

.field public static final SUI_black:I = 0x7f080136

.field public static final SUI_white:I = 0x7f080137

.field public static final SmallBranchButton:I = 0x7f080138

.field public static final SmallNumpadBranchButton:I = 0x7f080139

.field public static final SpinnerDatePickerDialog:I = 0x7f08013a

.field public static final SpinnerDatePickerStyle:I = 0x7f08013b

.field public static final T7_10_Error:I = 0x7f080010

.field public static final T7_1_Giant_Headline:I = 0x7f080011

.field public static final T7_2_Subhead:I = 0x7f080012

.field public static final T7_3_Group_Titles:I = 0x7f080013

.field public static final T7_4_Button_Titles:I = 0x7f080014

.field public static final T7_5_Achievements_Percentage:I = 0x7f08013c

.field public static final T7_5_Bodycopy:I = 0x7f080015

.field public static final T7_5_Bodycopy_Black:I = 0x7f080016

.field public static final T7_5_Bodycopy_SearchSubtitle:I = 0x7f080017

.field public static final T7_7_Details:I = 0x7f080018

.field public static final T7_8_Headline:I = 0x7f080019

.field public static final T7_9_Subtitle:I = 0x7f08001a

.field public static final TextAppearance_AppCompat:I = 0x7f08013d

.field public static final TextAppearance_AppCompat_Body1:I = 0x7f08013e

.field public static final TextAppearance_AppCompat_Body2:I = 0x7f08013f

.field public static final TextAppearance_AppCompat_Button:I = 0x7f080140

.field public static final TextAppearance_AppCompat_Caption:I = 0x7f080141

.field public static final TextAppearance_AppCompat_Display1:I = 0x7f080142

.field public static final TextAppearance_AppCompat_Display2:I = 0x7f080143

.field public static final TextAppearance_AppCompat_Display3:I = 0x7f080144

.field public static final TextAppearance_AppCompat_Display4:I = 0x7f080145

.field public static final TextAppearance_AppCompat_Headline:I = 0x7f080146

.field public static final TextAppearance_AppCompat_Inverse:I = 0x7f080147

.field public static final TextAppearance_AppCompat_Large:I = 0x7f080148

.field public static final TextAppearance_AppCompat_Large_Inverse:I = 0x7f080149

.field public static final TextAppearance_AppCompat_Light_SearchResult_Subtitle:I = 0x7f08014a

.field public static final TextAppearance_AppCompat_Light_SearchResult_Title:I = 0x7f08014b

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f08014c

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f08014d

.field public static final TextAppearance_AppCompat_Medium:I = 0x7f08014e

.field public static final TextAppearance_AppCompat_Medium_Inverse:I = 0x7f08014f

.field public static final TextAppearance_AppCompat_Menu:I = 0x7f080150

.field public static final TextAppearance_AppCompat_Notification:I = 0x7f080041

.field public static final TextAppearance_AppCompat_Notification_Info:I = 0x7f0800a1

.field public static final TextAppearance_AppCompat_Notification_Info_Media:I = 0x7f0800a2

.field public static final TextAppearance_AppCompat_Notification_Line2:I = 0x7f080151

.field public static final TextAppearance_AppCompat_Notification_Line2_Media:I = 0x7f080152

.field public static final TextAppearance_AppCompat_Notification_Media:I = 0x7f0800a3

.field public static final TextAppearance_AppCompat_Notification_Time:I = 0x7f0800a4

.field public static final TextAppearance_AppCompat_Notification_Time_Media:I = 0x7f0800a5

.field public static final TextAppearance_AppCompat_Notification_Title:I = 0x7f080042

.field public static final TextAppearance_AppCompat_Notification_Title_Media:I = 0x7f0800a6

.field public static final TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f080153

.field public static final TextAppearance_AppCompat_SearchResult_Title:I = 0x7f080154

.field public static final TextAppearance_AppCompat_Small:I = 0x7f080155

.field public static final TextAppearance_AppCompat_Small_Inverse:I = 0x7f080156

.field public static final TextAppearance_AppCompat_Subhead:I = 0x7f080157

.field public static final TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f080158

.field public static final TextAppearance_AppCompat_Title:I = 0x7f080159

.field public static final TextAppearance_AppCompat_Title_Inverse:I = 0x7f08015a

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f08015b

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f08015c

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f08015d

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f08015e

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f08015f

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f080160

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I = 0x7f080161

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f080162

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I = 0x7f080163

.field public static final TextAppearance_AppCompat_Widget_Button:I = 0x7f080164

.field public static final TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I = 0x7f080165

.field public static final TextAppearance_AppCompat_Widget_Button_Colored:I = 0x7f080166

.field public static final TextAppearance_AppCompat_Widget_Button_Inverse:I = 0x7f080167

.field public static final TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f080168

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Header:I = 0x7f080169

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f08016a

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f08016b

.field public static final TextAppearance_AppCompat_Widget_Switch:I = 0x7f08016c

.field public static final TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f08016d

.field public static final TextAppearance_Design_CollapsingToolbar_Expanded:I = 0x7f08016e

.field public static final TextAppearance_Design_Counter:I = 0x7f08016f

.field public static final TextAppearance_Design_Counter_Overflow:I = 0x7f080170

.field public static final TextAppearance_Design_Error:I = 0x7f080171

.field public static final TextAppearance_Design_Hint:I = 0x7f080172

.field public static final TextAppearance_Design_Snackbar_Message:I = 0x7f080173

.field public static final TextAppearance_Design_Tab:I = 0x7f080174

.field public static final TextAppearance_StatusBar_EventContent:I = 0x7f080043

.field public static final TextAppearance_StatusBar_EventContent_Info:I = 0x7f080044

.field public static final TextAppearance_StatusBar_EventContent_Line2:I = 0x7f080045

.field public static final TextAppearance_StatusBar_EventContent_Time:I = 0x7f080046

.field public static final TextAppearance_StatusBar_EventContent_Title:I = 0x7f080047

.field public static final TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f080175

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f080176

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f080177

.field public static final Theme:I = 0x7f080178

.field public static final ThemeOverlay_AppCompat:I = 0x7f08019b

.field public static final ThemeOverlay_AppCompat_ActionBar:I = 0x7f08019c

.field public static final ThemeOverlay_AppCompat_Dark:I = 0x7f08019d

.field public static final ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f08019e

.field public static final ThemeOverlay_AppCompat_Dialog:I = 0x7f08019f

.field public static final ThemeOverlay_AppCompat_Dialog_Alert:I = 0x7f0801a0

.field public static final ThemeOverlay_AppCompat_Light:I = 0x7f0801a1

.field public static final Theme_AppCompat:I = 0x7f080179

.field public static final Theme_AppCompat_CompactMenu:I = 0x7f08017a

.field public static final Theme_AppCompat_DayNight:I = 0x7f080008

.field public static final Theme_AppCompat_DayNight_DarkActionBar:I = 0x7f080009

.field public static final Theme_AppCompat_DayNight_Dialog:I = 0x7f08000a

.field public static final Theme_AppCompat_DayNight_DialogWhenLarge:I = 0x7f08000d

.field public static final Theme_AppCompat_DayNight_Dialog_Alert:I = 0x7f08000b

.field public static final Theme_AppCompat_DayNight_Dialog_MinWidth:I = 0x7f08000c

.field public static final Theme_AppCompat_DayNight_NoActionBar:I = 0x7f08000e

.field public static final Theme_AppCompat_Dialog:I = 0x7f08017b

.field public static final Theme_AppCompat_DialogWhenLarge:I = 0x7f08017e

.field public static final Theme_AppCompat_Dialog_Alert:I = 0x7f08017c

.field public static final Theme_AppCompat_Dialog_MinWidth:I = 0x7f08017d

.field public static final Theme_AppCompat_Light:I = 0x7f08017f

.field public static final Theme_AppCompat_Light_DarkActionBar:I = 0x7f080180

.field public static final Theme_AppCompat_Light_Dialog:I = 0x7f080181

.field public static final Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f080184

.field public static final Theme_AppCompat_Light_Dialog_Alert:I = 0x7f080182

.field public static final Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f080183

.field public static final Theme_AppCompat_Light_NoActionBar:I = 0x7f080185

.field public static final Theme_AppCompat_NoActionBar:I = 0x7f080186

.field public static final Theme_Catalyst:I = 0x7f080187

.field public static final Theme_Catalyst_RedBox:I = 0x7f080188

.field public static final Theme_Design:I = 0x7f080189

.field public static final Theme_Design_BottomSheetDialog:I = 0x7f08018a

.field public static final Theme_Design_Light:I = 0x7f08018b

.field public static final Theme_Design_Light_BottomSheetDialog:I = 0x7f08018c

.field public static final Theme_Design_Light_NoActionBar:I = 0x7f08018d

.field public static final Theme_Design_NoActionBar:I = 0x7f08018e

.field public static final Theme_FullScreenDialog:I = 0x7f08018f

.field public static final Theme_FullScreenDialogAnimatedFade:I = 0x7f080190

.field public static final Theme_FullScreenDialogAnimatedSlide:I = 0x7f080191

.field public static final Theme_IAPTheme:I = 0x7f080192

.field public static final Theme_MSA:I = 0x7f080193

.field public static final Theme_MSA_Dialog:I = 0x7f080194

.field public static final Theme_MSA_DialogWhenLarge:I = 0x7f080195

.field public static final Theme_MSA_NoActionBar:I = 0x7f080196

.field public static final Theme_MSA_Transparent:I = 0x7f080197

.field public static final Theme_ReactNative_AppCompat_Light:I = 0x7f080198

.field public static final Theme_ReactNative_AppCompat_Light_NoActionBar_FullScreen:I = 0x7f080199

.field public static final Theme_Smartglass:I = 0x7f08019a

.field public static final ViewPagerTabLayout:I = 0x7f0801a2

.field public static final ViewPagerTabTextAppearance:I = 0x7f0801a3

.field public static final VolumeBranchButton:I = 0x7f0801a4

.field public static final Widget_AppCompat_ActionBar:I = 0x7f0801a5

.field public static final Widget_AppCompat_ActionBar_Solid:I = 0x7f0801a6

.field public static final Widget_AppCompat_ActionBar_TabBar:I = 0x7f0801a7

.field public static final Widget_AppCompat_ActionBar_TabText:I = 0x7f0801a8

.field public static final Widget_AppCompat_ActionBar_TabView:I = 0x7f0801a9

.field public static final Widget_AppCompat_ActionButton:I = 0x7f0801aa

.field public static final Widget_AppCompat_ActionButton_CloseMode:I = 0x7f0801ab

.field public static final Widget_AppCompat_ActionButton_Overflow:I = 0x7f0801ac

.field public static final Widget_AppCompat_ActionMode:I = 0x7f0801ad

.field public static final Widget_AppCompat_ActivityChooserView:I = 0x7f0801ae

.field public static final Widget_AppCompat_AutoCompleteTextView:I = 0x7f0801af

.field public static final Widget_AppCompat_Button:I = 0x7f0801b0

.field public static final Widget_AppCompat_ButtonBar:I = 0x7f0801b6

.field public static final Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f0801b7

.field public static final Widget_AppCompat_Button_Borderless:I = 0x7f0801b1

.field public static final Widget_AppCompat_Button_Borderless_Colored:I = 0x7f0801b2

.field public static final Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f0801b3

.field public static final Widget_AppCompat_Button_Colored:I = 0x7f0801b4

.field public static final Widget_AppCompat_Button_Small:I = 0x7f0801b5

.field public static final Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f0801b8

.field public static final Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f0801b9

.field public static final Widget_AppCompat_CompoundButton_Switch:I = 0x7f0801ba

.field public static final Widget_AppCompat_DrawerArrowToggle:I = 0x7f0801bb

.field public static final Widget_AppCompat_DropDownItem_Spinner:I = 0x7f0801bc

.field public static final Widget_AppCompat_EditText:I = 0x7f0801bd

.field public static final Widget_AppCompat_ImageButton:I = 0x7f0801be

.field public static final Widget_AppCompat_Light_ActionBar:I = 0x7f0801bf

.field public static final Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f0801c0

.field public static final Widget_AppCompat_Light_ActionBar_Solid_Inverse:I = 0x7f0801c1

.field public static final Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f0801c2

.field public static final Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I = 0x7f0801c3

.field public static final Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f0801c4

.field public static final Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f0801c5

.field public static final Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f0801c6

.field public static final Widget_AppCompat_Light_ActionBar_TabView_Inverse:I = 0x7f0801c7

.field public static final Widget_AppCompat_Light_ActionButton:I = 0x7f0801c8

.field public static final Widget_AppCompat_Light_ActionButton_CloseMode:I = 0x7f0801c9

.field public static final Widget_AppCompat_Light_ActionButton_Overflow:I = 0x7f0801ca

.field public static final Widget_AppCompat_Light_ActionMode_Inverse:I = 0x7f0801cb

.field public static final Widget_AppCompat_Light_ActivityChooserView:I = 0x7f0801cc

.field public static final Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f0801cd

.field public static final Widget_AppCompat_Light_DropDownItem_Spinner:I = 0x7f0801ce

.field public static final Widget_AppCompat_Light_ListPopupWindow:I = 0x7f0801cf

.field public static final Widget_AppCompat_Light_ListView_DropDown:I = 0x7f0801d0

.field public static final Widget_AppCompat_Light_PopupMenu:I = 0x7f0801d1

.field public static final Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f0801d2

.field public static final Widget_AppCompat_Light_SearchView:I = 0x7f0801d3

.field public static final Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I = 0x7f0801d4

.field public static final Widget_AppCompat_ListMenuView:I = 0x7f0801d5

.field public static final Widget_AppCompat_ListPopupWindow:I = 0x7f0801d6

.field public static final Widget_AppCompat_ListView:I = 0x7f0801d7

.field public static final Widget_AppCompat_ListView_DropDown:I = 0x7f0801d8

.field public static final Widget_AppCompat_ListView_Menu:I = 0x7f0801d9

.field public static final Widget_AppCompat_NotificationActionContainer:I = 0x7f0800a7

.field public static final Widget_AppCompat_NotificationActionText:I = 0x7f0800a8

.field public static final Widget_AppCompat_PopupMenu:I = 0x7f0801da

.field public static final Widget_AppCompat_PopupMenu_Overflow:I = 0x7f0801db

.field public static final Widget_AppCompat_PopupWindow:I = 0x7f0801dc

.field public static final Widget_AppCompat_ProgressBar:I = 0x7f0801dd

.field public static final Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f0801de

.field public static final Widget_AppCompat_RatingBar:I = 0x7f0801df

.field public static final Widget_AppCompat_RatingBar_Indicator:I = 0x7f0801e0

.field public static final Widget_AppCompat_RatingBar_Small:I = 0x7f0801e1

.field public static final Widget_AppCompat_SearchView:I = 0x7f0801e2

.field public static final Widget_AppCompat_SearchView_ActionBar:I = 0x7f0801e3

.field public static final Widget_AppCompat_SeekBar:I = 0x7f0801e4

.field public static final Widget_AppCompat_SeekBar_Discrete:I = 0x7f0801e5

.field public static final Widget_AppCompat_Spinner:I = 0x7f0801e6

.field public static final Widget_AppCompat_Spinner_DropDown:I = 0x7f0801e7

.field public static final Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f0801e8

.field public static final Widget_AppCompat_Spinner_Underlined:I = 0x7f0801e9

.field public static final Widget_AppCompat_TextView_SpinnerItem:I = 0x7f0801ea

.field public static final Widget_AppCompat_Toolbar:I = 0x7f0801eb

.field public static final Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f0801ec

.field public static final Widget_Design_AppBarLayout:I = 0x7f0800a9

.field public static final Widget_Design_BottomSheet_Modal:I = 0x7f0801ed

.field public static final Widget_Design_CollapsingToolbar:I = 0x7f0801ee

.field public static final Widget_Design_CoordinatorLayout:I = 0x7f0801ef

.field public static final Widget_Design_FloatingActionButton:I = 0x7f0801f0

.field public static final Widget_Design_NavigationView:I = 0x7f0801f1

.field public static final Widget_Design_ScrimInsetsFrameLayout:I = 0x7f0801f2

.field public static final Widget_Design_Snackbar:I = 0x7f0801f3

.field public static final Widget_Design_TabLayout:I = 0x7f080004

.field public static final Widget_Design_TextInputLayout:I = 0x7f0801f4

.field public static final accountPickerListView:I = 0x7f0801f5

.field public static final accountPickerUserTile:I = 0x7f0801f6

.field public static final accountPickerUserTileUserName:I = 0x7f0801f7

.field public static final accountPickerUserTileUserNameBidirectional:I = 0x7f080000

.field public static final accountPickerUserTileUserNameRtl:I = 0x7f0801f8

.field public static final accountTile:I = 0x7f0801f9

.field public static final accountTileOverflowMenu:I = 0x7f0801fa

.field public static final achievements_friends_pivot_page_title_large:I = 0x7f0801fb

.field public static final achievements_friends_pivot_page_title_small:I = 0x7f0801fc

.field public static final achievements_page_title_style:I = 0x7f0801fd

.field public static final actionBar:I = 0x7f0801fe

.field public static final activity:I = 0x7f0801ff

.field public static final activityalert_item_action_text:I = 0x7f080200

.field public static final activityalert_item_date_text:I = 0x7f080201

.field public static final activityalert_item_gamertag_text:I = 0x7f080202

.field public static final activityalert_item_realname_text:I = 0x7f080203

.field public static final activityfeed_header_style:I = 0x7f080204

.field public static final activityfeed_item_achievement_text:I = 0x7f080205

.field public static final activityfeed_item_action_text:I = 0x7f080206

.field public static final activityfeed_item_date_text:I = 0x7f080207

.field public static final activityfeed_item_error_text:I = 0x7f080208

.field public static final activityfeed_item_gamertag_text:I = 0x7f080209

.field public static final activityfeed_item_leaderboard_score_text:I = 0x7f08020a

.field public static final activityfeed_item_leaderboard_subtitle_text:I = 0x7f08020b

.field public static final activityfeed_item_leaderboard_title_text:I = 0x7f08020c

.field public static final activityfeed_item_realname_text:I = 0x7f08020d

.field public static final activityfeed_item_score_text:I = 0x7f08020e

.field public static final activityfeed_item_ugc_caption_text:I = 0x7f08020f

.field public static final app_bar_animations_style:I = 0x7f080210

.field public static final app_bar_menu_item_style:I = 0x7f08001b

.field public static final app_bar_nowplaying_icon_style:I = 0x7f080211

.field public static final app_bar_nowplaying_icon_symbol_style:I = 0x7f080212

.field public static final app_bar_nowplaying_primary_text_style:I = 0x7f080213

.field public static final app_bar_nowplaying_secondary_text_style:I = 0x7f080214

.field public static final app_bar_nowplaying_tab_text_style:I = 0x7f080215

.field public static final app_bar_record_that_icon_style:I = 0x7f080216

.field public static final blocking_dialog_style:I = 0x7f08001c

.field public static final body_copy:I = 0x7f080217

.field public static final button:I = 0x7f080218

.field public static final button_text_only:I = 0x7f080219

.field public static final cancellable_dialog_style:I = 0x7f08001d

.field public static final chevron_down:I = 0x7f08021a

.field public static final choose_profile_color_dialog_style:I = 0x7f08021b

.field public static final clickable_layout:I = 0x7f08021c

.field public static final club_admin_home_button:I = 0x7f08021d

.field public static final club_admin_home_count:I = 0x7f08021e

.field public static final club_customize_box:I = 0x7f08021f

.field public static final club_home_action_button:I = 0x7f080220

.field public static final club_search_criteria_text:I = 0x7f080221

.field public static final club_search_match_style:I = 0x7f080222

.field public static final com_facebook_activity_theme:I = 0x7f080223

.field public static final com_facebook_auth_dialog:I = 0x7f080224

.field public static final com_facebook_auth_dialog_instructions_textview:I = 0x7f080225

.field public static final com_facebook_button:I = 0x7f080226

.field public static final com_facebook_button_like:I = 0x7f080227

.field public static final com_facebook_button_send:I = 0x7f080228

.field public static final com_facebook_button_share:I = 0x7f080229

.field public static final com_facebook_loginview_default_style:I = 0x7f08022a

.field public static final com_facebook_loginview_silver_style:I = 0x7f08022b

.field public static final common_dialog_close_style:I = 0x7f08022c

.field public static final companion_splash_button:I = 0x7f08022d

.field public static final companion_splash_description:I = 0x7f08022e

.field public static final companion_splash_error:I = 0x7f08022f

.field public static final companion_splash_textline:I = 0x7f080230

.field public static final companion_splash_title:I = 0x7f080231

.field public static final connect_dialog_autoconnect_checkbox_label:I = 0x7f080232

.field public static final connect_dialog_connect_button:I = 0x7f080233

.field public static final connect_dialog_console_name:I = 0x7f080234

.field public static final connect_dialog_console_name_edit:I = 0x7f080235

.field public static final connect_dialog_enter_ip_link:I = 0x7f080236

.field public static final connect_dialog_messages:I = 0x7f080237

.field public static final connect_dialog_style:I = 0x7f080238

.field public static final connect_dialog_title:I = 0x7f080239

.field public static final contact_friend_finder_frame:I = 0x7f08023a

.field public static final dark_dialog_style:I = 0x7f08023b

.field public static final dark_label_14:I = 0x7f08023c

.field public static final dark_label_18SB:I = 0x7f08023d

.field public static final dark_label_18SL:I = 0x7f08023e

.field public static final dark_label_36L:I = 0x7f08023f

.field public static final detail_item_primary:I = 0x7f080240

.field public static final detail_item_secondary:I = 0x7f080241

.field public static final detail_pivot_page_title:I = 0x7f080242

.field public static final dividerButtons:I = 0x7f080243

.field public static final dlc_info_text:I = 0x7f080244

.field public static final drawer_header_text:I = 0x7f080245

.field public static final drawer_row:I = 0x7f080246

.field public static final drawer_row_divider:I = 0x7f080247

.field public static final drawer_row_extended:I = 0x7f080248

.field public static final drawer_row_icon:I = 0x7f080249

.field public static final drawer_row_text:I = 0x7f08024a

.field public static final epg_appchannel_item_connection_error_text:I = 0x7f08024b

.field public static final epg_appchannel_item_connection_error_text_expanded_view:I = 0x7f08024c

.field public static final epg_appchannel_item_description_text:I = 0x7f08024d

.field public static final epg_appchannel_item_subtitle_text:I = 0x7f08024e

.field public static final epg_appchannel_item_title_text:I = 0x7f08024f

.field public static final epg_channel_header_favorite_indicator:I = 0x7f080250

.field public static final epg_error_text_style_compact:I = 0x7f080251

.field public static final epg_error_text_style_compact_stacked:I = 0x7f080252

.field public static final epg_fullscreen_error_frame_style:I = 0x7f080253

.field public static final epg_linear_layout_padding_view:I = 0x7f080254

.field public static final epg_loading_icon:I = 0x7f080255

.field public static final epg_loading_message_text:I = 0x7f080256

.field public static final epg_loading_message_wait_text:I = 0x7f080257

.field public static final epg_obscured_start:I = 0x7f080258

.field public static final epg_program_view_text:I = 0x7f080259

.field public static final epg_provider_chevron:I = 0x7f08025a

.field public static final epg_status_message_fullscreen_header:I = 0x7f08025b

.field public static final epg_status_message_fullscreen_horizontalRule:I = 0x7f08025c

.field public static final epg_status_message_fullscreen_single_line_connect_to_xbox_text_view:I = 0x7f08025d

.field public static final epg_status_message_fullscreen_text:I = 0x7f08025e

.field public static final epg_status_message_fullscreen_text_without_header:I = 0x7f08025f

.field public static final esrb_rating_descriptor_style:I = 0x7f080260

.field public static final esrb_rating_disclaimer_style:I = 0x7f080261

.field public static final expanded_app_bar_style:I = 0x7f08001e

.field public static final feedback_checkboxstyle:I = 0x7f080262

.field public static final feedback_dialog_style:I = 0x7f080263

.field public static final feedback_privacystyle:I = 0x7f080264

.field public static final feedback_sectiontextstyle:I = 0x7f080265

.field public static final feedback_titlestyle:I = 0x7f080266

.field public static final following_pivot_page_title_large:I = 0x7f080267

.field public static final following_pivot_page_title_small:I = 0x7f080268

.field public static final gallery_button_style:I = 0x7f080269

.field public static final gameprofile_capture_subtext:I = 0x7f08026a

.field public static final gameprofile_capture_text:I = 0x7f08026b

.field public static final gameprogress_gameclips_large_header:I = 0x7f08026c

.field public static final gameprogress_gameclips_small_header:I = 0x7f08026d

.field public static final home_screen_bat_icon:I = 0x7f08026e

.field public static final home_screen_bat_media_title:I = 0x7f08026f

.field public static final home_screen_bat_media_type:I = 0x7f080270

.field public static final home_screen_bat_promo_text:I = 0x7f080271

.field public static final home_screen_header:I = 0x7f080272

.field public static final home_screen_popup_button:I = 0x7f080273

.field public static final home_screen_popup_cmd_text:I = 0x7f080274

.field public static final home_screen_popup_companion_text:I = 0x7f080275

.field public static final home_screen_popup_provider_text:I = 0x7f080276

.field public static final home_screen_popup_select_provider_text:I = 0x7f080277

.field public static final home_screen_popup_title_text:I = 0x7f080278

.field public static final home_screen_recents_promo_image:I = 0x7f080279

.field public static final home_screen_recents_promo_text2:I = 0x7f08027a

.field public static final home_screen_subheader:I = 0x7f08027b

.field public static final ie_edittext:I = 0x7f08027c

.field public static final install_button:I = 0x7f08027d

.field public static final listView:I = 0x7f08027e

.field public static final media_progress_bar:I = 0x7f08027f

.field public static final media_progress_text:I = 0x7f080280

.field public static final mediabutton:I = 0x7f080281

.field public static final messages_conversations_details_text_L:I = 0x7f080282

.field public static final messages_conversations_details_text_SB:I = 0x7f080283

.field public static final messages_conversations_list_text:I = 0x7f080284

.field public static final mock_debug_text:I = 0x7f080285

.field public static final nowplaying_button_small:I = 0x7f080286

.field public static final nowplaying_subtitle:I = 0x7f080287

.field public static final nowplaying_title:I = 0x7f080288

.field public static final oobe_page_container:I = 0x7f080289

.field public static final overflowMenu:I = 0x7f08028a

.field public static final panorama_header:I = 0x7f08028b

.field public static final people_pre_search_item_gametag_text:I = 0x7f08028c

.field public static final people_search_flyout_button:I = 0x7f08028d

.field public static final people_search_input_text:I = 0x7f08028e

.field public static final people_search_item_action_match_text:I = 0x7f08028f

.field public static final people_search_item_gamertag_match_text:I = 0x7f080290

.field public static final people_search_item_realname_match_text:I = 0x7f080291

.field public static final phone_contact_title_style:I = 0x7f08001f

.field public static final pivot_header_below_title:I = 0x7f080292

.field public static final pivot_header_chevron:I = 0x7f080293

.field public static final pivot_header_chevron_left:I = 0x7f080294

.field public static final pivot_header_chevron_right:I = 0x7f080295

.field public static final pivot_header_epg:I = 0x7f080296

.field public static final pivot_page_header:I = 0x7f080297

.field public static final pivot_page_sub_header:I = 0x7f080298

.field public static final pivot_page_title:I = 0x7f080299

.field public static final play_button:I = 0x7f08029a

.field public static final popupMessage:I = 0x7f08029b

.field public static final profile_achievements_pivot_page_title_large:I = 0x7f08029c

.field public static final profile_achievements_pivot_page_title_small:I = 0x7f08029d

.field public static final profile_bio_label_text:I = 0x7f08029e

.field public static final profile_bio_value_text:I = 0x7f08029f

.field public static final profile_customize_edittext:I = 0x7f0802a0

.field public static final profile_label_text:I = 0x7f0802a1

.field public static final profile_label_text_B:I = 0x7f0802a2

.field public static final profile_label_value_text:I = 0x7f0802a3

.field public static final profile_location_label_text:I = 0x7f0802a4

.field public static final profile_location_value_text:I = 0x7f0802a5

.field public static final profile_page_title_style:I = 0x7f0802a6

.field public static final profile_social_togglebutton:I = 0x7f0802a7

.field public static final profile_topgames_title_style:I = 0x7f0802a8

.field public static final provider_picker_popup_text:I = 0x7f0802a9

.field public static final recent_activity_tile_info_text:I = 0x7f0802aa

.field public static final recent_activity_tile_info_value:I = 0x7f0802ab

.field public static final recent_activity_tile_subtitle:I = 0x7f0802ac

.field public static final recent_activity_tile_title:I = 0x7f0802ad

.field public static final remote_button_text_style:I = 0x7f0802ae

.field public static final remote_control_header_text_style:I = 0x7f0802af

.field public static final remote_control_pivot_button_style:I = 0x7f0802b0

.field public static final remote_control_style:I = 0x7f0802b1

.field public static final remote_control_text_style:I = 0x7f0802b2

.field public static final screen_header:I = 0x7f0802b3

.field public static final screen_header_gone_on_tablet:I = 0x7f0802b4

.field public static final screen_header_with_title:I = 0x7f0802b5

.field public static final search_flyout_background:I = 0x7f0802b6

.field public static final search_flyout_button:I = 0x7f0802b7

.field public static final search_flyout_style:I = 0x7f0802b8

.field public static final search_flyout_style_dimmed:I = 0x7f0802b9

.field public static final search_input_text:I = 0x7f0802ba

.field public static final settings_checkbox_text:I = 0x7f0802bb

.field public static final settings_entry_description:I = 0x7f0802bc

.field public static final settings_entry_title:I = 0x7f0802bd

.field public static final settings_section_name:I = 0x7f0802be

.field public static final signOutCheckBox:I = 0x7f0802bf

.field public static final signOutCheckBoxBidirectional:I = 0x7f080001

.field public static final signOutCheckBoxRtl:I = 0x7f0802c0

.field public static final standard_boldText:I = 0x7f0802c1

.field public static final standard_header:I = 0x7f080020

.field public static final standard_smallText:I = 0x7f0802c2

.field public static final standard_spinner:I = 0x7f0802c3

.field public static final standard_subText:I = 0x7f0802c4

.field public static final standard_subheader:I = 0x7f0802c5

.field public static final standard_text:I = 0x7f0802c6

.field public static final star_rating_icon_style:I = 0x7f0802c7

.field public static final streaming_dialog_style:I = 0x7f0802c8

.field public static final textDefault:I = 0x7f0802c9

.field public static final textHeader:I = 0x7f0802ca

.field public static final textLarge:I = 0x7f0802cb

.field public static final textLargest:I = 0x7f0802cc

.field public static final textLink:I = 0x7f0802cd

.field public static final textPopupMessageBody:I = 0x7f0802ce

.field public static final textPopupMessageHeader:I = 0x7f0802cf

.field public static final textStaticPage:I = 0x7f0802d0

.field public static final title_header:I = 0x7f0802d1

.field public static final tooltip_bubble_text:I = 0x7f0802d2

.field public static final tv_dialog_style:I = 0x7f0802d3

.field public static final tv_error_dialog_style:I = 0x7f0802d4

.field public static final tv_list_pivot_header:I = 0x7f0802d5

.field public static final tv_list_section_header:I = 0x7f0802d6

.field public static final tv_menu_row:I = 0x7f0802d7

.field public static final tv_menu_row_highlight:I = 0x7f0802d8

.field public static final tv_streamer_seekbar:I = 0x7f0802d9

.field public static final tv_success_dialog_style:I = 0x7f0802da

.field public static final tvepisode_description_style:I = 0x7f0802db

.field public static final userTile:I = 0x7f0802dc

.field public static final userTileDisplayName:I = 0x7f0802dd

.field public static final userTileEmail:I = 0x7f0802de

.field public static final userTileImage:I = 0x7f0802df

.field public static final userTileImageBidirectional:I = 0x7f080002

.field public static final userTileImageRtl:I = 0x7f0802e0

.field public static final visibility_gone_on_tablet_style:I = 0x7f0802e1

.field public static final volume_dialog_style:I = 0x7f0802e2

.field public static final xbox_symbol:I = 0x7f0802e3

.field public static final xbox_symbol_button:I = 0x7f0802e4

.field public static final xbox_symbol_dark:I = 0x7f0802e5

.field public static final xbox_symbol_fav:I = 0x7f0802e6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
