.class public Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "ComposeMessageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity$ComposeMessageAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

.field private oldInputMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(I)V

    .line 43
    return-void
.end method

.method private restoreSoftInputAdjustMode()V
    .locals 3

    .prologue
    .line 91
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 92
    .local v1, "wnd":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 93
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, p0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->oldInputMode:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 95
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 96
    return-void
.end method

.method private saveAndAdjustSoftInputAdjustMode()V
    .locals 3

    .prologue
    .line 82
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 83
    .local v1, "wnd":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 84
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v2, p0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->oldInputMode:I

    .line 85
    const/16 v2, 0x20

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    .line 87
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 88
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    const-string v0, "Compose Message"

    return-object v0
.end method

.method public leaveScreen(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "closeHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->leaveViewModel(Ljava/lang/Runnable;)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 47
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->onCreateContentView()V

    .line 51
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity$ComposeMessageAdapterProvider;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity$ComposeMessageAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->onCreate()V

    .line 54
    return-void
.end method

.method public onCreateContentView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    const v0, 0x7f0300a9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->setContentView(I)V

    .line 77
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->setShowUtilityBar(Z)V

    .line 78
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->setShowRightPane(Z)V

    .line 79
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 70
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecipients(Lcom/microsoft/xbox/toolkit/MultiSelection;)V

    .line 71
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 72
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStart()V

    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->saveAndAdjustSoftInputAdjustMode()V

    .line 60
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;->restoreSoftInputAdjustMode()V

    .line 65
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStop()V

    .line 66
    return-void
.end method
