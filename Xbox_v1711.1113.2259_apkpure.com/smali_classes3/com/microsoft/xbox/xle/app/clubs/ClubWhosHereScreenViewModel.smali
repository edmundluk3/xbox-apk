.class public Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubWhosHereScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;
    }
.end annotation


# static fields
.field private static final EPOCH_TIMESTAMP:Ljava/lang/String;

.field private static final FIVE_MINUTES_AGO_IN_MS:J = -0x493e0L

.field private static final HEADING_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_RESULTS:I = 0x64

.field private static final ONE_DAY_AGO_IN_MS:J = -0x5265c00L

.field private static final TAG:Ljava/lang/String;

.field private static final TEN_MINUTES_AGO_IN_MS:J = -0x927c0L

.field private static final THIRTY_MINUTES_AGO_IN_MS:J = -0x1b7740L


# instance fields
.field private allMembersCount:J

.field private club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private clubHashCode:I

.field private currentSearchTerm:Ljava/lang/String;

.field private filter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

.field private getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private final getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private volatile isSearchingService:Z

.field private final latestSearchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final loadingDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation
.end field

.field private memberDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation
.end field

.field private membersCount:J

.field private moderatorCount:J

.field private moderatorDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final moderatorXuids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ownerDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation
.end field

.field private ownerXuid:J

.field private searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

.field private searchResponseAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;",
            ">;"
        }
    .end annotation
.end field

.field private userDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 52
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->TAG:Ljava/lang/String;

    .line 58
    sget-object v0, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->defaultFormatMs:Ljava/text/SimpleDateFormat;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->EPOCH_TIMESTAMP:Ljava/lang/String;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->HEADING_LIST:Ljava/util/List;

    .line 95
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->HEADING_LIST:Ljava/util/List;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07029d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, -0x5265c00

    invoke-direct {v1, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->HEADING_LIST:Ljava/util/List;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07029e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, -0x1b7740

    invoke-direct {v1, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->HEADING_LIST:Ljava/util/List;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07029c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, -0x927c0

    invoke-direct {v1, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->HEADING_LIST:Ljava/util/List;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07029b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/32 v4, -0x493e0

    invoke-direct {v1, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->HEADING_LIST:Ljava/util/List;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07029a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v6, v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const-wide/16 v2, 0x0

    .line 133
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 67
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 76
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->allMembersCount:J

    .line 77
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->membersCount:J

    .line 78
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorCount:J

    .line 134
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 136
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubWhosHereScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 137
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->EVERYONE:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->filter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    .line 138
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->clubHashCode:I

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->userDataList:Ljava/util/List;

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->memberDataList:Ljava/util/List;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorDataList:Ljava/util/List;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->latestSearchResults:Ljava/util/List;

    .line 144
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorXuids:Ljava/util/Set;

    .line 145
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 146
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->searchResponseAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 147
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->onUserDataLoadCompleted(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->onSearchResult(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V

    return-void
.end method

.method private getLocalUserData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubWhosHereScreenViewModel$ClubWhosHereFilter:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown filter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 355
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    .line 346
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->userDataList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 348
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->memberDataList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 350
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorDataList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 352
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->ownerDataList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 344
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private getSearchResultData()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 362
    .local v1, "searchResults":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubWhosHereScreenViewModel$ClubWhosHereFilter:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 392
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown filter: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 396
    :cond_0
    :goto_0
    return-object v1

    .line 364
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 368
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 369
    .local v0, "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    iget-boolean v3, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isModerator:Z

    if-nez v3, :cond_1

    iget-boolean v3, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isOwner:Z

    if-nez v3, :cond_1

    .line 370
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 375
    .end local v0    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    :cond_2
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 376
    .restart local v0    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    iget-boolean v3, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isModerator:Z

    if-eqz v3, :cond_3

    .line 377
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 384
    .end local v0    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    :pswitch_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 385
    .restart local v0    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    iget-boolean v3, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isOwner:Z

    if-eqz v3, :cond_4

    .line 386
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 362
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private loadUserData()V
    .locals 6

    .prologue
    .line 602
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v2, :cond_0

    .line 603
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 606
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    monitor-enter v3

    .line 607
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 610
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 611
    .local v1, "xuidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 612
    .local v0, "member":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    if-eqz v4, :cond_1

    .line 613
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 620
    .end local v0    # "member":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    .end local v1    # "xuidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 617
    .restart local v1    # "xuidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    :try_start_1
    new-instance v2, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v2, v1, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 618
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    .line 620
    .end local v1    # "xuidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 621
    return-void
.end method

.method private onSearchResult(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V
    .locals 8
    .param p1, "response"    # Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;

    .prologue
    .line 649
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->isSearchingService:Z

    .line 650
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 652
    if-eqz p1, :cond_0

    .line 653
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->ownerXuid:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 654
    .local v0, "ownerXuidString":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;->getResults()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;

    .line 655
    .local v1, "result":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->latestSearchResults:Ljava/util/List;

    new-instance v4, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    iget-object v5, v1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorXuids:Ljava/util/Set;

    iget-object v7, v1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    iget-object v7, v7, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    iget-object v7, v1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    iget-object v7, v7, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    invoke-static {v7, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;ZZ)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 659
    .end local v0    # "ownerXuidString":Ljava/lang/String;
    .end local v1    # "result":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateAdapter()V

    .line 660
    return-void
.end method

.method private onUserDataLoadCompleted(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 624
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->TAG:Ljava/lang/String;

    const-string v4, "onUserDataLoadCompleted"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    monitor-enter v4

    .line 628
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 629
    .local v1, "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    if-eqz v5, :cond_0

    .line 630
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 631
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 632
    .local v2, "user":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v5, v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v6

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->getXuidLong()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 633
    iput-object v2, v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 634
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 642
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    .end local v2    # "user":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 630
    .restart local v0    # "i":I
    .restart local v1    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    .restart local v2    # "user":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 641
    .end local v0    # "i":I
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    .end local v2    # "user":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->sortAndFilterUserData()V

    .line 642
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 644
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateViewModelState()V

    .line 645
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateAdapter()V

    .line 646
    return-void
.end method

.method private sortAndFilterUserData()V
    .locals 26

    .prologue
    .line 510
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 511
    .local v4, "currentTime":J
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 512
    .local v2, "allList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 513
    .local v12, "membersList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 514
    .local v14, "moderatorList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 518
    .local v15, "ownerList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    sget-object v18, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->HEADING_LIST:Ljava/util/List;

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_f

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 519
    .local v8, "heading":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 520
    .local v3, "bucket":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    new-instance v6, Ljava/util/Date;

    iget-wide v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->currentTimeDelta:J

    move-wide/from16 v20, v0

    add-long v20, v20, v4

    move-wide/from16 v0, v20

    invoke-direct {v6, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 522
    .local v6, "cutoff":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 523
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v9, v0, :cond_5

    .line 524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 525
    .local v17, "user":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    move-object/from16 v20, v0

    if-nez v20, :cond_2

    .line 526
    const-string v20, "Heading in list, check logic"

    invoke-static/range {v20 .. v20}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 529
    add-int/lit8 v9, v9, -0x1

    .line 523
    :cond_1
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 530
    :cond_2
    iget-wide v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->currentTimeDelta:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-eqz v20, :cond_4

    iget-wide v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->currentTimeDelta:J

    move-wide/from16 v20, v0

    const-wide/32 v22, -0x493e0

    cmp-long v20, v20, v22

    if-nez v20, :cond_3

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    move-object/from16 v20, v0

    .line 531
    invoke-virtual/range {v20 .. v20}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenState()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    move-result-object v20

    sget-object v21, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->NotInClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_4

    :cond_3
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->presenceData:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    move-object/from16 v20, v0

    .line 532
    invoke-virtual/range {v20 .. v20}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->getLastSeenDate()Ljava/util/Date;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 533
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    add-int/lit8 v9, v9, -0x1

    goto :goto_2

    .line 537
    .end local v17    # "user":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    :cond_5
    monitor-exit v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v19

    if-lez v19, :cond_0

    .line 541
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 543
    .local v16, "subList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 545
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-interface {v2, v0, v3}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 546
    iget-object v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->headingText:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_6

    .line 547
    const/16 v19, 0x0

    new-instance v20, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v21

    iget-object v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->headingText:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v21 .. v23}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    iget-wide v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->currentTimeDelta:J

    move-wide/from16 v22, v0

    invoke-direct/range {v20 .. v23}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Ljava/lang/String;J)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v2, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 551
    :cond_6
    const/4 v11, 0x0

    .line 552
    .local v11, "membersCount":I
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_7
    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_8

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 553
    .local v10, "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    iget-boolean v0, v10, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isModerator:Z

    move/from16 v20, v0

    if-nez v20, :cond_7

    iget-boolean v0, v10, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isOwner:Z

    move/from16 v20, v0

    if-nez v20, :cond_7

    .line 554
    move-object/from16 v0, v16

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 555
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 537
    .end local v10    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    .end local v11    # "membersCount":I
    .end local v16    # "subList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    :catchall_0
    move-exception v18

    :try_start_1
    monitor-exit v19
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v18

    .line 559
    .restart local v11    # "membersCount":I
    .restart local v16    # "subList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    :cond_8
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v12, v0, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 560
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    .line 561
    if-lez v11, :cond_9

    .line 562
    const/16 v19, 0x0

    new-instance v20, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v21

    iget-object v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->headingText:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v21 .. v23}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    iget-wide v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->currentTimeDelta:J

    move-wide/from16 v22, v0

    invoke-direct/range {v20 .. v23}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Ljava/lang/String;J)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v12, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 566
    :cond_9
    const/4 v13, 0x0

    .line 567
    .local v13, "moderatorCount":I
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_a
    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_b

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 568
    .restart local v10    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    iget-boolean v0, v10, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isModerator:Z

    move/from16 v20, v0

    if-eqz v20, :cond_a

    .line 569
    move-object/from16 v0, v16

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 570
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 574
    .end local v10    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    :cond_b
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v14, v0, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 575
    if-lez v13, :cond_c

    .line 576
    const/16 v19, 0x0

    new-instance v20, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v21

    iget-object v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->headingText:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v21 .. v23}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    iget-wide v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->currentTimeDelta:J

    move-wide/from16 v22, v0

    invoke-direct/range {v20 .. v23}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Ljava/lang/String;J)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v14, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 580
    :cond_c
    const/4 v7, 0x0

    .line 581
    .local v7, "foundOwner":Z
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_d
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_e

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 582
    .restart local v10    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    iget-boolean v0, v10, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isOwner:Z

    move/from16 v20, v0

    if-eqz v20, :cond_d

    .line 583
    invoke-interface {v15, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584
    const/4 v7, 0x1

    .line 589
    .end local v10    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    :cond_e
    if-eqz v7, :cond_0

    .line 590
    const/16 v19, 0x0

    new-instance v20, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v21

    iget-object v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->headingText:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v21 .. v23}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    iget-wide v0, v8, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->currentTimeDelta:J

    move-wide/from16 v22, v0

    invoke-direct/range {v20 .. v23}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Ljava/lang/String;J)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v15, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 595
    .end local v3    # "bucket":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    .end local v6    # "cutoff":Ljava/util/Date;
    .end local v7    # "foundOwner":Z
    .end local v8    # "heading":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    .end local v9    # "i":I
    .end local v11    # "membersCount":I
    .end local v13    # "moderatorCount":I
    .end local v16    # "subList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    :cond_f
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->userDataList:Ljava/util/List;

    .line 596
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->memberDataList:Ljava/util/List;

    .line 597
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorDataList:Ljava/util/List;

    .line 598
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->ownerDataList:Ljava/util/List;

    .line 599
    return-void
.end method

.method private stopSearchTask()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->cancel()V

    .line 252
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    .line 254
    :cond_0
    return-void
.end method

.method private updateClubData(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 26
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 437
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 439
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v4

    .line 441
    .local v4, "clubRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v15}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v15

    invoke-static {v15}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    .line 442
    .local v13, "presenceList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    invoke-interface {v13}, Ljava/util/List;->hashCode()I

    move-result v12

    .line 443
    .local v12, "presenceHashCode":I
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v14

    .line 444
    .local v14, "rosterHashCode":I
    :goto_0
    mul-int/lit8 v15, v12, 0x1f

    add-int v5, v15, v14

    .line 447
    .local v5, "hashCode":I
    move-object/from16 v0, p0

    iget v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->clubHashCode:I

    if-eq v5, v15, :cond_b

    .line 448
    move-object/from16 v0, p0

    iput v5, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->clubHashCode:I

    .line 450
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v15}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v16

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->allMembersCount:J

    .line 451
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v15}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->moderatorsCount()J

    move-result-wide v16

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorCount:J

    .line 452
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v15}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 453
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v15}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->ownerXuid:J

    .line 455
    :cond_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->allMembersCount:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorCount:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    const-wide/16 v18, 0x1

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->membersCount:J

    .line 458
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorXuids:Ljava/util/Set;

    invoke-interface {v15}, Ljava/util/Set;->clear()V

    .line 459
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 461
    .local v11, "moderatorXuidsCopy":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz v4, :cond_2

    .line 462
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->moderator()Lcom/google/common/collect/ImmutableList;

    move-result-object v15

    invoke-static {v15}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 463
    .local v8, "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorXuids:Ljava/util/Set;

    move-object/from16 v16, v0

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    invoke-interface/range {v16 .. v17}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 464
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 443
    .end local v5    # "hashCode":I
    .end local v8    # "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .end local v11    # "moderatorXuidsCopy":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v14    # "rosterHashCode":I
    :cond_1
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 467
    .restart local v5    # "hashCode":I
    .restart local v11    # "moderatorXuidsCopy":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v14    # "rosterHashCode":I
    :cond_2
    const-string v15, "Roster not available to populate moderator list"

    invoke-static {v15}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 471
    :cond_3
    invoke-static {v13}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 474
    new-instance v9, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorCount:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    long-to-int v15, v0

    invoke-direct {v9, v15}, Ljava/util/ArrayList;-><init>(I)V

    .line 475
    .local v9, "moderatorList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    new-instance v7, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorCount:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    long-to-int v15, v0

    rsub-int/lit8 v15, v15, 0x64

    invoke-direct {v7, v15}, Ljava/util/ArrayList;-><init>(I)V

    .line 476
    .local v7, "memberList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_4
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_6

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 477
    .local v6, "member":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v11, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 478
    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 479
    :cond_5
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v16

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x64

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorCount:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    cmp-long v16, v16, v18

    if-gez v16, :cond_4

    .line 480
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 486
    .end local v6    # "member":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :cond_6
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_7

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    .line 487
    .local v10, "moderatorXuid":Ljava/lang/Long;
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    sget-object v18, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->EPOCH_TIMESTAMP:Ljava/lang/String;

    sget-object v19, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-static/range {v16 .. v19}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->with(JLjava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 490
    .end local v10    # "moderatorXuid":Ljava/lang/Long;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 491
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->clear()V

    .line 493
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_9

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 494
    .local v8, "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    move-object/from16 v18, v0

    new-instance v19, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    const/16 v20, 0x1

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->ownerXuid:J

    move-wide/from16 v24, v0

    cmp-long v15, v22, v24

    if-nez v15, :cond_8

    const/4 v15, 0x1

    :goto_5
    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v0, v8, v1, v15}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;ZZ)V

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 500
    .end local v8    # "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :catchall_0
    move-exception v15

    monitor-exit v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v15

    .line 494
    .restart local v8    # "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :cond_8
    const/4 v15, 0x0

    goto :goto_5

    .line 497
    .end local v8    # "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :cond_9
    :try_start_1
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 498
    .restart local v6    # "member":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadingDataList:Ljava/util/List;

    move-object/from16 v17, v0

    new-instance v18, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v6, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;ZZ)V

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 500
    .end local v6    # "member":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :cond_a
    monitor-exit v16
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 502
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->loadUserData()V

    .line 507
    .end local v7    # "memberList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    .end local v9    # "moderatorList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    .end local v11    # "moderatorXuidsCopy":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :goto_7
    return-void

    .line 504
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateViewModelState()V

    .line 505
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateAdapter()V

    goto :goto_7
.end method

.method private updateViewModelState()V
    .locals 2

    .prologue
    .line 406
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubWhosHereScreenViewModel$ClubWhosHereFilter:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getFilter()Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 434
    :goto_0
    return-void

    .line 408
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->userDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 409
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 411
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 415
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->memberDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 416
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 421
    :goto_1
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 422
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 418
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 424
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 428
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->ownerDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 429
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 431
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 406
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public getFilter()Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 308
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->filter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    return-object v0
.end method

.method public getInvalidReasonText()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 211
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 212
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 214
    .local v1, "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v2

    if-nez v2, :cond_2

    .line 215
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-ne v2, v3, :cond_1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0702da

    .line 216
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 220
    :goto_1
    return-object v2

    .line 212
    .end local v1    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 216
    .restart local v1    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070160

    .line 217
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 220
    :cond_2
    const-string v2, ""

    goto :goto_1
.end method

.method public getSearchHint()I
    .locals 2
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->filter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->ADMINS:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    if-ne v0, v1, :cond_0

    const v0, 0x7f0702ab

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0702aa

    goto :goto_0
.end method

.method public getSpinnerArrayItems()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->values()[Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserData()Ljava/util/List;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 328
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 330
    .local v1, "emptySearchTerm":Z
    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->serviceSearchEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getSearchResultData()Ljava/util/List;

    move-result-object v0

    .line 332
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    :goto_0
    if-nez v1, :cond_3

    .line 333
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .line 334
    .local v2, "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->matchesSearchTerm(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isHeading()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 335
    :cond_1
    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 330
    .end local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    .end local v2    # "item":Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getLocalUserData()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 340
    .restart local v0    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;>;"
    :cond_3
    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 303
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->isSearchingService:Z

    return v0
.end method

.method public loadOverride(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 263
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->ClubPresence:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 268
    return-void
.end method

.method public navigateToProfile(Ljava/lang/String;)V
    .locals 0
    .param p1, "userXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 400
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 402
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 403
    return-void
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 272
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onClubModelChanged (STATUS: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 277
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 278
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 280
    .local v1, "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_1
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 281
    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 283
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-eq v2, v3, :cond_2

    .line 284
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateClubData(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0

    .line 278
    .end local v1    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 286
    .restart local v1    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->InvalidState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 287
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 295
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 296
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 225
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubWhosHereScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 226
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 232
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Roster:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 234
    :cond_0
    return-void
.end method

.method public onStopOverride()V
    .locals 1

    .prologue
    .line 238
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStopOverride()V

    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 244
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->stopSearchTask()V

    .line 246
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    .line 247
    return-void
.end method

.method public serviceSearchEnabled()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 182
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubWhosHereScreenViewModel$ClubWhosHereFilter:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->filter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 191
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->allMembersCount:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->userDataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    move v0, v1

    .line 184
    goto :goto_0

    .line 186
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-eqz v2, :cond_1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->membersCount:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->memberDataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 188
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorCount:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorDataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 191
    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setFilter(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;)V
    .locals 1
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->filter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    if-ne v0, p1, :cond_0

    .line 324
    :goto_0
    return-void

    .line 321
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->filter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    .line 322
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateViewModelState()V

    .line 323
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateAdapter()V

    goto :goto_0
.end method

.method public setSearchTerm(Ljava/lang/CharSequence;)V
    .locals 5
    .param p1, "term"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v4, 0x1

    .line 162
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->stopSearchTask()V

    .line 164
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    .line 166
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->serviceSearchEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->isSearchingService:Z

    .line 169
    monitor-enter p0

    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->latestSearchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 171
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    new-instance v0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->searchResponseAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->searchAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->load(Z)V

    .line 178
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->updateAdapter()V

    .line 179
    return-void

    .line 164
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 171
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public showSearchSection()Z
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 151
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->serviceSearchEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 150
    :goto_0
    return v0

    .line 151
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showTooManyWarning()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 196
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubWhosHereScreenViewModel$ClubWhosHereFilter:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->filter:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 205
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->allMembersCount:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->userDataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    move v0, v1

    .line 198
    goto :goto_0

    .line 200
    :pswitch_1
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->membersCount:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->memberDataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 202
    :pswitch_2
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorCount:J

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->moderatorDataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;->currentSearchTerm:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 205
    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
