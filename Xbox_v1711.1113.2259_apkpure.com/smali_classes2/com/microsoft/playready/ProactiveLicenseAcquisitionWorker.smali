.class Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;
.super Ljava/lang/Object;
.source "ProactiveLicenseAcquisitionWorker.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mFactory:Lcom/microsoft/playready/PlayReadyFactory;

.field private mKeyID:Ljava/lang/String;

.field private mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/PlayReadyFactory;Lcom/microsoft/playready/ILicenseAcquisitionPlugin;Ljava/lang/String;)V
    .locals 1
    .param p1, "factory"    # Lcom/microsoft/playready/PlayReadyFactory;
    .param p2, "plugin"    # Lcom/microsoft/playready/ILicenseAcquisitionPlugin;
    .param p3, "keyID"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mKeyID:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .line 19
    iput-object v0, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 26
    iput-object p1, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 27
    iput-object p2, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .line 28
    iput-object p3, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mKeyID:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 34
    const/4 v1, 0x0

    .line 35
    .local v1, "licenseChallenge":Lcom/microsoft/playready/DrmProxy$ChallengeData;
    const/4 v2, 0x0

    .line 36
    .local v2, "licenseResponse":[B
    new-instance v0, Lcom/microsoft/playready/DrmProxy;

    iget-object v3, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mFactory:Lcom/microsoft/playready/PlayReadyFactory;

    invoke-direct {v0, v3}, Lcom/microsoft/playready/DrmProxy;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V

    .line 38
    .local v0, "drmProxy":Lcom/microsoft/playready/DrmProxy;
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 40
    new-instance v3, Ljava/lang/InterruptedException;

    invoke-direct {v3}, Ljava/lang/InterruptedException;-><init>()V

    throw v3

    .line 44
    :cond_0
    iget-object v3, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mKeyID:Ljava/lang/String;

    .line 45
    iget-object v4, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    invoke-interface {v4}, Lcom/microsoft/playready/ILicenseAcquisitionPlugin;->getChallengeCustomData()Ljava/lang/String;

    move-result-object v4

    .line 46
    iget-object v5, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    invoke-interface {v5}, Lcom/microsoft/playready/ILicenseAcquisitionPlugin;->getCurrentDomainInfo()Lcom/microsoft/playready/DomainInfo;

    move-result-object v5

    .line 43
    invoke-virtual {v0, v3, v4, v5}, Lcom/microsoft/playready/DrmProxy;->generateLicenseChallenge(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/playready/DomainInfo;)Lcom/microsoft/playready/DrmProxy$ChallengeData;

    move-result-object v1

    .line 48
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 50
    new-instance v3, Ljava/lang/InterruptedException;

    invoke-direct {v3}, Ljava/lang/InterruptedException;-><init>()V

    throw v3

    .line 53
    :cond_1
    iget-object v3, p0, Lcom/microsoft/playready/ProactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .line 54
    invoke-virtual {v1}, Lcom/microsoft/playready/DrmProxy$ChallengeData;->getChallengeBytes()[B

    move-result-object v4

    .line 55
    invoke-virtual {v1}, Lcom/microsoft/playready/DrmProxy$ChallengeData;->getEmbeddedServerUri()Ljava/lang/String;

    move-result-object v5

    .line 53
    invoke-interface {v3, v4, v5}, Lcom/microsoft/playready/ILicenseAcquisitionPlugin;->doLicenseRequest([BLjava/lang/String;)[B

    move-result-object v2

    .line 57
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 59
    new-instance v3, Ljava/lang/InterruptedException;

    invoke-direct {v3}, Ljava/lang/InterruptedException;-><init>()V

    throw v3

    .line 62
    :cond_2
    invoke-virtual {v0, v2}, Lcom/microsoft/playready/DrmProxy;->processLicenseReponse([B)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
