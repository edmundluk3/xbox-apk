.class public final Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;
.super Ljava/lang/Object;
.source "DataModule_ProvideSystemSettingsModelFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final module:Lcom/microsoft/xbox/data/DataModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/DataModule;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/DataModule;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;->module:Lcom/microsoft/xbox/data/DataModule;

    .line 19
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/DataModule;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/DataModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/DataModule;",
            ")",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;-><init>(Lcom/microsoft/xbox/data/DataModule;)V

    return-object v0
.end method

.method public static proxyProvideSystemSettingsModel(Lcom/microsoft/xbox/data/DataModule;)Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/DataModule;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/DataModule;->provideSystemSettingsModel()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;->module:Lcom/microsoft/xbox/data/DataModule;

    .line 24
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/DataModule;->provideSystemSettingsModel()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 23
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/DataModule_ProvideSystemSettingsModelFactory;->get()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    return-object v0
.end method
