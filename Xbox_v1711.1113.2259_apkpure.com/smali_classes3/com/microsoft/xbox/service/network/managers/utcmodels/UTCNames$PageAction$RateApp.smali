.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$RateApp;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RateApp"
.end annotation


# static fields
.field public static final Cancel:Ljava/lang/String; = "Rate App - Cancel"

.field public static final DontAskAgain:Ljava/lang/String; = "Rate App - Do Not Ask Again"

.field public static final Rate:Ljava/lang/String; = "Rate App - Rate the App"

.field public static final RemindLater:Ljava/lang/String; = "Rate App - Remind Me Later"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
