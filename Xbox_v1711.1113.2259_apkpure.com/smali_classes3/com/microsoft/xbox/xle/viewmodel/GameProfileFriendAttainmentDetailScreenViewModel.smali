.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
.source "GameProfileFriendAttainmentDetailScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;
    }
.end annotation


# instance fields
.field private friendItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

.field private final friendModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private isFriendAchievementDetailLoading:Z

.field private loadFriendAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;-><init>()V

    .line 25
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 26
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getCompareAchievementDetailFriendXuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 27
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getGameCompareProgressAchievement()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAttainmentDetailAdapter(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->onLoadFriendAchievementDetailDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->isFriendAchievementDetailLoading:Z

    return p1
.end method

.method private onLoadFriendAchievementDetailDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;I)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "achievementId"    # I

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->isFriendAchievementDetailLoading:Z

    .line 91
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 105
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->updateAdapter()V

    .line 106
    return-void

    .line 95
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p2, p3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAchievementDetailData(Ljava/lang/String;I)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 96
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 100
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 101
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-nez v0, :cond_3

    .line 110
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->isFriendAchievementDetailLoading:Z

    if-nez v0, :cond_1

    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 118
    :goto_0
    return-void

    .line 113
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 116
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getFriendGamertag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFriendPercentComplete()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFriendPreferredColor()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    goto :goto_0
.end method

.method public getFriendUnlockedDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->friendItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getTimeUnlocked()Ljava/util/Date;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->isFriendAchievementDetailLoading:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    instance-of v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->loadFriendAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->loadFriendAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->cancel()V

    .line 43
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getServiceConfigId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getAchievementId()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->loadFriendAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->loadFriendAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->load(Z)V

    .line 46
    :cond_1
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->load(Z)V

    .line 47
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->loadFriendsWhoEarnedThisAchievement:Z

    .line 34
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->onStartOverride()V

    .line 35
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->onStartOverride()V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->loadFriendAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel;->loadFriendAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendAttainmentDetailScreenViewModel$LoadFriendAchievementDetailAsyncTask;->cancel()V

    .line 75
    :cond_0
    return-void
.end method
