.class Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "AchievementsFriendsWhoEarnedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private achievementTextView:Landroid/widget/TextView;

.field private friendNameTextView:Landroid/widget/TextView;

.field private imageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$1;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;)Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->friendNameTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->friendNameTextView:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->achievementTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->achievementTextView:Landroid/widget/TextView;

    return-object p1
.end method
