.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$7;
.super Ljava/lang/Object;
.source "UTCClubs.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackPeopleNavigateToClub(JI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$clubId:J

.field final synthetic val$index:I


# direct methods
.method constructor <init>(JI)V
    .locals 1

    .prologue
    .line 222
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$7;->val$clubId:J

    iput p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$7;->val$index:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 225
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$7;->val$clubId:J

    invoke-static {v4, v5, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 226
    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$7;->val$index:I

    int-to-long v2, v1

    invoke-static {v4, v5, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 228
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 229
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ClubId"

    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$7;->val$clubId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 230
    const-string v1, "ListIndex"

    iget v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$7;->val$index:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 232
    const-string v1, "Clubs - People Navigate to Club"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 233
    return-void
.end method
