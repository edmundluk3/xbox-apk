.class Lorg/simpleframework/xml/util/Resolver$Cache;
.super Ljava/util/LinkedHashMap;
.source "Resolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/simpleframework/xml/util/Resolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Cache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/List",
        "<TM;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/simpleframework/xml/util/Resolver;


# direct methods
.method public constructor <init>(Lorg/simpleframework/xml/util/Resolver;)V
    .locals 3

    .prologue
    .line 307
    .local p0, "this":Lorg/simpleframework/xml/util/Resolver$Cache;, "Lorg/simpleframework/xml/util/Resolver<TM;>.Cache;"
    iput-object p1, p0, Lorg/simpleframework/xml/util/Resolver$Cache;->this$0:Lorg/simpleframework/xml/util/Resolver;

    .line 308
    const/16 v0, 0x400

    const/high16 v1, 0x3f400000    # 0.75f

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 309
    return-void
.end method


# virtual methods
.method public removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .param p1, "entry"    # Ljava/util/Map$Entry;

    .prologue
    .line 322
    .local p0, "this":Lorg/simpleframework/xml/util/Resolver$Cache;, "Lorg/simpleframework/xml/util/Resolver<TM;>.Cache;"
    invoke-virtual {p0}, Lorg/simpleframework/xml/util/Resolver$Cache;->size()I

    move-result v0

    const/16 v1, 0x400

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
