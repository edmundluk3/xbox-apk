.class public Lcom/microsoft/xbox/xle/app/activity/OOBEScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "OOBEScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    const-string v0, "OOBE"

    return-object v0
.end method

.method public getShouldShowAppbar()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method protected isShowUtilityBar()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 9
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 10
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/OOBEScreen;->setDesiredDrawerLockState(I)V

    .line 11
    return-void
.end method

.method public onCreateContentView()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method
