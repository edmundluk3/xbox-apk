.class public interface abstract Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;
.super Ljava/lang/Object;
.source "ActivityFeedFilterRepository.java"


# virtual methods
.method public abstract getPrefs()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setPrefs(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lio/reactivex/Completable;
    .param p1    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
