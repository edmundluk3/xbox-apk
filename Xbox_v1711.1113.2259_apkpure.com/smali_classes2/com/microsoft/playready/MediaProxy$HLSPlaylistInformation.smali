.class Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;
.super Ljava/lang/Object;
.source "MediaProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/MediaProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "HLSPlaylistInformation"
.end annotation


# instance fields
.field private final mFirstSegmentIndex:I

.field private final mKeyUrls:[Ljava/lang/String;

.field private final mPlaylistBody:Ljava/lang/String;

.field private final mPlaylistRootUri:Ljava/lang/String;

.field private final mSegmentUrls:[Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "firstSegmentIndex"    # I
    .param p2, "PlaylistRootUri"    # Ljava/lang/String;
    .param p3, "PlaylistBody"    # Ljava/lang/String;
    .param p4, "SegmentUrls"    # [Ljava/lang/String;
    .param p5, "KeyUrls"    # [Ljava/lang/String;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput p1, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mFirstSegmentIndex:I

    .line 78
    iput-object p2, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mPlaylistRootUri:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mPlaylistBody:Ljava/lang/String;

    .line 80
    iput-object p4, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mSegmentUrls:[Ljava/lang/String;

    .line 81
    iput-object p5, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mKeyUrls:[Ljava/lang/String;

    .line 82
    return-void
.end method


# virtual methods
.method public getFirstSegmentIndex()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mFirstSegmentIndex:I

    return v0
.end method

.method public getKeyUrls()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mKeyUrls:[Ljava/lang/String;

    return-object v0
.end method

.method public getPlaylistBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mPlaylistBody:Ljava/lang/String;

    return-object v0
.end method

.method public getPlaylistRootUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mPlaylistRootUri:Ljava/lang/String;

    return-object v0
.end method

.method public getSegmentUrls()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy$HLSPlaylistInformation;->mSegmentUrls:[Ljava/lang/String;

    return-object v0
.end method
