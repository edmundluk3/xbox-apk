.class Lcom/microsoft/playready/networkdevice/PrndDrmProxy$2;
.super Ljava/lang/Object;
.source "PrndDrmProxy.java"

# interfaces
.implements Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/playready/networkdevice/PrndDrmProxy;-><init>(Lcom/microsoft/playready/networkdevice/IPRNDFactory;Lcom/microsoft/playready/networkdevice/IReceiverSession;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy$2;->this$0:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call_1(I[B)Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;
    .locals 5
    .param p1, "arg1"    # I
    .param p2, "arg2"    # [B

    .prologue
    .line 115
    const/4 v2, 0x0

    .line 117
    .local v2, "retVal":Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;
    iget-object v3, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy$2;->this$0:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-static {v3}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->access$000(Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)Lcom/microsoft/playready/networkdevice/IReceiverSession;

    move-result-object v3

    invoke-interface {v3}, Lcom/microsoft/playready/networkdevice/IReceiverSession;->getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    move-result-object v1

    .line 119
    .local v1, "plugin":Lcom/microsoft/playready/networkdevice/IReceiverPlugin;
    if-eqz v1, :cond_0

    .line 121
    invoke-static {}, Lcom/microsoft/playready/networkdevice/ContentIDType;->values()[Lcom/microsoft/playready/networkdevice/ContentIDType;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-interface {v1, v3, p2}, Lcom/microsoft/playready/networkdevice/IReceiverPlugin;->getLicenseFetchCustomData(Lcom/microsoft/playready/networkdevice/ContentIDType;[B)Lcom/microsoft/playready/networkdevice/CustomData;

    move-result-object v0

    .line 123
    .local v0, "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    if-eqz v0, :cond_0

    .line 125
    new-instance v2, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;

    .end local v2    # "retVal":Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;
    iget-object v3, v0, Lcom/microsoft/playready/networkdevice/CustomData;->CustomDataTypeID:[B

    iget-object v4, v0, Lcom/microsoft/playready/networkdevice/CustomData;->CustomData:[B

    invoke-direct {v2, v3, v4}, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;-><init>([B[B)V

    .line 129
    .end local v0    # "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    .restart local v2    # "retVal":Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;
    :cond_0
    return-object v2
.end method

.method public call_2(Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;)Z
    .locals 4
    .param p1, "arg1"    # Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;

    .prologue
    .line 135
    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/PrndDrmProxy$2;->this$0:Lcom/microsoft/playready/networkdevice/PrndDrmProxy;

    invoke-static {v2}, Lcom/microsoft/playready/networkdevice/PrndDrmProxy;->access$000(Lcom/microsoft/playready/networkdevice/PrndDrmProxy;)Lcom/microsoft/playready/networkdevice/IReceiverSession;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/playready/networkdevice/IReceiverSession;->getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;

    move-result-object v1

    .line 137
    .local v1, "plugin":Lcom/microsoft/playready/networkdevice/IReceiverPlugin;
    if-eqz v1, :cond_1

    .line 139
    const/4 v0, 0x0

    .line 141
    .local v0, "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    if-eqz p1, :cond_0

    .line 143
    new-instance v0, Lcom/microsoft/playready/networkdevice/CustomData;

    .end local v0    # "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    iget-object v2, p1, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;->mField1:[B

    iget-object v3, p1, Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;->mField2:[B

    invoke-direct {v0, v2, v3}, Lcom/microsoft/playready/networkdevice/CustomData;-><init>([B[B)V

    .line 146
    .restart local v0    # "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    :cond_0
    invoke-interface {v1, v0}, Lcom/microsoft/playready/networkdevice/IReceiverPlugin;->onProcessLicenseCustomResponseData(Lcom/microsoft/playready/networkdevice/CustomData;)Z

    move-result v2

    .line 149
    .end local v0    # "cd":Lcom/microsoft/playready/networkdevice/CustomData;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method
