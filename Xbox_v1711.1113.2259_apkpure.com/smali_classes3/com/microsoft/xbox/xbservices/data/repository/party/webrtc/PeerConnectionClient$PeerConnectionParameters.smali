.class public Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PeerConnectionParameters"
.end annotation


# instance fields
.field public final aecDump:Z

.field public final audioCodec:Ljava/lang/String;

.field public final audioStartBitrate:I

.field private final dataChannelParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

.field public final disableBuiltInAEC:Z

.field public final disableBuiltInAGC:Z

.field public final disableBuiltInNS:Z

.field public final enableLevelControl:Z

.field public final loopback:Z

.field public final noAudioProcessing:Z

.field public final tracing:Z

.field public final useOpenSLES:Z

.field public final videoCodecHwAcceleration:Z


# direct methods
.method public constructor <init>(ZZZILjava/lang/String;ZZZZZZZLcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;)V
    .locals 0
    .param p1, "loopback"    # Z
    .param p2, "tracing"    # Z
    .param p3, "videoCodecHwAcceleration"    # Z
    .param p4, "audioStartBitrate"    # I
    .param p5, "audioCodec"    # Ljava/lang/String;
    .param p6, "noAudioProcessing"    # Z
    .param p7, "aecDump"    # Z
    .param p8, "useOpenSLES"    # Z
    .param p9, "disableBuiltInAEC"    # Z
    .param p10, "disableBuiltInAGC"    # Z
    .param p11, "disableBuiltInNS"    # Z
    .param p12, "enableLevelControl"    # Z
    .param p13, "dataChannelParameters"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->loopback:Z

    .line 167
    iput-boolean p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->tracing:Z

    .line 168
    iput-boolean p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->videoCodecHwAcceleration:Z

    .line 169
    iput p4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->audioStartBitrate:I

    .line 170
    iput-object p5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->audioCodec:Ljava/lang/String;

    .line 171
    iput-boolean p6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->noAudioProcessing:Z

    .line 172
    iput-boolean p7, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->aecDump:Z

    .line 173
    iput-boolean p8, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->useOpenSLES:Z

    .line 174
    iput-boolean p9, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->disableBuiltInAEC:Z

    .line 175
    iput-boolean p10, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->disableBuiltInAGC:Z

    .line 176
    iput-boolean p11, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->disableBuiltInNS:Z

    .line 177
    iput-boolean p12, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->enableLevelControl:Z

    .line 178
    iput-object p13, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->dataChannelParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    .line 179
    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;

    .prologue
    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PeerConnectionParameters;->dataChannelParameters:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;

    return-object v0
.end method
