.class public final enum Lcom/microsoft/xbox/smartglass/AuthError;
.super Ljava/lang/Enum;
.source "AuthError.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/AuthError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum AccountMaintenanceRequired:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum AccountNotCreated:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum AccountTypeNotAllowed:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum AgeVerificationRequired:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum BillingMaintenanceRequired:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum ChildAccountNotInFamily:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum ContentIsolation:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum ContentNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum ContentUpdateRequired:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum CountryNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum CsvTransitionRequired:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum Curfew:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum DevModeNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum DeviceChallengeRequired:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum EnforcementBan:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum ExpiredDeviceToken:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum ExpiredTitleToken:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum ExpiredUserToken:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum GamertagMustChange:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum InvalidDeviceToken:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum InvalidRefreshToken:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum InvalidTitleToken:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum InvalidUserToken:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum NewTermsOfUse:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum None:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum ParentalControlsBan:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum RetailAccountNotAllowed:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum RetailContentNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum SandboxNotAllowed:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum SignInCountExceeded:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum SubscriptionNotActivated:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum SystemUpdateRequired:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum ThirdPartyBan:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum Unknown:Lcom/microsoft/xbox/smartglass/AuthError;

.field public static final enum UnknownUser:Lcom/microsoft/xbox/smartglass/AuthError;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/AuthError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "None"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->None:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "Unknown"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->Unknown:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "DevModeNotAuthorized"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->DevModeNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "SystemUpdateRequired"

    invoke-direct {v2, v3, v8, v8}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->SystemUpdateRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "ContentUpdateRequired"

    invoke-direct {v2, v3, v9, v9}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->ContentUpdateRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "EnforcementBan"

    const/4 v4, 0x5

    const/4 v5, 0x5

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->EnforcementBan:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 24
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "ThirdPartyBan"

    const/4 v4, 0x6

    const/4 v5, 0x6

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->ThirdPartyBan:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "ParentalControlsBan"

    const/4 v4, 0x7

    const/4 v5, 0x7

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->ParentalControlsBan:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "SubscriptionNotActivated"

    const/16 v4, 0x8

    const/16 v5, 0x8

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->SubscriptionNotActivated:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "BillingMaintenanceRequired"

    const/16 v4, 0x9

    const/16 v5, 0x9

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->BillingMaintenanceRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "AccountNotCreated"

    const/16 v4, 0xa

    const/16 v5, 0xa

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->AccountNotCreated:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "NewTermsOfUse"

    const/16 v4, 0xb

    const/16 v5, 0xb

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->NewTermsOfUse:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 36
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "CountryNotAuthorized"

    const/16 v4, 0xc

    const/16 v5, 0xc

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->CountryNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 38
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "AgeVerificationRequired"

    const/16 v4, 0xd

    const/16 v5, 0xd

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->AgeVerificationRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 40
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "Curfew"

    const/16 v4, 0xe

    const/16 v5, 0xe

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->Curfew:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 42
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "ChildAccountNotInFamily"

    const/16 v4, 0xf

    const/16 v5, 0xf

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->ChildAccountNotInFamily:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 44
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "CsvTransitionRequired"

    const/16 v4, 0x10

    const/16 v5, 0x10

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->CsvTransitionRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 46
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "AccountMaintenanceRequired"

    const/16 v4, 0x11

    const/16 v5, 0x11

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->AccountMaintenanceRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 48
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "AccountTypeNotAllowed"

    const/16 v4, 0x12

    const/16 v5, 0x12

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->AccountTypeNotAllowed:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 50
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "ContentIsolation"

    const/16 v4, 0x13

    const/16 v5, 0x13

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->ContentIsolation:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 52
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "GamertagMustChange"

    const/16 v4, 0x14

    const/16 v5, 0x14

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->GamertagMustChange:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 54
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "DeviceChallengeRequired"

    const/16 v4, 0x15

    const/16 v5, 0x15

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->DeviceChallengeRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 56
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "SignInCountExceeded"

    const/16 v4, 0x16

    const/16 v5, 0x16

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->SignInCountExceeded:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 58
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "RetailAccountNotAllowed"

    const/16 v4, 0x17

    const/16 v5, 0x17

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->RetailAccountNotAllowed:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 60
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "SandboxNotAllowed"

    const/16 v4, 0x18

    const/16 v5, 0x18

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->SandboxNotAllowed:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 62
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "UnknownUser"

    const/16 v4, 0x19

    const/16 v5, 0x19

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->UnknownUser:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 64
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "RetailContentNotAuthorized"

    const/16 v4, 0x1a

    const/16 v5, 0x1a

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->RetailContentNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 66
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "ContentNotAuthorized"

    const/16 v4, 0x1b

    const/16 v5, 0x1b

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->ContentNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 68
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "ExpiredDeviceToken"

    const/16 v4, 0x1c

    const/16 v5, 0x1c

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->ExpiredDeviceToken:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 70
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "ExpiredTitleToken"

    const/16 v4, 0x1d

    const/16 v5, 0x1d

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->ExpiredTitleToken:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 72
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "ExpiredUserToken"

    const/16 v4, 0x1e

    const/16 v5, 0x1e

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->ExpiredUserToken:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 74
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "InvalidDeviceToken"

    const/16 v4, 0x1f

    const/16 v5, 0x1f

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->InvalidDeviceToken:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 76
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "InvalidTitleToken"

    const/16 v4, 0x20

    const/16 v5, 0x20

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->InvalidTitleToken:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 78
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "InvalidUserToken"

    const/16 v4, 0x21

    const/16 v5, 0x21

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->InvalidUserToken:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 80
    new-instance v2, Lcom/microsoft/xbox/smartglass/AuthError;

    const-string v3, "InvalidRefreshToken"

    const/16 v4, 0x22

    const/16 v5, 0x22

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/AuthError;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->InvalidRefreshToken:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 10
    const/16 v2, 0x23

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/AuthError;

    sget-object v3, Lcom/microsoft/xbox/smartglass/AuthError;->None:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/AuthError;->Unknown:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/AuthError;->DevModeNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/smartglass/AuthError;->SystemUpdateRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/smartglass/AuthError;->ContentUpdateRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->EnforcementBan:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->ThirdPartyBan:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->ParentalControlsBan:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->SubscriptionNotActivated:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->BillingMaintenanceRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->AccountNotCreated:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->NewTermsOfUse:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->CountryNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->AgeVerificationRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->Curfew:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->ChildAccountNotInFamily:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->CsvTransitionRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x11

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->AccountMaintenanceRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x12

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->AccountTypeNotAllowed:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x13

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->ContentIsolation:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x14

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->GamertagMustChange:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x15

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->DeviceChallengeRequired:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x16

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->SignInCountExceeded:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x17

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->RetailAccountNotAllowed:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x18

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->SandboxNotAllowed:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x19

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->UnknownUser:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->RetailContentNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->ContentNotAuthorized:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->ExpiredDeviceToken:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->ExpiredTitleToken:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->ExpiredUserToken:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->InvalidDeviceToken:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x20

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->InvalidTitleToken:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x21

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->InvalidUserToken:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    const/16 v3, 0x22

    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->InvalidRefreshToken:Lcom/microsoft/xbox/smartglass/AuthError;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->$VALUES:[Lcom/microsoft/xbox/smartglass/AuthError;

    .line 83
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/AuthError;->_lookup:Landroid/util/SparseArray;

    .line 86
    invoke-static {}, Lcom/microsoft/xbox/smartglass/AuthError;->values()[Lcom/microsoft/xbox/smartglass/AuthError;

    move-result-object v2

    array-length v3, v2

    .local v0, "val":Lcom/microsoft/xbox/smartglass/AuthError;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 87
    sget-object v4, Lcom/microsoft/xbox/smartglass/AuthError;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/AuthError;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 91
    iput p3, p0, Lcom/microsoft/xbox/smartglass/AuthError;->_value:I

    .line 92
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/AuthError;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 108
    sget-object v1, Lcom/microsoft/xbox/smartglass/AuthError;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/AuthError;

    .line 109
    .local v0, "val":Lcom/microsoft/xbox/smartglass/AuthError;
    if-nez v0, :cond_0

    .line 110
    sget-object v0, Lcom/microsoft/xbox/smartglass/AuthError;->None:Lcom/microsoft/xbox/smartglass/AuthError;

    .line 113
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/AuthError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/smartglass/AuthError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/AuthError;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/AuthError;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/smartglass/AuthError;->$VALUES:[Lcom/microsoft/xbox/smartglass/AuthError;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/AuthError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/AuthError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/microsoft/xbox/smartglass/AuthError;->_value:I

    return v0
.end method
