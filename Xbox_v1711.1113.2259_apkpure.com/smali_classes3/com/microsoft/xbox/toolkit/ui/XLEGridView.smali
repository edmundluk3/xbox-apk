.class public Lcom/microsoft/xbox/toolkit/ui/XLEGridView;
.super Landroid/widget/GridView;
.source "XLEGridView.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/ui/XLEAbstractAdapterViewBase;


# static fields
.field private static final GRID_VIEW_ANIMATION_NAME:Ljava/lang/String; = "GridView"

.field private static final LAYOUT_BLOCK_TIMEOUT_MS:I = 0x1f4

.field private static final SCROLL_BLOCK_TIMEOUT_MS:I = 0x7530


# instance fields
.field private blocking:Z

.field private firstTouchAfterNonBlocking:Z

.field private horizontalSpacing:I

.field private listViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private verticalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->blocking:Z

    .line 24
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->firstTouchAfterNonBlocking:Z

    .line 28
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLEGridView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->listViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 48
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setSoundEffectsEnabled(Z)V

    .line 49
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setSelector(I)V

    .line 50
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView$2;-><init>(Lcom/microsoft/xbox/toolkit/ui/XLEGridView;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/XLEGridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->onLayoutAnimationStart()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/ui/XLEGridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->onLayoutAnimationEnd()V

    return-void
.end method

.method private onLayoutAnimationEnd()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 136
    const-string v0, "XLEGridView"

    const-string v1, "LayoutAnimation end"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setBlocking(Z)V

    .line 138
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 139
    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 140
    return-void
.end method

.method private onLayoutAnimationStart()V
    .locals 2

    .prologue
    .line 130
    const-string v0, "XLEGridView"

    const-string v1, "LayoutAnimation start"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 132
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setBlocking(Z)V

    .line 133
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->blocking:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/GridView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAnimationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    const-string v0, "GridView"

    return-object v0
.end method

.method public getBlocking()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->blocking:Z

    return v0
.end method

.method public getHorizontalSpacingCompat()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->horizontalSpacing:I

    return v0
.end method

.method public getScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getFirstVisiblePosition()I

    move-result v0

    .line 150
    .local v0, "index":I
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 151
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_0

    .line 153
    .local v1, "offset":I
    :goto_0
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    invoke-direct {v3, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;-><init>(II)V

    return-object v3

    .line 151
    .end local v1    # "offset":I
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0
.end method

.method public getVerticalSpacingCompat()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->verticalSpacing:I

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 169
    return-object p0
.end method

.method public onDataUpdated()V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Landroid/widget/GridView;->onMeasure(II)V

    .line 76
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 77
    return-void

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 113
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->blocking:Z

    if-eqz v0, :cond_0

    .line 115
    const/4 v0, 0x1

    .line 125
    :goto_0
    return v0

    .line 119
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->firstTouchAfterNonBlocking:Z

    if-eqz v0, :cond_2

    .line 120
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 121
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    .line 123
    :cond_1
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->firstTouchAfterNonBlocking:Z

    .line 125
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/GridView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public restoreScrollState(II)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "offset"    # I

    .prologue
    .line 159
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setSelection(I)V

    .line 160
    return-void
.end method

.method public setBlocking(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->blocking:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->firstTouchAfterNonBlocking:Z

    .line 82
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->blocking:Z

    if-eq v0, p1, :cond_0

    .line 83
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->blocking:Z

    .line 84
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->blocking:Z

    if-eqz v0, :cond_2

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListLayout:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 90
    :cond_0
    :goto_1
    return-void

    .line 80
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 87
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListLayout:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->clearBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;)V

    goto :goto_1
.end method

.method public setHorizontalSpacing(I)V
    .locals 0
    .param p1, "horizontalSpacing"    # I

    .prologue
    .line 180
    invoke-super {p0, p1}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 181
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->horizontalSpacing:I

    .line 182
    return-void
.end method

.method public setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V
    .locals 1
    .param p1, "controller"    # Landroid/view/animation/LayoutAnimationController;

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/widget/GridView;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->listViewAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setLayoutAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 104
    return-void
.end method

.method public setVerticalSpacing(I)V
    .locals 0
    .param p1, "verticalSpacing"    # I

    .prologue
    .line 174
    invoke-super {p0, p1}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 175
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->verticalSpacing:I

    .line 176
    return-void
.end method
