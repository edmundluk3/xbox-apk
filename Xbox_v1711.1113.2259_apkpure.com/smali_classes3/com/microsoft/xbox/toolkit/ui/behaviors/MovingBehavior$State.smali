.class Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;
.super Ljava/lang/Object;
.source "MovingBehavior.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "State"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;->offset:I

    .line 176
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;->offset:I

    .line 180
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;->offset:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 193
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/MovingBehavior$State;->offset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 194
    return-void
.end method
