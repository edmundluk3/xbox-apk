.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
.super Ljava/lang/Object;
.source "EDSV2SearchResult.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$PostProcessableGson;


# instance fields
.field private continuationToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ContinuationToken"
    .end annotation
.end field

.field private filteredCount:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private impressionGuid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ImpressionGuid"
    .end annotation
.end field

.field private items:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private totalCount:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TotalCount"
    .end annotation
.end field

.field private totals:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Totals"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/rawtypes/SearchMediaGroupCount;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContinuationToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->continuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public getFilterResultCount()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v0, "fixedfilterResultCount":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;>;"
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;

    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_ALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_ALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isVideoBlocked()Z

    move-result v1

    if-nez v1, :cond_0

    .line 43
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;

    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MOVIE:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MOVIE:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;

    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_TV:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_TV:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;

    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_XBOXGAME:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_XBOXGAME:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;

    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_APP:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_APP:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isMusicBlocked()Z

    move-result v1

    if-nez v1, :cond_1

    .line 49
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;

    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSIC:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICARTIST:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 50
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;I)V

    .line 49
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    :cond_1
    return-object v0
.end method

.method public getFilterTypeCount(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)I
    .locals 1
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .prologue
    .line 27
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_ALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    if-ne p1, v0, :cond_0

    .line 28
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->totalCount:I

    .line 35
    :goto_0
    return v0

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->filteredCount:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->filteredCount:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->filteredCount:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 35
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImpressionGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->impressionGuid:Ljava/lang/String;

    return-object v0
.end method

.method public getItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->items:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->totalCount:I

    return v0
.end method

.method public postProcess()V
    .locals 7

    .prologue
    .line 97
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->items:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 98
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 99
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->items:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    .line 100
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->items:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 102
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 108
    .end local v1    # "i":I
    :cond_1
    const/4 v3, 0x0

    iput v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->totalCount:I

    .line 109
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->totals:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 111
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->filteredCount:Ljava/util/HashMap;

    .line 112
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->totals:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/SearchMediaGroupCount;

    .line 113
    .local v0, "groupCount":Lcom/microsoft/xbox/service/model/edsv2/rawtypes/SearchMediaGroupCount;
    iget-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/SearchMediaGroupCount;->Name:Ljava/lang/String;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->getTypeFromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    move-result-object v2

    .line 114
    .local v2, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    if-eqz v2, :cond_2

    .line 115
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->filteredCount:Ljava/util/HashMap;

    iget v5, v0, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/SearchMediaGroupCount;->Count:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->totalCount:I

    iget v5, v0, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/SearchMediaGroupCount;->Count:I

    add-int/2addr v4, v5

    iput v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->totalCount:I

    goto :goto_1

    .line 118
    :cond_2
    const-string v4, "EDSV2SearchResult"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "filter type ignored "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/SearchMediaGroupCount;->Name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 122
    .end local v0    # "groupCount":Lcom/microsoft/xbox/service/model/edsv2/rawtypes/SearchMediaGroupCount;
    .end local v2    # "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    :cond_3
    return-void
.end method

.method public setContinuationToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "continuationToken"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->continuationToken:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setItems(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    if-eqz p1, :cond_1

    .line 63
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 64
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 65
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 67
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    .end local v0    # "i":I
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->items:Ljava/util/ArrayList;

    .line 72
    return-void
.end method

.method public setTotalCount(I)V
    .locals 0
    .param p1, "totalCount"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->totalCount:I

    .line 92
    return-void
.end method
