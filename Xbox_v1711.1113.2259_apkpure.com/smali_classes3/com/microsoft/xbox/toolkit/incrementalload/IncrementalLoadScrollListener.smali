.class public final Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "IncrementalLoadScrollListener.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v7/widget/RecyclerView$OnScrollListener;"
    }
.end annotation


# static fields
.field private static final DEFAULT_THRESHOLD:F = 0.75f


# instance fields
.field private final incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader",
            "<TT;>;"
        }
    .end annotation
.end field

.field private isLoading:Z

.field private final layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

.field private previousTotalItemCount:I

.field private final threshold:F


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;Landroid/support/v7/widget/LinearLayoutManager;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "layoutManager"    # Landroid/support/v7/widget/LinearLayoutManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader",
            "<TT;>;",
            "Landroid/support/v7/widget/LinearLayoutManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener<TT;>;"
    .local p1, "incrementalLoader":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader<TT;>;"
    const/high16 v0, 0x3f400000    # 0.75f

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;-><init>(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;Landroid/support/v7/widget/LinearLayoutManager;F)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;Landroid/support/v7/widget/LinearLayoutManager;F)V
    .locals 6
    .param p1    # Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "layoutManager"    # Landroid/support/v7/widget/LinearLayoutManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "threshold"    # F
        .annotation build Landroid/support/annotation/FloatRange;
            from = 0.0
            to = 1.0
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader",
            "<TT;>;",
            "Landroid/support/v7/widget/LinearLayoutManager;",
            "F)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener<TT;>;"
    .local p1, "incrementalLoader":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader<TT;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->previousTotalItemCount:I

    .line 32
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 34
    const-wide/16 v0, 0x0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    float-to-double v4, p3

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->floatRange(DDD)V

    .line 36
    iput p3, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->threshold:F

    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;

    .line 38
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    .line 39
    return-void
.end method

.method private isThresholdExceeded(I)Z
    .locals 4
    .param p1, "realizedItemCount"    # I

    .prologue
    .line 72
    .local p0, "this":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener<TT;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v2

    add-int/2addr v2, p1

    int-to-float v0, v2

    .line 73
    .local v0, "realizedWindowPos":F
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->getItemCount()I

    move-result v1

    .line 75
    .local v1, "totalCount":I
    if-eqz v1, :cond_0

    int-to-float v2, v1

    div-float v2, v0, v2

    iget v3, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->threshold:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onLoadComplete()V
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener<TT;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->isLoading:Z

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->getItemCount()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->previousTotalItemCount:I

    .line 69
    return-void
.end method

.method private resetState()V
    .locals 1

    .prologue
    .line 62
    .local p0, "this":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->getItemCount()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->previousTotalItemCount:I

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->getItemCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->isLoading:Z

    .line 64
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 43
    .local p0, "this":Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;, "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener<TT;>;"
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;->onScrolled(Landroid/support/v7/widget/RecyclerView;II)V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->getItemCount()I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->previousTotalItemCount:I

    if-ge v0, v1, :cond_0

    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->resetState()V

    .line 49
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->isLoading:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->getItemCount()I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->previousTotalItemCount:I

    if-le v0, v1, :cond_1

    .line 50
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->onLoadComplete()V

    .line 53
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->isLoading:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;

    .line 54
    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;->hasMoreItemsToLoad()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->isThresholdExceeded(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->isLoading:Z

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoadScrollListener;->incrementalLoader:Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;

    invoke-interface {v0}, Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;->loadMoreItemsAsync()V

    .line 59
    :cond_2
    return-void
.end method
