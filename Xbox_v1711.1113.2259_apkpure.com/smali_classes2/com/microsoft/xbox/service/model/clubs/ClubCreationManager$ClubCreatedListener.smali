.class public interface abstract Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;
.super Ljava/lang/Object;
.source "ClubCreationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ClubCreatedListener"
.end annotation


# virtual methods
.method public abstract onClubCreated(J)V
.end method

.method public abstract onClubCreationError(I)V
.end method
