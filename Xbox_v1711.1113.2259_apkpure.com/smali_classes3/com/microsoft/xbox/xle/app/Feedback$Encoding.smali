.class final enum Lcom/microsoft/xbox/xle/app/Feedback$Encoding;
.super Ljava/lang/Enum;
.source "Feedback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/Feedback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Encoding"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/Feedback$Encoding;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

.field public static final enum UTF16:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

.field public static final enum UTF16LE:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

.field public static final enum UTF8:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;


# instance fields
.field valueName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 126
    new-instance v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    const-string v1, "UTF8"

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->UTF8:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    new-instance v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    const-string v1, "UTF16"

    const-string v2, "UTF-16"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->UTF16:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    new-instance v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    const-string v1, "UTF16LE"

    const-string v2, "UTF-16LE"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->UTF16LE:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    .line 125
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->UTF8:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->UTF16:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->UTF16LE:Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->$VALUES:[Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->valueName:Ljava/lang/String;

    .line 131
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->valueName:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/Feedback$Encoding;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    const-class v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/Feedback$Encoding;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->$VALUES:[Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/Feedback$Encoding;

    return-object v0
.end method


# virtual methods
.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/Feedback$Encoding;->valueName:Ljava/lang/String;

    return-object v0
.end method
