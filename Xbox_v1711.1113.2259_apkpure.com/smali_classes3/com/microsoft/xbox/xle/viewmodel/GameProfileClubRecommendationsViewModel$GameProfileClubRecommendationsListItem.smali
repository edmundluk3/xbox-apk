.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;
.super Ljava/lang/Object;
.source "GameProfileClubRecommendationsViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GameProfileClubRecommendationsListItem"
.end annotation


# instance fields
.field public final clubCardCategoryItem:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clubCardCategoryItem"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 201
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;->clubCardCategoryItem:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .line 202
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 206
    if-ne p1, p0, :cond_0

    .line 207
    const/4 v1, 0x1

    .line 212
    :goto_0
    return v1

    .line 208
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;

    if-nez v1, :cond_1

    .line 209
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 211
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;

    .line 212
    .local v0, "other":Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;->clubCardCategoryItem:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;->clubCardCategoryItem:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 218
    const/16 v0, 0x11

    .line 219
    .local v0, "result":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;->clubCardCategoryItem:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 220
    return v0
.end method
