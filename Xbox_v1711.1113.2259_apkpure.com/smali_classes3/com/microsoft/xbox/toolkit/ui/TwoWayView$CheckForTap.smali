.class final Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;
.super Ljava/lang/Object;
.source "TwoWayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CheckForTap"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 0

    .prologue
    .line 6046
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p2, "x1"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;

    .prologue
    .line 6046
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 6049
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 6094
    :cond_0
    :goto_0
    return-void

    .line 6053
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$402(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    .line 6055
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3300(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 6056
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v4

    if-nez v4, :cond_0

    .line 6057
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3502(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    .line 6059
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$500(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 6060
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 6061
    invoke-virtual {v0, v7}, Landroid/view/View;->setPressed(Z)V

    .line 6063
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3600(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    .line 6064
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v5

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3700(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;ILandroid/view/View;)V

    .line 6065
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->refreshDrawableState()V

    .line 6067
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v5

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3700(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;ILandroid/view/View;)V

    .line 6068
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->refreshDrawableState()V

    .line 6070
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isLongClickable()Z

    move-result v2

    .line 6072
    .local v2, "longClickable":Z
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3800(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 6073
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3800(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 6075
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_2

    instance-of v4, v1, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v4, :cond_2

    .line 6076
    if-eqz v2, :cond_3

    .line 6077
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v3

    .line 6078
    .local v3, "longPressTimeout":I
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 6085
    .end local v3    # "longPressTimeout":I
    :cond_2
    :goto_1
    if-eqz v2, :cond_4

    .line 6086
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3900(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    goto/16 :goto_0

    .line 6080
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    :cond_3
    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_1

    .line 6088
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$402(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    goto/16 :goto_0

    .line 6091
    .end local v2    # "longClickable":Z
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForTap;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$402(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;I)I

    goto/16 :goto_0
.end method
