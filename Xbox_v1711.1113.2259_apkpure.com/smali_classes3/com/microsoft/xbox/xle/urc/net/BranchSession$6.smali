.class Lcom/microsoft/xbox/xle/urc/net/BranchSession$6;
.super Ljava/lang/Object;
.source "BranchSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/net/BranchSession;->onNotifyStreamError(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

.field final synthetic val$errorType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/net/BranchSession;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    .prologue
    .line 436
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$6;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$6;->val$errorType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 439
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$6;->this$0:Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->access$400(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;

    .line 440
    .local v0, "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/BranchSession$6;->val$errorType:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;->onStreamError(Ljava/lang/String;)V

    goto :goto_0

    .line 442
    .end local v0    # "l":Lcom/microsoft/xbox/xle/urc/net/BranchSession$IStreamListener;
    :cond_0
    return-void
.end method
