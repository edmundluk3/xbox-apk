.class public final enum Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;
.super Ljava/lang/Enum;
.source "MediaTransportCapabilities.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum CanFastForward:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum CanPause:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum CanPlay:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum CanPlayAndPause:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum CanRewind:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum CanSeek:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum CanSkipBackward:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum CanSkipForward:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum CanStop:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum IsLiveTransport:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field public static final enum None:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "None"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->None:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "CanStop"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanStop:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "CanPause"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanPause:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "CanRewind"

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4, v8}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanRewind:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "CanFastForward"

    invoke-direct {v2, v3, v8, v9}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanFastForward:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 24
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "CanPlay"

    const/4 v4, 0x5

    const/16 v5, 0x10

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanPlay:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "CanPlayAndPause"

    const/4 v4, 0x6

    const/16 v5, 0x20

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanPlayAndPause:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "CanSkipForward"

    const/4 v4, 0x7

    const/16 v5, 0x40

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanSkipForward:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "CanSkipBackward"

    const/16 v4, 0x80

    invoke-direct {v2, v3, v9, v4}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanSkipBackward:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "CanSeek"

    const/16 v4, 0x9

    const/16 v5, 0x100

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanSeek:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    const-string v3, "IsLiveTransport"

    const/16 v4, 0xa

    const/16 v5, 0x200

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->IsLiveTransport:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 12
    const/16 v2, 0xb

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->None:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanStop:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanPause:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v3, v2, v7

    const/4 v3, 0x3

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanRewind:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanFastForward:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v3, v2, v8

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanPlay:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanPlayAndPause:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanSkipForward:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanSkipBackward:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v3, v2, v9

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->CanSeek:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->IsLiveTransport:Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->$VALUES:[Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 37
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->_lookup:Landroid/util/SparseArray;

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->values()[Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    move-result-object v2

    array-length v3, v2

    .local v0, "cap":Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 41
    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 40
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->_value:I

    .line 47
    return-void
.end method

.method public static fromInt(I)Ljava/util/EnumSet;
    .locals 6
    .param p0, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    const-class v2, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 64
    .local v1, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;>;"
    invoke-static {}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->values()[Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 65
    .local v0, "cap":Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->getValue()I

    move-result v5

    and-int/2addr v5, p0

    if-eqz v5, :cond_0

    .line 66
    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    .end local v0    # "cap":Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;
    :cond_1
    return-object v1
.end method

.method public static getValue(Ljava/util/EnumSet;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "capabilities":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;>;"
    const/4 v1, 0x0

    .line 81
    .local v1, "value":I
    if-eqz p0, :cond_0

    .line 82
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    .line 83
    .local v0, "cap":Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->getValue()I

    move-result v3

    or-int/2addr v1, v3

    .line 84
    goto :goto_0

    .line 87
    .end local v0    # "cap":Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;
    :cond_0
    return v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->$VALUES:[Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/microsoft/xbox/smartglass/MediaTransportCapabilities;->_value:I

    return v0
.end method
