.class abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;
.source "$AutoValue_MultiplayerDataTypes_CloudComputePackage.java"


# instance fields
.field private final crossSandbox:Ljava/lang/Boolean;

.field private final gsiSet:Ljava/lang/String;

.field private final titleId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "crossSandbox"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "gsiSet"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "titleId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->crossSandbox:Ljava/lang/Boolean;

    .line 19
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->gsiSet:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->titleId:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public crossSandbox()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->crossSandbox:Ljava/lang/Boolean;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v1

    .line 55
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;

    if-eqz v3, :cond_6

    move-object v0, p1

    .line 56
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;

    .line 57
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->crossSandbox:Ljava/lang/Boolean;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;->crossSandbox()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->gsiSet:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 58
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;->gsiSet()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->titleId:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 59
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;->titleId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 57
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->crossSandbox:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;->crossSandbox()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 58
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->gsiSet:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;->gsiSet()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 59
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->titleId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;->titleId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;
    :cond_6
    move v1, v2

    .line 61
    goto :goto_0
.end method

.method public gsiSet()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->gsiSet:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 66
    const/4 v0, 0x1

    .line 67
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->crossSandbox:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 69
    mul-int/2addr v0, v3

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->gsiSet:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 71
    mul-int/2addr v0, v3

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->titleId:Ljava/lang/String;

    if-nez v1, :cond_2

    :goto_2
    xor-int/2addr v0, v2

    .line 73
    return v0

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->crossSandbox:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    .line 70
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->gsiSet:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 72
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->titleId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public titleId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->titleId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloudComputePackage{crossSandbox="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->crossSandbox:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gsiSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->gsiSet:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_CloudComputePackage;->titleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
