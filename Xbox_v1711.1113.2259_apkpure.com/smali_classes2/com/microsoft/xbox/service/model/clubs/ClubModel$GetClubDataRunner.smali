.class Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ClubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetClubDataRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

.field private final requestFilters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)V
    .locals 1

    .prologue
    .line 1129
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)V

    .line 1130
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1132
    .local p2, "requestFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1124
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubHubService()Lcom/microsoft/xbox/service/clubs/IClubHubService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    .line 1133
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->requestFilters:Ljava/util/List;

    .line 1134
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1143
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->requestFilters:Ljava/util/List;

    if-nez v1, :cond_0

    .line 1144
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/clubs/IClubHubService;->getClubs(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    move-result-object v0

    .line 1149
    .local v0, "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;->clubs()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1150
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;->clubs()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 1153
    :goto_1
    return-object v1

    .line 1146
    .end local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->clubHubService:Lcom/microsoft/xbox/service/clubs/IClubHubService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$400(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->requestFilters:Ljava/util/List;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/IClubHubService;->getClubs(Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;

    move-result-object v0

    .restart local v0    # "response":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;
    goto :goto_0

    .line 1153
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1123
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->buildData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1163
    const-wide/16 v0, 0x2580

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1158
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$GetClubDataRunner;->requestFilters:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/util/List;)V

    .line 1159
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1138
    return-void
.end method
