.class public Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;
.super Ljava/lang/Object;
.source "LfgRosterItem.java"


# instance fields
.field private final header:Ljava/lang/String;

.field private final member:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

.field public final timePlayed:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "member"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "timePlayed"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->member:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 20
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->timePlayed:Ljava/lang/Integer;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->header:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "headerText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->member:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 28
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->timePlayed:Ljava/lang/Integer;

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->header:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public getHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->header:Ljava/lang/String;

    return-object v0
.end method

.method public getMemberSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->member:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public isHeaderRow()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgRosterItem;->member:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
