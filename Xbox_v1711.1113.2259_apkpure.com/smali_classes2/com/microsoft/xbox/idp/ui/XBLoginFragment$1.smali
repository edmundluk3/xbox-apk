.class Lcom/microsoft/xbox/idp/ui/XBLoginFragment$1;
.super Ljava/lang/Object;
.source "XBLoginFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/idp/ui/XBLoginFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/idp/ui/XBLoginFragment;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/idp/ui/XBLoginFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/idp/ui/XBLoginFragment;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/XBLoginFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    invoke-static {}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Creating loader_xb_login"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    new-instance v0, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader;

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/XBLoginFragment;

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "ARG_USER_PTR"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v4, "ARG_RPS_TICKET"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;

    .line 134
    invoke-static {v5}, Lcom/microsoft/xbox/idp/util/CacheUtil;->getResultCache(Ljava/lang/Class;)Lcom/microsoft/xbox/idp/util/ResultCache;

    move-result-object v5

    const-string v6, "KEY_RESULT_KEY"

    invoke-virtual {p2, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader;-><init>(Landroid/content/Context;JLjava/lang/String;Lcom/microsoft/xbox/idp/util/ResultCache;Ljava/lang/Object;)V

    .line 133
    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;)V
    .locals 6
    .param p2, "result"    # Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;",
            ">;",
            "Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;>;"
    invoke-static {}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "loader_xb_login finished"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-virtual {p2}, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;->hasData()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    invoke-virtual {p2}, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Data;

    .line 142
    .local v0, "data":Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Data;
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/XBLoginFragment;

    invoke-static {v2}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment;->access$100(Lcom/microsoft/xbox/idp/ui/XBLoginFragment;)Lcom/microsoft/xbox/idp/ui/XBLoginFragment$Callbacks;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$Status;->SUCCESS:Lcom/microsoft/xbox/idp/ui/XBLoginFragment$Status;

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Data;->getAuthFlowResult()Lcom/microsoft/xbox/idp/util/AuthFlowResult;

    move-result-object v4

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Data;->isCreateAccount()Z

    move-result v5

    invoke-interface {v2, v3, v4, v5}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$Callbacks;->onComplete(Lcom/microsoft/xbox/idp/ui/XBLoginFragment$Status;Lcom/microsoft/xbox/idp/util/AuthFlowResult;Z)V

    .line 158
    .end local v0    # "data":Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Data;
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-virtual {p2}, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;->getError()Lcom/microsoft/xbox/idp/toolkit/HttpError;

    move-result-object v1

    .line 145
    .local v1, "error":Lcom/microsoft/xbox/idp/toolkit/HttpError;
    invoke-static {}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loader_x_token: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/toolkit/HttpError;->getErrorCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 154
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/XBLoginFragment;

    invoke-static {v2}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment;->access$200(Lcom/microsoft/xbox/idp/ui/XBLoginFragment;)Lcom/microsoft/xbox/idp/ui/XBLoginFragment$State;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    sget-object v3, Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;->CATCHALL:Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->startErrorActivity(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;)V

    goto :goto_0

    .line 148
    :sswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/XBLoginFragment;

    invoke-static {v2}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment;->access$200(Lcom/microsoft/xbox/idp/ui/XBLoginFragment;)Lcom/microsoft/xbox/idp/ui/XBLoginFragment$State;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    sget-object v3, Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;->BAN:Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->startErrorActivity(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;)V

    goto :goto_0

    .line 151
    :sswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$1;->this$0:Lcom/microsoft/xbox/idp/ui/XBLoginFragment;

    invoke-static {v2}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment;->access$200(Lcom/microsoft/xbox/idp/ui/XBLoginFragment;)Lcom/microsoft/xbox/idp/ui/XBLoginFragment$State;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    sget-object v3, Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;->OFFLINE:Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->startErrorActivity(Lcom/microsoft/xbox/idp/ui/ErrorActivity$ErrorScreen;)V

    goto :goto_0

    .line 146
    :sswitch_data_0
    .sparse-switch
        -0x7fea23fd -> :sswitch_0
        -0x7822ffe2 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 129
    check-cast p2, Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment$1;->onLoadFinished(Landroid/support/v4/content/Loader;Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Lcom/microsoft/xbox/idp/toolkit/XBLoginLoader$Result;>;"
    invoke-static {}, Lcom/microsoft/xbox/idp/ui/XBLoginFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "loader_xb_login reset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    return-void
.end method
