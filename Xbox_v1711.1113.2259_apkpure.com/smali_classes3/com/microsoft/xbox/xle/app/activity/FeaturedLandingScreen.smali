.class public Lcom/microsoft/xbox/xle/app/activity/FeaturedLandingScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "FeaturedLandingScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string v0, "Store - Featured"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 24
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedLandingScreen;->onCreateContentView()V

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/FeaturedLandingScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedLandingScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 26
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f030100

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedLandingScreen;->setContentView(I)V

    .line 31
    return-void
.end method
