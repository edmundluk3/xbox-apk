.class public Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "StoreGamesScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen$StoreGamesAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen$StoreGamesAdapterProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen$StoreGamesAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen$StoreGamesAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen$StoreGamesAdapterProvider;

    .line 23
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "Store - Games"

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 27
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 29
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen;->onCreateContentView()V

    .line 31
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen$StoreGamesAdapterProvider;

    sget-object v2, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Games:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;Lcom/microsoft/xbox/service/model/StoreBrowseType;)V

    .line 32
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->setAsPivotPane()V

    .line 33
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;->onCreate()V

    .line 35
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 36
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 40
    const v0, 0x7f030214

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/StoreGamesScreen;->setContentView(I)V

    .line 41
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method
