.class public Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "MyClubsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final clubClickedAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "clubClickedAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->clubClickedAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->clubClickedAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 51
    const v0, 0x7f0301b3

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V
    .locals 1
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "holder":Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;, "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 44
    .local v0, "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;->onBind(Ljava/lang/Object;)V

    .line 47
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 34
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, p2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 35
    .local v1, "v":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0904f9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 36
    .local v0, "padding":I
    invoke-virtual {v1, v0, v4, v0, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 37
    new-instance v2, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;-><init>(Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;Landroid/view/View;)V

    return-object v2
.end method
