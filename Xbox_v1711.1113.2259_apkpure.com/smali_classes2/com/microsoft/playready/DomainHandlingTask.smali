.class public Lcom/microsoft/playready/DomainHandlingTask;
.super Ljava/util/concurrent/FutureTask;
.source "DomainHandlingTask.java"

# interfaces
.implements Lcom/microsoft/playready/IDomainHandlingTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/microsoft/playready/IDomainHandlingTask;"
    }
.end annotation


# instance fields
.field private mDomainHandlingListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/IDomainHandlerListener;",
            ">;"
        }
    .end annotation
.end field

.field private mListenersNotified:Z

.field mOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/DomainHandlingWorker;)V
    .locals 2
    .param p1, "worker"    # Lcom/microsoft/playready/DomainHandlingWorker;

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 10
    iput-object v1, p0, Lcom/microsoft/playready/DomainHandlingTask;->mDomainHandlingListeners:Ljava/util/ArrayList;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/playready/DomainHandlingTask;->mListenersNotified:Z

    .line 12
    iput-object v1, p0, Lcom/microsoft/playready/DomainHandlingTask;->mOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    .line 17
    invoke-virtual {p1}, Lcom/microsoft/playready/DomainHandlingWorker;->getDomainOperationType()Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/DomainHandlingTask;->mOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/DomainHandlingTask;->mDomainHandlingListeners:Ljava/util/ArrayList;

    .line 19
    return-void
.end method


# virtual methods
.method public addDomainHandlerListener(Lcom/microsoft/playready/IDomainHandlerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/IDomainHandlerListener;

    .prologue
    .line 44
    monitor-enter p0

    .line 46
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/playready/DomainHandlingTask;->mListenersNotified:Z

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/microsoft/playready/DomainHandlingTask;->mDomainHandlingListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    :goto_0
    monitor-exit p0

    .line 55
    return-void

    .line 52
    :cond_0
    invoke-interface {p1, p0}, Lcom/microsoft/playready/IDomainHandlerListener;->onDomainOperationCompleted(Lcom/microsoft/playready/IDomainHandlingTask;)V

    goto :goto_0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected done()V
    .locals 3

    .prologue
    .line 24
    monitor-enter p0

    .line 26
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/DomainHandlingTask;->mDomainHandlingListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 31
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/playready/DomainHandlingTask;->mListenersNotified:Z

    .line 24
    monitor-exit p0

    .line 33
    return-void

    .line 26
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/IDomainHandlerListener;

    .line 28
    .local v0, "listener":Lcom/microsoft/playready/IDomainHandlerListener;
    invoke-interface {v0, p0}, Lcom/microsoft/playready/IDomainHandlerListener;->onDomainOperationCompleted(Lcom/microsoft/playready/IDomainHandlingTask;)V

    goto :goto_0

    .line 24
    .end local v0    # "listener":Lcom/microsoft/playready/IDomainHandlerListener;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getDomainOperationType()Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/playready/DomainHandlingTask;->mOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    return-object v0
.end method

.method public getResponseCustomData()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    invoke-super {p0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public removeDomainHandlerListener(Lcom/microsoft/playready/IDomainHandlerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/IDomainHandlerListener;

    .prologue
    .line 60
    monitor-enter p0

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/DomainHandlingTask;->mDomainHandlingListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 60
    monitor-exit p0

    .line 64
    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
