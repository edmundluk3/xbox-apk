.class Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProgressChallengesScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadLimitedTimeChallengesAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$1;

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 210
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshGameProgressLimitedTimeChallenges()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshTitleProgress(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadTitleProgress(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgressLimitedTimeChallenge(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 216
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->onLoadLimitedTimeChallengesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 218
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->onLoadLimitedTimeChallengesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 230
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 207
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 222
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;Z)Z

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->updateAdapter()V

    .line 225
    return-void
.end method
