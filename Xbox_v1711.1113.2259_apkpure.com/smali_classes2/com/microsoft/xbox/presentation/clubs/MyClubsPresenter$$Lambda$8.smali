.class final synthetic Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$8;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;

.field private final arg$2:Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$8;->arg$1:Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$8;->arg$2:Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$8;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$8;-><init>(Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$8;->arg$1:Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter$$Lambda$8;->arg$2:Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;

    check-cast p1, Lio/reactivex/Observable;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;->lambda$null$3(Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
