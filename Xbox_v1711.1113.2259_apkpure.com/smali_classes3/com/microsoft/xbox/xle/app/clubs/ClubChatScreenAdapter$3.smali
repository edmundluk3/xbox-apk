.class Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$3;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "ClubChatScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 482
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 485
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;->onScrolled(Landroid/support/v7/widget/RecyclerView;II)V

    .line 487
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/LinearLayoutManager;

    .line 489
    .local v0, "linearLayoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    if-gez p3, :cond_0

    .line 490
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .line 491
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->shouldLoadMoreHistory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 492
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->loadMoreHistory()V

    .line 494
    :cond_0
    return-void
.end method
