.class public final Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;
.super Ljava/lang/Object;
.source "ServiceModule_ProvideDefaultOkHttpClientFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lokhttp3/OkHttpClient;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final localeHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final loggingInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/data/service/ServiceModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/ServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/ServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/ServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "loggingInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/logging/HttpLoggingInterceptor;>;"
    .local p3, "localeHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->module:Lcom/microsoft/xbox/data/service/ServiceModule;

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->loggingInterceptorProvider:Ljavax/inject/Provider;

    .line 31
    sget-boolean v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 33
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/ServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/ServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/ServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "loggingInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/logging/HttpLoggingInterceptor;>;"
    .local p2, "localeHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;>;"
    new-instance v0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;-><init>(Lcom/microsoft/xbox/data/service/ServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideDefaultOkHttpClient(Lcom/microsoft/xbox/data/service/ServiceModule;Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;)Lokhttp3/OkHttpClient;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/ServiceModule;
    .param p1, "loggingInterceptor"    # Lokhttp3/logging/HttpLoggingInterceptor;
    .param p2, "localeHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;

    .prologue
    .line 59
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/data/service/ServiceModule;->provideDefaultOkHttpClient(Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;)Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->get()Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method

.method public get()Lokhttp3/OkHttpClient;
    .locals 3

    .prologue
    .line 37
    iget-object v2, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->module:Lcom/microsoft/xbox/data/service/ServiceModule;

    iget-object v0, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->loggingInterceptorProvider:Ljavax/inject/Provider;

    .line 39
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/logging/HttpLoggingInterceptor;

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/ServiceModule_ProvideDefaultOkHttpClientFactory;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;

    .line 38
    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/data/service/ServiceModule;->provideDefaultOkHttpClient(Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;)Lokhttp3/OkHttpClient;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 37
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient;

    return-object v0
.end method
