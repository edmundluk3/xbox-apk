.class public abstract Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;
.super Ljava/lang/Object;
.source "ClubCardModel.java"


# static fields
.field private static final DEFAULT_BACKGROUND_COLOR:I

.field private static final DEFAULT_TAG_COLOR:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->DEFAULT_BACKGROUND_COLOR:I

    .line 25
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->DEFAULT_TAG_COLOR:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Ljava/util/List;)Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;
    .locals 18
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;)",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "socialTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v17

    .line 67
    .local v17, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;

    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v1

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v3

    .line 70
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Title:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    if-ne v4, v5, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->localizedTitleFamilyName()Ljava/lang/String;

    move-result-object v4

    :goto_0
    if-eqz v17, :cond_1

    .line 71
    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v5

    :goto_1
    if-eqz v17, :cond_2

    .line 72
    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getTertiaryColor()I

    move-result v6

    .line 73
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->bannerImageUrl()Ljava/lang/String;

    move-result-object v8

    .line 75
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Title:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    if-ne v9, v10, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 76
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 77
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->description()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceTodayCount()J

    move-result-wide v12

    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v14

    .line 80
    invoke-static/range {p1 .. p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v16

    invoke-direct/range {v0 .. v16}, Lcom/microsoft/xbox/service/model/clubs/AutoValue_ClubCardModel;-><init>(JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLcom/google/common/collect/ImmutableList;)V

    .line 67
    return-object v0

    .line 70
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->getLocalizedString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 71
    :cond_1
    sget v5, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->DEFAULT_BACKGROUND_COLOR:I

    goto :goto_1

    .line 72
    :cond_2
    sget v6, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->DEFAULT_TAG_COLOR:I

    goto :goto_2

    .line 75
    :cond_3
    const/4 v9, 0x0

    goto :goto_3
.end method


# virtual methods
.method public abstract backgroundColor()I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end method

.method public abstract bannerImageUrl()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract clubId()J
.end method

.method public abstract clubTypeName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract description()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract displayImageUrl()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract glyphImageUrl()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract memberCount()J
.end method

.method public abstract name()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract presenceCount()J
.end method

.method public abstract socialTags()Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation
.end method

.method public abstract tagColor()I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end method

.method public abstract type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
