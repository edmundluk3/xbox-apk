.class public Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "RelatedScreen.java"


# instance fields
.field private final vm:Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 16
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;->onCreateContentView()V

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;->vm:Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;

    .line 18
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "Related"

    return-object v0
.end method

.method public getScreenPercent()I
    .locals 19

    .prologue
    .line 45
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v16

    const v17, 0x7f0e0786

    invoke-virtual/range {v16 .. v17}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 46
    .local v2, "contentFrame":Landroid/view/View;
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 47
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 48
    .local v8, "r":Landroid/content/res/Resources;
    const v16, 0x7f090023

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    .line 49
    .local v13, "titleSz":F
    const v16, 0x7f090253

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    .line 50
    .local v12, "titleMgrBottom":F
    const v16, 0x7f090154

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    .line 51
    .local v10, "remoteSz":F
    const v16, 0x7f090253

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    .line 52
    .local v9, "remoteMgrBottom":F
    const v16, 0x7f0900ca

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    .line 54
    .local v11, "switchPanelMarginTop":F
    const/high16 v16, 0x40000000    # 2.0f

    const v17, 0x7f090262

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    mul-float v3, v16, v17

    .line 55
    .local v3, "detailsMarginBottom2":F
    add-float v16, v13, v12

    add-float v16, v16, v9

    add-float v16, v16, v11

    add-float v16, v16, v10

    add-float v14, v16, v3

    .line 56
    .local v14, "toSubtract":F
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v16, v16, v14

    const/high16 v17, 0x40000000    # 2.0f

    div-float v5, v16, v17

    .line 57
    .local v5, "height":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;->vm:Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;->getAspectRatio()F

    move-result v1

    .line 58
    .local v1, "ar":F
    div-float v15, v5, v1

    .line 59
    .local v15, "width":F
    const v16, 0x7f0b0007

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 60
    .local v6, "numOfColumns":I
    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;->vm:Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;->getExtraDp(F)F

    move-result v17

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    .line 61
    .local v4, "extra":F
    int-to-float v0, v6

    move/from16 v16, v0

    add-float v17, v15, v4

    mul-float v7, v16, v17

    .line 62
    .local v7, "paneWidth":F
    const/high16 v16, 0x42c80000    # 100.0f

    mul-float v16, v16, v7

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v16, v16, v17

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->round(F)I

    move-result v16

    return v16
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;->vm:Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 25
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 29
    const v0, 0x7f0300dd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/RelatedScreen;->setContentView(I)V

    .line 30
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method
