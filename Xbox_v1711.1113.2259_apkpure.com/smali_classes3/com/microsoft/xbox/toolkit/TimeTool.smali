.class public Lcom/microsoft/xbox/toolkit/TimeTool;
.super Ljava/lang/Object;
.source "TimeTool.java"


# static fields
.field private static final NSTOSEC:D = 1.0E-9

.field private static instance:Lcom/microsoft/xbox/toolkit/TimeTool;


# instance fields
.field private allSamples:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/TimeSample;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Lcom/microsoft/xbox/toolkit/TimeTool;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/TimeTool;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/TimeTool;->instance:Lcom/microsoft/xbox/toolkit/TimeTool;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/TimeTool;->allSamples:Ljava/util/ArrayList;

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/toolkit/TimeTool;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/toolkit/TimeTool;->instance:Lcom/microsoft/xbox/toolkit/TimeTool;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/TimeTool;->allSamples:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 17
    return-void
.end method

.method public getAverageLPS()J
    .locals 6

    .prologue
    .line 36
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/TimeTool;->getAverageTime()J

    move-result-wide v2

    long-to-double v2, v2

    const-wide v4, 0x3e112e0be826d695L    # 1.0E-9

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    double-to-long v0, v0

    return-wide v0
.end method

.method public getAverageTime()J
    .locals 10

    .prologue
    .line 26
    const-wide/16 v6, 0x0

    .line 27
    .local v6, "sum":J
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/TimeTool;->allSamples:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    int-to-long v0, v5

    .line 28
    .local v0, "count":J
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/TimeTool;->allSamples:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/TimeSample;

    .line 29
    .local v4, "sample":Lcom/microsoft/xbox/toolkit/TimeSample;
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/TimeSample;->getElapsed()J

    move-result-wide v2

    .line 30
    .local v2, "elapsed":J
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/TimeSample;->getElapsed()J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 31
    goto :goto_0

    .line 32
    .end local v2    # "elapsed":J
    .end local v4    # "sample":Lcom/microsoft/xbox/toolkit/TimeSample;
    :cond_0
    div-long v8, v6, v0

    return-wide v8
.end method

.method public getMaximumTime()J
    .locals 7

    .prologue
    .line 44
    const-wide/16 v0, 0x0

    .line 45
    .local v0, "current":J
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/TimeTool;->allSamples:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/TimeSample;

    .line 46
    .local v4, "sample":Lcom/microsoft/xbox/toolkit/TimeSample;
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/TimeSample;->getElapsed()J

    move-result-wide v2

    .line 47
    .local v2, "elapsed":J
    cmp-long v6, v2, v0

    if-lez v6, :cond_0

    .line 48
    move-wide v0, v2

    goto :goto_0

    .line 51
    .end local v2    # "elapsed":J
    .end local v4    # "sample":Lcom/microsoft/xbox/toolkit/TimeSample;
    :cond_1
    return-wide v0
.end method

.method public getMinimumLPS()J
    .locals 6

    .prologue
    .line 40
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/TimeTool;->getMaximumTime()J

    move-result-wide v2

    long-to-double v2, v2

    const-wide v4, 0x3e112e0be826d695L    # 1.0E-9

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    double-to-long v0, v0

    return-wide v0
.end method

.method public getSampleCount()J
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/TimeTool;->allSamples:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public start()Lcom/microsoft/xbox/toolkit/TimeSample;
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcom/microsoft/xbox/toolkit/TimeSample;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/TimeSample;-><init>()V

    .line 21
    .local v0, "t":Lcom/microsoft/xbox/toolkit/TimeSample;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/TimeTool;->allSamples:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/TimeTool;->allSamples:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/TimeTool;->allSamples:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/TimeSample;

    return-object v1
.end method
