.class public abstract Lcom/microsoft/xbox/smartglass/AuxiliaryStreamListener;
.super Ljava/lang/Object;
.source "AuxiliaryStreamListener.java"

# interfaces
.implements Ljava/util/EventListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnect()V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method public onError(Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 0
    .param p1, "error"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 44
    return-void
.end method

.method public onReceive(Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;)V
    .locals 0
    .param p1, "readStats"    # Lcom/microsoft/xbox/smartglass/AuxiliaryStreamReadStats;

    .prologue
    .line 27
    return-void
.end method

.method public onSend(Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;)V
    .locals 0
    .param p1, "writeStats"    # Lcom/microsoft/xbox/smartglass/AuxiliaryStreamWriteStats;

    .prologue
    .line 36
    return-void
.end method
