.class public Lcom/microsoft/xbox/smartglass/EnvironmentSettings;
.super Ljava/lang/Object;
.source "EnvironmentSettings.java"


# instance fields
.field private final _pEnvironmentSettings:Lcom/microsoft/xbox/smartglass/RefTPtr;

.field public final currentEnvironment:Lcom/microsoft/xbox/smartglass/Environment;

.field public final xliveAudienceUri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "SmartGlassSDK"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->xliveAudienceUri:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->currentEnvironment:Lcom/microsoft/xbox/smartglass/Environment;

    .line 23
    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->_pEnvironmentSettings:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 24
    return-void
.end method

.method constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .param p1, "xliveAudienceUri"    # Ljava/lang/String;
    .param p2, "currentEnvironment"    # I
    .param p3, "pEnvironmentSettings"    # J

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->xliveAudienceUri:Ljava/lang/String;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {v0, p3, p4}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->_pEnvironmentSettings:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 30
    invoke-static {p2}, Lcom/microsoft/xbox/smartglass/Environment;->fromInt(I)Lcom/microsoft/xbox/smartglass/Environment;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->currentEnvironment:Lcom/microsoft/xbox/smartglass/Environment;

    .line 31
    return-void
.end method

.method private native getOAuthLoginUriNative(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native getOAuthLogoutUriNative(JLjava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native getXliveServiceUriNative(JLjava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method


# virtual methods
.method public getOAuthLoginUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "clientId"    # Ljava/lang/String;
    .param p2, "policy"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->_pEnvironmentSettings:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->getOAuthLoginUriNative(JLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOAuthLogoutUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "clientId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->_pEnvironmentSettings:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->getOAuthLogoutUriNative(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getXliveServiceUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "subDomain"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->_pEnvironmentSettings:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/EnvironmentSettings;->getXliveServiceUriNative(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
