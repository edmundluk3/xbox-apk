.class public Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "TrendingXblScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-string v0, "Trending"

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 39
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070722

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 23
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreen;->onCreateContentView()V

    .line 24
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 25
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 29
    const v0, 0x7f03023c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreen;->setContentView(I)V

    .line 30
    return-void
.end method
