.class public Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubHomeScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;
    }
.end annotation


# static fields
.field private static final EMPTY_PAIR:Landroid/support/v4/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private acceptInvitationCallback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

.field private club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private clubAccountDetails:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private clubUpdateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation
.end field

.field private currentMembershipAction:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

.field private getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;

.field private isLoadingClubAccountDetails:Z

.field ownerSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

.field private ownerXuidRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field playerSummaries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation
.end field

.field private playerXuidsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final titleIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private titleImageLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private final titleImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private viewerRoles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 94
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->TAG:Ljava/lang/String;

    .line 95
    new-instance v0, Landroid/support/v4/util/Pair;

    const-string v1, ""

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->EMPTY_PAIR:Landroid/support/v4/util/Pair;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 5
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 118
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubUpdateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 119
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->ownerXuidRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 120
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->playerXuidsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 155
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->acceptInvitationCallback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    .line 208
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 210
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubHomeScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 211
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    .line 212
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleImageLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleImages:Ljava/util/List;

    .line 215
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->currentMembershipAction:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    .line 217
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V

    .line 219
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubUpdateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v2

    .line 221
    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 222
    invoke-virtual {v1, v2, v3, v4}, Lio/reactivex/Observable;->debounce(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v1

    .line 223
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 224
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 219
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->ownerXuidRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 245
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    .line 246
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 247
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 248
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 249
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 243
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->playerXuidsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 263
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    .line 264
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 265
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 268
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 269
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 261
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 280
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/Runnable;
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # Ljava/lang/Runnable;

    .prologue
    .line 92
    invoke-virtual/range {p0 .. p6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/Runnable;
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # Ljava/lang/Runnable;

    .prologue
    .line 92
    invoke-virtual/range {p0 .. p6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->showError(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->onClubTitleImagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isLoadingClubAccountDetails:Z

    return p1
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->onClubAccountDetailsLoaded(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V

    return-void
.end method

.method private cancelActiveTasks()V
    .locals 0

    .prologue
    .line 759
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->cancelGetClubAccountDetailsTask()V

    .line 760
    return-void
.end method

.method private cancelGetClubAccountDetailsTask()V
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;

    if-eqz v0, :cond_0

    .line 764
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;->cancel()V

    .line 765
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;

    .line 767
    :cond_0
    return-void
.end method

.method private isClubSettingsAvailable()Z
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isViewerFollower()Z
    .locals 2

    .prologue
    .line 737
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Follower:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$doMembershipAction$10(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$doMembershipAction$11(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$doMembershipAction$12(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 632
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$keepClub$8(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 556
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    :cond_0
    const v0, 0x7f0702b9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->showError(I)V

    .line 559
    :cond_1
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z
    .locals 1
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 221
    invoke-static {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateClubData()V

    .line 226
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateTitleData()V

    .line 227
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateUserData()V

    .line 229
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSuspended()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->cancelGetClubAccountDetailsTask()V

    .line 232
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;

    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetClubAccountDetailsTask;->load(Z)V

    .line 239
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    .line 240
    return-void

    .line 236
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/lang/Long;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "xuid"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;->load(Ljava/util/Collection;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "owner"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 252
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->ownerSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 253
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    .line 254
    return-void
.end method

.method static synthetic lambda$new$4(Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "ignore"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 257
    return-void
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/util/Collection;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "xuidList"    # Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->userSummaryRepository:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;->load(Ljava/util/Collection;)Lio/reactivex/Observable;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Lio/reactivex/Observable;->toList()Lio/reactivex/Single;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    .line 265
    return-object v0
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/util/List;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p1, "playerSummaries"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 272
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->playerSummaries:Ljava/util/List;

    .line 273
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    .line 274
    return-void
.end method

.method static synthetic lambda$new$7(Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "ignore"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 277
    return-void
.end method

.method static synthetic lambda$null$14(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 682
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$playGame$9(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 567
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;->android()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->launchGame(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;)V

    return-void
.end method

.method static synthetic lambda$showInvitationPendingDialog$13(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 675
    const-string v0, "Clubs - Home Accept Invitation"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 676
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->acceptInvitationCallback:Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->acceptInvitation(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 677
    return-void
.end method

.method static synthetic lambda$showInvitationPendingDialog$15(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 680
    const-string v0, "Clubs - Home Decline Invitation"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 682
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$18;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->rejectInvitation(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 683
    return-void
.end method

.method static synthetic lambda$toggleFollowClub$16(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 744
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$toggleFollowClub$17(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 748
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    return-void
.end method

.method private launchGame(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;)V
    .locals 4
    .param p1, "deeplink"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;

    .prologue
    .line 577
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;->Uri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 578
    .local v1, "playIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->newInstance()Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v0

    .line 579
    .local v0, "deepLinkRequest":Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->addIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    .line 580
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->defaultExecute()Landroid/content/Intent;

    .line 582
    return-void
.end method

.method private onClubAccountDetailsLoaded(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V
    .locals 1
    .param p1, "response"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 916
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isLoadingClubAccountDetails:Z

    .line 917
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubAccountDetails:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .line 918
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 919
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    .line 920
    return-void
.end method

.method private onClubTitleImagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 883
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;>;"
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 913
    :goto_0
    return-void

    .line 887
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 888
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 889
    .local v2, "titleId":Ljava/lang/Long;
    const/4 v0, 0x0

    .line 890
    .local v0, "found":Z
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 891
    .local v1, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-wide v8, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 892
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleImages:Ljava/util/List;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->getBoxArt()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/support/v4/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/util/Pair;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 893
    const/4 v0, 0x1

    .line 898
    .end local v1    # "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :cond_2
    if-nez v0, :cond_0

    .line 899
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Title data not loaded for title id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleImages:Ljava/util/List;

    const/4 v5, 0x0

    const-string v6, ""

    invoke-static {v5, v6}, Landroid/support/v4/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/util/Pair;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 905
    .end local v0    # "found":Z
    .end local v2    # "titleId":Ljava/lang/Long;
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 910
    :pswitch_1
    sget-object v3, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Failed to get the title images."

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 883
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private showInvitationPendingDialog()V
    .locals 7

    .prologue
    .line 670
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07016e

    .line 671
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07016d

    .line 672
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070159

    .line 673
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07070e

    .line 678
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v6

    move-object v0, p0

    .line 670
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 684
    return-void
.end method

.method private updateClubData()V
    .locals 1

    .prologue
    .line 820
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 823
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSettingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 824
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 825
    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    .line 828
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSettingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 829
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->access$300(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->currentMembershipAction:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    .line 831
    return-void

    .line 825
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 829
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    goto :goto_1
.end method

.method private updateTitleData()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 834
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 835
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->associatedTitles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 837
    .local v0, "associatedTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 838
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 839
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleImages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 841
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 844
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleImages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 845
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleImageLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    new-instance v6, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-direct {v6, p0, v7, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/util/Collection;Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->StartLoadFromUI(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;

    .line 856
    .end local v0    # "associatedTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_1
    :goto_0
    return-void

    .line 853
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 854
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleImages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0
.end method

.method private updateUserData()V
    .locals 6

    .prologue
    .line 859
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 861
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 862
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->ownerXuidRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 866
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 867
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 868
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 869
    .local v1, "playerList":Ljava/util/SortedSet;, "Ljava/util/SortedSet<Ljava/lang/Long;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 870
    .local v0, "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->lastSeenState()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InGame:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    if-ne v3, v4, :cond_1

    .line 871
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 875
    .end local v0    # "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :cond_2
    invoke-interface {v1}, Ljava/util/SortedSet;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 876
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->playerXuidsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 880
    .end local v1    # "playerList":Ljava/util/SortedSet;, "Ljava/util/SortedSet<Ljava/lang/Long;>;"
    :cond_3
    return-void
.end method


# virtual methods
.method public customizeClub()V
    .locals 4

    .prologue
    .line 585
    const-string v0, "Clubs - Home Customize"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 586
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreen;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;-><init>(J)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 587
    return-void
.end method

.method public doMembershipAction()V
    .locals 7

    .prologue
    .line 603
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubHomeScreenViewModel$MembershipAction:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->currentMembershipAction:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 637
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Attempted to do unavailable membership action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->currentMembershipAction:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 640
    :goto_0
    return-void

    .line 605
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->showInvitationPendingDialog()V

    goto :goto_0

    .line 609
    :pswitch_1
    const-string v0, "Clubs - Home Request to Join"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 610
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCreateJoinParticipateClubs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v2

    const v3, 0x7f0706eb

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;I)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->requestToJoin(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    goto :goto_0

    .line 614
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 615
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 617
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070494

    .line 618
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 616
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 619
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 614
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 625
    :pswitch_2
    const-string v0, "Clubs - Home Cancel Request to Join"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 627
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->cancelRequestToJoin(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    goto :goto_0

    .line 632
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->removeSelfFromClub(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    goto/16 :goto_0

    .line 603
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getBackgroundImageUri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 356
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->backgroundImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 357
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->backgroundImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 356
    :goto_0
    return-object v0

    .line 357
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getClubTypeHeader()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const v4, 0x7f0701f1

    .line 289
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 290
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubGenre:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 316
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown club genre: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 321
    :cond_0
    :goto_0
    const-string v2, ""

    :goto_1
    return-object v2

    .line 293
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 301
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown club type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 295
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07026b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 297
    :pswitch_2
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070267

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 299
    :pswitch_3
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 306
    :pswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 307
    .local v1, "header":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->localizedTitleFamilyName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-ne v2, v3, :cond_1

    .line 309
    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 310
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    .line 311
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 291
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 293
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 450
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->description()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getDisplayImageUri()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 363
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 364
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 363
    :goto_0
    return-object v0

    .line 364
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFollowActionIconLabel()Landroid/support/v4/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 731
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isViewerFollower()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/v4/util/Pair;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070fc6

    .line 732
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 731
    :goto_0
    return-object v0

    .line 732
    :cond_0
    new-instance v0, Landroid/support/v4/util/Pair;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070fc5

    .line 733
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0700ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getFollowersCount()J
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->followersCount()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getGlyphUri()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 346
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 347
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 348
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 351
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getHereTodayCount()J
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceTodayCount()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getInviteButtonIconLabel()Landroid/support/v4/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSettingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 689
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/v4/util/Pair;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070ff8

    .line 690
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07029f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 693
    :goto_0
    return-object v0

    .line 690
    :cond_0
    new-instance v0, Landroid/support/v4/util/Pair;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070f4e

    .line 691
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0702a2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 693
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->EMPTY_PAIR:Landroid/support/v4/util/Pair;

    goto :goto_0
.end method

.method public getMembersCount()J
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getMembershipActionIconLabelPair()Landroid/support/v4/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 700
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$ClubHomeScreenViewModel$MembershipAction:[I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->currentMembershipAction:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 718
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->EMPTY_PAIR:Landroid/support/v4/util/Pair;

    .line 722
    .local v0, "membershipActionPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    return-object v0

    .line 702
    .end local v0    # "membershipActionPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :pswitch_0
    new-instance v0, Landroid/support/v4/util/Pair;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070f2b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0702e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 703
    .restart local v0    # "membershipActionPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_0

    .line 706
    .end local v0    # "membershipActionPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :pswitch_1
    new-instance v0, Landroid/support/v4/util/Pair;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070e9a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07021e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 707
    .restart local v0    # "membershipActionPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_0

    .line 710
    .end local v0    # "membershipActionPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :pswitch_2
    new-instance v0, Landroid/support/v4/util/Pair;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070ef7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07021c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 711
    .restart local v0    # "membershipActionPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_0

    .line 714
    .end local v0    # "membershipActionPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :pswitch_3
    new-instance v0, Landroid/support/v4/util/Pair;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070f96

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07021f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 715
    .restart local v0    # "membershipActionPair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_0

    .line 700
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getOwner()Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 465
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->ownerSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    return-object v0
.end method

.method public getPlayers()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 470
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->playerSummaries:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPlayingNowText()Ljava/lang/String;
    .locals 12
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 382
    const-string v4, ""

    .line 383
    .local v4, "playingNowText":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 385
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 386
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceInGameCount()J

    move-result-wide v2

    .line 388
    .local v2, "playingNow":J
    const-wide/16 v8, 0x0

    cmp-long v5, v2, v8

    if-lez v5, :cond_0

    .line 389
    sget-object v5, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubGenre:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->ordinal()I

    move-result v8

    aget v5, v5, v8

    packed-switch v5, :pswitch_data_0

    .line 412
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown club genre: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 418
    .end local v2    # "playingNow":J
    :cond_0
    :goto_0
    return-object v4

    .line 391
    .restart local v2    # "playingNow":J
    :pswitch_0
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070265

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v5, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 392
    goto :goto_0

    .line 395
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 396
    .local v1, "playingNowBuilder":Ljava/lang/StringBuilder;
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070265

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v5, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    const-string v5, " - "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->maxMembersInGame()J

    move-result-wide v8

    sub-long v6, v8, v2

    .line 400
    .local v6, "slotsAvailable":J
    const-wide/16 v8, 0x1

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    .line 401
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f0702d5

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v5, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 409
    goto :goto_0

    .line 402
    :cond_1
    const-wide/16 v8, 0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 403
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f07024b

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 405
    :cond_2
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f07023c

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getPrimaryColor()Ljava/lang/Integer;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 428
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v0

    .line 429
    .local v0, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_0
    return-object v1

    .end local v0    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :cond_1
    move-object v0, v1

    .line 428
    goto :goto_0
.end method

.method public getPrivacyIcon()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 326
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 327
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 328
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 336
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown club type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 341
    :cond_0
    const-string v1, ""

    :goto_0
    return-object v1

    .line 330
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070f64

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 332
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070f79

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 334
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070efe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 328
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getSecondaryColor()Ljava/lang/Integer;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 434
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v0

    .line 435
    .local v0, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getSecondaryColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_0
    return-object v1

    .end local v0    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :cond_1
    move-object v0, v1

    .line 434
    goto :goto_0
.end method

.method public getSuspendedText()Ljava/lang/String;
    .locals 12
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const-wide/16 v10, 0x1

    .line 535
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 536
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->suspendedUntilUtc()Ljava/util/Date;

    move-result-object v1

    .line 538
    .local v1, "suspendedUntilUtc":Ljava/util/Date;
    :goto_0
    if-eqz v1, :cond_3

    .line 539
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    sub-long v4, v6, v8

    .line 540
    .local v4, "diffFromNow":J
    sget-object v6, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v4, v5, v7}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    .line 542
    .local v2, "daysUntilDelete":J
    cmp-long v6, v2, v10

    if-gez v6, :cond_1

    .line 543
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0702d9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 550
    .end local v2    # "daysUntilDelete":J
    .end local v4    # "diffFromNow":J
    :goto_1
    return-object v6

    .line 536
    .end local v1    # "suspendedUntilUtc":Ljava/util/Date;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 544
    .restart local v1    # "suspendedUntilUtc":Ljava/util/Date;
    .restart local v2    # "daysUntilDelete":J
    .restart local v4    # "diffFromNow":J
    :cond_1
    cmp-long v6, v2, v10

    if-nez v6, :cond_2

    .line 545
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0702d7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 547
    :cond_2
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0702d8

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 550
    .end local v2    # "daysUntilDelete":J
    .end local v4    # "diffFromNow":J
    :cond_3
    const-string v6, ""

    goto :goto_1
.end method

.method public getTagColor()I
    .locals 1

    .prologue
    .line 445
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTertiaryColor()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getTertiaryColor()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 423
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getSocialTags()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTertiaryColor()Ljava/lang/Integer;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 440
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v0

    .line 441
    .local v0, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getTertiaryColor()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_0
    return-object v1

    .end local v0    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :cond_1
    move-object v0, v1

    .line 440
    goto :goto_0
.end method

.method public getTitleIds()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 455
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTitleImageUris()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 460
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleImages:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public goToOwner()V
    .locals 2

    .prologue
    .line 664
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->ownerSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->ownerSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 667
    :cond_0
    return-void
.end method

.method public inviteToClub()V
    .locals 4

    .prologue
    .line 590
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-eqz v0, :cond_0

    .line 591
    const-string v0, "Clubs - Home Invite"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 592
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreen;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;-><init>(J)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 594
    :cond_0
    return-void
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 784
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isLoadingClubAccountDetails:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isClubSuspended()Z
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCustomizeEnabled()Z
    .locals 2

    .prologue
    .line 492
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSuspended()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSettingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFollowEnabled()Z
    .locals 2

    .prologue
    .line 727
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSuspended()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isViewerBanned()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInviteEnabled()Z
    .locals 2

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSuspended()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isViewerBanned()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isKeepClubEnabled()Z
    .locals 2

    .prologue
    .line 521
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSuspended()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubAccountDetails:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubAccountDetails:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .line 523
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->hasOwnerSuspension()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 524
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 521
    :goto_0
    return v0

    .line 524
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMembershipActionEnabled()Z
    .locals 2

    .prologue
    .line 509
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSuspended()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->currentMembershipAction:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$MembershipAction;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotificationsEnabled()Z
    .locals 2

    .prologue
    .line 501
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSuspended()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSettingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlayGameEnabled()Z
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 529
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 530
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;->android()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 528
    :goto_0
    return v0

    .line 530
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReportEnabled()Z
    .locals 2

    .prologue
    .line 505
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSuspended()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSettingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTitleClub()Z
    .locals 3

    .prologue
    .line 283
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 284
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Title:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isViewerBanned()Z
    .locals 2

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isClubSettingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewerRoles:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keepClub()V
    .locals 2

    .prologue
    .line 555
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->ownerKeepClub(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 560
    return-void
.end method

.method public loadOverride(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 789
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 790
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Detail:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->ClubPresence:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 794
    return-void
.end method

.method public navigateToEnforcement()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 643
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 644
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    sget-object v1, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Club:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 646
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 647
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 648
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 651
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 653
    .end local v0    # "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    :cond_0
    return-void
.end method

.method public navigateToNotifications()V
    .locals 4

    .prologue
    .line 597
    const-string v0, "Clubs - Home Notifications"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 598
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const-string v1, "TODO: navigateToNotifications"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 599
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    .line 600
    return-void
.end method

.method public navigateToTitle(I)V
    .locals 8
    .param p1, "position"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 474
    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    int-to-long v4, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 477
    :try_start_0
    new-instance v7, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 478
    .local v7, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 480
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    invoke-virtual {v0, v1, v7}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PushScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    .end local v7    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return-void

    .line 482
    :catch_0
    move-exception v6

    .line 483
    .local v6, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to navigate to game profile for id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->titleIds:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 798
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 802
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 803
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    .line 804
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubUpdateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 810
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 811
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    .line 812
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Club model changed to invalid state."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 798
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 771
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubHomeScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 772
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 776
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 777
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 778
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 780
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 754
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStopOverride()V

    .line 755
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->cancelActiveTasks()V

    .line 756
    return-void
.end method

.method public playGame()V
    .locals 6

    .prologue
    .line 563
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isPlayGameEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07061a

    .line 565
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0707c7

    .line 566
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v3

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07060d

    .line 568
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 564
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 573
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    .line 574
    return-void
.end method

.method public reportClub()V
    .locals 4

    .prologue
    .line 656
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-eqz v0, :cond_0

    .line 657
    const-string v0, "Clubs - Home Report"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 658
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->navigateToEnforcement()V

    .line 659
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->updateAdapter()V

    .line 661
    :cond_0
    return-void
.end method

.method public toggleFollowClub()V
    .locals 4

    .prologue
    .line 741
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->isViewerFollower()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742
    const-string v0, "Clubs - Home Unfollow"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 744
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->unfollowClub(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 750
    :goto_0
    return-void

    .line 746
    :cond_0
    const-string v0, "Clubs - Home Follow"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackHomeActions(Ljava/lang/String;Ljava/lang/Long;)V

    .line 748
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->followClub(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    goto :goto_0
.end method
