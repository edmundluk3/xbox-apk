.class Lcom/microsoft/xbox/xle/epg/TvStreamer$14;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupMediaPlayer(Lcom/microsoft/playready/PRMediaPlayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

.field final synthetic val$player:Lcom/microsoft/playready/PRMediaPlayer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;Lcom/microsoft/playready/PRMediaPlayer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 640
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->val$player:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "p"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v2, 0x0

    .line 644
    const-string v0, "TvStreamer"

    const-string v1, "Stream prepared"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->finishPreparing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 669
    :cond_0
    :goto_0
    return-void

    .line 651
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$1200(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->FULLSCREEN:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v0, v1, :cond_2

    .line 652
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->setRequestedOrientation(I)V

    .line 654
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$300(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$300(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 656
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestLiveTvInfo()Z

    .line 657
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$1300(Lcom/microsoft/xbox/xle/epg/TvStreamer;Z)Z

    .line 658
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->val$player:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v0}, Lcom/microsoft/playready/PRMediaPlayer;->start()V

    .line 660
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$000(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getSeekPerformed()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 661
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Stream Seek"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->endTrackPerformance(Ljava/lang/String;)V

    .line 662
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$000(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->setSeekPerformed(Z)V

    .line 665
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->getIsRadioChannel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$14;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->access$100(Lcom/microsoft/xbox/xle/epg/TvStreamer;)Lcom/microsoft/xbox/xle/epg/TvStreamerModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setIsLoading(Z)V

    goto :goto_0
.end method
