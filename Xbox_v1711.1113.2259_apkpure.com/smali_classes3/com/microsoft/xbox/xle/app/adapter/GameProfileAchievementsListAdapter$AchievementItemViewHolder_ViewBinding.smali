.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "GameProfileAchievementsListAdapter$AchievementItemViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;

    .line 24
    const v0, 0x7f0e05eb

    const-string v1, "field \'imageView\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 25
    const v0, 0x7f0e0694

    const-string v1, "field \'unlockedDetailsTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->unlockedDetailsTextView:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f0e0323

    const-string v1, "field \'gamerscoreTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->gamerscoreTextView:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f0e068d

    const-string v1, "field \'titleView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->titleView:Landroid/widget/TextView;

    .line 28
    const v0, 0x7f0e0698

    const-string v1, "field \'timeRemainingTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findOptionalViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timeRemainingTextView:Landroid/widget/TextView;

    .line 29
    const v0, 0x7f0e0690

    const-string v1, "field \'gamerscoreIconView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f0e068e

    const-string v1, "field \'rewardIconView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rewardIconView:Landroid/widget/TextView;

    .line 31
    const v0, 0x7f0e068f

    const-string v1, "field \'rareIconView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rareIconView:Landroid/widget/TextView;

    .line 32
    const v0, 0x7f0e0697

    const-string v1, "field \'timerIconView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findOptionalViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timerIconView:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f0e0691

    const-string v1, "field \'descriptionTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findOptionalViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->descriptionTextView:Landroid/widget/TextView;

    .line 34
    const v0, 0x7f0e0692

    const-string v1, "field \'rarityTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rarityTextView:Landroid/widget/TextView;

    .line 35
    const v0, 0x7f0e0693

    const-string v1, "field \'lockedAchievementIconView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findOptionalViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->lockedAchievementIconView:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f0e0695

    const-string v1, "field \'progressTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findOptionalViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->progressTextView:Landroid/widget/TextView;

    .line 37
    const v0, 0x7f0e0696

    const-string v1, "field \'achieveComparisonBar\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->achieveComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 38
    const v0, 0x7f0e068c

    const-string v1, "field \'imageContainer\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->imageContainer:Landroid/view/View;

    .line 39
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;

    .line 45
    .local v0, "target":Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 46
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;

    .line 48
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 49
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->unlockedDetailsTextView:Landroid/widget/TextView;

    .line 50
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->gamerscoreTextView:Landroid/widget/TextView;

    .line 51
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->titleView:Landroid/widget/TextView;

    .line 52
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timeRemainingTextView:Landroid/widget/TextView;

    .line 53
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->gamerscoreIconView:Landroid/widget/TextView;

    .line 54
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rewardIconView:Landroid/widget/TextView;

    .line 55
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rareIconView:Landroid/widget/TextView;

    .line 56
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->timerIconView:Landroid/widget/TextView;

    .line 57
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->descriptionTextView:Landroid/widget/TextView;

    .line 58
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->rarityTextView:Landroid/widget/TextView;

    .line 59
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->lockedAchievementIconView:Landroid/widget/TextView;

    .line 60
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->progressTextView:Landroid/widget/TextView;

    .line 61
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->achieveComparisonBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 62
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;->imageContainer:Landroid/view/View;

    .line 63
    return-void
.end method
