.class public Lcom/microsoft/xbox/xle/ui/SearchBarView;
.super Landroid/widget/RelativeLayout;
.source "SearchBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;,
        Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;
    }
.end annotation


# instance fields
.field private clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private onSearchBarListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

.field private onShowOrDismissKeyboardListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

.field private searchBarLayout:Landroid/view/View;

.field private searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->init(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->init(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->init(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onSearchBarListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/SearchBarView;)Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/SearchBarView;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onShowOrDismissKeyboardListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 64
    const-string v4, "layout_inflater"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 65
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f0301ed

    invoke-virtual {v3, v4, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 67
    const v4, 0x7f0e09c7

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchBarLayout:Landroid/view/View;

    .line 68
    const v4, 0x7f0e09c9

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 69
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v5, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/SearchBarView$1;-><init>(Lcom/microsoft/xbox/xle/ui/SearchBarView;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 91
    const v4, 0x7f0e09ca

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/ui/SearchBarView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 92
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 94
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    new-instance v5, Lcom/microsoft/xbox/xle/ui/SearchBarView$2;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/SearchBarView$2;-><init>(Lcom/microsoft/xbox/xle/ui/SearchBarView;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 117
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v5, Lcom/microsoft/xbox/xle/ui/SearchBarView$3;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/ui/SearchBarView$3;-><init>(Lcom/microsoft/xbox/xle/ui/SearchBarView;)V

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070b49

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "resourceLanguage":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "ja-jp"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 127
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getTextSize()F

    move-result v1

    .line 128
    .local v1, "textSize":F
    const/high16 v4, 0x40400000    # 3.0f

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v6, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 129
    .local v2, "textSizeReduction":F
    cmpl-float v4, v1, v2

    if-lez v4, :cond_0

    .line 130
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const/4 v5, 0x0

    sub-float v6, v1, v2

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setTextSize(IF)V

    .line 133
    .end local v1    # "textSize":F
    .end local v2    # "textSizeReduction":F
    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->requestFocus()Z

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onSearchBarListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onSearchBarListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;->onClear()V

    .line 197
    :cond_0
    return-void
.end method

.method public disableSearch()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setEnabled(Z)V

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 167
    return-void
.end method

.method public enableSearch()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setEnabled(Z)V

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 172
    return-void
.end method

.method public getSearchTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onSetActive()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public onSetInactive()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->clearFocus()V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->clearButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->requestFocus()Z

    .line 177
    return-void
.end method

.method public requestFocusAndShowIME()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setSelected(Z)V

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setFocusableInTouchMode(Z)V

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->requestFocus()Z

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onShowOrDismissKeyboardListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onShowOrDismissKeyboardListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;->showKeyboard(Landroid/view/View;)V

    .line 186
    :cond_0
    return-void
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 1
    .param p1, "hint"    # Ljava/lang/String;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 143
    :cond_0
    return-void
.end method

.method public setInputFilters([Landroid/text/InputFilter;)V
    .locals 1
    .param p1, "filter"    # [Landroid/text/InputFilter;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 201
    return-void
.end method

.method public setOnSearchBarListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;)V
    .locals 0
    .param p1, "onSearchBarListener"    # Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onSearchBarListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;

    .line 137
    return-void
.end method

.method public setOnShowOrDismissKeyboardListener(Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;)V
    .locals 0
    .param p1, "onShowOrDismissKeyboardListener"    # Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->onShowOrDismissKeyboardListener:Lcom/microsoft/xbox/xle/ui/SearchBarView$OnShowOrDismissKeyboardListener;

    .line 147
    return-void
.end method

.method public setSearchTag(Ljava/lang/String;)V
    .locals 2
    .param p1, "searchTag"    # Ljava/lang/String;

    .prologue
    .line 150
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->requestFocus()Z

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setSelection(I)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/SearchBarView;->searchTagInputEdit:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->clearFocus()V

    goto :goto_0
.end method
