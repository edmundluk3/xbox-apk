.class public final enum Lcom/microsoft/xbox/smartglass/GamePadButtons;
.super Ljava/lang/Enum;
.source "GamePadButtons.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/GamePadButtons;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum Clear:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum DPadDown:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum DPadLeft:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum DPadRight:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum DPadUp:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum Enroll:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum LeftShoulder:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum LeftThumbStick:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum Menu:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum Nexus:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum PadA:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum PadB:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum PadX:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum PadY:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum RightShoulder:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum RightThumbStick:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field public static final enum View:Lcom/microsoft/xbox/smartglass/GamePadButtons;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/GamePadButtons;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "Clear"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Clear:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "Enroll"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Enroll:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "Nexus"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Nexus:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "Menu"

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4, v8}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Menu:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 22
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "View"

    invoke-direct {v2, v3, v8, v9}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->View:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 24
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "PadA"

    const/4 v4, 0x5

    const/16 v5, 0x10

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadA:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 26
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "PadB"

    const/4 v4, 0x6

    const/16 v5, 0x20

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadB:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 28
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "PadX"

    const/4 v4, 0x7

    const/16 v5, 0x40

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadX:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "PadY"

    const/16 v4, 0x80

    invoke-direct {v2, v3, v9, v4}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadY:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "DPadUp"

    const/16 v4, 0x9

    const/16 v5, 0x100

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadUp:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 34
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "DPadDown"

    const/16 v4, 0xa

    const/16 v5, 0x200

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadDown:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 36
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "DPadLeft"

    const/16 v4, 0xb

    const/16 v5, 0x400

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadLeft:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 38
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "DPadRight"

    const/16 v4, 0xc

    const/16 v5, 0x800

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadRight:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 40
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "LeftShoulder"

    const/16 v4, 0xd

    const/16 v5, 0x1000

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->LeftShoulder:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 42
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "RightShoulder"

    const/16 v4, 0xe

    const/16 v5, 0x2000

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->RightShoulder:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 44
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "LeftThumbStick"

    const/16 v4, 0xf

    const/16 v5, 0x4000

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->LeftThumbStick:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 46
    new-instance v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const-string v3, "RightThumbStick"

    const/16 v4, 0x10

    const v5, 0x8000

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/GamePadButtons;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->RightThumbStick:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 12
    const/16 v2, 0x11

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/GamePadButtons;

    sget-object v3, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Clear:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Enroll:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Nexus:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v3, v2, v7

    const/4 v3, 0x3

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Menu:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/smartglass/GamePadButtons;->View:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v3, v2, v8

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadA:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadB:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadX:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    sget-object v3, Lcom/microsoft/xbox/smartglass/GamePadButtons;->PadY:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v3, v2, v9

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadUp:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadDown:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadLeft:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    const/16 v3, 0xc

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->DPadRight:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    const/16 v3, 0xd

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->LeftShoulder:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    const/16 v3, 0xe

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->RightShoulder:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    const/16 v3, 0xf

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->LeftThumbStick:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    const/16 v3, 0x10

    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->RightThumbStick:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->$VALUES:[Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 49
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;->_lookup:Landroid/util/SparseArray;

    .line 52
    invoke-static {}, Lcom/microsoft/xbox/smartglass/GamePadButtons;->values()[Lcom/microsoft/xbox/smartglass/GamePadButtons;

    move-result-object v2

    array-length v3, v2

    .local v0, "buttons":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 53
    sget-object v4, Lcom/microsoft/xbox/smartglass/GamePadButtons;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/GamePadButtons;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 55
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput p3, p0, Lcom/microsoft/xbox/smartglass/GamePadButtons;->_value:I

    .line 59
    return-void
.end method

.method public static fromInt(I)Ljava/util/EnumSet;
    .locals 6
    .param p0, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/GamePadButtons;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    const-class v2, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 76
    .local v1, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/GamePadButtons;>;"
    invoke-static {}, Lcom/microsoft/xbox/smartglass/GamePadButtons;->values()[Lcom/microsoft/xbox/smartglass/GamePadButtons;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 77
    .local v0, "buttons":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/GamePadButtons;->getValue()I

    move-result v5

    and-int/2addr v5, p0

    if-eqz v5, :cond_0

    .line 78
    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 76
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 82
    .end local v0    # "buttons":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    :cond_1
    return-object v1
.end method

.method public static getValue(Ljava/util/EnumSet;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/GamePadButtons;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 91
    .local p0, "buttonSet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/microsoft/xbox/smartglass/GamePadButtons;>;"
    const/4 v1, 0x0

    .line 93
    .local v1, "value":I
    if-eqz p0, :cond_0

    .line 95
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .line 96
    .local v0, "button":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/GamePadButtons;->getValue()I

    move-result v3

    or-int/2addr v1, v3

    .line 97
    goto :goto_0

    .line 100
    .end local v0    # "button":Lcom/microsoft/xbox/smartglass/GamePadButtons;
    :cond_0
    return v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/GamePadButtons;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/GamePadButtons;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/GamePadButtons;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/smartglass/GamePadButtons;->$VALUES:[Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/GamePadButtons;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/GamePadButtons;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/microsoft/xbox/smartglass/GamePadButtons;->_value:I

    return v0
.end method
