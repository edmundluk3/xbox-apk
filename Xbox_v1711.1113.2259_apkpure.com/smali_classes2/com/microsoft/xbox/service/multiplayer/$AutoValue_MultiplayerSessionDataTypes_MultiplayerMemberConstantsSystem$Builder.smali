.class final Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;
.super Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem$Builder;
.source "$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private index:Ljava/lang/Integer;

.field private initialize:Ljava/lang/Boolean;

.field private xuid:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem$Builder;-><init>()V

    .line 81
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem$Builder;-><init>()V

    .line 83
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;->index()Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;->index:Ljava/lang/Integer;

    .line 84
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;->initialize()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;->initialize:Ljava/lang/Boolean;

    .line 85
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;->xuid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;->xuid:Ljava/lang/String;

    .line 86
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem;
    .locals 4

    .prologue
    .line 104
    new-instance v0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem;

    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;->index:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;->initialize:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;->xuid:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;)V

    return-object v0
.end method

.method public index(Ljava/lang/Integer;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem$Builder;
    .locals 0
    .param p1, "index"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 89
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;->index:Ljava/lang/Integer;

    .line 90
    return-object p0
.end method

.method public initialize(Ljava/lang/Boolean;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem$Builder;
    .locals 0
    .param p1, "initialize"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 94
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;->initialize:Ljava/lang/Boolean;

    .line 95
    return-object p0
.end method

.method public xuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberConstantsSystem$Builder;
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 99
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/$AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberConstantsSystem$Builder;->xuid:Ljava/lang/String;

    .line 100
    return-object p0
.end method
