.class public final Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "ClubBackgroundChangeViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClubBackgroundChangeScreenParameters"
.end annotation


# instance fields
.field private final clubId:J

.field private final removeBackgroundEnabled:Z

.field private final selectionCompletedRef:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JZLcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 3
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "removeBackgroundEnabled"    # Z
    .param p4    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JZ",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 259
    .local p4, "selectionCompleted":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 260
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 261
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 263
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;->clubId:J

    .line 264
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;->removeBackgroundEnabled:Z

    .line 265
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;->selectionCompletedRef:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 266
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;

    .prologue
    .line 247
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;->clubId:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;

    .prologue
    .line 247
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;->removeBackgroundEnabled:Z

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;

    .prologue
    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel$ClubBackgroundChangeScreenParameters;->selectionCompletedRef:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method
