.class public final Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_DLAssetsDataTypes_ClubpicList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubpicsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private defaultClubpics:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;",
            ">;"
        }
    .end annotation
.end field

.field private defaultVersion:I

.field private final versionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v3, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 24
    iput v3, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->defaultVersion:I

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->defaultClubpics:Lcom/google/common/collect/ImmutableList;

    .line 27
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->versionAdapter:Lcom/google/gson/TypeAdapter;

    .line 28
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->clubpicsAdapter:Lcom/google/gson/TypeAdapter;

    .line 29
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;
    .locals 5
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_0

    .line 55
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 56
    const/4 v3, 0x0

    .line 82
    :goto_0
    return-object v3

    .line 58
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 59
    iget v2, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->defaultVersion:I

    .line 60
    .local v2, "version":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->defaultClubpics:Lcom/google/common/collect/ImmutableList;

    .line 61
    .local v1, "clubpics":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;>;"
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 62
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_1

    .line 64
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 67
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 77
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 67
    :sswitch_0
    const-string v4, "version"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v4, "clubpics"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    .line 69
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->versionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 70
    goto :goto_1

    .line 73
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->clubpicsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "clubpics":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;>;"
    check-cast v1, Lcom/google/common/collect/ImmutableList;

    .line 74
    .restart local v1    # "clubpics":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;>;"
    goto :goto_1

    .line 81
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 82
    new-instance v3, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList;

    invoke-direct {v3, v2, v1}, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList;-><init>(ILcom/google/common/collect/ImmutableList;)V

    goto :goto_0

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x14f51cd8 -> :sswitch_0
        0x4a75d9df -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultClubpics(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;",
            ">;)",
            "Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "defaultClubpics":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->defaultClubpics:Lcom/google/common/collect/ImmutableList;

    .line 36
    return-object p0
.end method

.method public setDefaultVersion(I)Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultVersion"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->defaultVersion:I

    .line 32
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    if-nez p2, :cond_0

    .line 42
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 51
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 46
    const-string v0, "version"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->versionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;->version()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 48
    const-string v0, "clubpics"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->clubpicsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;->clubpics()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 50
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p2, Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_ClubpicList$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$ClubpicList;)V

    return-void
.end method
