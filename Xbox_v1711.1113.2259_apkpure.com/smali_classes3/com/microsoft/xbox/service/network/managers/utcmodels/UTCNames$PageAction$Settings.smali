.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Settings;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# static fields
.field public static final Connection:Ljava/lang/String; = "Connection"

.field public static final ConsoleRegion:Ljava/lang/String; = "Settings Use Console Region"

.field public static final ConsoleSetup:Ljava/lang/String; = "Settings - Console Setup"

.field public static final DefaultHomeAchievements:Ljava/lang/String; = "Settings - Default Homescreen Achievements"

.field public static final DefaultHomeActivityFeed:Ljava/lang/String; = "Settings - Default Homescreen ActivityFeed"

.field public static final DefaultHomeClubs:Ljava/lang/String; = "Settings - Default Homescreen Clubs"

.field public static final DefaultHomeFriends:Ljava/lang/String; = "Settings - Default Homescreen Friends"

.field public static final DefaultHomeMessages:Ljava/lang/String; = "Settings - Default Homescreen Messages"

.field public static final DefaultHomeTrending:Ljava/lang/String; = "Settings - Default Homescreen Trending"

.field public static final DefaultHomeTutorial:Ljava/lang/String; = "Settings - Default Homescreen Tutorial"

.field public static final Language:Ljava/lang/String; = "Settings - Select Language"

.field public static final ManualIP:Ljava/lang/String; = "Manual IP"

.field public static final NotificationAchievement:Ljava/lang/String; = "Settings - Achievement Notifications"

.field public static final NotificationBroadcast:Ljava/lang/String; = "Settings - Broadcast Notifications"

.field public static final NotificationClubAdminDemotion:Ljava/lang/String; = "Settings - Club Admin Demotion Notifications"

.field public static final NotificationClubAdminPromo:Ljava/lang/String; = "Settings - Club Admin Promotion Notifications"

.field public static final NotificationClubInvitation:Ljava/lang/String; = "Settings - Club Invitation Notifications"

.field public static final NotificationClubInvitationRequest:Ljava/lang/String; = "Settings - Club Invitation Request Notifications"

.field public static final NotificationClubJoined:Ljava/lang/String; = "Settings - Club Joined Notifications"

.field public static final NotificationClubMembers:Ljava/lang/String; = "Settings - Club Members Notifications"

.field public static final NotificationClubRecommendation:Ljava/lang/String; = "Settings - Club Recommendations Notifications"

.field public static final NotificationClubReports:Ljava/lang/String; = "Settings - Club Report Notifications"

.field public static final NotificationClubTransfer:Ljava/lang/String; = "Settings - Club Transfer Notifications"

.field public static final NotificationFriendOnline:Ljava/lang/String; = "Settings - Favorite Online Notifications"

.field public static final NotificationLFG:Ljava/lang/String; = "Settings - LFG Notifications"

.field public static final NotificationNewMessage:Ljava/lang/String; = "Settings - New Message Notifications"

.field public static final NotificationPartyInvites:Ljava/lang/String; = "Settings - Party Invites Notifications"

.field public static final NotificationTournamentMatchReady:Ljava/lang/String; = "Settings - Tournament Match Ready"

.field public static final NotificationTournamentStatus:Ljava/lang/String; = "Settings - Tournament Status"

.field public static final NotificationTournamentTeamStatus:Ljava/lang/String; = "Settings - Tournament Team Status"

.field public static final PurchaseResult:Ljava/lang/String; = "PurchaseResult"

.field public static final Remote:Ljava/lang/String; = "Remote"

.field public static final ShowClubNotificationInChatBubble:Ljava/lang/String; = "Settings - Show Club Notifications in ChatBubble"

.field public static final ShowMessageNotificationInChatBubble:Ljava/lang/String; = "Settings - Show Message Notifications in ChatBubble"

.field public static final Signout:Ljava/lang/String; = "Sign Out"

.field public static final StarRating:Ljava/lang/String; = "StarRating"

.field public static final StoreRegion:Ljava/lang/String; = "Settings - Select Store Region"

.field public static final Volume:Ljava/lang/String; = "Volume"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
