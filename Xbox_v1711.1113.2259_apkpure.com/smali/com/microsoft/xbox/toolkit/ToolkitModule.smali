.class public Lcom/microsoft/xbox/toolkit/ToolkitModule;
.super Ljava/lang/Object;
.source "ToolkitModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideCircularDrawableFactory()Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 13
    sget-object v0, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->INSTANCE:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    return-object v0
.end method
