.class Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "RecentsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRecentsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$1;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 88
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 103
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 94
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 114
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 85
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;Z)Z

    .line 109
    return-void
.end method
