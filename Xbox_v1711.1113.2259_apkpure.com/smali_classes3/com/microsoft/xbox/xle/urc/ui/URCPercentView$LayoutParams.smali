.class public Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "URCPercentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/ui/URCPercentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public bottom:F

.field public left:F

.field public right:F

.field public top:F


# direct methods
.method public constructor <init>(IIFFFF)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "l"    # F
    .param p4, "t"    # F
    .param p5, "r"    # F
    .param p6, "b"    # F

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 135
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->__init(FFFF)V

    .line 136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x0

    .line 139
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 140
    sget-object v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPercentView_Layout:[I

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 141
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPercentView_Layout_left:I

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    .line 142
    .local v2, "l":F
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPercentView_Layout_right:I

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    .line 143
    .local v3, "r":F
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPercentView_Layout_top:I

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    .line 144
    .local v4, "t":F
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCPercentView_Layout_bottom:I

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    .line 145
    .local v1, "b":F
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 147
    invoke-direct {p0, v2, v4, v3, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->__init(FFFF)V

    .line 148
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 5
    .param p1, "source"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v2, 0x0

    .line 151
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 153
    instance-of v1, p1, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 154
    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;

    .line 155
    .local v0, "lpm":Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;
    iget v1, v0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->left:F

    iget v2, v0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->top:F

    iget v3, v0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->right:F

    iget v4, v0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->bottom:F

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->__init(FFFF)V

    .line 159
    .end local v0    # "lpm":Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;
    :goto_0
    return-void

    .line 157
    :cond_0
    invoke-direct {p0, v2, v2, v2, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->__init(FFFF)V

    goto :goto_0
.end method

.method private __init(FFFF)V
    .locals 0
    .param p1, "l"    # F
    .param p2, "t"    # F
    .param p3, "r"    # F
    .param p4, "b"    # F

    .prologue
    .line 162
    iput p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->left:F

    .line 163
    iput p3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->right:F

    .line 164
    iput p2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->top:F

    .line 165
    iput p4, p0, Lcom/microsoft/xbox/xle/urc/ui/URCPercentView$LayoutParams;->bottom:F

    .line 166
    return-void
.end method
