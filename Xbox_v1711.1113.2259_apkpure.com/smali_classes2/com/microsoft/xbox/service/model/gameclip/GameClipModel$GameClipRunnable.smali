.class Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "GameClipModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GameClipRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;->this$0:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$1;

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;-><init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;)V

    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;->this$0:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->access$100(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->xuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;->this$0:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->access$100(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->scid:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;->this$0:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->access$100(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    move-result-object v3

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;->gameClipId:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameClipInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;->buildData()Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 141
    const-wide/16 v0, 0xbc1

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;->this$0:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 137
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method
