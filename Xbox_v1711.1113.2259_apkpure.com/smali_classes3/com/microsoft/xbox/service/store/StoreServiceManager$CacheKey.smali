.class Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;
.super Ljava/lang/Object;
.source "StoreServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CacheKey"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final language:Ljava/lang/String;

.field private final market:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "market"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->market:Ljava/lang/String;

    .line 187
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->language:Ljava/lang/String;

    .line 188
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 192
    if-ne p1, p0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v1

    .line 194
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;

    if-nez v3, :cond_2

    move v1, v2

    .line 195
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 197
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;

    .line 198
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->market:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->market:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->language:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->language:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 204
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->hashCode:I

    if-nez v0, :cond_0

    .line 205
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->hashCode:I

    .line 206
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->market:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->hashCode:I

    .line 207
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->language:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->hashCode:I

    .line 209
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
