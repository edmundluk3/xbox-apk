.class public Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;
.super Landroid/support/design/widget/CoordinatorLayout$Behavior;
.source "StackingBehavior.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/design/widget/CoordinatorLayout$Behavior",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field protected static final NOT_ID:I = -0x1


# instance fields
.field private enabled:Z

.field final previousSiblingId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>()V

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->enabled:Z

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->previousSiblingId:I

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->enabled:Z

    .line 40
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->StackingBehavior_Params:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 42
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, -0x1

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->previousSiblingId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 46
    return-void

    .line 44
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method private static computeHOffset(Ljava/util/Set;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "deps":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    const/4 v1, 0x0

    .line 107
    .local v1, "hOffset":I
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 108
    .local v0, "dep":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 109
    .local v2, "lp":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    iget v3, v2, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v3, v5

    iget v5, v2, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v3, v5

    add-int/2addr v1, v3

    .line 110
    invoke-virtual {v2}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->getBehavior()Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v3

    instance-of v3, v3, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;

    if-eqz v3, :cond_0

    .line 111
    invoke-virtual {v2}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->getBehavior()Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->getOffset()I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0

    .line 114
    .end local v0    # "dep":Landroid/view/View;
    .end local v2    # "lp":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    :cond_1
    return v1
.end method

.method protected static computeSortedDependencies(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Ljava/util/Set;
    .locals 2
    .param p0, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p1, "child"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "Landroid/view/View;",
            ")",
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior$1;-><init>(Landroid/support/design/widget/CoordinatorLayout;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 130
    .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->computeSortedDependencies(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Ljava/util/Set;)V

    .line 131
    return-object v0
.end method

.method private static computeSortedDependencies(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Ljava/util/Set;)V
    .locals 3
    .param p0, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p1, "child"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "Landroid/view/View;",
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p2, "result":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    invoke-virtual {p0, p1}, Landroid/support/design/widget/CoordinatorLayout;->getDependencies(Landroid/view/View;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 136
    .local v0, "dep":Landroid/view/View;
    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 137
    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-static {p0, v0, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->computeSortedDependencies(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Ljava/util/Set;)V

    goto :goto_0

    .line 141
    .end local v0    # "dep":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private notifyOnLaidOut(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Ljava/util/Set;)V
    .locals 5
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/design/widget/CoordinatorLayout;",
            "Landroid/view/View;",
            "Ljava/util/Set",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p3, "deps":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 83
    .local v1, "dep":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 84
    .local v2, "lp":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    invoke-virtual {v2}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->getBehavior()Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v0

    .line 85
    .local v0, "b":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    instance-of v4, v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;

    if-eqz v4, :cond_0

    .line 86
    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;

    .end local v0    # "b":Landroid/support/design/widget/CoordinatorLayout$Behavior;
    invoke-virtual {v0, p1, v1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->onDepenencyLaidOut(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V

    goto :goto_0

    .line 89
    .end local v1    # "dep":Landroid/view/View;
    .end local v2    # "lp":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    :cond_1
    return-void
.end method


# virtual methods
.method public getOffset()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method protected getPreviousSibling(Landroid/support/design/widget/CoordinatorLayout;)Landroid/view/View;
    .locals 2
    .param p1, "coordinatorLayout"    # Landroid/support/design/widget/CoordinatorLayout;

    .prologue
    .line 35
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->previousSiblingId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->previousSiblingId:I

    invoke-virtual {p1, v0}, Landroid/support/design/widget/CoordinatorLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->enabled:Z

    return v0
.end method

.method protected isPreviousSibling(Landroid/view/View;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 30
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 31
    .local v0, "depId":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->previousSiblingId:I

    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public layoutDependsOn(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .locals 1
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "dependency"    # Landroid/view/View;

    .prologue
    .line 55
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->isPreviousSibling(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method protected onDependencyMoved(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "dependency"    # Landroid/view/View;

    .prologue
    .line 95
    return-void
.end method

.method protected onDepenencyLaidOut(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "dependency"    # Landroid/view/View;

    .prologue
    .line 92
    return-void
.end method

.method public onLayoutChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .locals 4
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "layoutDirection"    # I

    .prologue
    .line 73
    invoke-static {p1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->computeSortedDependencies(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Ljava/util/Set;

    move-result-object v0

    .line 74
    .local v0, "deps":Ljava/util/Set;, "Ljava/util/Set<Landroid/view/View;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->computeHOffset(Ljava/util/Set;)I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->getOffset()I

    move-result v3

    add-int v1, v2, v3

    .line 75
    .local v1, "hOffset":I
    invoke-virtual {p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;->onLayoutChild(Landroid/view/View;I)V

    .line 76
    invoke-static {p2, v1}, Landroid/support/v4/view/ViewCompat;->offsetTopAndBottom(Landroid/view/View;I)V

    .line 77
    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->notifyOnLaidOut(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Ljava/util/Set;)V

    .line 78
    const/4 v2, 0x1

    return v2
.end method

.method public onMeasureChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;IIII)Z
    .locals 10
    .param p1, "parent"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "parentWidthMeasureSpec"    # I
    .param p4, "widthUsed"    # I
    .param p5, "parentHeightMeasureSpec"    # I
    .param p6, "heightUsed"    # I

    .prologue
    .line 60
    invoke-static {p5}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 61
    .local v8, "height":I
    invoke-static {p1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->computeSortedDependencies(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->computeHOffset(Ljava/util/Set;)I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->getOffset()I

    move-result v1

    add-int v7, v0, v1

    .line 63
    .local v7, "hOffset":I
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 64
    .local v9, "lpChild":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    iget v0, v9, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->height:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/high16 v6, 0x40000000    # 2.0f

    .line 65
    .local v6, "childHMode":I
    :goto_0
    sub-int v0, v8, v7

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .local v4, "childHeightMeasureSpec":I
    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v3, p4

    move/from16 v5, p6

    .line 66
    invoke-virtual/range {v0 .. v5}, Landroid/support/design/widget/CoordinatorLayout;->onMeasureChild(Landroid/view/View;IIII)V

    .line 68
    const/4 v0, 0x1

    return v0

    .line 64
    .end local v4    # "childHeightMeasureSpec":I
    .end local v6    # "childHMode":I
    :cond_0
    const/high16 v6, -0x80000000

    goto :goto_0
.end method

.method public onStartNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;I)Z
    .locals 1
    .param p1, "coordinatorLayout"    # Landroid/support/design/widget/CoordinatorLayout;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "directTargetChild"    # Landroid/view/View;
    .param p4, "target"    # Landroid/view/View;
    .param p5, "nestedScrollAxes"    # I

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->enabled:Z

    return v0
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->enabled:Z

    .line 103
    return-void
.end method
