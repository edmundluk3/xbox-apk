.class public Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;
.super Landroid/widget/LinearLayout;
.source "EPGRecentChannelItemView.java"


# instance fields
.field private mCallSignView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mEpisodeImage:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

.field private mEpisodeImageNotAvailable:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mEpisodeShowtime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mStreamButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private mTuneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method static synthetic lambda$setChannel$0(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Landroid/view/View;)V
    .locals 7
    .param p0, "channel"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 132
    const-string v2, "RecentChannelTune"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelCallSign()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getCurrentProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v0

    .line 135
    .local v0, "currentProgram1":Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    .line 136
    .local v1, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-nez v1, :cond_0

    .line 143
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v3

    const-string v4, "ShowTuned"

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v6

    if-nez v0, :cond_1

    const-string v2, ""

    :goto_1
    invoke-virtual {v3, v4, v5, v6, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getTuneToUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getTitleLocation()Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 141
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method static synthetic lambda$setChannel$1(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Landroid/view/View;)V
    .locals 6
    .param p0, "channel"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 151
    const-string v3, "RecentChannelStream"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelCallSign()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "providerId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "channelId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getCurrentProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v2, 0x0

    .line 155
    .local v2, "showId":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v3, v4, :cond_0

    .line 156
    invoke-static {v1, v0, v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->trackStreamLaunchAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_0
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->streamChannel(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void

    .line 154
    .end local v2    # "showId":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getCurrentProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 48
    const v0, 0x7f0e0587

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 49
    const v0, 0x7f0e0588

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mCallSignView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 50
    const v0, 0x7f0e0584

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 51
    const v0, 0x7f0e0585

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeShowtime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 52
    const v0, 0x7f0e0586

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    const v0, 0x7f0e0583

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImage:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    .line 54
    const v0, 0x7f0e0582

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImageNotAvailable:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 55
    const v0, 0x7f0e0589

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mTuneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 56
    const v0, 0x7f0e058a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mStreamButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 57
    return-void
.end method

.method public setChannel(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 14
    .param p1, "channel"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getCurrentProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v3

    .line 63
    .local v3, "currentProgram":Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelImageUrl()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelImageUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_4

    .line 64
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelImageUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 65
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 66
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mCallSignView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 76
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "channelHeadend":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_5

    if-eqz v1, :cond_5

    .line 78
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 79
    .local v5, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_0

    .line 80
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    .end local v5    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_1
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 90
    :goto_1
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getStartTime()I

    move-result v7

    int-to-long v8, v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    cmp-long v7, v8, v10

    if-gtz v7, :cond_b

    .line 91
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentSeriesTitle()Ljava/lang/String;

    move-result-object v4

    .line 92
    .local v4, "parentSeriesTitle":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowTitle()Ljava/lang/String;

    move-result-object v6

    .line 93
    .local v6, "showTitle":Ljava/lang/String;
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    .line 94
    .local v2, "currentChannel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->isContentAppropriate(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v7

    if-nez v7, :cond_6

    const/4 v0, 0x1

    .line 97
    .local v0, "blockExplicit":Z
    :goto_2
    if-eqz v0, :cond_7

    .line 98
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070481

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 109
    :goto_3
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeShowtime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v7, :cond_2

    .line 110
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeShowtime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->getShowtimeForProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeShowtime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 114
    :cond_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImage:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 117
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowImageUrl()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_9

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowImageUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_9

    if-nez v0, :cond_9

    .line 118
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImage:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowImageUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setImageSrc(Ljava/lang/String;)V

    .line 119
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImage:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setVisibility(I)V

    .line 120
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImageNotAvailable:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v7, :cond_3

    .line 121
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImageNotAvailable:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 130
    :cond_3
    :goto_4
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mTuneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 131
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mTuneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->shouldEnableStreaming(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 146
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mStreamButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 171
    .end local v0    # "blockExplicit":Z
    .end local v2    # "currentChannel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .end local v4    # "parentSeriesTitle":Ljava/lang/String;
    .end local v6    # "showTitle":Ljava/lang/String;
    :goto_5
    return-void

    .line 68
    .end local v1    # "channelHeadend":Ljava/lang/String;
    :cond_4
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mCallSignView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelCallSign()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mCallSignView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 70
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 71
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 72
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const-string v8, ""

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 86
    .restart local v1    # "channelHeadend":Ljava/lang/String;
    :cond_5
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 94
    .restart local v2    # "currentChannel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .restart local v4    # "parentSeriesTitle":Ljava/lang/String;
    .restart local v6    # "showTitle":Ljava/lang/String;
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 100
    .restart local v0    # "blockExplicit":Z
    :cond_7
    if-eqz v4, :cond_8

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_8

    .line 101
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v7, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 104
    :cond_8
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v7, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 124
    :cond_9
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImage:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setVisibility(I)V

    .line 125
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImageNotAvailable:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v7, :cond_3

    .line 126
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImageNotAvailable:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_4

    .line 148
    :cond_a
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mStreamButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 150
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mStreamButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Landroid/view/View$OnClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    .line 163
    .end local v0    # "blockExplicit":Z
    .end local v2    # "currentChannel":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .end local v4    # "parentSeriesTitle":Ljava/lang/String;
    .end local v6    # "showTitle":Ljava/lang/String;
    :cond_b
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0704ba

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mParentSeriesTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 165
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeShowtime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 166
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImage:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setVisibility(I)V

    .line 167
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mTuneButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 168
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mStreamButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 169
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/EPGRecentChannelItemView;->mEpisodeImageNotAvailable:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto/16 :goto_5
.end method
