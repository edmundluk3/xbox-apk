.class public final Lcom/microsoft/xboxone/smartglass/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xboxone/smartglass/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ACTION_ADD_TO_NOW_PLAYING:I = 0x7f07005b

.field public static final ACTION_ALL_SEASONS:I = 0x7f07005c

.field public static final ACTION_DETAILS:I = 0x7f07005d

.field public static final ACTION_DETAILS_MS:I = 0x7f07005e

.field public static final ACTION_GET_ON_XBOX:I = 0x7f07005f

.field public static final ACTION_LATEST_EPISODE:I = 0x7f070060

.field public static final ACTION_LAUNCH_IN_IE:I = 0x7f070061

.field public static final ACTION_MORE:I = 0x7f070062

.field public static final ACTION_NEXT_EPISODE:I = 0x7f070063

.field public static final ACTION_NOT_AIRING:I = 0x7f070064

.field public static final ACTION_PIN:I = 0x7f070065

.field public static final ACTION_PLAY_ON_XBOX:I = 0x7f070066

.field public static final ACTION_PLAY_TOP_SONGS_ON_XBOX:I = 0x7f070067

.field public static final ACTION_PLAY_TRAILER:I = 0x7f070068

.field public static final ACTION_PREVIOUS_EPISODE:I = 0x7f070069

.field public static final ACTION_RATE:I = 0x7f07006a

.field public static final ACTION_SEASON_EPISODES:I = 0x7f07006b

.field public static final ACTION_UNPIN:I = 0x7f07006c

.field public static final Accessibility_BackButton:I = 0x7f07006d

.field public static final Accessibility_Checked:I = 0x7f07006e

.field public static final Accessibility_ConsoleConnect:I = 0x7f07006f

.field public static final Accessibility_NowPlaying:I = 0x7f070070

.field public static final Accessibility_NowPlaying_Close:I = 0x7f070071

.field public static final Accessibility_Unchecked:I = 0x7f070072

.field public static final AccountSettings_SignIn:I = 0x7f070073

.field public static final AccountSettings_SignOut:I = 0x7f070074

.field public static final AchievementDetail_Achievement_HeaderTitle:I = 0x7f070075

.field public static final AchievementDetail_AppProgress_Button_Text:I = 0x7f070076

.field public static final AchievementDetail_GameProgress_Button_Text:I = 0x7f070077

.field public static final AchievementDetail_InProgress:I = 0x7f070078

.field public static final AchievementDetail_Locked:I = 0x7f070079

.field public static final AchievementDetail_Reward_Art:I = 0x7f07007a

.field public static final AchievementDetail_Reward_Art_LockedDescription:I = 0x7f07007b

.field public static final AchievementDetail_Reward_InApp:I = 0x7f07007c

.field public static final AchievementDetail_Reward_InApp_LockedDescription:I = 0x7f07007d

.field public static final AchievementDetail_Reward_InGame:I = 0x7f07007e

.field public static final AchievementDetail_Reward_InGame_LockedDescription:I = 0x7f07007f

.field public static final AchievementDetail_Unlocked:I = 0x7f070080

.field public static final Achievement_ProgressPercentage:I = 0x7f070081

.field public static final Achievement_SecretAchievement_Description:I = 0x7f070082

.field public static final Achievement_SecretAchievement_Title:I = 0x7f070083

.field public static final AchievementsDetailsProgress:I = 0x7f070084

.field public static final AchievementsDetailsReward:I = 0x7f070085

.field public static final AchievementsError:I = 0x7f070086

.field public static final AchievementsFeaturedChallenges:I = 0x7f070087

.field public static final AchievementsFeaturedTitle:I = 0x7f070088

.field public static final AchievementsHome_ChallengesGallery_HeaderTitle_Tablet:I = 0x7f070089

.field public static final AchievementsHome_Challenges_NoData:I = 0x7f07008a

.field public static final AchievementsHome_Challenges_Phone:I = 0x7f07008b

.field public static final AchievementsHome_Challenges_Tablet:I = 0x7f07008c

.field public static final AchievementsHome_FriendsAchievements_HeaderTitle_Phone:I = 0x7f07008d

.field public static final AchievementsHome_FriendsAchievements_HeaderTitle_Tablet:I = 0x7f07008e

.field public static final AchievementsHome_FriendsAchievements_NoData:I = 0x7f07008f

.field public static final AchievementsHome_HeaderTitle_Phone:I = 0x7f070090

.field public static final AchievementsHome_HeaderTitle_Tablet:I = 0x7f070091

.field public static final AchievementsHome_LastGamePlayed:I = 0x7f070092

.field public static final AchievementsHome_LastGamePlayed_NoData:I = 0x7f070093

.field public static final AchievementsHome_ProgressGallery_FilterMostRecent360_Tablet:I = 0x7f070094

.field public static final AchievementsHome_ProgressGallery_FilterMostRecentDurango_Tablet:I = 0x7f070095

.field public static final AchievementsHome_ProgressGallery_FilterMostRecent_Tablet:I = 0x7f070096

.field public static final AchievementsHome_ProgressGallery_HeaderTitle_Tablet:I = 0x7f070097

.field public static final AchievementsHome_Progress_FilterMostRecent:I = 0x7f070098

.field public static final AchievementsHome_Progress_FilterMostRecent360:I = 0x7f070099

.field public static final AchievementsHome_Progress_FilterMostRecentDurango:I = 0x7f07009a

.field public static final AchievementsHome_Progress_HeaderTitle_Phone:I = 0x7f07009b

.field public static final AchievementsHome_Progress_HeaderTitle_Tablet:I = 0x7f07009c

.field public static final AchievementsHome_Progress_LastPlayedText:I = 0x7f07009d

.field public static final AchievementsHome_Progress_NoData:I = 0x7f07009e

.field public static final AchievementsHome_Summary_HeaderTitle_Phone:I = 0x7f07009f

.field public static final AchievementsProgressDurango:I = 0x7f0700a0

.field public static final AchievementsProgressMostRecent:I = 0x7f0700a1

.field public static final AchievementsProgressOther:I = 0x7f0700a2

.field public static final AchievementsProgressTitle:I = 0x7f0700a3

.field public static final AchievementsProgressXbox360Games:I = 0x7f0700a4

.field public static final AchievementsStopComparingTitle:I = 0x7f0700a5

.field public static final Achievements_SecretDescription:I = 0x7f0700a6

.field public static final ActivityAlerts_ListEntry:I = 0x7f0700a7

.field public static final ActivityAlerts_ListEntry_1Alert:I = 0x7f0700a8

.field public static final ActivityAlerts_ListEntry_Empty:I = 0x7f0700a9

.field public static final ActivityAlerts_ListEntry_Header:I = 0x7f0700aa

.field public static final ActivityAlerts_List_2People:I = 0x7f0700ab

.field public static final ActivityAlerts_List_AddedFriendText:I = 0x7f0700ac

.field public static final ActivityAlerts_List_CommentedOthersText:I = 0x7f0700ad

.field public static final ActivityAlerts_List_CommentedText:I = 0x7f0700ae

.field public static final ActivityAlerts_List_HeaderText:I = 0x7f0700af

.field public static final ActivityAlerts_List_LikedText:I = 0x7f0700b0

.field public static final ActivityAlerts_List_NPeople:I = 0x7f0700b1

.field public static final ActivityAlerts_List_SharedText:I = 0x7f0700b2

.field public static final ActivityAlerts_NoData:I = 0x7f0700b3

.field public static final ActivityAlerts_ServiceFailure:I = 0x7f0700b4

.field public static final ActivityFeed_1CommentText:I = 0x7f0700b5

.field public static final ActivityFeed_1LikeText:I = 0x7f0700b6

.field public static final ActivityFeed_1ShareText:I = 0x7f0700b7

.field public static final ActivityFeed_CommentsText:I = 0x7f0700b8

.field public static final ActivityFeed_ConfirmDeleteComment_Body:I = 0x7f0700b9

.field public static final ActivityFeed_ConfirmDeleteComment_Header:I = 0x7f0700ba

.field public static final ActivityFeed_ConfirmDeleteItem_Body:I = 0x7f0700bb

.field public static final ActivityFeed_ConfirmDeleteItem_Header:I = 0x7f0700bc

.field public static final ActivityFeed_DeleteComment:I = 0x7f0700bd

.field public static final ActivityFeed_DeleteError:I = 0x7f0700be

.field public static final ActivityFeed_Delete_Post:I = 0x7f0700bf

.field public static final ActivityFeed_Filter_Body:I = 0x7f0700c0

.field public static final ActivityFeed_Filter_Button_Narrator:I = 0x7f0700c1

.field public static final ActivityFeed_Filter_Clubs_Checkbox_Description:I = 0x7f0700c2

.field public static final ActivityFeed_Filter_Clubs_Checkbox_Label:I = 0x7f0700c3

.field public static final ActivityFeed_Filter_Confirm:I = 0x7f0700c4

.field public static final ActivityFeed_Filter_Favorites_Checkbox_Description:I = 0x7f0700c5

.field public static final ActivityFeed_Filter_Favorites_Checkbox_Label:I = 0x7f0700c6

.field public static final ActivityFeed_Filter_Friends_Checkbox_Description:I = 0x7f0700c7

.field public static final ActivityFeed_Filter_Friends_Checkbox_Label:I = 0x7f0700c8

.field public static final ActivityFeed_Filter_Games_Checkbox_Description:I = 0x7f0700c9

.field public static final ActivityFeed_Filter_Games_Checkbox_Label:I = 0x7f0700ca

.field public static final ActivityFeed_Filter_Header:I = 0x7f0700cb

.field public static final ActivityFeed_Filter_Popular_Checkbox_Description:I = 0x7f0700cc

.field public static final ActivityFeed_Filter_Popular_Checkbox_Label:I = 0x7f0700cd

.field public static final ActivityFeed_HideAll_Feed_Failure:I = 0x7f0700ce

.field public static final ActivityFeed_Hide_All_From_Club:I = 0x7f0700cf

.field public static final ActivityFeed_Hide_All_From_Game:I = 0x7f0700d0

.field public static final ActivityFeed_Hide_All_From_Game_Error_Body:I = 0x7f0700d1

.field public static final ActivityFeed_Hide_All_From_Game_Error_Cancel_Option:I = 0x7f0700d2

.field public static final ActivityFeed_Hide_All_From_Game_Error_Header:I = 0x7f0700d3

.field public static final ActivityFeed_Hide_All_From_Game_Error_HideAll_Option:I = 0x7f0700d4

.field public static final ActivityFeed_Hide_All_From_Game_Error_ManageGames_Option:I = 0x7f0700d5

.field public static final ActivityFeed_Hide_Feed_Failure:I = 0x7f0700d6

.field public static final ActivityFeed_Hide_From_Feed:I = 0x7f0700d7

.field public static final ActivityFeed_Hide_Post:I = 0x7f0700d8

.field public static final ActivityFeed_LikesText:I = 0x7f0700d9

.field public static final ActivityFeed_Nobody_Posted:I = 0x7f0700da

.field public static final ActivityFeed_PageTitle:I = 0x7f0700db

.field public static final ActivityFeed_Pin_Error:I = 0x7f0700dc

.field public static final ActivityFeed_Pin_Notification:I = 0x7f0700dd

.field public static final ActivityFeed_Pin_Overwrite_Body:I = 0x7f0700de

.field public static final ActivityFeed_Pin_Overwrite_Header:I = 0x7f0700df

.field public static final ActivityFeed_Pin_Post:I = 0x7f0700e0

.field public static final ActivityFeed_Pin_Text:I = 0x7f0700e1

.field public static final ActivityFeed_Pin_To_Club:I = 0x7f0700e2

.field public static final ActivityFeed_Pin_To_Gamehub:I = 0x7f0700e3

.field public static final ActivityFeed_Pin_To_Page:I = 0x7f0700e4

.field public static final ActivityFeed_Pin_To_Profile:I = 0x7f0700e5

.field public static final ActivityFeed_Pinning:I = 0x7f0700e6

.field public static final ActivityFeed_Profile_NoData:I = 0x7f0700e7

.field public static final ActivityFeed_Report_Post:I = 0x7f0700e8

.field public static final ActivityFeed_SharesText:I = 0x7f0700e9

.field public static final ActivityFeed_Show_In_Feed:I = 0x7f0700ea

.field public static final ActivityFeed_StatusPost_DiscardTextError_Body:I = 0x7f0700eb

.field public static final ActivityFeed_StatusPost_DiscardTextError_Header:I = 0x7f0700ec

.field public static final ActivityFeed_StatusPost_EntryText:I = 0x7f0700ed

.field public static final ActivityFeed_Undo:I = 0x7f0700ee

.field public static final ActivityFeed_Unhide_Feed_Failure:I = 0x7f0700ef

.field public static final ActivityFeed_Unpin_Error:I = 0x7f0700f0

.field public static final ActivityFeed_Unpin_Notification:I = 0x7f0700f1

.field public static final ActivityFeed_Unpin_Post:I = 0x7f0700f2

.field public static final ActivityFeed_Unpinning:I = 0x7f0700f3

.field public static final ActivityFeed_Weblink_Error:I = 0x7f0700f4

.field public static final ActivityFeed_YouLikedText:I = 0x7f0700f5

.field public static final ActivityItem_ServiceFailure:I = 0x7f0700f6

.field public static final Activity_Controller:I = 0x7f0700f7

.field public static final Activity_ErrorText:I = 0x7f0700f8

.field public static final Activity_LoadingText:I = 0x7f0700f9

.field public static final Activity_Play:I = 0x7f0700fa

.field public static final Activity_ProviderSpecific:I = 0x7f0700fb

.field public static final Activity_Providers:I = 0x7f0700fc

.field public static final Activity_Purchased:I = 0x7f0700fd

.field public static final Activity_Reload:I = 0x7f0700fe

.field public static final Activity_RequiresGold:I = 0x7f0700ff

.field public static final Activity_SmartGlass_Title:I = 0x7f070100

.field public static final Activity_XboxApp_LoadingText:I = 0x7f070101

.field public static final Activity_XboxApp_Title:I = 0x7f070102

.field public static final Actors_Header:I = 0x7f070103

.field public static final AddFriend_Error_TooManyFriends:I = 0x7f070104

.field public static final AllowNotificationsPromptDescription:I = 0x7f070105

.field public static final AllowNotificationsPromptTitle:I = 0x7f070106

.field public static final AppBar_Button_fastforward:I = 0x7f070107

.field public static final AppBar_Button_pause:I = 0x7f070108

.field public static final AppBar_Button_play:I = 0x7f070109

.field public static final AppBar_Button_rewind:I = 0x7f07010a

.field public static final AppBar_EpgCalendar:I = 0x7f07010b

.field public static final AppBar_JumpToNow:I = 0x7f07010c

.field public static final AppBar_Remote:I = 0x7f07010d

.field public static final AppChannel_Favorite:I = 0x7f07010e

.field public static final Auth_AccountCreation:I = 0x7f07010f

.field public static final Auth_AccountError:I = 0x7f070110

.field public static final Auth_GenericError:I = 0x7f070111

.field public static final Auth_TOU:I = 0x7f070112

.field public static final Beam_Filter_All_Streamers:I = 0x7f070113

.field public static final Beam_Filter_New_Streamers:I = 0x7f070114

.field public static final Beam_Filter_Rising_Stars:I = 0x7f070115

.field public static final Beam_Filter_Stream_groups:I = 0x7f070116

.field public static final Beam_Filter_Stream_teams:I = 0x7f070117

.field public static final Beam_Launch:I = 0x7f070118

.field public static final Beam_Launching:I = 0x7f070119

.field public static final Beam_Name:I = 0x7f07011a

.field public static final Bio_Header:I = 0x7f07011b

.field public static final Browse_Add_ons:I = 0x7f07011c

.field public static final Browse_Add_ons_Popular:I = 0x7f07011d

.field public static final Browse_Add_ons_Recent:I = 0x7f07011e

.field public static final Browse_Add_ons_Tablet:I = 0x7f07011f

.field public static final Browse_Apps:I = 0x7f070120

.field public static final Browse_Apps_Tablet:I = 0x7f070121

.field public static final Browse_Browse:I = 0x7f070122

.field public static final Browse_Browse_Tablet:I = 0x7f070123

.field public static final Browse_ComingSoon:I = 0x7f070124

.field public static final Browse_DealsWithGold:I = 0x7f070125

.field public static final Browse_DealsWithGold_Tablet:I = 0x7f070126

.field public static final Browse_Error_NoResults:I = 0x7f070127

.field public static final Browse_Featured:I = 0x7f070128

.field public static final Browse_Games:I = 0x7f070129

.field public static final Browse_GamesWithGold:I = 0x7f07012a

.field public static final Browse_GamesWithGold_Tablet:I = 0x7f07012b

.field public static final Browse_Games_Popular:I = 0x7f07012c

.field public static final Browse_Games_Recent:I = 0x7f07012d

.field public static final Browse_Games_Tablet:I = 0x7f07012e

.field public static final Browse_Gold:I = 0x7f07012f

.field public static final Browse_Gold_Tablet:I = 0x7f070130

.field public static final Browse_Header_Phone:I = 0x7f070131

.field public static final Browse_Header_Phone_MS:I = 0x7f070132

.field public static final Browse_Header_Tablet:I = 0x7f070133

.field public static final Browse_Header_Tablet_MS:I = 0x7f070134

.field public static final Browse_New:I = 0x7f070135

.field public static final Browse_Popular:I = 0x7f070136

.field public static final Browse_Recent:I = 0x7f070137

.field public static final Browse_Top_Free:I = 0x7f070138

.field public static final Browse_Top_Paid:I = 0x7f070139

.field public static final BusyIndicator_LoadingText:I = 0x7f07013a

.field public static final COMMA_SEPARATOR:I = 0x7f07013b

.field public static final CONNECTION_REQ_TEXT:I = 0x7f07013c

.field public static final ChallengeDetail_Reward_Art_LockedDescription:I = 0x7f07013d

.field public static final ChallengeDetail_Reward_InApp_LockedDescription:I = 0x7f07013e

.field public static final ChallengeDetail_Reward_InGame_LockedDescription:I = 0x7f07013f

.field public static final Challenge_Countdown_Days:I = 0x7f070140

.field public static final Challenge_Countdown_Days_Left:I = 0x7f070141

.field public static final Challenge_Countdown_OneDay:I = 0x7f070142

.field public static final Challenge_Countdown_OneDay_Left:I = 0x7f070143

.field public static final Challenges_Header:I = 0x7f070144

.field public static final ChangeRelationship_Checkbox_Subtext_ShareRealName:I = 0x7f070145

.field public static final ChangeRelationship_Checkbox_Subtext_ShareRealName_Alt:I = 0x7f070146

.field public static final ChangeRelationship_Checkbox_Subtext_ShareRealName_Everyone:I = 0x7f070147

.field public static final ChangeRelationship_Checkbox_Subtext_ShareRealName_Friends:I = 0x7f070148

.field public static final ChangeRelationship_Label_Favorite:I = 0x7f070149

.field public static final ChangeRelationship_Label_Friend:I = 0x7f07014a

.field public static final ChangeRelationship_RadioButton_Subtext_Favorite:I = 0x7f07014b

.field public static final ChangeRelationship_RadioButton_Subtext_Friend:I = 0x7f07014c

.field public static final Chat_Button_Add:I = 0x7f07014d

.field public static final Chat_Button_Delete:I = 0x7f07014e

.field public static final Chat_Button_Leave:I = 0x7f07014f

.field public static final Chat_Button_Mute:I = 0x7f070150

.field public static final Chat_Button_Rename:I = 0x7f070151

.field public static final CloseButtonText:I = 0x7f070152

.field public static final Club_AcceptAll:I = 0x7f070153

.field public static final Club_AcceptInvite:I = 0x7f070154

.field public static final Club_AcceptInvite_Accepted_Description:I = 0x7f070155

.field public static final Club_AcceptInvite_Accepted_Title:I = 0x7f070156

.field public static final Club_AcceptInvite_FailLimit_Body:I = 0x7f070157

.field public static final Club_AcceptInvite_FailLimit_Title:I = 0x7f070158

.field public static final Club_AcceptInvite_Short:I = 0x7f070159

.field public static final Club_AcceptInvite_Success_Body_Long:I = 0x7f07015a

.field public static final Club_AcceptInvite_Success_Body_Short:I = 0x7f07015b

.field public static final Club_AcceptInvite_Success_Title:I = 0x7f07015c

.field public static final Club_AcceptInvite_TooMany_Description:I = 0x7f07015d

.field public static final Club_AcceptInvite_TooMany_Title:I = 0x7f07015e

.field public static final Club_AcceptRequest:I = 0x7f07015f

.field public static final Club_Access_Blocked_Body:I = 0x7f070160

.field public static final Club_Access_Blocked_Title:I = 0x7f070161

.field public static final Club_Actions:I = 0x7f070162

.field public static final Club_Activity:I = 0x7f070163

.field public static final Club_Admin_RequestToJoin_Empty:I = 0x7f070164

.field public static final Club_Admin_Subtitle:I = 0x7f070165

.field public static final Club_BanFromClub:I = 0x7f070166

.field public static final Club_BannedList:I = 0x7f070167

.field public static final Club_Banned_Access:I = 0x7f070168

.field public static final Club_Chat:I = 0x7f070169

.field public static final Club_Chat_Channels:I = 0x7f07016a

.field public static final Club_ChooseClub:I = 0x7f07016b

.field public static final Club_Choose_Moderators:I = 0x7f07016c

.field public static final Club_ConfirmInvite_Body:I = 0x7f07016d

.field public static final Club_ConfirmInvite_Title:I = 0x7f07016e

.field public static final Club_Create_AnyoneCan:I = 0x7f07016f

.field public static final Club_Create_Back:I = 0x7f070170

.field public static final Club_Create_Cancel:I = 0x7f070171

.field public static final Club_Create_ChooseName_CheckAvailability:I = 0x7f070172

.field public static final Club_Create_ChooseName_DefaultFormat1:I = 0x7f070173

.field public static final Club_Create_ChooseName_DefaultFormat2:I = 0x7f070174

.field public static final Club_Create_ChooseName_DefaultFormat3:I = 0x7f070175

.field public static final Club_Create_ChooseName_DefaultFormat4:I = 0x7f070176

.field public static final Club_Create_ChooseName_DefaultFormat5:I = 0x7f070177

.field public static final Club_Create_ChooseName_Description:I = 0x7f070178

.field public static final Club_Create_ChooseName_ErrorNameNotAvailable:I = 0x7f070179

.field public static final Club_Create_ChooseName_Title:I = 0x7f07017a

.field public static final Club_Create_ChooseName_WhatClubName:I = 0x7f07017b

.field public static final Club_Create_ChooseTags_Summary:I = 0x7f07017c

.field public static final Club_Create_ChooseType_Description1:I = 0x7f07017d

.field public static final Club_Create_ChooseType_HiddenDescription1:I = 0x7f07017e

.field public static final Club_Create_ChooseType_PrivateDescription1:I = 0x7f07017f

.field public static final Club_Create_ChooseType_PublicDescription1:I = 0x7f070180

.field public static final Club_Create_ChooseType_Remaining:I = 0x7f070181

.field public static final Club_Create_ChooseType_Title:I = 0x7f070182

.field public static final Club_Create_ChooseType_WhatType:I = 0x7f070183

.field public static final Club_Create_ChooseType_YouCanCreate:I = 0x7f070184

.field public static final Club_Create_ChooseType_YouHaveRemaining:I = 0x7f070185

.field public static final Club_Create_Customize_Done:I = 0x7f070186

.field public static final Club_Create_Disclaimer:I = 0x7f070187

.field public static final Club_Create_DoesNotStartWithLetterClubName:I = 0x7f070188

.field public static final Club_Create_Error:I = 0x7f070189

.field public static final Club_Create_FreeNameChange:I = 0x7f07018a

.field public static final Club_Create_GenericError:I = 0x7f07018b

.field public static final Club_Create_HiddenAnyoneDescription:I = 0x7f07018c

.field public static final Club_Create_HiddenClub:I = 0x7f07018d

.field public static final Club_Create_HiddenMembersDescription:I = 0x7f07018e

.field public static final Club_Create_InappropriateClubName:I = 0x7f07018f

.field public static final Club_Create_InvalidClubName:I = 0x7f070190

.field public static final Club_Create_InvitedCan:I = 0x7f070191

.field public static final Club_Create_MembersCan:I = 0x7f070192

.field public static final Club_Create_Next:I = 0x7f070193

.field public static final Club_Create_NonEnglishCharacterClubName:I = 0x7f070194

.field public static final Club_Create_NotReady:I = 0x7f070195

.field public static final Club_Create_OutOfClubs:I = 0x7f070196

.field public static final Club_Create_PrivateAnyoneDescription:I = 0x7f070197

.field public static final Club_Create_PrivateClub:I = 0x7f070198

.field public static final Club_Create_PrivateMembersDescription:I = 0x7f070199

.field public static final Club_Create_PublicAnyoneDescription:I = 0x7f07019a

.field public static final Club_Create_PublicClub:I = 0x7f07019b

.field public static final Club_Create_PublicMembersDescription:I = 0x7f07019c

.field public static final Club_Create_SpacesInFrontClubName:I = 0x7f07019d

.field public static final Club_Create_Summary_CreateClub:I = 0x7f07019e

.field public static final Club_Create_Summary_CreateClub_Confirmation:I = 0x7f07019f

.field public static final Club_Create_Summary_Description1:I = 0x7f0701a0

.field public static final Club_Create_Summary_Description2:I = 0x7f0701a1

.field public static final Club_Create_Summary_Description3:I = 0x7f0701a2

.field public static final Club_Create_Summary_Title:I = 0x7f0701a3

.field public static final Club_Create_TooLongClubName:I = 0x7f0701a4

.field public static final Club_Create_TooLongDetailClubName:I = 0x7f0701a5

.field public static final Club_Create_TooManySpacesClubName:I = 0x7f0701a6

.field public static final Club_Create_TryAgain:I = 0x7f0701a7

.field public static final Club_Customize_AddBackground:I = 0x7f0701a8

.field public static final Club_Customize_AllGames:I = 0x7f0701a9

.field public static final Club_Customize_BackgroundImage:I = 0x7f0701aa

.field public static final Club_Customize_BrowseAllAchievements:I = 0x7f0701ab

.field public static final Club_Customize_BrowseClubPics:I = 0x7f0701ac

.field public static final Club_Customize_BrowseGamerpics:I = 0x7f0701ad

.field public static final Club_Customize_ChangeBackground:I = 0x7f0701ae

.field public static final Club_Customize_ChangeBackgroundImage:I = 0x7f0701af

.field public static final Club_Customize_ChangeBackground_Error:I = 0x7f0701b0

.field public static final Club_Customize_ChangeBackground_Menu:I = 0x7f0701b1

.field public static final Club_Customize_ChangeBackground_NoRecentAchievements:I = 0x7f0701b2

.field public static final Club_Customize_ChangeBackground_NoScreenshots:I = 0x7f0701b3

.field public static final Club_Customize_ChangeClubName_AlreadyChanged_Error_Body:I = 0x7f0701b4

.field public static final Club_Customize_ChangeClubName_AlreadyChanged_Error_Title:I = 0x7f0701b5

.field public static final Club_Customize_ChangeClubName_GenericError:I = 0x7f0701b6

.field public static final Club_Customize_ChangeClubName_NotFreeUpdate_Error:I = 0x7f0701b7

.field public static final Club_Customize_ChangeColor:I = 0x7f0701b8

.field public static final Club_Customize_ChangeProfilePic:I = 0x7f0701b9

.field public static final Club_Customize_ClubBackgroundImage:I = 0x7f0701ba

.field public static final Club_Customize_DescriptionPlaceholder:I = 0x7f0701bb

.field public static final Club_Customize_DescriptionPlaceholderDetail:I = 0x7f0701bc

.field public static final Club_Customize_ErrorUpdatingBackgroundImage:I = 0x7f0701bd

.field public static final Club_Customize_ErrorUpdatingClubColor:I = 0x7f0701be

.field public static final Club_Customize_ErrorUpdatingClubName:I = 0x7f0701bf

.field public static final Club_Customize_ErrorUpdatingDescription:I = 0x7f0701c0

.field public static final Club_Customize_ErrorUpdatingDisplayImage:I = 0x7f0701c1

.field public static final Club_Customize_ErrorUpdatingTags:I = 0x7f0701c2

.field public static final Club_Customize_ErrorUpdatingTitles:I = 0x7f0701c3

.field public static final Club_Customize_GamesPlaceholder:I = 0x7f0701c4

.field public static final Club_Customize_GamesPlaceholderDetail:I = 0x7f0701c5

.field public static final Club_Customize_Games_TooManySelectedError_Detail:I = 0x7f0701c6

.field public static final Club_Customize_Games_TooManySelectedError_Header:I = 0x7f0701c7

.field public static final Club_Customize_Header:I = 0x7f0701c8

.field public static final Club_Customize_NumGames_Selected:I = 0x7f0701c9

.field public static final Club_Customize_ProfileImage:I = 0x7f0701ca

.field public static final Club_Customize_RecentAchievements:I = 0x7f0701cb

.field public static final Club_Customize_RemoveBackgroundImage:I = 0x7f0701cc

.field public static final Club_Customize_Screenshots:I = 0x7f0701cd

.field public static final Club_Customize_SearchForGame:I = 0x7f0701ce

.field public static final Club_Customize_SelectGames:I = 0x7f0701cf

.field public static final Club_Customize_TagsChoose:I = 0x7f0701d0

.field public static final Club_Customize_TagsNumberSelected:I = 0x7f0701d1

.field public static final Club_Customize_TagsPlaceholderDetail:I = 0x7f0701d2

.field public static final Club_Customize_Tags_TooManySelectedError_Detail:I = 0x7f0701d3

.field public static final Club_Customize_Tags_TooManySelectedError_Header:I = 0x7f0701d4

.field public static final Club_DeclineAllInvites:I = 0x7f0701d5

.field public static final Club_DeclineAllInvites_Body:I = 0x7f0701d6

.field public static final Club_DeclineInvite:I = 0x7f0701d7

.field public static final Club_DecoratedNameFormat:I = 0x7f0701d8

.field public static final Club_Delete_Confirm_Immediate_Body:I = 0x7f0701d9

.field public static final Club_Delete_Confirm_Ok:I = 0x7f0701da

.field public static final Club_Delete_Confirm_Suspend_Body:I = 0x7f0701db

.field public static final Club_Delete_Confirm_Title:I = 0x7f0701dc

.field public static final Club_Delete_Failure:I = 0x7f0701dd

.field public static final Club_Delete_Success:I = 0x7f0701de

.field public static final Club_Discover_Card_Description:I = 0x7f0701df

.field public static final Club_Discover_Card_Hide:I = 0x7f0701e0

.field public static final Club_Discover_Card_OK:I = 0x7f0701e1

.field public static final Club_Discover_Card_Title:I = 0x7f0701e2

.field public static final Club_Discovery_Trending:I = 0x7f0701e3

.field public static final Club_Error_WithErrorCode:I = 0x7f0701e4

.field public static final Club_Feed_Decorated_Gamertag:I = 0x7f0701e5

.field public static final Club_Feed_DiscardFeed:I = 0x7f0701e6

.field public static final Club_Feed_EmptyState:I = 0x7f0701e7

.field public static final Club_Feed_Member_Only:I = 0x7f0701e8

.field public static final Club_Feed_PostError:I = 0x7f0701e9

.field public static final Club_Feed_SaySomethingToTheClub:I = 0x7f0701ea

.field public static final Club_Feed_SendTitle:I = 0x7f0701eb

.field public static final Club_Follow:I = 0x7f0701ec

.field public static final Club_Follower:I = 0x7f0701ed

.field public static final Club_Full:I = 0x7f0701ee

.field public static final Club_Games:I = 0x7f0701ef

.field public static final Club_HiddenParentheses:I = 0x7f0701f0

.field public static final Club_HiddenType:I = 0x7f0701f1

.field public static final Club_Home_Parties:I = 0x7f0701f2

.field public static final Club_Info:I = 0x7f0701f3

.field public static final Club_Info_Bio:I = 0x7f0701f4

.field public static final Club_Info_Customize:I = 0x7f0701f5

.field public static final Club_Info_ErrorLoading:I = 0x7f0701f6

.field public static final Club_Info_Invite:I = 0x7f0701f7

.field public static final Club_Info_ManageNotifications:I = 0x7f0701f8

.field public static final Club_Info_NoData:I = 0x7f0701f9

.field public static final Club_Info_PeopleHereNow:I = 0x7f0701fa

.field public static final Club_Info_PeopleHereToday:I = 0x7f0701fb

.field public static final Club_Info_PlayingNow:I = 0x7f0701fc

.field public static final Club_Info_PreferPlay:I = 0x7f0701fd

.field public static final Club_Info_Report:I = 0x7f0701fe

.field public static final Club_Info_UsuallyPlays:I = 0x7f0701ff

.field public static final Club_Invitations:I = 0x7f070200

.field public static final Club_Invitations_InvitedBy:I = 0x7f070201

.field public static final Club_Invitations_InvitedByUnknown:I = 0x7f070202

.field public static final Club_Invitations_YouHaveBeenInvited:I = 0x7f070203

.field public static final Club_InviteSent:I = 0x7f070204

.field public static final Club_InviteToClub:I = 0x7f070205

.field public static final Club_InviteToClub_Error_Details:I = 0x7f070206

.field public static final Club_InviteToClub_Error_Title:I = 0x7f070207

.field public static final Club_Invite_Header:I = 0x7f070208

.field public static final Club_Invite_PartialFail:I = 0x7f070209

.field public static final Club_Invite_To:I = 0x7f07020a

.field public static final Club_LastSeen:I = 0x7f07020b

.field public static final Club_LinkedGames:I = 0x7f07020c

.field public static final Club_LookingForGroup_Title:I = 0x7f07020d

.field public static final Club_ManageClub:I = 0x7f07020e

.field public static final Club_Manage_Invites:I = 0x7f07020f

.field public static final Club_Manage_Invites_Ignore:I = 0x7f070210

.field public static final Club_Manage_Invites_IgnoreAll:I = 0x7f070211

.field public static final Club_Manage_Invites_InviteAll:I = 0x7f070212

.field public static final Club_Manage_Invites_InviteAll_Sent:I = 0x7f070213

.field public static final Club_Manage_Members:I = 0x7f070214

.field public static final Club_Manage_Reports:I = 0x7f070215

.field public static final Club_Manage_Title:I = 0x7f070216

.field public static final Club_Member_ErrorLoading:I = 0x7f070217

.field public static final Club_Member_MemberOnlyAccess:I = 0x7f070218

.field public static final Club_Member_Search_NoMatch:I = 0x7f070219

.field public static final Club_Membership_Banned:I = 0x7f07021a

.field public static final Club_Membership_CancelInvitation:I = 0x7f07021b

.field public static final Club_Membership_CancelRequestToJoin:I = 0x7f07021c

.field public static final Club_Membership_PendingJoin:I = 0x7f07021d

.field public static final Club_Membership_RequestToJoin:I = 0x7f07021e

.field public static final Club_Membership_Resign:I = 0x7f07021f

.field public static final Club_Membership_Resign_Confirmation_Description:I = 0x7f070220

.field public static final Club_Membership_Resign_Confirmation_Title:I = 0x7f070221

.field public static final Club_Membership_Resign_Yes:I = 0x7f070222

.field public static final Club_Moderation_Ban:I = 0x7f070223

.field public static final Club_Moderation_ConfirmBanMember:I = 0x7f070224

.field public static final Club_Moderation_ConfirmBanMemberTitle:I = 0x7f070225

.field public static final Club_Moderation_ConfirmRemoveMember:I = 0x7f070226

.field public static final Club_Moderation_ConfirmRemoveMemberTitle:I = 0x7f070227

.field public static final Club_Moderation_DeleteRequestToJoinError:I = 0x7f070228

.field public static final Club_Moderation_Demote:I = 0x7f070229

.field public static final Club_Moderation_FailedToBan_Body:I = 0x7f07022a

.field public static final Club_Moderation_FailedToBan_Title:I = 0x7f07022b

.field public static final Club_Moderation_FailedToDemote_Body:I = 0x7f07022c

.field public static final Club_Moderation_FailedToDemote_Title:I = 0x7f07022d

.field public static final Club_Moderation_FailedToIgnoreAll_PartialError:I = 0x7f07022e

.field public static final Club_Moderation_FailedToPromote_Body:I = 0x7f07022f

.field public static final Club_Moderation_FailedToPromote_Title:I = 0x7f070230

.field public static final Club_Moderation_FailedToRemove_Body:I = 0x7f070231

.field public static final Club_Moderation_FailedToRemove_Title:I = 0x7f070232

.field public static final Club_Moderation_FailedToUnban_Body:I = 0x7f070233

.field public static final Club_Moderation_FailedToUnban_Title:I = 0x7f070234

.field public static final Club_Moderation_InappropriateClub_Summary:I = 0x7f070235

.field public static final Club_Moderation_Promote:I = 0x7f070236

.field public static final Club_Moderation_Remove:I = 0x7f070237

.field public static final Club_Moderation_Remove_Confirmation_Description:I = 0x7f070238

.field public static final Club_Moderation_Remove_Confirmation_Title:I = 0x7f070239

.field public static final Club_Moderation_ReportToModerators:I = 0x7f07023a

.field public static final Club_Moderation_Unban:I = 0x7f07023b

.field public static final Club_NoSlotsAvailable:I = 0x7f07023c

.field public static final Club_Notification_AdminPromotion_Android:I = 0x7f07023d

.field public static final Club_Notification_Demotion_Aggregate_Android:I = 0x7f07023e

.field public static final Club_Notification_Demotion_Android:I = 0x7f07023f

.field public static final Club_Notification_Invitation_Android:I = 0x7f070240

.field public static final Club_Notification_Joined_Android:I = 0x7f070241

.field public static final Club_Notification_ModerationInvitation_Aggregate_Android:I = 0x7f070242

.field public static final Club_Notification_ModerationInvitation_Android:I = 0x7f070243

.field public static final Club_Notification_ModerationReport_Aggregate_Android:I = 0x7f070244

.field public static final Club_Notification_ModerationReport_Android:I = 0x7f070245

.field public static final Club_Notification_NewMember_Aggregate_Android:I = 0x7f070246

.field public static final Club_Notification_NewMember_Android:I = 0x7f070247

.field public static final Club_Notification_OwnerPromotion_Android:I = 0x7f070248

.field public static final Club_Notification_Recommendation_Android:I = 0x7f070249

.field public static final Club_Notification_Title:I = 0x7f07024a

.field public static final Club_OneSlotAvailable:I = 0x7f07024b

.field public static final Club_Owner:I = 0x7f07024c

.field public static final Club_Party:I = 0x7f07024d

.field public static final Club_Party_HereNow_Format:I = 0x7f07024e

.field public static final Club_Party_Status_Format:I = 0x7f07024f

.field public static final Club_Picker_NoAdmin:I = 0x7f070250

.field public static final Club_Picker_NoOwner:I = 0x7f070251

.field public static final Club_Picker_NoPostFeed:I = 0x7f070252

.field public static final Club_Play:I = 0x7f070253

.field public static final Club_Play_CreateAPost:I = 0x7f070254

.field public static final Club_Play_Empty_String:I = 0x7f070255

.field public static final Club_Play_Empty_Title:I = 0x7f070256

.field public static final Club_Play_ErrorLoading:I = 0x7f070257

.field public static final Club_Play_Happening_Now:I = 0x7f070258

.field public static final Club_Play_Invite_Only:I = 0x7f070259

.field public static final Club_Play_Joinable:I = 0x7f07025a

.field public static final Club_Play_Now:I = 0x7f07025b

.field public static final Club_Play_PartyStarted:I = 0x7f07025c

.field public static final Club_Play_PostedJustNow:I = 0x7f07025d

.field public static final Club_Play_Rules:I = 0x7f07025e

.field public static final Club_Play_Upcoming:I = 0x7f07025f

.field public static final Club_Presence_Here:I = 0x7f070260

.field public static final Club_Presence_LFG:I = 0x7f070261

.field public static final Club_Presence_LFGs:I = 0x7f070262

.field public static final Club_Presence_Parties:I = 0x7f070263

.field public static final Club_Presence_Party:I = 0x7f070264

.field public static final Club_Presence_PlayingNow:I = 0x7f070265

.field public static final Club_PrivateParentheses:I = 0x7f070266

.field public static final Club_PrivateType:I = 0x7f070267

.field public static final Club_ProfileColor_Picker_Title:I = 0x7f070268

.field public static final Club_ProfileImage_Picker_Title:I = 0x7f070269

.field public static final Club_PublicParentheses:I = 0x7f07026a

.field public static final Club_PublicType:I = 0x7f07026b

.field public static final Club_RecommendationSent:I = 0x7f07026c

.field public static final Club_Recommendation_PartialFail:I = 0x7f07026d

.field public static final Club_RecommendationsSent:I = 0x7f07026e

.field public static final Club_RecommendedBy:I = 0x7f07026f

.field public static final Club_RecommendedOnly:I = 0x7f070270

.field public static final Club_ReportCount_MultipleFormat:I = 0x7f070271

.field public static final Club_ReportCount_Single:I = 0x7f070272

.field public static final Club_ReportItemError:I = 0x7f070273

.field public static final Club_ReportList_BanGamertagFormat:I = 0x7f070274

.field public static final Club_ReportList_BanSuccess:I = 0x7f070275

.field public static final Club_ReportList_DeleteError:I = 0x7f070276

.field public static final Club_ReportList_DeleteItem:I = 0x7f070277

.field public static final Club_ReportList_IgnoreReport:I = 0x7f070278

.field public static final Club_ReportList_MessageGamertagFormat:I = 0x7f070279

.field public static final Club_ReportList_NoData:I = 0x7f07027a

.field public static final Club_Report_Description:I = 0x7f07027b

.field public static final Club_Reporter_Title_Format:I = 0x7f07027c

.field public static final Club_Reporting_Acknowledgement1:I = 0x7f07027d

.field public static final Club_Reporting_Acknowledgement2:I = 0x7f07027e

.field public static final Club_Reporting_AddOptionalComment:I = 0x7f07027f

.field public static final Club_Reporting_Admin_AddInfo:I = 0x7f070280

.field public static final Club_Reporting_ClubAdmin:I = 0x7f070281

.field public static final Club_Reporting_EnforcementTeam:I = 0x7f070282

.field public static final Club_Reporting_Enforcement_AddInfo:I = 0x7f070283

.field public static final Club_Reporting_InappropriateChatMessage:I = 0x7f070284

.field public static final Club_Reporting_InappropriateComment:I = 0x7f070285

.field public static final Club_Reporting_InappropriateFeedItem:I = 0x7f070286

.field public static final Club_Reporting_ReasonInappropriateChatMessage:I = 0x7f070287

.field public static final Club_Reporting_ReasonInappropriateComment:I = 0x7f070288

.field public static final Club_Reporting_ReasonInappropriateFeedItem:I = 0x7f070289

.field public static final Club_Reporting_Report:I = 0x7f07028a

.field public static final Club_Reporting_ReportTo:I = 0x7f07028b

.field public static final Club_Reports_Title:I = 0x7f07028c

.field public static final Club_Reports_Title_Format:I = 0x7f07028d

.field public static final Club_Requested_Join:I = 0x7f07028e

.field public static final Club_Retire_Button_Title:I = 0x7f07028f

.field public static final Club_Roster_All_Count:I = 0x7f070290

.field public static final Club_Roster_Banned_Count:I = 0x7f070291

.field public static final Club_Roster_Everyone_Count:I = 0x7f070292

.field public static final Club_Roster_FilterBannedOnly:I = 0x7f070293

.field public static final Club_Roster_FilterEveryone:I = 0x7f070294

.field public static final Club_Roster_FilterMembersOnly:I = 0x7f070295

.field public static final Club_Roster_FilterModeratorsOnly:I = 0x7f070296

.field public static final Club_Roster_FilterNonmemberOnly:I = 0x7f070297

.field public static final Club_Roster_FilterOwnerOnly:I = 0x7f070298

.field public static final Club_Roster_FilterSearchResults:I = 0x7f070299

.field public static final Club_Roster_Heading_HereNow:I = 0x7f07029a

.field public static final Club_Roster_Heading_Last10:I = 0x7f07029b

.field public static final Club_Roster_Heading_Last30:I = 0x7f07029c

.field public static final Club_Roster_Heading_Members:I = 0x7f07029d

.field public static final Club_Roster_Heading_Today:I = 0x7f07029e

.field public static final Club_Roster_Invite:I = 0x7f07029f

.field public static final Club_Roster_Members_Count:I = 0x7f0702a0

.field public static final Club_Roster_Moderators_Count:I = 0x7f0702a1

.field public static final Club_Roster_Recommend:I = 0x7f0702a2

.field public static final Club_Roster_Search:I = 0x7f0702a3

.field public static final Club_Roster_SelectInvites:I = 0x7f0702a4

.field public static final Club_Roster_SelectRecommendations:I = 0x7f0702a5

.field public static final Club_Roster_Title:I = 0x7f0702a6

.field public static final Club_Roster_Too_Many:I = 0x7f0702a7

.field public static final Club_Roster_VoiceOver_Last10:I = 0x7f0702a8

.field public static final Club_Roster_VoiceOver_Last30:I = 0x7f0702a9

.field public static final Club_Search_Members:I = 0x7f0702aa

.field public static final Club_Search_Moderators:I = 0x7f0702ab

.field public static final Club_Search_Placeholder:I = 0x7f0702ac

.field public static final Club_Search_Title:I = 0x7f0702ad

.field public static final Club_SendInvite:I = 0x7f0702ae

.field public static final Club_Settings:I = 0x7f0702af

.field public static final Club_Settings_ActivityTitle:I = 0x7f0702b0

.field public static final Club_Settings_AskToJoin:I = 0x7f0702b1

.field public static final Club_Settings_Broadcasts:I = 0x7f0702b2

.field public static final Club_Settings_Broadcasts_ShowMature:I = 0x7f0702b3

.field public static final Club_Settings_Broadcasts_SpecificGames:I = 0x7f0702b4

.field public static final Club_Settings_ChatTitle:I = 0x7f0702b5

.field public static final Club_Settings_Chat_Description:I = 0x7f0702b6

.field public static final Club_Settings_Delete:I = 0x7f0702b7

.field public static final Club_Settings_EnableJoin:I = 0x7f0702b8

.field public static final Club_Settings_Failed_Keep_Club:I = 0x7f0702b9

.field public static final Club_Settings_HiddenClub_Details:I = 0x7f0702ba

.field public static final Club_Settings_Keep_Club:I = 0x7f0702bb

.field public static final Club_Settings_Lfg_Join_Disabled:I = 0x7f0702bc

.field public static final Club_Settings_Lfg_Join_Enabled:I = 0x7f0702bd

.field public static final Club_Settings_Lfg_Tournament_Join_Enabled:I = 0x7f0702be

.field public static final Club_Settings_MembershipTitle:I = 0x7f0702bf

.field public static final Club_Settings_MultiPlayerTitle:I = 0x7f0702c0

.field public static final Club_Settings_Others:I = 0x7f0702c1

.field public static final Club_Settings_PlayTitle:I = 0x7f0702c2

.field public static final Club_Settings_PrivateClub_Details:I = 0x7f0702c3

.field public static final Club_Settings_PublicClub_Details:I = 0x7f0702c4

.field public static final Club_Settings_Reset:I = 0x7f0702c5

.field public static final Club_Settings_Retire:I = 0x7f0702c6

.field public static final Club_Settings_Retire_Body:I = 0x7f0702c7

.field public static final Club_Settings_Retire_Ok:I = 0x7f0702c8

.field public static final Club_Settings_Retire_Title:I = 0x7f0702c9

.field public static final Club_Settings_Spinner_Moderators:I = 0x7f0702ca

.field public static final Club_Settings_Transfer:I = 0x7f0702cb

.field public static final Club_Settings_UpdateFailed_Body:I = 0x7f0702cc

.field public static final Club_Settings_UpdateFailed_Title:I = 0x7f0702cd

.field public static final Club_Settings_WhoCanChat:I = 0x7f0702ce

.field public static final Club_Settings_WhoCanInvite:I = 0x7f0702cf

.field public static final Club_Settings_WhoCanJoinGames:I = 0x7f0702d0

.field public static final Club_Settings_WhoCanMakeLfg:I = 0x7f0702d1

.field public static final Club_Settings_WhoCanPostToFeed:I = 0x7f0702d2

.field public static final Club_Settings_WhoCanSetTopic:I = 0x7f0702d3

.field public static final Club_ShareTo:I = 0x7f0702d4

.field public static final Club_SlotsAvailable:I = 0x7f0702d5

.field public static final Club_Suspension_ClubSuspended:I = 0x7f0702d6

.field public static final Club_Suspension_DeleteInADay:I = 0x7f0702d7

.field public static final Club_Suspension_DeleteInDaysFormat:I = 0x7f0702d8

.field public static final Club_Suspension_DeleteLessThanOneDay:I = 0x7f0702d9

.field public static final Club_Suspension_Generic:I = 0x7f0702da

.field public static final Club_Transfer_Confirm_Body:I = 0x7f0702db

.field public static final Club_Transfer_Confirm_Ok:I = 0x7f0702dc

.field public static final Club_Transfer_Confirm_Title:I = 0x7f0702dd

.field public static final Club_Transfer_Error_Body:I = 0x7f0702de

.field public static final Club_Transfer_Error_Title:I = 0x7f0702df

.field public static final Club_Transfer_NoModerators:I = 0x7f0702e0

.field public static final Club_Transfer_Success:I = 0x7f0702e1

.field public static final Club_Transfer_TransferButtonTitle:I = 0x7f0702e2

.field public static final Club_Unfollow:I = 0x7f0702e3

.field public static final Club_ViewJoinRequests:I = 0x7f0702e4

.field public static final Club_Watch:I = 0x7f0702e5

.field public static final Club_Watch_ErrorLoading:I = 0x7f0702e6

.field public static final Club_Watch_MatureWarning_Body:I = 0x7f0702e7

.field public static final Club_Watch_MatureWarning_Ok:I = 0x7f0702e8

.field public static final Club_Watch_MatureWarning_OkSticky:I = 0x7f0702e9

.field public static final Club_Watch_MatureWarning_Title:I = 0x7f0702ea

.field public static final Club_Watch_NoContent:I = 0x7f0702eb

.field public static final Clubs_Chat_ChangeMessageOfTheDay:I = 0x7f0702ec

.field public static final Clubs_Chat_ChangeMessageOfTheDay_Alternate:I = 0x7f0702ed

.field public static final Clubs_Chat_CharacterCount:I = 0x7f0702ee

.field public static final Clubs_Chat_DirectMention_Error:I = 0x7f0702ef

.field public static final Clubs_Chat_DirectMention_NoContent:I = 0x7f0702f0

.field public static final Clubs_Chat_DirectMention_Pivot_AllClubs:I = 0x7f0702f1

.field public static final Clubs_Chat_DirectMention_Pivot_ThisClub:I = 0x7f0702f2

.field public static final Clubs_Chat_DirectMention_Title:I = 0x7f0702f3

.field public static final Clubs_Chat_EditText_Hint:I = 0x7f0702f4

.field public static final Clubs_Chat_Error_CannotChat:I = 0x7f0702f5

.field public static final Clubs_Chat_Error_CannotSetTopic:I = 0x7f0702f6

.field public static final Clubs_Chat_Error_CannotViewChat:I = 0x7f0702f7

.field public static final Clubs_Chat_Error_Loading:I = 0x7f0702f8

.field public static final Clubs_Chat_Error_Refresh:I = 0x7f0702f9

.field public static final Clubs_Chat_Error_Role_Admins:I = 0x7f0702fa

.field public static final Clubs_Chat_Error_Role_Members:I = 0x7f0702fb

.field public static final Clubs_Chat_Error_Role_Moderators:I = 0x7f0702fc

.field public static final Clubs_Chat_Error_Role_Owner:I = 0x7f0702fd

.field public static final Clubs_Chat_Error_Role_Visitors:I = 0x7f0702fe

.field public static final Clubs_Chat_Joined_Channel:I = 0x7f0702ff

.field public static final Clubs_Chat_Left_Channel:I = 0x7f070300

.field public static final Clubs_Chat_MessageOfTheDay_Edit_Title:I = 0x7f070301

.field public static final Clubs_Chat_MessageOfTheDay_NotAllowed:I = 0x7f070302

.field public static final Clubs_Chat_MessageOfTheDay_Post:I = 0x7f070303

.field public static final Clubs_Chat_MessageOfTheDay_PostedBy:I = 0x7f070304

.field public static final Clubs_Chat_MessageOfTheDay_Report:I = 0x7f070305

.field public static final Clubs_Chat_MessageOfTheDay_Title:I = 0x7f070306

.field public static final Clubs_Chat_Message_Delete:I = 0x7f070307

.field public static final Clubs_Chat_Message_Report:I = 0x7f070308

.field public static final Clubs_Chat_Message_Role_Admin:I = 0x7f070309

.field public static final Clubs_Chat_Message_Role_Member:I = 0x7f07030a

.field public static final Clubs_Chat_Message_Role_Owner:I = 0x7f07030b

.field public static final Clubs_Chat_Message_Role_SeparatorFormat:I = 0x7f07030c

.field public static final Clubs_Chat_Message_Role_Visitor:I = 0x7f07030d

.field public static final Clubs_Chat_Message_Timestamp_NumDaysAgo:I = 0x7f07030e

.field public static final Clubs_Chat_Message_Timestamp_Yesterday:I = 0x7f07030f

.field public static final Clubs_Chat_More_People_Typing:I = 0x7f070310

.field public static final Clubs_Chat_Notification_DirectMention_Alternate_Android:I = 0x7f070311

.field public static final Clubs_Chat_Notification_DirectMention_Android:I = 0x7f070312

.field public static final Clubs_Chat_Notification_Message_Replacement:I = 0x7f070313

.field public static final Clubs_Chat_Notification_NewMessages_Alternate_Android:I = 0x7f070314

.field public static final Clubs_Chat_Notification_NewMessages_Android:I = 0x7f070315

.field public static final Clubs_Chat_Notification_SenderInClub_Android:I = 0x7f070316

.field public static final Clubs_Chat_Notification_Settings_AllMessages:I = 0x7f070317

.field public static final Clubs_Chat_Notification_Settings_ChatHeading:I = 0x7f070318

.field public static final Clubs_Chat_Notification_Settings_DeviceDefault:I = 0x7f070319

.field public static final Clubs_Chat_Notification_Settings_GoToSettings:I = 0x7f07031a

.field public static final Clubs_Chat_Notification_Settings_NoNotifications:I = 0x7f07031b

.field public static final Clubs_Chat_Notification_Settings_NotifyMeWhen:I = 0x7f07031c

.field public static final Clubs_Chat_Notification_Settings_OnlyForDirectMentions:I = 0x7f07031d

.field public static final Clubs_Chat_Notification_Settings_ShowContent:I = 0x7f07031e

.field public static final Clubs_Chat_Notification_Settings_Title:I = 0x7f07031f

.field public static final Clubs_Chat_One_Person_Typing:I = 0x7f070320

.field public static final Clubs_Chat_Reporting_Message:I = 0x7f070321

.field public static final Clubs_Chat_Reporting_MessageOfTheDay:I = 0x7f070322

.field public static final Clubs_Chat_Two_People_Typing:I = 0x7f070323

.field public static final Clubs_Chat_TypingIndicator_MorePeople:I = 0x7f070324

.field public static final Clubs_Chat_TypingIndicator_NoPeople:I = 0x7f070325

.field public static final Clubs_Chat_TypingIndicator_OnePerson:I = 0x7f070326

.field public static final Clubs_Chat_UnreadButton_MarkAsRead:I = 0x7f070327

.field public static final Clubs_Chat_UnreadButton_NewMessages:I = 0x7f070328

.field public static final Clubs_Chat_UnreadButton_NumNewMessage:I = 0x7f070329

.field public static final Clubs_Chat_UnreadButton_NumNewMessages:I = 0x7f07032a

.field public static final Clubs_Club:I = 0x7f07032b

.field public static final Clubs_Clubs:I = 0x7f07032c

.field public static final Clubs_Discovery:I = 0x7f07032d

.field public static final Clubs_Discovery_CreateClub:I = 0x7f07032e

.field public static final Clubs_Discovery_FindClub:I = 0x7f07032f

.field public static final Clubs_Discovery_MyClubs:I = 0x7f070330

.field public static final Clubs_Discovery_SearchByTags:I = 0x7f070331

.field public static final Clubs_Discovery_SearchCriteria:I = 0x7f070332

.field public static final Clubs_Discovery_SearchResults_Many:I = 0x7f070333

.field public static final Clubs_Discovery_SearchResults_One:I = 0x7f070334

.field public static final Clubs_Followers:I = 0x7f070335

.field public static final Clubs_Recommend:I = 0x7f070df7

.field public static final Clubs_Report_InappropriateClubBackgroundImage:I = 0x7f070336

.field public static final Clubs_Report_InappropriateClubBackgroundImage_Description:I = 0x7f070337

.field public static final Clubs_Report_InappropriateClubDescription:I = 0x7f070338

.field public static final Clubs_Report_InappropriateClubDescription_Description:I = 0x7f070339

.field public static final Clubs_Report_InappropriateClubName:I = 0x7f07033a

.field public static final Clubs_Report_InappropriateClubName_Description:I = 0x7f07033b

.field public static final Clubs_Report_InappropriateClubProfileImage:I = 0x7f07033c

.field public static final Clubs_Report_InappropriateClubProfileImage_Description:I = 0x7f07033d

.field public static final Clubs_Search_Choose_Recent_Game:I = 0x7f070df8

.field public static final Clubs_Search_Choose_Tag:I = 0x7f070df9

.field public static final Clubs_YourClubs:I = 0x7f07033e

.field public static final Comment_Failure_Body:I = 0x7f07033f

.field public static final Comment_TextEntryWatermark:I = 0x7f070340

.field public static final Comments_Filter_Header:I = 0x7f070341

.field public static final Comments_List_NoData:I = 0x7f070342

.field public static final Companion_Exit_Description:I = 0x7f070343

.field public static final Companion_Exit_Title:I = 0x7f070344

.field public static final CompareGame_AchievementItem_LockedWithPercentage_Phone:I = 0x7f070345

.field public static final CompareGame_AchievementItem_LockedWithPercentage_Tablet:I = 0x7f070346

.field public static final CompareGame_AchievementItem_Locked_Tablet:I = 0x7f070347

.field public static final CompareGame_AchievementItem_UnlockedWithTime_Tablet:I = 0x7f070348

.field public static final CompareGame_NoAchievementsAvailable:I = 0x7f070349

.field public static final CompareGame_PageTitle:I = 0x7f07034a

.field public static final CompareGame_TitleStat_GameProgress:I = 0x7f07034b

.field public static final CompareGame_TitleStat_TimePlayed:I = 0x7f07034c

.field public static final CompareGame_YouGamertagDisplayText:I = 0x7f07034d

.field public static final ConnectDialog_ApplyPin:I = 0x7f07034e

.field public static final ConnectDialog_Connect:I = 0x7f07034f

.field public static final ConnectDialog_ConnectAutomatically:I = 0x7f070350

.field public static final ConnectDialog_Connect_Cap:I = 0x7f070351

.field public static final ConnectDialog_Connect_Tablet:I = 0x7f070352

.field public static final ConnectDialog_ConsoleNotListed:I = 0x7f070353

.field public static final ConnectDialog_ConsolesOnYourNetwork:I = 0x7f070354

.field public static final ConnectDialog_ContinueAsGuest:I = 0x7f070355

.field public static final ConnectDialog_Disconnect:I = 0x7f070356

.field public static final ConnectDialog_EnterIPAddress:I = 0x7f070357

.field public static final ConnectDialog_EnterIPAddressGhostText:I = 0x7f070358

.field public static final ConnectDialog_EnterIPAddressTitle:I = 0x7f070359

.field public static final ConnectDialog_EnterPin:I = 0x7f07035a

.field public static final ConnectDialog_IPAddressLocationOnConsole:I = 0x7f07035b

.field public static final ConnectDialog_LearnMore:I = 0x7f07035c

.field public static final ConnectDialog_LinktoFAQ:I = 0x7f07035d

.field public static final ConnectDialog_NoConsolesFound:I = 0x7f07035e

.field public static final ConnectDialog_NotSignedInOnConsolePrompt:I = 0x7f07035f

.field public static final ConnectDialog_NotSignedInOnConsoleWarning:I = 0x7f070360

.field public static final ConnectDialog_PairingErrorMessage:I = 0x7f070361

.field public static final ConnectDialog_PowerOffFailure:I = 0x7f070362

.field public static final ConnectDialog_PowerOnFailure:I = 0x7f070363

.field public static final ConnectDialog_PoweringOff:I = 0x7f070364

.field public static final ConnectDialog_PoweringOn:I = 0x7f070365

.field public static final ConnectDialog_SignInOnConsole:I = 0x7f070366

.field public static final ConnectDialog_TryAgain:I = 0x7f070367

.field public static final ConnectDialog_TurnOff:I = 0x7f070368

.field public static final ConnectDialog_TurnOn:I = 0x7f070369

.field public static final ConnectDialog_XboxApp_NoConsolesFound:I = 0x7f07036a

.field public static final Connect_Reconnecting:I = 0x7f07036b

.field public static final Connection_DeviceTime_Incorrect:I = 0x7f07036c

.field public static final Connection_Wifi_Needed_Error:I = 0x7f07036d

.field public static final Connection_XboxApp_Wifi_Needed_Error:I = 0x7f07036e

.field public static final ConsoleBar_ConnectedToConsole:I = 0x7f07036f

.field public static final ConsoleBar_NowPlaying_SubTitle:I = 0x7f070370

.field public static final ConsoleDiscovery_Enter_IP_Hint:I = 0x7f070371

.field public static final ConsoleDiscovery_Find_IP_HowTo:I = 0x7f070372

.field public static final ConsoleDiscovery_Searching:I = 0x7f070373

.field public static final Console_Connection_Autoconnect:I = 0x7f070374

.field public static final Console_Connection_Connect_Button_Txt:I = 0x7f070375

.field public static final Console_Connection_Connecting:I = 0x7f070376

.field public static final Console_Connection_Disconnect:I = 0x7f070377

.field public static final Console_Connection_ForgetConsole:I = 0x7f070378

.field public static final Console_Connection_Tab_Title:I = 0x7f070379

.field public static final Console_Connection_Title:I = 0x7f07037a

.field public static final Console_Connection_TurnOff:I = 0x7f07037b

.field public static final Console_Management_Page_Description:I = 0x7f07037c

.field public static final Console_Management_Page_Title:I = 0x7f07037d

.field public static final Console_Management_Switch_Console_Button_Text:I = 0x7f07037e

.field public static final Console_Management_Switch_Console_Warning_Description:I = 0x7f07037f

.field public static final Console_Management_Switch_Console_Warning_Title:I = 0x7f070380

.field public static final Consoles_Connection_Tab_Title:I = 0x7f070381

.field public static final Contacts_Permission_Denied:I = 0x7f070382

.field public static final Contacts_Permission_Denied_Header:I = 0x7f070383

.field public static final Content_Explicit:I = 0x7f070384

.field public static final ControllerMode_Guide:I = 0x7f070385

.field public static final Controller_Exit:I = 0x7f070386

.field public static final Controller_HelpNavigate:I = 0x7f070387

.field public static final Controller_HelpNavigateSwipe:I = 0x7f070388

.field public static final Controller_HelpNavigateTap:I = 0x7f070389

.field public static final Controller_HelpNavigate_FirstLine:I = 0x7f07038a

.field public static final Controller_HelpNavigate_SecondLine:I = 0x7f07038b

.field public static final Controller_HelpSelect:I = 0x7f07038c

.field public static final Conversation_DeleteConfirmation_Body:I = 0x7f07038d

.field public static final Conversation_DeleteConfirmation_Header:I = 0x7f07038e

.field public static final Conversation_Delete_MenuText:I = 0x7f07038f

.field public static final Conversation_Reply_Failure:I = 0x7f070390

.field public static final Crews_Header:I = 0x7f070391

.field public static final Current_release_bug_fixes:I = 0x7f070392

.field public static final Current_release_caveat:I = 0x7f070393

.field public static final Current_release_title:I = 0x7f070394

.field public static final CustomizeProfile_AddBio:I = 0x7f070395

.field public static final CustomizeProfile_AddLocation:I = 0x7f070396

.field public static final CustomizeProfile_AddRealName:I = 0x7f070397

.field public static final CustomizeProfile_ChangeColor:I = 0x7f070398

.field public static final CustomizeProfile_ChangeGamerpic:I = 0x7f070399

.field public static final CustomizeProfile_ChangeGamertag:I = 0x7f07039a

.field public static final CustomizeProfile_ChooseColor:I = 0x7f07039b

.field public static final CustomizeProfile_ChooseGamerpic:I = 0x7f07039c

.field public static final CustomizeProfile_ChooseGamertag:I = 0x7f07039d

.field public static final CustomizeProfile_Color:I = 0x7f07039e

.field public static final CustomizeProfile_ColorLoadError:I = 0x7f07039f

.field public static final CustomizeProfile_EditAvatar:I = 0x7f0703a0

.field public static final CustomizeProfile_Gamerpic:I = 0x7f0703a1

.field public static final CustomizeProfile_GamerpicLoadError:I = 0x7f0703a2

.field public static final CustomizeProfile_GamertagAvailable:I = 0x7f0703a3

.field public static final CustomizeProfile_GamertagCheckAvailability:I = 0x7f0703a4

.field public static final CustomizeProfile_GamertagCheckAvailabilityError:I = 0x7f0703a5

.field public static final CustomizeProfile_GamertagClaim:I = 0x7f0703a6

.field public static final CustomizeProfile_GamertagError:I = 0x7f0703a7

.field public static final CustomizeProfile_GamertagHint:I = 0x7f0703a8

.field public static final CustomizeProfile_GamertagNoFreeChange:I = 0x7f0703a9

.field public static final CustomizeProfile_GamertagNotAvailable:I = 0x7f0703aa

.field public static final CustomizeProfile_GamertagPolicy1:I = 0x7f0703ab

.field public static final CustomizeProfile_GamertagPolicy2:I = 0x7f0703ac

.field public static final CustomizeProfile_GamertagSuggestions:I = 0x7f0703ad

.field public static final CustomizeProfile_GamertagXboxLink:I = 0x7f0703ae

.field public static final CustomizeProfile_SwitchToAvatar:I = 0x7f0703af

.field public static final CustomizeProfile_SwitchToGamerPic:I = 0x7f0703b0

.field public static final Customize_Gamerpic_Choose_Custom:I = 0x7f0703b1

.field public static final Customize_Gamerpic_Choose_From_Photos:I = 0x7f0703b2

.field public static final Customize_Gamerpic_Custom_Access_Camera_Body:I = 0x7f0703b3

.field public static final Customize_Gamerpic_Custom_Access_Camera_Header:I = 0x7f0703b4

.field public static final Customize_Gamerpic_Custom_Access_Photos_Body:I = 0x7f0703b5

.field public static final Customize_Gamerpic_Custom_Access_Photos_Header:I = 0x7f0703b6

.field public static final Customize_Gamerpic_Take_A_Photo:I = 0x7f0703b7

.field public static final DETAILS_ADDITIONAL:I = 0x7f0703b8

.field public static final DETAILS_ADDTOMYSHOWS:I = 0x7f0703b9

.field public static final DETAILS_ADDTOPLAYLIST:I = 0x7f0703ba

.field public static final DETAILS_ALBUM_TITLE:I = 0x7f0703bb

.field public static final DETAILS_ALLOWEDPROVIDERS:I = 0x7f0703bc

.field public static final DETAILS_AWARDS:I = 0x7f0703bd

.field public static final DETAILS_COOP:I = 0x7f0703be

.field public static final DETAILS_DESCRIPTION:I = 0x7f0703bf

.field public static final DETAILS_DEVELOPER:I = 0x7f0703c0

.field public static final DETAILS_DIRECTOR:I = 0x7f0703c1

.field public static final DETAILS_DURATION:I = 0x7f0703c2

.field public static final DETAILS_EXPLICIT_CONTENT:I = 0x7f0703c3

.field public static final DETAILS_GAMEOVERVIEW_NONE:I = 0x7f0703c4

.field public static final DETAILS_GAMEOVERVIEW_OFFLINE:I = 0x7f0703c5

.field public static final DETAILS_GAMEOVERVIEW_ONLINE:I = 0x7f0703c6

.field public static final DETAILS_GAMEOVERVIEW_ONLINE_OFFLINE:I = 0x7f0703c7

.field public static final DETAILS_GENRES:I = 0x7f0703c8

.field public static final DETAILS_LABEL:I = 0x7f0703c9

.field public static final DETAILS_MULTIPLAYER:I = 0x7f0703ca

.field public static final DETAILS_NETWORKS:I = 0x7f0703cb

.field public static final DETAILS_PUBLISHER:I = 0x7f0703cc

.field public static final DETAILS_RATING:I = 0x7f0703cd

.field public static final DETAILS_RELEASEDATE:I = 0x7f0703ce

.field public static final DETAILS_RESOLUTION:I = 0x7f0703cf

.field public static final DETAILS_RESOLUTION_HD:I = 0x7f0703d0

.field public static final DETAILS_RESOLUTION_HDANDSD:I = 0x7f0703d1

.field public static final DETAILS_RESOLUTION_HD_BRACKETED:I = 0x7f0703d2

.field public static final DETAILS_RESOLUTION_SD:I = 0x7f0703d3

.field public static final DETAILS_RESOLUTION_SD_BRACKETED:I = 0x7f0703d4

.field public static final DETAILS_STARRING:I = 0x7f0703d5

.field public static final DETAILS_STUDIOS:I = 0x7f0703d6

.field public static final DETAILS_TV_MULTIPLE_SEASON_FORMAT_OTHERS:I = 0x7f0703d7

.field public static final DETAILS_TV_SEASON_ONLY_FORMAT:I = 0x7f0703d8

.field public static final DeepLink_Return_Minecraft:I = 0x7f0703d9

.field public static final DeepLink_Return_ToGame:I = 0x7f0703da

.field public static final Delete_Confirmation_Success:I = 0x7f0703db

.field public static final Delete_Failure_Body:I = 0x7f0703dc

.field public static final Delete_Failure_Header:I = 0x7f0703dd

.field public static final Detail_Movie_Title:I = 0x7f0703de

.field public static final Detail_Series_Title:I = 0x7f0703df

.field public static final Details_AddOn_Phone:I = 0x7f0703e0

.field public static final Details_AddOn_Tablet:I = 0x7f0703e1

.field public static final Details_Albums_Phone:I = 0x7f0703e2

.field public static final Details_Albums_Tablet:I = 0x7f0703e3

.field public static final Details_Biography_Phone:I = 0x7f0703e4

.field public static final Details_Bundles_Phone:I = 0x7f0703e5

.field public static final Details_Bundles_Tablet:I = 0x7f0703e6

.field public static final Details_CastAndCrew_Phone:I = 0x7f0703e7

.field public static final Details_CastAndCrew_Tablet:I = 0x7f0703e8

.field public static final Details_Coming_Soon:I = 0x7f0703e9

.field public static final Details_CompatibleWith_Phone:I = 0x7f0703ea

.field public static final Details_CompatibleWith_Tablet:I = 0x7f0703eb

.field public static final Details_Episodes_Phone:I = 0x7f0703ec

.field public static final Details_Episodes_Tablet:I = 0x7f0703ed

.field public static final Details_Explicit:I = 0x7f0703ee

.field public static final Details_Extras_Phone:I = 0x7f0703ef

.field public static final Details_Extras_Tablet:I = 0x7f0703f0

.field public static final Details_Gallery_Phone:I = 0x7f0703f1

.field public static final Details_Gallery_Tablet:I = 0x7f0703f2

.field public static final Details_GameAchievementProgress_Tablet:I = 0x7f0703f3

.field public static final Details_GameChallenges_Tablet:I = 0x7f0703f4

.field public static final Details_GameHelp:I = 0x7f0703f5

.field public static final Details_GameProgress_AchievementProgress_Phone:I = 0x7f0703f6

.field public static final Details_GameProgress_Challenges_Phone:I = 0x7f0703f7

.field public static final Details_GameProgress_Channel_Phone:I = 0x7f0703f8

.field public static final Details_GameProgress_Channel_Tablet:I = 0x7f0703f9

.field public static final Details_GameProgress_Phone:I = 0x7f0703fa

.field public static final Details_Genre:I = 0x7f0703fb

.field public static final Details_GetOnXboxOne:I = 0x7f0703fc

.field public static final Details_Header_Phone:I = 0x7f0703fd

.field public static final Details_Header_Tablet:I = 0x7f0703fe

.field public static final Details_Help:I = 0x7f0703ff

.field public static final Details_IncludedContent_Phone:I = 0x7f070400

.field public static final Details_IncludedContent_Tablet:I = 0x7f070401

.field public static final Details_Languages:I = 0x7f070402

.field public static final Details_MediaType_Activity:I = 0x7f070403

.field public static final Details_MediaType_Activity_Tablet:I = 0x7f070404

.field public static final Details_MediaType_Album:I = 0x7f070405

.field public static final Details_MediaType_Album_Tablet:I = 0x7f070406

.field public static final Details_MediaType_App:I = 0x7f070407

.field public static final Details_MediaType_App_Tablet:I = 0x7f070408

.field public static final Details_MediaType_Artist:I = 0x7f070409

.field public static final Details_MediaType_Artist_Tablet:I = 0x7f07040a

.field public static final Details_MediaType_DLC:I = 0x7f07040b

.field public static final Details_MediaType_DLC_Tablet:I = 0x7f07040c

.field public static final Details_MediaType_Episode:I = 0x7f07040d

.field public static final Details_MediaType_Episode_Tablet:I = 0x7f07040e

.field public static final Details_MediaType_Extras:I = 0x7f07040f

.field public static final Details_MediaType_Extras_Tablet:I = 0x7f070410

.field public static final Details_MediaType_Game:I = 0x7f070411

.field public static final Details_MediaType_Game_Tablet:I = 0x7f070412

.field public static final Details_MediaType_Movie:I = 0x7f070413

.field public static final Details_MediaType_Movie_Tablet:I = 0x7f070414

.field public static final Details_MediaType_Season:I = 0x7f070415

.field public static final Details_MediaType_Season_Tablet:I = 0x7f070416

.field public static final Details_MediaType_Series:I = 0x7f070417

.field public static final Details_MediaType_Series_Tablet:I = 0x7f070418

.field public static final Details_MediaType_Show:I = 0x7f070419

.field public static final Details_MediaType_Show_Tablet:I = 0x7f07041a

.field public static final Details_MediaType_TV:I = 0x7f07041b

.field public static final Details_MediaType_TV_Tablet:I = 0x7f07041c

.field public static final Details_MediaType_Track:I = 0x7f07041d

.field public static final Details_MediaType_Track_Tablet:I = 0x7f07041e

.field public static final Details_Overview_Phone:I = 0x7f07041f

.field public static final Details_Parent_Title:I = 0x7f070420

.field public static final Details_Providers:I = 0x7f070421

.field public static final Details_Related_Phone:I = 0x7f070422

.field public static final Details_Related_Tablet:I = 0x7f070423

.field public static final Details_Released:I = 0x7f070424

.field public static final Details_SeasonPicker_AllOption:I = 0x7f070425

.field public static final Details_ShowLess:I = 0x7f070426

.field public static final Details_ShowMore:I = 0x7f070427

.field public static final Details_SmartGlassCompanion:I = 0x7f070428

.field public static final Details_Title_ParentalRating:I = 0x7f070429

.field public static final Details_Title_ParentalRatingColon:I = 0x7f07042a

.field public static final Details_Title_Studios:I = 0x7f07042b

.field public static final Details_TopSongs_Phone:I = 0x7f07042c

.field public static final Details_TopSongs_Tablet:I = 0x7f07042d

.field public static final Details_TrackList_Phone:I = 0x7f07042e

.field public static final Details_TrackList_Tablet:I = 0x7f07042f

.field public static final Details_XboxApp_MediaType_Activity:I = 0x7f070430

.field public static final Details_XboxApp_MediaType_Activity_Tablet:I = 0x7f070431

.field public static final Details_XboxApp_SmartGlassCompanion:I = 0x7f070432

.field public static final DeveloperMode_ExitDeveloperMode:I = 0x7f070433

.field public static final DeveloperMode_SwitchToDeveloperMode:I = 0x7f070434

.field public static final Dialog_Attention_Title:I = 0x7f070435

.field public static final Directors_Header:I = 0x7f070436

.field public static final Drawer_Achievements:I = 0x7f070437

.field public static final Drawer_ActivityFeed:I = 0x7f070438

.field public static final Drawer_Captures:I = 0x7f070439

.field public static final Drawer_Connection:I = 0x7f07043a

.field public static final Drawer_DVR:I = 0x7f070dfa

.field public static final Drawer_Featured:I = 0x7f07043b

.field public static final Drawer_Friends:I = 0x7f07043c

.field public static final Drawer_GameClips:I = 0x7f07043d

.field public static final Drawer_Games_Store:I = 0x7f07043e

.field public static final Drawer_Games_Store_MS:I = 0x7f07043f

.field public static final Drawer_Home:I = 0x7f070440

.field public static final Drawer_Messages:I = 0x7f070441

.field public static final Drawer_Pins:I = 0x7f070442

.field public static final Drawer_Search:I = 0x7f070443

.field public static final Drawer_Settings:I = 0x7f070444

.field public static final Drawer_TV:I = 0x7f070445

.field public static final Drawer_Trending:I = 0x7f070446

.field public static final EPG_AddFavoriteChannel_GeneralError:I = 0x7f070447

.field public static final EPG_AddFavoriteChannel_ListFullError:I = 0x7f070448

.field public static final EPG_AddFavoriteShow_GeneralError:I = 0x7f070449

.field public static final EPG_AddedFavoriteChannel_Message_Text:I = 0x7f07044a

.field public static final EPG_AppChannels_AppsHeader:I = 0x7f07044b

.field public static final EPG_AppChannels_NoShows:I = 0x7f07044c

.field public static final EPG_Cannot_Find_Tuner:I = 0x7f07044d

.field public static final EPG_ChannelDialog_Description:I = 0x7f07044e

.field public static final EPG_ConnectionError_AppChannels:I = 0x7f07044f

.field public static final EPG_ConnectionError_Favorites:I = 0x7f070450

.field public static final EPG_ConnectionError_Favorites_Add:I = 0x7f070451

.field public static final EPG_ConnectionError_Favorites_Remove:I = 0x7f070452

.field public static final EPG_ConnectionError_RecentChannels:I = 0x7f070453

.field public static final EPG_ConnectionError_Remote:I = 0x7f070454

.field public static final EPG_ConnectionError_Tune:I = 0x7f070455

.field public static final EPG_ConnectionError_TvListings:I = 0x7f070456

.field public static final EPG_Disconnected:I = 0x7f070457

.field public static final EPG_Error_AppChannels_Channels:I = 0x7f070458

.field public static final EPG_Error_AppChannels_NoShowInfo:I = 0x7f070459

.field public static final EPG_Error_AppChannels_Shows_Generic:I = 0x7f07045a

.field public static final EPG_Error_Code:I = 0x7f07045b

.field public static final EPG_Error_Console_Setup_Directions_STB:I = 0x7f07045c

.field public static final EPG_Error_Favorites_ChannelLimit:I = 0x7f07045d

.field public static final EPG_Error_Remote_Console_Setup:I = 0x7f07045e

.field public static final EPG_Error_TryAgain:I = 0x7f07045f

.field public static final EPG_Error_TvListings_Channels:I = 0x7f070460

.field public static final EPG_Error_TvListings_Settings:I = 0x7f070461

.field public static final EPG_Favorites_Add:I = 0x7f070462

.field public static final EPG_Favorites_AppChannels_NoFavorites:I = 0x7f070463

.field public static final EPG_Favorites_Remove:I = 0x7f070464

.field public static final EPG_GeneralError:I = 0x7f070465

.field public static final EPG_GoldGating_Get_Gold_Message:I = 0x7f070466

.field public static final EPG_GoldGating_Got_Gold_Question:I = 0x7f070467

.field public static final EPG_GoldGating_Header:I = 0x7f070468

.field public static final EPG_HD:I = 0x7f070469

.field public static final EPG_Headers_AppChannels:I = 0x7f07046a

.field public static final EPG_Headers_Favorites:I = 0x7f07046b

.field public static final EPG_Headers_RecentChannels:I = 0x7f07046c

.field public static final EPG_Headers_TvListings:I = 0x7f07046d

.field public static final EPG_LoadingMessage_AppChannels:I = 0x7f07046e

.field public static final EPG_LoadingMessage_Favorites:I = 0x7f07046f

.field public static final EPG_LoadingMessage_Recents:I = 0x7f070470

.field public static final EPG_LoadingMessage_TVListings:I = 0x7f070471

.field public static final EPG_LoadingMessage_TakeSomeTime:I = 0x7f070472

.field public static final EPG_MiniDetails_ChannelNumber:I = 0x7f070473

.field public static final EPG_Multi_Headend_All_Providers:I = 0x7f070474

.field public static final EPG_Multi_Headend_Choose_a_Different_Provider:I = 0x7f070475

.field public static final EPG_Multi_Headend_Choose_a_Provider:I = 0x7f070476

.field public static final EPG_Multi_Headend_Connection_Error:I = 0x7f070477

.field public static final EPG_Multi_Headend_General_Error:I = 0x7f070478

.field public static final EPG_Multi_Headend_Providers:I = 0x7f070479

.field public static final EPG_Multi_Headend_Select_a_Provider:I = 0x7f07047a

.field public static final EPG_MyShows_MovieHeader:I = 0x7f07047b

.field public static final EPG_MyShows_TvHeader:I = 0x7f07047c

.field public static final EPG_No_Info_Available:I = 0x7f07047d

.field public static final EPG_OOBE_MESSAGE_TEXT:I = 0x7f07047e

.field public static final EPG_Page_Title:I = 0x7f07047f

.field public static final EPG_ParentalControls_BlockedDetails:I = 0x7f070480

.field public static final EPG_ParentalControls_BlockedTitle:I = 0x7f070481

.field public static final EPG_PopularOnDemand_Movies:I = 0x7f070482

.field public static final EPG_PopularOnDemand_TV:I = 0x7f070483

.field public static final EPG_RemoveFavoriteChannel_GeneralError:I = 0x7f070484

.field public static final EPG_RemoveFavoriteShow_GeneralError:I = 0x7f070485

.field public static final EPG_SD:I = 0x7f070486

.field public static final EPG_Settings_Filter_All:I = 0x7f070487

.field public static final EPG_Settings_Filter_HD:I = 0x7f070488

.field public static final EPG_Settings_Filter_HideSD:I = 0x7f070489

.field public static final EPG_Settings_Filter_SectionHeader:I = 0x7f07048a

.field public static final EPG_Settings_Load:I = 0x7f07048b

.field public static final EPG_Streaming_Description:I = 0x7f07048c

.field public static final EPG_Streaming_Error_Not_Supported:I = 0x7f07048d

.field public static final EPG_Streaming_Error_Unknown:I = 0x7f07048e

.field public static final EPG_Today:I = 0x7f07048f

.field public static final EPG_Unavailable:I = 0x7f070490

.field public static final EXTRAS_HEADER:I = 0x7f070491

.field public static final EmptyState_MyShows_MoviesText:I = 0x7f070492

.field public static final EmptyState_MyShows_TVText:I = 0x7f070493

.field public static final Enforcement_Access_Club_Action:I = 0x7f070494

.field public static final Enforcement_Access_Club_Chat_Action:I = 0x7f070495

.field public static final Enforcement_Access_Club_Feed_And_Comment_Action:I = 0x7f070496

.field public static final Enforcement_Access_GameDVR_Action:I = 0x7f070497

.field public static final Enforcement_Add_Friend_Action:I = 0x7f070498

.field public static final Enforcement_Broadcast_Action:I = 0x7f070499

.field public static final Enforcement_Change_Club_Chat_MOTD_Action:I = 0x7f07049a

.field public static final Enforcement_Change_Group_Message_Topic_Action:I = 0x7f07049b

.field public static final Enforcement_Comment_On_Activity_Feed_Action:I = 0x7f07049c

.field public static final Enforcement_Create_Or_Join_LFG_Action:I = 0x7f07049d

.field public static final Enforcement_Delete_Club_Chat_Message_Action:I = 0x7f07049e

.field public static final Enforcement_Edit_Bio_Action:I = 0x7f07049f

.field public static final Enforcement_Edit_Club_Emblem_Action:I = 0x7f0704a0

.field public static final Enforcement_Edit_Location_Action:I = 0x7f0704a1

.field public static final Enforcement_Join_Party_Action:I = 0x7f0704a2

.field public static final Enforcement_Message_Format:I = 0x7f0704a3

.field public static final Enforcement_Message_Header:I = 0x7f0704a4

.field public static final Enforcement_Participate_Multiplayer_Minecraft_Action:I = 0x7f0704a5

.field public static final Enforcement_Purchase_Action:I = 0x7f0704a6

.field public static final Enforcement_Reply_Message_Action:I = 0x7f0704a7

.field public static final Enforcement_Send_Club_Chat_Message_Action:I = 0x7f0704a8

.field public static final Enforcement_Send_Message_Action:I = 0x7f0704a9

.field public static final Enforcement_Send_Message_From_Activity_Feed_Action:I = 0x7f0704aa

.field public static final Enforcement_Send_Message_From_Profile_Action:I = 0x7f0704ab

.field public static final Enforcement_Share_And_Comment_Action:I = 0x7f0704ac

.field public static final Enforcement_Share_Content_Action:I = 0x7f0704ad

.field public static final Enforcement_Share_To_Social_Network_Action:I = 0x7f0704ae

.field public static final Enforcement_String_Vet_Gameclip_Name_Action:I = 0x7f0704af

.field public static final Enforcement_String_Vet_Location_Action:I = 0x7f0704b0

.field public static final Enforcement_String_Vet_Screenshot_Name_Action:I = 0x7f0704b1

.field public static final Enforcement_Upload_Gameclip_Action:I = 0x7f0704b2

.field public static final Enforcement_Upload_Kinect_Content_Action:I = 0x7f0704b3

.field public static final Enforcement_Upload_Screenshot_Action:I = 0x7f0704b4

.field public static final Enforcement_View_Profile_Action:I = 0x7f0704b5

.field public static final EpgDetails_Showtime:I = 0x7f0704b6

.field public static final EpgSchedule_GeneralError:I = 0x7f0704b7

.field public static final EpgSchedule_Loading:I = 0x7f0704b8

.field public static final EpgSchedule_NetworkError:I = 0x7f0704b9

.field public static final EpgSchedule_Unavailable:I = 0x7f0704ba

.field public static final Epg_Details:I = 0x7f0704bb

.field public static final Epg_NMinutesText:I = 0x7f0704bc

.field public static final Epg_NowButton_Text:I = 0x7f0704bd

.field public static final Epg_OneHour_Text:I = 0x7f0704be

.field public static final Epg_Retry:I = 0x7f0704bf

.field public static final Epg_TuneToChannel:I = 0x7f0704c0

.field public static final Epgl_NHoursText:I = 0x7f0704c1

.field public static final ErrorState_NetworkButton:I = 0x7f0704c2

.field public static final ErrorState_PageError:I = 0x7f0704c3

.field public static final ErrorState_PaneError:I = 0x7f0704c4

.field public static final ErrorState_RetryButton:I = 0x7f0704c5

.field public static final Error_CanNotOpenScreenshot:I = 0x7f0704c6

.field public static final Error_CanNotPlayVideo:I = 0x7f0704c7

.field public static final FORWARD_SLASH_SEPARATOR:I = 0x7f0704c8

.field public static final FamilyPasscode_0:I = 0x7f0704c9

.field public static final FamilyPasscode_1:I = 0x7f0704ca

.field public static final FamilyPasscode_2:I = 0x7f0704cb

.field public static final FamilyPasscode_3:I = 0x7f0704cc

.field public static final FamilyPasscode_4:I = 0x7f0704cd

.field public static final FamilyPasscode_5:I = 0x7f0704ce

.field public static final FamilyPasscode_6:I = 0x7f0704cf

.field public static final FamilyPasscode_7:I = 0x7f0704d0

.field public static final FamilyPasscode_8:I = 0x7f0704d1

.field public static final FamilyPasscode_9:I = 0x7f0704d2

.field public static final FamilyPasscode_Cancel:I = 0x7f0704d3

.field public static final FamilyPasscode_Header:I = 0x7f0704d4

.field public static final Featured_Landing_PageTitle:I = 0x7f0704d5

.field public static final FeedbackDialog_CableBoxText:I = 0x7f0704d6

.field public static final FeedbackDialog_CableTVProviderText:I = 0x7f0704d7

.field public static final FeedbackDialog_CategoryText:I = 0x7f0704d8

.field public static final FeedbackDialog_ChooseOneText:I = 0x7f0704d9

.field public static final FeedbackDialog_ConfirmationHeaderText:I = 0x7f0704da

.field public static final FeedbackDialog_ConfirmationText:I = 0x7f0704db

.field public static final FeedbackDialog_CrashBodyText:I = 0x7f0704dc

.field public static final FeedbackDialog_CrashSubject:I = 0x7f0704dd

.field public static final FeedbackDialog_CrashTitleText:I = 0x7f0704de

.field public static final FeedbackDialog_CurrentCultureText:I = 0x7f0704df

.field public static final FeedbackDialog_FeedbackBodyText:I = 0x7f0704e0

.field public static final FeedbackDialog_FeedbackSubject:I = 0x7f0704e1

.field public static final FeedbackDialog_FeedbackTitleText:I = 0x7f0704e2

.field public static final FeedbackDialog_GamerTagText:I = 0x7f0704e3

.field public static final FeedbackDialog_HarwareManufacturerText:I = 0x7f0704e4

.field public static final FeedbackDialog_HarwareModelText:I = 0x7f0704e5

.field public static final FeedbackDialog_LayoutOrientationText:I = 0x7f0704e6

.field public static final FeedbackDialog_LogFileDataText:I = 0x7f0704e7

.field public static final FeedbackDialog_MSFT_Alias:I = 0x7f070dfb

.field public static final FeedbackDialog_MailBodyStep1Text:I = 0x7f0704e8

.field public static final FeedbackDialog_NoThanksButtonText:I = 0x7f0704e9

.field public static final FeedbackDialog_OSVersionText:I = 0x7f0704ea

.field public static final FeedbackDialog_PrivacyStatement:I = 0x7f0704eb

.field public static final FeedbackDialog_ProcessorArchitectureText:I = 0x7f0704ec

.field public static final FeedbackDialog_RateText:I = 0x7f0704ed

.field public static final FeedbackDialog_ReportProblemText:I = 0x7f0704ee

.field public static final FeedbackDialog_SGVersionText:I = 0x7f0704ef

.field public static final FeedbackDialog_ScreenshotText:I = 0x7f0704f0

.field public static final FeedbackDialog_SuggestionText:I = 0x7f0704f1

.field public static final FeedbackDialog_XboxApp_ConfirmationText:I = 0x7f0704f2

.field public static final FeedbackDialog_XboxApp_CrashSubject:I = 0x7f0704f3

.field public static final FeedbackDialog_XboxApp_FeedbackBodyText:I = 0x7f0704f4

.field public static final FeedbackDialog_XboxApp_FeedbackSubject:I = 0x7f0704f5

.field public static final FeedbackDialog_XboxApp_SGVersionText:I = 0x7f0704f6

.field public static final FeedbackDialog_ZipCodeText:I = 0x7f0704f7

.field public static final FeedbackForm_CrashTellUsText:I = 0x7f0704f8

.field public static final FeedbackForm_Frowny:I = 0x7f070dfc

.field public static final FeedbackForm_RateExperienceText:I = 0x7f070dfd

.field public static final FeedbackForm_Smiley:I = 0x7f070dfe

.field public static final FeedbackForm_TellUsText:I = 0x7f070dff

.field public static final FeedbackForm_Title:I = 0x7f070e00

.field public static final FirstRun_AppChannels_BtnText:I = 0x7f0704f9

.field public static final FirstRun_AppChannels_Header:I = 0x7f0704fa

.field public static final FirstRun_AppChannels_Text:I = 0x7f0704fb

.field public static final FirstRun_Favorites_AppChannels_Header:I = 0x7f0704fc

.field public static final FirstRun_Favorites_AppChannels_TextIcon:I = 0x7f0704fd

.field public static final FirstRun_Favorites_AppChannels_Text_First:I = 0x7f0704fe

.field public static final FirstRun_Favorites_AppChannels_Text_Second:I = 0x7f0704ff

.field public static final FirstRun_Favorites_Channel_TextIcon:I = 0x7f070500

.field public static final FirstRun_Favorites_Channel_Text_First:I = 0x7f070501

.field public static final FirstRun_Favorites_Header:I = 0x7f070502

.field public static final FirstRun_Favorites_TextIcon:I = 0x7f070503

.field public static final FirstRun_Favorites_Text_First:I = 0x7f070504

.field public static final FirstRun_Favorites_Text_Second:I = 0x7f070505

.field public static final FirstRun_Favorites_TvListings_Channel_Text_First:I = 0x7f070506

.field public static final FirstRun_Favorites_TvListings_Header:I = 0x7f070507

.field public static final FirstRun_Favorites_TvListings_Text_First:I = 0x7f070508

.field public static final FirstRun_Favorites_TvListings_Text_Second:I = 0x7f070509

.field public static final FirstRun_RecentChannels_Header:I = 0x7f07050a

.field public static final FirstRun_TVListings_BtnText:I = 0x7f07050b

.field public static final FirstRun_TVListings_GoldUpsell_BtnText:I = 0x7f07050c

.field public static final FirstRun_TVListings_Gold_Message:I = 0x7f07050d

.field public static final FirstRun_TVListings_Header:I = 0x7f07050e

.field public static final FirstRun_TVListings_Message_Text:I = 0x7f07050f

.field public static final Followers_List_Empty:I = 0x7f070510

.field public static final Followers_Title:I = 0x7f070511

.field public static final FriendFinder_AddFriendsSuccessfully_Toast:I = 0x7f070e01

.field public static final FriendFinder_EnterCode_Dialog_Text_Line1:I = 0x7f070512

.field public static final FriendFinder_EnterCode_Dialog_Text_Line2:I = 0x7f070513

.field public static final FriendFinder_EnterCode_Dialog_Title:I = 0x7f070514

.field public static final FriendFinder_Facebook_Linked_Error:I = 0x7f070515

.field public static final FriendFinder_Facebook_Share_Description:I = 0x7f070516

.field public static final FriendFinder_Facebook_Share_Title:I = 0x7f070517

.field public static final FriendFinder_Facebook_Upsell_Description_Default_LineTwo:I = 0x7f070518

.field public static final FriendFinder_Facebook_Upsell_Description_NoFriends_LineOne:I = 0x7f070519

.field public static final FriendFinder_Facebook_Upsell_Description_SomeFriends_LineOne:I = 0x7f07051a

.field public static final FriendFinder_Facebook_Upsell_ShareToFacebook:I = 0x7f07051b

.field public static final FriendFinder_Facebook_Upsell_Title_ManyFriends:I = 0x7f07051c

.field public static final FriendFinder_Facebook_Upsell_Title_NoFriends:I = 0x7f07051d

.field public static final FriendFinder_Facebook_Upsell_Title_OneFriend:I = 0x7f07051e

.field public static final FriendFinder_Facebook_Upsell_Title_ThreeFriends:I = 0x7f07051f

.field public static final FriendFinder_Facebook_Upsell_Title_TwoFriends:I = 0x7f070520

.field public static final FriendFinder_Facebook_ViewSuggestion_Buttontext:I = 0x7f070521

.field public static final FriendFinder_FailedToAddFriends_Toast:I = 0x7f070e02

.field public static final FriendFinder_Generic_Error:I = 0x7f070522

.field public static final FriendFinder_LinkFacebook_Dialog_Text_Blocked_LineThree:I = 0x7f070523

.field public static final FriendFinder_LinkFacebook_Dialog_Text_Blocked_LineTwo:I = 0x7f070524

.field public static final FriendFinder_LinkFacebook_Dialog_Text_Default:I = 0x7f070525

.field public static final FriendFinder_LinkFacebook_Dialog_Text_NotSet_LineThree:I = 0x7f070526

.field public static final FriendFinder_LinkFacebook_Dialog_Text_NotSet_LineTwo:I = 0x7f070527

.field public static final FriendFinder_LinkFacebook_Dialog_Text_PeopleIChoose_LineTwo:I = 0x7f070528

.field public static final FriendFinder_LinkFacebook_Dialog_Title:I = 0x7f070529

.field public static final FriendFinder_LinkFacebook_Generic_Error:I = 0x7f07052a

.field public static final FriendFinder_LinkFacebook_Inline_Error:I = 0x7f07052b

.field public static final FriendFinder_LinkFacebook_NotInterest:I = 0x7f07052c

.field public static final FriendFinder_LinkFacebook_NotInterest_Subtext:I = 0x7f07052d

.field public static final FriendFinder_LinkFacebook_Subtext:I = 0x7f07052e

.field public static final FriendFinder_LinkPhone_Dialog_Text_Blocked_Bullet1:I = 0x7f07052f

.field public static final FriendFinder_LinkPhone_Dialog_Text_Blocked_Bullet2:I = 0x7f070530

.field public static final FriendFinder_LinkPhone_Dialog_Text_Blocked_Line1:I = 0x7f070531

.field public static final FriendFinder_LinkPhone_Dialog_Text_Friends_I_Choose_Line1:I = 0x7f070532

.field public static final FriendFinder_LinkPhone_Dialog_Text_Last_Line:I = 0x7f070533

.field public static final FriendFinder_LinkPhone_Dialog_Text_Line1:I = 0x7f070534

.field public static final FriendFinder_LinkPhone_Dialog_Text_Line2:I = 0x7f070535

.field public static final FriendFinder_LinkPhone_Dialog_Title:I = 0x7f070536

.field public static final FriendFinder_LinkPhone_Error:I = 0x7f070537

.field public static final FriendFinder_LinkPhone_Generic_Error:I = 0x7f070e03

.field public static final FriendFinder_Link_Phone_Subtext:I = 0x7f070538

.field public static final FriendFinder_Not_Interested_Phone_Subtext:I = 0x7f070539

.field public static final FriendFinder_OptIn:I = 0x7f07053a

.field public static final FriendFinder_OptOut:I = 0x7f07053b

.field public static final FriendFinder_PhoneAddContacts_Dialog_Dialog_Text:I = 0x7f070e04

.field public static final FriendFinder_PhoneAddContacts_Dialog_Title:I = 0x7f070e05

.field public static final FriendFinder_PhoneCheckValidPhoneNubmerReminder:I = 0x7f07053c

.field public static final FriendFinder_PhoneContacts_GamingIsBetter_Dialog_Title:I = 0x7f070e06

.field public static final FriendFinder_PhoneInviteFriends_Dialog_Text:I = 0x7f070e07

.field public static final FriendFinder_PhoneInviteFriends_Dialog_Title:I = 0x7f070e08

.field public static final FriendFinder_PhoneInviteFriends_Message:I = 0x7f070e09

.field public static final FriendFinder_PhoneInviteFriends_On_Xbox:I = 0x7f070e0a

.field public static final FriendFinder_PhoneInviteFriends_SelectContact_Toast:I = 0x7f070e0b

.field public static final FriendFinder_PhoneNoContacts_Dialog_Title:I = 0x7f070e0c

.field public static final FriendFinder_PhoneNoContacts_GamingIsBetter_Dialog_Text:I = 0x7f070e0d

.field public static final FriendFinder_PhoneNumberHint:I = 0x7f07053d

.field public static final FriendFinder_PhoneVerification_Dialog_Text_Line1:I = 0x7f07053e

.field public static final FriendFinder_PhoneVerification_Dialog_Text_Line2:I = 0x7f07053f

.field public static final FriendFinder_PhoneVerification_Dialog_Title:I = 0x7f070540

.field public static final FriendFinder_Phone_AddFriends_ButtonText:I = 0x7f070e0e

.field public static final FriendFinder_Phone_Back_ButtonText:I = 0x7f070541

.field public static final FriendFinder_Phone_CallMe_ButtonText:I = 0x7f070542

.field public static final FriendFinder_Phone_ChangeRegion_ButtonText:I = 0x7f070543

.field public static final FriendFinder_Phone_Done_Button_Text:I = 0x7f070e0f

.field public static final FriendFinder_Phone_EnterCode_Text:I = 0x7f070544

.field public static final FriendFinder_Phone_FindFriends_ButtonText:I = 0x7f070545

.field public static final FriendFinder_Phone_Next_ButtonText:I = 0x7f070546

.field public static final FriendFinder_Phone_ResendTheCode_ButtonText:I = 0x7f070547

.field public static final FriendFinder_Phone_SearchHint:I = 0x7f070548

.field public static final FriendFinder_Phone_SendInvite_ButtonText:I = 0x7f070e10

.field public static final FriendFinder_Phone_Skip_ButtonText:I = 0x7f070549

.field public static final FriendFinder_Phone_Unlink_ButtonText:I = 0x7f07054a

.field public static final FriendFinder_Phone_VerifyCode_ButtonText:I = 0x7f07054b

.field public static final FriendFinder_RepairFacebook_Settings_Title:I = 0x7f07054c

.field public static final FriendFinder_RepairFacebook_Subtext:I = 0x7f07054d

.field public static final FriendFinder_Repair_Error:I = 0x7f07054e

.field public static final FriendFinder_Repair_Facebook:I = 0x7f07054f

.field public static final FriendFinder_ShareToFacebook:I = 0x7f070550

.field public static final FriendFinder_Suggestion_Filter_Phone:I = 0x7f070551

.field public static final FriendFinder_Suggestion_Phone_No_Contacts:I = 0x7f070552

.field public static final FriendFinder_UnlinkFacebook_Dialog_Text_LineOne:I = 0x7f070553

.field public static final FriendFinder_UnlinkFacebook_Dialog_Text_LineThree:I = 0x7f070554

.field public static final FriendFinder_UnlinkFacebook_Dialog_Text_LineTwo:I = 0x7f070555

.field public static final FriendFinder_UnlinkFacebook_Dialog_Title:I = 0x7f070556

.field public static final FriendFinder_UnlinkFacebook_Generic_Error:I = 0x7f070557

.field public static final FriendFinder_UnlinkFacebook_Inline_Error:I = 0x7f070558

.field public static final FriendFinder_UnlinkFacebook_Subtext:I = 0x7f070559

.field public static final FriendFinder_UnlinkPhone_Dialog_Text_Bullet1:I = 0x7f07055a

.field public static final FriendFinder_UnlinkPhone_Dialog_Text_Bullet2:I = 0x7f07055b

.field public static final FriendFinder_UnlinkPhone_Dialog_Text_Bullet3:I = 0x7f07055c

.field public static final FriendFinder_UnlinkPhone_Dialog_Title:I = 0x7f07055d

.field public static final FriendFinder_UnlinkPhone_Error:I = 0x7f07055e

.field public static final FriendFinder_Unlink_Phone_Subtext:I = 0x7f07055f

.field public static final FriendSearch:I = 0x7f070560

.field public static final FriendsAchievementsHeader:I = 0x7f070561

.field public static final FriendsAndGames_Filter_FriendsAndGames:I = 0x7f070562

.field public static final FriendsAndGames_Filter_Muted:I = 0x7f070563

.field public static final FriendsAndGames_Filter_PeopleAndGames:I = 0x7f070564

.field public static final FriendsAndGames_Filter_PeopleAndGames_Count:I = 0x7f070565

.field public static final FriendsAndGames_Header:I = 0x7f070566

.field public static final FriendsFinder_Link_Facebook:I = 0x7f070567

.field public static final FriendsFinder_Link_Phone:I = 0x7f070568

.field public static final FriendsFinder_Not_Interested_Phone:I = 0x7f070569

.field public static final FriendsFinder_Unlink_Facebook:I = 0x7f07056a

.field public static final FriendsFinder_Unlink_Phone:I = 0x7f07056b

.field public static final FriendsGamesClubs_ClubsCount:I = 0x7f07056c

.field public static final FriendsGamesClubs_Header:I = 0x7f07056d

.field public static final FriendsHub_ActivityFeed_Broadcasting:I = 0x7f07056e

.field public static final FriendsHub_ActivityFeed_NoData:I = 0x7f07056f

.field public static final FriendsHub_ClubFilterOptionClubFirst:I = 0x7f070570

.field public static final FriendsHub_ClubFilterOptionCombined:I = 0x7f070571

.field public static final FriendsHub_ClubFilterOptionFriendsFirst:I = 0x7f070572

.field public static final FriendsHub_CouldNotFindGamer:I = 0x7f070573

.field public static final FriendsHub_FilterOptionBroadcasting:I = 0x7f070574

.field public static final FriendsHub_FilterOptionFavorites:I = 0x7f070575

.field public static final FriendsHub_FilterOptionFollowers:I = 0x7f070576

.field public static final FriendsHub_FilterOptionFriends:I = 0x7f070577

.field public static final FriendsHub_FilterOptionRecentPlayers:I = 0x7f070578

.field public static final FriendsHub_FriendsList_Favorite_NoData:I = 0x7f070579

.field public static final FriendsHub_FriendsList_FollowerAddedDate:I = 0x7f07057a

.field public static final FriendsHub_FriendsList_Follower_NoData:I = 0x7f07057b

.field public static final FriendsHub_FriendsList_Friends_NoData:I = 0x7f07057c

.field public static final FriendsHub_FriendsList_NewFollowersCount:I = 0x7f07057d

.field public static final FriendsHub_FriendsList_OnlineFriendsCount:I = 0x7f07057e

.field public static final FriendsHub_FriendsList_Presence_Broadcasting:I = 0x7f07057f

.field public static final FriendsHub_FriendsList_Presence_Device_Xbox360:I = 0x7f070580

.field public static final FriendsHub_FriendsList_Presence_LastSeen:I = 0x7f070581

.field public static final FriendsHub_FriendsList_Presence_Offline:I = 0x7f070582

.field public static final FriendsHub_FriendsList_Presence_OfflineCount:I = 0x7f070583

.field public static final FriendsHub_FriendsList_Presence_RichPresence:I = 0x7f070584

.field public static final FriendsHub_FriendsList_RecentPlayers_NoData:I = 0x7f070585

.field public static final FriendsHub_HeaderTitle_Phone:I = 0x7f070586

.field public static final FriendsHub_HeaderTitle_Tablet:I = 0x7f070587

.field public static final FriendsHub_Recommendations_Fail_To_Remove_Suggestion:I = 0x7f070588

.field public static final FriendsHub_Recommendations_List_Header:I = 0x7f070589

.field public static final FriendsHub_Recommendations_See_All_Button:I = 0x7f07058a

.field public static final FriendsHub_SearchButton_Phone:I = 0x7f07058b

.field public static final FriendsHub_Search_GOTO_PROFILE:I = 0x7f07058c

.field public static final FriendsHub_SectionHeaderActivityFeed_Phone:I = 0x7f07058d

.field public static final FriendsHub_SectionHeaderActivityFeed_Tablet:I = 0x7f07058e

.field public static final FriendsHub_SectionHeaderFriendsList_Phone:I = 0x7f07058f

.field public static final FriendsSearch_Results:I = 0x7f070590

.field public static final FriendsSearch_SearchBoxHint:I = 0x7f070591

.field public static final FriendsSearch_SearchingText:I = 0x7f070592

.field public static final FriendsWhoPlay_Header_Phone:I = 0x7f070593

.field public static final FriendsWhoPlay_Header_Tablet:I = 0x7f070594

.field public static final FutureShowTimes_OnNow:I = 0x7f070595

.field public static final FutureShowtimes_Header:I = 0x7f070596

.field public static final FutureShowtimes_Header_NotAllCaps:I = 0x7f070597

.field public static final FutureShowtimes_TimeFormat:I = 0x7f070598

.field public static final GameConsumables_Header:I = 0x7f070599

.field public static final GameDVR_DeletedClip_Playback_Error:I = 0x7f07059a

.field public static final GameDVR_Filter_CommunityClips_Recent:I = 0x7f07059b

.field public static final GameDVR_Filter_CommunityClips_Saved:I = 0x7f07059c

.field public static final GameDVR_Filter_CommunityScreenShots_Recent:I = 0x7f07059d

.field public static final GameDVR_Filter_CommunityScreenShots_Saved:I = 0x7f07059e

.field public static final GameDVR_Filter_MyGameClips_Recent:I = 0x7f07059f

.field public static final GameDVR_Filter_MyGameClips_Saved:I = 0x7f0705a0

.field public static final GameDVR_Filter_MyScreenShots_Recent:I = 0x7f0705a1

.field public static final GameDVR_Filter_MyScreenShots_Saved:I = 0x7f0705a2

.field public static final GameDVR_ServiceFailure_Playback_Error:I = 0x7f0705a3

.field public static final GameData_Achievement_Rarity_Rare:I = 0x7f0705a4

.field public static final GamePickerSelect_Error:I = 0x7f0705a5

.field public static final GamePicker_Console_Game:I = 0x7f0705a6

.field public static final GamePicker_Error:I = 0x7f0705a7

.field public static final GamePicker_Find_Game:I = 0x7f0705a8

.field public static final GamePicker_No_Content:I = 0x7f0705a9

.field public static final GamePicker_PC_Games:I = 0x7f0705aa

.field public static final GamePicker_Title:I = 0x7f0705ab

.field public static final GameProfile_Achievement_AchievementEarned:I = 0x7f0705ac

.field public static final GameProfile_Achievement_CompareWithFriend:I = 0x7f0705ad

.field public static final GameProfile_Achievement_Detail_Goto:I = 0x7f0705ae

.field public static final GameProfile_Achievement_Detail_list_header:I = 0x7f0705af

.field public static final GameProfile_Achievement_Gamerscore:I = 0x7f0705b0

.field public static final GameProfile_Achievement_Stat_Rank:I = 0x7f0705b1

.field public static final GameProfile_Achievement_StopComparison:I = 0x7f0705b2

.field public static final GameProfile_Achievement_leaderboards:I = 0x7f0705b3

.field public static final GameProfile_ActivityFeed_NoData:I = 0x7f0705b4

.field public static final GameProfile_ActivityFeed_NoData_body:I = 0x7f0705b5

.field public static final GameProfile_ActivityFeed_NoData_header:I = 0x7f0705b6

.field public static final GameProfile_Captures_Filter_Everything:I = 0x7f0705b7

.field public static final GameProfile_Captures_Filter_GameClips:I = 0x7f0705b8

.field public static final GameProfile_Captures_Filter_Screenshots:I = 0x7f0705b9

.field public static final GameProfile_Captures_No_Data:I = 0x7f0705ba

.field public static final GameProfile_Captures_Title:I = 0x7f0705bb

.field public static final GameProfile_Compare_Header:I = 0x7f0705bc

.field public static final GameProfile_Compare_NoData_body:I = 0x7f0705bd

.field public static final GameProfile_Compare_NoData_header:I = 0x7f0705be

.field public static final GameProfile_Friends_And_Clubs:I = 0x7f0705bf

.field public static final GameProfile_Friends_ClubsForTitle:I = 0x7f0705c0

.field public static final GameProfile_Friends_ClubsForTitle_NoData:I = 0x7f0705c1

.field public static final GameProfile_Friends_ClubsForUserAndTitle_NoData:I = 0x7f0705c2

.field public static final GameProfile_Friends_FriendsWhoPlay_Title:I = 0x7f0705c3

.field public static final GameProfile_Friends_NoBroadcasters:I = 0x7f0705c4

.field public static final GameProfile_Friends_NoFriends:I = 0x7f0705c5

.field public static final GameProfile_Friends_RelatedClubs:I = 0x7f0705c6

.field public static final GameProfile_Friends_Spotlight_Title:I = 0x7f0705c7

.field public static final GameProfile_Friends_Suggestions_Title:I = 0x7f0705c8

.field public static final GameProfile_Friends_Title:I = 0x7f0705c9

.field public static final GameProfile_Friends_YourClubs:I = 0x7f0705ca

.field public static final GameProfile_Info_ActionFailure:I = 0x7f0705cb

.field public static final GameProfile_Info_Follow_Button_Text:I = 0x7f0705cc

.field public static final GameProfile_Info_Friends_Who_Play_Text:I = 0x7f0705cd

.field public static final GameProfile_Info_Play_Button_Text:I = 0x7f0705ce

.field public static final GameProfile_Info_Title:I = 0x7f0705cf

.field public static final GameProfile_Info_TrendingPosts_Button_Text:I = 0x7f0705d0

.field public static final GameProfile_Info_Unfollow_Button_Text:I = 0x7f0705d1

.field public static final GameProfile_Info_Unfollow_Error_Text:I = 0x7f0705d2

.field public static final GameProfile_Info_ViewInStore_Button_Text:I = 0x7f0705d3

.field public static final GameProfile_Info_ViewInStore_Button_Text_MS:I = 0x7f0705d4

.field public static final GameProfile_Spotlight_Beam_Message:I = 0x7f0705d5

.field public static final GameProfile_Spotlight_Twitch_Message:I = 0x7f0705d6

.field public static final GameProfile_Spotlight_Who_Is_Broadcasting:I = 0x7f0705d7

.field public static final GameProgress_AchievementsGallery_HeaderTitle_Tablet:I = 0x7f0705d8

.field public static final GameProgress_Achievements_FilterOptionAll:I = 0x7f0705d9

.field public static final GameProgress_Achievements_FilterOptionEarned:I = 0x7f0705da

.field public static final GameProgress_Achievements_FilterOptionLocked:I = 0x7f0705db

.field public static final GameProgress_Achievements_HeaderTitle_Phone:I = 0x7f0705dc

.field public static final GameProgress_Achievements_HeaderTitle_Tablet:I = 0x7f0705dd

.field public static final GameProgress_Achievements_NoData:I = 0x7f0705de

.field public static final GameProgress_Achievements_NoEarned:I = 0x7f0705df

.field public static final GameProgress_Achievements_NoLocked:I = 0x7f0705e0

.field public static final GameProgress_Achievements_PercentGamersUnlocked:I = 0x7f0705e1

.field public static final GameProgress_AppDetails_Button_Text:I = 0x7f0705e2

.field public static final GameProgress_Captures_Channel_HeaderTitle_Phone:I = 0x7f0705e3

.field public static final GameProgress_Captures_Channel_HeaderTitle_Tablet:I = 0x7f0705e4

.field public static final GameProgress_ChallengesGallery_HeaderTitle_Tablet:I = 0x7f0705e5

.field public static final GameProgress_Challenges_HeaderTitle_Phone:I = 0x7f0705e6

.field public static final GameProgress_Challenges_HeaderTitle_Tablet:I = 0x7f0705e7

.field public static final GameProgress_Challenges_NoData:I = 0x7f0705e8

.field public static final GameProgress_Channel_FilterOptionCommunity:I = 0x7f0705e9

.field public static final GameProgress_Channel_FilterOptionMyChannel:I = 0x7f0705ea

.field public static final GameProgress_Channel_HeaderTitle_Phone:I = 0x7f0705eb

.field public static final GameProgress_Channel_HeaderTitle_Tablet:I = 0x7f0705ec

.field public static final GameProgress_Channel_NoData:I = 0x7f0705ed

.field public static final GameProgress_Channel_NoData_Community:I = 0x7f0705ee

.field public static final GameProgress_Channel_NoData_CommunityButtonText:I = 0x7f0705ef

.field public static final GameProgress_Channel_Screenshots_NoData:I = 0x7f0705f0

.field public static final GameProgress_Channel_Screenshots_NoData_Community:I = 0x7f0705f1

.field public static final GameProgress_Channel_Screenshots_NoData_CommunityButtonText:I = 0x7f0705f2

.field public static final GameProgress_Channel_View:I = 0x7f0705f3

.field public static final GameProgress_Channel_Views:I = 0x7f0705f4

.field public static final GameProgress_GameDetails_Button_Text:I = 0x7f0705f5

.field public static final GameProgress_Progress_HeaderTitle_Phone:I = 0x7f0705f6

.field public static final GameProgress_Progress_HeaderTitle_Tablet:I = 0x7f0705f7

.field public static final GameStreaming_BeginTest:I = 0x7f0705f8

.field public static final GameStreaming_ConnectToTest:I = 0x7f0705f9

.field public static final GameStreaming_SignInToTest:I = 0x7f0705fa

.field public static final GameStreaming_TestComplete:I = 0x7f0705fb

.field public static final GameStreaming_TestInProgress:I = 0x7f0705fc

.field public static final GameStreaming_TestNetworkStreaming:I = 0x7f0705fd

.field public static final Gamerscore_Leaderboard_Just_You:I = 0x7f0705fe

.field public static final Gamerscore_Leaderboard_Monthly_Gamerscore:I = 0x7f0705ff

.field public static final Gamerscore_Leaderboard_Rank_Gamertag:I = 0x7f070600

.field public static final Gamerscore_Leaderboard_Title:I = 0x7f070601

.field public static final General_A_Thousand_Plus:I = 0x7f070602

.field public static final General_App_Settings:I = 0x7f070603

.field public static final General_Club_Chat:I = 0x7f070604

.field public static final General_My_Conversations:I = 0x7f070605

.field public static final General_Open:I = 0x7f070606

.field public static final General_RemindMe:I = 0x7f070607

.field public static final General_Single_Viewer:I = 0x7f070608

.field public static final General_Take_Action_In_Xbox_App:I = 0x7f070609

.field public static final General_Viewer:I = 0x7f07060a

.field public static final General_Viewers:I = 0x7f07060b

.field public static final General_Viewers_Count:I = 0x7f07060c

.field public static final Generic_Cancel:I = 0x7f07060d

.field public static final Generic_Episode:I = 0x7f07060e

.field public static final Generic_Error_Title:I = 0x7f07060f

.field public static final Generic_Load_More_Failed:I = 0x7f070610

.field public static final Generic_No:I = 0x7f070611

.field public static final Generic_Season:I = 0x7f070612

.field public static final Generic_X_Slash_Y:I = 0x7f070613

.field public static final Generic_Yes:I = 0x7f070614

.field public static final Global_Achievements_Completed:I = 0x7f070615

.field public static final Global_Android:I = 0x7f070616

.field public static final Global_Busy_Wait:I = 0x7f070617

.field public static final Global_Challenge_StartsIn:I = 0x7f070618

.field public static final Global_Challenge_TimeRemaining:I = 0x7f070619

.field public static final Global_LeavingApp:I = 0x7f07061a

.field public static final Global_MinutesText:I = 0x7f07061b

.field public static final Global_MissingPrivilegeError_DialogBody:I = 0x7f07061c

.field public static final Global_MissingPrivilegeError_DialogTitle:I = 0x7f07061d

.field public static final Global_NHoursText:I = 0x7f07061e

.field public static final Global_NHrText:I = 0x7f07061f

.field public static final Global_NHrsMMinsText:I = 0x7f070620

.field public static final Global_NHrsText:I = 0x7f070621

.field public static final Global_NMinText:I = 0x7f070622

.field public static final Global_NMinsMSecsText:I = 0x7f070623

.field public static final Global_NMinsText:I = 0x7f070624

.field public static final Global_NMinutesText:I = 0x7f070625

.field public static final Global_NSecText:I = 0x7f070626

.field public static final Global_NSecsText:I = 0x7f070627

.field public static final Global_Now:I = 0x7f070628

.field public static final Global_NumberOfDaysAgo:I = 0x7f070629

.field public static final Global_NumberOfHoursAgo_Short:I = 0x7f07062a

.field public static final Global_NumberOfMinutesAgo_Short:I = 0x7f07062b

.field public static final Global_NumberOfYearsAgo:I = 0x7f07062c

.field public static final Global_OneHour_Text:I = 0x7f07062d

.field public static final Global_OneMinute:I = 0x7f07062e

.field public static final Global_PC:I = 0x7f07062f

.field public static final Global_RefreshText:I = 0x7f070630

.field public static final Global_Search_Criteria:I = 0x7f070e11

.field public static final Global_Today:I = 0x7f070631

.field public static final Global_UnknownValue_Dash:I = 0x7f070632

.field public static final Global_Windows10:I = 0x7f070633

.field public static final Global_WindowsPhone:I = 0x7f070634

.field public static final Global_Xbox360:I = 0x7f070635

.field public static final Global_XboxOne:I = 0x7f070636

.field public static final Global_iOS:I = 0x7f070637

.field public static final GroupMessaging_Add_People:I = 0x7f070638

.field public static final GroupMessaging_Addedto_Conversation:I = 0x7f070639

.field public static final GroupMessaging_Cancel:I = 0x7f07063a

.field public static final GroupMessaging_Choose_People:I = 0x7f07063b

.field public static final GroupMessaging_Conversation_Members:I = 0x7f07063c

.field public static final GroupMessaging_Conversation_Renamedto:I = 0x7f07063d

.field public static final GroupMessaging_Create_Conversation:I = 0x7f07063e

.field public static final GroupMessaging_Date_Formatting_Other:I = 0x7f07063f

.field public static final GroupMessaging_Date_Formatting_Today:I = 0x7f070640

.field public static final GroupMessaging_Date_Formatting_Week:I = 0x7f070641

.field public static final GroupMessaging_Date_Formatting_Yesterday:I = 0x7f070642

.field public static final GroupMessaging_Delete:I = 0x7f070643

.field public static final GroupMessaging_Delete_Conversation_Dialog_Message:I = 0x7f070644

.field public static final GroupMessaging_Delete_Conversation_Dialog_Title:I = 0x7f070645

.field public static final GroupMessaging_ErrorAddingPeopleFormat:I = 0x7f070646

.field public static final GroupMessaging_ErrorAddingPeople_PeopleBlockedYou_Title:I = 0x7f070647

.field public static final GroupMessaging_ErrorAddingThreeOrMorePeople:I = 0x7f070648

.field public static final GroupMessaging_ErrorAddingTwoPeople:I = 0x7f070649

.field public static final GroupMessaging_ErrorAdding_YourPeoplePrivacy_Body:I = 0x7f07064a

.field public static final GroupMessaging_ErrorChangingTopic_YourPeoplePrivacy_Body:I = 0x7f07064b

.field public static final GroupMessaging_ErrorDeletingMessage_YourPeoplePrivacy_Body:I = 0x7f07064c

.field public static final GroupMessaging_ErrorMessagingPeople_PeopleBlockedYou_Title:I = 0x7f07064d

.field public static final GroupMessaging_ErrorMessaging_YourPeoplePrivacy_Body:I = 0x7f07064e

.field public static final GroupMessaging_ErrorParticipating_PeopleYouBlocked_Body:I = 0x7f07064f

.field public static final GroupMessaging_ErrorParticipating_PeopleYouBlocked_Title:I = 0x7f070650

.field public static final GroupMessaging_ErrorPeople_BlockedYou_Body:I = 0x7f070651

.field public static final GroupMessaging_ErrorPeople_BlockedYou_Body_Short:I = 0x7f070652

.field public static final GroupMessaging_ErrorRenaming_PeopleBlockedYou_Title:I = 0x7f070653

.field public static final GroupMessaging_Error_YourPeoplePrivacy_Title:I = 0x7f070654

.field public static final GroupMessaging_Failed_To_Add_Some_Users_To_Conversation:I = 0x7f070655

.field public static final GroupMessaging_Failed_To_Remove_From_Conversation:I = 0x7f070656

.field public static final GroupMessaging_Failed_To_Rename_Conversation:I = 0x7f070657

.field public static final GroupMessaging_Failed_To_Set_Muted_Property:I = 0x7f070658

.field public static final GroupMessaging_Leave:I = 0x7f070659

.field public static final GroupMessaging_Left_Conversation:I = 0x7f07065a

.field public static final GroupMessaging_Message_SharedContent:I = 0x7f07065b

.field public static final GroupMessaging_Mute:I = 0x7f07065c

.field public static final GroupMessaging_Mute_Confirmation:I = 0x7f07065d

.field public static final GroupMessaging_Mute_ConfirmationDescription:I = 0x7f07065e

.field public static final GroupMessaging_New_Message:I = 0x7f07065f

.field public static final GroupMessaging_Online_Section:I = 0x7f070660

.field public static final GroupMessaging_PresenceCount:I = 0x7f070661

.field public static final GroupMessaging_Rename:I = 0x7f070662

.field public static final GroupMessaging_Rename_Button:I = 0x7f070663

.field public static final GroupMessaging_Rename_Default_Text:I = 0x7f070664

.field public static final GroupMessaging_Save:I = 0x7f070665

.field public static final GroupMessaging_SelectedFriends:I = 0x7f070666

.field public static final GroupMessaging_Send:I = 0x7f070667

.field public static final GroupMessaging_SentTo:I = 0x7f070668

.field public static final GroupMessaging_Share_To_Conversation:I = 0x7f070669

.field public static final GroupMessaging_Start_Conversation:I = 0x7f07066a

.field public static final GroupMessaging_Unmute:I = 0x7f07066b

.field public static final GroupMessaging_View_Profile:I = 0x7f07066c

.field public static final GroupMessaging_View_all:I = 0x7f07066d

.field public static final HOME_OOBE:I = 0x7f07066e

.field public static final Home_ActivityFeed_Recommendation_Add_Friend:I = 0x7f07066f

.field public static final Home_ActivityFeed_Recommendation_Header:I = 0x7f070670

.field public static final Home_BAT_Details:I = 0x7f070671

.field public static final Home_BAT_Details_MS:I = 0x7f070672

.field public static final Home_BAT_RemoteControl:I = 0x7f070673

.field public static final Home_BAT_SmartGlassCompanion:I = 0x7f070674

.field public static final Home_BAT_TVGuide:I = 0x7f070675

.field public static final Home_Featured_Error:I = 0x7f070676

.field public static final Home_Featured_PageTitle:I = 0x7f070677

.field public static final Home_FirstRun:I = 0x7f070678

.field public static final Home_Pins_PageTitle:I = 0x7f070679

.field public static final Home_Recents_PageTitle:I = 0x7f07067a

.field public static final Home_Recents_RecentsEmpty:I = 0x7f07067b

.field public static final Home_Recents_RecentsEmptyDesc:I = 0x7f07067c

.field public static final Home_Recents_RecentsError:I = 0x7f07067d

.field public static final Home_XboxOneSmartGlass:I = 0x7f07067e

.field public static final Home_XboxOne_XboxApp:I = 0x7f07067f

.field public static final Hover_Chat_Inbox_Title:I = 0x7f070680

.field public static final Hover_Chat_Open:I = 0x7f070681

.field public static final Hover_Chat_Title:I = 0x7f070682

.field public static final LRC_Error_Code_FailedToRetrieveData:I = 0x7f070683

.field public static final Language_BrazilPortuguese:I = 0x7f070e12

.field public static final Language_Danish:I = 0x7f070e13

.field public static final Language_Dutch:I = 0x7f070e14

.field public static final Language_Finnish:I = 0x7f070e15

.field public static final Language_French:I = 0x7f070e16

.field public static final Language_German:I = 0x7f070e17

.field public static final Language_Italian:I = 0x7f070e18

.field public static final Language_Japanese:I = 0x7f070e19

.field public static final Language_Korean:I = 0x7f070e1a

.field public static final Language_LATAMSpanish:I = 0x7f070e1b

.field public static final Language_Norwegian:I = 0x7f070e1c

.field public static final Language_Polish:I = 0x7f070e1d

.field public static final Language_Portuguese:I = 0x7f070e1e

.field public static final Language_Russian:I = 0x7f070e1f

.field public static final Language_SimplifiedChinese:I = 0x7f070e20

.field public static final Language_Spanish:I = 0x7f070e21

.field public static final Language_Swedish:I = 0x7f070e22

.field public static final Language_TraditionalChinese:I = 0x7f070e23

.field public static final Language_Turkish:I = 0x7f070e24

.field public static final Language_UKEnglish:I = 0x7f070e25

.field public static final Language_USEnglish:I = 0x7f070e26

.field public static final Launch_DifferentTitle_Warning_Message:I = 0x7f070684

.field public static final Launch_GameProfile_Text:I = 0x7f070685

.field public static final Launcher_CurrentlyPlaying:I = 0x7f070686

.field public static final Launcher_DataNotIngested_Error:I = 0x7f070687

.field public static final Launcher_ErrorToRetrieveDetail:I = 0x7f070688

.field public static final Launcher_NativeCompanionConfirmationMessage:I = 0x7f070689

.field public static final Launcher_NativeCompanionConfirmationMessage_No:I = 0x7f07068a

.field public static final Launcher_NativeCompanionConfirmationMessage_Yes:I = 0x7f07068b

.field public static final Launcher_NoProvidersAvailableForPin:I = 0x7f07068c

.field public static final Launcher_NoProvidersOrShowTimesAvailable:I = 0x7f07068d

.field public static final Launcher_OneGuideAvailable:I = 0x7f07068e

.field public static final Launcher_PlayOnSmartGlass:I = 0x7f07068f

.field public static final Launcher_PlayToFill:I = 0x7f070690

.field public static final Launcher_PlayToFull:I = 0x7f070691

.field public static final Launcher_PlayToSnap:I = 0x7f070692

.field public static final Launcher_PlayToXboxOne:I = 0x7f070693

.field public static final Launcher_ProvidersOrShowTimesError:I = 0x7f070694

.field public static final Launcher_SelectProviderToPin:I = 0x7f070695

.field public static final Launcher_SelectProviderToPlay:I = 0x7f070696

.field public static final Launcher_ShowCompanion:I = 0x7f070697

.field public static final Launcher_ShowOneGuide:I = 0x7f070698

.field public static final Launcher_ShowRemote:I = 0x7f070699

.field public static final Launcher_SmartGlassCompanionAvailable:I = 0x7f07069a

.field public static final Launcher_Unsnap:I = 0x7f07069b

.field public static final Launcher_XboxApp_NativeCompanionConfirmationMessage_No:I = 0x7f07069c

.field public static final Launcher_XboxApp_PlayOnSmartGlass:I = 0x7f07069d

.field public static final Leaderboard_HeaderTitle_Final:I = 0x7f07069e

.field public static final Leaderboard_HeaderTitle_Phone:I = 0x7f07069f

.field public static final Leaderboard_HeaderTitle_Tablet:I = 0x7f0706a0

.field public static final Leaderboard_NoData:I = 0x7f0706a1

.field public static final Leaderboard_NoStats:I = 0x7f0706a2

.field public static final Leaderboard_Share_Comparison:I = 0x7f0706a3

.field public static final Leaderboard_Share_Your_Progress:I = 0x7f0706a4

.field public static final Leaderboard_Stats_Unit_Ahead:I = 0x7f0706a5

.field public static final Leaderboard_Stats_Unit_Behind:I = 0x7f0706a6

.field public static final Leaderboard_Stats_Unit_Tied:I = 0x7f0706a7

.field public static final Leaderboard_Stats_Unit_You:I = 0x7f0706a8

.field public static final LegacyGameProgress_Achievements_HeaderTitle:I = 0x7f0706a9

.field public static final LegacyGameProgress_Progress_HeaderTitle_Phone:I = 0x7f0706aa

.field public static final LegacyGameProgress_Progress_HeaderTitle_Tablet:I = 0x7f0706ab

.field public static final LegacyGameProgress_Progress_LastPlayed:I = 0x7f0706ac

.field public static final Lfg_AchievementFormat:I = 0x7f0706ad

.field public static final Lfg_Add_Description:I = 0x7f0706ae

.field public static final Lfg_Card_Future_Time_Format:I = 0x7f0706af

.field public static final Lfg_Card_Today_Time_Format:I = 0x7f0706b0

.field public static final Lfg_Club_Name_Format:I = 0x7f0706b1

.field public static final Lfg_Create:I = 0x7f0706b2

.field public static final Lfg_Create_Conflict_Error:I = 0x7f0706b3

.field public static final Lfg_Create_Conflict_Error_Cancel_Existing_Button:I = 0x7f0706b4

.field public static final Lfg_Create_Conflict_Error_Dismiss_Button:I = 0x7f0706b5

.field public static final Lfg_Create_Day_Format:I = 0x7f0706b6

.field public static final Lfg_Create_Done:I = 0x7f0706b7

.field public static final Lfg_Create_Error:I = 0x7f0706b8

.field public static final Lfg_Create_Go_To_Privacy_Settings:I = 0x7f0706b9

.field public static final Lfg_Create_Incorrect_Privacy_Setting_Error:I = 0x7f0706ba

.field public static final Lfg_Create_Incorrect_Privacy_Setting_Error_Message:I = 0x7f0706bb

.field public static final Lfg_Create_Language:I = 0x7f0706bc

.field public static final Lfg_Create_Need:I = 0x7f0706bd

.field public static final Lfg_Create_Post:I = 0x7f0706be

.field public static final Lfg_Create_Privilege_Revoked:I = 0x7f0706bf

.field public static final Lfg_Create_ScheduledTimePassed_Error:I = 0x7f0706c0

.field public static final Lfg_Create_StartDate:I = 0x7f0706c1

.field public static final Lfg_Create_StartTime:I = 0x7f0706c2

.field public static final Lfg_Create_Time_Format:I = 0x7f0706c3

.field public static final Lfg_Create_Visibility:I = 0x7f0706c4

.field public static final Lfg_Date_Filter_Later_Today:I = 0x7f0706c5

.field public static final Lfg_Details_Accepted:I = 0x7f0706c6

.field public static final Lfg_Details_Close_Group:I = 0x7f0706c7

.field public static final Lfg_Details_Club_Tags:I = 0x7f0706c8

.field public static final Lfg_Details_Host:I = 0x7f0706c9

.field public static final Lfg_Details_Interested:I = 0x7f0706ca

.field public static final Lfg_Details_Interested_Header:I = 0x7f0706cb

.field public static final Lfg_Details_LFG_Tags:I = 0x7f0706cc

.field public static final Lfg_Details_Leave_Group:I = 0x7f0706cd

.field public static final Lfg_Details_More:I = 0x7f0706ce

.field public static final Lfg_Details_TimePlayed:I = 0x7f0706cf

.field public static final Lfg_Details_TimePlayedFormat:I = 0x7f0706d0

.field public static final Lfg_Edit_Description:I = 0x7f0706d1

.field public static final Lfg_Edit_Post:I = 0x7f0706d2

.field public static final Lfg_Edit_Tags:I = 0x7f0706d3

.field public static final Lfg_Error_Scheduled_In_Past:I = 0x7f0706d4

.field public static final Lfg_Error_Scheduled_No_Start:I = 0x7f0706d5

.field public static final Lfg_Failed_To_Create:I = 0x7f0706d6

.field public static final Lfg_Filter_AnyLanguage:I = 0x7f0706d7

.field public static final Lfg_Guest_Name_Format:I = 0x7f0706d8

.field public static final Lfg_Have_Label:I = 0x7f0706d9

.field public static final Lfg_Host_Name_Format:I = 0x7f0706da

.field public static final Lfg_Interested_Placeholder_Text:I = 0x7f0706db

.field public static final Lfg_Interested_Prepopulated_Text:I = 0x7f0706dc

.field public static final Lfg_Join_Error:I = 0x7f070e27

.field public static final Lfg_Leave_Error:I = 0x7f0706dd

.field public static final Lfg_Looking_For_Format:I = 0x7f0706de

.field public static final Lfg_Need_Confirmed:I = 0x7f0706df

.field public static final Lfg_Need_Format:I = 0x7f0706e0

.field public static final Lfg_Need_Full:I = 0x7f0706e1

.field public static final Lfg_Need_Label:I = 0x7f0706e2

.field public static final Lfg_Need_Pending:I = 0x7f0706e3

.field public static final Lfg_Page_Header:I = 0x7f0706e4

.field public static final Lfg_Posted_Time:I = 0x7f0706e5

.field public static final Lfg_Report_Host_Button_Text:I = 0x7f0706e6

.field public static final Lfg_Search:I = 0x7f0706e7

.field public static final Lfg_Search_Error:I = 0x7f0706e8

.field public static final Lfg_Search_NoResults:I = 0x7f0706e9

.field public static final Lfg_Search_Tags:I = 0x7f0706ea

.field public static final Lfg_Send_Interested_Message_Error:I = 0x7f0706eb

.field public static final Lfg_Share_ClubFeed:I = 0x7f0706ec

.field public static final Lfg_Share_Post:I = 0x7f0706ed

.field public static final Lfg_Share_UserFeed:I = 0x7f0706ee

.field public static final Lfg_StartDate_Any:I = 0x7f0706ef

.field public static final Lfg_StartDate_Immediate:I = 0x7f0706f0

.field public static final Lfg_StartDate_Today:I = 0x7f0706f1

.field public static final Lfg_StartDate_Tomorrow:I = 0x7f0706f2

.field public static final Lfg_StartTime_Now:I = 0x7f0706f3

.field public static final Lfg_Starts_Label:I = 0x7f0706f4

.field public static final Lfg_Suggested:I = 0x7f0706f5

.field public static final Lfg_Suggested_Error:I = 0x7f0706f6

.field public static final Lfg_Suggested_NoData:I = 0x7f0706f7

.field public static final Lfg_TagPicker_Achievements_Error:I = 0x7f0706f8

.field public static final Lfg_TagPicker_Achievements_No_Content:I = 0x7f0706f9

.field public static final Lfg_TagPicker_Achievements_Tab:I = 0x7f0706fa

.field public static final Lfg_TagPicker_Custom_Tags_Placeholder:I = 0x7f0706fb

.field public static final Lfg_TagPicker_Earned_Achievements:I = 0x7f0706fc

.field public static final Lfg_TagPicker_Popular_Tab:I = 0x7f0706fd

.field public static final Lfg_TagPicker_System_Error:I = 0x7f0706fe

.field public static final Lfg_TagPicker_System_No_Content:I = 0x7f0706ff

.field public static final Lfg_TagPicker_System_Tab:I = 0x7f070700

.field public static final Lfg_TagPicker_Title:I = 0x7f070701

.field public static final Lfg_TagPicker_Trending_Error:I = 0x7f070702

.field public static final Lfg_TagPicker_Trending_No_Content:I = 0x7f070703

.field public static final Lfg_TagPicker_Unearned_Achievements:I = 0x7f070704

.field public static final Lfg_TagsFormat:I = 0x7f070705

.field public static final Lfg_Upcoming:I = 0x7f070706

.field public static final Lfg_Upcoming_Error:I = 0x7f070707

.field public static final Lfg_Upcoming_NoData:I = 0x7f070708

.field public static final Lfg_Upcoming_NoData_Body:I = 0x7f070709

.field public static final Lfg_Upcoming_NoData_Title:I = 0x7f07070a

.field public static final Lfg_Vetting_Accept_Error:I = 0x7f07070b

.field public static final Lfg_Vetting_Cancel_Button:I = 0x7f07070c

.field public static final Lfg_Vetting_Confirm:I = 0x7f07070d

.field public static final Lfg_Vetting_Decline:I = 0x7f07070e

.field public static final Lfg_Vetting_Decline_Error:I = 0x7f07070f

.field public static final Lfg_Vetting_Decline_One:I = 0x7f070710

.field public static final Lfg_Vetting_Done:I = 0x7f070711

.field public static final Lfg_Vetting_Group_Full_Label:I = 0x7f070712

.field public static final Lfg_Vetting_Interested_Count_Format:I = 0x7f070713

.field public static final Lfg_Vetting_Mass_Decline_Error:I = 0x7f070714

.field public static final Lfg_Vetting_Mass_Decline_Format:I = 0x7f070715

.field public static final Lfg_Vetting_More:I = 0x7f070716

.field public static final Lfg_Vetting_Need_Format:I = 0x7f070717

.field public static final Lfg_Vetting_Report_Button:I = 0x7f070718

.field public static final Lfg_Vetting_Send_Message_Button:I = 0x7f070719

.field public static final Lfg_Vetting_Updating:I = 0x7f07071a

.field public static final Lfg_Vetting_View_Profile_Button:I = 0x7f07071b

.field public static final Lfg_View_Error:I = 0x7f07071c

.field public static final Lfg_View_History:I = 0x7f07071d

.field public static final Lfg_Visibility_Club:I = 0x7f07071e

.field public static final Lfg_Visibility_Friends:I = 0x7f07071f

.field public static final Lfg_Visibility_Info:I = 0x7f070720

.field public static final Lfg_Visibility_Private:I = 0x7f070721

.field public static final Lfg_Visibility_XboxLive:I = 0x7f070722

.field public static final Like_Count_Millions:I = 0x7f070723

.field public static final Like_Count_Thousands:I = 0x7f070724

.field public static final Likes_Filter_Header:I = 0x7f070725

.field public static final Likes_List_NoData:I = 0x7f070726

.field public static final LookForXboxPage_ErrorHeader_CanNotConnect:I = 0x7f070727

.field public static final LookForXboxPage_ErrorHeader_CanNotFind:I = 0x7f070728

.field public static final LookForXboxPage_ErrorHeader_CanNotSignIn:I = 0x7f070729

.field public static final LookForXboxPage_Error_CanNotConnect:I = 0x7f07072a

.field public static final LookForXboxPage_Error_CanNotFind:I = 0x7f07072b

.field public static final LookForXboxPage_XboxApp_Error_CanNotConnect:I = 0x7f07072c

.field public static final LookForXboxPage_XboxApp_Error_CanNotFind:I = 0x7f07072d

.field public static final MainPage_Connection_State_Connected:I = 0x7f07072e

.field public static final MainPage_Connection_State_Connected_Tablet:I = 0x7f07072f

.field public static final MainPage_Connection_State_Connecting:I = 0x7f070730

.field public static final MainPage_Connection_State_Connecting_Tablet:I = 0x7f070731

.field public static final MainPage_Connection_State_Disconnected:I = 0x7f070732

.field public static final MainPage_Connection_State_NoNetwork:I = 0x7f070733

.field public static final MediaDetails_Format:I = 0x7f070734

.field public static final MediaState_DragInstruction:I = 0x7f070735

.field public static final MessageDialog_Cancel:I = 0x7f070736

.field public static final MessageDialog_Confirm:I = 0x7f070737

.field public static final MessageDialog_OK:I = 0x7f070738

.field public static final Message_Detail_Message:I = 0x7f070739

.field public static final Messages_Action_ReportPerson:I = 0x7f07073a

.field public static final Messages_Action_SendPartyInvite:I = 0x7f07073b

.field public static final Messages_BlockButton:I = 0x7f07073c

.field public static final Messages_BlockUserConfirmation_DialogBody:I = 0x7f07073d

.field public static final Messages_BlockUserConfirmation_DialogTitle:I = 0x7f07073e

.field public static final Messages_ComposeMessage_AddRecipientHintText:I = 0x7f07073f

.field public static final Messages_ComposeMessage_CancelButton:I = 0x7f070740

.field public static final Messages_ComposeMessage_CannotSendError:I = 0x7f070741

.field public static final Messages_ComposeMessage_CannotSendError_DialogTitle:I = 0x7f070742

.field public static final Messages_ComposeMessage_CharacterCount:I = 0x7f070743

.field public static final Messages_ComposeMessage_ChooseRecipients_DoneButton:I = 0x7f070744

.field public static final Messages_ComposeMessage_ChooseRecipients_HeaderTitle_Phone:I = 0x7f070745

.field public static final Messages_ComposeMessage_ChooseRecipients_HeaderTitle_Tablet:I = 0x7f070746

.field public static final Messages_ComposeMessage_ChooseRecipients_NumRemaining:I = 0x7f070747

.field public static final Messages_ComposeMessage_ChooseRecipients_NumSelected:I = 0x7f070748

.field public static final Messages_ComposeMessage_DiscardChanges_DialogBody:I = 0x7f070749

.field public static final Messages_ComposeMessage_DiscardChanges_DialogTitle:I = 0x7f07074a

.field public static final Messages_ComposeMessage_EnterMessageHintText:I = 0x7f07074b

.field public static final Messages_ComposeMessage_GenericError:I = 0x7f07074c

.field public static final Messages_ComposeMessage_SendButton:I = 0x7f07074d

.field public static final Messages_ComposeMessage_Sending:I = 0x7f07074e

.field public static final Messages_ConfirmDeleteMessage:I = 0x7f07074f

.field public static final Messages_Copy_Text:I = 0x7f070750

.field public static final Messages_DeleteMessageButton:I = 0x7f070751

.field public static final Messages_Error_AttachmentFailed:I = 0x7f070752

.field public static final Messages_Error_BadReputation_DialogBody:I = 0x7f070753

.field public static final Messages_Error_BadReputation_DialogBody_ClubCreation:I = 0x7f070754

.field public static final Messages_Error_BadReputation_DialogBody_URL:I = 0x7f070755

.field public static final Messages_Error_BadReputation_DialogTitle:I = 0x7f070756

.field public static final Messages_Error_BadReputation_GetMoreInfo:I = 0x7f070757

.field public static final Messages_Error_FailedToBlockUser:I = 0x7f070758

.field public static final Messages_Error_FailedToDeleteMessage:I = 0x7f070759

.field public static final Messages_Error_FailedToUnblockUser:I = 0x7f07075a

.field public static final Messages_Error_NoMessages:I = 0x7f07075b

.field public static final Messages_Error_ViewUnsupportedLegacyMessage:I = 0x7f07075c

.field public static final Messages_Error_ViewUnsupportedMessage:I = 0x7f07075d

.field public static final Messages_HeaderTitle_Phone:I = 0x7f07075e

.field public static final Messages_HeaderTitle_Tablet:I = 0x7f07075f

.field public static final Messages_MessageDetail_Deleting:I = 0x7f070760

.field public static final Messages_NewMessageButton:I = 0x7f070761

.field public static final Messages_NewMessage_HeaderTitle_Phone:I = 0x7f070762

.field public static final Messages_NewMessage_HeaderTitle_Tablet:I = 0x7f070763

.field public static final Messages_ReplyMessageButton:I = 0x7f070764

.field public static final Messages_ReplyMessage_HeaderTitle_Phone:I = 0x7f070765

.field public static final Messages_ReplyMessage_HeaderTitle_Tablet:I = 0x7f070766

.field public static final Messages_Report_Hint:I = 0x7f070767

.field public static final Messages_Report_Text:I = 0x7f070768

.field public static final Messages_Report_Title:I = 0x7f070769

.field public static final Messages_SectionHeaderAllMessages_Tablet:I = 0x7f07076a

.field public static final Messages_UnblockButton:I = 0x7f07076b

.field public static final Messages_ViewProfileButton:I = 0x7f07076c

.field public static final Messages_WhatsNew_Messaging_BugFix:I = 0x7f07076d

.field public static final Minute_Text:I = 0x7f07076e

.field public static final NOWPLAYING_COMPANION_BUTTON_TEXT:I = 0x7f07076f

.field public static final NOWPLAYING_HELP_BUTTON_TEXT:I = 0x7f070770

.field public static final NOWPLAYING_TVGUIDE_BUTTON_TEXT:I = 0x7f070771

.field public static final Narrator_AButton:I = 0x7f070772

.field public static final Narrator_AddRecipient:I = 0x7f070773

.field public static final Narrator_AddtoFavorites:I = 0x7f070774

.field public static final Narrator_ArrowDown:I = 0x7f070775

.field public static final Narrator_ArrowLeft:I = 0x7f070776

.field public static final Narrator_ArrowRight:I = 0x7f070777

.field public static final Narrator_ArrowUp:I = 0x7f070778

.field public static final Narrator_BButton:I = 0x7f070779

.field public static final Narrator_BackIcon:I = 0x7f07077a

.field public static final Narrator_BlueButton:I = 0x7f07077b

.field public static final Narrator_ChannelDown:I = 0x7f07077c

.field public static final Narrator_ChannelUp:I = 0x7f07077d

.field public static final Narrator_ClearText:I = 0x7f07077e

.field public static final Narrator_Close:I = 0x7f07077f

.field public static final Narrator_ConnectIcon_Connected:I = 0x7f070780

.field public static final Narrator_ConnectIcon_Connecting:I = 0x7f070781

.field public static final Narrator_ConnectIcon_Disconnected:I = 0x7f070782

.field public static final Narrator_FFW:I = 0x7f070783

.field public static final Narrator_FeedbackIcon:I = 0x7f070784

.field public static final Narrator_GotoLive:I = 0x7f070785

.field public static final Narrator_GreenButton:I = 0x7f070786

.field public static final Narrator_IEPlayHere:I = 0x7f070787

.field public static final Narrator_InfoButton:I = 0x7f070788

.field public static final Narrator_InfoIcon:I = 0x7f070789

.field public static final Narrator_InputButton:I = 0x7f07078a

.field public static final Narrator_MenuButton:I = 0x7f07078b

.field public static final Narrator_MoreIcon:I = 0x7f07078c

.field public static final Narrator_Mute:I = 0x7f07078d

.field public static final Narrator_NewMessage:I = 0x7f07078e

.field public static final Narrator_OnOff:I = 0x7f07078f

.field public static final Narrator_PageDown:I = 0x7f070790

.field public static final Narrator_PageUp:I = 0x7f070791

.field public static final Narrator_PauseIcon:I = 0x7f070792

.field public static final Narrator_PlayIcon:I = 0x7f070793

.field public static final Narrator_PlayIconOneGuide:I = 0x7f070794

.field public static final Narrator_PowerButton:I = 0x7f070795

.field public static final Narrator_PreviousChannel:I = 0x7f070796

.field public static final Narrator_RWD:I = 0x7f070797

.field public static final Narrator_RecordButton:I = 0x7f070798

.field public static final Narrator_RedButton:I = 0x7f070799

.field public static final Narrator_RefreshIcon:I = 0x7f07079a

.field public static final Narrator_RemoteIcon:I = 0x7f07079b

.field public static final Narrator_RemovefromFavorties:I = 0x7f07079c

.field public static final Narrator_SearchIcon:I = 0x7f07079d

.field public static final Narrator_Seek:I = 0x7f07079e

.field public static final Narrator_SelectButton:I = 0x7f07079f

.field public static final Narrator_SkipBack:I = 0x7f0707a0

.field public static final Narrator_SkipForward:I = 0x7f0707a1

.field public static final Narrator_Unmute:I = 0x7f0707a2

.field public static final Narrator_ViewButton:I = 0x7f0707a3

.field public static final Narrator_VolumeDown:I = 0x7f0707a4

.field public static final Narrator_VolumeIcon:I = 0x7f0707a5

.field public static final Narrator_VolumeUp:I = 0x7f0707a6

.field public static final Narrator_XButton:I = 0x7f0707a7

.field public static final Narrator_XboxButton:I = 0x7f0707a8

.field public static final Narrator_YButton:I = 0x7f0707a9

.field public static final Narrator_YellowButton:I = 0x7f0707aa

.field public static final NetworkTest_CancelTest:I = 0x7f0707ab

.field public static final NetworkTest_GenericError:I = 0x7f0707ac

.field public static final NetworkTest_NeedStopGameStreamingError:I = 0x7f0707ad

.field public static final NetworkTest_NotProceed:I = 0x7f0707ae

.field public static final NetworkTest_Proceed:I = 0x7f0707af

.field public static final NetworkTest_RebootConsole:I = 0x7f0707b0

.field public static final NetworkTest_ResumeTest:I = 0x7f0707b1

.field public static final NetworkTest_RetryTest:I = 0x7f0707b2

.field public static final NetworkTest_StopGameStreamingWarning:I = 0x7f0707b3

.field public static final Notification_Inbox_Error_Generic:I = 0x7f0707b4

.field public static final Notification_Inbox_Title:I = 0x7f0707b5

.field public static final Notifications_Permission_Denied:I = 0x7f0707b6

.field public static final NowPlayingAlbumTitle:I = 0x7f0707b7

.field public static final NowPlayingAppTitle:I = 0x7f0707b8

.field public static final NowPlayingDashTitle:I = 0x7f0707b9

.field public static final NowPlayingErrorTitle:I = 0x7f0707ba

.field public static final NowPlayingGameTitle:I = 0x7f0707bb

.field public static final NowPlayingMovieTitle:I = 0x7f0707bc

.field public static final NowPlayingTVTitle:I = 0x7f0707bd

.field public static final NowPlaying_CannotLaunchInDisconnected:I = 0x7f0707be

.field public static final NowPlaying_CannotPlay360:I = 0x7f0707bf

.field public static final NowPlaying_ChangeTitleQuestion:I = 0x7f0707c0

.field public static final NowPlaying_DiscoverEPGText:I = 0x7f0707c1

.field public static final NowPlaying_MoreInfo:I = 0x7f0707c2

.field public static final NowPlaying_Title_Phone:I = 0x7f0707c3

.field public static final NowPlaying_Title_Tablet:I = 0x7f0707c4

.field public static final NowPlaying_TrackSubtitleTablet:I = 0x7f0707c5

.field public static final NowPlaying_Unavailable:I = 0x7f0707c6

.field public static final OK_Text:I = 0x7f0707c7

.field public static final OOBE_Adjust_Daylight_Saving:I = 0x7f0707c8

.field public static final OOBE_Back:I = 0x7f0707c9

.field public static final OOBE_Background_Title:I = 0x7f0707ca

.field public static final OOBE_Cancel:I = 0x7f0707cb

.field public static final OOBE_Cancel_Alert_Title:I = 0x7f0707cc

.field public static final OOBE_Choose_Time_Zone:I = 0x7f0707cd

.field public static final OOBE_Console_Set_Up:I = 0x7f0707ce

.field public static final OOBE_Console_Set_Up_Button_Title:I = 0x7f0707cf

.field public static final OOBE_Console_Set_Up_Details:I = 0x7f0707d0

.field public static final OOBE_Console_Setup_Complete:I = 0x7f0707d1

.field public static final OOBE_Console_Setup_Complete_Details_Line_One:I = 0x7f0707d2

.field public static final OOBE_Console_Setup_Complete_Details_Line_Two:I = 0x7f0707d3

.field public static final OOBE_Do_Not_Show_Again:I = 0x7f0707d4

.field public static final OOBE_Done:I = 0x7f0707d5

.field public static final OOBE_Energy_Saving_Detail_Line_One:I = 0x7f0707d6

.field public static final OOBE_Energy_Saving_Detail_Line_Three:I = 0x7f0707d7

.field public static final OOBE_Energy_Saving_Detail_Line_Two:I = 0x7f0707d8

.field public static final OOBE_Energy_Saving_Title:I = 0x7f0707d9

.field public static final OOBE_Enter_Code:I = 0x7f0707da

.field public static final OOBE_Enter_Code_Detail:I = 0x7f0707db

.field public static final OOBE_Enter_Code_Subdetail:I = 0x7f0707dc

.field public static final OOBE_Finish:I = 0x7f0707dd

.field public static final OOBE_Finished_Setup_On_Console_Body:I = 0x7f0707de

.field public static final OOBE_Finished_Setup_On_Console_Title:I = 0x7f0707df

.field public static final OOBE_FirstPage_SwipeText:I = 0x7f0707e0

.field public static final OOBE_FirstPage_SwipeText_2:I = 0x7f0707e1

.field public static final OOBE_FirstPage_SwipeText_Phone:I = 0x7f0707e2

.field public static final OOBE_FirstPage_Title:I = 0x7f0707e3

.field public static final OOBE_FirstPage_Title_Phone:I = 0x7f0707e4

.field public static final OOBE_FirstRun_ContentTitle:I = 0x7f0707e5

.field public static final OOBE_Get_Started:I = 0x7f0707e6

.field public static final OOBE_Instant_On_Detail_Line_One:I = 0x7f0707e7

.field public static final OOBE_Instant_On_Detail_Line_Three:I = 0x7f0707e8

.field public static final OOBE_Instant_On_Detail_Line_Two:I = 0x7f0707e9

.field public static final OOBE_Instant_On_Title:I = 0x7f0707ea

.field public static final OOBE_Keep_Console_Up_To_Date:I = 0x7f0707eb

.field public static final OOBE_Keep_Games_Up_To_Date:I = 0x7f0707ec

.field public static final OOBE_LastPage_DescriptionText:I = 0x7f0707ed

.field public static final OOBE_LastPage_DoneText:I = 0x7f0707ee

.field public static final OOBE_LastPage_TitleText:I = 0x7f0707ef

.field public static final OOBE_Next:I = 0x7f0707f0

.field public static final OOBE_Page1_Description:I = 0x7f0707f1

.field public static final OOBE_Page1_Title:I = 0x7f0707f2

.field public static final OOBE_Page1_Title_Phone:I = 0x7f0707f3

.field public static final OOBE_Page2_Description:I = 0x7f0707f4

.field public static final OOBE_Page2_Title:I = 0x7f0707f5

.field public static final OOBE_Page3_Description:I = 0x7f0707f6

.field public static final OOBE_Page3_Title:I = 0x7f0707f7

.field public static final OOBE_Page4_Description:I = 0x7f0707f8

.field public static final OOBE_Page4_Title:I = 0x7f0707f9

.field public static final OOBE_Power_Settings:I = 0x7f0707fa

.field public static final OOBE_Service_Error_Body:I = 0x7f0707fb

.field public static final OOBE_Service_Error_Exit_Off_Console:I = 0x7f0707fc

.field public static final OOBE_Service_Error_Title:I = 0x7f0707fd

.field public static final OOBE_Session_Code_Error:I = 0x7f0707fe

.field public static final OOBE_Settings_Console_Setup:I = 0x7f0707ff

.field public static final OOBE_Settings_Console_Setup_Subtitle:I = 0x7f070800

.field public static final OOBE_Settings_Welcome_To_Xbox:I = 0x7f070801

.field public static final OOBE_Settings_Welcome_To_Xbox_Subtitle:I = 0x7f070802

.field public static final OOBE_SignIn_Next:I = 0x7f070803

.field public static final OOBE_SignIn_Phone:I = 0x7f070804

.field public static final OOBE_Skip:I = 0x7f070805

.field public static final OOBE_Skip_Title:I = 0x7f070806

.field public static final OOBE_Timezone_Abu_Dhabi_Muscat:I = 0x7f070807

.field public static final OOBE_Timezone_Adelaide:I = 0x7f070808

.field public static final OOBE_Timezone_Alaska:I = 0x7f070809

.field public static final OOBE_Timezone_Aleutian_Islands:I = 0x7f07080a

.field public static final OOBE_Timezone_Amman:I = 0x7f07080b

.field public static final OOBE_Timezone_Amsterdam_Berlin_Bern_Rome_Stockholm_Vienna:I = 0x7f07080c

.field public static final OOBE_Timezone_Anadyr_Petropavlovsk_Kamchatsky:I = 0x7f07080d

.field public static final OOBE_Timezone_Araguaina:I = 0x7f07080e

.field public static final OOBE_Timezone_Arizona:I = 0x7f07080f

.field public static final OOBE_Timezone_Ashgabat_Tashkent:I = 0x7f070810

.field public static final OOBE_Timezone_Astana:I = 0x7f070811

.field public static final OOBE_Timezone_Astrakhan_Ulyanovsk:I = 0x7f070812

.field public static final OOBE_Timezone_Asuncion:I = 0x7f070813

.field public static final OOBE_Timezone_Athens_Bucharest:I = 0x7f070814

.field public static final OOBE_Timezone_Atlantic_Time_Canada:I = 0x7f070815

.field public static final OOBE_Timezone_Auckland_Wellington:I = 0x7f070816

.field public static final OOBE_Timezone_Azores:I = 0x7f070817

.field public static final OOBE_Timezone_Baghdad:I = 0x7f070818

.field public static final OOBE_Timezone_Baja_California:I = 0x7f070819

.field public static final OOBE_Timezone_Baku:I = 0x7f07081a

.field public static final OOBE_Timezone_Bangkok_Hanoi_Jakarta:I = 0x7f07081b

.field public static final OOBE_Timezone_Barnaul_Gorno_Altaysk:I = 0x7f07081c

.field public static final OOBE_Timezone_Beijing_Chongqing_Hong_Kong_Urumqi:I = 0x7f07081d

.field public static final OOBE_Timezone_Beirut:I = 0x7f07081e

.field public static final OOBE_Timezone_Belgrade_Bratislava_Budapest_Ljubljana_Prague:I = 0x7f07081f

.field public static final OOBE_Timezone_Bogota_Lima_Quito_Rio_Branco:I = 0x7f070820

.field public static final OOBE_Timezone_Bougainville_Island:I = 0x7f070821

.field public static final OOBE_Timezone_Brasilia:I = 0x7f070822

.field public static final OOBE_Timezone_Brisbane:I = 0x7f070823

.field public static final OOBE_Timezone_Brussels_Copenhagen_Madrid_Paris:I = 0x7f070824

.field public static final OOBE_Timezone_Cabo_Verde_Is:I = 0x7f070825

.field public static final OOBE_Timezone_Cairo:I = 0x7f070826

.field public static final OOBE_Timezone_Canberra_Melbourne_Sydney:I = 0x7f070827

.field public static final OOBE_Timezone_Caracas:I = 0x7f070828

.field public static final OOBE_Timezone_Casablanca:I = 0x7f070829

.field public static final OOBE_Timezone_Cayenne_Fortaleza:I = 0x7f07082a

.field public static final OOBE_Timezone_Central_America:I = 0x7f07082b

.field public static final OOBE_Timezone_Central_Time_US_Canada:I = 0x7f07082c

.field public static final OOBE_Timezone_Chatham_Islands:I = 0x7f07082d

.field public static final OOBE_Timezone_Chennai_Kolkata_Mumbai_New_Delhi:I = 0x7f07082e

.field public static final OOBE_Timezone_Chetumal:I = 0x7f07082f

.field public static final OOBE_Timezone_Chihuahua_La_Paz_Mazatlan:I = 0x7f070830

.field public static final OOBE_Timezone_Chisinau:I = 0x7f070831

.field public static final OOBE_Timezone_Chita:I = 0x7f070832

.field public static final OOBE_Timezone_Chokurdakh:I = 0x7f070833

.field public static final OOBE_Timezone_City_of_Buenos_Aires:I = 0x7f070834

.field public static final OOBE_Timezone_Coordinated_Universal_Time:I = 0x7f070835

.field public static final OOBE_Timezone_Coordinated_Universal_Time_02:I = 0x7f070836

.field public static final OOBE_Timezone_Coordinated_Universal_Time_08:I = 0x7f070837

.field public static final OOBE_Timezone_Coordinated_Universal_Time_09:I = 0x7f070838

.field public static final OOBE_Timezone_Coordinated_Universal_Time_11:I = 0x7f070839

.field public static final OOBE_Timezone_Coordinated_Universal_Time_12:I = 0x7f07083a

.field public static final OOBE_Timezone_Coordinated_Universal_Time_13:I = 0x7f07083b

.field public static final OOBE_Timezone_Cuiaba:I = 0x7f07083c

.field public static final OOBE_Timezone_Damascus:I = 0x7f07083d

.field public static final OOBE_Timezone_Darwin:I = 0x7f07083e

.field public static final OOBE_Timezone_Dhaka:I = 0x7f07083f

.field public static final OOBE_Timezone_Dublin_Edinburgh_Lisbon_London:I = 0x7f070840

.field public static final OOBE_Timezone_Easter_Island:I = 0x7f070841

.field public static final OOBE_Timezone_Eastern_Time_US_Canada:I = 0x7f070842

.field public static final OOBE_Timezone_Ekaterinburg:I = 0x7f070843

.field public static final OOBE_Timezone_Eucla:I = 0x7f070844

.field public static final OOBE_Timezone_Fiji:I = 0x7f070845

.field public static final OOBE_Timezone_Gaza_Hebron:I = 0x7f070846

.field public static final OOBE_Timezone_Georgetown_La_Paz_Manaus_San_Juan:I = 0x7f070847

.field public static final OOBE_Timezone_Greenland:I = 0x7f070848

.field public static final OOBE_Timezone_Guadalajara_Mexico_City_Monterrey:I = 0x7f070849

.field public static final OOBE_Timezone_Guam_Port_Moresby:I = 0x7f07084a

.field public static final OOBE_Timezone_Haiti:I = 0x7f07084b

.field public static final OOBE_Timezone_Harare_Pretoria:I = 0x7f07084c

.field public static final OOBE_Timezone_Havanah:I = 0x7f07084d

.field public static final OOBE_Timezone_Hawaii:I = 0x7f07084e

.field public static final OOBE_Timezone_Helsinki_Kyiv_Riga_Sofia_Tallinn_Vilnius:I = 0x7f07084f

.field public static final OOBE_Timezone_Hobart:I = 0x7f070850

.field public static final OOBE_Timezone_Hovd:I = 0x7f070851

.field public static final OOBE_Timezone_Indiana_East:I = 0x7f070852

.field public static final OOBE_Timezone_International_Date_Line_West:I = 0x7f070853

.field public static final OOBE_Timezone_Irkutsk:I = 0x7f070854

.field public static final OOBE_Timezone_Islamabad_Karachi:I = 0x7f070855

.field public static final OOBE_Timezone_Istanbul:I = 0x7f070856

.field public static final OOBE_Timezone_Izhevsk_Samara:I = 0x7f070857

.field public static final OOBE_Timezone_Jerusalem:I = 0x7f070858

.field public static final OOBE_Timezone_Kabul:I = 0x7f070859

.field public static final OOBE_Timezone_Kaliningrad:I = 0x7f07085a

.field public static final OOBE_Timezone_Kathmandu:I = 0x7f07085b

.field public static final OOBE_Timezone_Kiritimati_Island:I = 0x7f07085c

.field public static final OOBE_Timezone_Krasnoyarsk:I = 0x7f07085d

.field public static final OOBE_Timezone_Kuala_Lumpur_Singapore:I = 0x7f07085e

.field public static final OOBE_Timezone_Kuwait_Riyadh:I = 0x7f07085f

.field public static final OOBE_Timezone_Lord_Howe_Island:I = 0x7f070860

.field public static final OOBE_Timezone_Magadan:I = 0x7f070861

.field public static final OOBE_Timezone_Marquesas_Islands:I = 0x7f070862

.field public static final OOBE_Timezone_Minsk:I = 0x7f070863

.field public static final OOBE_Timezone_Monrovia_Reykjavik:I = 0x7f070864

.field public static final OOBE_Timezone_Montevideo:I = 0x7f070865

.field public static final OOBE_Timezone_Moscow_St_Petersburg_Volgograd:I = 0x7f070866

.field public static final OOBE_Timezone_Mountain_Time_US_Canada:I = 0x7f070867

.field public static final OOBE_Timezone_Nairobi:I = 0x7f070868

.field public static final OOBE_Timezone_Newfoundland:I = 0x7f070869

.field public static final OOBE_Timezone_Norfolk_Island:I = 0x7f07086a

.field public static final OOBE_Timezone_Novosibirsk:I = 0x7f07086b

.field public static final OOBE_Timezone_Nukualofa:I = 0x7f07086c

.field public static final OOBE_Timezone_Omsk:I = 0x7f07086d

.field public static final OOBE_Timezone_Osaka_Sapporo_Tokyo:I = 0x7f07086e

.field public static final OOBE_Timezone_Pacific_Time_US_Canada:I = 0x7f07086f

.field public static final OOBE_Timezone_Perth:I = 0x7f070870

.field public static final OOBE_Timezone_Port_Louis:I = 0x7f070871

.field public static final OOBE_Timezone_Pyongyang:I = 0x7f070872

.field public static final OOBE_Timezone_Saint_Pierre_and_Miquelon:I = 0x7f070873

.field public static final OOBE_Timezone_Sakhalin:I = 0x7f070874

.field public static final OOBE_Timezone_Salvador:I = 0x7f070875

.field public static final OOBE_Timezone_Samoa:I = 0x7f070876

.field public static final OOBE_Timezone_Santiago:I = 0x7f070877

.field public static final OOBE_Timezone_Sarajevo_Skopje_Warsaw_Zagreb:I = 0x7f070878

.field public static final OOBE_Timezone_Saratov:I = 0x7f070879

.field public static final OOBE_Timezone_Saskatchewan:I = 0x7f07087a

.field public static final OOBE_Timezone_Seoul:I = 0x7f07087b

.field public static final OOBE_Timezone_Solomon_Is_New_Caledonia:I = 0x7f07087c

.field public static final OOBE_Timezone_Sri_Jayawardenepura:I = 0x7f07087d

.field public static final OOBE_Timezone_Taipei:I = 0x7f07087e

.field public static final OOBE_Timezone_Tehran:I = 0x7f07087f

.field public static final OOBE_Timezone_Tibilis:I = 0x7f070880

.field public static final OOBE_Timezone_Tomsk:I = 0x7f070881

.field public static final OOBE_Timezone_Tripoli:I = 0x7f070882

.field public static final OOBE_Timezone_Turks_and_Caicos:I = 0x7f070883

.field public static final OOBE_Timezone_Ulaanbaatar:I = 0x7f070884

.field public static final OOBE_Timezone_Vladivostok:I = 0x7f070885

.field public static final OOBE_Timezone_West_Central_Africa:I = 0x7f070886

.field public static final OOBE_Timezone_Windhoek:I = 0x7f070887

.field public static final OOBE_Timezone_Yakutsk:I = 0x7f070888

.field public static final OOBE_Timezone_Yangon_Rangoon:I = 0x7f070889

.field public static final OOBE_Timezone_Yeravan:I = 0x7f07088a

.field public static final OOBE_Update_Settings:I = 0x7f07088b

.field public static final OOBE_What_Is_New_Text:I = 0x7f07088c

.field public static final OOBE_XboxApp_Background_Title:I = 0x7f07088d

.field public static final OOBE_XboxApp_FirstPage_Title_Phone:I = 0x7f07088e

.field public static final OOBE_XboxApp_LastPage_DoneText:I = 0x7f07088f

.field public static final OOBE_XboxApp_Page3_Description:I = 0x7f070890

.field public static final OVERVIEW_HEADER:I = 0x7f070891

.field public static final Off:I = 0x7f070892

.field public static final On:I = 0x7f070893

.field public static final PARENTAL_RATING:I = 0x7f070894

.field public static final PARENTAL_RATING_AUSTRALIATV_AV15:I = 0x7f070895

.field public static final PARENTAL_RATING_AUSTRALIATV_C:I = 0x7f070896

.field public static final PARENTAL_RATING_AUSTRALIATV_G:I = 0x7f070897

.field public static final PARENTAL_RATING_AUSTRALIATV_M:I = 0x7f070898

.field public static final PARENTAL_RATING_AUSTRALIATV_MA15:I = 0x7f070899

.field public static final PARENTAL_RATING_AUSTRALIATV_P:I = 0x7f07089a

.field public static final PARENTAL_RATING_AUSTRALIATV_PG:I = 0x7f07089b

.field public static final PARENTAL_RATING_AUSTRALIATV_R18:I = 0x7f07089c

.field public static final PARENTAL_RATING_AUSTRALIA_E:I = 0x7f07089d

.field public static final PARENTAL_RATING_AUSTRALIA_G:I = 0x7f07089e

.field public static final PARENTAL_RATING_AUSTRALIA_M:I = 0x7f07089f

.field public static final PARENTAL_RATING_AUSTRALIA_MA15:I = 0x7f0708a0

.field public static final PARENTAL_RATING_AUSTRALIA_PG:I = 0x7f0708a1

.field public static final PARENTAL_RATING_AUSTRALIA_R18:I = 0x7f0708a2

.field public static final PARENTAL_RATING_AUSTRALIA_X18:I = 0x7f0708a3

.field public static final PARENTAL_RATING_AUSTRIA_0:I = 0x7f0708a4

.field public static final PARENTAL_RATING_AUSTRIA_10:I = 0x7f0708a5

.field public static final PARENTAL_RATING_AUSTRIA_12:I = 0x7f0708a6

.field public static final PARENTAL_RATING_AUSTRIA_14:I = 0x7f0708a7

.field public static final PARENTAL_RATING_AUSTRIA_16:I = 0x7f0708a8

.field public static final PARENTAL_RATING_AUSTRIA_18:I = 0x7f0708a9

.field public static final PARENTAL_RATING_AUSTRIA_6:I = 0x7f0708aa

.field public static final PARENTAL_RATING_BRAZILTV_10:I = 0x7f0708ab

.field public static final PARENTAL_RATING_BRAZILTV_12:I = 0x7f0708ac

.field public static final PARENTAL_RATING_BRAZILTV_14:I = 0x7f0708ad

.field public static final PARENTAL_RATING_BRAZILTV_16:I = 0x7f0708ae

.field public static final PARENTAL_RATING_BRAZILTV_18:I = 0x7f0708af

.field public static final PARENTAL_RATING_BRAZILTV_ER:I = 0x7f0708b0

.field public static final PARENTAL_RATING_BRAZILTV_L:I = 0x7f0708b1

.field public static final PARENTAL_RATING_BRAZIL_10:I = 0x7f0708b2

.field public static final PARENTAL_RATING_BRAZIL_12:I = 0x7f0708b3

.field public static final PARENTAL_RATING_BRAZIL_14:I = 0x7f0708b4

.field public static final PARENTAL_RATING_BRAZIL_16:I = 0x7f0708b5

.field public static final PARENTAL_RATING_BRAZIL_18:I = 0x7f0708b6

.field public static final PARENTAL_RATING_BRAZIL_ER:I = 0x7f0708b7

.field public static final PARENTAL_RATING_BRAZIL_L:I = 0x7f0708b8

.field public static final PARENTAL_RATING_CANADATV_14:I = 0x7f0708b9

.field public static final PARENTAL_RATING_CANADATV_18:I = 0x7f0708ba

.field public static final PARENTAL_RATING_CANADATV_C:I = 0x7f0708bb

.field public static final PARENTAL_RATING_CANADATV_C8:I = 0x7f0708bc

.field public static final PARENTAL_RATING_CANADATV_E:I = 0x7f0708bd

.field public static final PARENTAL_RATING_CANADATV_G:I = 0x7f0708be

.field public static final PARENTAL_RATING_CANADATV_PG:I = 0x7f0708bf

.field public static final PARENTAL_RATING_CANADA_14:I = 0x7f0708c0

.field public static final PARENTAL_RATING_CANADA_18:I = 0x7f0708c1

.field public static final PARENTAL_RATING_CANADA_E:I = 0x7f0708c2

.field public static final PARENTAL_RATING_CANADA_G:I = 0x7f0708c3

.field public static final PARENTAL_RATING_CANADA_PG:I = 0x7f0708c4

.field public static final PARENTAL_RATING_CANADA_R:I = 0x7f0708c5

.field public static final PARENTAL_RATING_CERO_A:I = 0x7f0708c6

.field public static final PARENTAL_RATING_CERO_B:I = 0x7f0708c7

.field public static final PARENTAL_RATING_CERO_C:I = 0x7f0708c8

.field public static final PARENTAL_RATING_CERO_D:I = 0x7f0708c9

.field public static final PARENTAL_RATING_CERO_Z:I = 0x7f0708ca

.field public static final PARENTAL_RATING_DENMARK_11:I = 0x7f0708cb

.field public static final PARENTAL_RATING_DENMARK_15:I = 0x7f0708cc

.field public static final PARENTAL_RATING_DENMARK_7:I = 0x7f0708cd

.field public static final PARENTAL_RATING_DENMARK_A:I = 0x7f0708ce

.field public static final PARENTAL_RATING_DESCRIPTOR_0:I = 0x7f0708cf

.field public static final PARENTAL_RATING_DESCRIPTOR_1:I = 0x7f0708d0

.field public static final PARENTAL_RATING_DESCRIPTOR_10:I = 0x7f0708d1

.field public static final PARENTAL_RATING_DESCRIPTOR_100:I = 0x7f0708d2

.field public static final PARENTAL_RATING_DESCRIPTOR_10001:I = 0x7f0708d3

.field public static final PARENTAL_RATING_DESCRIPTOR_10002:I = 0x7f0708d4

.field public static final PARENTAL_RATING_DESCRIPTOR_10003:I = 0x7f0708d5

.field public static final PARENTAL_RATING_DESCRIPTOR_10004:I = 0x7f0708d6

.field public static final PARENTAL_RATING_DESCRIPTOR_10005:I = 0x7f0708d7

.field public static final PARENTAL_RATING_DESCRIPTOR_10006:I = 0x7f0708d8

.field public static final PARENTAL_RATING_DESCRIPTOR_10007:I = 0x7f0708d9

.field public static final PARENTAL_RATING_DESCRIPTOR_10008:I = 0x7f0708da

.field public static final PARENTAL_RATING_DESCRIPTOR_10009:I = 0x7f0708db

.field public static final PARENTAL_RATING_DESCRIPTOR_10010:I = 0x7f0708dc

.field public static final PARENTAL_RATING_DESCRIPTOR_101:I = 0x7f0708dd

.field public static final PARENTAL_RATING_DESCRIPTOR_102:I = 0x7f0708de

.field public static final PARENTAL_RATING_DESCRIPTOR_103:I = 0x7f0708df

.field public static final PARENTAL_RATING_DESCRIPTOR_104:I = 0x7f0708e0

.field public static final PARENTAL_RATING_DESCRIPTOR_105:I = 0x7f0708e1

.field public static final PARENTAL_RATING_DESCRIPTOR_106:I = 0x7f0708e2

.field public static final PARENTAL_RATING_DESCRIPTOR_107:I = 0x7f0708e3

.field public static final PARENTAL_RATING_DESCRIPTOR_108:I = 0x7f0708e4

.field public static final PARENTAL_RATING_DESCRIPTOR_109:I = 0x7f0708e5

.field public static final PARENTAL_RATING_DESCRIPTOR_11:I = 0x7f0708e6

.field public static final PARENTAL_RATING_DESCRIPTOR_110:I = 0x7f0708e7

.field public static final PARENTAL_RATING_DESCRIPTOR_12:I = 0x7f0708e8

.field public static final PARENTAL_RATING_DESCRIPTOR_13:I = 0x7f0708e9

.field public static final PARENTAL_RATING_DESCRIPTOR_14:I = 0x7f0708ea

.field public static final PARENTAL_RATING_DESCRIPTOR_14000:I = 0x7f0708eb

.field public static final PARENTAL_RATING_DESCRIPTOR_14001:I = 0x7f0708ec

.field public static final PARENTAL_RATING_DESCRIPTOR_14002:I = 0x7f0708ed

.field public static final PARENTAL_RATING_DESCRIPTOR_14003:I = 0x7f0708ee

.field public static final PARENTAL_RATING_DESCRIPTOR_14004:I = 0x7f0708ef

.field public static final PARENTAL_RATING_DESCRIPTOR_14005:I = 0x7f0708f0

.field public static final PARENTAL_RATING_DESCRIPTOR_14006:I = 0x7f0708f1

.field public static final PARENTAL_RATING_DESCRIPTOR_15:I = 0x7f0708f2

.field public static final PARENTAL_RATING_DESCRIPTOR_16:I = 0x7f0708f3

.field public static final PARENTAL_RATING_DESCRIPTOR_17:I = 0x7f0708f4

.field public static final PARENTAL_RATING_DESCRIPTOR_18:I = 0x7f0708f5

.field public static final PARENTAL_RATING_DESCRIPTOR_19:I = 0x7f0708f6

.field public static final PARENTAL_RATING_DESCRIPTOR_2:I = 0x7f0708f7

.field public static final PARENTAL_RATING_DESCRIPTOR_20:I = 0x7f0708f8

.field public static final PARENTAL_RATING_DESCRIPTOR_21:I = 0x7f0708f9

.field public static final PARENTAL_RATING_DESCRIPTOR_22:I = 0x7f0708fa

.field public static final PARENTAL_RATING_DESCRIPTOR_2283:I = 0x7f0708fb

.field public static final PARENTAL_RATING_DESCRIPTOR_23:I = 0x7f0708fc

.field public static final PARENTAL_RATING_DESCRIPTOR_24:I = 0x7f0708fd

.field public static final PARENTAL_RATING_DESCRIPTOR_25:I = 0x7f0708fe

.field public static final PARENTAL_RATING_DESCRIPTOR_26:I = 0x7f0708ff

.field public static final PARENTAL_RATING_DESCRIPTOR_27:I = 0x7f070900

.field public static final PARENTAL_RATING_DESCRIPTOR_28:I = 0x7f070901

.field public static final PARENTAL_RATING_DESCRIPTOR_29:I = 0x7f070902

.field public static final PARENTAL_RATING_DESCRIPTOR_3:I = 0x7f070903

.field public static final PARENTAL_RATING_DESCRIPTOR_30:I = 0x7f070904

.field public static final PARENTAL_RATING_DESCRIPTOR_31:I = 0x7f070905

.field public static final PARENTAL_RATING_DESCRIPTOR_32:I = 0x7f070906

.field public static final PARENTAL_RATING_DESCRIPTOR_320010:I = 0x7f070907

.field public static final PARENTAL_RATING_DESCRIPTOR_320020:I = 0x7f070908

.field public static final PARENTAL_RATING_DESCRIPTOR_320030:I = 0x7f070909

.field public static final PARENTAL_RATING_DESCRIPTOR_320040:I = 0x7f07090a

.field public static final PARENTAL_RATING_DESCRIPTOR_320050:I = 0x7f07090b

.field public static final PARENTAL_RATING_DESCRIPTOR_320060:I = 0x7f07090c

.field public static final PARENTAL_RATING_DESCRIPTOR_33:I = 0x7f07090d

.field public static final PARENTAL_RATING_DESCRIPTOR_34:I = 0x7f07090e

.field public static final PARENTAL_RATING_DESCRIPTOR_35:I = 0x7f07090f

.field public static final PARENTAL_RATING_DESCRIPTOR_36:I = 0x7f070910

.field public static final PARENTAL_RATING_DESCRIPTOR_37:I = 0x7f070911

.field public static final PARENTAL_RATING_DESCRIPTOR_38:I = 0x7f070912

.field public static final PARENTAL_RATING_DESCRIPTOR_39:I = 0x7f070913

.field public static final PARENTAL_RATING_DESCRIPTOR_4:I = 0x7f070914

.field public static final PARENTAL_RATING_DESCRIPTOR_40:I = 0x7f070915

.field public static final PARENTAL_RATING_DESCRIPTOR_4000:I = 0x7f070916

.field public static final PARENTAL_RATING_DESCRIPTOR_4001:I = 0x7f070917

.field public static final PARENTAL_RATING_DESCRIPTOR_4002:I = 0x7f070918

.field public static final PARENTAL_RATING_DESCRIPTOR_4003:I = 0x7f070919

.field public static final PARENTAL_RATING_DESCRIPTOR_4004:I = 0x7f07091a

.field public static final PARENTAL_RATING_DESCRIPTOR_4005:I = 0x7f07091b

.field public static final PARENTAL_RATING_DESCRIPTOR_4006:I = 0x7f07091c

.field public static final PARENTAL_RATING_DESCRIPTOR_41:I = 0x7f07091d

.field public static final PARENTAL_RATING_DESCRIPTOR_42:I = 0x7f07091e

.field public static final PARENTAL_RATING_DESCRIPTOR_43:I = 0x7f07091f

.field public static final PARENTAL_RATING_DESCRIPTOR_44:I = 0x7f070920

.field public static final PARENTAL_RATING_DESCRIPTOR_45:I = 0x7f070921

.field public static final PARENTAL_RATING_DESCRIPTOR_46:I = 0x7f070922

.field public static final PARENTAL_RATING_DESCRIPTOR_47:I = 0x7f070923

.field public static final PARENTAL_RATING_DESCRIPTOR_48:I = 0x7f070924

.field public static final PARENTAL_RATING_DESCRIPTOR_49:I = 0x7f070925

.field public static final PARENTAL_RATING_DESCRIPTOR_4999:I = 0x7f070926

.field public static final PARENTAL_RATING_DESCRIPTOR_5:I = 0x7f070927

.field public static final PARENTAL_RATING_DESCRIPTOR_50:I = 0x7f070928

.field public static final PARENTAL_RATING_DESCRIPTOR_5000:I = 0x7f070929

.field public static final PARENTAL_RATING_DESCRIPTOR_5001:I = 0x7f07092a

.field public static final PARENTAL_RATING_DESCRIPTOR_5002:I = 0x7f07092b

.field public static final PARENTAL_RATING_DESCRIPTOR_5003:I = 0x7f07092c

.field public static final PARENTAL_RATING_DESCRIPTOR_5004:I = 0x7f07092d

.field public static final PARENTAL_RATING_DESCRIPTOR_5005:I = 0x7f07092e

.field public static final PARENTAL_RATING_DESCRIPTOR_5006:I = 0x7f07092f

.field public static final PARENTAL_RATING_DESCRIPTOR_51:I = 0x7f070930

.field public static final PARENTAL_RATING_DESCRIPTOR_52:I = 0x7f070931

.field public static final PARENTAL_RATING_DESCRIPTOR_53:I = 0x7f070932

.field public static final PARENTAL_RATING_DESCRIPTOR_54:I = 0x7f070933

.field public static final PARENTAL_RATING_DESCRIPTOR_55:I = 0x7f070934

.field public static final PARENTAL_RATING_DESCRIPTOR_56:I = 0x7f070935

.field public static final PARENTAL_RATING_DESCRIPTOR_57:I = 0x7f070936

.field public static final PARENTAL_RATING_DESCRIPTOR_58:I = 0x7f070937

.field public static final PARENTAL_RATING_DESCRIPTOR_59:I = 0x7f070938

.field public static final PARENTAL_RATING_DESCRIPTOR_6:I = 0x7f070939

.field public static final PARENTAL_RATING_DESCRIPTOR_60:I = 0x7f07093a

.field public static final PARENTAL_RATING_DESCRIPTOR_6000:I = 0x7f07093b

.field public static final PARENTAL_RATING_DESCRIPTOR_6001:I = 0x7f07093c

.field public static final PARENTAL_RATING_DESCRIPTOR_6002:I = 0x7f07093d

.field public static final PARENTAL_RATING_DESCRIPTOR_6003:I = 0x7f07093e

.field public static final PARENTAL_RATING_DESCRIPTOR_6004:I = 0x7f07093f

.field public static final PARENTAL_RATING_DESCRIPTOR_6005:I = 0x7f070940

.field public static final PARENTAL_RATING_DESCRIPTOR_6006:I = 0x7f070941

.field public static final PARENTAL_RATING_DESCRIPTOR_61:I = 0x7f070942

.field public static final PARENTAL_RATING_DESCRIPTOR_62:I = 0x7f070943

.field public static final PARENTAL_RATING_DESCRIPTOR_63:I = 0x7f070944

.field public static final PARENTAL_RATING_DESCRIPTOR_64:I = 0x7f070945

.field public static final PARENTAL_RATING_DESCRIPTOR_65:I = 0x7f070946

.field public static final PARENTAL_RATING_DESCRIPTOR_66:I = 0x7f070947

.field public static final PARENTAL_RATING_DESCRIPTOR_67:I = 0x7f070948

.field public static final PARENTAL_RATING_DESCRIPTOR_68:I = 0x7f070949

.field public static final PARENTAL_RATING_DESCRIPTOR_69:I = 0x7f07094a

.field public static final PARENTAL_RATING_DESCRIPTOR_7:I = 0x7f07094b

.field public static final PARENTAL_RATING_DESCRIPTOR_70:I = 0x7f07094c

.field public static final PARENTAL_RATING_DESCRIPTOR_71:I = 0x7f07094d

.field public static final PARENTAL_RATING_DESCRIPTOR_72:I = 0x7f07094e

.field public static final PARENTAL_RATING_DESCRIPTOR_73:I = 0x7f07094f

.field public static final PARENTAL_RATING_DESCRIPTOR_74:I = 0x7f070950

.field public static final PARENTAL_RATING_DESCRIPTOR_75:I = 0x7f070951

.field public static final PARENTAL_RATING_DESCRIPTOR_76:I = 0x7f070952

.field public static final PARENTAL_RATING_DESCRIPTOR_77:I = 0x7f070953

.field public static final PARENTAL_RATING_DESCRIPTOR_78:I = 0x7f070954

.field public static final PARENTAL_RATING_DESCRIPTOR_79:I = 0x7f070955

.field public static final PARENTAL_RATING_DESCRIPTOR_8:I = 0x7f070956

.field public static final PARENTAL_RATING_DESCRIPTOR_9:I = 0x7f070957

.field public static final PARENTAL_RATING_ESRB_AO:I = 0x7f070958

.field public static final PARENTAL_RATING_ESRB_E:I = 0x7f070959

.field public static final PARENTAL_RATING_ESRB_E10:I = 0x7f07095a

.field public static final PARENTAL_RATING_ESRB_E10_RP:I = 0x7f07095b

.field public static final PARENTAL_RATING_ESRB_EC:I = 0x7f07095c

.field public static final PARENTAL_RATING_ESRB_M:I = 0x7f07095d

.field public static final PARENTAL_RATING_ESRB_RP:I = 0x7f07095e

.field public static final PARENTAL_RATING_ESRB_T:I = 0x7f07095f

.field public static final PARENTAL_RATING_EXPLICIT_EXPLICIT:I = 0x7f070960

.field public static final PARENTAL_RATING_FINLAND_K11:I = 0x7f070961

.field public static final PARENTAL_RATING_FINLAND_K13:I = 0x7f070962

.field public static final PARENTAL_RATING_FINLAND_K15:I = 0x7f070963

.field public static final PARENTAL_RATING_FINLAND_K18:I = 0x7f070964

.field public static final PARENTAL_RATING_FINLAND_K3:I = 0x7f070965

.field public static final PARENTAL_RATING_FINLAND_K7:I = 0x7f070966

.field public static final PARENTAL_RATING_FINLAND_ST:I = 0x7f070967

.field public static final PARENTAL_RATING_FPB_10:I = 0x7f070968

.field public static final PARENTAL_RATING_FPB_13:I = 0x7f070969

.field public static final PARENTAL_RATING_FPB_16:I = 0x7f07096a

.field public static final PARENTAL_RATING_FPB_18:I = 0x7f07096b

.field public static final PARENTAL_RATING_FPB_A:I = 0x7f07096c

.field public static final PARENTAL_RATING_FPB_PG:I = 0x7f07096d

.field public static final PARENTAL_RATING_FRANCETV_10:I = 0x7f07096e

.field public static final PARENTAL_RATING_FRANCETV_12:I = 0x7f07096f

.field public static final PARENTAL_RATING_FRANCETV_16:I = 0x7f070970

.field public static final PARENTAL_RATING_FRANCETV_18:I = 0x7f070971

.field public static final PARENTAL_RATING_FRANCE_12:I = 0x7f070972

.field public static final PARENTAL_RATING_FRANCE_16:I = 0x7f070973

.field public static final PARENTAL_RATING_FRANCE_18:I = 0x7f070974

.field public static final PARENTAL_RATING_FRANCE_U:I = 0x7f070975

.field public static final PARENTAL_RATING_FRANCE_X:I = 0x7f070976

.field public static final PARENTAL_RATING_GAME:I = 0x7f070977

.field public static final PARENTAL_RATING_GERMANY_FSK0:I = 0x7f070978

.field public static final PARENTAL_RATING_GERMANY_FSK12:I = 0x7f070979

.field public static final PARENTAL_RATING_GERMANY_FSK16:I = 0x7f07097a

.field public static final PARENTAL_RATING_GERMANY_FSK18:I = 0x7f07097b

.field public static final PARENTAL_RATING_GERMANY_FSK6:I = 0x7f07097c

.field public static final PARENTAL_RATING_GERMANY_SPIO_JK:I = 0x7f07097d

.field public static final PARENTAL_RATING_GRB_12:I = 0x7f07097e

.field public static final PARENTAL_RATING_GRB_15:I = 0x7f07097f

.field public static final PARENTAL_RATING_GRB_18:I = 0x7f070980

.field public static final PARENTAL_RATING_GRB_ALL:I = 0x7f070981

.field public static final PARENTAL_RATING_IRELAND_12A:I = 0x7f070982

.field public static final PARENTAL_RATING_IRELAND_15A:I = 0x7f070983

.field public static final PARENTAL_RATING_IRELAND_16:I = 0x7f070984

.field public static final PARENTAL_RATING_IRELAND_18:I = 0x7f070985

.field public static final PARENTAL_RATING_IRELAND_G:I = 0x7f070986

.field public static final PARENTAL_RATING_IRELAND_PG:I = 0x7f070987

.field public static final PARENTAL_RATING_ITALYTV_14:I = 0x7f070988

.field public static final PARENTAL_RATING_ITALYTV_ADULT:I = 0x7f070989

.field public static final PARENTAL_RATING_ITALYTV_MINOR:I = 0x7f07098a

.field public static final PARENTAL_RATING_ITALY_T:I = 0x7f07098b

.field public static final PARENTAL_RATING_ITALY_VM14:I = 0x7f07098c

.field public static final PARENTAL_RATING_ITALY_VM18:I = 0x7f07098d

.field public static final PARENTAL_RATING_JAPAN_1:I = 0x7f07098e

.field public static final PARENTAL_RATING_JAPAN_2:I = 0x7f07098f

.field public static final PARENTAL_RATING_JAPAN_3:I = 0x7f070990

.field public static final PARENTAL_RATING_JAPAN_4:I = 0x7f070991

.field public static final PARENTAL_RATING_JAPAN_5:I = 0x7f070992

.field public static final PARENTAL_RATING_JAPAN_6:I = 0x7f070993

.field public static final PARENTAL_RATING_JAPAN_7:I = 0x7f070994

.field public static final PARENTAL_RATING_JAPAN_8:I = 0x7f070995

.field public static final PARENTAL_RATING_KMRB_12:I = 0x7f070996

.field public static final PARENTAL_RATING_KMRB_15:I = 0x7f070997

.field public static final PARENTAL_RATING_KMRB_18:I = 0x7f070998

.field public static final PARENTAL_RATING_KMRB_ALL:I = 0x7f070999

.field public static final PARENTAL_RATING_MEXICOTV_A:I = 0x7f07099a

.field public static final PARENTAL_RATING_MEXICOTV_AA:I = 0x7f07099b

.field public static final PARENTAL_RATING_MEXICOTV_B:I = 0x7f07099c

.field public static final PARENTAL_RATING_MEXICOTV_B15:I = 0x7f07099d

.field public static final PARENTAL_RATING_MEXICOTV_C:I = 0x7f07099e

.field public static final PARENTAL_RATING_MEXICOTV_D:I = 0x7f07099f

.field public static final PARENTAL_RATING_MEXICO_A:I = 0x7f0709a0

.field public static final PARENTAL_RATING_MEXICO_AA:I = 0x7f0709a1

.field public static final PARENTAL_RATING_MEXICO_B:I = 0x7f0709a2

.field public static final PARENTAL_RATING_MEXICO_B15:I = 0x7f0709a3

.field public static final PARENTAL_RATING_MEXICO_C:I = 0x7f0709a4

.field public static final PARENTAL_RATING_MEXICO_D:I = 0x7f0709a5

.field public static final PARENTAL_RATING_MPAA_AO:I = 0x7f0709a6

.field public static final PARENTAL_RATING_MPAA_G:I = 0x7f0709a7

.field public static final PARENTAL_RATING_MPAA_NC17:I = 0x7f0709a8

.field public static final PARENTAL_RATING_MPAA_PG:I = 0x7f0709a9

.field public static final PARENTAL_RATING_MPAA_PG13:I = 0x7f0709aa

.field public static final PARENTAL_RATING_MPAA_R:I = 0x7f0709ab

.field public static final PARENTAL_RATING_MPAA_X:I = 0x7f0709ac

.field public static final PARENTAL_RATING_NETHERLANDS_12:I = 0x7f0709ad

.field public static final PARENTAL_RATING_NETHERLANDS_16:I = 0x7f0709ae

.field public static final PARENTAL_RATING_NETHERLANDS_6:I = 0x7f0709af

.field public static final PARENTAL_RATING_NETHERLANDS_9:I = 0x7f0709b0

.field public static final PARENTAL_RATING_NETHERLANDS_AL:I = 0x7f0709b1

.field public static final PARENTAL_RATING_NEW_ZEALAND_G:I = 0x7f0709b2

.field public static final PARENTAL_RATING_NEW_ZEALAND_MATURE:I = 0x7f0709b3

.field public static final PARENTAL_RATING_NEW_ZEALAND_PG:I = 0x7f0709b4

.field public static final PARENTAL_RATING_NEW_ZEALAND_R:I = 0x7f0709b5

.field public static final PARENTAL_RATING_NEW_ZEALAND_R13:I = 0x7f0709b6

.field public static final PARENTAL_RATING_NEW_ZEALAND_R15:I = 0x7f0709b7

.field public static final PARENTAL_RATING_NEW_ZEALAND_R16:I = 0x7f0709b8

.field public static final PARENTAL_RATING_NEW_ZEALAND_R18:I = 0x7f0709b9

.field public static final PARENTAL_RATING_NORWAY_11:I = 0x7f0709ba

.field public static final PARENTAL_RATING_NORWAY_15:I = 0x7f0709bb

.field public static final PARENTAL_RATING_NORWAY_18:I = 0x7f0709bc

.field public static final PARENTAL_RATING_NORWAY_7:I = 0x7f0709bd

.field public static final PARENTAL_RATING_NORWAY_A:I = 0x7f0709be

.field public static final PARENTAL_RATING_NOT_RATED:I = 0x7f0709bf

.field public static final PARENTAL_RATING_OFLC_AU_E:I = 0x7f0709c0

.field public static final PARENTAL_RATING_OFLC_AU_G:I = 0x7f0709c1

.field public static final PARENTAL_RATING_OFLC_AU_G8:I = 0x7f0709c2

.field public static final PARENTAL_RATING_OFLC_AU_M:I = 0x7f0709c3

.field public static final PARENTAL_RATING_OFLC_AU_M15:I = 0x7f0709c4

.field public static final PARENTAL_RATING_OFLC_AU_MA15:I = 0x7f0709c5

.field public static final PARENTAL_RATING_OFLC_AU_PG:I = 0x7f0709c6

.field public static final PARENTAL_RATING_OFLC_AU_R18:I = 0x7f0709c7

.field public static final PARENTAL_RATING_OFLC_AU_REFUSED:I = 0x7f0709c8

.field public static final PARENTAL_RATING_OFLC_AU_X18:I = 0x7f0709c9

.field public static final PARENTAL_RATING_OFLC_NZ_G:I = 0x7f0709ca

.field public static final PARENTAL_RATING_OFLC_NZ_M:I = 0x7f0709cb

.field public static final PARENTAL_RATING_OFLC_NZ_PG:I = 0x7f0709cc

.field public static final PARENTAL_RATING_OFLC_NZ_R:I = 0x7f0709cd

.field public static final PARENTAL_RATING_OFLC_NZ_R13:I = 0x7f0709ce

.field public static final PARENTAL_RATING_OFLC_NZ_R15:I = 0x7f0709cf

.field public static final PARENTAL_RATING_OFLC_NZ_R16:I = 0x7f0709d0

.field public static final PARENTAL_RATING_OFLC_NZ_R18:I = 0x7f0709d1

.field public static final PARENTAL_RATING_PEGIBBFC_12:I = 0x7f0709d2

.field public static final PARENTAL_RATING_PEGIBBFC_15:I = 0x7f0709d3

.field public static final PARENTAL_RATING_PEGIBBFC_16:I = 0x7f0709d4

.field public static final PARENTAL_RATING_PEGIBBFC_18:I = 0x7f0709d5

.field public static final PARENTAL_RATING_PEGIBBFC_3:I = 0x7f0709d6

.field public static final PARENTAL_RATING_PEGIBBFC_7:I = 0x7f0709d7

.field public static final PARENTAL_RATING_PEGIBBFC_PG:I = 0x7f0709d8

.field public static final PARENTAL_RATING_PEGIBBFC_U:I = 0x7f0709d9

.field public static final PARENTAL_RATING_PEGI_11:I = 0x7f0709da

.field public static final PARENTAL_RATING_PEGI_12:I = 0x7f0709db

.field public static final PARENTAL_RATING_PEGI_15:I = 0x7f0709dc

.field public static final PARENTAL_RATING_PEGI_16:I = 0x7f0709dd

.field public static final PARENTAL_RATING_PEGI_18:I = 0x7f0709de

.field public static final PARENTAL_RATING_PEGI_3:I = 0x7f0709df

.field public static final PARENTAL_RATING_PEGI_4:I = 0x7f0709e0

.field public static final PARENTAL_RATING_PEGI_6:I = 0x7f0709e1

.field public static final PARENTAL_RATING_PEGI_7:I = 0x7f0709e2

.field public static final PARENTAL_RATING_SINGAPORE_AGE_ADVISORY:I = 0x7f0709e3

.field public static final PARENTAL_RATING_SINGAPORE_GENERAL:I = 0x7f0709e4

.field public static final PARENTAL_RATING_SINGAPORE_MATURE_18:I = 0x7f0709e5

.field public static final PARENTAL_RATING_SPAINTV_13:I = 0x7f0709e6

.field public static final PARENTAL_RATING_SPAINTV_18:I = 0x7f0709e7

.field public static final PARENTAL_RATING_SPAINTV_7:I = 0x7f0709e8

.field public static final PARENTAL_RATING_SPAINTV_GP:I = 0x7f0709e9

.field public static final PARENTAL_RATING_SPAINTV_MINOR:I = 0x7f0709ea

.field public static final PARENTAL_RATING_SPAINTV_X:I = 0x7f0709eb

.field public static final PARENTAL_RATING_SPAIN_13:I = 0x7f0709ec

.field public static final PARENTAL_RATING_SPAIN_15:I = 0x7f0709ed

.field public static final PARENTAL_RATING_SPAIN_16:I = 0x7f0709ee

.field public static final PARENTAL_RATING_SPAIN_18:I = 0x7f0709ef

.field public static final PARENTAL_RATING_SPAIN_7:I = 0x7f0709f0

.field public static final PARENTAL_RATING_SPAIN_E:I = 0x7f0709f1

.field public static final PARENTAL_RATING_SPAIN_TP:I = 0x7f0709f2

.field public static final PARENTAL_RATING_SPAIN_X:I = 0x7f0709f3

.field public static final PARENTAL_RATING_SWEDEN_11:I = 0x7f0709f4

.field public static final PARENTAL_RATING_SWEDEN_15:I = 0x7f0709f5

.field public static final PARENTAL_RATING_SWEDEN_7:I = 0x7f0709f6

.field public static final PARENTAL_RATING_SWEDEN_BTL:I = 0x7f0709f7

.field public static final PARENTAL_RATING_TAIWAN_GENERAL:I = 0x7f0709f8

.field public static final PARENTAL_RATING_TAIWAN_PG:I = 0x7f0709f9

.field public static final PARENTAL_RATING_TAIWAN_PROTECTED:I = 0x7f0709fa

.field public static final PARENTAL_RATING_TAIWAN_RESTRICTED:I = 0x7f0709fb

.field public static final PARENTAL_RATING_UK_12:I = 0x7f0709fc

.field public static final PARENTAL_RATING_UK_12A:I = 0x7f0709fd

.field public static final PARENTAL_RATING_UK_15:I = 0x7f0709fe

.field public static final PARENTAL_RATING_UK_18:I = 0x7f0709ff

.field public static final PARENTAL_RATING_UK_PG:I = 0x7f070a00

.field public static final PARENTAL_RATING_UK_R18:I = 0x7f070a01

.field public static final PARENTAL_RATING_UK_U:I = 0x7f070a02

.field public static final PARENTAL_RATING_UK_UC:I = 0x7f070a03

.field public static final PARENTAL_RATING_USK_12:I = 0x7f070a04

.field public static final PARENTAL_RATING_USK_16:I = 0x7f070a05

.field public static final PARENTAL_RATING_USK_6:I = 0x7f070a06

.field public static final PARENTAL_RATING_USK_ALL_AGES:I = 0x7f070a07

.field public static final PARENTAL_RATING_USK_NO_YOUTH:I = 0x7f070a08

.field public static final PARENTAL_RATING_USTV_14:I = 0x7f070a09

.field public static final PARENTAL_RATING_USTV_G:I = 0x7f070a0a

.field public static final PARENTAL_RATING_USTV_MA:I = 0x7f070a0b

.field public static final PARENTAL_RATING_USTV_PG:I = 0x7f070a0c

.field public static final PARENTAL_RATING_USTV_Y:I = 0x7f070a0d

.field public static final PARENTAL_RATING_USTV_Y7:I = 0x7f070a0e

.field public static final PARENTAL_RATING_USTV_Y7FV:I = 0x7f070a0f

.field public static final PARENTAL_UNRATED:I = 0x7f070a10

.field public static final PARENTAL_UNRATED_OR_PENDING:I = 0x7f070a11

.field public static final Party_Block:I = 0x7f070a12

.field public static final Party_Broadcast:I = 0x7f070a13

.field public static final Party_Broadcast_Audio_Included:I = 0x7f070a14

.field public static final Party_Broadcast_Audio_Not_Included:I = 0x7f070a15

.field public static final Party_Broadcast_Dismiss_Option:I = 0x7f070a16

.field public static final Party_Broadcast_Icon_Narrator:I = 0x7f070a17

.field public static final Party_Broadcast_Include_My_Audio_Option:I = 0x7f070a18

.field public static final Party_Change_Audio_Devices:I = 0x7f070a19

.field public static final Party_Chat:I = 0x7f070a1a

.field public static final Party_Chat_Disconnected:I = 0x7f070a1b

.field public static final Party_Connecting:I = 0x7f070a1c

.field public static final Party_Controls:I = 0x7f070a1d

.field public static final Party_Controls_Invite_Only:I = 0x7f070a1e

.field public static final Party_Controls_Joinable:I = 0x7f070a1f

.field public static final Party_Disconnected:I = 0x7f070a20

.field public static final Party_Ejected:I = 0x7f070a21

.field public static final Party_Error_Blocked:I = 0x7f070a22

.field public static final Party_Error_Enforcement_Url:I = 0x7f070a23

.field public static final Party_Error_Full:I = 0x7f070a24

.field public static final Party_Error_Generic:I = 0x7f070a25

.field public static final Party_Error_Invite_Generic:I = 0x7f070a26

.field public static final Party_Error_Invite_Too_Many:I = 0x7f070a27

.field public static final Party_Error_Members_Only:I = 0x7f070a28

.field public static final Party_Error_Network:I = 0x7f070a29

.field public static final Party_Error_Other_Party:I = 0x7f070a2a

.field public static final Party_Error_Privacy:I = 0x7f070a2b

.field public static final Party_Failed_To_OptIn_Broadcast_Error:I = 0x7f070a2c

.field public static final Party_Invite:I = 0x7f070a2d

.field public static final Party_Invite_Only_Multiple:I = 0x7f070a2e

.field public static final Party_Invite_Only_Single:I = 0x7f070a2f

.field public static final Party_Invite_To_Party:I = 0x7f070a30

.field public static final Party_Join:I = 0x7f070a31

.field public static final Party_Joinable_Multiple:I = 0x7f070a32

.field public static final Party_Joinable_Single:I = 0x7f070a33

.field public static final Party_Leader_Icon_Narrator:I = 0x7f070a34

.field public static final Party_Leave:I = 0x7f070a35

.field public static final Party_Member_Icon_Narrator:I = 0x7f070a36

.field public static final Party_Mute:I = 0x7f070a37

.field public static final Party_Mute_Me:I = 0x7f070a38

.field public static final Party_Notification_Broadcast:I = 0x7f070a39

.field public static final Party_Notification_Game_Invite:I = 0x7f070a3a

.field public static final Party_Notification_Invite:I = 0x7f070a3b

.field public static final Party_Notification_Join:I = 0x7f070a3c

.field public static final Party_Notification_Leave:I = 0x7f070a3d

.field public static final Party_Notification_Title:I = 0x7f070a3e

.field public static final Party_Owner:I = 0x7f070a3f

.field public static final Party_Profile:I = 0x7f070a40

.field public static final Party_Remove:I = 0x7f070a41

.field public static final Party_Show_Chat:I = 0x7f070a42

.field public static final Party_Start:I = 0x7f070a43

.field public static final Party_TextChat_Gamerpic_Narrator:I = 0x7f070a44

.field public static final Party_Title:I = 0x7f070a45

.field public static final Party_Unmute:I = 0x7f070a46

.field public static final Party_Unmute_Me:I = 0x7f070a47

.field public static final Party_View:I = 0x7f070a48

.field public static final Party_You_Removed:I = 0x7f070a49

.field public static final PeopleHub_Info_Customize:I = 0x7f070a4a

.field public static final PeopleHub_Info_No_Followers:I = 0x7f070a4b

.field public static final PeopleHub_Info_You_And:I = 0x7f070a4c

.field public static final PeoplePicker_ChooseFriends:I = 0x7f070a4d

.field public static final People_Filter_Clubs_First:I = 0x7f070a4e

.field public static final People_Filter_Friends_First:I = 0x7f070a4f

.field public static final People_NoSuggestionsDescription:I = 0x7f070a50

.field public static final Peoplehub_Info_Joinable_Header:I = 0x7f070a51

.field public static final Picker_DoneButton_Text:I = 0x7f070a52

.field public static final Pins_AddFailed:I = 0x7f070a53

.field public static final Pins_DeleteFailed:I = 0x7f070a54

.field public static final Pins_Error:I = 0x7f070a55

.field public static final Pins_Error_MovePin:I = 0x7f070a56

.field public static final Pins_NoPins:I = 0x7f070a57

.field public static final Pins_PageTitle:I = 0x7f070a58

.field public static final Pins_PageTitle_Phone:I = 0x7f070a59

.field public static final Pins_PageTitle_Tablet:I = 0x7f070a5a

.field public static final Pins_TutorialText:I = 0x7f070a5b

.field public static final Pins_TutorialTitle:I = 0x7f070a5c

.field public static final PivotHeaderFontSize:I = 0x7f070a5d

.field public static final PopularGames_Header:I = 0x7f070a5e

.field public static final PopularGames_Header_Tablet:I = 0x7f070a5f

.field public static final PopularGames_NoData:I = 0x7f070a60

.field public static final PopularGames_Playing_1Friend:I = 0x7f070a61

.field public static final PopularGames_Playing_NFriends:I = 0x7f070a62

.field public static final PopularGames_Plays_1Friend:I = 0x7f070a63

.field public static final PopularGames_Plays_NFriends:I = 0x7f070a64

.field public static final PopularGames_ServiceError:I = 0x7f070a65

.field public static final ProfileButton_Broadcasting:I = 0x7f070a66

.field public static final ProfileButton_Broadcasting_Details:I = 0x7f070a67

.field public static final ProfileButton_WatchBroadcast:I = 0x7f070a68

.field public static final ProfileCard_Block_Error:I = 0x7f070a69

.field public static final ProfileCard_Block_Subtext:I = 0x7f070a6a

.field public static final ProfileCard_Inappropriate_LFG_Application:I = 0x7f070a6b

.field public static final ProfileCard_Inappropriate_LFG_Content:I = 0x7f070a6c

.field public static final ProfileCard_Report:I = 0x7f070a6d

.field public static final ProfileCard_Report_Activity_Feed_Breaks_Club_Rules:I = 0x7f070a6e

.field public static final ProfileCard_Report_BioLoc:I = 0x7f070a6f

.field public static final ProfileCard_Report_Cheating:I = 0x7f070a70

.field public static final ProfileCard_Report_Comment_Breaks_Club_Rules:I = 0x7f070a71

.field public static final ProfileCard_Report_Content_Breaks_Club_Rules:I = 0x7f070a72

.field public static final ProfileCard_Report_Error:I = 0x7f070a73

.field public static final ProfileCard_Report_InappropriateClub:I = 0x7f070a74

.field public static final ProfileCard_Report_InappropriateComment:I = 0x7f070a75

.field public static final ProfileCard_Report_InappropriateFeedItem:I = 0x7f070a76

.field public static final ProfileCard_Report_InappropriateGameClip:I = 0x7f070a77

.field public static final ProfileCard_Report_InappropriateGamerTag:I = 0x7f070a78

.field public static final ProfileCard_Report_InappropriateScreenshot:I = 0x7f070a79

.field public static final ProfileCard_Report_InfoString:I = 0x7f070a7a

.field public static final ProfileCard_Report_Message:I = 0x7f070a7b

.field public static final ProfileCard_Report_Message_Breaks_Club_Rules:I = 0x7f070a7c

.field public static final ProfileCard_Report_PlayerName:I = 0x7f070a7d

.field public static final ProfileCard_Report_PlayerPic:I = 0x7f070a7e

.field public static final ProfileCard_Report_QuitEarly:I = 0x7f070a7f

.field public static final ProfileCard_Report_Reason_Title:I = 0x7f070a80

.field public static final ProfileCard_Report_SelectReason:I = 0x7f070a81

.field public static final ProfileCard_Report_SendReportTo_Title:I = 0x7f070a82

.field public static final ProfileCard_Report_Spam:I = 0x7f070a83

.field public static final ProfileCard_Report_SubmitButton:I = 0x7f070a84

.field public static final ProfileCard_Report_Success:I = 0x7f070a85

.field public static final ProfileCard_Report_SuccessSubtext:I = 0x7f070a86

.field public static final ProfileCard_Report_TextReasonHint:I = 0x7f070a87

.field public static final ProfileCard_Report_Unsporting:I = 0x7f070a88

.field public static final ProfileCard_Report_VoiceComm:I = 0x7f070a89

.field public static final ProfileRecentTitle:I = 0x7f070a8a

.field public static final ProfileTitle:I = 0x7f070a8b

.field public static final Profile_Achievements_HeaderTitle_Phone:I = 0x7f070a8c

.field public static final Profile_Achievements_HeaderTitle_Tablet:I = 0x7f070a8d

.field public static final Profile_Achievements_NoData_Me:I = 0x7f070a8e

.field public static final Profile_Achievements_NoData_You:I = 0x7f070a8f

.field public static final Profile_BioHeader:I = 0x7f070a90

.field public static final Profile_ChangeFriendship:I = 0x7f070a91

.field public static final Profile_ChannelGallery_Captures_HeaderTitle_Me_Tablet:I = 0x7f070a92

.field public static final Profile_ChannelGallery_Captures_HeaderTitle_You_Tablet:I = 0x7f070a93

.field public static final Profile_ChannelGallery_HeaderTitle_Me_Tablet:I = 0x7f070a94

.field public static final Profile_ChannelGallery_HeaderTitle_You_Tablet:I = 0x7f070a95

.field public static final Profile_Channel_Capture_HeaderTitle_Me_Phone:I = 0x7f070a96

.field public static final Profile_Channel_Capture_HeaderTitle_Me_Tablet:I = 0x7f070a97

.field public static final Profile_Channel_Capture_HeaderTitle_You_Phone:I = 0x7f070a98

.field public static final Profile_Channel_Capture_HeaderTitle_You_Tablet:I = 0x7f070a99

.field public static final Profile_Channel_HeaderTitle_Me_Phone:I = 0x7f070a9a

.field public static final Profile_Channel_HeaderTitle_Me_Tablet:I = 0x7f070a9b

.field public static final Profile_Channel_HeaderTitle_You_Phone:I = 0x7f070a9c

.field public static final Profile_Channel_HeaderTitle_You_Tablet:I = 0x7f070a9d

.field public static final Profile_Channel_NoData_Me:I = 0x7f070a9e

.field public static final Profile_Channel_NoData_Screenshots_Me:I = 0x7f070a9f

.field public static final Profile_Channel_NoData_Screenshots_You:I = 0x7f070aa0

.field public static final Profile_Channel_NoData_You:I = 0x7f070aa1

.field public static final Profile_Channel_NoSavedData_Me:I = 0x7f070aa2

.field public static final Profile_Channel_NoSavedData_Screenshots_Me:I = 0x7f070aa3

.field public static final Profile_Channel_NoSavedData_Screenshots_Them:I = 0x7f070aa4

.field public static final Profile_Channel_NoSavedData_Them:I = 0x7f070aa5

.field public static final Profile_Channel_Views:I = 0x7f070aa6

.field public static final Profile_Clubs_NoData:I = 0x7f070aa7

.field public static final Profile_Clubs_NoInvitable:I = 0x7f070aa8

.field public static final Profile_FollowFilters_MeBlockedEmptyState:I = 0x7f070aa9

.field public static final Profile_FollowFilters_MeFollowersEmptyState:I = 0x7f070aaa

.field public static final Profile_FollowFilters_MeFriendsEmptyState:I = 0x7f070aab

.field public static final Profile_FollowFilters_MeGamesEmptyState:I = 0x7f070aac

.field public static final Profile_FollowFilters_MeMutedEmptyState:I = 0x7f070aad

.field public static final Profile_FollowFilters_YouClubsEmptyState:I = 0x7f070aae

.field public static final Profile_FollowFilters_YouFriendsAndGamesEmptyState:I = 0x7f070aaf

.field public static final Profile_FollowFilters_YouFriendsEmptyState:I = 0x7f070ab0

.field public static final Profile_FollowFilters_YouGamesEmptyState:I = 0x7f070ab1

.field public static final Profile_Friends_Filter_Block:I = 0x7f070ab2

.field public static final Profile_Friends_Filter_Followers:I = 0x7f070ab3

.field public static final Profile_Friends_Filter_Friends:I = 0x7f070ab4

.field public static final Profile_Friends_Filter_FriendsAndGames:I = 0x7f070ab5

.field public static final Profile_Friends_Filter_Games:I = 0x7f070ab6

.field public static final Profile_Friends_Filter_Muted:I = 0x7f070ab7

.field public static final Profile_Friends_Gallery_HeaderTitle:I = 0x7f070ab8

.field public static final Profile_Friends_Gallery_NoData:I = 0x7f070ab9

.field public static final Profile_Friends_HeaderTitle_Phone:I = 0x7f070aba

.field public static final Profile_Friends_HeaderTitle_Tablet:I = 0x7f070abb

.field public static final Profile_Friends_Just_You_Count:I = 0x7f070abc

.field public static final Profile_Friends_NoData:I = 0x7f070abd

.field public static final Profile_GameDVR_Filter_YouGameClips_Recent:I = 0x7f070abe

.field public static final Profile_GameDVR_Filter_YouGameClips_Saved:I = 0x7f070abf

.field public static final Profile_GameDVR_Filter_YouMyScreenShots_Recent:I = 0x7f070ac0

.field public static final Profile_GameDVR_Filter_YouMyScreenShots_Saved:I = 0x7f070ac1

.field public static final Profile_LocationHeader:I = 0x7f070ac2

.field public static final Profile_Profile_AddFriend:I = 0x7f070ac3

.field public static final Profile_Profile_AddFriend_MaxReached_Message:I = 0x7f070ac4

.field public static final Profile_Profile_AddFriend_MaxReached_Title:I = 0x7f070ac5

.field public static final Profile_Profile_Block:I = 0x7f070ac6

.field public static final Profile_Profile_Failed_Add_Favorite:I = 0x7f070ac7

.field public static final Profile_Profile_Failed_Add_Friend:I = 0x7f070ac8

.field public static final Profile_Profile_Failed_Remove_Favorite:I = 0x7f070ac9

.field public static final Profile_Profile_Failed_Remove_Friend:I = 0x7f070aca

.field public static final Profile_Profile_Favorite:I = 0x7f070acb

.field public static final Profile_Profile_Followers:I = 0x7f070acc

.field public static final Profile_Profile_Following:I = 0x7f070acd

.field public static final Profile_Profile_FollowingAndFollowers:I = 0x7f070ace

.field public static final Profile_Profile_Friends:I = 0x7f070acf

.field public static final Profile_Profile_HeaderTitle:I = 0x7f070ad0

.field public static final Profile_Profile_RemoveFriend:I = 0x7f070ad1

.field public static final Profile_Profile_RemoveFriendConfirmation_DialogBody:I = 0x7f070ad2

.field public static final Profile_Profile_RemoveFriendConfirmation_DialogTitle:I = 0x7f070ad3

.field public static final Profile_Profile_SendMessage:I = 0x7f070ad4

.field public static final Profile_Profile_Unblock:I = 0x7f070ad5

.field public static final Profile_Profile_Unfavorite:I = 0x7f070ad6

.field public static final Profile_Recents_AppProgressText:I = 0x7f070ad7

.field public static final Profile_Recents_GameProgressText:I = 0x7f070ad8

.field public static final Profile_Recents_HeaderTitle_Phone:I = 0x7f070ad9

.field public static final Profile_Recents_HeaderTitle_Tablet:I = 0x7f070ada

.field public static final Profile_Recents_LastPlayedText:I = 0x7f070adb

.field public static final Profile_Recents_NoData_Me:I = 0x7f070adc

.field public static final Profile_Recents_NoData_You:I = 0x7f070add

.field public static final Profile_Recents_Stats_You:I = 0x7f070ade

.field public static final Profile_Reporting_Reported_Admin:I = 0x7f070adf

.field public static final Profile_Reporting_Reporting:I = 0x7f070ae0

.field public static final Profile_Reporting_Reporting_Admin:I = 0x7f070ae1

.field public static final Profile_Reporting_reported:I = 0x7f070ae2

.field public static final Profile_Reporting_text_out_of:I = 0x7f070ae3

.field public static final Profile_Reputation:I = 0x7f070ae4

.field public static final Profile_Reputation_AvoidMe:I = 0x7f070ae5

.field public static final Profile_Reputation_Good_Player:I = 0x7f070ae6

.field public static final Profile_Reputation_Needs_Improvement:I = 0x7f070ae7

.field public static final Profile_Reputation_Super_Star:I = 0x7f070ae8

.field public static final Profile_Reputation_Unknown:I = 0x7f070ae9

.field public static final ProviderOffer_Free:I = 0x7f070aea

.field public static final Purchase_AddToQueue:I = 0x7f070aeb

.field public static final Purchase_AddToQueue_Question:I = 0x7f070aec

.field public static final Purchase_Available_Header:I = 0x7f070aed

.field public static final Purchase_Buy:I = 0x7f070aee

.field public static final Purchase_Complete:I = 0x7f070aef

.field public static final Purchase_Complete_Collection:I = 0x7f070af0

.field public static final Purchase_Complete_Message:I = 0x7f070af1

.field public static final Purchase_Complete_Thanks:I = 0x7f070af2

.field public static final Purchase_Confirmation_1:I = 0x7f070af3

.field public static final Purchase_Confirmation_2:I = 0x7f070af4

.field public static final Purchase_Confirmation_3:I = 0x7f070af5

.field public static final Purchase_Confirmation_4:I = 0x7f070af6

.field public static final Purchase_Confirmation_5:I = 0x7f070af7

.field public static final Purchase_Confirmation_Consumable:I = 0x7f070af8

.field public static final Purchase_Confirmation_NoHome:I = 0x7f070af9

.field public static final Purchase_Confirmation_Queued:I = 0x7f070afa

.field public static final Purchase_Confirmation_SupportLink:I = 0x7f070afb

.field public static final Purchase_Done:I = 0x7f070afc

.field public static final Purchase_GetItFree:I = 0x7f070afd

.field public static final Purchase_Gold_Member_EA_Access_Free_With:I = 0x7f070afe

.field public static final Purchase_Gold_Member_Free_Play_Days:I = 0x7f070aff

.field public static final Purchase_Gold_Member_Free_With:I = 0x7f070b00

.field public static final Purchase_Preorder:I = 0x7f070b01

.field public static final Purchase_Purchased:I = 0x7f070b02

.field public static final Purchase_Redeem_A_Code:I = 0x7f070b03

.field public static final Purchase_Title_Purchased:I = 0x7f070b04

.field public static final PushNotification_Achievement_Body_Format_Android:I = 0x7f070b05

.field public static final PushNotification_Achievement_Voiceover_Format_Android:I = 0x7f070b06

.field public static final PushNotification_Broadcast_Format_Android:I = 0x7f070b07

.field public static final PushNotification_Broadcast_Format_iOS:I = 0x7f070b08

.field public static final PushNotification_Message_Format_Android:I = 0x7f070b09

.field public static final PushNotification_Message_Service:I = 0x7f070b0a

.field public static final PushNotification_Online_Format_Android:I = 0x7f070b0b

.field public static final PushNotification_Online_Format_iOS:I = 0x7f070b0c

.field public static final PushNotification_Shared_Content_Format_Android:I = 0x7f070b0d

.field public static final RECORD_THAT_ACCESSIBILITY:I = 0x7f070b0e

.field public static final RECORD_THAT_ERROR:I = 0x7f070b0f

.field public static final REVIEWS_HEADER:I = 0x7f070b10

.field public static final Radio_NoVideo_Error:I = 0x7f070b11

.field public static final Rate_App_Body:I = 0x7f070b12

.field public static final Rate_App_DontAskButton:I = 0x7f070b13

.field public static final Rate_App_Header:I = 0x7f070b14

.field public static final Rate_App_RemindButton:I = 0x7f070b15

.field public static final Rate_XboxApp_Body:I = 0x7f070b16

.field public static final RealNameSharing_AddFriendDescription:I = 0x7f070b17

.field public static final RealNameSharing_ChooseWhoSeesYourName:I = 0x7f070b18

.field public static final RealNameSharing_ChosenFriends:I = 0x7f070b19

.field public static final RealNameSharing_Edit:I = 0x7f070b1a

.field public static final RealNameSharing_EnforcementBanned_Description:I = 0x7f070b1b

.field public static final RealNameSharing_ErrorAddingFriend:I = 0x7f070b1c

.field public static final RealNameSharing_ErrorAtStartup_Description:I = 0x7f070b1d

.field public static final RealNameSharing_ErrorChangeRemove:I = 0x7f070b1e

.field public static final RealNameSharing_ErrorPartialFail:I = 0x7f070b1f

.field public static final RealNameSharing_ErrorRealNameSave:I = 0x7f070b20

.field public static final RealNameSharing_Error_SaveName:I = 0x7f070b21

.field public static final RealNameSharing_Error_SaveSharing:I = 0x7f070b22

.field public static final RealNameSharing_Everyone:I = 0x7f070b23

.field public static final RealNameSharing_FirstName:I = 0x7f070b24

.field public static final RealNameSharing_FriendsofFriends:I = 0x7f070b25

.field public static final RealNameSharing_GoBack:I = 0x7f070b26

.field public static final RealNameSharing_HowYouLook:I = 0x7f070b27

.field public static final RealNameSharing_HowYouLookDetail:I = 0x7f070b28

.field public static final RealNameSharing_HowYouLook_Description:I = 0x7f070b29

.field public static final RealNameSharing_LastName:I = 0x7f070b2a

.field public static final RealNameSharing_Next:I = 0x7f070b2b

.field public static final RealNameSharing_NoThanks:I = 0x7f070b2c

.field public static final RealNameSharing_Save:I = 0x7f070b2d

.field public static final RealNameSharing_SetUpNameSharing:I = 0x7f070b2e

.field public static final RealNameSharing_SettingsRealNameConcatentation:I = 0x7f070b2f

.field public static final RealNameSharing_Share:I = 0x7f070b30

.field public static final RealNameSharing_ShareWithFriendsOfFriends:I = 0x7f070b31

.field public static final RealNameSharing_ShareWithSettings:I = 0x7f070b32

.field public static final RealNameSharing_StartRealNameSharing:I = 0x7f070b33

.field public static final RealNameSharing_StartRealNameSharing_Description1:I = 0x7f070b34

.field public static final RealNameSharing_StartRealNameSharing_Description2:I = 0x7f070b35

.field public static final RealNameSharing_StartSharing:I = 0x7f070b36

.field public static final RealNameSharing_StopSharing:I = 0x7f070b37

.field public static final RealNameSharing_TryAgain:I = 0x7f070b38

.field public static final RealNameSharing_Update:I = 0x7f070b39

.field public static final RealNameSharing_UpdateRealNameSharing:I = 0x7f070b3a

.field public static final RealNameSharing_UpdateSettings:I = 0x7f070b3b

.field public static final RealNameSharing_UpdateSharing:I = 0x7f070b3c

.field public static final RealNameSharing_WhatYouChose:I = 0x7f070b3d

.field public static final RealNameSharing_toggleOff:I = 0x7f070b3e

.field public static final RealNameSharing_toggleOn:I = 0x7f070b3f

.field public static final RelatedItems_NoResultsFound:I = 0x7f070b40

.field public static final Related_Header:I = 0x7f070b41

.field public static final Remote_ConnectedTo:I = 0x7f070b42

.field public static final Remote_Select:I = 0x7f070b43

.field public static final Remote_Title:I = 0x7f070b44

.field public static final Remote_Universal_Title:I = 0x7f070b45

.field public static final Remote_Unsupported:I = 0x7f070b46

.field public static final Remote_Xbox_Title:I = 0x7f070b47

.field public static final ResourceFlowDirection:I = 0x7f070b48

.field public static final ResourceLanguage:I = 0x7f070b49

.field public static final Root_Drawer:I = 0x7f070b4a

.field public static final Screenshot_ChooseAction:I = 0x7f070b4b

.field public static final Screenshot_Save:I = 0x7f070b4c

.field public static final Screenshot_Saving:I = 0x7f070b4d

.field public static final Screenshot_Share:I = 0x7f070b4e

.field public static final Screenshot_Sharing:I = 0x7f070b4f

.field public static final Screenshots_Saved:I = 0x7f070b50

.field public static final Screenshots_SavedError:I = 0x7f070b51

.field public static final Screenshots_ShareError:I = 0x7f070b52

.field public static final SearchHelpText_BasicPrompt:I = 0x7f070b53

.field public static final SearchHelpText_Prompt1_Category1:I = 0x7f070b54

.field public static final SearchHelpText_Prompt1_Category2:I = 0x7f070b55

.field public static final SearchHelpText_Prompt1_Category3:I = 0x7f070b56

.field public static final SearchHelpText_Prompt2_Category1:I = 0x7f070b57

.field public static final SearchHelpText_Prompt2_Category2:I = 0x7f070b58

.field public static final SearchHelpText_Prompt2_Category3:I = 0x7f070b59

.field public static final SearchResultFilterItem_All:I = 0x7f070b5a

.field public static final SearchResultFilterItem_Apps:I = 0x7f070b5b

.field public static final SearchResultFilterItem_Games:I = 0x7f070b5c

.field public static final SearchResultFilterItem_Movies:I = 0x7f070b5d

.field public static final SearchResultFilterItem_Music:I = 0x7f070b5e

.field public static final SearchResultFilterItem_TV:I = 0x7f070b5f

.field public static final SearchResult_BusyIndicatorText:I = 0x7f070b60

.field public static final SearchResults_FilterHeader:I = 0x7f070b61

.field public static final SearchResults_NoResult:I = 0x7f070b62

.field public static final SearchResults_PageTitle:I = 0x7f070b63

.field public static final SearchResults_PageTitle_Phone:I = 0x7f070b64

.field public static final SearchSuggestions_Title:I = 0x7f070b65

.field public static final SearchTerms_Title:I = 0x7f070b66

.field public static final Search_DefaultText:I = 0x7f070b67

.field public static final Search_DetailsSeparator:I = 0x7f070b68

.field public static final Search_ErrorText:I = 0x7f070b69

.field public static final Search_PlaceHolderText:I = 0x7f070b6a

.field public static final Search_PlayOnXbox:I = 0x7f070b6b

.field public static final Send_ErrorText:I = 0x7f070b6c

.field public static final Service_ErrorText:I = 0x7f070b6d

.field public static final Service_ErrorTextBody_Post_ServiceFailure:I = 0x7f070b6e

.field public static final Service_ErrorTextBody_Share_ServiceFailure:I = 0x7f070b6f

.field public static final Settings_About_SectionHeader:I = 0x7f070b70

.field public static final Settings_About_Version:I = 0x7f070b71

.field public static final Settings_Account_SectionHeader:I = 0x7f070b72

.field public static final Settings_Chat_Notifications_Show_As_Hover:I = 0x7f070b73

.field public static final Settings_Chat_Notifications_Show_As_Title:I = 0x7f070b74

.field public static final Settings_Chat_Notifications_Show_As_Toast:I = 0x7f070b75

.field public static final Settings_Clear:I = 0x7f070b76

.field public static final Settings_Developer_SandboxId:I = 0x7f070b77

.field public static final Settings_Developer_SectionHeader:I = 0x7f070b78

.field public static final Settings_General_Header:I = 0x7f070b79

.field public static final Settings_GetStartedButton:I = 0x7f070b7a

.field public static final Settings_GetStartedDescription:I = 0x7f070b7b

.field public static final Settings_HelpAndPrivacyDescription_iOS:I = 0x7f070b7c

.field public static final Settings_HelpAndPrivacySectionTitle_iOS:I = 0x7f070b7d

.field public static final Settings_HelpButton:I = 0x7f070b7e

.field public static final Settings_Language_Chinese_CN:I = 0x7f070b7f

.field public static final Settings_Language_Chinese_TW:I = 0x7f070b80

.field public static final Settings_Language_Danish:I = 0x7f070b81

.field public static final Settings_Language_Default:I = 0x7f070b82

.field public static final Settings_Language_Description:I = 0x7f070b83

.field public static final Settings_Language_Dutch:I = 0x7f070b84

.field public static final Settings_Language_English_GB:I = 0x7f070b85

.field public static final Settings_Language_English_US:I = 0x7f070b86

.field public static final Settings_Language_Finnish:I = 0x7f070b87

.field public static final Settings_Language_French:I = 0x7f070b88

.field public static final Settings_Language_German:I = 0x7f070b89

.field public static final Settings_Language_Header:I = 0x7f070b8a

.field public static final Settings_Language_Italian:I = 0x7f070b8b

.field public static final Settings_Language_Japanese:I = 0x7f070b8c

.field public static final Settings_Language_Korean:I = 0x7f070b8d

.field public static final Settings_Language_Norwegian:I = 0x7f070b8e

.field public static final Settings_Language_Polish:I = 0x7f070b8f

.field public static final Settings_Language_Portuguese_BR:I = 0x7f070b90

.field public static final Settings_Language_Portuguese_PT:I = 0x7f070b91

.field public static final Settings_Language_Russian:I = 0x7f070b92

.field public static final Settings_Language_Spanish_ES:I = 0x7f070b93

.field public static final Settings_Language_Spanish_MX:I = 0x7f070b94

.field public static final Settings_Language_Swedish:I = 0x7f070b95

.field public static final Settings_Language_Title:I = 0x7f070b96

.field public static final Settings_Language_Turkish:I = 0x7f070b97

.field public static final Settings_Language_Warning:I = 0x7f070b98

.field public static final Settings_LinkedAccount_Title:I = 0x7f070b99

.field public static final Settings_Location_Argentina:I = 0x7f070b9a

.field public static final Settings_Location_Australia:I = 0x7f070b9b

.field public static final Settings_Location_Austria:I = 0x7f070b9c

.field public static final Settings_Location_Belgium:I = 0x7f070b9d

.field public static final Settings_Location_Brazil:I = 0x7f070b9e

.field public static final Settings_Location_Canada:I = 0x7f070b9f

.field public static final Settings_Location_Chile:I = 0x7f070ba0

.field public static final Settings_Location_China:I = 0x7f070ba1

.field public static final Settings_Location_Colombia:I = 0x7f070ba2

.field public static final Settings_Location_Czech_Republic:I = 0x7f070ba3

.field public static final Settings_Location_Denmark:I = 0x7f070ba4

.field public static final Settings_Location_Description:I = 0x7f070ba5

.field public static final Settings_Location_Finland:I = 0x7f070ba6

.field public static final Settings_Location_France:I = 0x7f070ba7

.field public static final Settings_Location_Germany:I = 0x7f070ba8

.field public static final Settings_Location_Greece:I = 0x7f070ba9

.field public static final Settings_Location_Hong_Kong:I = 0x7f070baa

.field public static final Settings_Location_Hungary:I = 0x7f070bab

.field public static final Settings_Location_India:I = 0x7f070bac

.field public static final Settings_Location_Ireland:I = 0x7f070bad

.field public static final Settings_Location_Israel:I = 0x7f070bae

.field public static final Settings_Location_Italy:I = 0x7f070baf

.field public static final Settings_Location_Japan:I = 0x7f070bb0

.field public static final Settings_Location_Mexico:I = 0x7f070bb1

.field public static final Settings_Location_Netherlands:I = 0x7f070bb2

.field public static final Settings_Location_New_Zealand:I = 0x7f070bb3

.field public static final Settings_Location_Norway:I = 0x7f070bb4

.field public static final Settings_Location_Poland:I = 0x7f070bb5

.field public static final Settings_Location_Portugal:I = 0x7f070bb6

.field public static final Settings_Location_Russia:I = 0x7f070bb7

.field public static final Settings_Location_Saudi_Arabia:I = 0x7f070bb8

.field public static final Settings_Location_Singapore:I = 0x7f070bb9

.field public static final Settings_Location_Slovakia:I = 0x7f070bba

.field public static final Settings_Location_South_Africa:I = 0x7f070bbb

.field public static final Settings_Location_South_Korea:I = 0x7f070bbc

.field public static final Settings_Location_Spain:I = 0x7f070bbd

.field public static final Settings_Location_Sweden:I = 0x7f070bbe

.field public static final Settings_Location_Switzerland:I = 0x7f070bbf

.field public static final Settings_Location_Taiwan:I = 0x7f070bc0

.field public static final Settings_Location_Title:I = 0x7f070bc1

.field public static final Settings_Location_Turkey:I = 0x7f070bc2

.field public static final Settings_Location_UAE:I = 0x7f070bc3

.field public static final Settings_Location_United_Kingdom:I = 0x7f070bc4

.field public static final Settings_Location_United_States:I = 0x7f070bc5

.field public static final Settings_Messages_Notifications_Show_As_Hover:I = 0x7f070bc6

.field public static final Settings_Messages_Notifications_Show_As_Title:I = 0x7f070bc7

.field public static final Settings_Messages_Notifications_Show_As_Toast:I = 0x7f070bc8

.field public static final Settings_Notification_Achievements_Description:I = 0x7f070bc9

.field public static final Settings_Notification_Achievements_Title:I = 0x7f070bca

.field public static final Settings_Notification_Broadcast_Description:I = 0x7f070bcb

.field public static final Settings_Notification_Broadcast_Title:I = 0x7f070bcc

.field public static final Settings_Notification_ClubDemoted_Description:I = 0x7f070bcd

.field public static final Settings_Notification_ClubDemoted_Title:I = 0x7f070bce

.field public static final Settings_Notification_ClubFollower_Description:I = 0x7f070bcf

.field public static final Settings_Notification_ClubFollower_Title:I = 0x7f070bd0

.field public static final Settings_Notification_ClubInvited_Description:I = 0x7f070bd1

.field public static final Settings_Notification_ClubInvited_Title:I = 0x7f070bd2

.field public static final Settings_Notification_ClubJoin_Description:I = 0x7f070bd3

.field public static final Settings_Notification_ClubJoin_Title:I = 0x7f070bd4

.field public static final Settings_Notification_ClubJoined_Description:I = 0x7f070bd5

.field public static final Settings_Notification_ClubJoined_Title:I = 0x7f070bd6

.field public static final Settings_Notification_ClubPromoted_Description:I = 0x7f070bd7

.field public static final Settings_Notification_ClubPromoted_Title:I = 0x7f070bd8

.field public static final Settings_Notification_ClubRecommended_Description:I = 0x7f070bd9

.field public static final Settings_Notification_ClubRecommended_Title:I = 0x7f070bda

.field public static final Settings_Notification_ClubReport_Description:I = 0x7f070bdb

.field public static final Settings_Notification_ClubReport_Title:I = 0x7f070bdc

.field public static final Settings_Notification_ClubRequestedToJoin_Description:I = 0x7f070bdd

.field public static final Settings_Notification_ClubRequestedToJoin_Title:I = 0x7f070bde

.field public static final Settings_Notification_ClubTransfer_Description:I = 0x7f070bdf

.field public static final Settings_Notification_ClubTransfer_Title:I = 0x7f070be0

.field public static final Settings_Notification_Description:I = 0x7f070be1

.field public static final Settings_Notification_Lfg_Description:I = 0x7f070be2

.field public static final Settings_Notification_Lfg_Title:I = 0x7f070be3

.field public static final Settings_Notification_Message_Description:I = 0x7f070be4

.field public static final Settings_Notification_Message_Title:I = 0x7f070be5

.field public static final Settings_Notification_Party_Invite_Description:I = 0x7f070be6

.field public static final Settings_Notification_Party_Invite_Title:I = 0x7f070be7

.field public static final Settings_Notification_Presence_Description:I = 0x7f070be8

.field public static final Settings_Notification_Presence_Title:I = 0x7f070be9

.field public static final Settings_Notification_Title:I = 0x7f070bea

.field public static final Settings_Notifications_Arena_Header:I = 0x7f070beb

.field public static final Settings_Notifications_Clubs_Header:I = 0x7f070bec

.field public static final Settings_Notifications_Failure_Description:I = 0x7f070bed

.field public static final Settings_Notifications_Failure_Title:I = 0x7f070bee

.field public static final Settings_Notifications_Header:I = 0x7f070bef

.field public static final Settings_Notifications_LFG_Header:I = 0x7f070bf0

.field public static final Settings_Notifications_Social_Header:I = 0x7f070bf1

.field public static final Settings_PageTitle:I = 0x7f070bf2

.field public static final Settings_Preferences_AllowNotificationsDescription:I = 0x7f070bf3

.field public static final Settings_Preferences_AllowNotificationsOption:I = 0x7f070bf4

.field public static final Settings_Preferences_AppRestart:I = 0x7f070bf5

.field public static final Settings_Preferences_LandingPage_SectionDescription:I = 0x7f070bf6

.field public static final Settings_Preferences_LandingPage_SectionHeader:I = 0x7f070bf7

.field public static final Settings_Preferences_SectionHeader:I = 0x7f070bf8

.field public static final Settings_Preferences_StayAwakeAlwaysOption:I = 0x7f070bf9

.field public static final Settings_Preferences_StayAwakeAlwaysOptionDescription:I = 0x7f070bfa

.field public static final Settings_Preferences_StayAwakeOption:I = 0x7f070bfb

.field public static final Settings_Preferences_StayAwakeOptionDescription:I = 0x7f070bfc

.field public static final Settings_Preferences_StayAwakeRemoteOption:I = 0x7f070bfd

.field public static final Settings_Preferences_StayAwakeRemoteOptionDescription:I = 0x7f070bfe

.field public static final Settings_Preferences_StayAwakeSystemOption:I = 0x7f070bff

.field public static final Settings_Preferences_StayAwakeSystemOptionDescription:I = 0x7f070c00

.field public static final Settings_Preferences_StayAwake_SectionHeader:I = 0x7f070c01

.field public static final Settings_Preferences_UseConsoleRegion:I = 0x7f070c02

.field public static final Settings_Preferences_UseConsoleRegion_Description:I = 0x7f070c03

.field public static final Settings_Preferences_XboxApp_StayAwakeAlwaysOptionDescription:I = 0x7f070c04

.field public static final Settings_Preferences_XboxApp_StayAwakeOptionDescription:I = 0x7f070c05

.field public static final Settings_PrivacyButton:I = 0x7f070c06

.field public static final Settings_Purchase:I = 0x7f070c07

.field public static final Settings_Purchase_Header:I = 0x7f070c08

.field public static final Settings_Save:I = 0x7f070c09

.field public static final Settings_SignOutButton:I = 0x7f070c0a

.field public static final Settings_SignOutButton_SigningOut:I = 0x7f070c0b

.field public static final Settings_Sound_DisableSoundOption:I = 0x7f070c0c

.field public static final Settings_Sound_DisableSoundOptionDescription:I = 0x7f070c0d

.field public static final Settings_Sound_SectionHeader:I = 0x7f070c0e

.field public static final Settings_Sound_XboxApp_DisableSoundOptionDescription:I = 0x7f070c0f

.field public static final Settings_Title:I = 0x7f070c10

.field public static final Settings_Tournaments_All_Body:I = 0x7f070c11

.field public static final Settings_Tournaments_All_Header:I = 0x7f070c12

.field public static final Settings_Tournaments_MatchReady_Body:I = 0x7f070c13

.field public static final Settings_Tournaments_MatchReady_Header:I = 0x7f070c14

.field public static final Settings_Tournaments_StateChange_Body:I = 0x7f070c15

.field public static final Settings_Tournaments_StateChange_Header:I = 0x7f070c16

.field public static final Settings_Tournaments_TeamStatus_Body:I = 0x7f070c17

.field public static final Settings_Tournaments_TeamStatus_Header:I = 0x7f070c18

.field public static final Settings_WhatsNew:I = 0x7f070c19

.field public static final Settings_WhatsNew_Description:I = 0x7f070c1a

.field public static final Settings_WhatsNew_Link:I = 0x7f070c1b

.field public static final Settings_XboxApp_GetStartedDescription:I = 0x7f070c1c

.field public static final Settings_XboxApp_Notification_Description:I = 0x7f070c1d

.field public static final Share_CaptionScreen_DiscardTextError_Body:I = 0x7f070c1e

.field public static final Share_CaptionScreen_DiscardTextError_Header:I = 0x7f070c1f

.field public static final Share_CaptionScreen_HeaderText:I = 0x7f070c20

.field public static final Share_CaptionScreen_ShareButtonText:I = 0x7f070c21

.field public static final Share_CaptionScreen_TextEntryWatermark:I = 0x7f070c22

.field public static final Share_DisplayError_Body:I = 0x7f070c23

.field public static final Share_OptionScreen_ActivityFeedText:I = 0x7f070c24

.field public static final Share_OptionScreen_MessageText:I = 0x7f070c25

.field public static final Share_OptionsScreen_HeaderText:I = 0x7f070c26

.field public static final Shares_Filter_Header:I = 0x7f070c27

.field public static final Shares_List_NoData:I = 0x7f070c28

.field public static final SignIn_Cancel:I = 0x7f070c29

.field public static final SignIn_GettingData:I = 0x7f070c2a

.field public static final SignIn_SignIn:I = 0x7f070c2b

.field public static final SignIn_SigningIn:I = 0x7f070c2c

.field public static final SignIn_Troubleshoot:I = 0x7f070c2d

.field public static final Store_AvailableOn:I = 0x7f070c2e

.field public static final Store_BuyAnyway:I = 0x7f070c2f

.field public static final Store_Discount_Narrator:I = 0x7f070c30

.field public static final Store_GamePass_BuyCopy:I = 0x7f070c31

.field public static final Store_GamePass_Description:I = 0x7f070c32

.field public static final Store_GamePass_FreeWith:I = 0x7f070c33

.field public static final Store_GamePass_FreeWith_NoGlyph:I = 0x7f070c34

.field public static final Store_GamePass_Install_Button:I = 0x7f070c35

.field public static final Store_GamePass_Install_Dialog_Body:I = 0x7f070c36

.field public static final Store_GamePass_Install_Dialog_Title:I = 0x7f070c37

.field public static final Store_GamePass_List1_Title:I = 0x7f070c38

.field public static final Store_GamePass_List2_Title:I = 0x7f070c39

.field public static final Store_GamePass_List3_Title:I = 0x7f070c3a

.field public static final Store_GamePass_List4_Title:I = 0x7f070c3b

.field public static final Store_GamePass_List5_Title:I = 0x7f070c3c

.field public static final Store_GamePass_NoConsole_Dialog_Body:I = 0x7f070c3d

.field public static final Store_GamePass_NoConsole_Dialog_Title:I = 0x7f070c3e

.field public static final Store_GamePass_Title:I = 0x7f070c3f

.field public static final Store_GamePass_Title_Action:I = 0x7f070c40

.field public static final Store_GamePass_Title_All:I = 0x7f070c41

.field public static final Store_GamePass_Title_Awards:I = 0x7f070c42

.field public static final Store_GamePass_Title_Featured:I = 0x7f070c43

.field public static final Store_GamePass_Title_Indies:I = 0x7f070c44

.field public static final Store_GamePass_Title_Recent:I = 0x7f070c45

.field public static final Store_GamePass_Title_Sports:I = 0x7f070c46

.field public static final Store_Platform:I = 0x7f070c47

.field public static final Store_PurchaseWarning:I = 0x7f070c48

.field public static final Store_SystemRequirements_Architecture:I = 0x7f070c49

.field public static final Store_SystemRequirements_Architecture_Required:I = 0x7f070c4a

.field public static final Store_SystemRequirements_DirectXVersion:I = 0x7f070c4b

.field public static final Store_SystemRequirements_Graphics:I = 0x7f070c4c

.field public static final Store_SystemRequirements_Memory:I = 0x7f070c4d

.field public static final Store_SystemRequirements_Minimum:I = 0x7f070c4e

.field public static final Store_SystemRequirements_OS:I = 0x7f070c4f

.field public static final Store_SystemRequirements_OS_Value:I = 0x7f070c50

.field public static final Store_SystemRequirements_OS_ValueWithVersion:I = 0x7f070c51

.field public static final Store_SystemRequirements_Processor:I = 0x7f070c52

.field public static final Store_SystemRequirements_Recommended:I = 0x7f070c53

.field public static final Store_SystemRequirements_Title:I = 0x7f070c54

.field public static final Store_SystemRequirements_VideoMemory:I = 0x7f070c55

.field public static final Stream_Header:I = 0x7f070c56

.field public static final Stream_TV:I = 0x7f070c57

.field public static final Streaming_Attempting_Reconnect:I = 0x7f070c58

.field public static final Streaming_Begin_Header:I = 0x7f070c59

.field public static final Streaming_Best_Quality:I = 0x7f070c5a

.field public static final Streaming_Buffering:I = 0x7f070c5b

.field public static final Streaming_Choose_a_Source:I = 0x7f070c5c

.field public static final Streaming_Close_Stream:I = 0x7f070c5d

.field public static final Streaming_Connecting:I = 0x7f070c5e

.field public static final Streaming_Connection_Error:I = 0x7f070c5f

.field public static final Streaming_Console_Error:I = 0x7f070c60

.field public static final Streaming_Current_Source:I = 0x7f070c61

.field public static final Streaming_Currently_Playing:I = 0x7f070c62

.field public static final Streaming_Device_Not_Supported_Error:I = 0x7f070c63

.field public static final Streaming_Disconnect_By_User_Error:I = 0x7f070c64

.field public static final Streaming_Disconnect_Error:I = 0x7f070c65

.field public static final Streaming_General_Error:I = 0x7f070c66

.field public static final Streaming_General_Error_Long:I = 0x7f070c67

.field public static final Streaming_General_Error_Restart:I = 0x7f070c68

.field public static final Streaming_HDCP_Error:I = 0x7f070c69

.field public static final Streaming_HDMI_In:I = 0x7f070c6a

.field public static final Streaming_Header:I = 0x7f070c6b

.field public static final Streaming_High_Quality:I = 0x7f070c6c

.field public static final Streaming_Interrupted_Error:I = 0x7f070c6d

.field public static final Streaming_Lost_Network_Error:I = 0x7f070c6e

.field public static final Streaming_Low_Quality:I = 0x7f070c6f

.field public static final Streaming_Low_Resources_Error:I = 0x7f070c70

.field public static final Streaming_MHEG_Error:I = 0x7f070c71

.field public static final Streaming_MTC_Live:I = 0x7f070c72

.field public static final Streaming_Medium_Quality:I = 0x7f070c73

.field public static final Streaming_Network_Error:I = 0x7f070c74

.field public static final Streaming_Network_Not_Supported_Error:I = 0x7f070c75

.field public static final Streaming_Network_Quality:I = 0x7f070c76

.field public static final Streaming_Network_Too_Slow_Error:I = 0x7f070c77

.field public static final Streaming_New_Feature:I = 0x7f070c78

.field public static final Streaming_No_Content_Error:I = 0x7f070c79

.field public static final Streaming_No_Signal:I = 0x7f070c7a

.field public static final Streaming_On_Next:I = 0x7f070c7b

.field public static final Streaming_Other_User_Changed_Quality:I = 0x7f070c7c

.field public static final Streaming_Parental_Controls_Block:I = 0x7f070c7d

.field public static final Streaming_Playing_Radio_Channel:I = 0x7f070c7e

.field public static final Streaming_Reconnect_To_Stream:I = 0x7f070c7f

.field public static final Streaming_Reconnecting:I = 0x7f070c80

.field public static final Streaming_Region_Not_Supported_Error:I = 0x7f070c81

.field public static final Streaming_Remote_Cannot_Control_Source:I = 0x7f070c82

.field public static final Streaming_Restart_Stream:I = 0x7f070c83

.field public static final Streaming_Resume:I = 0x7f070c84

.field public static final Streaming_Sign_In_Before_Streaming:I = 0x7f070c85

.field public static final Streaming_Sign_In_Error:I = 0x7f070c86

.field public static final Streaming_Signal_Lost_Error:I = 0x7f070c87

.field public static final Streaming_Signed_Out_Error:I = 0x7f070c88

.field public static final Streaming_Start_Error:I = 0x7f070c89

.field public static final Streaming_Stop:I = 0x7f070c8a

.field public static final Streaming_TV_Tuner:I = 0x7f070c8b

.field public static final Streaming_Try_Again_Later:I = 0x7f070c8c

.field public static final Streaming_Version_Not_Supported_Error:I = 0x7f070c8d

.field public static final Streaming_Video:I = 0x7f070c8e

.field public static final Streaming_XboxApp_General_Error_Restart:I = 0x7f070c8f

.field public static final Streaming_XboxApp_Other_User_Changed_Quality:I = 0x7f070c90

.field public static final Suggestions_All_People:I = 0x7f070c91

.field public static final Suggestions_Facebook_Friends:I = 0x7f070c92

.field public static final Suggestions_HeaderTitle:I = 0x7f070c93

.field public static final Suggestions_Header_People:I = 0x7f070c94

.field public static final Suggestions_Invite_Phone_Contacts_Subtext:I = 0x7f070e28

.field public static final Suggestions_Invite_Phone_Contacts_Text:I = 0x7f070e29

.field public static final Suggestions_People_You_May_Know:I = 0x7f070c95

.field public static final Suggestions_VIPs:I = 0x7f070c96

.field public static final Synopsis_Header:I = 0x7f070c97

.field public static final TOU_Accept:I = 0x7f070c98

.field public static final TVDetails_DescriptionHeader:I = 0x7f070c99

.field public static final TVDetails_SeasonsHeader:I = 0x7f070c9a

.field public static final TVDetails_SeasonsHeader_Tablet:I = 0x7f070c9b

.field public static final TVDetails_Upcoming:I = 0x7f070c9c

.field public static final TVSeason_Aired_Title:I = 0x7f070c9d

.field public static final TextInput_Confirm:I = 0x7f070c9e

.field public static final TextInput_Help:I = 0x7f070c9f

.field public static final ThirdPartyNotice_Header:I = 0x7f070ca0

.field public static final TitleUpdateAvailable_Text:I = 0x7f070ca1

.field public static final TitleUpdateAvailable_Title:I = 0x7f070ca2

.field public static final TitleUpdateRequired_Text:I = 0x7f070ca3

.field public static final TitleUpdateRequired_Title:I = 0x7f070ca4

.field public static final TitleUpdate_GetItNow:I = 0x7f070ca5

.field public static final TopGames_Header:I = 0x7f070ca6

.field public static final TopGames_Header_Tablet:I = 0x7f070ca7

.field public static final TopGames_PlayedN:I = 0x7f070ca8

.field public static final TopGames_Played_Once:I = 0x7f070ca9

.field public static final TraceService_OutputFile:I = 0x7f070caa

.field public static final Trending_Filter_Text_Posts:I = 0x7f070cab

.field public static final Trending_GetBeam:I = 0x7f070cac

.field public static final Trending_Global_Error:I = 0x7f070cad

.field public static final Trending_Global_NoData:I = 0x7f070cae

.field public static final Trending_PersonIsStreaming:I = 0x7f070caf

.field public static final Trending_PopularBeamStreams:I = 0x7f070cb0

.field public static final Trending_PopularStreams:I = 0x7f070cb1

.field public static final Trending_StreamName:I = 0x7f070cb2

.field public static final Trending_Streaming:I = 0x7f070cb3

.field public static final Trending_StreamingBeam:I = 0x7f070cb4

.field public static final Trending_StreamingGame:I = 0x7f070cb5

.field public static final Trending_StreamingNow:I = 0x7f070cb6

.field public static final Trending_Streaming_Followers:I = 0x7f070cb7

.field public static final Trending_Streaming_NoData:I = 0x7f070cb8

.field public static final Trending_Streaming_NoPopular:I = 0x7f070cb9

.field public static final Trending_Streaming_OnlineNow:I = 0x7f070cba

.field public static final Trending_Streaming_Views:I = 0x7f070cbb

.field public static final Trending_Streaming_Watching:I = 0x7f070cbc

.field public static final Trending_TrendingBeamStreams:I = 0x7f070cbd

.field public static final Trending_TrendingNow:I = 0x7f070cbe

.field public static final Trending_TrendingStreams:I = 0x7f070cbf

.field public static final Trending_WatchMore:I = 0x7f070cc0

.field public static final Trending_WatchMoreBeam:I = 0x7f070cc1

.field public static final Tutorial_Alert_Subtitle:I = 0x7f070cc2

.field public static final Tutorial_Alert_Title:I = 0x7f070cc3

.field public static final Tutorial_Club_Button_Text:I = 0x7f070cc4

.field public static final Tutorial_Club_Subtitle:I = 0x7f070cc5

.field public static final Tutorial_Club_Title:I = 0x7f070cc6

.field public static final Tutorial_Console_Setup_Beta_Title:I = 0x7f070cc7

.field public static final Tutorial_Console_Setup_Button_Text:I = 0x7f070cc8

.field public static final Tutorial_Console_Setup_Subtitle:I = 0x7f070cc9

.field public static final Tutorial_Console_Setup_Title:I = 0x7f070cca

.field public static final Tutorial_Console_Xbox_One_Setup_Title:I = 0x7f070ccb

.field public static final Tutorial_Console_Xbox_One_X_Setup_Title:I = 0x7f070ccc

.field public static final Tutorial_Drawer_Title:I = 0x7f070ccd

.field public static final Tutorial_Facebook_Button_Text:I = 0x7f070cce

.field public static final Tutorial_Facebook_Subtitle:I = 0x7f070ccf

.field public static final Tutorial_Facebook_Title:I = 0x7f070cd0

.field public static final Tutorial_Friend_Suggestions_Button_Text:I = 0x7f070cd1

.field public static final Tutorial_Friend_Suggestions_Subtitle:I = 0x7f070cd2

.field public static final Tutorial_Friend_Suggestions_Title:I = 0x7f070cd3

.field public static final Tutorial_New_Console_Setup_Beta_Title:I = 0x7f070cd4

.field public static final Tutorial_New_Console_Setup_Title:I = 0x7f070cd5

.field public static final Tutorial_Redeem_Code_Button_Text:I = 0x7f070cd6

.field public static final Tutorial_Redeem_Code_Subtitle:I = 0x7f070cd7

.field public static final Tutorial_Redeem_Code_Title:I = 0x7f070cd8

.field public static final Tutorial_Shop_For_Games_Button_Text:I = 0x7f070cd9

.field public static final Tutorial_Shop_For_Games_Subtitle:I = 0x7f070cda

.field public static final Tutorial_Shop_For_Games_Title:I = 0x7f070cdb

.field public static final Tutorial_Tab_Title:I = 0x7f070cdc

.field public static final Tutorial_Welcome_Subtitle:I = 0x7f070cdd

.field public static final Tutorial_Welcome_Title:I = 0x7f070cde

.field public static final URC_ConnectionError_Remote:I = 0x7f070e2a

.field public static final URC_Error_Console_Setup_Directions_STB:I = 0x7f070e2b

.field public static final URC_Error_Remote_Console_Setup:I = 0x7f070e2c

.field public static final URC_Error_TvListings_Settings:I = 0x7f070e2d

.field public static final UniversalRemote_Button_0:I = 0x7f070cdf

.field public static final UniversalRemote_Button_1:I = 0x7f070ce0

.field public static final UniversalRemote_Button_2:I = 0x7f070ce1

.field public static final UniversalRemote_Button_3:I = 0x7f070ce2

.field public static final UniversalRemote_Button_4:I = 0x7f070ce3

.field public static final UniversalRemote_Button_5:I = 0x7f070ce4

.field public static final UniversalRemote_Button_6:I = 0x7f070ce5

.field public static final UniversalRemote_Button_7:I = 0x7f070ce6

.field public static final UniversalRemote_Button_8:I = 0x7f070ce7

.field public static final UniversalRemote_Button_9:I = 0x7f070ce8

.field public static final UniversalRemote_Button_CH:I = 0x7f070ce9

.field public static final UniversalRemote_Button_CHANGE_LAYOUT:I = 0x7f070cea

.field public static final UniversalRemote_Button_CHANNEL:I = 0x7f070ceb

.field public static final UniversalRemote_Button_DASH:I = 0x7f070cec

.field public static final UniversalRemote_Button_DVR:I = 0x7f070ced

.field public static final UniversalRemote_Button_DeviceType_all:I = 0x7f070cee

.field public static final UniversalRemote_Button_DeviceType_avr:I = 0x7f070cef

.field public static final UniversalRemote_Button_DeviceType_stb:I = 0x7f070cf0

.field public static final UniversalRemote_Button_DeviceType_tv:I = 0x7f070cf1

.field public static final UniversalRemote_Button_ENTER:I = 0x7f070cf2

.field public static final UniversalRemote_Button_EXIT:I = 0x7f070cf3

.field public static final UniversalRemote_Button_GUIDE:I = 0x7f070cf4

.field public static final UniversalRemote_Button_INFO:I = 0x7f070cf5

.field public static final UniversalRemote_Button_MENU:I = 0x7f070cf6

.field public static final UniversalRemote_Button_PAGE:I = 0x7f070cf7

.field public static final UniversalRemote_Button_SELECT:I = 0x7f070cf8

.field public static final UniversalRemote_Button_UrcPower_off:I = 0x7f070cf9

.field public static final UniversalRemote_Button_UrcPower_on:I = 0x7f070cfa

.field public static final UniversalRemote_Button_UrcPower_onoff:I = 0x7f070cfb

.field public static final UniversalRemote_Button_VOL:I = 0x7f070cfc

.field public static final UniversalRemote_Button_btn_back:I = 0x7f070cfd

.field public static final UniversalRemote_Button_btn_cancel:I = 0x7f070cfe

.field public static final UniversalRemote_Button_btn_ch_enter:I = 0x7f070cff

.field public static final UniversalRemote_Button_btn_clear:I = 0x7f070d00

.field public static final UniversalRemote_Button_btn_day_minus:I = 0x7f070d01

.field public static final UniversalRemote_Button_btn_day_plus:I = 0x7f070d02

.field public static final UniversalRemote_Button_btn_delimiter:I = 0x7f070d03

.field public static final UniversalRemote_Button_btn_dvr:I = 0x7f070d04

.field public static final UniversalRemote_Button_btn_exit:I = 0x7f070d05

.field public static final UniversalRemote_Button_btn_format:I = 0x7f070d06

.field public static final UniversalRemote_Button_btn_func_a:I = 0x7f070d07

.field public static final UniversalRemote_Button_btn_func_b:I = 0x7f070d08

.field public static final UniversalRemote_Button_btn_func_c:I = 0x7f070d09

.field public static final UniversalRemote_Button_btn_func_d:I = 0x7f070d0a

.field public static final UniversalRemote_Button_btn_guide:I = 0x7f070d0b

.field public static final UniversalRemote_Button_btn_help:I = 0x7f070d0c

.field public static final UniversalRemote_Button_btn_home:I = 0x7f070d0d

.field public static final UniversalRemote_Button_btn_info:I = 0x7f070d0e

.field public static final UniversalRemote_Button_btn_interactive:I = 0x7f070d0f

.field public static final UniversalRemote_Button_btn_last:I = 0x7f070d10

.field public static final UniversalRemote_Button_btn_live:I = 0x7f070d11

.field public static final UniversalRemote_Button_btn_menu:I = 0x7f070d12

.field public static final UniversalRemote_Button_btn_options:I = 0x7f070d13

.field public static final UniversalRemote_Button_btn_plus_100:I = 0x7f070d14

.field public static final UniversalRemote_Button_btn_sap:I = 0x7f070d15

.field public static final UniversalRemote_Button_btn_select:I = 0x7f070d16

.field public static final UniversalRemote_Button_btn_services:I = 0x7f070d17

.field public static final UniversalRemote_Button_btn_setup:I = 0x7f070d18

.field public static final UniversalRemote_Button_btn_slow:I = 0x7f070d19

.field public static final UniversalRemote_Button_btn_subtitle:I = 0x7f070d1a

.field public static final UniversalRemote_Button_btn_tools:I = 0x7f070d1b

.field public static final UniversalRemote_Button_btn_vod:I = 0x7f070d1c

.field public static final UniversalRemote_UnconfiguredMessage:I = 0x7f070d1d

.field public static final Unshared_Activity_Feed_Error:I = 0x7f070d1e

.field public static final Unshared_Activity_Feed_Nothing_To_Share:I = 0x7f070d1f

.field public static final Unshared_Activity_Feed_Share_Something:I = 0x7f070d20

.field public static final Upload_Pic_Child_Blocked_Body:I = 0x7f070d21

.field public static final Upload_Pic_Child_Blocked_Header:I = 0x7f070d22

.field public static final Upload_Pic_Choose_Another:I = 0x7f070d23

.field public static final Upload_Pic_Edit_Picture_Subtitle_Android:I = 0x7f070d24

.field public static final Upload_Pic_Edit_Picture_Title:I = 0x7f070d25

.field public static final Upload_Pic_Failed:I = 0x7f070d26

.field public static final Upload_Pic_Missing_Priv:I = 0x7f070d27

.field public static final Upload_Pic_Quarantine:I = 0x7f070d28

.field public static final Upload_Pic_Ready_To_Upload:I = 0x7f070d29

.field public static final Upload_Pic_Ready_To_Upload_Description:I = 0x7f070d2a

.field public static final Upload_Pic_Select_Picture:I = 0x7f070d2b

.field public static final Upload_Pic_Selected_Pic_Description:I = 0x7f070d2c

.field public static final Upload_Pic_Selection_Error:I = 0x7f070d2d

.field public static final Upload_Pic_Success_Subtitle:I = 0x7f070d2e

.field public static final Upload_Pic_Success_Title:I = 0x7f070d2f

.field public static final Upload_Pic_Upload:I = 0x7f070d30

.field public static final UrcReorder_OFF:I = 0x7f070d31

.field public static final UrcReorder_ON:I = 0x7f070d32

.field public static final UrcStatus_ErrorLabel:I = 0x7f070d33

.field public static final UtilityBar_Friends_Tooltips:I = 0x7f070d34

.field public static final VERTICAL_BAR_SEPARATOR:I = 0x7f070d35

.field public static final VideoDetails_AiringNow:I = 0x7f070d36

.field public static final VideoDetails_PlayNow:I = 0x7f070d37

.field public static final VoiceOver_Achievements_Cell_Progress_Text:I = 0x7f070d38

.field public static final VoiceOver_Action_Hint_Double_Tap:I = 0x7f070d39

.field public static final VoiceOver_Action_Hint_Double_Tap_Select:I = 0x7f070d3a

.field public static final VoiceOver_Action_Hint_Enter_Search_Term:I = 0x7f070d3b

.field public static final VoiceOver_ActivityFeed_Control_Text:I = 0x7f070d3c

.field public static final VoiceOver_ActivityFeed_GameClip_Text:I = 0x7f070d3d

.field public static final VoiceOver_ActivityFeed_Go_To_Item:I = 0x7f070d3e

.field public static final VoiceOver_ActivityFeed_Screenshot_Text:I = 0x7f070d3f

.field public static final VoiceOver_AddFavoriteChannel_Button:I = 0x7f070d40

.field public static final VoiceOver_Alerts_Control_Text:I = 0x7f070d41

.field public static final VoiceOver_Automatically_Reconnect_Button_Text:I = 0x7f070d42

.field public static final VoiceOver_Bronze_Icon_Text:I = 0x7f070d43

.field public static final VoiceOver_Checkbox_Button_Text:I = 0x7f070d44

.field public static final VoiceOver_Checked_Checkbox_Button_Text:I = 0x7f070d45

.field public static final VoiceOver_Close_Button_Text:I = 0x7f070d46

.field public static final VoiceOver_Comment_Button_Text:I = 0x7f070d47

.field public static final VoiceOver_Comparison_NotApplicable_Text:I = 0x7f070d48

.field public static final VoiceOver_Connect_Control_Text:I = 0x7f070d49

.field public static final VoiceOver_Controller_A_Button:I = 0x7f070d4a

.field public static final VoiceOver_Controller_B_Button:I = 0x7f070d4b

.field public static final VoiceOver_Controller_Icon:I = 0x7f070d4c

.field public static final VoiceOver_Controller_Menu_Button:I = 0x7f070d4d

.field public static final VoiceOver_Controller_View_Button:I = 0x7f070d4e

.field public static final VoiceOver_Controller_X_Button:I = 0x7f070d4f

.field public static final VoiceOver_Controller_Xbox_Button:I = 0x7f070d50

.field public static final VoiceOver_Controller_Y_Button:I = 0x7f070d51

.field public static final VoiceOver_Delete_Button_Text:I = 0x7f070d52

.field public static final VoiceOver_EditMessage_Icon_Text:I = 0x7f070d53

.field public static final VoiceOver_Edit_Pencil_Button_Text:I = 0x7f070d54

.field public static final VoiceOver_Friends_Control_Text:I = 0x7f070d55

.field public static final VoiceOver_FrownFace_Button_Text:I = 0x7f070d56

.field public static final VoiceOver_GameScore_Label_Text:I = 0x7f070d57

.field public static final VoiceOver_Game_Profile_Button_Text:I = 0x7f070d58

.field public static final VoiceOver_Gamer_Profile_Button_Text:I = 0x7f070d59

.field public static final VoiceOver_GoldMember_Icon_Text:I = 0x7f070d5a

.field public static final VoiceOver_Gold_Icon_Text:I = 0x7f070d5b

.field public static final VoiceOver_InfoChannel_Button:I = 0x7f070d5c

.field public static final VoiceOver_LaunchRemote_Icon:I = 0x7f070d5d

.field public static final VoiceOver_Left_Arrow_Button:I = 0x7f070d5e

.field public static final VoiceOver_Like_Post_Text:I = 0x7f070d5f

.field public static final VoiceOver_Locked_Icon:I = 0x7f070d60

.field public static final VoiceOver_Logout_Button_Text:I = 0x7f070d61

.field public static final VoiceOver_Messages_Control_Text:I = 0x7f070d62

.field public static final VoiceOver_MoreAction_Button_Text:I = 0x7f070d63

.field public static final VoiceOver_NumAlerts_Text:I = 0x7f070d64

.field public static final VoiceOver_NumNewMessages_Text:I = 0x7f070d65

.field public static final VoiceOver_NumberDaysAgo_Text:I = 0x7f070d66

.field public static final VoiceOver_NumberHoursAgo_Text:I = 0x7f070d67

.field public static final VoiceOver_NumberMinutesAgo_Text:I = 0x7f070d68

.field public static final VoiceOver_Off_Toggle_Switch_Text:I = 0x7f070d69

.field public static final VoiceOver_On_Toggle_Switch_Text:I = 0x7f070d6a

.field public static final VoiceOver_OneAlert_Text:I = 0x7f070d6b

.field public static final VoiceOver_OneDayAgo_Text:I = 0x7f070d6c

.field public static final VoiceOver_OneGuide_Text:I = 0x7f070d6d

.field public static final VoiceOver_OneHourAgo_Text:I = 0x7f070d6e

.field public static final VoiceOver_OneMinuteAgo_Text:I = 0x7f070d6f

.field public static final VoiceOver_OneNewMessage_Text:I = 0x7f070d70

.field public static final VoiceOver_Out_Of_Total_Text:I = 0x7f070d71

.field public static final VoiceOver_PageOfTotalPages:I = 0x7f070d72

.field public static final VoiceOver_Party:I = 0x7f070d73

.field public static final VoiceOver_PlayChannel_Button:I = 0x7f070d74

.field public static final VoiceOver_ProgramInfo_Button:I = 0x7f070d75

.field public static final VoiceOver_ProgressPercentage:I = 0x7f070d76

.field public static final VoiceOver_RemoveFavoriteChannel_Button:I = 0x7f070d77

.field public static final VoiceOver_Reward_Icon_Text:I = 0x7f070d78

.field public static final VoiceOver_Right_Arrow_Button:I = 0x7f070d79

.field public static final VoiceOver_Search_Button:I = 0x7f070d7a

.field public static final VoiceOver_Search_Button_Text:I = 0x7f070d7b

.field public static final VoiceOver_Search_Clear_Button:I = 0x7f070d7c

.field public static final VoiceOver_Selected_Radio_Button_Text:I = 0x7f070d7d

.field public static final VoiceOver_Share_Activity_Button_Text:I = 0x7f070d7e

.field public static final VoiceOver_Share_Button_Text:I = 0x7f070d7f

.field public static final VoiceOver_Silver_Icon_Text:I = 0x7f070d80

.field public static final VoiceOver_SmileyFace_Button_Text:I = 0x7f070d81

.field public static final VoiceOver_StarRating_Text:I = 0x7f070d82

.field public static final VoiceOver_Start_A_Party:I = 0x7f070d83

.field public static final VoiceOver_StreamChannel_Button:I = 0x7f070d84

.field public static final VoiceOver_Tags:I = 0x7f070d85

.field public static final VoiceOver_Unchecked_Checkbox_Button_Text:I = 0x7f070d86

.field public static final VoiceOver_Unlike_Post_Text:I = 0x7f070d87

.field public static final VoiceOver_Unselected_Radio_Button_Text:I = 0x7f070d88

.field public static final VoiceOver_Volume_Icon:I = 0x7f070d89

.field public static final VoiceOver_Will_Not_Automatically_Reconnect_Button_Text:I = 0x7f070d8a

.field public static final Warning_Title:I = 0x7f070d8b

.field public static final WebBrowser_GhostText:I = 0x7f070d8c

.field public static final WebBrowser_HelpNavigate:I = 0x7f070d8d

.field public static final WebBrowser_HelpSelect:I = 0x7f070d8e

.field public static final WebLinks_AddLink_Button:I = 0x7f070d8f

.field public static final WebLinks_GetLink_Button:I = 0x7f070d90

.field public static final WebLinks_GetLink_PlaceHolderText:I = 0x7f070d91

.field public static final WebLinks_ImageError:I = 0x7f070d92

.field public static final WebLinks_PreviewError:I = 0x7f070d93

.field public static final WebLinks_WebLinkPicker_Button:I = 0x7f070d94

.field public static final WhatsNew_ContinueButtonText_ToMain:I = 0x7f070d95

.field public static final WhatsNew_ContinueButtonText_ToPrevious:I = 0x7f070d96

.field public static final WhatsNew_Header:I = 0x7f070d97

.field public static final WhatsNew_HomePageSelectionDialog_Body1:I = 0x7f070d98

.field public static final WhatsNew_HomePageSelectionDialog_Body2:I = 0x7f070d99

.field public static final WhatsNew_HomePageSelectionDialog_Title:I = 0x7f070d9a

.field public static final WhatsNew_ItemDescription1:I = 0x7f070d9b

.field public static final WhatsNew_ItemDescription2:I = 0x7f070d9c

.field public static final WhatsNew_ItemDescription3:I = 0x7f070d9d

.field public static final WhatsNew_ItemTitle1:I = 0x7f070d9e

.field public static final WhatsNew_ItemTitle2:I = 0x7f070d9f

.field public static final WhatsNew_ItemTitle3:I = 0x7f070da0

.field public static final WhatsNew_ItemVideoUrl1:I = 0x7f070da1

.field public static final WhatsNew_ItemVideoUrl2:I = 0x7f070da2

.field public static final WhatsNew_ItemVideoUrl3:I = 0x7f070da3

.field public static final WhatsNew_OneGuide_NewMarkets:I = 0x7f070da4

.field public static final WhatsNew_OneGuide_Streaming:I = 0x7f070da5

.field public static final WhatsNew_XboxApp_ItemDescription1:I = 0x7f070da6

.field public static final Whats_New_1:I = 0x7f070da7

.field public static final Whats_New_2:I = 0x7f070da8

.field public static final Whats_New_3:I = 0x7f070da9

.field public static final Whats_New_4:I = 0x7f070daa

.field public static final Writers_Header:I = 0x7f070dab

.field public static final XboxApp:I = 0x7f070dac

.field public static final XboxApp_PrivacyPolicyAgreement:I = 0x7f070dad

.field public static final XboxSmartGlass:I = 0x7f070dae

.field public static final abc_action_bar_home_description:I = 0x7f070000

.field public static final abc_action_bar_home_description_format:I = 0x7f070001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f070002

.field public static final abc_action_bar_up_description:I = 0x7f070003

.field public static final abc_action_menu_overflow_description:I = 0x7f070004

.field public static final abc_action_mode_done:I = 0x7f070005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f070006

.field public static final abc_activitychooserview_choose_application:I = 0x7f070007

.field public static final abc_capital_off:I = 0x7f070008

.field public static final abc_capital_on:I = 0x7f070009

.field public static final abc_font_family_body_1_material:I = 0x7f070e2e

.field public static final abc_font_family_body_2_material:I = 0x7f070e2f

.field public static final abc_font_family_button_material:I = 0x7f070e30

.field public static final abc_font_family_caption_material:I = 0x7f070e31

.field public static final abc_font_family_display_1_material:I = 0x7f070e32

.field public static final abc_font_family_display_2_material:I = 0x7f070e33

.field public static final abc_font_family_display_3_material:I = 0x7f070e34

.field public static final abc_font_family_display_4_material:I = 0x7f070e35

.field public static final abc_font_family_headline_material:I = 0x7f070e36

.field public static final abc_font_family_menu_material:I = 0x7f070e37

.field public static final abc_font_family_subhead_material:I = 0x7f070e38

.field public static final abc_font_family_title_material:I = 0x7f070e39

.field public static final abc_search_hint:I = 0x7f07000a

.field public static final abc_searchview_description_clear:I = 0x7f07000b

.field public static final abc_searchview_description_query:I = 0x7f07000c

.field public static final abc_searchview_description_search:I = 0x7f07000d

.field public static final abc_searchview_description_submit:I = 0x7f07000e

.field public static final abc_searchview_description_voice:I = 0x7f07000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f070010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f070011

.field public static final abc_toolbar_collapse_description:I = 0x7f070012

.field public static final about_content:I = 0x7f070df4

.field public static final accept:I = 0x7f070e3a

.field public static final account_menu_add_account:I = 0x7f070047

.field public static final account_picker_list_body:I = 0x7f070048

.field public static final account_picker_list_header:I = 0x7f070049

.field public static final account_picker_menu_dismiss:I = 0x7f07004a

.field public static final account_setting_up:I = 0x7f07004b

.field public static final account_setting_up_header:I = 0x7f07004c

.field public static final achievements_earned_percentage:I = 0x7f070e3b

.field public static final action_settings:I = 0x7f070e3c

.field public static final activity_feed_screenshot_view_type:I = 0x7f070df6

.field public static final app_market:I = 0x7f07004d

.field public static final app_name:I = 0x7f070e3d

.field public static final appbar_scrolling_view_behavior:I = 0x7f070e3e

.field public static final authentication_button_finish:I = 0x7f07004e

.field public static final authentication_button_next:I = 0x7f07004f

.field public static final authentication_button_previous:I = 0x7f070050

.field public static final bottom_sheet_behavior:I = 0x7f070e3f

.field public static final btn_0:I = 0x7f070e40

.field public static final btn_1:I = 0x7f070e41

.field public static final btn_2:I = 0x7f070e42

.field public static final btn_3:I = 0x7f070e43

.field public static final btn_4:I = 0x7f070e44

.field public static final btn_5:I = 0x7f070e45

.field public static final btn_6:I = 0x7f070e46

.field public static final btn_7:I = 0x7f070e47

.field public static final btn_8:I = 0x7f070e48

.field public static final btn_9:I = 0x7f070e49

.field public static final btn_ch:I = 0x7f070e4a

.field public static final btn_ch_enter:I = 0x7f070e4b

.field public static final btn_delimiter:I = 0x7f070e4c

.field public static final btn_dvr:I = 0x7f070e4d

.field public static final btn_exit:I = 0x7f070e4e

.field public static final btn_guide:I = 0x7f070e4f

.field public static final btn_info:I = 0x7f070e50

.field public static final btn_live:I = 0x7f070e51

.field public static final btn_menu:I = 0x7f070e52

.field public static final btn_page:I = 0x7f070e53

.field public static final btn_select:I = 0x7f070e54

.field public static final btn_vol:I = 0x7f070e55

.field public static final catalyst_copy_button:I = 0x7f070e56

.field public static final catalyst_debugjs:I = 0x7f070e57

.field public static final catalyst_debugjs_off:I = 0x7f070e58

.field public static final catalyst_dismiss_button:I = 0x7f070e59

.field public static final catalyst_element_inspector:I = 0x7f070e5a

.field public static final catalyst_heap_capture:I = 0x7f070e5b

.field public static final catalyst_hot_module_replacement:I = 0x7f070e5c

.field public static final catalyst_hot_module_replacement_off:I = 0x7f070e5d

.field public static final catalyst_jsload_error:I = 0x7f070e5e

.field public static final catalyst_live_reload:I = 0x7f070e5f

.field public static final catalyst_live_reload_off:I = 0x7f070e60

.field public static final catalyst_loading_from_url:I = 0x7f070e61

.field public static final catalyst_perf_monitor:I = 0x7f070e62

.field public static final catalyst_perf_monitor_off:I = 0x7f070e63

.field public static final catalyst_poke_sampling_profiler:I = 0x7f070e64

.field public static final catalyst_reload_button:I = 0x7f070e65

.field public static final catalyst_reloadjs:I = 0x7f070e66

.field public static final catalyst_remotedbg_error:I = 0x7f070e67

.field public static final catalyst_remotedbg_message:I = 0x7f070e68

.field public static final catalyst_report_button:I = 0x7f070e69

.field public static final catalyst_settings:I = 0x7f070e6a

.field public static final catalyst_settings_title:I = 0x7f070e6b

.field public static final character_counter_pattern:I = 0x7f070e6c

.field public static final colon_delimiter:I = 0x7f070e6d

.field public static final com_facebook_device_auth_instructions:I = 0x7f070013

.field public static final com_facebook_image_download_unknown_error:I = 0x7f070014

.field public static final com_facebook_internet_permission_error_message:I = 0x7f070015

.field public static final com_facebook_internet_permission_error_title:I = 0x7f070016

.field public static final com_facebook_like_button_liked:I = 0x7f070017

.field public static final com_facebook_like_button_not_liked:I = 0x7f070018

.field public static final com_facebook_loading:I = 0x7f070019

.field public static final com_facebook_loginview_cancel_action:I = 0x7f07001a

.field public static final com_facebook_loginview_log_in_button:I = 0x7f07001b

.field public static final com_facebook_loginview_log_in_button_continue:I = 0x7f07001c

.field public static final com_facebook_loginview_log_in_button_long:I = 0x7f07001d

.field public static final com_facebook_loginview_log_out_action:I = 0x7f07001e

.field public static final com_facebook_loginview_log_out_button:I = 0x7f07001f

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f070020

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f070021

.field public static final com_facebook_send_button_text:I = 0x7f070022

.field public static final com_facebook_share_button_text:I = 0x7f070023

.field public static final com_facebook_smart_device_instructions:I = 0x7f070024

.field public static final com_facebook_smart_device_instructions_or:I = 0x7f070025

.field public static final com_facebook_smart_login_confirmation_cancel:I = 0x7f070026

.field public static final com_facebook_smart_login_confirmation_continue_as:I = 0x7f070df5

.field public static final com_facebook_smart_login_confirmation_title:I = 0x7f070027

.field public static final com_facebook_tooltip_default:I = 0x7f070028

.field public static final comma_delimiter:I = 0x7f070e6e

.field public static final common_google_play_services_enable_button:I = 0x7f070029

.field public static final common_google_play_services_enable_text:I = 0x7f07002a

.field public static final common_google_play_services_enable_title:I = 0x7f07002b

.field public static final common_google_play_services_install_button:I = 0x7f07002c

.field public static final common_google_play_services_install_text_phone:I = 0x7f07002d

.field public static final common_google_play_services_install_text_tablet:I = 0x7f07002e

.field public static final common_google_play_services_install_title:I = 0x7f07002f

.field public static final common_google_play_services_notification_ticker:I = 0x7f070030

.field public static final common_google_play_services_unknown_issue:I = 0x7f070031

.field public static final common_google_play_services_unsupported_text:I = 0x7f070032

.field public static final common_google_play_services_unsupported_title:I = 0x7f070033

.field public static final common_google_play_services_update_button:I = 0x7f070034

.field public static final common_google_play_services_update_text:I = 0x7f070035

.field public static final common_google_play_services_update_title:I = 0x7f070036

.field public static final common_google_play_services_updating_text:I = 0x7f070037

.field public static final common_google_play_services_updating_title:I = 0x7f070038

.field public static final common_google_play_services_wear_update_text:I = 0x7f070039

.field public static final common_open_on_phone:I = 0x7f07003a

.field public static final common_signin_button_text:I = 0x7f07003b

.field public static final common_signin_button_text_long:I = 0x7f07003c

.field public static final create_calendar_message:I = 0x7f070e6f

.field public static final create_calendar_title:I = 0x7f070e70

.field public static final crop_image_activity_title:I = 0x7f070e71

.field public static final crop_image_menu_crop:I = 0x7f070e72

.field public static final crop_image_menu_rotate_left:I = 0x7f070e73

.field public static final crop_image_menu_rotate_right:I = 0x7f070e74

.field public static final decline:I = 0x7f070e75

.field public static final details_user_rate_count:I = 0x7f070e76

.field public static final device_specs_fail_title:I = 0x7f070e77

.field public static final epg_channel:I = 0x7f070e78

.field public static final epg_header_date_format:I = 0x7f070e79

.field public static final epg_header_now:I = 0x7f070e7a

.field public static final epg_header_provider:I = 0x7f070e7b

.field public static final epg_header_today:I = 0x7f070e7c

.field public static final epg_header_weekday_format:I = 0x7f070e7d

.field public static final error_body_generic_failure:I = 0x7f070051

.field public static final error_header_generic_failure:I = 0x7f070052

.field public static final error_header_server_network_error:I = 0x7f070053

.field public static final error_overlay_no_network:I = 0x7f070054

.field public static final esrb_rating_online_interactions:I = 0x7f070e7e

.field public static final exo_controls_fastforward_description:I = 0x7f07003d

.field public static final exo_controls_next_description:I = 0x7f07003e

.field public static final exo_controls_pause_description:I = 0x7f07003f

.field public static final exo_controls_play_description:I = 0x7f070040

.field public static final exo_controls_previous_description:I = 0x7f070041

.field public static final exo_controls_rewind_description:I = 0x7f070042

.field public static final exo_controls_stop_description:I = 0x7f070043

.field public static final facebook_app_id:I = 0x7f070e7f

.field public static final feedback_body:I = 0x7f070e80

.field public static final feedback_crash_body:I = 0x7f070e81

.field public static final feedback_crash_dialog_text:I = 0x7f070e82

.field public static final feedback_crash_dialog_title:I = 0x7f070e83

.field public static final feedback_crash_subject:I = 0x7f070e84

.field public static final feedback_dialog_cancel_button:I = 0x7f070e85

.field public static final feedback_dialog_send_button:I = 0x7f070e86

.field public static final feedback_dialog_text:I = 0x7f070e87

.field public static final feedback_dialog_title:I = 0x7f070e88

.field public static final feedback_email:I = 0x7f070e89

.field public static final feedback_mail_chooser_text:I = 0x7f070e8a

.field public static final feedback_placeholder:I = 0x7f070e8b

.field public static final feedback_subject:I = 0x7f070e8c

.field public static final friend_status_hour_ago:I = 0x7f070e8d

.field public static final friend_status_minute_ago:I = 0x7f070e8e

.field public static final gamerscore_empty:I = 0x7f070e8f

.field public static final hostHasConfirmedMemberWithdraw_body_Android:I = 0x7f070daf

.field public static final hostHasNewApplicant_body_Android:I = 0x7f070db0

.field public static final ic_ABXYButtonBaseLowerLeft:I = 0x7f070e90

.field public static final ic_ABXYButtonBaseLowerRight:I = 0x7f070e91

.field public static final ic_ABXYButtonBaseTopLeft:I = 0x7f070e92

.field public static final ic_ABXYButtonBaseTopRight:I = 0x7f070e93

.field public static final ic_Accept:I = 0x7f070e94

.field public static final ic_AchievementLocked:I = 0x7f070e95

.field public static final ic_Achievements:I = 0x7f070e96

.field public static final ic_ActionbarMessage:I = 0x7f070e97

.field public static final ic_ActivityFeed:I = 0x7f070e98

.field public static final ic_ActivityFeed2:I = 0x7f070e99

.field public static final ic_Add:I = 0x7f070e9a

.field public static final ic_AddConsole:I = 0x7f070e9b

.field public static final ic_AddHome:I = 0x7f070e9c

.field public static final ic_AddTo:I = 0x7f070e9d

.field public static final ic_AddToCollection:I = 0x7f070e9e

.field public static final ic_AddToNowPlaying:I = 0x7f070e9f

.field public static final ic_AddToNowPlaying2:I = 0x7f070ea0

.field public static final ic_AdornerConnected:I = 0x7f070ea1

.field public static final ic_AllDevices:I = 0x7f070ea2

.field public static final ic_AppFill:I = 0x7f070ea3

.field public static final ic_AppFull:I = 0x7f070ea4

.field public static final ic_AppIconAvatar:I = 0x7f070ea5

.field public static final ic_AppIconDefault:I = 0x7f070ea6

.field public static final ic_AppIconHelp:I = 0x7f070ea7

.field public static final ic_AppIconParty:I = 0x7f070ea8

.field public static final ic_AppIconPeople:I = 0x7f070ea9

.field public static final ic_AppSnap:I = 0x7f070eaa

.field public static final ic_AppSwap:I = 0x7f070eab

.field public static final ic_ApplicationInstall:I = 0x7f070eac

.field public static final ic_ApplicationLaunch:I = 0x7f070ead

.field public static final ic_ApplyAction:I = 0x7f070eae

.field public static final ic_Apps:I = 0x7f070eaf

.field public static final ic_ArcadeGame:I = 0x7f070eb0

.field public static final ic_Arrow2Down:I = 0x7f070eb1

.field public static final ic_Arrow2Left:I = 0x7f070eb2

.field public static final ic_Arrow2Right:I = 0x7f070eb3

.field public static final ic_Arrow2Up:I = 0x7f070eb4

.field public static final ic_Arrow3Down:I = 0x7f070eb5

.field public static final ic_Arrow3Left:I = 0x7f070eb6

.field public static final ic_Arrow3Right:I = 0x7f070eb7

.field public static final ic_Arrow3Up:I = 0x7f070eb8

.field public static final ic_ArrowDown:I = 0x7f070eb9

.field public static final ic_ArrowLeft:I = 0x7f070eba

.field public static final ic_ArrowRight:I = 0x7f070ebb

.field public static final ic_ArrowUp:I = 0x7f070ebc

.field public static final ic_ArrowsLeftRight:I = 0x7f070ebd

.field public static final ic_ArrowsLeftRight2:I = 0x7f070ebe

.field public static final ic_ArrowsUpDown:I = 0x7f070ebf

.field public static final ic_ArrowsUpDown2:I = 0x7f070ec0

.field public static final ic_AspectRatio:I = 0x7f070ec1

.field public static final ic_Attachment:I = 0x7f070ec2

.field public static final ic_AttractMode:I = 0x7f070ec3

.field public static final ic_AvatarAward:I = 0x7f070ec4

.field public static final ic_AvatarItem:I = 0x7f070ec5

.field public static final ic_AvatarMyFeatures:I = 0x7f070ec6

.field public static final ic_AvatarNew:I = 0x7f070ec7

.field public static final ic_AvatarRotateLeft:I = 0x7f070ec8

.field public static final ic_AvatarRotateRight:I = 0x7f070ec9

.field public static final ic_Backspace:I = 0x7f070eca

.field public static final ic_BadRecommendation:I = 0x7f070ecb

.field public static final ic_Batteries:I = 0x7f070ecc

.field public static final ic_BatteryPack:I = 0x7f070ecd

.field public static final ic_BeaconsActivity:I = 0x7f070ece

.field public static final ic_BluRayRGBY:I = 0x7f070ecf

.field public static final ic_BluRaySubtitles:I = 0x7f070ed0

.field public static final ic_Bluetooth:I = 0x7f070ed1

.field public static final ic_Broadcasting:I = 0x7f070ed2

.field public static final ic_Browser:I = 0x7f070ed3

.field public static final ic_BrowserCursor:I = 0x7f070ed4

.field public static final ic_BrowserHistory:I = 0x7f070ed5

.field public static final ic_BrowserPrivate:I = 0x7f070ed6

.field public static final ic_BumperLeft:I = 0x7f070ed7

.field public static final ic_BumperRight:I = 0x7f070ed8

.field public static final ic_ButtonA:I = 0x7f070ed9

.field public static final ic_ButtonB:I = 0x7f070eda

.field public static final ic_ButtonLetterA:I = 0x7f070edb

.field public static final ic_ButtonLetterB:I = 0x7f070edc

.field public static final ic_ButtonLetterX:I = 0x7f070edd

.field public static final ic_ButtonLetterY:I = 0x7f070ede

.field public static final ic_ButtonMenu:I = 0x7f070edf

.field public static final ic_ButtonView:I = 0x7f070ee0

.field public static final ic_ButtonX:I = 0x7f070ee1

.field public static final ic_ButtonY:I = 0x7f070ee2

.field public static final ic_Cables:I = 0x7f070ee3

.field public static final ic_Calculator:I = 0x7f070ee4

.field public static final ic_Camera:I = 0x7f070ee5

.field public static final ic_CameraTimer:I = 0x7f070ee6

.field public static final ic_CardPrepaid:I = 0x7f070ee7

.field public static final ic_Cart:I = 0x7f070ee8

.field public static final ic_ChangeTheme:I = 0x7f070ee9

.field public static final ic_ChargingCable:I = 0x7f070eea

.field public static final ic_Chat:I = 0x7f070eeb

.field public static final ic_ChatFilled:I = 0x7f070eec

.field public static final ic_ChatHeadset:I = 0x7f070eed

.field public static final ic_ChatInviteFriend:I = 0x7f070eee

.field public static final ic_ChatMute:I = 0x7f070eef

.field public static final ic_ChatVideo:I = 0x7f070ef0

.field public static final ic_CheckBox:I = 0x7f070ef1

.field public static final ic_CheckBoxChecked:I = 0x7f070ef2

.field public static final ic_ChevronDown:I = 0x7f070ef3

.field public static final ic_ChevronLeft:I = 0x7f070ef4

.field public static final ic_ChevronRight:I = 0x7f070ef5

.field public static final ic_ChevronUp:I = 0x7f070ef6

.field public static final ic_Close:I = 0x7f070ef7

.field public static final ic_ClosedCaptions:I = 0x7f070ef8

.field public static final ic_ClosedCaptionsInternational:I = 0x7f070ef9

.field public static final ic_Cloud:I = 0x7f070efa

.field public static final ic_CloudNotSynced:I = 0x7f070efb

.field public static final ic_CloudNotSynced2:I = 0x7f070efc

.field public static final ic_CloudSyncing:I = 0x7f070efd

.field public static final ic_ClubHidden:I = 0x7f070efe

.field public static final ic_Clubs:I = 0x7f070eff

.field public static final ic_Color:I = 0x7f070f00

.field public static final ic_ColorWheel:I = 0x7f070f01

.field public static final ic_CompareGames:I = 0x7f070f02

.field public static final ic_Complaint:I = 0x7f070f03

.field public static final ic_ComplaintError:I = 0x7f070f04

.field public static final ic_ComposeMessage:I = 0x7f070f05

.field public static final ic_Computer:I = 0x7f070f06

.field public static final ic_ConnectToConsole:I = 0x7f070f07

.field public static final ic_Console:I = 0x7f070f08

.field public static final ic_ConsoleControllerPluggedIn:I = 0x7f070f09

.field public static final ic_ConsoleInitialSetup:I = 0x7f070f0a

.field public static final ic_ConsoleMultiGuest:I = 0x7f070f0b

.field public static final ic_ConsoleSettings:I = 0x7f070f0c

.field public static final ic_ConsoleSleep:I = 0x7f070f0d

.field public static final ic_ControllerBatteryFull:I = 0x7f070f0e

.field public static final ic_ControllerBatteryHalf:I = 0x7f070f0f

.field public static final ic_ControllerBatteryLow:I = 0x7f070f10

.field public static final ic_ControllerBatteryThreeQuarter:I = 0x7f070f11

.field public static final ic_ControllerNewRelease:I = 0x7f070f12

.field public static final ic_ControllerOff:I = 0x7f070f13

.field public static final ic_Crown:I = 0x7f070f14

.field public static final ic_CursorHand:I = 0x7f070f15

.field public static final ic_CursorHandClosed:I = 0x7f070f16

.field public static final ic_Delete:I = 0x7f070f17

.field public static final ic_Details:I = 0x7f070f18

.field public static final ic_Diamond:I = 0x7f070f19

.field public static final ic_DiscArtist:I = 0x7f070f1a

.field public static final ic_DiscBad:I = 0x7f070f1b

.field public static final ic_DiscCantPlay:I = 0x7f070f1c

.field public static final ic_DiscGame:I = 0x7f070f1d

.field public static final ic_DiscMusic:I = 0x7f070f1e

.field public static final ic_Download:I = 0x7f070f1f

.field public static final ic_DownloadSeeAll:I = 0x7f070f20

.field public static final ic_Dpad:I = 0x7f070f21

.field public static final ic_DpadDown:I = 0x7f070f22

.field public static final ic_DpadLeft:I = 0x7f070f23

.field public static final ic_DpadRight:I = 0x7f070f24

.field public static final ic_DpadUp:I = 0x7f070f25

.field public static final ic_Edit:I = 0x7f070f26

.field public static final ic_Eject:I = 0x7f070f27

.field public static final ic_Epg:I = 0x7f070f28

.field public static final ic_Explore:I = 0x7f070f29

.field public static final ic_ExploreContent:I = 0x7f070f2a

.field public static final ic_Eye:I = 0x7f070f2b

.field public static final ic_FacebookLogo:I = 0x7f070f2c

.field public static final ic_Family:I = 0x7f070f2d

.field public static final ic_FastForward:I = 0x7f070f2e

.field public static final ic_Favorite:I = 0x7f070f2f

.field public static final ic_FavoriteAdd:I = 0x7f070f30

.field public static final ic_FavoriteHelp:I = 0x7f070f31

.field public static final ic_FavoriteRemove:I = 0x7f070f32

.field public static final ic_FavoriteRemove2:I = 0x7f070f33

.field public static final ic_FilterIcon:I = 0x7f070f34

.field public static final ic_Fitness:I = 0x7f070f35

.field public static final ic_Flag:I = 0x7f070f36

.field public static final ic_Folder:I = 0x7f070f37

.field public static final ic_FolderAllItems:I = 0x7f070f38

.field public static final ic_FolderAvatarItems:I = 0x7f070f39

.field public static final ic_FolderDemos:I = 0x7f070f3a

.field public static final ic_FolderFindContent:I = 0x7f070f3b

.field public static final ic_FolderGamerPics:I = 0x7f070f3c

.field public static final ic_FolderGames:I = 0x7f070f3d

.field public static final ic_FolderGenre:I = 0x7f070f3e

.field public static final ic_FolderMusic:I = 0x7f070f3f

.field public static final ic_FolderProfile:I = 0x7f070f40

.field public static final ic_FolderSystemItems:I = 0x7f070f41

.field public static final ic_FolderThemes:I = 0x7f070f42

.field public static final ic_FolderVideo:I = 0x7f070f43

.field public static final ic_ForearmStrap:I = 0x7f070f44

.field public static final ic_FullScreen:I = 0x7f070f45

.field public static final ic_Game:I = 0x7f070f46

.field public static final ic_GameAdd:I = 0x7f070f47

.field public static final ic_GameCompare:I = 0x7f070f48

.field public static final ic_GameDemos:I = 0x7f070f49

.field public static final ic_GameFavorite:I = 0x7f070f4a

.field public static final ic_GameJoin:I = 0x7f070f4b

.field public static final ic_GamePlay:I = 0x7f070f4c

.field public static final ic_GameProfile:I = 0x7f070f4d

.field public static final ic_GameRecommendations:I = 0x7f070f4e

.field public static final ic_GameRemove:I = 0x7f070f4f

.field public static final ic_GameWait:I = 0x7f070f50

.field public static final ic_GamerScore:I = 0x7f070f51

.field public static final ic_GamerTagChange:I = 0x7f070f52

.field public static final ic_GamerTagSettings:I = 0x7f070f53

.field public static final ic_GamerTagTakePicture:I = 0x7f070f54

.field public static final ic_GoToEnd:I = 0x7f070f55

.field public static final ic_GoldSubscription:I = 0x7f070f56

.field public static final ic_GotoLiveBroadcast:I = 0x7f070f57

.field public static final ic_Harddrive:I = 0x7f070f58

.field public static final ic_HdQualityResolution:I = 0x7f070f59

.field public static final ic_HdmiCable:I = 0x7f070f5a

.field public static final ic_HeadsetAdapter:I = 0x7f070f5b

.field public static final ic_HeartBroken:I = 0x7f070f5c

.field public static final ic_HeartEmpty:I = 0x7f070f5d

.field public static final ic_HeartFull:I = 0x7f070f5e

.field public static final ic_Home:I = 0x7f070f5f

.field public static final ic_Info:I = 0x7f070f60

.field public static final ic_Info16:I = 0x7f070f61

.field public static final ic_Infrastructure:I = 0x7f070f62

.field public static final ic_InstructionManual:I = 0x7f070f63

.field public static final ic_Internet:I = 0x7f070f64

.field public static final ic_InternetWithController:I = 0x7f070f65

.field public static final ic_Key:I = 0x7f070f66

.field public static final ic_Keyboard:I = 0x7f070f67

.field public static final ic_Kinect:I = 0x7f070f68

.field public static final ic_KinectBroadcasting:I = 0x7f070f69

.field public static final ic_KinectDisconnected:I = 0x7f070f6a

.field public static final ic_KinectIR:I = 0x7f070f6b

.field public static final ic_KinectSettings:I = 0x7f070f6c

.field public static final ic_LeftStickClick:I = 0x7f070f6d

.field public static final ic_LeftThumbDown:I = 0x7f070f6e

.field public static final ic_LeftThumbLeft:I = 0x7f070f6f

.field public static final ic_LeftThumbRight:I = 0x7f070f70

.field public static final ic_LeftThumbUp:I = 0x7f070f71

.field public static final ic_LfgCreateParty:I = 0x7f070f72

.field public static final ic_LfgParty:I = 0x7f070f73

.field public static final ic_LicenseStore:I = 0x7f070f74

.field public static final ic_List:I = 0x7f070f75

.field public static final ic_ListBulleted:I = 0x7f070f76

.field public static final ic_ListJustified:I = 0x7f070f77

.field public static final ic_LiveText:I = 0x7f070f78

.field public static final ic_Lock:I = 0x7f070f79

.field public static final ic_MediaDisc:I = 0x7f070f7a

.field public static final ic_MediaDvd:I = 0x7f070f7b

.field public static final ic_MediaHdDvd:I = 0x7f070f7c

.field public static final ic_MediaMusic:I = 0x7f070f7d

.field public static final ic_MediaRemote:I = 0x7f070f7e

.field public static final ic_MemoryUnitExternal:I = 0x7f070f7f

.field public static final ic_MemoryUnitInternal:I = 0x7f070f80

.field public static final ic_Menu:I = 0x7f070f81

.field public static final ic_MenuTrading:I = 0x7f070f82

.field public static final ic_Message:I = 0x7f070f83

.field public static final ic_MessageFriendRequest:I = 0x7f070f84

.field public static final ic_MessageJoinParty:I = 0x7f070f85

.field public static final ic_MessageReply:I = 0x7f070f86

.field public static final ic_MessageSend:I = 0x7f070f87

.field public static final ic_MessageVideo:I = 0x7f070f88

.field public static final ic_MessageVoice:I = 0x7f070f89

.field public static final ic_MicOff:I = 0x7f070f8a

.field public static final ic_MicOff2:I = 0x7f070f8b

.field public static final ic_MicOn:I = 0x7f070f8c

.field public static final ic_MicrosoftPoints:I = 0x7f070f8d

.field public static final ic_ModeSelector:I = 0x7f070f8e

.field public static final ic_MoreActions:I = 0x7f070f8f

.field public static final ic_MostPopular:I = 0x7f070f90

.field public static final ic_MoveToBack:I = 0x7f070f91

.field public static final ic_MoveToFront:I = 0x7f070f92

.field public static final ic_Movies:I = 0x7f070f93

.field public static final ic_MultiPlayerGold:I = 0x7f070f94

.field public static final ic_MultiPlayerJoinChallenge:I = 0x7f070f95

.field public static final ic_MultiPlayerLeaveParty:I = 0x7f070f96

.field public static final ic_MultiPlayerPartyJoinable:I = 0x7f070f97

.field public static final ic_MultiPlayerShare:I = 0x7f070f98

.field public static final ic_MultiPlayerTimer:I = 0x7f070f99

.field public static final ic_MultiPlayerWimax:I = 0x7f070f9a

.field public static final ic_Music:I = 0x7f070f9b

.field public static final ic_MusicTracks:I = 0x7f070f9c

.field public static final ic_Navigation:I = 0x7f070f9d

.field public static final ic_NegativeFeedback:I = 0x7f070f9e

.field public static final ic_NegativeFeedbackCircled:I = 0x7f070f9f

.field public static final ic_Network:I = 0x7f070fa0

.field public static final ic_NetworkConnected:I = 0x7f070fa1

.field public static final ic_NetworkDisconnected:I = 0x7f070fa2

.field public static final ic_NetworkDisconnected2:I = 0x7f070fa3

.field public static final ic_NetworkSettings:I = 0x7f070fa4

.field public static final ic_Networks:I = 0x7f070fa5

.field public static final ic_NewReleases:I = 0x7f070fa6

.field public static final ic_Next:I = 0x7f070fa7

.field public static final ic_NoThumbnail:I = 0x7f070fa8

.field public static final ic_NoVideoCamera:I = 0x7f070fa9

.field public static final ic_NoVideoCamera2:I = 0x7f070faa

.field public static final ic_NotOnline:I = 0x7f070fab

.field public static final ic_NowPlaying:I = 0x7f070fac

.field public static final ic_OptionalMediaUpdate:I = 0x7f070fad

.field public static final ic_PC:I = 0x7f070fae

.field public static final ic_ParentalLock:I = 0x7f070faf

.field public static final ic_PartyLeader:I = 0x7f070fb0

.field public static final ic_Pause:I = 0x7f070fb1

.field public static final ic_PhoneCall:I = 0x7f070fb2

.field public static final ic_PhoneContacts:I = 0x7f070fb3

.field public static final ic_PhoneHangUp:I = 0x7f070fb4

.field public static final ic_Pin:I = 0x7f070fb5

.field public static final ic_PinSolid:I = 0x7f070fb6

.field public static final ic_Placeholder:I = 0x7f070fb7

.field public static final ic_Play:I = 0x7f070fb8

.field public static final ic_PlayPause:I = 0x7f070fb9

.field public static final ic_PlayWithCircle:I = 0x7f070fba

.field public static final ic_Player:I = 0x7f070fbb

.field public static final ic_PlayerAchievement:I = 0x7f070fbc

.field public static final ic_PlayerAdd:I = 0x7f070fbd

.field public static final ic_PlayerDownloadProfile:I = 0x7f070fbe

.field public static final ic_PlayerGold:I = 0x7f070fbf

.field public static final ic_PlayerKinect:I = 0x7f070fc0

.field public static final ic_PlayerRecoverProfile:I = 0x7f070fc1

.field public static final ic_PlayerRemove:I = 0x7f070fc2

.field public static final ic_PlayerRespond:I = 0x7f070fc3

.field public static final ic_PlayerSettings:I = 0x7f070fc4

.field public static final ic_PlayerSignIn:I = 0x7f070fc5

.field public static final ic_PlayerSignOut:I = 0x7f070fc6

.field public static final ic_PlayerSkipSignIn:I = 0x7f070fc7

.field public static final ic_PlayerSwitchProfile:I = 0x7f070fc8

.field public static final ic_PlayerWait:I = 0x7f070fc9

.field public static final ic_Playlist:I = 0x7f070fca

.field public static final ic_PlaylistAdd:I = 0x7f070fcb

.field public static final ic_PlaylistGenre:I = 0x7f070fcc

.field public static final ic_PlaylistMusic:I = 0x7f070fcd

.field public static final ic_PlaylistRemove:I = 0x7f070fce

.field public static final ic_Power:I = 0x7f070fcf

.field public static final ic_PowerCord:I = 0x7f070fd0

.field public static final ic_PowerSupply:I = 0x7f070fd1

.field public static final ic_Previous:I = 0x7f070fd2

.field public static final ic_Profile:I = 0x7f070fd3

.field public static final ic_ProfileSearch:I = 0x7f070fd4

.field public static final ic_QRCode:I = 0x7f070fd5

.field public static final ic_Question:I = 0x7f070fd6

.field public static final ic_RadioBtnOff:I = 0x7f070fd7

.field public static final ic_RadioBtnOn:I = 0x7f070fd8

.field public static final ic_Ratings:I = 0x7f070fd9

.field public static final ic_Reading:I = 0x7f070fda

.field public static final ic_Recent:I = 0x7f070fdb

.field public static final ic_Record:I = 0x7f070fdc

.field public static final ic_RecordThat:I = 0x7f070fdd

.field public static final ic_Related:I = 0x7f070fde

.field public static final ic_Reminder:I = 0x7f070fdf

.field public static final ic_Remove:I = 0x7f070fe0

.field public static final ic_Repairs:I = 0x7f070fe1

.field public static final ic_Repeat:I = 0x7f070fe2

.field public static final ic_RepeatBlock:I = 0x7f070fe3

.field public static final ic_RepeatOnce:I = 0x7f070fe4

.field public static final ic_RepeatRefresh:I = 0x7f070fe5

.field public static final ic_Replay:I = 0x7f070fe6

.field public static final ic_Reward:I = 0x7f070fe7

.field public static final ic_Rewind:I = 0x7f070fe8

.field public static final ic_RightStickClick:I = 0x7f070fe9

.field public static final ic_RightThumbDown:I = 0x7f070fea

.field public static final ic_RightThumbLeft:I = 0x7f070feb

.field public static final ic_RightThumbRight:I = 0x7f070fec

.field public static final ic_RightThumbUp:I = 0x7f070fed

.field public static final ic_RingOfLight:I = 0x7f070fee

.field public static final ic_RotateClockwise:I = 0x7f070fef

.field public static final ic_RotateCounterClockwise:I = 0x7f070ff0

.field public static final ic_SGFavorite:I = 0x7f070ff1

.field public static final ic_Save:I = 0x7f070ff2

.field public static final ic_ScreenBar:I = 0x7f070ff3

.field public static final ic_Search:I = 0x7f070ff4

.field public static final ic_SelectScene:I = 0x7f070ff5

.field public static final ic_SelectedFilterIcon:I = 0x7f070ff6

.field public static final ic_SemanticZoom:I = 0x7f070ff7

.field public static final ic_SendMessage:I = 0x7f070ff8

.field public static final ic_Sensor:I = 0x7f070ff9

.field public static final ic_SensorWithStand:I = 0x7f070ffa

.field public static final ic_Settings:I = 0x7f070ffb

.field public static final ic_SettingsDisplaySound:I = 0x7f070ffc

.field public static final ic_SgControllerDown:I = 0x7f070ffd

.field public static final ic_SgControllerLeft:I = 0x7f070ffe

.field public static final ic_SgControllerRight:I = 0x7f070fff

.field public static final ic_SgControllerUp:I = 0x7f071000

.field public static final ic_Share:I = 0x7f071001

.field public static final ic_ShuffleOff:I = 0x7f071002

.field public static final ic_ShuffleOn:I = 0x7f071003

.field public static final ic_SignalFull:I = 0x7f071004

.field public static final ic_SignalHalf:I = 0x7f071005

.field public static final ic_SignalLow:I = 0x7f071006

.field public static final ic_SignalNone:I = 0x7f071007

.field public static final ic_SignalThreeQuarter:I = 0x7f071008

.field public static final ic_SkipBack:I = 0x7f071009

.field public static final ic_SkipForward:I = 0x7f07100a

.field public static final ic_SmartDj:I = 0x7f07100b

.field public static final ic_SmartGlass:I = 0x7f07100c

.field public static final ic_SmartGlassConnected:I = 0x7f07100d

.field public static final ic_SmartGlassConnectingConsole:I = 0x7f07100e

.field public static final ic_SmartGlassConnectingRingsFull:I = 0x7f07100f

.field public static final ic_SmartGlassConnectingRingsInner:I = 0x7f071010

.field public static final ic_SmartGlassConnectingRingsMid:I = 0x7f071011

.field public static final ic_SmartGlassConnectingRingsOuter:I = 0x7f071012

.field public static final ic_SmartGlassGamerScore:I = 0x7f071013

.field public static final ic_SmartGlassPhone:I = 0x7f071014

.field public static final ic_SmartGlassRemote:I = 0x7f071015

.field public static final ic_SmartGlassTablet:I = 0x7f071016

.field public static final ic_SmartVj:I = 0x7f071017

.field public static final ic_Smiley:I = 0x7f071018

.field public static final ic_Sports:I = 0x7f071019

.field public static final ic_Stack:I = 0x7f07101a

.field public static final ic_StandardDefinition:I = 0x7f07101b

.field public static final ic_StartBack:I = 0x7f07101c

.field public static final ic_StartParty:I = 0x7f07101d

.field public static final ic_StepBack:I = 0x7f07101e

.field public static final ic_StepForward:I = 0x7f07101f

.field public static final ic_Stop:I = 0x7f071020

.field public static final ic_Store:I = 0x7f071021

.field public static final ic_Store2:I = 0x7f071022

.field public static final ic_StoreMS:I = 0x7f071023

.field public static final ic_Streaming:I = 0x7f071024

.field public static final ic_SubscriptionAdd:I = 0x7f071025

.field public static final ic_Subscriptions:I = 0x7f071026

.field public static final ic_Subscriptions2:I = 0x7f071027

.field public static final ic_Swap:I = 0x7f071028

.field public static final ic_SwitchFocus:I = 0x7f071029

.field public static final ic_SwitcherMultitask:I = 0x7f07102a

.field public static final ic_Tag:I = 0x7f07102b

.field public static final ic_TimeForward:I = 0x7f07102c

.field public static final ic_TitleMenu:I = 0x7f07102d

.field public static final ic_Tournament:I = 0x7f07102e

.field public static final ic_TransferCable:I = 0x7f07102f

.field public static final ic_TriggerLeft:I = 0x7f071030

.field public static final ic_TriggerRight:I = 0x7f071031

.field public static final ic_Tutorial:I = 0x7f071032

.field public static final ic_TvBluRaySettings:I = 0x7f071033

.field public static final ic_TvCableSettings:I = 0x7f071034

.field public static final ic_TvMonitor:I = 0x7f071035

.field public static final ic_TvRecommendationsTv:I = 0x7f071036

.field public static final ic_TvSettings:I = 0x7f071037

.field public static final ic_UnSnap:I = 0x7f071038

.field public static final ic_Unlock:I = 0x7f071039

.field public static final ic_UnlockedAchievement:I = 0x7f07103a

.field public static final ic_Unpin:I = 0x7f07103b

.field public static final ic_Unpin2:I = 0x7f07103c

.field public static final ic_UnrecognizedFormat:I = 0x7f07103d

.field public static final ic_Usb:I = 0x7f07103e

.field public static final ic_UsbDevice:I = 0x7f07103f

.field public static final ic_UserTypeAdult:I = 0x7f071040

.field public static final ic_UserTypeChild:I = 0x7f071041

.field public static final ic_UserTypeTeen:I = 0x7f071042

.field public static final ic_Video:I = 0x7f071043

.field public static final ic_Video1:I = 0x7f071044

.field public static final ic_Video11:I = 0x7f071045

.field public static final ic_VideoBanned:I = 0x7f071046

.field public static final ic_VideoCamera:I = 0x7f071047

.field public static final ic_VideoCameraDisconnected:I = 0x7f071048

.field public static final ic_VideoGames:I = 0x7f071049

.field public static final ic_VideoNewRelease:I = 0x7f07104a

.field public static final ic_VideoPopular:I = 0x7f07104b

.field public static final ic_VideoRecommendations:I = 0x7f07104c

.field public static final ic_ViewProfileDetails:I = 0x7f07104d

.field public static final ic_VirtualKeyboardClose:I = 0x7f07104e

.field public static final ic_Volume:I = 0x7f07104f

.field public static final ic_VolumeDisabled:I = 0x7f071050

.field public static final ic_VolumeDisabled2:I = 0x7f071051

.field public static final ic_VolumeMute:I = 0x7f071052

.field public static final ic_Warning:I = 0x7f071053

.field public static final ic_Wifi:I = 0x7f071054

.field public static final ic_Wifi1:I = 0x7f071055

.field public static final ic_Wifi2:I = 0x7f071056

.field public static final ic_Wifi3:I = 0x7f071057

.field public static final ic_WifiWarning4:I = 0x7f071058

.field public static final ic_XPA:I = 0x7f071059

.field public static final ic_Xbox360:I = 0x7f07105a

.field public static final ic_XboxClassic:I = 0x7f07105b

.field public static final ic_XboxGamePassIcon:I = 0x7f07105c

.field public static final ic_XboxOneDown:I = 0x7f07105d

.field public static final ic_XboxOneUp:I = 0x7f07105e

.field public static final ic_Xenon:I = 0x7f07105f

.field public static final ic_ZoomIn:I = 0x7f071060

.field public static final ic_ZoomOut:I = 0x7f071061

.field public static final ic_changeFriendShip:I = 0x7f071062

.field public static final ic_friends:I = 0x7f071063

.field public static final imminentLfgApplicantConfirmed_body_Android:I = 0x7f070db1

.field public static final imminentLfgExpiredForApplicant_body_Android:I = 0x7f070db2

.field public static final imminentLfgExpiredForHost_body_Android:I = 0x7f070db3

.field public static final imminentLfgFilledWithoutApplicant_body_Android:I = 0x7f070db4

.field public static final imminentLfgNotification_title:I = 0x7f070db5

.field public static final lfgNotification_title:I = 0x7f070db6

.field public static final media_details_metacritic:I = 0x7f071064

.field public static final message_seperator:I = 0x7f071065

.field public static final message_textcount_foramt:I = 0x7f071066

.field public static final messenger_send_button_text:I = 0x7f070044

.field public static final more_actions:I = 0x7f071067

.field public static final narrator_btn_arrow_down:I = 0x7f071068

.field public static final narrator_btn_arrow_left:I = 0x7f071069

.field public static final narrator_btn_arrow_right:I = 0x7f07106a

.field public static final narrator_btn_arrow_up:I = 0x7f07106b

.field public static final narrator_btn_back:I = 0x7f07106c

.field public static final narrator_btn_blue:I = 0x7f07106d

.field public static final narrator_btn_channel_down:I = 0x7f07106e

.field public static final narrator_btn_channel_up:I = 0x7f07106f

.field public static final narrator_btn_fast_forward:I = 0x7f071070

.field public static final narrator_btn_green:I = 0x7f071071

.field public static final narrator_btn_input:I = 0x7f071072

.field public static final narrator_btn_more:I = 0x7f071073

.field public static final narrator_btn_mute:I = 0x7f071074

.field public static final narrator_btn_page_down:I = 0x7f071075

.field public static final narrator_btn_page_up:I = 0x7f071076

.field public static final narrator_btn_pause:I = 0x7f071077

.field public static final narrator_btn_play:I = 0x7f071078

.field public static final narrator_btn_power:I = 0x7f071079

.field public static final narrator_btn_previous_channel:I = 0x7f07107a

.field public static final narrator_btn_record:I = 0x7f07107b

.field public static final narrator_btn_red:I = 0x7f07107c

.field public static final narrator_btn_rewind:I = 0x7f07107d

.field public static final narrator_btn_skip_forward:I = 0x7f07107e

.field public static final narrator_btn_stop:I = 0x7f07107f

.field public static final narrator_btn_volume_down:I = 0x7f071080

.field public static final narrator_btn_volume_up:I = 0x7f071081

.field public static final narrator_btn_yellow:I = 0x7f071082

.field public static final pick_image_intent_chooser_title:I = 0x7f071083

.field public static final pipe_delimiter:I = 0x7f071084

.field public static final platform_pc:I = 0x7f071085

.field public static final platform_xbox:I = 0x7f071086

.field public static final power_off:I = 0x7f071087

.field public static final power_on:I = 0x7f071088

.field public static final power_on_off:I = 0x7f071089

.field public static final power_stb:I = 0x7f07108a

.field public static final power_system:I = 0x7f07108b

.field public static final power_tv:I = 0x7f07108c

.field public static final scheduledLfgApplicantConfirmed_body_Android:I = 0x7f070db7

.field public static final scheduledLfgCanceled_body_Android:I = 0x7f070db8

.field public static final scheduledLfgExpiredForApplicant_body_Android:I = 0x7f070db9

.field public static final scheduledLfgExpiredForHost_body_Android:I = 0x7f070dba

.field public static final scheduledLfgExpired_body_Android:I = 0x7f070dbb

.field public static final scheduledLfgFilledWithoutApplicant_body_Android:I = 0x7f070dbc

.field public static final scheduledLfgNotification_title:I = 0x7f070dbd

.field public static final sdk_version_name:I = 0x7f07108d

.field public static final search_data_result_filter_count:I = 0x7f07108e

.field public static final search_menu_title:I = 0x7f070045

.field public static final sign_out_dialog_button_cancel:I = 0x7f070055

.field public static final sign_out_dialog_button_sign_out:I = 0x7f070056

.field public static final sign_out_dialog_checkbox:I = 0x7f070057

.field public static final sign_out_dialog_title:I = 0x7f070058

.field public static final slash_delimiter:I = 0x7f07108f

.field public static final status_bar_notification_info_overflow:I = 0x7f070046

.field public static final store_picture_message:I = 0x7f071090

.field public static final store_picture_title:I = 0x7f071091

.field public static final teamInvite_body:I = 0x7f070dbe

.field public static final teamJoin_body:I = 0x7f070dbf

.field public static final teamLeave_body:I = 0x7f070dc0

.field public static final time_remaining_expired:I = 0x7f071092

.field public static final time_remaining_expired_days:I = 0x7f071093

.field public static final tournamentMatchAvailable_body_Android:I = 0x7f070dc1

.field public static final tournamentNotification_title:I = 0x7f070dc2

.field public static final tournamentStatusChange_canceled_body_Android:I = 0x7f070dc3

.field public static final tournamentStatusChange_checkinstarted_body_Android:I = 0x7f070dc4

.field public static final tournamentStatusChange_completed_body_Android:I = 0x7f070dc5

.field public static final tournamentStatusChange_started_body_Android:I = 0x7f070dc6

.field public static final tournamentTeamStatusChange_checkedin_body_Android:I = 0x7f070dc7

.field public static final tournamentTeamStatusChange_eliminated_body_Android:I = 0x7f070dc8

.field public static final tournamentTeamStatusChange_missedcheckin_body_Android:I = 0x7f070dc9

.field public static final tournamentTeamStatusChange_playing_body_Android:I = 0x7f070dca

.field public static final tournamentTeamStatusChange_registered_body_Android:I = 0x7f070dcb

.field public static final tournamentTeamStatusChange_standby_body_Android:I = 0x7f070dcc

.field public static final tournamentTeamStatusChange_unregistered_body_Android:I = 0x7f070dcd

.field public static final tournamentTeamStatusChange_waitlisted_body_Android:I = 0x7f070dce

.field public static final user_tile_image_content_description:I = 0x7f070059

.field public static final volume_down:I = 0x7f071094

.field public static final volume_muteToggle:I = 0x7f071095

.field public static final volume_up:I = 0x7f071096

.field public static final webflow_header:I = 0x7f07005a

.field public static final whatsnew_link1:I = 0x7f071097

.field public static final whatsnew_link2:I = 0x7f071098

.field public static final whatsnew_link3:I = 0x7f071099

.field public static final whatsnew_parameter2:I = 0x7f07109a

.field public static final xbid_age_group_adult:I = 0x7f070dcf

.field public static final xbid_age_group_adult_details_android:I = 0x7f070dd0

.field public static final xbid_age_group_child:I = 0x7f070dd1

.field public static final xbid_age_group_child_details_android:I = 0x7f070dd2

.field public static final xbid_age_group_teen:I = 0x7f070dd3

.field public static final xbid_age_group_teen_details_android:I = 0x7f070dd4

.field public static final xbid_another_gamertag:I = 0x7f070dd5

.field public static final xbid_another_sign_in:I = 0x7f070dd6

.field public static final xbid_ban_error_body_android:I = 0x7f070dd7

.field public static final xbid_ban_error_header_android:I = 0x7f070dd8

.field public static final xbid_catchall_error_android:I = 0x7f070dd9

.field public static final xbid_claim_it:I = 0x7f070dda

.field public static final xbid_close:I = 0x7f070ddb

.field public static final xbid_cobrand_id:I = 0x7f07109b

.field public static final xbid_creation_error_android:I = 0x7f070ddc

.field public static final xbid_customize_profile:I = 0x7f070ddd

.field public static final xbid_different_gamer_tag_answer:I = 0x7f070dde

.field public static final xbid_error_offline_android:I = 0x7f070ddf

.field public static final xbid_first_and_last_name_android:I = 0x7f070de0

.field public static final xbid_friend_finder_opt_in:I = 0x7f070de1

.field public static final xbid_gamertag_available:I = 0x7f070de2

.field public static final xbid_gamertag_check_availability:I = 0x7f070de3

.field public static final xbid_gamertag_checking_android:I = 0x7f070de4

.field public static final xbid_gamertag_checking_error:I = 0x7f070de5

.field public static final xbid_gamertag_not_available_android:I = 0x7f070de6

.field public static final xbid_gamertag_not_available_no_suggestions_android:I = 0x7f070de7

.field public static final xbid_gamertag_placeholder:I = 0x7f070de8

.field public static final xbid_introducing_android:I = 0x7f070de9

.field public static final xbid_lets_play_android:I = 0x7f070dea

.field public static final xbid_more_info:I = 0x7f070deb

.field public static final xbid_next:I = 0x7f070dec

.field public static final xbid_privacy_settings_header_android:I = 0x7f070ded

.field public static final xbid_sign_up_header:I = 0x7f070dee

.field public static final xbid_sign_up_subheader_android:I = 0x7f070def

.field public static final xbid_tools_email:I = 0x7f07109c

.field public static final xbid_tools_empty:I = 0x7f07109d

.field public static final xbid_tools_gamertag:I = 0x7f07109e

.field public static final xbid_tools_user_name:I = 0x7f07109f

.field public static final xbid_try_again:I = 0x7f070df0

.field public static final xbid_voice_over_close_button_text:I = 0x7f070df1

.field public static final xbid_voice_over_game_score_label_text:I = 0x7f070df2

.field public static final xbid_welcome_back:I = 0x7f070df3

.field public static final xbox_music_title:I = 0x7f0710a0

.field public static final xbox_video_title:I = 0x7f0710a1

.field public static final xpa_badging_play_anywhere:I = 0x7f0710a2

.field public static final xpa_badging_xbox:I = 0x7f0710a3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
