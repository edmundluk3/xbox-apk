.class public abstract Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
.super Ljava/lang/Object;
.source "PartyDetailsViewState.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static loadingInstance()Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 37
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;

    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v2, v1

    move-object v4, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;ZLio/reactivex/Observable;ZLjava/lang/Throwable;)V

    return-object v0
.end method

.method public static withContent(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;Z)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .locals 7
    .param p0, "oldState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "partySession"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "userAllowsBroadcasting"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;Z)",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;"
        }
    .end annotation

    .prologue
    .line 65
    .local p2, "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 66
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;

    .line 70
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->memberStream()Lio/reactivex/Observable;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;ZLio/reactivex/Observable;ZLjava/lang/Throwable;)V

    .line 66
    return-object v0
.end method

.method public static withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .locals 7
    .param p0, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 77
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 78
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;

    move-object v2, v1

    move-object v4, v1

    move v5, v3

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;ZLio/reactivex/Observable;ZLjava/lang/Throwable;)V

    return-object v0
.end method

.method public static withStream(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;Lio/reactivex/Observable;)Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .locals 7
    .param p0, "oldState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lio/reactivex/Observable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;)",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 50
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 51
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;

    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v1

    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userAllowsBroadcasting()Z

    move-result v3

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->isLoading()Z

    move-result v5

    .line 57
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->error()Ljava/lang/Throwable;

    move-result-object v6

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyDetailsViewState;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;ZLio/reactivex/Observable;ZLjava/lang/Throwable;)V

    .line 51
    return-object v0
.end method


# virtual methods
.method public abstract error()Ljava/lang/Throwable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public hasLFG()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public abstract isLoading()Z
.end method

.method public abstract memberStream()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end method

.method public abstract partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->isLoading()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    const-string v1, "Loading"

    .line 106
    :goto_0
    return-object v1

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->error()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 97
    const-string v1, "Error"

    goto :goto_0

    .line 99
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .local v0, "str":Ljava/lang/StringBuilder;
    const-string v1, "party="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string v1, "party"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    const-string v1, ",summaries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 104
    const-string v1, ",userAllowsBroadcasting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userAllowsBroadcasting()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 101
    :cond_2
    const-string v1, "null"

    goto :goto_1

    .line 103
    :cond_3
    const-string v1, "null"

    goto :goto_2
.end method

.method public abstract userAllowsBroadcasting()Z
.end method

.method public abstract userSummaries()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation
.end method
