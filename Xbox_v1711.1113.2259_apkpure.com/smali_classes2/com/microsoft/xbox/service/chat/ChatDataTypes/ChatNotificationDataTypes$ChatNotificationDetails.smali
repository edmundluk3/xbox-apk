.class public Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;
.super Ljava/lang/Object;
.source "ChatNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChatNotificationDetails"
.end annotation


# instance fields
.field public final channelId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field private volatile transient chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

.field private volatile transient hashCode:I

.field public final messageText:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final notificationType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "notificationType"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "messageText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "channelId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 158
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 160
    iput-object p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->notificationType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    .line 161
    iput-object p2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->messageText:Ljava/lang/String;

    .line 162
    iput-object p3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->channelId:Ljava/lang/String;

    .line 163
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 177
    if-ne p1, p0, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v1

    .line 179
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;

    if-nez v3, :cond_2

    move v1, v2

    .line 180
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 182
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;

    .line 183
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;
    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->notificationType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->notificationType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->messageText:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->messageText:Ljava/lang/String;

    .line 184
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->channelId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->channelId:Ljava/lang/String;

    .line 185
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getChatChannel()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    if-nez v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->channelId:Ljava/lang/String;

    const-class v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    const-class v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    new-instance v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelIdJsonAdapter;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelIdJsonAdapter;-><init>()V

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->chatChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 191
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->hashCode:I

    if-nez v0, :cond_0

    .line 192
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->hashCode:I

    .line 193
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->notificationType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$NotificationType;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->hashCode:I

    .line 194
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->messageText:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->hashCode:I

    .line 195
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->channelId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->hashCode:I

    .line 198
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationDetails;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
