.class public final enum Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;
.super Ljava/lang/Enum;
.source "NavigationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NavigationGeneric"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

.field public static final enum AUTHOR_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

.field public static final enum ITEM_DETAIL:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 425
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    const-string v1, "ITEM_DETAIL"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->ITEM_DETAIL:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    const-string v1, "AUTHOR_PROFILE"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->AUTHOR_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    .line 424
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->ITEM_DETAIL:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->AUTHOR_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 424
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 424
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;
    .locals 1

    .prologue
    .line 424
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    return-object v0
.end method
