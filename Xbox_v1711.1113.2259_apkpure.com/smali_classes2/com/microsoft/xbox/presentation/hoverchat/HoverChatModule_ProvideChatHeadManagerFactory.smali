.class public final Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;
.super Ljava/lang/Object;
.source "HoverChatModule_ProvideChatHeadManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/flipkart/chatheads/ui/ChatHeadManager",
        "<",
        "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
        ">;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

.field private final windowManagerContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p2, "contextProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/Context;>;"
    .local p3, "windowManagerContainerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->module:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    .line 30
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->contextProvider:Ljavax/inject/Provider;

    .line 32
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->windowManagerContainerProvider:Ljavax/inject/Provider;

    .line 34
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "contextProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/Context;>;"
    .local p2, "windowManagerContainerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;-><init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideChatHeadManager(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Landroid/content/Context;Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;)Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "windowManagerContainer"    # Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;",
            "Landroid/content/Context;",
            "Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;",
            ")",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;->provideChatHeadManager(Landroid/content/Context;Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;)Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->module:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->contextProvider:Ljavax/inject/Provider;

    .line 39
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->windowManagerContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;->provideChatHeadManager(Landroid/content/Context;Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;)Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 38
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHeadManager;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideChatHeadManagerFactory;->get()Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-result-object v0

    return-object v0
.end method
