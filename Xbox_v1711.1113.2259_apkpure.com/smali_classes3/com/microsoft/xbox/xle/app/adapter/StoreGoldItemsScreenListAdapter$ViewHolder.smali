.class Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "StoreGoldItemsScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field private goldScreenFullPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private goldScreenGoldWeekendTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private goldScreenReducedPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private releaseTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private rowLayout:Landroid/view/View;

.field private sectionSubheader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private titleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$1;

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->rowLayout:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->titleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->titleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->releaseTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->releaseTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->goldScreenFullPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->goldScreenFullPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->goldScreenReducedPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->goldScreenReducedPrice:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->goldScreenGoldWeekendTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->goldScreenGoldWeekendTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->sectionSubheader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method static synthetic access$802(Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenListAdapter$ViewHolder;->sectionSubheader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object p1
.end method
