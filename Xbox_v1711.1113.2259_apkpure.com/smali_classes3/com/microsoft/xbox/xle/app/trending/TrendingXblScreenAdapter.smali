.class public Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;
.super Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;
.source "TrendingXblScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;
    }
.end annotation


# static fields
.field private static final LOOP_DELAY_MILLIS:I = 0x1388


# instance fields
.field private carouselControl:Landroid/widget/RelativeLayout;

.field private emptyListContainer:Landroid/widget/FrameLayout;

.field private filterSpinner:Landroid/widget/Spinner;

.field private filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;",
            ">;"
        }
    .end annotation
.end field

.field private headerContainer:Landroid/widget/LinearLayout;

.field private isInitialized:Z

.field private listContainer:Landroid/widget/RelativeLayout;

.field private loopRunnable:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;

.field private previousListState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private topicPagerAdapter:Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;

.field private viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

.field private viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V

    .line 41
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->previousListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;)Landroid/support/v4/widget/SwipeRefreshLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;)Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    return-object v0
.end method

.method private ensureInitialized()V
    .locals 4

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->isInitialized:Z

    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->isInitialized:Z

    .line 63
    const v0, 0x7f0e035b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 64
    const v0, 0x7f0e0af3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->emptyListContainer:Landroid/widget/FrameLayout;

    .line 65
    const v0, 0x7f0e0af4

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->carouselControl:Landroid/widget/RelativeLayout;

    .line 66
    const v0, 0x7f0e0af5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 68
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;-><init>(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->topicPagerAdapter:Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;

    .line 69
    const v0, 0x7f0e035c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->listContainer:Landroid/widget/RelativeLayout;

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->topicPagerAdapter:Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 87
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->topicPagerAdapter:Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;-><init>(Landroid/support/v4/view/ViewPager;Landroid/support/v4/view/PagerAdapter;Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->loopRunnable:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;

    .line 88
    const v0, 0x7f0e0af6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 90
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x1090008

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    .line 92
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getFilterItems()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v1, 0x7f03020a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 109
    :cond_0
    return-void
.end method

.method private updateListHeader()V
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->previousListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    .line 145
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_1

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->carouselControl:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeViewFromParent(Landroid/view/View;)V

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->emptyListContainer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->carouselControl:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeViewFromParent(Landroid/view/View;)V

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->listContainer:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 166
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->previousListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 167
    return-void

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->previousListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    .line 155
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->carouselControl:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeViewFromParent(Landroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->headerContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->carouselControl:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeViewFromParent(Landroid/view/View;)V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->headerContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method


# virtual methods
.method public getHeader()Landroid/view/View;
    .locals 3

    .prologue
    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->headerContainer:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    .line 121
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->headerContainer:Landroid/widget/LinearLayout;

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->headerContainer:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 123
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 124
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->headerContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->headerContainer:Landroid/widget/LinearLayout;

    return-object v1
.end method

.method public onStart()V
    .locals 4

    .prologue
    .line 54
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->onStart()V

    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->ensureInitialized()V

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->loopRunnable:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/view/ViewPager;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 57
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->onStop()V

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->loopRunnable:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter$CarouselLoopRunnable;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 115
    return-void
.end method

.method public updateViewOverride()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;->updateViewOverride()V

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->previousListState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    if-eq v0, v2, :cond_0

    .line 135
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->updateListHeader()V

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->carouselControl:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getTopicData()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->topicPagerAdapter:Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicPagerAdapter;->updateView()V

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->isBusy()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 141
    return-void

    :cond_2
    move v0, v1

    .line 138
    goto :goto_0
.end method
