.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$TVStreaming;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TVStreaming"
.end annotation


# static fields
.field public static final Close:Ljava/lang/String; = "Stream Close"

.field public static final ExpandPIP:Ljava/lang/String; = "Stream Expanded PIP"

.field public static final ForceClose:Ljava/lang/String; = "Stream Force Close"

.field public static final FullscreenLive:Ljava/lang/String; = "Stream Fullscreen Live"

.field public static final FullscreenOneGuide:Ljava/lang/String; = "Stream Fullscreen OneGuide"

.field public static final FullscreenPause:Ljava/lang/String; = "Stream Fullscreen Pause"

.field public static final FullscreenPlay:Ljava/lang/String; = "Stream Fullscreen Play"

.field public static final FullscreenScrubber:Ljava/lang/String; = "Stream Fullscreen Scrubber"

.field public static final FullscreenSkipBack:Ljava/lang/String; = "Stream Fullscreen SkipBack"

.field public static final FullscreenSkipForward:Ljava/lang/String; = "Stream Fullscreen SkipForward"

.field public static final Launch:Ljava/lang/String; = "Stream Launch"

.field public static final QualityPicked:Ljava/lang/String; = "Stream Quality Picker"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
