.class public interface abstract Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;
.super Ljava/lang/Object;
.source "EPGProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IFetchListener"
.end annotation


# virtual methods
.method public abstract onFetchComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V
.end method

.method public abstract onFetchListServiceComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V
.end method

.method public abstract onFetchProgress(I)V
.end method

.method public abstract onSendComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;Z)V
.end method

.method public abstract onSetFavoriteError(Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
.end method
