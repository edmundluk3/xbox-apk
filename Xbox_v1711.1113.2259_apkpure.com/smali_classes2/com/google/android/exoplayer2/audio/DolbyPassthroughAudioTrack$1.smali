.class Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;
.super Landroid/os/Handler;
.source "DolbyPassthroughAudioTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;-><init>(IIIIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;


# direct methods
.method constructor <init>(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 90
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 140
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    const-string v3, "unknown message..ignoring!!!"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->w(Ljava/lang/String;)V

    .line 144
    :goto_0
    return-void

    .line 92
    :pswitch_0
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 93
    .local v1, "size":I
    iget v0, p1, Landroid/os/Message;->arg2:I

    .line 94
    .local v0, "bufferIndex":I
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/Logger;->allowVerbose()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "writing to track : size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bufferIndex = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->v(Ljava/lang/String;)V

    .line 98
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    iget-object v3, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v3}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$100(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)[[B

    move-result-object v3

    aget-object v3, v3, v0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4, v1}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$201(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;[BII)I

    .line 99
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer2/util/Logger;->allowVerbose()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 100
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    const-string/jumbo v3, "writing to  track  done"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->v(Ljava/lang/String;)V

    .line 102
    :cond_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$300(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Ljava/util/concurrent/Semaphore;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 106
    .end local v0    # "bufferIndex":I
    .end local v1    # "size":I
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    const-string v3, "pausing track"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 107
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$401(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V

    .line 108
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$500(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Landroid/os/ConditionVariable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_0

    .line 112
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    const-string v3, "playing track"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 113
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$601(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V

    .line 114
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$500(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Landroid/os/ConditionVariable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_0

    .line 118
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    const-string v3, "flushing track"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 119
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$701(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V

    .line 120
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$500(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Landroid/os/ConditionVariable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_0

    .line 124
    :pswitch_4
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    const-string v3, "stopping track"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 125
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$801(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V

    .line 126
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$500(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Landroid/os/ConditionVariable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_0

    .line 130
    :pswitch_5
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    const-string v3, "releasing track"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 131
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$901(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    .line 132
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$000(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Lcom/google/android/exoplayer2/util/Logger;

    move-result-object v2

    const-string v3, "not in stopped state...stopping"

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 133
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$1001(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V

    .line 135
    :cond_2
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$1101(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)V

    .line 136
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack$1;->this$0:Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;

    invoke-static {v2}, Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;->access$500(Lcom/google/android/exoplayer2/audio/DolbyPassthroughAudioTrack;)Landroid/os/ConditionVariable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_0

    .line 90
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
