.class public final Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;
.super Ljava/lang/Object;
.source "MessageModel_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/service/model/MessageModel;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final hoverChatHeadRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "hoverChatHeadRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    .local p2, "hoverChatTelemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    sget-boolean v0, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;->hoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    .line 23
    sget-boolean v0, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    .line 25
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/service/model/MessageModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "hoverChatHeadRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    .local p1, "hoverChatTelemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;>;"
    new-instance v0, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectHoverChatHeadRepository(Lcom/microsoft/xbox/service/model/MessageModel;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/MessageModel;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "hoverChatHeadRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 46
    return-void
.end method

.method public static injectHoverChatTelemetryService(Lcom/microsoft/xbox/service/model/MessageModel;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/MessageModel;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "hoverChatTelemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    .line 52
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/service/model/MessageModel;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/service/model/MessageModel;

    .prologue
    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;->hoverChatHeadRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;->hoverChatTelemetryServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iput-object v0, p1, Lcom/microsoft/xbox/service/model/MessageModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    .line 41
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/MessageModel_MembersInjector;->injectMembers(Lcom/microsoft/xbox/service/model/MessageModel;)V

    return-void
.end method
