.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubAdminSettingsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    }
.end annotation


# static fields
.field private static final CHAT_OPTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;",
            ">;"
        }
    .end annotation
.end field

.field private static final FEED_OPTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;",
            ">;"
        }
    .end annotation
.end field

.field private static final INVITE_OPTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;",
            ">;"
        }
    .end annotation
.end field

.field private static final LFG_OPTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private broadcastMatureContentEnabled:Z

.field private broadcastWatchClubTitlesOnly:Z

.field private chatOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

.field private feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

.field private getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;

.field private getModeratorsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private inviteOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

.field private isDeletingClub:Z

.field private isLoadingClubAccountDetails:Z

.field private isLoadingModerators:Z

.field private isTransferringClub:Z

.field private lfgEnabled:Z

.field private lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

.field private membershipEnabled:Z

.field private mostRecentAccountDetails:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 94
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->TAG:Ljava/lang/String;

    .line 96
    new-array v0, v4, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ModeratorsAndOwner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->INVITE_OPTIONS:Ljava/util/List;

    .line 97
    new-array v0, v5, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Moderators:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Owner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->FEED_OPTIONS:Ljava/util/List;

    .line 98
    new-array v0, v5, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Moderators:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Owner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->LFG_OPTIONS:Ljava/util/List;

    .line 99
    new-array v0, v4, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ModeratorsAndOwner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->CHAT_OPTIONS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 123
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 124
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminSettingsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 125
    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLoadingClubAccountDetails:Z

    return p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->onClubAccountDetailsLoaded(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->onModeratorsLoaded(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->onModeratorSelected(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    return-void
.end method

.method static synthetic access$lambda$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->onDeleteComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$lambda$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->onTransferComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private cancelGetClubAccountDetailsTask()V
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->cancel()V

    .line 588
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;

    .line 590
    :cond_0
    return-void
.end method

.method private cancelGetModeratorsTask()V
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getModeratorsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getModeratorsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 581
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getModeratorsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 583
    :cond_0
    return-void
.end method

.method private cancelTasks()V
    .locals 0

    .prologue
    .line 574
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->cancelGetModeratorsTask()V

    .line 575
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->cancelGetClubAccountDetailsTask()V

    .line 576
    return-void
.end method

.method private getClubSettings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 547
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getModeratorXuids()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 495
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 497
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 498
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->moderator()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 499
    .local v3, "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 500
    .local v5, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v1

    .line 502
    .local v1, "meXuid":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 503
    .local v2, "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 505
    .local v4, "xuid":Ljava/lang/String;
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 506
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 513
    .end local v1    # "meXuid":Ljava/lang/String;
    .end local v2    # "moderator":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .end local v3    # "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v4    # "xuid":Ljava/lang/String;
    .end local v5    # "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    :cond_2
    return-object v5
.end method

.method private getOptionTitles(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    .local p1, "options":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 339
    .local v1, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 340
    .local v0, "settingOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 343
    .end local v0    # "settingOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    :cond_0
    return-object v1
.end method

.method private isTitleClub()Z
    .locals 3

    .prologue
    .line 541
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 542
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->Title:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$chatOptionChanged$8(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
    .param p1, "oldOption"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .prologue
    .line 436
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 438
    monitor-enter p0

    .line 439
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->chatOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 440
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 442
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 443
    return-void

    .line 440
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic lambda$feedOptionChanged$6(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
    .param p1, "oldOption"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .prologue
    .line 384
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 386
    monitor-enter p0

    .line 387
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 388
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 391
    return-void

    .line 388
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic lambda$inviteOptionChanged$5(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
    .param p1, "oldOption"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .prologue
    .line 358
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 360
    monitor-enter p0

    .line 361
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->inviteOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 362
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 364
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 365
    return-void

    .line 362
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic lambda$lfgEnableJoin$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    .prologue
    .line 244
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 246
    monitor-enter p0

    .line 247
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgEnabled:Z

    .line 248
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 251
    return-void

    .line 247
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 248
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic lambda$lfgOptionChanged$7(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
    .param p1, "oldOption"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .prologue
    .line 410
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 412
    monitor-enter p0

    .line 413
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 414
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 417
    return-void

    .line 414
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic lambda$membershipEnableJoin$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    .prologue
    .line 219
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 221
    monitor-enter p0

    .line 222
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->membershipEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->membershipEnabled:Z

    .line 223
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 226
    return-void

    .line 222
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic lambda$onModeratorSelected$9(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
    .param p1, "moderator"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 697
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isTransferringClub:Z

    .line 698
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->transferOwnership(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 699
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 700
    return-void
.end method

.method static synthetic lambda$setMatureContentEnabled$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    .prologue
    .line 271
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 273
    monitor-enter p0

    .line 274
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastMatureContentEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastMatureContentEnabled:Z

    .line 275
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 278
    return-void

    .line 274
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 275
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic lambda$setToDefault$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    .prologue
    .line 205
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 206
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 207
    return-void
.end method

.method static synthetic lambda$setWatchClubTitlesOnly$4(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    .prologue
    .line 297
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 299
    monitor-enter p0

    .line 300
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastWatchClubTitlesOnly:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastWatchClubTitlesOnly:Z

    .line 301
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 304
    return-void

    .line 300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 301
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic lambda$showDeleteDialogWithBody$10(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;

    .prologue
    .line 751
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsDeleteClubConfirm(J)V

    .line 752
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isDeletingClub:Z

    .line 753
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->deleteClub(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 754
    return-void
.end method

.method private navigateToClubHomeScreen()V
    .locals 3

    .prologue
    .line 794
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;

    const-class v2, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreen;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 795
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->goBack()V

    .line 796
    return-void
.end method

.method private navigateToScreenBeforeClubPivot()V
    .locals 4

    .prologue
    .line 784
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->CountPopsToScreen(Ljava/lang/Class;)I

    move-result v2

    add-int/lit8 v1, v2, 0x1

    .line 787
    .local v1, "popCount":I
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreens(I)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 791
    :goto_0
    return-void

    .line 788
    :catch_0
    move-exception v0

    .line 789
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Failed to navigate to the screen before the club pivot screen"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onClubAccountDetailsLoaded(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .prologue
    .line 731
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLoadingClubAccountDetails:Z

    .line 732
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->mostRecentAccountDetails:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    .line 734
    if-nez p1, :cond_0

    .line 735
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 742
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 743
    return-void

    .line 736
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->canDeleteImmediately()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 737
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0701d9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showDeleteDialogWithBody(Ljava/lang/String;)V

    goto :goto_0

    .line 739
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0701db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showDeleteDialogWithBody(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onDeleteComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 760
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isDeletingClub:Z

    .line 762
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 781
    :goto_0
    :pswitch_0
    return-void

    .line 765
    :pswitch_1
    const v0, 0x7f0701de

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 767
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->mostRecentAccountDetails:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->canDeleteImmediately()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 768
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->navigateToScreenBeforeClubPivot()V

    goto :goto_0

    .line 770
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->navigateToClubHomeScreen()V

    goto :goto_0

    .line 777
    :pswitch_2
    const v0, 0x7f0701dd

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 778
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 762
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private onModeratorSelected(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 7
    .param p1, "moderator"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 691
    if-eqz p1, :cond_0

    .line 692
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0702dd

    .line 693
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702db

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 694
    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0702dc

    .line 695
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070736

    .line 701
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 692
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 704
    :cond_0
    return-void
.end method

.method private onModeratorsLoaded(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 675
    .local p1, "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLoadingModerators:Z

    .line 676
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 678
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 679
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 688
    :goto_0
    return-void

    .line 682
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->setClubId(J)V

    .line 685
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    .line 686
    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showClubModeratorPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    goto :goto_0
.end method

.method private onTransferComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 7
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 707
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isTransferringClub:Z

    .line 709
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {p1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 728
    :goto_0
    :pswitch_0
    return-void

    .line 712
    :pswitch_1
    const v0, 0x7f0702e1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 713
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->goBack()V

    goto :goto_0

    .line 718
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0702df

    .line 719
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702de

    .line 720
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0707c7

    .line 721
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    const-string v5, ""

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 718
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 725
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 709
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private settingsAvailable()Z
    .locals 1

    .prologue
    .line 551
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getClubSettings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showDeleteDialogWithBody(Ljava/lang/String;)V
    .locals 7
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 746
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0701dc

    .line 747
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0701da

    .line 749
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07060d

    .line 755
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    move-object v2, p1

    .line 746
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 757
    return-void
.end method

.method private declared-synchronized updateDataFromClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 2
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 635
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->membershipEnabled:Z

    .line 636
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgEnabled:Z

    .line 638
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastMatureContentEnabled:Z

    .line 639
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastWatchClubTitlesOnly:Z

    .line 641
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->inviteOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 643
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubHubDataTypes$ClubHubSettingsRole:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 653
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Owner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 657
    :goto_2
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubHubDataTypes$ClubHubSettingsRole:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 667
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Owner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 671
    :goto_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    :goto_4
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->chatOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 672
    monitor-exit p0

    return-void

    .line 636
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 641
    :cond_1
    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ModeratorsAndOwner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    goto :goto_1

    .line 645
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 635
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 649
    :pswitch_1
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Moderators:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    goto :goto_2

    .line 659
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    goto :goto_3

    .line 663
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Moderators:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    goto :goto_3

    .line 671
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ModeratorsAndOwner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 643
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 657
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public canDeleteClub()Z
    .locals 1

    .prologue
    .line 537
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isTitleClub()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canTransferClub()Z
    .locals 1

    .prologue
    .line 533
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isTitleClub()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized chatOptionChanged(I)Z
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 426
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->CHAT_OPTIONS:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    int-to-long v4, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 428
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->chatOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->CHAT_OPTIONS:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsChatPostFilter(JI)V

    .line 430
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->chatOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 431
    .local v6, "oldOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->CHAT_OPTIONS:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->chatOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 433
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->chatOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 434
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->getMinimumEquivalentRole()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-result-object v1

    invoke-static {p0, v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    move-result-object v2

    .line 433
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setWhoCanChat(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    const/4 v0, 0x1

    .line 448
    .end local v6    # "oldOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 426
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public deleteClub()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 518
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 520
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    .line 521
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsDeleteClub(J)V

    .line 522
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLoadingClubAccountDetails:Z

    .line 524
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->cancelGetClubAccountDetailsTask()V

    .line 525
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;

    .line 526
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getClubAccountDetailsTask:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$GetClubAccountDetailsTask;->load(Z)V

    .line 528
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 530
    :cond_0
    return-void
.end method

.method public enableDelete()Z
    .locals 1

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLoadingClubAccountDetails:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isDeletingClub:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enableTransfer()Z
    .locals 1

    .prologue
    .line 468
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLoadingModerators:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isTransferringClub:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized feedOptionChanged(I)Z
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 374
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->FEED_OPTIONS:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    int-to-long v4, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 376
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->FEED_OPTIONS:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsActivityFeedPostFilter(JI)V

    .line 378
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 379
    .local v6, "oldOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->FEED_OPTIONS:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 381
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 382
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->getMinimumEquivalentRole()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-result-object v1

    invoke-static {p0, v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    move-result-object v2

    .line 381
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setWhoCanPostToFeed(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 393
    const/4 v0, 0x1

    .line 396
    .end local v6    # "oldOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 374
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getChatOptions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->CHAT_OPTIONS:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getOptionTitles(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFeedOptions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->FEED_OPTIONS:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getOptionTitles(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getInviteOptions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 317
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->INVITE_OPTIONS:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getOptionTitles(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getLfgJoinDescription()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgEnabled:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0702be

    .line 194
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 198
    :goto_0
    monitor-exit p0

    return-object v0

    .line 194
    :cond_0
    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0702bc

    .line 195
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 198
    :cond_1
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getLfgOptions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 327
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->LFG_OPTIONS:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getOptionTitles(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPrivacyDescription()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 171
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 173
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    .line 174
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 182
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown club type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 187
    :cond_0
    const-string v1, ""

    :goto_0
    return-object v1

    .line 176
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702c4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 178
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 180
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0702ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 174
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getPrivacyHeader()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 129
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 131
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    .line 132
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 140
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown club type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 145
    :cond_0
    const-string v1, ""

    :goto_0
    return-object v1

    .line 134
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07026b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 136
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070267

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 138
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0701f1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 132
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getPrivacyHeaderIcon()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 150
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 152
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_0

    .line 153
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown club type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 166
    :cond_0
    const-string v1, ""

    :goto_0
    return-object v1

    .line 155
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070f64

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 157
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070f79

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 159
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070efe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 153
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized getSelectedChatOption()I
    .locals 2

    .prologue
    .line 464
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->CHAT_OPTIONS:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->chatOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSelectedFeedOption()I
    .locals 2

    .prologue
    .line 456
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->FEED_OPTIONS:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->feedOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSelectedInviteOption()I
    .locals 2

    .prologue
    .line 452
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->INVITE_OPTIONS:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->inviteOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSelectedLfgOption()I
    .locals 2

    .prologue
    .line 460
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->LFG_OPTIONS:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized inviteOptionChanged(I)Z
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 347
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->INVITE_OPTIONS:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    int-to-long v4, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->inviteOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->INVITE_OPTIONS:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsInviteFilter(JI)V

    .line 352
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->inviteOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 353
    .local v6, "oldOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->INVITE_OPTIONS:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->inviteOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 355
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->inviteOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 356
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->getMinimumEquivalentRole()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-result-object v1

    invoke-static {p0, v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    move-result-object v2

    .line 355
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setWhoCanInvite(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    const/4 v0, 0x1

    .line 370
    .end local v6    # "oldOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 347
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 600
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLoadingModerators:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isTransferringClub:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLoadingClubAccountDetails:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isDeletingClub:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized isLfgEnabled()Z
    .locals 1

    .prologue
    .line 260
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isMatureContentEnabled()Z
    .locals 1

    .prologue
    .line 286
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastMatureContentEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isMembershipEnabled()Z
    .locals 1

    .prologue
    .line 234
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->membershipEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isWatchClubTitlesOnly()Z
    .locals 1

    .prologue
    .line 312
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastWatchClubTitlesOnly:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized lfgEnableJoin(Z)Z
    .locals 3
    .param p1, "checked"    # Z

    .prologue
    .line 238
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgEnabled:Z

    if-eq v0, p1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgEnabled:Z

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgEnabled:Z

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->enableLfg(ZLcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    const/4 v0, 0x1

    .line 256
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized lfgOptionChanged(I)Z
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 400
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->LFG_OPTIONS:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    int-to-long v4, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 402
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->LFG_OPTIONS:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsPlayPostFilter(JI)V

    .line 404
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 405
    .local v6, "oldOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->LFG_OPTIONS:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 407
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->lfgOption:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 408
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->getMinimumEquivalentRole()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    move-result-object v1

    invoke-static {p0, v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    move-result-object v2

    .line 407
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setWhoCanCreateLfg(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    const/4 v0, 0x1

    .line 422
    .end local v6    # "oldOption":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected loadOverride(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 594
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 595
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 596
    return-void
.end method

.method public membershipEnableJoin(Z)Z
    .locals 3
    .param p1, "checked"    # Z

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->membershipEnabled:Z

    if-eq v0, p1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsMembershipToggleJoin(JZ)V

    .line 214
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->membershipEnabled:Z

    .line 216
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->membershipEnabled:Z

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->enableJoin(ZLcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 228
    const/4 v0, 0x1

    .line 230
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 609
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 631
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    .line 632
    return-void

    .line 613
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 615
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 616
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 617
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 618
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 619
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateDataFromClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0

    .line 626
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 627
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Club model changed to invalid state."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 609
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 556
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminSettingsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 557
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 562
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 563
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 565
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 569
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStopOverride()V

    .line 570
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->cancelTasks()V

    .line 571
    return-void
.end method

.method public declared-synchronized setMatureContentEnabled(Z)Z
    .locals 3
    .param p1, "checked"    # Z

    .prologue
    .line 264
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastMatureContentEnabled:Z

    if-eq v0, p1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsWatchMatureContent(JZ)V

    .line 266
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastMatureContentEnabled:Z

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastMatureContentEnabled:Z

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->enableMatureContentEnabled(ZLcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    const/4 v0, 0x1

    .line 282
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setToDefault()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsResetDefault(J)V

    .line 203
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setToDefaultSettings(Lcom/microsoft/xbox/service/model/clubs/ClubModel$SettingsChangeFailedCallback;)V

    .line 209
    :cond_0
    return-void
.end method

.method public declared-synchronized setWatchClubTitlesOnly(Z)Z
    .locals 3
    .param p1, "checked"    # Z

    .prologue
    .line 290
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastWatchClubTitlesOnly:Z

    if-eq v0, p1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->settingsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsWatchClubTitlesOnly(JZ)V

    .line 292
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastWatchClubTitlesOnly:Z

    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->broadcastWatchClubTitlesOnly:Z

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->enableWatchClubTitlesOnly(ZLcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    const/4 v0, 0x1

    .line 308
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public transferClub()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 476
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->cancelTasks()V

    .line 478
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSettingsTransferOwnership(J)V

    .line 479
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getModeratorXuids()Ljava/util/List;

    move-result-object v0

    .line 481
    .local v0, "moderators":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 482
    const v1, 0x7f0702e0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->showError(I)V

    .line 492
    :goto_0
    return-void

    .line 484
    :cond_0
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->isLoadingModerators:Z

    .line 486
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->cancelGetModeratorsTask()V

    .line 487
    new-instance v1, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getModeratorXuids()Ljava/util/List;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getModeratorsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 488
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->getModeratorsTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    .line 490
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;->updateAdapter()V

    goto :goto_0
.end method
