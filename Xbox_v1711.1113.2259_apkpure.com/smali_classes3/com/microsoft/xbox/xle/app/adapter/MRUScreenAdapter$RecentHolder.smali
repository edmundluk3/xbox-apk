.class Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;
.super Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecentHolder"
.end annotation


# instance fields
.field private final image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 3
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "parentWidth"    # I
    .param p3, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p4, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    const v2, 0x7f0b0021

    .line 620
    invoke-direct {p0, p1, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$BaseHolder;-><init>(Landroid/view/View;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    .line 621
    const v0, 0x7f0e00df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 622
    const v0, 0x7f0e07cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 623
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0, p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->setRowHeight(II)V

    .line 624
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p0, v0, p2, v1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->setImageWidth(Landroid/widget/ImageView;II)V

    .line 625
    return-void
.end method


# virtual methods
.method public bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 4
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    .line 629
    if-eqz p1, :cond_1

    .line 630
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->bindNPI(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 632
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v1

    .line 633
    .local v1, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 634
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 635
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v2, v1, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f070fca

    :goto_0
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 636
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackground(Landroid/view/View;)V

    .line 641
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, v1, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->bindRowTitle(Landroid/widget/TextView;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 643
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v1    # "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    :cond_1
    return-void

    .line 635
    .restart local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v1    # "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    :cond_2
    const v2, 0x7f070fdb

    goto :goto_0

    .line 638
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v3, v1, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setImageUrl(Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Ljava/lang/String;)V

    .line 639
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackgroundOptional(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/view/View;)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->getAdapterPosition()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$RecentHolder;->onRecentClick(I)V

    .line 648
    return-void
.end method
