.class Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1$2;
.super Ljava/util/TimerTask;
.source "GameStreamingNetworkTestScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1$2;->this$1:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 115
    const-string v0, "GameStreamingNetworkTestScreen"

    const-string v1, "Network Test Timeout!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1$2;->this$1:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->access$100(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1$2;->this$1:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->access$400(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->stopGameStream()Z

    .line 118
    const-string v0, "Network Test"

    const-string v1, "timeout"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    return-void
.end method
