.class public Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;
.super Landroid/widget/LinearLayout;
.source "DetailsMoreOrLessView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static DEFAULT_ANIMATION_DURATION:I

.field private static LESS:Ljava/lang/String;

.field private static MORE:Ljava/lang/String;


# instance fields
.field private alwaysShowAllText:Z

.field private descriptionViewEllipsizeHeight:I

.field private descriptionViewExpandHeight:I

.field private detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

.field private ellipaisAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

.field private expandAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

.field private moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const/16 v0, 0x1f4

    sput v0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->DEFAULT_ANIMATION_DURATION:I

    .line 39
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070427

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->MORE:Ljava/lang/String;

    .line 40
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070426

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->LESS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->alwaysShowAllText:Z

    .line 63
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 64
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f0300da

    invoke-virtual {v2, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 66
    const v3, 0x7f0e04fc

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    .line 67
    const v3, 0x7f0e04fd

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 69
    if-eqz p2, :cond_1

    .line 70
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->DetailsMoreOrLessView:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 71
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->alwaysShowAllText:Z

    .line 72
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 73
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0c0141

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 74
    .local v1, "color":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setTextColor(I)V

    .line 75
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 77
    .end local v1    # "color":I
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 80
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->alwaysShowAllText:Z

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setAlwaysShowText(Z)V

    .line 82
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->alwaysShowAllText:Z

    if-nez v3, :cond_2

    .line 83
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    new-instance v4, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$1;-><init>(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setEllipsizeListener(Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView$EllipsizeListener;)V

    .line 100
    invoke-virtual {p0, p0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v3, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    :goto_0
    return-void

    .line 103
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->MORE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    return-object v0
.end method

.method private handleMoreAndLessClickEvent()V
    .locals 4

    .prologue
    .line 126
    sget-object v0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->LESS:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->MORE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->ellipaisAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->descriptionViewEllipsizeHeight:I

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->ellipaisAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->ellipaisAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->setTargetView(Landroid/view/View;)V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->ellipaisAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    new-instance v1, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$2;-><init>(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->ellipaisAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    sget v1, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->DEFAULT_ANIMATION_DURATION:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->setDuration(J)V

    .line 151
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->ellipaisAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 182
    :goto_1
    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->ellipaisAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->reset()V

    goto :goto_0

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->descriptionViewEllipsizeHeight:I

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getTextExpandedHeight()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->descriptionViewExpandHeight:I

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->LESS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->expandAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    if-nez v0, :cond_2

    .line 158
    new-instance v0, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->descriptionViewEllipsizeHeight:I

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->descriptionViewExpandHeight:I

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->expandAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->expandAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->setTargetView(Landroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->expandAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    sget v1, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->DEFAULT_ANIMATION_DURATION:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->setDuration(J)V

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->expandAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    new-instance v1, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView$3;-><init>(Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 180
    :goto_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->expandAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->expandAnimation:Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/anim/HeightAnimation;->reset()V

    goto :goto_2
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->moreOrLessButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->handleMoreAndLessClickEvent()V

    .line 58
    :cond_0
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->alwaysShowAllText:Z

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->getCollapsedLineCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setLines(I)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/DetailsMoreOrLessView;->detailsDescription:Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/CustomTypefaceEllipsizeTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
