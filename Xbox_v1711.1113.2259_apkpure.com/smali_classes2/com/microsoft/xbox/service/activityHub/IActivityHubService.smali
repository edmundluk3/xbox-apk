.class public interface abstract Lcom/microsoft/xbox/service/activityHub/IActivityHubService;
.super Ljava/lang/Object;
.source "IActivityHubService.java"


# annotations
.annotation build Landroid/support/annotation/WorkerThread;
.end annotation


# virtual methods
.method public abstract getTrendingCategoryItems(ILjava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getTrendingTags(ILjava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;
    .param p1    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getTrendingTitleItems(ILjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getTrendingTopicItems(ILjava/lang/String;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getTrendingUserItems(ILjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
