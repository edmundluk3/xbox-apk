.class final synthetic Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$25;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

.field private final arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$25;->arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$25;->arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;)Ljava/util/concurrent/Callable;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$25;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$25;-><init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;)V

    return-object v0
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$25;->arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$25;->arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->lambda$null$24(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$AutoSystemUpdatesChangedAction;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    move-result-object v0

    return-object v0
.end method
