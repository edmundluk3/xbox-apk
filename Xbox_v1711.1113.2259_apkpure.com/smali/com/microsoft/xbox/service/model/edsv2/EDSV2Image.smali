.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
.super Ljava/lang/Object;
.source "EDSV2Image.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$PostProcessableGson;


# static fields
.field public static final BOX_ART_PURPOSE:Ljava/lang/String; = "BoxArt"


# instance fields
.field private backgroundColor:Ljava/lang/String;

.field private height:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Height"
    .end annotation
.end field

.field private imagePositionInfo:Ljava/lang/String;

.field private purpose:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Purpose"
    .end annotation
.end field

.field public purposes:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Purposes"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private url:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ResizeUrl"
    .end annotation
.end field

.field private width:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Width"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->url:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;)V
    .locals 1
    .param p1, "bigCatImage"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->url:Ljava/lang/String;

    .line 37
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 39
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;->backgroundColor:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->backgroundColor:Ljava/lang/String;

    .line 40
    iget v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;->height:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->height:I

    .line 41
    iget v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;->width:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->width:I

    .line 42
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;->imagePositionInfo:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->imagePositionInfo:Ljava/lang/String;

    .line 43
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;->imagePurpose:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->purpose:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;->getValidImageUriString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->url:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method public getBackgroundColor()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->backgroundColor:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->height:I

    return v0
.end method

.method public getImagePositionInfo()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->imagePositionInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getPurpose()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->purpose:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->url:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->width:I

    return v0
.end method

.method public postProcess()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->purpose:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 97
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->purpose:Ljava/lang/String;

    .line 99
    :cond_0
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "h"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->height:I

    .line 53
    return-void
.end method

.method public setPurpose(Ljava/lang/String;)V
    .locals 1
    .param p1, "purpose"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->purpose:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->purpose:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->purpose:Ljava/lang/String;

    .line 81
    :cond_0
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->url:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "w"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->width:I

    .line 61
    return-void
.end method
