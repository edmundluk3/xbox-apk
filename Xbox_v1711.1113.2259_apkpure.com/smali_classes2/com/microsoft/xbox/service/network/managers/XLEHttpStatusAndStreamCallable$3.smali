.class final Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;
.super Ljava/lang/Object;
.source "XLEHttpStatusAndStreamCallable.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newPutInstance(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$headers:Ljava/util/List;

.field final synthetic val$postBody:Ljava/lang/String;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;->val$url:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;->val$headers:Ljava/util/List;

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;->val$postBody:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;->val$url:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;->val$headers:Ljava/util/List;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;->val$postBody:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->putStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable$3;->call()Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method
