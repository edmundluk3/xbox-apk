.class public final enum Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;
.super Ljava/lang/Enum;
.source "HoverChatBadger.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

.field private static final BADGE_TO_BOUNDS_RATIO:F = 0.25f

.field public static final enum INSTANCE:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

.field private static final TEXT_TO_BADGE_RATIO:F = 0.7f


# instance fields
.field private final badgeBackgroundPaint:Landroid/graphics/Paint;

.field private final badgeBackgroundRect:Landroid/graphics/RectF;

.field private final badgeIconPaint:Landroid/graphics/Paint;

.field private final badgeIconRect:Landroid/graphics/RectF;

.field private final icon:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->INSTANCE:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    sget-object v1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->INSTANCE:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->$VALUES:[Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundPaint:Landroid/graphics/Paint;

    .line 32
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundPaint:Landroid/graphics/Paint;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMePreferredColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "fonts/SegXboxSymbol.ttf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 36
    .local v0, "iconTypeface":Landroid/graphics/Typeface;
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    .line 37
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 38
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 39
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 41
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    .line 42
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    .line 44
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070eec

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->icon:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->$VALUES:[Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    return-object v0
.end method


# virtual methods
.method public addBadgeToCanvas(Landroid/graphics/Canvas;Landroid/graphics/Rect;Z)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "totalBounds"    # Landroid/graphics/Rect;
    .param p3, "isRight"    # Z

    .prologue
    const/4 v3, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 52
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3e800000    # 0.25f

    mul-float v0, v1, v2

    .line 53
    .local v0, "badgeSize":F
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    const v2, 0x3f333333    # 0.7f

    mul-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 55
    if-eqz p3, :cond_2

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    iget v2, p2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    sub-float/2addr v2, v0

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    iput v3, v1, Landroid/graphics/RectF;->top:F

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    iget v2, p2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 67
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 70
    if-nez p3, :cond_0

    .line 71
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 72
    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    div-float v3, v0, v6

    div-float v4, v0, v6

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->icon:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->icon:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v2

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->descent()F

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    sub-float/2addr v3, v4

    div-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v4

    div-float/2addr v3, v6

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->icon:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeIconPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 84
    if-nez p3, :cond_1

    .line 85
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 87
    :cond_1
    return-void

    .line 61
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    iput v3, v1, Landroid/graphics/RectF;->left:F

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    iput v3, v1, Landroid/graphics/RectF;->top:F

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->badgeBackgroundRect:Landroid/graphics/RectF;

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0
.end method
