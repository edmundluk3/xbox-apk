.class public abstract Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/PopupScreenAdapterBase;
.source "HomeScreenPopupScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/xle/viewmodel/PopupScreenAdapterBase;"
    }
.end annotation


# instance fields
.field private final closeButton:Landroid/view/View;

.field private final cmdClickListener:Landroid/view/View$OnClickListener;

.field private commands:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;",
            ">;"
        }
    .end annotation
.end field

.field protected final inflater:Landroid/view/LayoutInflater;

.field private final popupContainer:Landroid/widget/LinearLayout;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter<TT;>;"
    .local p1, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PopupScreenAdapterBase;-><init>()V

    .line 272
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$6;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$6;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->cmdClickListener:Landroid/view/View$OnClickListener;

    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;

    .line 41
    const v0, 0x7f0e06e2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->popupContainer:Landroid/widget/LinearLayout;

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->popupContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 43
    const v0, 0x7f0e06d9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->closeButton:Landroid/view/View;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->closeButton:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->closeButton:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$1;

    .prologue
    .line 31
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter<TT;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;

    return-object v0
.end method

.method static synthetic access$200(Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->isDisplayProvider(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Landroid/widget/ImageView;)V
    .locals 0
    .param p0, "x0"    # Landroid/widget/ImageView;

    .prologue
    .line 31
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->updateImageBackground(Landroid/widget/ImageView;)V

    return-void
.end method

.method static synthetic access$400(Landroid/content/res/Resources;Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/res/Resources;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .prologue
    .line 31
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->getHasString(Landroid/content/res/Resources;Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private addNewCommands()V
    .locals 9

    .prologue
    .line 254
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter<TT;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->commands:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    .line 255
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->commands:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 256
    .local v0, "cmd":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->inflater:Landroid/view/LayoutInflater;

    const v6, 0x7f030142

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->popupContainer:Landroid/widget/LinearLayout;

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 257
    .local v3, "v":Landroid/view/View;
    iget v5, v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->imageResId:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 258
    const v5, 0x7f0e06e3

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 259
    .local v1, "cmdButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    iget v5, v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->imageResId:I

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(I)V

    .line 261
    .end local v1    # "cmdButton":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    :cond_0
    const v5, 0x7f0e06e4

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 262
    .local v2, "cmdText":Landroid/widget/TextView;
    iget v5, v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 263
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->cmdClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 265
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->popupContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 267
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->getContentDescriptionId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 270
    .end local v0    # "cmd":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;
    .end local v2    # "cmdText":Landroid/widget/TextView;
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public static createContentItemScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;)",
            "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    .local p0, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<Lcom/microsoft/xbox/service/model/pins/LaunchableItem;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V

    return-object v0
.end method

.method public static createPinScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;)",
            "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V

    return-object v0
.end method

.method public static createPromoScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
    .locals 1
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo;",
            ")",
            "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelPromo$Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$5;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V

    return-object v0
.end method

.method public static createRecentScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;)",
            "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$3;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V

    return-object v0
.end method

.method private static getHasString(Landroid/content/res/Resources;Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;)Ljava/lang/String;
    .locals 2
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "state"    # Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    .prologue
    .line 228
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$7;->$SwitchMap$com$microsoft$xbox$service$model$pins$ContentUtil$HasState:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 236
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 230
    :pswitch_0
    const v0, 0x7f07069a

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 232
    :pswitch_1
    const v0, 0x7f07068e

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static isDisplayProvider(Ljava/lang/String;)Z
    .locals 1
    .param p0, "contentType"    # Ljava/lang/String;

    .prologue
    .line 224
    const-string v0, "DApp"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DGame"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static updateImageBackground(Landroid/widget/ImageView;)V
    .locals 2
    .param p0, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 216
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 217
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 222
    :goto_0
    return-void

    .line 220
    :cond_0
    sget v1, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract createAndBindHeader(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 242
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->getHeaderData()Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->createAndBindHeader(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;)V

    .line 243
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->getCommands()Ljava/util/ArrayList;

    move-result-object v0

    .line 244
    .local v0, "modelCommands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;>;"
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->commands:Ljava/util/ArrayList;

    if-eq v0, v1, :cond_0

    .line 245
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->commands:Ljava/util/ArrayList;

    .line 246
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->popupContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 247
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->addNewCommands()V

    .line 249
    :cond_0
    return-void
.end method
