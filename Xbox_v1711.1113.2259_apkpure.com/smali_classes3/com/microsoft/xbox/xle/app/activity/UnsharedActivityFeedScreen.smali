.class public Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "UnsharedActivityFeedScreen.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 11
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;->onCreateContentView()V

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 14
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 18
    const v0, 0x7f03002e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;->setContentView(I)V

    .line 19
    return-void
.end method
