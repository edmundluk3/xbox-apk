.class public Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
.source "PartyDetailsViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/party/PartyDetailsView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;
    }
.end annotation


# instance fields
.field private listAdapter:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

.field memberList:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0852
    .end annotation
.end field

.field presenter:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private streamsBound:Z

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field

.field private viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>()V

    .line 47
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 50
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->streamsBound:Z

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 50
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->streamsBound:Z

    .line 59
    return-void
.end method

.method private declared-synchronized bindMemberChangeStream(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->memberStream()Lio/reactivex/Observable;

    move-result-object v0

    .line 102
    .local v0, "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->streamsBound:Z

    if-nez v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 104
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 105
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 103
    invoke-virtual {v1, v2}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 120
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->streamsBound:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    :cond_0
    monitor-exit p0

    return-void

    .line 101
    .end local v0    # "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method static synthetic lambda$bindMemberChangeStream$0(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;
    .param p1, "member"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v1

    .line 107
    .local v1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;>;"
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 108
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    if-eqz v3, :cond_1

    .line 109
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    .line 110
    .local v2, "memberItem":Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->partyMember()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 111
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-virtual {v3, v0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->notifyItemChanged(ILjava/lang/Object;)V

    .line 107
    .end local v2    # "memberItem":Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected item type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_1

    .line 117
    :cond_2
    return-void
.end method

.method private mapViewStateToPartyDetailsListItems(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)Ljava/util/List;
    .locals 14
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getRoster()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    .line 158
    .local v4, "partyMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v8

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->meIsHost(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v1, 0x1

    .line 159
    .local v1, "meIsHost":Z
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    .line 160
    .local v6, "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    :goto_2
    if-eqz v4, :cond_5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 161
    .local v3, "partyMemberCount":I
    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    add-int/lit8 v8, v3, 0x1

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 163
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;>;"
    sget-object v8, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;->INSTANCE:Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsHeaderItem;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    if-eqz v4, :cond_7

    .line 165
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 166
    .local v2, "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    const/4 v5, 0x0

    .line 167
    .local v5, "partyUser":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    invoke-virtual {v6}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 168
    .local v7, "userSummary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    invoke-virtual {v7}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v10

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-nez v10, :cond_0

    .line 169
    move-object v5, v7

    .line 174
    .end local v7    # "userSummary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :cond_1
    if-eqz v1, :cond_6

    invoke-virtual {v5}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v10

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    cmp-long v8, v10, v12

    if-eqz v8, :cond_6

    const/4 v8, 0x1

    :goto_5
    invoke-static {v2, v5, v8}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;Z)Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 157
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;>;"
    .end local v1    # "meIsHost":Z
    .end local v2    # "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .end local v3    # "partyMemberCount":I
    .end local v4    # "partyMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    .end local v5    # "partyUser":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .end local v6    # "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 158
    .restart local v4    # "partyMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 159
    .restart local v1    # "meIsHost":Z
    :cond_4
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    goto :goto_2

    .line 160
    .restart local v6    # "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    :cond_5
    const/4 v3, 0x0

    goto :goto_3

    .line 174
    .restart local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;>;"
    .restart local v2    # "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .restart local v3    # "partyMemberCount":I
    .restart local v5    # "partyUser":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :cond_6
    const/4 v8, 0x0

    goto :goto_5

    .line 178
    .end local v2    # "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .end local v5    # "partyUser":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :cond_7
    return-object v0
.end method

.method private renderMemberList(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    .prologue
    .line 125
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v2

    .line 126
    .local v2, "oldList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->mapViewStateToPartyDetailsListItems(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)Ljava/util/List;

    move-result-object v1

    .line 128
    .local v1, "newList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyDetailsListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->clear()V

    .line 129
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 131
    new-instance v3, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;

    invoke-direct {v3, p0, v2, v1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$PartyDetailItemDiffCallback;-><init>(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;Ljava/util/List;Ljava/util/List;)V

    invoke-static {v3}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v0

    .line 132
    .local v0, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 133
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    const-string v0, "Party - Party Details View"

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 153
    const v0, 0x7f0301a2

    return v0
.end method

.method protected getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->presenter:Lcom/microsoft/xbox/presentation/party/PartyDetailsPresenter;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreate()V

    .line 64
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;)V

    .line 65
    return-void
.end method

.method public onCreateContentView()V
    .locals 4

    .prologue
    .line 69
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreateContentView()V

    .line 71
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    .line 74
    .local v0, "viewIntentRelay":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;>;"
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl$$Lambda$1;->lambdaFactory$(Lcom/jakewharton/rxrelay2/PublishRelay;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;Lio/reactivex/Observable;)V

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->memberList:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/party/PartyDetailsListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 77
    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 78
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onDestroy()V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 84
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V
    .locals 2
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    .prologue
    .line 88
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 98
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->viewStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 95
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->bindMemberChangeStream(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V

    .line 96
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->renderMemberList(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V

    goto :goto_0
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    check-cast p1, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->render(Lcom/microsoft/xbox/presentation/party/PartyDetailsViewState;)V

    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<+",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
