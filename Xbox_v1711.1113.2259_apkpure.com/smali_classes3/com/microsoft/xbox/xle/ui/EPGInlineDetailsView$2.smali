.class Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$2;
.super Ljava/lang/Object;
.source "EPGInlineDetailsView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

.field final synthetic val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$2;->this$0:Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$2;->val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 208
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$2;->val$channel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->tuneToChannel()V

    .line 213
    :goto_0
    return-void

    .line 211
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog()V

    goto :goto_0
.end method
