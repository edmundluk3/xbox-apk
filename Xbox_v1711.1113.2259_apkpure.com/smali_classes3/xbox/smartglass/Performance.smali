.class public Lxbox/smartglass/Performance;
.super LMicrosoft/Telemetry/Data;
.source "Performance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxbox/smartglass/Performance$Schema;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LMicrosoft/Telemetry/Data",
        "<",
        "Lxbox/smartglass/CommonData;",
        ">;"
    }
.end annotation


# instance fields
.field private cached:I

.field private duration:I

.field private name:Ljava/lang/String;

.field private retryCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 265
    invoke-direct {p0}, LMicrosoft/Telemetry/Data;-><init>()V

    .line 267
    return-void
.end method

.method public static getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 260
    sget-object v0, Lxbox/smartglass/Performance$Schema;->schemaDef:Lcom/microsoft/bond/SchemaDef;

    return-object v0
.end method


# virtual methods
.method public clone()Lcom/microsoft/bond/BondSerializable;
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lxbox/smartglass/Performance;->clone()Lcom/microsoft/bond/BondSerializable;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Lcom/microsoft/bond/StructDef;)Lcom/microsoft/bond/BondMirror;
    .locals 1
    .param p1, "structDef"    # Lcom/microsoft/bond/StructDef;

    .prologue
    .line 244
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCached()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lxbox/smartglass/Performance;->cached:I

    return v0
.end method

.method public final getDuration()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lxbox/smartglass/Performance;->duration:I

    return v0
.end method

.method public getField(Lcom/microsoft/bond/FieldDef;)Ljava/lang/Object;
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;

    .prologue
    .line 204
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 214
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 206
    :sswitch_0
    iget-object v0, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    goto :goto_0

    .line 208
    :sswitch_1
    iget v0, p0, Lxbox/smartglass/Performance;->duration:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 210
    :sswitch_2
    iget v0, p0, Lxbox/smartglass/Performance;->cached:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 212
    :sswitch_3
    iget v0, p0, Lxbox/smartglass/Performance;->retryCount:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 204
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
    .end sparse-switch
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getRetryCount()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lxbox/smartglass/Performance;->retryCount:I

    return v0
.end method

.method public getSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 252
    invoke-static {}, Lxbox/smartglass/Performance;->getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v0

    return-object v0
.end method

.method public marshal(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 0
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 411
    invoke-static {p0, p1}, Lcom/microsoft/bond/internal/Marshaler;->marshal(Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/ProtocolWriter;)V

    .line 412
    return-void
.end method

.method public memberwiseCompare(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 466
    if-nez p1, :cond_1

    .line 472
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 470
    check-cast v0, Lxbox/smartglass/Performance;

    .line 472
    .local v0, "that":Lxbox/smartglass/Performance;
    invoke-virtual {p0, v0}, Lxbox/smartglass/Performance;->memberwiseCompareQuick(Lxbox/smartglass/Performance;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lxbox/smartglass/Performance;->memberwiseCompareDeep(Lxbox/smartglass/Performance;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected memberwiseCompareDeep(Lxbox/smartglass/Performance;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/Performance;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 487
    const/4 v0, 0x1

    .line 488
    .local v0, "equals":Z
    if-eqz v0, :cond_1

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareDeep(LMicrosoft/Telemetry/Data;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 489
    :goto_0
    if-eqz v0, :cond_3

    iget-object v3, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    if-nez v3, :cond_2

    :cond_0
    move v0, v1

    .line 490
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 488
    goto :goto_0

    .line 489
    :cond_2
    iget-object v3, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method protected memberwiseCompareQuick(Lxbox/smartglass/Performance;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/Performance;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 476
    const/4 v0, 0x1

    .line 477
    .local v0, "equals":Z
    if-eqz v0, :cond_1

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Data;->memberwiseCompareQuick(LMicrosoft/Telemetry/Data;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    .line 478
    :goto_0
    if-eqz v0, :cond_4

    iget-object v1, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    iget-object v4, p1, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    if-nez v4, :cond_3

    move v4, v2

    :goto_2
    if-ne v1, v4, :cond_4

    move v0, v2

    .line 479
    :goto_3
    if-eqz v0, :cond_6

    iget-object v1, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    if-nez v1, :cond_5

    :cond_0
    move v0, v2

    .line 480
    :goto_4
    if-eqz v0, :cond_7

    iget v1, p0, Lxbox/smartglass/Performance;->duration:I

    iget v4, p1, Lxbox/smartglass/Performance;->duration:I

    if-ne v1, v4, :cond_7

    move v0, v2

    .line 481
    :goto_5
    if-eqz v0, :cond_8

    iget v1, p0, Lxbox/smartglass/Performance;->cached:I

    iget v4, p1, Lxbox/smartglass/Performance;->cached:I

    if-ne v1, v4, :cond_8

    move v0, v2

    .line 482
    :goto_6
    if-eqz v0, :cond_9

    iget v1, p0, Lxbox/smartglass/Performance;->retryCount:I

    iget v4, p1, Lxbox/smartglass/Performance;->retryCount:I

    if-ne v1, v4, :cond_9

    move v0, v2

    .line 483
    :goto_7
    return v0

    :cond_1
    move v0, v3

    .line 477
    goto :goto_0

    :cond_2
    move v1, v3

    .line 478
    goto :goto_1

    :cond_3
    move v4, v3

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_3

    .line 479
    :cond_5
    iget-object v1, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_0

    :cond_6
    move v0, v3

    goto :goto_4

    :cond_7
    move v0, v3

    .line 480
    goto :goto_5

    :cond_8
    move v0, v3

    .line 481
    goto :goto_6

    :cond_9
    move v0, v3

    .line 482
    goto :goto_7
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readBegin()V

    .line 303
    invoke-virtual {p0, p1}, Lxbox/smartglass/Performance;->readNested(Lcom/microsoft/bond/ProtocolReader;)V

    .line 304
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readEnd()V

    .line 305
    return-void
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 323
    return-void
.end method

.method public readNested(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 311
    sget-object v0, Lcom/microsoft/bond/ProtocolCapability;->TAGGED:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v0}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 312
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Performance;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Performance;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-static {p1}, Lcom/microsoft/bond/internal/ReadHelper;->skipPartialStruct(Lcom/microsoft/bond/ProtocolReader;)V

    goto :goto_0
.end method

.method protected readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z
    .locals 5
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 360
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 362
    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 403
    :goto_0
    return v2

    .line 379
    .local v0, "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :sswitch_0
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    .line 397
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldEnd()V

    .line 369
    .end local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldBegin()Lcom/microsoft/bond/ProtocolReader$FieldTag;

    move-result-object v0

    .line 371
    .restart local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP:Lcom/microsoft/bond/BondDataType;

    if-eq v3, v4, :cond_1

    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_3

    .line 373
    :cond_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_2

    .line 400
    .local v1, "isPartial":Z
    :goto_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    move v2, v1

    .line 403
    goto :goto_0

    .end local v1    # "isPartial":Z
    :cond_2
    move v1, v2

    .line 373
    goto :goto_2

    .line 377
    :cond_3
    iget v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->id:I

    sparse-switch v3, :sswitch_data_0

    .line 393
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-virtual {p1, v3}, Lcom/microsoft/bond/ProtocolReader;->skip(Lcom/microsoft/bond/BondDataType;)V

    goto :goto_1

    .line 383
    :sswitch_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/Performance;->duration:I

    goto :goto_1

    .line 387
    :sswitch_2
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/Performance;->cached:I

    goto :goto_1

    .line 390
    :sswitch_3
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/Performance;->retryCount:I

    goto :goto_1

    .line 377
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
    .end sparse-switch
.end method

.method protected readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 326
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 328
    .local v0, "canOmitFields":Z
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 329
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 331
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 332
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    .line 339
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 340
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/Performance;->duration:I

    .line 347
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_5

    .line 348
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/Performance;->cached:I

    .line 351
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_7

    .line 352
    :cond_6
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/Performance;->retryCount:I

    .line 354
    :cond_7
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    .line 355
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 273
    const-string v0, "Performance"

    const-string v1, "xbox.smartglass.Performance"

    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/Performance;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    return-void
.end method

.method protected reset(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "qualifiedName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 277
    invoke-super {p0, p1, p2}, LMicrosoft/Telemetry/Data;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    .line 279
    iput v1, p0, Lxbox/smartglass/Performance;->duration:I

    .line 280
    iput v1, p0, Lxbox/smartglass/Performance;->cached:I

    .line 281
    iput v1, p0, Lxbox/smartglass/Performance;->retryCount:I

    .line 282
    return-void
.end method

.method public final setCached(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 80
    iput p1, p0, Lxbox/smartglass/Performance;->cached:I

    .line 81
    return-void
.end method

.method public final setDuration(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 66
    iput p1, p0, Lxbox/smartglass/Performance;->duration:I

    .line 67
    return-void
.end method

.method public setField(Lcom/microsoft/bond/FieldDef;Ljava/lang/Object;)V
    .locals 1
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 223
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 237
    .end local p2    # "value":Ljava/lang/Object;
    :goto_0
    return-void

    .line 225
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_0
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    goto :goto_0

    .line 228
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_1
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/Performance;->duration:I

    goto :goto_0

    .line 231
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_2
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/Performance;->cached:I

    goto :goto_0

    .line 234
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_3
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/Performance;->retryCount:I

    goto :goto_0

    .line 223
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
    .end sparse-switch
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public final setRetryCount(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 94
    iput p1, p0, Lxbox/smartglass/Performance;->retryCount:I

    .line 95
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    invoke-static {p1, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V

    .line 289
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 295
    check-cast p2, Lcom/microsoft/bond/SchemaDef;

    .end local p2    # "schema":Lcom/microsoft/bond/BondSerializable;
    invoke-static {p1, p2, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/BondSerializable;)V

    .line 296
    return-void
.end method

.method public write(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 2
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 418
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeBegin()V

    .line 420
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->getFirstPassWriter()Lcom/microsoft/bond/ProtocolWriter;

    move-result-object v0

    .local v0, "firstPassWriter":Lcom/microsoft/bond/ProtocolWriter;
    if-eqz v0, :cond_0

    .line 422
    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/Performance;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 423
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Performance;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 429
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeEnd()V

    .line 430
    return-void

    .line 427
    :cond_0
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/Performance;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    goto :goto_0
.end method

.method public writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V
    .locals 8
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x28

    const/16 v6, 0x1e

    .line 433
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 434
    .local v0, "canOmitFields":Z
    sget-object v1, Lxbox/smartglass/Performance$Schema;->metadata:Lcom/microsoft/bond/Metadata;

    invoke-virtual {p1, v1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructBegin(Lcom/microsoft/bond/BondSerializable;Z)V

    .line 435
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Data;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 437
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0xa

    invoke-static {}, Lxbox/smartglass/Performance$Schema;->access$000()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 438
    iget-object v1, p0, Lxbox/smartglass/Performance;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 439
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 441
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x14

    invoke-static {}, Lxbox/smartglass/Performance$Schema;->access$100()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 442
    iget v1, p0, Lxbox/smartglass/Performance;->duration:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 443
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 445
    if-eqz v0, :cond_0

    iget v1, p0, Lxbox/smartglass/Performance;->cached:I

    int-to-long v2, v1

    invoke-static {}, Lxbox/smartglass/Performance$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/bond/Metadata;->getDefault_value()Lcom/microsoft/bond/Variant;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/bond/Variant;->getUint_value()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 446
    :cond_0
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Performance$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v6, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 447
    iget v1, p0, Lxbox/smartglass/Performance;->cached:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 448
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 453
    :goto_0
    if-eqz v0, :cond_1

    iget v1, p0, Lxbox/smartglass/Performance;->retryCount:I

    int-to-long v2, v1

    invoke-static {}, Lxbox/smartglass/Performance$Schema;->access$300()Lcom/microsoft/bond/Metadata;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/bond/Metadata;->getDefault_value()Lcom/microsoft/bond/Variant;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/bond/Variant;->getUint_value()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 454
    :cond_1
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Performance$Schema;->access$300()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v7, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 455
    iget v1, p0, Lxbox/smartglass/Performance;->retryCount:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 456
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 461
    :goto_1
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructEnd(Z)V

    .line 462
    return-void

    .line 450
    :cond_2
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Performance$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v6, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldOmitted(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    goto :goto_0

    .line 458
    :cond_3
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    invoke-static {}, Lxbox/smartglass/Performance$Schema;->access$300()Lcom/microsoft/bond/Metadata;

    move-result-object v2

    invoke-virtual {p1, v1, v7, v2}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldOmitted(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    goto :goto_1
.end method
