.class Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$2;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "XboxRemoteControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$2;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$2;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$100(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->sendToggleViewKey()V

    .line 113
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 137
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$2;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$100(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->sendPowerUpDownKey()V

    .line 125
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl$2;->this$0:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->access$100(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->sendXboxKey()V

    .line 119
    const/4 v0, 0x1

    return v0
.end method
