.class public final Lcom/microsoft/xbox/idp/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/idp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accountPickerBase:I = 0x7f0e010f

.field public static final action0:I = 0x7f0e07e2

.field public static final action_bar:I = 0x7f0e00fd

.field public static final action_bar_activity_content:I = 0x7f0e0000

.field public static final action_bar_container:I = 0x7f0e00fc

.field public static final action_bar_root:I = 0x7f0e00f8

.field public static final action_bar_spinner:I = 0x7f0e0001

.field public static final action_bar_subtitle:I = 0x7f0e00db

.field public static final action_bar_title:I = 0x7f0e00da

.field public static final action_context_bar:I = 0x7f0e00fe

.field public static final action_dismiss:I = 0x7f0e0c15

.field public static final action_divider:I = 0x7f0e07e6

.field public static final action_menu_divider:I = 0x7f0e0002

.field public static final action_menu_presenter:I = 0x7f0e0003

.field public static final action_mode_bar:I = 0x7f0e00fa

.field public static final action_mode_bar_stub:I = 0x7f0e00f9

.field public static final action_mode_close_button:I = 0x7f0e00dc

.field public static final activity_chooser_view_content:I = 0x7f0e00dd

.field public static final add:I = 0x7f0e008c

.field public static final alertTitle:I = 0x7f0e00f1

.field public static final always:I = 0x7f0e00b9

.field public static final baseScreenBody:I = 0x7f0e0210

.field public static final baseScreenHeader:I = 0x7f0e020f

.field public static final baseScreenProgressView:I = 0x7f0e020e

.field public static final baseScreenView:I = 0x7f0e020d

.field public static final beginning:I = 0x7f0e00b5

.field public static final bottom:I = 0x7f0e0097

.field public static final buttonPanel:I = 0x7f0e00e4

.field public static final button_next:I = 0x7f0e0bd1

.field public static final button_previous:I = 0x7f0e0bd0

.field public static final cancel_action:I = 0x7f0e07e3

.field public static final checkbox:I = 0x7f0e00f4

.field public static final chronometer:I = 0x7f0e07eb

.field public static final collapseActionView:I = 0x7f0e00ba

.field public static final contentPanel:I = 0x7f0e00e7

.field public static final custom:I = 0x7f0e00ee

.field public static final customPanel:I = 0x7f0e00ed

.field public static final decor_content_parent:I = 0x7f0e00fb

.field public static final default_activity_button:I = 0x7f0e00e0

.field public static final disableHome:I = 0x7f0e0081

.field public static final edit_query:I = 0x7f0e00ff

.field public static final end:I = 0x7f0e009b

.field public static final end_padder:I = 0x7f0e07f5

.field public static final expand_activities_button:I = 0x7f0e00de

.field public static final expanded_menu:I = 0x7f0e00f3

.field public static final home:I = 0x7f0e0019

.field public static final homeAsUp:I = 0x7f0e0082

.field public static final icon:I = 0x7f0e00e2

.field public static final ifRoom:I = 0x7f0e00bb

.field public static final image:I = 0x7f0e00df

.field public static final imageUserTile:I = 0x7f0e0111

.field public static final info:I = 0x7f0e07ec

.field public static final line1:I = 0x7f0e07f1

.field public static final line3:I = 0x7f0e07f3

.field public static final listAccounts:I = 0x7f0e0110

.field public static final listMode:I = 0x7f0e007e

.field public static final list_item:I = 0x7f0e00e1

.field public static final loader_acc_prov_get_profile:I = 0x7f0e0021

.field public static final loader_claim_gamertag:I = 0x7f0e0022

.field public static final loader_event_initialization:I = 0x7f0e0023

.field public static final loader_finish_sign_in:I = 0x7f0e0024

.field public static final loader_get_privacy_settings:I = 0x7f0e0025

.field public static final loader_header_get_profile:I = 0x7f0e0026

.field public static final loader_intro_gamer_image:I = 0x7f0e0027

.field public static final loader_intro_gamer_profile:I = 0x7f0e0028

.field public static final loader_post_profile:I = 0x7f0e0029

.field public static final loader_reserve_gamertag:I = 0x7f0e002a

.field public static final loader_set_privacy_settings:I = 0x7f0e002b

.field public static final loader_sign_out:I = 0x7f0e002c

.field public static final loader_start_sign_in:I = 0x7f0e002d

.field public static final loader_suggestions:I = 0x7f0e002e

.field public static final loader_user_image_url:I = 0x7f0e002f

.field public static final loader_welcome_gamer_image:I = 0x7f0e0030

.field public static final loader_welcome_gamer_profile:I = 0x7f0e0031

.field public static final loader_x_token:I = 0x7f0e0032

.field public static final loader_xb_login:I = 0x7f0e0033

.field public static final loader_xb_logout:I = 0x7f0e0034

.field public static final media_actions:I = 0x7f0e07e5

.field public static final middle:I = 0x7f0e00b6

.field public static final msa_sdk_webflow_webview_resolve_interrupt:I = 0x7f0e0058

.field public static final msa_sdk_webflow_webview_sign_in:I = 0x7f0e0059

.field public static final msa_sdk_webflow_webview_sign_up:I = 0x7f0e005a

.field public static final multiply:I = 0x7f0e008d

.field public static final never:I = 0x7f0e00bc

.field public static final none:I = 0x7f0e0077

.field public static final normal:I = 0x7f0e007f

.field public static final parentPanel:I = 0x7f0e00e6

.field public static final progressView:I = 0x7f0e0a5b

.field public static final progress_circular:I = 0x7f0e005c

.field public static final progress_horizontal:I = 0x7f0e005d

.field public static final radio:I = 0x7f0e00f6

.field public static final screen:I = 0x7f0e008e

.field public static final scrollIndicatorDown:I = 0x7f0e00ec

.field public static final scrollIndicatorUp:I = 0x7f0e00e8

.field public static final scrollView:I = 0x7f0e00e9

.field public static final search_badge:I = 0x7f0e0101

.field public static final search_bar:I = 0x7f0e0100

.field public static final search_button:I = 0x7f0e0102

.field public static final search_close_btn:I = 0x7f0e0107

.field public static final search_edit_frame:I = 0x7f0e0103

.field public static final search_go_btn:I = 0x7f0e0109

.field public static final search_mag_icon:I = 0x7f0e0104

.field public static final search_plate:I = 0x7f0e0105

.field public static final search_src_text:I = 0x7f0e0106

.field public static final search_voice_btn:I = 0x7f0e010a

.field public static final select_dialog_listview:I = 0x7f0e010b

.field public static final shortcut:I = 0x7f0e00f5

.field public static final showCustom:I = 0x7f0e0083

.field public static final showHome:I = 0x7f0e0084

.field public static final showTitle:I = 0x7f0e0085

.field public static final signOutCheckBox:I = 0x7f0e0a4c

.field public static final spacer:I = 0x7f0e00e5

.field public static final split_action_bar:I = 0x7f0e0061

.field public static final src_atop:I = 0x7f0e008f

.field public static final src_in:I = 0x7f0e0090

.field public static final src_over:I = 0x7f0e0091

.field public static final static_page_body_first:I = 0x7f0e0a5d

.field public static final static_page_body_second:I = 0x7f0e0a5e

.field public static final static_page_buttons:I = 0x7f0e0a5f

.field public static final static_page_header:I = 0x7f0e0a5c

.field public static final status_bar_latest_event_content:I = 0x7f0e07e4

.field public static final submenuarrow:I = 0x7f0e00f7

.field public static final submit_area:I = 0x7f0e0108

.field public static final tabMode:I = 0x7f0e0080

.field public static final text:I = 0x7f0e07f4

.field public static final text2:I = 0x7f0e07f2

.field public static final textAddAccount:I = 0x7f0e019c

.field public static final textEmail:I = 0x7f0e0112

.field public static final textFirstLast:I = 0x7f0e0113

.field public static final textSpacerNoButtons:I = 0x7f0e00eb

.field public static final time:I = 0x7f0e07ea

.field public static final title:I = 0x7f0e00e3

.field public static final title_template:I = 0x7f0e00f0

.field public static final top:I = 0x7f0e00a0

.field public static final topPanel:I = 0x7f0e00ef

.field public static final up:I = 0x7f0e0068

.field public static final useLogo:I = 0x7f0e0086

.field public static final userTileOverflowMenu:I = 0x7f0e0114

.field public static final webFlowButtons:I = 0x7f0e0bcf

.field public static final withText:I = 0x7f0e00bd

.field public static final wrap_content:I = 0x7f0e0092

.field public static final xbid_aleady_have_gamer_tag_answer:I = 0x7f0e0bfc

.field public static final xbid_body_fragment:I = 0x7f0e0be1

.field public static final xbid_bottom_bar_shadow:I = 0x7f0e0bf0

.field public static final xbid_claim_it:I = 0x7f0e0bf1

.field public static final xbid_claim_it_bar:I = 0x7f0e0bfd

.field public static final xbid_clear_text:I = 0x7f0e0bf6

.field public static final xbid_close:I = 0x7f0e0bea

.field public static final xbid_different_gamer_tag_answer:I = 0x7f0e0bff

.field public static final xbid_display_name:I = 0x7f0e0bef

.field public static final xbid_done:I = 0x7f0e0bec

.field public static final xbid_enter_gamertag:I = 0x7f0e0bf5

.field public static final xbid_enter_gamertag_comment:I = 0x7f0e0bf8

.field public static final xbid_enter_gamertag_container:I = 0x7f0e0bf4

.field public static final xbid_error_buttons:I = 0x7f0e0be2

.field public static final xbid_error_left_button:I = 0x7f0e0be4

.field public static final xbid_error_message:I = 0x7f0e0be6

.field public static final xbid_error_right_button:I = 0x7f0e0be5

.field public static final xbid_gamerpic:I = 0x7f0e0bed

.field public static final xbid_gamerscore:I = 0x7f0e0bfe

.field public static final xbid_gamertag:I = 0x7f0e0bee

.field public static final xbid_greeting_text:I = 0x7f0e0be3

.field public static final xbid_header_fragment:I = 0x7f0e0be0

.field public static final xbid_privacy:I = 0x7f0e0bfa

.field public static final xbid_privacy_details:I = 0x7f0e0bfb

.field public static final xbid_scroll_container:I = 0x7f0e0beb

.field public static final xbid_search:I = 0x7f0e0bf7

.field public static final xbid_suggestion_text:I = 0x7f0e0c00

.field public static final xbid_suggestions_list:I = 0x7f0e0bf9

.field public static final xbid_title:I = 0x7f0e0bf2

.field public static final xbid_title_comment:I = 0x7f0e0bf3

.field public static final xbid_user_email:I = 0x7f0e0be9

.field public static final xbid_user_image:I = 0x7f0e0be7

.field public static final xbid_user_name:I = 0x7f0e0be8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
