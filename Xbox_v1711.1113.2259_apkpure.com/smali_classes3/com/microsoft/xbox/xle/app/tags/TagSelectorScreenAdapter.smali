.class public Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "TagSelectorScreenAdapter.java"


# instance fields
.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private tagListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

.field private tagsHashCode:I

.field private viewModel:Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;)V
    .locals 7
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 31
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    .line 34
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;->getTags()Ljava/util/List;

    move-result-object v3

    .line 36
    .local v3, "systemTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 38
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    const v4, 0x7f0e0aac

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 40
    const v4, 0x7f0e0aad

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 42
    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0c0149

    .line 43
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    if-eqz v1, :cond_0

    .line 44
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v4

    :goto_0
    invoke-direct {v5, v6, v4, v3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;-><init>(IILjava/util/List;)V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->tagListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .line 47
    const v4, 0x7f0e0aae

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 48
    .local v0, "errorPane":Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;->getErrorString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 50
    const v4, 0x7f0e0aaf

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 51
    .local v2, "noContentPane":Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;->getNoContentString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 53
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->tagListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 54
    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->tagsHashCode:I

    .line 55
    return-void

    .line 44
    .end local v0    # "errorPane":Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;
    .end local v2    # "noContentPane":Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;
    :cond_0
    const v4, -0x777778

    goto :goto_0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    return-object v0
.end method

.method public setSelectedTagListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V
    .locals 1
    .param p1, "tagSelectedListener"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->tagListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V

    .line 59
    return-void
.end method

.method protected updateViewOverride()V
    .locals 3

    .prologue
    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;->getTags()Ljava/util/List;

    move-result-object v0

    .line 67
    .local v0, "systemTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->tagsHashCode:I

    if-eq v1, v2, :cond_0

    .line 68
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->tagsHashCode:I

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->tagListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->clear()V

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->tagListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->tagListAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 74
    return-void
.end method
