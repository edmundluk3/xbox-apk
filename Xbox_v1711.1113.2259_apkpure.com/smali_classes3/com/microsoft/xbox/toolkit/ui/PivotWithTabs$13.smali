.class Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$13;
.super Ljava/lang/Object;
.source "PivotWithTabs.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/functions/GenericFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->adjustBottomMargin(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
        "<",
        "Ljava/lang/Boolean;",
        "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

.field final synthetic val$bottomMargin:I


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .prologue
    .line 420
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$13;->this$0:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$13;->val$bottomMargin:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public eval(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 423
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$13;->val$bottomMargin:I

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->adjustBottomMargin(I)V

    .line 424
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic eval(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 420
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$13;->eval(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
