.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
.super Ljava/lang/Object;
.source "EDSV2Airing.java"


# instance fields
.field public CallSign:Ljava/lang/String;

.field public ChannelId:Ljava/lang/String;

.field public ChannelName:Ljava/lang/String;

.field public ChannelNumber:Ljava/lang/String;

.field public EndTime:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
    .end annotation
.end field

.field public EpisodeName:Ljava/lang/String;

.field public HeadendId:Ljava/lang/String;

.field public ID:Ljava/lang/String;

.field public Images:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation
.end field

.field public IsHD:Z

.field public IsRepeat:Z

.field public StartTime:Ljava/util/Date;
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
