.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;
.super Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
.source "$AutoValue_ClubModerationDataTypes_ClubItemReport.java"


# instance fields
.field private final reportingXuid:J

.field private final textReason:Ljava/lang/String;


# direct methods
.method constructor <init>(JLjava/lang/String;)V
    .locals 1
    .param p1, "reportingXuid"    # J
    .param p3, "textReason"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->reportingXuid:J

    .line 17
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->textReason:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p1, p0, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v1

    .line 44
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 45
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    .line 46
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->reportingXuid:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->textReason:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 47
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->textReason()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->textReason:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->textReason()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    :cond_4
    move v1, v2

    .line 49
    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const v8, 0xf4243

    .line 54
    const/4 v0, 0x1

    .line 55
    .local v0, "h":I
    mul-int/2addr v0, v8

    .line 56
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->reportingXuid:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->reportingXuid:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 57
    mul-int/2addr v0, v8

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->textReason:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 59
    return v0

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->textReason:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public reportingXuid()J
    .locals 2

    .prologue
    .line 22
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->reportingXuid:J

    return-wide v0
.end method

.method public textReason()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->textReason:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubItemReport{reportingXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->reportingXuid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", textReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubModerationDataTypes_ClubItemReport;->textReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
