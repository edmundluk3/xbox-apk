.class public Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "BundlesScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;",
        ">",
        "Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;"
    }
.end annotation


# instance fields
.field private isLoadingBundlesData:Z

.field private loadBundlesDataTask:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel",
            "<TT;>.",
            "LoadBundlesDataAsyncTask;"
        }
    .end annotation
.end field

.field protected mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 27
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 19
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 29
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getBundlesAdapter(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 31
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 32
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 33
    .local v1, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 34
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 35
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 36
    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->isLoadingBundlesData:Z

    return p1
.end method


# virtual methods
.method public getBundlesData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBundles()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public hasBundlesData()Z
    .locals 1

    .prologue
    .line 100
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBundles()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 45
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->isLoadingBundlesData:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 51
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->loadBundlesDataTask:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->load(Z)V

    .line 52
    return-void
.end method

.method public navigateToDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 104
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 105
    return-void
.end method

.method public onLoadBundlesDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    const/4 v1, 0x0

    .line 78
    const-string v0, "BundlesDataScreenViewModel"

    const-string v2, "onLoadBundlesData Completed"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->isLoadingBundlesData:Z

    .line 81
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 95
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->setShouldHideScreen(Z)V

    .line 96
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->updateAdapter()V

    .line 97
    return-void

    .line 85
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->hasBundlesData()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_2
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_2

    .line 89
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v2, :cond_0

    .line 90
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 95
    goto :goto_1

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 61
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getBundlesAdapter(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 62
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 56
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->loadBundlesDataTask:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;

    .line 57
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->loadBundlesDataTask:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->loadBundlesDataTask:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;->cancel()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;->loadBundlesDataTask:Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel$LoadBundlesDataAsyncTask;

    .line 71
    :cond_0
    return-void
.end method
