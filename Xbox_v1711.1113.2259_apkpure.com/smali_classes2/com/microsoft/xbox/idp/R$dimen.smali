.class public final Lcom/microsoft/xbox/idp/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/idp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f09012d

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f09012e

.field public static final abc_action_bar_default_height_material:I = 0x7f090001

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f09012f

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f090130

.field public static final abc_action_bar_elevation_material:I = 0x7f090155

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f090156

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f090157

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f090158

.field public static final abc_action_bar_progress_bar_size:I = 0x7f090002

.field public static final abc_action_bar_stacked_max_height:I = 0x7f090159

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f09015a

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f09015b

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f09015c

.field public static final abc_action_button_min_height_material:I = 0x7f09015d

.field public static final abc_action_button_min_width_material:I = 0x7f09015e

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f09015f

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f090000

.field public static final abc_button_inset_horizontal_material:I = 0x7f090160

.field public static final abc_button_inset_vertical_material:I = 0x7f090161

.field public static final abc_button_padding_horizontal_material:I = 0x7f090162

.field public static final abc_button_padding_vertical_material:I = 0x7f090163

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f090164

.field public static final abc_config_prefDialogWidth:I = 0x7f090005

.field public static final abc_control_corner_material:I = 0x7f090165

.field public static final abc_control_inset_material:I = 0x7f090166

.field public static final abc_control_padding_material:I = 0x7f090167

.field public static final abc_dialog_fixed_height_major:I = 0x7f090006

.field public static final abc_dialog_fixed_height_minor:I = 0x7f090007

.field public static final abc_dialog_fixed_width_major:I = 0x7f090008

.field public static final abc_dialog_fixed_width_minor:I = 0x7f090009

.field public static final abc_dialog_min_width_major:I = 0x7f09000a

.field public static final abc_dialog_min_width_minor:I = 0x7f09000b

.field public static final abc_dialog_padding_material:I = 0x7f09016a

.field public static final abc_dialog_padding_top_material:I = 0x7f09016b

.field public static final abc_disabled_alpha_material_dark:I = 0x7f09016d

.field public static final abc_disabled_alpha_material_light:I = 0x7f09016e

.field public static final abc_dropdownitem_icon_width:I = 0x7f09016f

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f090170

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f090171

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f090172

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f090173

.field public static final abc_edit_text_inset_top_material:I = 0x7f090174

.field public static final abc_floating_window_z:I = 0x7f090175

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f090176

.field public static final abc_panel_menu_list_width:I = 0x7f090177

.field public static final abc_progress_bar_height_material:I = 0x7f090178

.field public static final abc_search_view_preferred_height:I = 0x7f090179

.field public static final abc_search_view_preferred_width:I = 0x7f09017a

.field public static final abc_seekbar_track_background_height_material:I = 0x7f09017b

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f09017c

.field public static final abc_select_dialog_padding_start_material:I = 0x7f09017d

.field public static final abc_switch_padding:I = 0x7f090148

.field public static final abc_text_size_body_1_material:I = 0x7f09017e

.field public static final abc_text_size_body_2_material:I = 0x7f09017f

.field public static final abc_text_size_button_material:I = 0x7f090180

.field public static final abc_text_size_caption_material:I = 0x7f090181

.field public static final abc_text_size_display_1_material:I = 0x7f090182

.field public static final abc_text_size_display_2_material:I = 0x7f090183

.field public static final abc_text_size_display_3_material:I = 0x7f090184

.field public static final abc_text_size_display_4_material:I = 0x7f090185

.field public static final abc_text_size_headline_material:I = 0x7f090186

.field public static final abc_text_size_large_material:I = 0x7f090187

.field public static final abc_text_size_medium_material:I = 0x7f090188

.field public static final abc_text_size_menu_header_material:I = 0x7f090189

.field public static final abc_text_size_menu_material:I = 0x7f09018a

.field public static final abc_text_size_small_material:I = 0x7f09018b

.field public static final abc_text_size_subhead_material:I = 0x7f09018c

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f090003

.field public static final abc_text_size_title_material:I = 0x7f09018d

.field public static final abc_text_size_title_material_toolbar:I = 0x7f090004

.field public static final accountPickerMargin:I = 0x7f09018e

.field public static final buttonBorder:I = 0x7f0901c1

.field public static final buttonHeight:I = 0x7f0901c2

.field public static final disabled_alpha_material_dark:I = 0x7f09026a

.field public static final disabled_alpha_material_light:I = 0x7f09026b

.field public static final highlight_alpha_material_colored:I = 0x7f090325

.field public static final highlight_alpha_material_dark:I = 0x7f090326

.field public static final highlight_alpha_material_light:I = 0x7f090327

.field public static final marginFooter:I = 0x7f09036f

.field public static final marginLarge:I = 0x7f090370

.field public static final marginMedium:I = 0x7f090371

.field public static final marginSmall:I = 0x7f090372

.field public static final marginXLarge:I = 0x7f090373

.field public static final maxAccountPickerHeight:I = 0x7f090376

.field public static final maxAccountPickerWidth:I = 0x7f090377

.field public static final menuHeight:I = 0x7f090379

.field public static final menuMarginRight:I = 0x7f09037a

.field public static final menuMarginTopBottom:I = 0x7f09037b

.field public static final menuWidth:I = 0x7f09037c

.field public static final notification_large_icon_height:I = 0x7f0903e8

.field public static final notification_large_icon_width:I = 0x7f0903e9

.field public static final notification_subtext_size:I = 0x7f0903ed

.field public static final paddingPopupMessageContentMaxWidth:I = 0x7f09041e

.field public static final paddingPopupMessageLarge:I = 0x7f09041f

.field public static final paddingPopupMessageMedium:I = 0x7f090420

.field public static final paddingPopupMessageParagraph:I = 0x7f090421

.field public static final paddingPopupMessageSmall:I = 0x7f090422

.field public static final userTile:I = 0x7f090588

.field public static final xbid_bg_stroke_width:I = 0x7f09059d

.field public static final xbid_body_image_diameter:I = 0x7f09059e

.field public static final xbid_body_padding_horizontal:I = 0x7f09059f

.field public static final xbid_body_padding_vertical:I = 0x7f0905a0

.field public static final xbid_bottom_bar_shadow_height:I = 0x7f0905a1

.field public static final xbid_button_border_width:I = 0x7f0905a2

.field public static final xbid_clickable_view_border_width:I = 0x7f0905a3

.field public static final xbid_clickable_view_padding:I = 0x7f0905a4

.field public static final xbid_header_image_diameter:I = 0x7f0905a5

.field public static final xbid_margin_body_standard:I = 0x7f0905a6

.field public static final xbid_margin_button:I = 0x7f0905a7

.field public static final xbid_margin_button_top:I = 0x7f0905a8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
