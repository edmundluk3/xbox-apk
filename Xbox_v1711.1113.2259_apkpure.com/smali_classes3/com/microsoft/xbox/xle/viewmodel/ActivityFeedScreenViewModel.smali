.class public Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;
.source "ActivityFeedScreenViewModel.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 0
    .param p1, "layout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleActivityFeedScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 9
    return-void
.end method


# virtual methods
.method public canHidePost()Z
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    return v0
.end method

.method public isPostButtonSticky()Z
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x1

    return v0
.end method

.method public showWhatsNewDialog()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/WhatsNewDetailsScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 23
    return-void
.end method
