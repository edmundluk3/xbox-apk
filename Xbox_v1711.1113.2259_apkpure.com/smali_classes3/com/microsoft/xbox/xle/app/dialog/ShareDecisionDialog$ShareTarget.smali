.class public final enum Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;
.super Ljava/lang/Enum;
.source "ShareDecisionDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ShareTarget"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

.field public static final enum ActivityFeed:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

.field public static final enum Clubs:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

.field public static final enum Messages:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

.field public static final enum SpecificClub:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    const-string v1, "ActivityFeed"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->ActivityFeed:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    const-string v1, "Clubs"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->Clubs:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    const-string v1, "Messages"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->Messages:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    const-string v1, "SpecificClub"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->SpecificClub:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    .line 37
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->ActivityFeed:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->Clubs:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->Messages:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->SpecificClub:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->$VALUES:[Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static allTargets()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->ActivityFeed:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->Clubs:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->Messages:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->$VALUES:[Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    return-object v0
.end method
