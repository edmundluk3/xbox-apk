.class Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$2;
.super Ljava/lang/Object;
.source "URCButtonHandler.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;->registerChild(Landroid/view/View;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

.field final synthetic val$bi:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$2;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$2;->val$bi:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "urcButton"    # Landroid/view/View;

    .prologue
    .line 157
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 158
    .local v0, "b":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$2;->val$bi:Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/urc/ui/URCButtonHandler$ButtonInfo;->registered_command:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->sendButton(Ljava/lang/String;Ljava/lang/String;)Z

    .line 166
    :cond_0
    return-void
.end method
