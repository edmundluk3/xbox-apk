.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "GameProfileAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LeaderboadHeaderViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
        ">;"
    }
.end annotation


# instance fields
.field compareAchievementButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0645
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .line 120
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 121
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackCompareButtonAction()V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->navigateToPeopleSelectorActivity()V

    .line 127
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V
    .locals 0
    .param p1, "compositeItem"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 133
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 115
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;)V

    return-void
.end method
