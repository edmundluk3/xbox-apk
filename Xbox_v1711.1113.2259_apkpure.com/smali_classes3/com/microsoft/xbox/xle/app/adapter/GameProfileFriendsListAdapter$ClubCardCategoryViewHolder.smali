.class Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "GameProfileFriendsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClubCardCategoryViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private clubList:Landroid/support/v7/widget/RecyclerView;

.field private header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

.field private seeAllButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;Landroid/view/View;)V
    .locals 4
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 216
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    .line 217
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 219
    const v0, 0x7f0e02c6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 220
    const v0, 0x7f0e02c7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->seeAllButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 221
    const v0, 0x7f0e02c8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->clubList:Landroid/support/v7/widget/RecyclerView;

    .line 223
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .line 224
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->clubList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 230
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->clubList:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2, v3, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 231
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;
    .param p1, "cardModel"    # Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    .prologue
    .line 226
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->getGameTitleId()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackGameHubNavigateToSuggestedClub(JJ)V

    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->clubId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V

    .line 228
    return-void
.end method

.method static synthetic lambda$onBind$1(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 240
    const-string v0, "Clubs - Game Hub See All Clubs"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;->clubCardCategoryItem:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    invoke-interface {v1, v0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 242
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;)V
    .locals 3
    .param p1, "item"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 235
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 236
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;->clubCardCategoryItem:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .line 237
    .local v0, "clubCardCategoryItem":Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->headerText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->seeAllButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->clear()V

    .line 245
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->recommendationListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;->getClubModels()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubCardListAdapter;->addAll(Ljava/util/Collection;)V

    .line 246
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 208
    check-cast p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;->onBind(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;)V

    return-void
.end method
