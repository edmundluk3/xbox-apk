.class public final enum Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;
.super Ljava/lang/Enum;
.source "ActivityHubServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/activityHub/IActivityHubService;


# annotations
.annotation build Landroid/support/annotation/WorkerThread;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/activityHub/IActivityHubService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

.field private static final CATEGORY_STUB_FILE:Ljava/lang/String; = "stubdata/TrendingAllCategories.json"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

.field private static final TAG:Ljava/lang/String;

.field private static final TAGS_STUB_FILE:Ljava/lang/String; = "stubdata/TrendingTags.json"

.field private static final TITLE_STUB_FILE:Ljava/lang/String; = "stubdata/TrendingAllCategories.json"

.field private static final TOPIC_STUB_FILE:Ljava/lang/String; = "stubdata/TrendingSingleTopic.json"

.field private static final USER_STUB_FILE:Ljava/lang/String; = "stubdata/TrendingAllCategories.json"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    .line 28
    const-class v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;

    return-object v0
.end method


# virtual methods
.method public getTrendingCategoryItems(ILjava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;
    .locals 3
    .param p1, "maxItems"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 40
    .local p2, "categoryFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;>;"
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/TrendingAllCategories.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseWithCategories;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_0
    return-object v1

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTrendingTags(ILjava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;
    .locals 6
    .param p1, "maxItems"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "scid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 79
    sget-object v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubServiceStub;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrendingTags(maxItems:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " scid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 82
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 83
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 86
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/TrendingTags.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    return-object v1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTrendingTitleItems(ILjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    .locals 3
    .param p1, "maxItems"    # I
    .param p2, "titleId"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 70
    .local p3, "categoryFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;>;"
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/TrendingAllCategories.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :goto_0
    return-object v1

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTrendingTopicItems(ILjava/lang/String;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
    .locals 3
    .param p1, "maxItems"    # I
    .param p2, "topicId"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 50
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/TrendingSingleTopic.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    :goto_0
    return-object v1

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTrendingUserItems(ILjava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    .locals 3
    .param p1, "maxItems"    # I
    .param p2, "xuid"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;",
            ">;)",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 60
    .local p3, "categoryFilters":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;>;"
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v2, "stubdata/TrendingAllCategories.json"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingFlatResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-object v1

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "ex":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_0
.end method
