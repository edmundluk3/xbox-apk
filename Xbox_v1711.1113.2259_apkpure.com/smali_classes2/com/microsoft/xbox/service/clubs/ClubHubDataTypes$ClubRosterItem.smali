.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubRosterItem"
.end annotation


# instance fields
.field private volatile transient createdDateObject:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1121
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRosterItem$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRosterItem$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(JJLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .locals 8
    .param p0, "actorXuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "xuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p4, "createdDate"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "localizedRole"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v0, 0x1

    .line 1113
    invoke-static {v0, v1, p0, p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 1114
    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 1115
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1117
    new-instance v1, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRosterItem;

    move-wide v2, p0

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRosterItem;-><init>(JJLjava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public abstract actorXuid()J
.end method

.method public abstract createdDate()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public getCreatedDate()Ljava/util/Date;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1126
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDateObject:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1127
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDate()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->convert(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDateObject:Ljava/util/Date;

    .line 1128
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDateObject:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1129
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not parse member creation date: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1130
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDateObject:Ljava/util/Date;

    .line 1134
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDateObject:Ljava/util/Date;

    return-object v0
.end method

.method public abstract localizedRole()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract xuid()J
.end method
