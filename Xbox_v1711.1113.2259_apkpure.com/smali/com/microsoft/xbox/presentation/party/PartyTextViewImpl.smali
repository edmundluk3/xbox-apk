.class public Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
.source "PartyTextViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/party/PartyTextView;


# instance fields
.field characterCountText:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0871
    .end annotation
.end field

.field memberList:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e086c
    .end annotation
.end field

.field private memberListAdapter:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

.field messageInput:Landroid/widget/EditText;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e086f
    .end annotation
.end field

.field messageList:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e086d
    .end annotation
.end field

.field myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private oldInputMode:I

.field presenter:Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field sendButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0870
    .end annotation
.end field

.field statusText:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e086b
    .end annotation
.end field

.field private streamsBound:Z

.field private textListAdapter:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->streamsBound:Z

    .line 77
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->streamsBound:Z

    .line 77
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 85
    return-void
.end method

.method private declared-synchronized bindStreams(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)V
    .locals 7
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->textStream()Lio/reactivex/Observable;

    move-result-object v1

    .line 163
    .local v1, "textStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->memberStream()Lio/reactivex/Observable;

    move-result-object v0

    .line 164
    .local v0, "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->streamsBound:Z

    if-nez v2, :cond_0

    .line 165
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v3, 0x2

    new-array v3, v3, [Lio/reactivex/disposables/Disposable;

    const/4 v4, 0x0

    .line 167
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v1, v5}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v6

    .line 168
    invoke-virtual {v5, v6}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 181
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v0, v5}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v6

    .line 182
    invoke-virtual {v5, v6}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v5

    aput-object v5, v3, v4

    .line 165
    invoke-virtual {v2, v3}, Lio/reactivex/disposables/CompositeDisposable;->addAll([Lio/reactivex/disposables/Disposable;)Z

    .line 192
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->streamsBound:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :cond_0
    monitor-exit p0

    return-void

    .line 162
    .end local v0    # "memberStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    .end local v1    # "textStream":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic lambda$bindStreams$2(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;
    .param p1, "message"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->textListAdapter:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->add(Ljava/lang/Object;)V

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->textListAdapter:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->textListAdapter:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->notifyItemInserted(I)V

    .line 174
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->myXuidProvider:Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;

    invoke-interface {v1}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuidString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageList:Landroid/support/v7/widget/RecyclerView;

    .line 175
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isScrolledToBottom(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->textListAdapter:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getDataCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPosition(Landroid/support/v7/widget/RecyclerView;I)V

    .line 178
    :cond_1
    return-void
.end method

.method static synthetic lambda$bindStreams$3(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;
    .param p1, "member"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 183
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberListAdapter:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v1

    .line 184
    .local v1, "memberList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 185
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->partyMember()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberListAdapter:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    invoke-virtual {v2, v0, p1}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->notifyItemChanged(ILjava/lang/Object;)V

    .line 184
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_1
    return-void
.end method

.method static synthetic lambda$onCreateContentView$0(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;
    .param p1, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$onCreateContentView$1(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;Ljava/lang/String;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;
    .param p1, "ignored"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageInput:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageInput:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dismissKeyboard(Landroid/view/View;)V

    .line 109
    return-void
.end method

.method private mapViewStateToPartyMemberListItems(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)Ljava/util/List;
    .locals 14
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/party/PartyTextViewState;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getRoster()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    .line 243
    .local v4, "partyMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v8

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->meIsHost(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v1, 0x1

    .line 244
    .local v1, "meIsHost":Z
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    .line 245
    .local v6, "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    :goto_2
    if-eqz v4, :cond_5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 246
    .local v3, "partyMemberCount":I
    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    add-int/lit8 v8, v3, 0x1

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 248
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;>;"
    if-eqz v4, :cond_7

    .line 249
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 250
    .local v2, "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    const/4 v5, 0x0

    .line 251
    .local v5, "partyUser":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    invoke-virtual {v6}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 252
    .local v7, "userSummary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    invoke-virtual {v7}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v10

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-nez v10, :cond_0

    .line 253
    move-object v5, v7

    .line 258
    .end local v7    # "userSummary":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :cond_1
    if-eqz v1, :cond_6

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v5}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->xuid()J

    move-result-wide v12

    cmp-long v8, v10, v12

    if-eqz v8, :cond_6

    const/4 v8, 0x1

    :goto_5
    invoke-static {v2, v5, v8}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->with(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;Z)Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 242
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;>;"
    .end local v1    # "meIsHost":Z
    .end local v2    # "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .end local v3    # "partyMemberCount":I
    .end local v4    # "partyMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    .end local v5    # "partyUser":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .end local v6    # "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 243
    .restart local v4    # "partyMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 244
    .restart local v1    # "meIsHost":Z
    :cond_4
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    goto :goto_2

    .line 245
    .restart local v6    # "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    :cond_5
    const/4 v3, 0x0

    goto :goto_3

    .line 258
    .restart local v0    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;>;"
    .restart local v2    # "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .restart local v3    # "partyMemberCount":I
    .restart local v5    # "partyUser":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :cond_6
    const/4 v8, 0x0

    goto :goto_5

    .line 262
    .end local v2    # "partyMember":Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .end local v5    # "partyUser":Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    :cond_7
    return-object v0
.end method

.method private renderHeader(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)V
    .locals 8
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    .prologue
    const/4 v5, 0x1

    .line 197
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v1

    .line 198
    .local v1, "partySession":Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getRoster()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 200
    .local v0, "partyMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :goto_0
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 201
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v5, :cond_3

    .line 202
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->statusText:Landroid/widget/TextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isJoinable()Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f070a33

    :goto_1
    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    :cond_0
    :goto_2
    return-void

    .line 198
    .end local v0    # "partyMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 202
    .restart local v0    # "partyMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    :cond_2
    const v2, 0x7f070a2f

    goto :goto_1

    .line 204
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->statusText:Landroid/widget/TextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isJoinable()Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f070a32

    :goto_3
    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    const v2, 0x7f070a2e

    goto :goto_3
.end method

.method private renderMemberList(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)V
    .locals 4
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    .prologue
    .line 210
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberListAdapter:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v2

    .line 211
    .local v2, "oldList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->mapViewStateToPartyMemberListItems(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)Ljava/util/List;

    move-result-object v1

    .line 213
    .local v1, "newList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberListAdapter:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->clear()V

    .line 214
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberListAdapter:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;->addAll(Ljava/util/Collection;)V

    .line 216
    new-instance v3, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v3, v2, v1}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v3}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v0

    .line 217
    .local v0, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberListAdapter:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    invoke-virtual {v0, v3}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 218
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    const-string v0, "Party - Party Chat View"

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 238
    const v0, 0x7f0301a7

    return v0
.end method

.method protected getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->presenter:Lcom/microsoft/xbox/presentation/party/PartyTextPresenter;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreate()V

    .line 90
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;)V

    .line 91
    return-void
.end method

.method public onCreateContentView()V
    .locals 3

    .prologue
    .line 95
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreateContentView()V

    .line 97
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    invoke-direct {v1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->textListAdapter:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->messageList:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->textListAdapter:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 100
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    invoke-direct {v1}, Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberListAdapter:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberList:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->memberListAdapter:Lcom/microsoft/xbox/presentation/party/PartyMemberIconListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->sendButton:Landroid/widget/Button;

    .line 104
    invoke-static {v1}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 105
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 106
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 110
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 112
    .local v0, "textSent":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/party/PartyTextViewIntents$SendTextIntent;>;"
    const/4 v1, 0x1

    new-array v1, v1, [Lio/reactivex/ObservableSource;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 113
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 143
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onDestroy()V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 145
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 119
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onStart()V

    .line 120
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 122
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 124
    .local v1, "wnd":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->oldInputMode:I

    .line 125
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 127
    .end local v1    # "wnd":Landroid/view/Window;
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 131
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 133
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 135
    .local v1, "wnd":Landroid/view/Window;
    iget v2, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->oldInputMode:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 138
    .end local v1    # "wnd":Landroid/view/Window;
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onStop()V

    .line 139
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)V
    .locals 2
    .param p1, "viewState"    # Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    .prologue
    .line 149
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 159
    :goto_0
    return-void

    .line 151
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 155
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->bindStreams(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)V

    .line 156
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->renderHeader(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)V

    .line 157
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->renderMemberList(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)V

    goto :goto_0
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 46
    check-cast p1, Lcom/microsoft/xbox/presentation/party/PartyTextViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->render(Lcom/microsoft/xbox/presentation/party/PartyTextViewState;)V

    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<+",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
