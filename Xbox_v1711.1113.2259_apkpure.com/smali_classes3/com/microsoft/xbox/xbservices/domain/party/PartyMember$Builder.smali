.class public abstract Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
.super Ljava/lang/Object;
.source "PartyMember.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
.end method

.method public abstract isHost(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
.end method

.method public abstract isSelfMuted(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
.end method

.method public abstract isTalking(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
.end method

.method public abstract multiplayerMember(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
.end method

.method public abstract privacyPermissions(Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
.end method

.method public abstract rosterKey(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$Builder;
.end method
