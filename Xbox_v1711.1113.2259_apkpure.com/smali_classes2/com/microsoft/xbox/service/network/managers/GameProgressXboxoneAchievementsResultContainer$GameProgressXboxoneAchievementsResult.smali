.class public Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
.super Ljava/lang/Object;
.source "GameProgressXboxoneAchievementsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameProgressXboxoneAchievementsResult"
.end annotation


# instance fields
.field public achievements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation
.end field

.field public pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 4
    .param p0, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 283
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 284
    .local v0, "builder":Lcom/google/gson/GsonBuilder;
    const-class v2, Ljava/util/Date;

    new-instance v3, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCRoundtripDateConverterJSONDeserializer;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCRoundtripDateConverterJSONDeserializer;-><init>()V

    invoke-virtual {v0, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v1

    .line 286
    .local v1, "gson":Lcom/google/gson/Gson;
    const-class v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    invoke-static {v1, p0, v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Lcom/google/gson/Gson;Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    return-object v2
.end method
