.class Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleHubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ToggleFollowingStateRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private follow:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

.field private titleId:J


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;JZ)V
    .locals 0
    .param p2, "titleId"    # J
    .param p4, "follow"    # Z

    .prologue
    .line 298
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 299
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->titleId:J

    .line 300
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->follow:Z

    .line 301
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;

    .prologue
    .line 293
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->follow:Z

    return v0
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->buildData()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/lang/Void;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 305
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->titleId:J

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->follow:Z

    invoke-interface {v0, v2, v3, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->setFollowingState(JZ)V

    .line 306
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 320
    const-wide/16 v0, 0x2454

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 315
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel$ToggleFollowingStateRunner;->titleId:J

    invoke-virtual {v0, p1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->updateFollowState(Lcom/microsoft/xbox/toolkit/AsyncResult;J)V

    .line 316
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 311
    return-void
.end method
