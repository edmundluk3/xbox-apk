.class final Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState;
.super Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
.source "AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState.java"


# instance fields
.field private final isHidden:Z

.field private final position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;


# direct methods
.method constructor <init>(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)V
    .locals 2
    .param p1, "isHidden"    # Z
    .param p2, "position"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;-><init>()V

    .line 16
    iput-boolean p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState;->isHidden:Z

    .line 17
    if-nez p2, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null position"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState;->position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    .line 21
    return-void
.end method


# virtual methods
.method public isHidden()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState;->isHidden:Z

    return v0
.end method

.method public position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState;->position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BadgeViewState{isHidden="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState;->isHidden:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_BadgeViewState;->position:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
