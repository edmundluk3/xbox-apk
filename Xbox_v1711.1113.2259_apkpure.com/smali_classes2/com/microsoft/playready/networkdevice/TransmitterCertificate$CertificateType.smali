.class public final enum Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;
.super Ljava/lang/Enum;
.source "TransmitterCertificate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/networkdevice/TransmitterCertificate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CertificateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum Application:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum Device:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum Domain:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum Issuer:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum KeyFileSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum LicenseSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum Metering:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum PC:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum RevocationSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum Server:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum Service:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum Silverlight:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

.field public static final enum Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;


# instance fields
.field private certificateType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 85
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v4, v4}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 86
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "PC"

    invoke-direct {v0, v1, v5, v5}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->PC:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 87
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "Device"

    invoke-direct {v0, v1, v6, v6}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Device:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 88
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "Domain"

    invoke-direct {v0, v1, v7, v7}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Domain:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 89
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "Issuer"

    invoke-direct {v0, v1, v8, v8}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Issuer:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 90
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "RevocationSigner"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->RevocationSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 91
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "Service"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Service:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 92
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "Silverlight"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Silverlight:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 93
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "Application"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Application:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 94
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "Metering"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Metering:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 95
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "KeyFileSigner"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->KeyFileSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 96
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "Server"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Server:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 97
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    const-string v1, "LicenseSigner"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->LicenseSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 83
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->PC:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Device:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Domain:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Issuer:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->RevocationSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Service:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Silverlight:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Application:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Metering:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->KeyFileSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Server:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->LicenseSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->$VALUES:[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "certType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->certificateType:I

    .line 103
    iput p3, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->certificateType:I

    .line 104
    return-void
.end method

.method static fromValue(I)Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 113
    sget-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 115
    .local v0, "retVal":Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;
    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->Unknown:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    iget v1, v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->certificateType:I

    if-gt v1, p0, :cond_0

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->LicenseSigner:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    iget v1, v1, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->certificateType:I

    if-gt p0, v1, :cond_0

    .line 117
    invoke-static {}, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->values()[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    move-result-object v1

    aget-object v0, v1, p0

    .line 120
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    const-class v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->$VALUES:[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    invoke-virtual {v0}, [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    return-object v0
.end method


# virtual methods
.method getCertificateType()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;->certificateType:I

    return v0
.end method
