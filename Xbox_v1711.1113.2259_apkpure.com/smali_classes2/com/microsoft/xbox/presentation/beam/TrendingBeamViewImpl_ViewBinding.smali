.class public Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_ViewBinding;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;
.source "TrendingBeamViewImpl_ViewBinding.java"


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Landroid/view/View;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;Landroid/view/View;)V

    .line 25
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;

    .line 27
    const v0, 0x7f0e0aea

    const-string v1, "field \'contentList\'"

    const-class v2, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    .line 28
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;

    .line 33
    .local v0, "target":Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 34
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;

    .line 36
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    .line 38
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;->unbind()V

    .line 39
    return-void
.end method
