.class public final Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;
.super Ljava/lang/Object;
.source "MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lokhttp3/OkHttpClient;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final localeHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final loggingInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

.field private final xTokenAuthenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final xblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p2, "xTokenAuthenticatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;>;"
    .local p3, "xTokenHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;>;"
    .local p4, "xUserAgentHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;>;"
    .local p5, "xblCorrelationIdHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;>;"
    .local p6, "loggingInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/logging/HttpLoggingInterceptor;>;"
    .local p7, "localeHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;>;"
    .local p8, "contentRestrictionsHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 50
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->module:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    .line 51
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    .line 53
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 55
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 57
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->xblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 59
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 60
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->loggingInterceptorProvider:Ljavax/inject/Provider;

    .line 61
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_6
    iput-object p7, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 63
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 64
    :cond_7
    iput-object p8, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 66
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 9
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/logging/HttpLoggingInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "xTokenAuthenticatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;>;"
    .local p2, "xTokenHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;>;"
    .local p3, "xUserAgentHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;>;"
    .local p4, "xblCorrelationIdHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;>;"
    .local p5, "loggingInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/logging/HttpLoggingInterceptor;>;"
    .local p6, "localeHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;>;"
    .local p7, "contentRestrictionsHeaderInterceptorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;>;"
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;-><init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvidesMultiplayerSessionClient(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;)Lokhttp3/OkHttpClient;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;
    .param p1, "xTokenAuthenticator"    # Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;
    .param p2, "xTokenHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;
    .param p3, "xUserAgentHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;
    .param p4, "xblCorrelationIdHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;
    .param p5, "loggingInterceptor"    # Lokhttp3/logging/HttpLoggingInterceptor;
    .param p6, "localeHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;
    .param p7, "contentRestrictionsHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;

    .prologue
    .line 116
    invoke-virtual/range {p0 .. p7}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;->providesMultiplayerSessionClient(Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;)Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->get()Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method

.method public get()Lokhttp3/OkHttpClient;
    .locals 8

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->module:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;

    iget-object v1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->xTokenAuthenticatorProvider:Ljavax/inject/Provider;

    .line 72
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->xTokenHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 73
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;

    iget-object v3, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->xUserAgentHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 74
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;

    iget-object v4, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->xblCorrelationIdHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 75
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;

    iget-object v5, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->loggingInterceptorProvider:Ljavax/inject/Provider;

    .line 76
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lokhttp3/logging/HttpLoggingInterceptor;

    iget-object v6, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->localeHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 77
    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;

    iget-object v7, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule_ProvidesMultiplayerSessionClientFactory;->contentRestrictionsHeaderInterceptorProvider:Ljavax/inject/Provider;

    .line 78
    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;

    .line 71
    invoke-virtual/range {v0 .. v7}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;->providesMultiplayerSessionClient(Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XXblCorrelationIdHeaderInterceptor;Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;)Lokhttp3/OkHttpClient;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 70
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient;

    return-object v0
.end method
