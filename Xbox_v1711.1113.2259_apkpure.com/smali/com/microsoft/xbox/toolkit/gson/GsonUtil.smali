.class public Lcom/microsoft/xbox/toolkit/gson/GsonUtil;
.super Ljava/lang/Object;
.source "GsonUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/gson/GsonUtil$JsonBodyBuilder;,
        Lcom/microsoft/xbox/toolkit/gson/GsonUtil$GsonObjectHolder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildJsonBody(Lcom/microsoft/xbox/toolkit/gson/GsonUtil$JsonBodyBuilder;)Ljava/lang/String;
    .locals 3
    .param p0, "builder"    # Lcom/microsoft/xbox/toolkit/gson/GsonUtil$JsonBodyBuilder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 182
    .local v0, "out":Ljava/io/StringWriter;
    :try_start_0
    new-instance v1, Lcom/google/gson/stream/JsonWriter;

    invoke-direct {v1, v0}, Lcom/google/gson/stream/JsonWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 184
    .local v1, "w":Lcom/google/gson/stream/JsonWriter;
    :try_start_1
    invoke-interface {p0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil$JsonBodyBuilder;->buildBody(Lcom/google/gson/stream/JsonWriter;)V

    .line 185
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 187
    :try_start_2
    invoke-virtual {v1}, Lcom/google/gson/stream/JsonWriter;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 190
    invoke-virtual {v0}, Ljava/io/StringWriter;->close()V

    .line 185
    return-object v2

    .line 187
    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Lcom/google/gson/stream/JsonWriter;->close()V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 190
    .end local v1    # "w":Lcom/google/gson/stream/JsonWriter;
    :catchall_1
    move-exception v2

    invoke-virtual {v0}, Ljava/io/StringWriter;->close()V

    throw v2
.end method

.method public static createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;
    .locals 4

    .prologue
    .line 165
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0x80

    aput v3, v1, v2

    .line 166
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithModifiers([I)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 167
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/AutoValueGsonAdapterFactory;->create()Lcom/google/gson/TypeAdapterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 168
    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/gson/AutoValueGsonAdapterFactory;->create()Lcom/google/gson/TypeAdapterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;-><init>()V

    .line 169
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/google/common/collect/ImmutableList;

    new-instance v2, Lcom/microsoft/xbox/toolkit/gson/ImmutableListDeserializer;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/gson/ImmutableListDeserializer;-><init>()V

    .line 170
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/google/common/collect/ImmutableSet;

    new-instance v2, Lcom/microsoft/xbox/toolkit/gson/ImmutableSetDeserializer;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/gson/ImmutableSetDeserializer;-><init>()V

    .line 171
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 165
    return-object v0
.end method

.method public static deserializeJson(Lcom/google/gson/Gson;Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 9
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/Gson;",
            "Ljava/io/InputStream;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 117
    .local p2, "resultClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v3, 0x0

    .line 118
    .local v3, "iReader":Ljava/io/InputStreamReader;
    const/4 v0, 0x0

    .line 119
    .local v0, "bReader":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 121
    .local v5, "result":Ljava/lang/Object;, "TT;"
    :try_start_0
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    .end local v3    # "iReader":Ljava/io/InputStreamReader;
    .local v4, "iReader":Ljava/io/InputStreamReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 123
    .end local v0    # "bReader":Ljava/io/BufferedReader;
    .local v1, "bReader":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {p0, v1, p2}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v5

    .line 127
    if-eqz v1, :cond_0

    .line 129
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 133
    :cond_0
    :goto_0
    if-eqz v4, :cond_5

    .line 135
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-object v0, v1

    .end local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v0    # "bReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 140
    .end local v4    # "iReader":Ljava/io/InputStreamReader;
    .end local v5    # "result":Ljava/lang/Object;, "TT;"
    .restart local v3    # "iReader":Ljava/io/InputStreamReader;
    :cond_1
    :goto_1
    return-object v5

    .line 136
    .end local v0    # "bReader":Ljava/io/BufferedReader;
    .end local v3    # "iReader":Ljava/io/InputStreamReader;
    .restart local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v4    # "iReader":Ljava/io/InputStreamReader;
    .restart local v5    # "result":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v6

    move-object v0, v1

    .end local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v0    # "bReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 137
    .end local v4    # "iReader":Ljava/io/InputStreamReader;
    .restart local v3    # "iReader":Ljava/io/InputStreamReader;
    goto :goto_1

    .line 124
    :catch_1
    move-exception v2

    .line 125
    .local v2, "ex":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v6, "GsonUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deserializedJson error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 127
    if-eqz v0, :cond_2

    .line 129
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 133
    :cond_2
    :goto_3
    if-eqz v3, :cond_1

    .line 135
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_1

    .line 136
    :catch_2
    move-exception v6

    goto :goto_1

    .line 127
    .end local v2    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v0, :cond_3

    .line 129
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    .line 133
    :cond_3
    :goto_5
    if-eqz v3, :cond_4

    .line 135
    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    .line 137
    :cond_4
    :goto_6
    throw v6

    .line 130
    .end local v0    # "bReader":Ljava/io/BufferedReader;
    .end local v3    # "iReader":Ljava/io/InputStreamReader;
    .restart local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v4    # "iReader":Ljava/io/InputStreamReader;
    :catch_3
    move-exception v6

    goto :goto_0

    .end local v1    # "bReader":Ljava/io/BufferedReader;
    .end local v4    # "iReader":Ljava/io/InputStreamReader;
    .restart local v0    # "bReader":Ljava/io/BufferedReader;
    .restart local v2    # "ex":Ljava/lang/Exception;
    .restart local v3    # "iReader":Ljava/io/InputStreamReader;
    :catch_4
    move-exception v6

    goto :goto_3

    .end local v2    # "ex":Ljava/lang/Exception;
    :catch_5
    move-exception v7

    goto :goto_5

    .line 136
    :catch_6
    move-exception v7

    goto :goto_6

    .line 127
    .end local v3    # "iReader":Ljava/io/InputStreamReader;
    .restart local v4    # "iReader":Ljava/io/InputStreamReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "iReader":Ljava/io/InputStreamReader;
    .restart local v3    # "iReader":Ljava/io/InputStreamReader;
    goto :goto_4

    .end local v0    # "bReader":Ljava/io/BufferedReader;
    .end local v3    # "iReader":Ljava/io/InputStreamReader;
    .restart local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v4    # "iReader":Ljava/io/InputStreamReader;
    :catchall_2
    move-exception v6

    move-object v0, v1

    .end local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v0    # "bReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "iReader":Ljava/io/InputStreamReader;
    .restart local v3    # "iReader":Ljava/io/InputStreamReader;
    goto :goto_4

    .line 124
    .end local v3    # "iReader":Ljava/io/InputStreamReader;
    .restart local v4    # "iReader":Ljava/io/InputStreamReader;
    :catch_7
    move-exception v2

    move-object v3, v4

    .end local v4    # "iReader":Ljava/io/InputStreamReader;
    .restart local v3    # "iReader":Ljava/io/InputStreamReader;
    goto :goto_2

    .end local v0    # "bReader":Ljava/io/BufferedReader;
    .end local v3    # "iReader":Ljava/io/InputStreamReader;
    .restart local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v4    # "iReader":Ljava/io/InputStreamReader;
    :catch_8
    move-exception v2

    move-object v0, v1

    .end local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v0    # "bReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "iReader":Ljava/io/InputStreamReader;
    .restart local v3    # "iReader":Ljava/io/InputStreamReader;
    goto :goto_2

    .end local v0    # "bReader":Ljava/io/BufferedReader;
    .end local v3    # "iReader":Ljava/io/InputStreamReader;
    .restart local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v4    # "iReader":Ljava/io/InputStreamReader;
    :cond_5
    move-object v0, v1

    .end local v1    # "bReader":Ljava/io/BufferedReader;
    .restart local v0    # "bReader":Ljava/io/BufferedReader;
    move-object v3, v4

    .end local v4    # "iReader":Ljava/io/InputStreamReader;
    .restart local v3    # "iReader":Ljava/io/InputStreamReader;
    goto :goto_1
.end method

.method public static deserializeJson(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 5
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .param p1, "input"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/Gson;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 145
    .local p2, "resultClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v1, 0x0

    .line 147
    .local v1, "result":Ljava/lang/Object;, "TT;"
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 151
    .end local v1    # "result":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object v1

    .line 148
    .restart local v1    # "result":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v0

    .line 149
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "GsonUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deserializedJson error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/InputStream;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "resultClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 57
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Lcom/google/gson/Gson;Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public static deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "stream"    # Ljava/io/InputStream;
    .param p2, "typeForAdapter"    # Ljava/lang/reflect/Type;
    .param p3, "typeAdapter"    # Ljava/lang/Object;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/InputStream;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "resultClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 87
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Lcom/google/gson/Gson;Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public static deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/util/Map;)Ljava/lang/Object;
    .locals 5
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/InputStream;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Object;",
            ">;)TT;"
        }
    .end annotation

    .prologue
    .line 92
    .local p1, "resultClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p2, "adapters":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/reflect/Type;Ljava/lang/Object;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 93
    .local v0, "builder":Lcom/google/gson/GsonBuilder;
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 94
    .local v1, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/reflect/Type;Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/reflect/Type;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    goto :goto_0

    .line 96
    .end local v1    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/reflect/Type;Ljava/lang/Object;>;"
    :cond_0
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v2

    invoke-static {v2, p0, p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Lcom/google/gson/Gson;Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method public static deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .param p0, "input"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "resultClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 71
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public static deserializeJson(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "input"    # Ljava/lang/String;
    .param p2, "typeForAdapter"    # Ljava/lang/reflect/Type;
    .param p3, "typeAdapter"    # Ljava/lang/Object;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "resultClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 112
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public static deserializeJsonWithDateAdapter(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p0, "stream"    # Ljava/io/InputStream;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/io/InputStream;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 161
    .local p1, "resultClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil$GsonObjectHolder;->access$000()Lcom/google/gson/Gson;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Lcom/google/gson/Gson;Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static deserializeJsonWithDateAdapter(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "resultClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil$GsonObjectHolder;->access$000()Lcom/google/gson/Gson;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 175
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 176
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v0, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
