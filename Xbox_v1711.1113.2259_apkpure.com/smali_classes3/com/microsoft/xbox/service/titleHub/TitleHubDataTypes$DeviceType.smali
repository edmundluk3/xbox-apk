.class public final enum Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;
.super Ljava/lang/Enum;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeviceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum MCapensis:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum MoLIVE:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum PC:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum Web:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum Win32:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum WindowsOneCore:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum WindowsOneCoreMobile:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum WindowsPhone:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum WindowsPhone7:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum Xbox360:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

.field public static final enum XboxOne:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "WindowsOneCore"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->WindowsOneCore:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "WindowsOneCoreMobile"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->WindowsOneCoreMobile:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "MCapensis"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->MCapensis:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "XboxOne"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->XboxOne:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "Xbox360"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->Xbox360:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "MoLIVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->MoLIVE:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "WindowsPhone"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->WindowsPhone:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "WindowsPhone7"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->WindowsPhone7:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "PC"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->PC:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "Web"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->Web:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    const-string v1, "Win32"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->Win32:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    .line 29
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->WindowsOneCore:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->WindowsOneCoreMobile:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->MCapensis:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->XboxOne:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->Xbox360:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->MoLIVE:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->WindowsPhone:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->WindowsPhone7:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->PC:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->Web:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->Win32:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->$VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->$VALUES:[Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    return-object v0
.end method
