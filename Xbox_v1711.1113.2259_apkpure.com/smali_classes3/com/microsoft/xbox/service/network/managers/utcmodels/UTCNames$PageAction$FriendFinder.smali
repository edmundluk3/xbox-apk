.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$FriendFinder;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FriendFinder"
.end annotation


# static fields
.field public static final ContactsAddFriends:Ljava/lang/String; = "Friend Finder - Contacts Add Friends"

.field public static final ContactsCallMe:Ljava/lang/String; = "Friend Finder - Contacts Call Me"

.field public static final ContactsCancel:Ljava/lang/String; = "Friend Finder - Contacts Cancel"

.field public static final ContactsChangeRegion:Ljava/lang/String; = "Friend Finder - Contacts Change Region"

.field public static final ContactsClose:Ljava/lang/String; = "Friend Finder - Contacts Close"

.field public static final ContactsDontAsk:Ljava/lang/String; = "Friend Finder - Contacts Dont Ask"

.field public static final ContactsLink:Ljava/lang/String; = "Friend Finder - Contacts Link"

.field public static final ContactsNext:Ljava/lang/String; = "Friend Finder - Contacts Next"

.field public static final ContactsOK:Ljava/lang/String; = "Friend Finder - Contacts OK"

.field public static final ContactsOptOut:Ljava/lang/String; = "Friend Finder - Contacts Opt Out"

.field public static final ContactsResendCode:Ljava/lang/String; = "Friend Finder - Contacts Resend Code"

.field public static final ContactsSendInvite:Ljava/lang/String; = "Friend Finder - Contacts Send Invite"

.field public static final ContactsShowInvite:Ljava/lang/String; = "Friend Finder - Contacts Show Invite"

.field public static final ContactsSkip:Ljava/lang/String; = "Friend Finder - Contacts Skip"

.field public static final ContactsUnlink:Ljava/lang/String; = "Friend Finder - Contacts Unlink"

.field public static final ContactsUnlinkConfirmed:Ljava/lang/String; = "Friend Finder - Contacts Unlink Confirmed"

.field public static final ContactsVerifyPhone:Ljava/lang/String; = "Friend Finder - Contacts Verify Phone"

.field public static final FacebookOptOut:Ljava/lang/String; = "Friend Finder - Facebook Opt Out"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
