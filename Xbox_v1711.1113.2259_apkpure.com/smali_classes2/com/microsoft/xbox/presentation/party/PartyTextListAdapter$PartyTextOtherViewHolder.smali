.class public Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PartyTextListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PartyTextOtherViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
        ">;"
    }
.end annotation


# instance fields
.field arrow:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e049e
    .end annotation
.end field

.field gamertag:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e049c
    .end annotation
.end field

.field realname:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e049d
    .end annotation
.end field

.field text:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e04a0
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

.field timeSizer:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0499
    .end annotation
.end field

.field timeStamp:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e049a
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    .line 151
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 152
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->timeSizer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->arrow:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 156
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;)V
    .locals 3
    .param p1, "partyMessage"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 160
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->gamertag:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->gamertag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->this$0:Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->access$000(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;I)Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "timeStamp":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 164
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->timeStamp:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->text:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->message()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    return-void

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->timeStamp:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 167
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->timeStamp:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 130
    check-cast p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;->onBind(Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;)V

    return-void
.end method
