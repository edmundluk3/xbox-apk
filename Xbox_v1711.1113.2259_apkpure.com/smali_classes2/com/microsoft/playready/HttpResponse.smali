.class Lcom/microsoft/playready/HttpResponse;
.super Ljava/lang/Object;
.source "HttpClient.java"


# instance fields
.field private mResponse:[B

.field private mStatusCode:I

.field private mStatusMessage:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;[B)V
    .locals 0
    .param p1, "statusCode"    # I
    .param p2, "statusMessage"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "response"    # [B

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput p1, p0, Lcom/microsoft/playready/HttpResponse;->mStatusCode:I

    .line 41
    iput-object p2, p0, Lcom/microsoft/playready/HttpResponse;->mStatusMessage:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/microsoft/playready/HttpResponse;->mUrl:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/microsoft/playready/HttpResponse;->mResponse:[B

    .line 44
    return-void
.end method


# virtual methods
.method public getResponse()[B
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/playready/HttpResponse;->mResponse:[B

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/microsoft/playready/HttpResponse;->mStatusCode:I

    return v0
.end method

.method public getStatusMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/playready/HttpResponse;->mStatusMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/playready/HttpResponse;->mUrl:Ljava/lang/String;

    return-object v0
.end method
