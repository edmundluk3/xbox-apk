.class public Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "XboxAuthActivityAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$EnvironmentSelectorListener;
    }
.end annotation


# instance fields
.field private enviromentSelectButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private loginButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private logoView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;

.field private welcomeArea:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;

    .line 43
    const v0, 0x7f0e0780

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->welcomeArea:Landroid/view/View;

    .line 44
    const v0, 0x7f0e0783

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->loginButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 45
    const v0, 0x7f0e0781

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->logoView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->loginButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->loginButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->enviromentSelectButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onDestroy()V

    .line 96
    const-string v0, "XboxAuthActivityAdapter"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->loginButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->requestFocus()Z

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->loginButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    return-void
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->loginButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;->welcomeArea:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 90
    return-void
.end method
