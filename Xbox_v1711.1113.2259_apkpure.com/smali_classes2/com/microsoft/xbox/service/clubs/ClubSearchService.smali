.class public final enum Lcom/microsoft/xbox/service/clubs/ClubSearchService;
.super Ljava/lang/Enum;
.source "ClubSearchService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/clubs/IClubSearchService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubSearchService;",
        ">;",
        "Lcom/microsoft/xbox/service/clubs/IClubSearchService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubSearchService;

.field private static final COMMMA_DELIMINATOR:Ljava/lang/String; = ","

.field private static final CONTRACT_VERSION:I = 0x1

.field private static final COUNT_PARAM:Ljava/lang/String; = "count="

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubSearchService;

.field private static final PARAM_DELIMINATOR:Ljava/lang/String; = "&"

.field private static final QUERY_PARAM:Ljava/lang/String; = "q="

.field private static final RESULT_COUNT:I = 0x1e

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final SUGGESTIONS_BASE_ENDPOINT:Ljava/lang/String; = "https://clubsearch.xboxlive.com/suggest?"

.field private static final TAG:Ljava/lang/String;

.field private static final TAGS_PARAM:Ljava/lang/String; = "tags="

.field private static final TITLES_PARAM:Ljava/lang/String; = "titles="


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubSearchService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    .line 23
    new-array v0, v3, [Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    .line 27
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->TAG:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->STATIC_HEADERS:Ljava/util/List;

    .line 46
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 64
    .local p2, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 66
    .local v0, "urlBuilder":Ljava/lang/StringBuilder;
    const-string v1, "count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 69
    if-eqz p1, :cond_0

    .line 70
    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    const-string v1, "q="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    :cond_0
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 76
    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    const-string v1, "tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string v1, ","

    invoke-static {v1, p2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    :cond_1
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 82
    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    const-string v1, "titles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    const-string v1, ","

    invoke-static {v1, p3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static synthetic lambda$getSuggestions$0(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 4
    .param p0, "params"    # Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    const-string v0, "https://clubsearch.xboxlive.com/suggest?"

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->query()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->titles()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->getUrl(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->STATIC_HEADERS:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubSearchService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubSearchService;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubSearchService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubSearchService;

    return-object v0
.end method


# virtual methods
.method public getSuggestions(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;
    .locals 3
    .param p1, "params"    # Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 54
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubSearchService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSuggestions(params:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 56
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 58
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchService$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;

    return-object v0
.end method
