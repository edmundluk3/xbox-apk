.class public Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;
.super Landroid/widget/RelativeLayout;
.source "TvStreamerProgressBar.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar$IStreamerProgressBarOnSeekListener;
    }
.end annotation


# static fields
.field private static final EPOCH_CONVERSION_MILLIS:J = 0xa9730b66800L

.field private static HNS_IN_SEC:J = 0x0L

.field private static HOUR_IN_MILLIS:J = 0x0L

.field private static final TAG:Ljava/lang/String; = "TvStreamerProgressBar"


# instance fields
.field private mBufferSizeHns:J

.field private mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mCurrentTimeHns:J

.field private mCurrentTimeMillis:J

.field private mDurationInSec:J

.field private mEndTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mEndTimeHns:J

.field private mEndTimeMillis:J

.field private mEpochHns:J

.field private mSeekPerformed:Z

.field private mStartRoundUpTime:Ljava/util/Date;

.field private mStartTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mStartTimeHns:J

.field private mStartTimeMillis:J

.field private mStreamerSeekBar:Landroid/widget/SeekBar;

.field private mTimeline:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const-wide/32 v0, 0x36ee80

    sput-wide v0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->HOUR_IN_MILLIS:J

    .line 43
    const-wide/32 v0, 0x989680

    sput-wide v0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->HNS_IN_SEC:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->init(Landroid/content/Context;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->init(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->init(Landroid/content/Context;)V

    .line 81
    return-void
.end method

.method private getMillisFromHns(J)J
    .locals 5
    .param p1, "timeInHns"    # J

    .prologue
    .line 183
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEpochHns:J

    add-long/2addr v0, p1

    const-wide/16 v2, 0x2710

    div-long/2addr v0, v2

    const-wide v2, 0xa9730b66800L

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private getRoundDownTime(J)Ljava/util/Date;
    .locals 7
    .param p1, "exactTime"    # J

    .prologue
    const/16 v5, 0xc

    const/4 v3, 0x0

    .line 158
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 159
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 160
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 161
    .local v2, "unroundedMinutes":I
    div-int/lit8 v4, v2, 0x1e

    if-lez v4, :cond_0

    const/4 v1, 0x1

    .line 162
    .local v1, "passHalf":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 163
    const/16 v4, 0x1e

    invoke-virtual {v0, v5, v4}, Ljava/util/Calendar;->set(II)V

    .line 167
    :goto_1
    const/16 v4, 0xd

    invoke-virtual {v0, v4, v3}, Ljava/util/Calendar;->set(II)V

    .line 168
    const/16 v4, 0xe

    invoke-virtual {v0, v4, v3}, Ljava/util/Calendar;->set(II)V

    .line 169
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    return-object v3

    .end local v1    # "passHalf":Z
    :cond_0
    move v1, v3

    .line 161
    goto :goto_0

    .line 165
    .restart local v1    # "passHalf":Z
    :cond_1
    invoke-virtual {v0, v5, v3}, Ljava/util/Calendar;->set(II)V

    goto :goto_1
.end method

.method private getTimeText(Ljava/util/Date;)Ljava/lang/String;
    .locals 3
    .param p1, "time"    # Ljava/util/Date;

    .prologue
    .line 173
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm aa"

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 85
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f03024b

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 87
    const v1, 0x7f0e0b4b

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    .line 88
    const v1, 0x7f0e0b4a

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mTimeline:Landroid/widget/ProgressBar;

    .line 89
    const v1, 0x7f0e0b4c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 90
    const v1, 0x7f0e0b4d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 91
    const v1, 0x7f0e0b4e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 92
    return-void
.end method

.method private seekToCurrent()V
    .locals 4

    .prologue
    .line 220
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 221
    .local v0, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "payload"

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    const-string v2, "tuner"

    const-string v3, "btn.seek"

    invoke-virtual {v1, v2, v3, v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->sendButton(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z

    .line 223
    return-void
.end method


# virtual methods
.method public getSeekPerformed()Z
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mSeekPerformed:Z

    return v0
.end method

.method public initialize(JJJJJ)V
    .locals 5
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J
    .param p5, "currentTime"    # J
    .param p7, "epoch"    # J
    .param p9, "maxBufferSize"    # J

    .prologue
    .line 95
    const-string v0, "TvStreamerProgressBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Updating scrub bar data: ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") @ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", EPOCH "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7, p8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", MAX "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p9, p10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeHns:J

    .line 98
    iput-wide p3, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTimeHns:J

    .line 99
    iput-wide p5, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    .line 100
    iput-wide p7, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEpochHns:J

    .line 101
    iput-wide p9, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mBufferSizeHns:J

    .line 104
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeHns:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 105
    const-string v0, "TvStreamerProgressBar"

    const-string v1, "Current time from console less than start time"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeHns:J

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    .line 112
    :cond_0
    :goto_0
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeHns:J

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getMillisFromHns(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeMillis:J

    .line 113
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTimeHns:J

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getMillisFromHns(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTimeMillis:J

    .line 114
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getMillisFromHns(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeMillis:J

    .line 115
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTimeMillis:J

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeMillis:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mDurationInSec:J

    .line 116
    return-void

    .line 107
    :cond_1
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTimeHns:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 108
    const-string v0, "TvStreamerProgressBar"

    const-string v1, "Current time from console greater than than end time"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTimeHns:J

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    goto :goto_0
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 1
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 232
    if-nez p1, :cond_0

    .line 233
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->setVisibility(I)V

    .line 235
    :cond_0
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    .line 228
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 240
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 245
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 120
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 123
    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeMillis:J

    invoke-direct {p0, v4, v5}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getRoundDownTime(J)Ljava/util/Date;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartRoundUpTime:Ljava/util/Date;

    .line 124
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartRoundUpTime:Ljava/util/Date;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getTimeText(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 128
    .local v0, "calendar":Ljava/util/Calendar;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartRoundUpTime:Ljava/util/Date;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 129
    const/16 v4, 0xa

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->roll(II)V

    .line 130
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getTimeText(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mBufferSizeHns:J

    sget-wide v8, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->HNS_IN_SEC:J

    div-long/2addr v6, v8

    long-to-int v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setMax(I)V

    .line 134
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mDurationInSec:J

    long-to-int v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 135
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeMillis:J

    iget-wide v8, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeMillis:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 136
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 139
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 140
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mTimeline:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 141
    new-instance v4, Ljava/util/Date;

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeMillis:J

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    long-to-double v4, v4

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartRoundUpTime:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    long-to-double v6, v6

    sub-double/2addr v4, v6

    sget-wide v6, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->HOUR_IN_MILLIS:J

    long-to-double v6, v6

    div-double v2, v4, v6

    .line 142
    .local v2, "percentage":D
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mTimeline:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getWidth()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v2

    double-to-int v4, v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mTimeline:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getLeft()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 143
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v1}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    new-instance v5, Ljava/util/Date;

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeMillis:J

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getTimeText(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 148
    .restart local v1    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mTimeline:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getWidth()I

    move-result v4

    int-to-long v4, v4

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeMillis:J

    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartRoundUpTime:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    mul-long/2addr v4, v6

    sget-wide v6, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->HOUR_IN_MILLIS:J

    div-long/2addr v4, v6

    long-to-int v4, v4

    iput v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 149
    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 250
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getSecondaryProgress()I

    move-result v1

    if-le p2, v1, :cond_0

    .line 251
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getSecondaryProgress()I

    move-result p2

    .line 252
    invoke-virtual {p1, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 254
    :cond_0
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeHns:J

    int-to-long v4, p2

    sget-wide v6, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->HNS_IN_SEC:J

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    .line 255
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getMillisFromHns(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeMillis:J

    .line 256
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeMillis:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getTimeText(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 258
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mTimeline:Landroid/widget/ProgressBar;

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getWidth()I

    move-result v1

    int-to-long v2, v1

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeMillis:J

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartRoundUpTime:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    mul-long/2addr v2, v4

    sget-wide v4, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->HOUR_IN_MILLIS:J

    div-long/2addr v2, v4

    long-to-int v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 259
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 260
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 264
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Stream FullScreen Scrubber"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 265
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mSeekPerformed:Z

    .line 270
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Stream Seek"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->beginTrackPerformance(Ljava/lang/String;)V

    .line 271
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->seekToCurrent()V

    .line 272
    return-void
.end method

.method public seek(I)V
    .locals 8
    .param p1, "offset"    # I

    .prologue
    const-wide/16 v6, 0x0

    .line 192
    if-nez p1, :cond_0

    .line 210
    :goto_0
    return-void

    .line 196
    :cond_0
    int-to-long v2, p1

    sget-wide v4, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->HNS_IN_SEC:J

    mul-long v0, v2, v4

    .line 197
    .local v0, "offsetHns":J
    cmp-long v2, v0, v6

    if-gez v2, :cond_1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 198
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStartTimeHns:J

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    .line 199
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 207
    :goto_1
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->getMillisFromHns(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeMillis:J

    .line 209
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->seekToCurrent()V

    goto :goto_0

    .line 200
    :cond_1
    cmp-long v2, v0, v6

    if-lez v2, :cond_2

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    add-long/2addr v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTimeHns:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 201
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mEndTimeHns:J

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    .line 202
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getSecondaryProgress()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1

    .line 204
    :cond_2
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mCurrentTimeHns:J

    .line 205
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    add-int/2addr v3, p1

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1
.end method

.method public setSeekPerformed(Z)V
    .locals 0
    .param p1, "performed"    # Z

    .prologue
    .line 279
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mSeekPerformed:Z

    .line 280
    return-void
.end method

.method public showLive()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getSecondaryProgress()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getSecondaryProgress()I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->mStreamerSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/TvStreamerProgressBar;->seek(I)V

    .line 216
    :cond_0
    return-void
.end method
