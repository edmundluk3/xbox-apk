.class public Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "GameProfileLfgScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method


# virtual methods
.method public getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getHeader()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;->getHeader()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0706e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 22
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 23
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;->onCreateContentView()V

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 27
    .local v0, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 28
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;->setAsPivotPane()V

    .line 29
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f030126

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameProfileLfgScreen;->setContentView(I)V

    .line 34
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method
