.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club;
.super Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;
.source "AutoValue_ClubHubDataTypes_Club.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(JLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;JJJJJJJJLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;Ljava/util/Date;JJJJ)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "profile"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
    .param p4, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
    .param p5, "shortName"    # Ljava/lang/String;
    .param p6, "ownerXuid"    # Ljava/lang/String;
    .param p7, "founderXuid"    # J
    .param p9, "creationDateUtc"    # Ljava/lang/String;
    .param p10, "glyphImageUrl"    # Ljava/lang/String;
    .param p11, "bannerImageUrl"    # Ljava/lang/String;
    .param p12, "settings"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .param p13, "followersCount"    # J
    .param p15, "membersCount"    # J
    .param p17, "moderatorsCount"    # J
    .param p19, "recommendedCount"    # J
    .param p21, "requestedToJoinCount"    # J
    .param p23, "clubPresenceCount"    # J
    .param p25, "clubPresenceTodayCount"    # J
    .param p27, "clubPresenceInGameCount"    # J
    .param p29, "roster"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .param p30, "targetRoles"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
    .param p31, "recommendation"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    .param p33, "titleDeeplinks"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;
    .param p34, "state"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;
    .param p35, "suspendedUntilUtc"    # Ljava/util/Date;
    .param p36, "reportCount"    # J
    .param p38, "reportedItemsCount"    # J
    .param p40, "maxMembersPerClub"    # J
    .param p42, "maxMembersInGame"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;",
            "JJJJJJJJ",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;",
            "Ljava/util/Date;",
            "JJJJ)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p32, "clubPresence":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    invoke-direct/range {p0 .. p43}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;-><init>(JLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;JJJJJJJJLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;Ljava/util/Date;JJJJ)V

    .line 30
    return-void
.end method
