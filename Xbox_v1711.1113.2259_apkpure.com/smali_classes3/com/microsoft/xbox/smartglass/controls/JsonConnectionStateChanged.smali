.class Lcom/microsoft/xbox/smartglass/controls/JsonConnectionStateChanged;
.super Lorg/json/JSONObject;
.source "JsonConnectionStateChanged.java"


# direct methods
.method public constructor <init>(ZZ)V
    .locals 1
    .param p1, "isConnected"    # Z
    .param p2, "isUsingLocalConnection"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 12
    const-string v0, "isUsingLocalConnection"

    invoke-virtual {p0, v0, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonConnectionStateChanged;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 13
    const-string v0, "isConnected"

    invoke-virtual {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/JsonConnectionStateChanged;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 14
    return-void
.end method
