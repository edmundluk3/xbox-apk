.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FriendsWhoPlayed"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;
    }
.end annotation


# instance fields
.field public final currentlyPlayingCount:I

.field public final havePlayedCount:I

.field private final people:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IILjava/util/List;)V
    .locals 1
    .param p1, "currentlyPlayingCount"    # I
    .param p2, "havePlayedCount"    # I
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 479
    .local p3, "people":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 480
    iput p1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;->currentlyPlayingCount:I

    .line 481
    iput p2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;->havePlayedCount:I

    .line 482
    if-nez p3, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;->people:Ljava/util/List;

    .line 483
    return-void

    .line 482
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method


# virtual methods
.method public getPeople()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;",
            ">;"
        }
    .end annotation

    .prologue
    .line 487
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;->people:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method
