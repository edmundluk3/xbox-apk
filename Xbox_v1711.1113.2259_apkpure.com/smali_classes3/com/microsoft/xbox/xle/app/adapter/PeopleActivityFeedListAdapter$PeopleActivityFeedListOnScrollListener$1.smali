.class Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;
.super Landroid/os/CountDownTimer;
.source "PeopleActivityFeedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;

.field final synthetic val$recyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;JJLandroid/support/v7/widget/RecyclerView;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 283
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;

    iput-object p6, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->val$recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 8

    .prologue
    .line 288
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->val$recyclerView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->access$000(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 289
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->val$recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v4}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v0

    .line 290
    .local v0, "firstLocation":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->val$recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v4}, Landroid/support/v7/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v2

    .line 291
    .local v2, "lastLocation":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v3, "profileRecentItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    move v1, v0

    .local v1, "i":I
    :goto_0
    if-gt v1, v2, :cond_1

    .line 293
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getItem(I)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 294
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter;->getItem(I)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 298
    const-string v4, "Home"

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->val$recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    int-to-long v6, v5

    invoke-static {v4, v6, v7, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCImpression;->trackImpressionEvents(Ljava/lang/String;JLjava/util/ArrayList;)V

    .line 299
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener$1;->this$1:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;->access$002(Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapter$PeopleActivityFeedListOnScrollListener;Z)Z

    .line 303
    .end local v0    # "firstLocation":I
    .end local v1    # "i":I
    .end local v2    # "lastLocation":I
    .end local v3    # "profileRecentItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    :cond_2
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 285
    return-void
.end method
