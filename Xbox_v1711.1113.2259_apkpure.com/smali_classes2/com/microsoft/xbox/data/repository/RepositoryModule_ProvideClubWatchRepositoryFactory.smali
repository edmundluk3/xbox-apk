.class public final Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;
.super Ljava/lang/Object;
.source "RepositoryModule_ProvideClubWatchRepositoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final beamServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamService;",
            ">;"
        }
    .end annotation
.end field

.field private final clubModelManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;"
        }
    .end annotation
.end field

.field private final dataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaHubServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/RepositoryModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p2, "beamServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/beam/BeamService;>;"
    .local p3, "mediaHubServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;>;"
    .local p4, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;>;"
    .local p5, "clubModelManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;>;"
    .local p6, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    .line 41
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->beamServiceProvider:Ljavax/inject/Provider;

    .line 43
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->mediaHubServiceProvider:Ljavax/inject/Provider;

    .line 45
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->dataMapperProvider:Ljavax/inject/Provider;

    .line 47
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->clubModelManagerProvider:Ljavax/inject/Provider;

    .line 49
    sget-boolean v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 50
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->schedulerProvider:Ljavax/inject/Provider;

    .line 51
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 7
    .param p0, "module"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/RepositoryModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/beam/BeamService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "beamServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/beam/BeamService;>;"
    .local p2, "mediaHubServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;>;"
    .local p3, "dataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;>;"
    .local p4, "clubModelManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;>;"
    .local p5, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;-><init>(Lcom/microsoft/xbox/data/repository/RepositoryModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideClubWatchRepository(Lcom/microsoft/xbox/data/repository/RepositoryModule;Lcom/microsoft/xbox/data/service/beam/BeamService;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/repository/RepositoryModule;
    .param p1, "beamService"    # Lcom/microsoft/xbox/data/service/beam/BeamService;
    .param p2, "mediaHubService"    # Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    .param p3, "dataMapper"    # Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;
    .param p4, "clubModelManager"    # Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;
    .param p5, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .prologue
    .line 92
    invoke-virtual/range {p0 .. p5}, Lcom/microsoft/xbox/data/repository/RepositoryModule;->provideClubWatchRepository(Lcom/microsoft/xbox/data/service/beam/BeamService;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;
    .locals 6

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->module:Lcom/microsoft/xbox/data/repository/RepositoryModule;

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->beamServiceProvider:Ljavax/inject/Provider;

    .line 57
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/data/service/beam/BeamService;

    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->mediaHubServiceProvider:Ljavax/inject/Provider;

    .line 58
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    iget-object v3, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->dataMapperProvider:Ljavax/inject/Provider;

    .line 59
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;

    iget-object v4, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->clubModelManagerProvider:Ljavax/inject/Provider;

    .line 60
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;

    iget-object v5, p0, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->schedulerProvider:Ljavax/inject/Provider;

    .line 61
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 56
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/data/repository/RepositoryModule;->provideClubWatchRepository(Lcom/microsoft/xbox/data/service/beam/BeamService;Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 55
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/RepositoryModule_ProvideClubWatchRepositoryFactory;->get()Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepository;

    move-result-object v0

    return-object v0
.end method
