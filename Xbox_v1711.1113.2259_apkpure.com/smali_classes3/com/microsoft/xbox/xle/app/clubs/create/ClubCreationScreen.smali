.class public Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "ClubCreationScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    .line 10
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const-string v0, "Clubs - Create Club view"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 14
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 15
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreen;->onCreateContentView()V

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 17
    return-void
.end method

.method public onCreateContentView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    const v0, 0x7f030074

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreen;->setContentView(I)V

    .line 23
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreen;->setShowUtilityBar(Z)V

    .line 24
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreen;->setShowRightPane(Z)V

    .line 25
    return-void
.end method
