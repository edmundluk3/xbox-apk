.class Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
.super Ljava/lang/Object;
.source "EPGIteratorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FetchState"
.end annotation


# instance fields
.field public channel:I

.field public time:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "channel"    # I
    .param p2, "time"    # I

    .prologue
    .line 429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 430
    iput p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->channel:I

    .line 431
    iput p2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->time:I

    .line 432
    return-void
.end method


# virtual methods
.method protected clone()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    .locals 3

    .prologue
    .line 436
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    iget v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->channel:I

    iget v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->time:I

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;-><init>(II)V

    return-object v0
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->clone()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 441
    if-nez p1, :cond_1

    .line 454
    :cond_0
    :goto_0
    return v2

    .line 445
    :cond_1
    if-ne p0, p1, :cond_2

    move v2, v1

    .line 446
    goto :goto_0

    .line 449
    :cond_2
    instance-of v3, p1, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 453
    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    .line 454
    .local v0, "f":Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    iget v3, v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->channel:I

    iget v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->channel:I

    if-ne v3, v4, :cond_3

    iget v3, v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->time:I

    iget v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->time:I

    if-ne v3, v4, :cond_3

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method
