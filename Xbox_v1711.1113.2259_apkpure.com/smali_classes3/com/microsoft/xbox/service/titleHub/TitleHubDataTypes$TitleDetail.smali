.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TitleDetail"
.end annotation


# instance fields
.field public final description:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final developerName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final publisherName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final releaseDate:Ljava/util/Date;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final shortDescription:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final vuiDisplayName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "developerName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "publisherName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "releaseDate"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "shortDescription"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "vuiDisplayName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 324
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->description:Ljava/lang/String;

    .line 325
    iput-object p2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->developerName:Ljava/lang/String;

    .line 326
    iput-object p3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->publisherName:Ljava/lang/String;

    .line 327
    iput-object p4, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->releaseDate:Ljava/util/Date;

    .line 328
    iput-object p5, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->shortDescription:Ljava/lang/String;

    .line 329
    iput-object p6, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->vuiDisplayName:Ljava/lang/String;

    .line 330
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 334
    if-ne p1, p0, :cond_1

    .line 340
    :cond_0
    :goto_0
    return v1

    .line 336
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    if-nez v3, :cond_2

    move v1, v2

    .line 337
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 339
    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;

    .line 340
    .local v0, "other":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->description:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->description:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->developerName:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->developerName:Ljava/lang/String;

    .line 341
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->publisherName:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->publisherName:Ljava/lang/String;

    .line 342
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->releaseDate:Ljava/util/Date;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->releaseDate:Ljava/util/Date;

    .line 343
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->shortDescription:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->shortDescription:Ljava/lang/String;

    .line 344
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->vuiDisplayName:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->vuiDisplayName:Ljava/lang/String;

    .line 345
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 351
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    if-nez v0, :cond_0

    .line 352
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    .line 353
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->description:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    .line 354
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->developerName:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    .line 355
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->publisherName:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    .line 356
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->releaseDate:Ljava/util/Date;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    .line 357
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->shortDescription:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    .line 358
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->vuiDisplayName:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    .line 361
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleDetail;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 366
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
