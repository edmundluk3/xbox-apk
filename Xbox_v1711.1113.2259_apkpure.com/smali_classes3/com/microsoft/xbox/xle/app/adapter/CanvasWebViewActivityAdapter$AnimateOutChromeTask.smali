.class Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;
.super Ljava/lang/Object;
.source "CanvasWebViewActivityAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimateOutChromeTask"
.end annotation


# static fields
.field private static final TIMER_DELAY:I = 0xbb8


# instance fields
.field private started:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;)V
    .locals 1

    .prologue
    .line 470
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->this$0:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 471
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->timer:Ljava/util/Timer;

    .line 472
    return-void
.end method


# virtual methods
.method public isStarted()Z
    .locals 1

    .prologue
    .line 495
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->started:Z

    return v0
.end method

.method public start()V
    .locals 4

    .prologue
    .line 475
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->started:Z

    if-nez v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 483
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->started:Z

    .line 485
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 488
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->started:Z

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 490
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$AnimateOutChromeTask;->started:Z

    .line 492
    :cond_0
    return-void
.end method
