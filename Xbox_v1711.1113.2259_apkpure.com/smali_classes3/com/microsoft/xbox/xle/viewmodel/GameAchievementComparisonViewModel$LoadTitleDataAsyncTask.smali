.class Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameAchievementComparisonViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadTitleDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$1;

    .prologue
    .line 458
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 4

    .prologue
    .line 462
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 463
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->shouldRefresh(J)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 490
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 485
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 468
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 469
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 470
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 480
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 481
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 458
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 474
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 475
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->access$602(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;Z)Z

    .line 476
    return-void
.end method
