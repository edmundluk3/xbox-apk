.class public Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;
.super Landroid/widget/FrameLayout;
.source "URCViewControl.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "URCViewControl"


# instance fields
.field private alphaAnimator:Landroid/animation/ValueAnimator;

.field private mainView:Landroid/view/View;

.field private scrollValueAnimator:Landroid/animation/ValueAnimator;

.field private subTextView:Landroid/widget/TextView;

.field private textView:Landroid/widget/TextView;

.field private urcStatusCannotControlSource:Ljava/lang/String;

.field private urcStatusConsoleSetupDirectionsSTB:Ljava/lang/String;

.field private urcStatusDisconnect:Ljava/lang/String;

.field private urcStatusError:Ljava/lang/String;

.field private urcStatusFirstRun:Ljava/lang/String;

.field private urcViewControlId:I

.field private willDisplaySTBInstructions:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->willDisplaySTBInstructions:Z

    .line 44
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->scrollValueAnimator:Landroid/animation/ValueAnimator;

    .line 45
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->alphaAnimator:Landroid/animation/ValueAnimator;

    .line 49
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->willDisplaySTBInstructions:Z

    .line 44
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->scrollValueAnimator:Landroid/animation/ValueAnimator;

    .line 45
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->alphaAnimator:Landroid/animation/ValueAnimator;

    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->willDisplaySTBInstructions:Z

    .line 44
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->scrollValueAnimator:Landroid/animation/ValueAnimator;

    .line 45
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->alphaAnimator:Landroid/animation/ValueAnimator;

    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->scrollValueAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->alphaAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v7, -0x1

    .line 64
    sget-object v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCViewControl:[I

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 65
    .local v0, "array":Landroid/content/res/TypedArray;
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    .line 66
    .local v1, "branchSession":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    sget v5, Lcom/microsoft/xbox/xle/urc/R$layout;->urc_view_control:I

    iput v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcViewControlId:I

    .line 67
    sget v5, Lcom/microsoft/xbox/xle/urc/R$string;->URC_ConnectionError_Remote:I

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusDisconnect:Ljava/lang/String;

    .line 68
    sget v5, Lcom/microsoft/xbox/xle/urc/R$string;->URC_Error_TvListings_Settings:I

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusError:Ljava/lang/String;

    .line 69
    sget v5, Lcom/microsoft/xbox/xle/urc/R$string;->URC_Error_Remote_Console_Setup:I

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusFirstRun:Ljava/lang/String;

    .line 70
    sget v5, Lcom/microsoft/xbox/xle/urc/R$string;->URC_Error_Console_Setup_Directions_STB:I

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusConsoleSetupDirectionsSTB:Ljava/lang/String;

    .line 71
    sget v5, Lcom/microsoft/xbox/xle/urc/R$string;->Streaming_Remote_Cannot_Control_Source:I

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusCannotControlSource:Ljava/lang/String;

    .line 72
    if-eqz v0, :cond_3

    .line 75
    :try_start_0
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCViewControl_urcViewControlId:I

    iget v6, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcViewControlId:I

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    iput v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcViewControlId:I

    .line 77
    const/4 v3, 0x0

    .line 79
    .local v3, "val":Ljava/lang/String;
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCViewControl_urcStatusDisconnectStringId:I

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 80
    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusDisconnect:Ljava/lang/String;

    .end local v3    # "val":Ljava/lang/String;
    :cond_0
    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusDisconnect:Ljava/lang/String;

    .line 81
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCViewControl_urcStatusErrorStringId:I

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 82
    .restart local v3    # "val":Ljava/lang/String;
    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusError:Ljava/lang/String;

    .end local v3    # "val":Ljava/lang/String;
    :cond_1
    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusError:Ljava/lang/String;

    .line 83
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCViewControl_urcStatusFirstRunStringId:I

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 84
    .restart local v3    # "val":Ljava/lang/String;
    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusFirstRun:Ljava/lang/String;

    .end local v3    # "val":Ljava/lang/String;
    :cond_2
    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusFirstRun:Ljava/lang/String;

    .line 85
    sget v5, Lcom/microsoft/xbox/xle/urc/R$styleable;->URCViewControl_urcStatusFirstRunSubTextStringId:I

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 86
    .restart local v3    # "val":Ljava/lang/String;
    iput-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusConsoleSetupDirectionsSTB:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 93
    .end local v3    # "val":Ljava/lang/String;
    :cond_3
    const-string v5, "layout_inflater"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 94
    .local v4, "vi":Landroid/view/LayoutInflater;
    iget v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcViewControlId:I

    const/4 v6, 0x1

    invoke-virtual {v4, v5, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 96
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 97
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    sget v5, Lcom/microsoft/xbox/xle/urc/R$id;->urc_main_container:I

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->mainView:Landroid/view/View;

    .line 100
    sget v5, Lcom/microsoft/xbox/xle/urc/R$id;->urc_status:I

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    .line 101
    sget v5, Lcom/microsoft/xbox/xle/urc/R$id;->urc_sub_status:I

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    .line 103
    iget-object v5, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->mainView:Landroid/view/View;

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->setNarratorContent(Landroid/view/View;)V

    .line 105
    return-void

    .line 88
    .end local v2    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "vi":Landroid/view/LayoutInflater;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v5
.end method

.method private setNarratorContent(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 362
    if-nez p1, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    sget v2, Lcom/microsoft/xbox/xle/urc/R$id;->btn_power:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 368
    .local v1, "btnPower":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 369
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_power:I

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 371
    :cond_2
    sget v2, Lcom/microsoft/xbox/xle/urc/R$id;->btn_more:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 372
    .local v0, "btnMore":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 373
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    sget v3, Lcom/microsoft/xbox/xle/urc/R$string;->narrator_btn_more:I

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->resolveString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateErrorState([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 6
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 263
    if-eqz p1, :cond_0

    .line 267
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 269
    aget-object v1, p1, v0

    .line 271
    .local v1, "info":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    iget-object v2, v1, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    const-string v3, "stb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 272
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->willDisplaySTBInstructions:Z

    .line 280
    .end local v0    # "i":I
    .end local v1    # "info":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getIsInHdmiMode()Z

    move-result v2

    if-nez v2, :cond_3

    .line 281
    const-string v2, "URCViewControl"

    const-string v3, "USB tuner found"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusCannotControlSource:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->mainView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 284
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 287
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 308
    :cond_1
    :goto_1
    return-void

    .line 267
    .restart local v0    # "i":I
    .restart local v1    # "info":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 289
    .end local v0    # "i":I
    .end local v1    # "info":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    :cond_3
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->willDisplaySTBInstructions:Z

    if-eqz v2, :cond_4

    .line 290
    const-string v2, "URCViewControl"

    const-string v3, "No connected STB found"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusFirstRun:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->mainView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 294
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 296
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 297
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusConsoleSetupDirectionsSTB:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 301
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->mainView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 302
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 304
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 305
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private updateViewState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 220
    sget-object v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$2;->$SwitchMap$com$microsoft$xbox$xle$urc$net$IBranchConnection$ConnectionState:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusDisconnect:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->mainView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 234
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->mainView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 243
    :pswitch_1
    if-nez p2, :cond_1

    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->urcStatusError:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->mainView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->subTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 257
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getDevices()[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->updateErrorState([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V

    goto :goto_0

    .line 220
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public cancelFirstRunAnimation()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->scrollValueAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->scrollValueAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->alphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->alphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 217
    :cond_1
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 312
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 313
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 315
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;)V

    .line 317
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestLiveTvInfo()Z

    .line 318
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 319
    return-void
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 0
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->updateErrorState([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V

    .line 340
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    .line 334
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->updateViewState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V

    .line 335
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 323
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 325
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 326
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 327
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 328
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ILiveTvInfoListener;)V

    .line 330
    :cond_0
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 345
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 350
    return-void
.end method

.method public onLiveTvInfoReceived(Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;

    .prologue
    .line 354
    if-eqz p1, :cond_0

    .line 355
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne v0, v1, :cond_0

    .line 356
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->updateViewState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V

    .line 359
    :cond_0
    return-void
.end method

.method public performFirstRunAnimation(Landroid/widget/ScrollView;Landroid/animation/TimeInterpolator;J)V
    .locals 7
    .param p1, "scrollView"    # Landroid/widget/ScrollView;
    .param p2, "interpolator"    # Landroid/animation/TimeInterpolator;
    .param p3, "timeDelay"    # J

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->willDisplaySTBInstructions:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getIsInHdmiMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 112
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/widget/ScrollView;->setAlpha(F)V

    .line 206
    :goto_0
    return-void

    .line 116
    :cond_1
    if-eqz p1, :cond_2

    .line 117
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 118
    new-instance v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;Landroid/widget/ScrollView;Landroid/animation/TimeInterpolator;J)V

    invoke-virtual {p1, v0}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 201
    invoke-virtual {p1}, Landroid/widget/ScrollView;->postInvalidate()V

    goto :goto_0

    .line 204
    :cond_2
    const-string v0, "URCViewControl"

    const-string v1, "There was no scrollView to animate on firstrun!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
