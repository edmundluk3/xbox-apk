.class Lcom/facebook/network/connectionclass/ByteArrayScanner;
.super Ljava/lang/Object;
.source "ByteArrayScanner.java"


# instance fields
.field private mCurrentOffset:I

.field private mData:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mDelimiter:C

.field private mDelimiterSet:Z

.field private mTotalLength:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private advance()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->throwIfNotReset()V

    .line 116
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->throwIfDelimiterNotSet()V

    .line 117
    iget v3, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mTotalLength:I

    iget v4, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    if-gt v3, v4, :cond_0

    .line 118
    new-instance v3, Ljava/util/NoSuchElementException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Reading past end of input stream at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 120
    :cond_0
    iget-object v3, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mData:[B

    iget v4, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    iget v5, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mTotalLength:I

    iget-char v6, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mDelimiter:C

    invoke-static {v3, v4, v5, v6}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->indexOf([BIIC)I

    move-result v0

    .line 125
    .local v0, "index":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    .line 126
    iget v3, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mTotalLength:I

    iget v4, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    sub-int v1, v3, v4

    .line 127
    .local v1, "length":I
    iget v3, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mTotalLength:I

    iput v3, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    move v2, v1

    .line 132
    .end local v1    # "length":I
    .local v2, "length":I
    :goto_0
    return v2

    .line 130
    .end local v2    # "length":I
    :cond_1
    iget v3, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    sub-int v1, v0, v3

    .line 131
    .restart local v1    # "length":I
    add-int/lit8 v3, v0, 0x1

    iput v3, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    move v2, v1

    .line 132
    .end local v1    # "length":I
    .restart local v2    # "length":I
    goto :goto_0
.end method

.method private static indexOf([BIIC)I
    .locals 2
    .param p0, "data"    # [B
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "ch"    # C

    .prologue
    .line 152
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_1

    .line 153
    aget-byte v1, p0, v0

    if-ne v1, p3, :cond_0

    .line 157
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 152
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static parseInt([BII)I
    .locals 8
    .param p0, "buffer"    # [B
    .param p1, "start"    # I
    .param p2, "end"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .prologue
    .line 138
    const/16 v2, 0xa

    .line 139
    .local v2, "radix":I
    const/4 v3, 0x0

    .local v3, "result":I
    move v4, p1

    .line 140
    .end local p1    # "start":I
    .local v4, "start":I
    :goto_0
    if-ge v4, p2, :cond_2

    .line 141
    add-int/lit8 p1, v4, 0x1

    .end local v4    # "start":I
    .restart local p1    # "start":I
    aget-byte v5, p0, v4

    add-int/lit8 v0, v5, -0x30

    .line 142
    .local v0, "digit":I
    if-ltz v0, :cond_0

    const/16 v5, 0x9

    if-le v0, v5, :cond_1

    .line 143
    :cond_0
    new-instance v5, Ljava/lang/NumberFormatException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid int in buffer at "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, p1, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 145
    :cond_1
    mul-int v5, v3, v2

    add-int v1, v5, v0

    .line 146
    .local v1, "next":I
    move v3, v1

    move v4, p1

    .line 147
    .end local p1    # "start":I
    .restart local v4    # "start":I
    goto :goto_0

    .line 148
    .end local v0    # "digit":I
    .end local v1    # "next":I
    :cond_2
    return v3
.end method

.method private throwIfDelimiterNotSet()V
    .locals 2

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mDelimiterSet:Z

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call useDelimiter first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    return-void
.end method

.method private throwIfNotReset()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mData:[B

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call reset first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    return-void
.end method


# virtual methods
.method public nextInt()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->throwIfNotReset()V

    .line 92
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->throwIfDelimiterNotSet()V

    .line 93
    iget v1, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    .line 94
    .local v1, "offset":I
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->advance()I

    move-result v0

    .line 95
    .local v0, "length":I
    iget-object v3, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mData:[B

    add-int v4, v1, v0

    invoke-static {v3, v1, v4}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->parseInt([BII)I

    move-result v2

    .line 99
    .local v2, "value":I
    return v2
.end method

.method public nextString()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->throwIfNotReset()V

    .line 57
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->throwIfDelimiterNotSet()V

    .line 58
    iget v1, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    .line 59
    .local v1, "offset":I
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->advance()I

    move-result v0

    .line 60
    .local v0, "length":I
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mData:[B

    invoke-direct {v2, v3, v1, v0}, Ljava/lang/String;-><init>([BII)V

    return-object v2
.end method

.method public nextStringEquals(Ljava/lang/String;)Z
    .locals 6
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 71
    iget v2, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    .line 72
    .local v2, "offset":I
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->advance()I

    move-result v1

    .line 73
    .local v1, "length":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v4, v1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v3

    .line 76
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 77
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    iget-object v5, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mData:[B

    aget-byte v5, v5, v2

    if-ne v4, v5, :cond_0

    .line 80
    add-int/lit8 v2, v2, 0x1

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 82
    :cond_2
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public reset([BI)Lcom/facebook/network/connectionclass/ByteArrayScanner;
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "length"    # I

    .prologue
    const/4 v0, 0x0

    .line 24
    iput-object p1, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mData:[B

    .line 25
    iput v0, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mCurrentOffset:I

    .line 26
    iput p2, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mTotalLength:I

    .line 27
    iput-boolean v0, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mDelimiterSet:Z

    .line 28
    return-object p0
.end method

.method public skip()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->throwIfNotReset()V

    .line 109
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->throwIfDelimiterNotSet()V

    .line 110
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->advance()I

    .line 111
    return-void
.end method

.method public useDelimiter(C)Lcom/facebook/network/connectionclass/ByteArrayScanner;
    .locals 1
    .param p1, "delimiter"    # C

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/facebook/network/connectionclass/ByteArrayScanner;->throwIfNotReset()V

    .line 33
    iput-char p1, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mDelimiter:C

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/network/connectionclass/ByteArrayScanner;->mDelimiterSet:Z

    .line 35
    return-object p0
.end method
