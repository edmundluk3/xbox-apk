.class Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameDetailsProgressPhoneScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LikeClickAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

.field private newLikeState:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;ZLjava/lang/String;)V
    .locals 0
    .param p2, "gameClip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .param p3, "newLikeState"    # Z
    .param p4, "xuid"    # Ljava/lang/String;

    .prologue
    .line 374
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 375
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->newLikeState:Z

    .line 376
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 377
    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->xuid:Ljava/lang/String;

    .line 378
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 382
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 383
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 398
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->newLikeState:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->likeGameClip(ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 393
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 388
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 389
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 407
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->newLikeState:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->gameClip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    .line 408
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 369
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel$LikeClickAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 403
    return-void
.end method
