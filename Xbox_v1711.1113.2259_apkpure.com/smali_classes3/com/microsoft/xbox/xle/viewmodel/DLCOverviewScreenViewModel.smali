.class public Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "DLCOverviewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;",
        ">;"
    }
.end annotation


# instance fields
.field private isVmBroken:Z

.field private model:Lcom/microsoft/xbox/service/model/TitleModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->isVmBroken:Z

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getDLCOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 48
    return-void
.end method


# virtual methods
.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method public getConsumablePriceList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getMediaType()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getAvailabilities()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 157
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getDefaultParentalRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayListPrice()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getMsrp()Ljava/math/BigDecimal;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayPrice()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getListPrice()Ljava/math/BigDecimal;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 69
    const v0, 0x7f070683

    return v0
.end method

.method public getHasPurchased()Z
    .locals 2

    .prologue
    .line 163
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getStoreCollectionItem()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    move-result-object v0

    .line 165
    .local v0, "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    if-eqz v0, :cond_0

    .line 166
    const/4 v1, 0x1

    .line 170
    .end local v0    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIsAdult()Z
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method public getIsFree()Z
    .locals 2

    .prologue
    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getListPrice()Ljava/math/BigDecimal;

    move-result-object v0

    .line 120
    .local v0, "price":Ljava/math/BigDecimal;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getAvailabilityId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIsOwned()Z
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getHasPurchased()Z

    move-result v0

    return v0
.end method

.method public getLanguages()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getPublisher()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPurchaseButtonLabelText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getReleaseDate()Ljava/util/Date;

    move-result-object v0

    .line 100
    .local v0, "releaseDate":Ljava/util/Date;
    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070b01

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 104
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070aee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getPurchaseDate()Ljava/lang/String;
    .locals 8
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 175
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v4, :cond_1

    .line 176
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getStoreCollectionItem()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    move-result-object v1

    .line 177
    .local v1, "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    if-eqz v1, :cond_1

    .line 179
    iget-object v4, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->acquiredDate:Ljava/util/Date;

    if-eqz v4, :cond_1

    .line 180
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    iget-object v6, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->acquiredDate:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 181
    .local v2, "timeDiff":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    .line 182
    const/4 v4, 0x0

    .line 192
    .end local v1    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    .end local v2    # "timeDiff":J
    :goto_0
    return-object v4

    .line 185
    .restart local v1    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    .restart local v2    # "timeDiff":J
    :cond_0
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v4}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 186
    .local v0, "dateFormat":Ljava/text/DateFormat;
    iget-object v4, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;->acquiredDate:Ljava/util/Date;

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 192
    .end local v0    # "dateFormat":Ljava/text/DateFormat;
    .end local v1    # "storeCollectionItem":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    .end local v2    # "timeDiff":J
    :cond_1
    const-string v4, ""

    goto :goto_0
.end method

.method public getRatingDescriptors()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getRatingDescriptors()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRatingId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getRatingId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowPurchaseButton()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 134
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getIsAdult()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 152
    :goto_0
    return v0

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getMediaType()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromInt(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v0

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DConsumable:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    if-ne v0, v3, :cond_1

    move v0, v2

    .line 140
    goto :goto_0

    .line 144
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getIsOwned()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 145
    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getDisplayPrice()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 149
    goto :goto_0

    :cond_3
    move v0, v2

    .line 152
    goto :goto_0
.end method

.method public isViewModelBroken()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->isVmBroken:Z

    return v0
.end method

.method public launchDLC()V
    .locals 5

    .prologue
    .line 196
    const-string v2, "marketplace://deeplink?destination=details&id=%s"

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "launchUrl":Ljava/lang/String;
    const-wide/32 v2, 0x53e91323

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUriThenRemote(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V

    .line 198
    return-void
.end method

.method public launchHelp()V
    .locals 5

    .prologue
    .line 256
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getTitleId()J

    move-result-wide v2

    .line 257
    .local v2, "titleId":J
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getProductId()Ljava/lang/String;

    move-result-object v1

    .line 258
    .local v1, "productId":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "name":Ljava/lang/String;
    invoke-static {v2, v3, v1, v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchHelp(JLjava/lang/String;Ljava/lang/String;)V

    .line 260
    return-void
.end method

.method protected loadDetailsInBackGround(Z)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3
    .param p1, "forceLoad"    # Z

    .prologue
    .line 273
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 275
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 276
    .local v0, "loadGameDetailStatus":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->shouldRefresh()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 280
    :cond_0
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->IncludePurchaseFlow:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 282
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->loadParentItems(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 283
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 284
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->loadStoreCollectionItem(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 291
    :cond_1
    :goto_0
    return-object v0

    .line 286
    :cond_2
    const-string v1, "DLCOverviewScreenViewModel"

    const-string v2, "Invalid big cat id - cannot get purchase information"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getDLCOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 65
    return-void
.end method

.method protected onStartOverride()V
    .locals 4

    .prologue
    .line 52
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onStartOverride()V

    .line 56
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getTitleId()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->isVmBroken:Z

    goto :goto_0
.end method

.method public purchaseConsumableDLC(Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;)V
    .locals 9
    .param p1, "purchaseItem"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 229
    if-eqz p1, :cond_1

    .line 230
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 231
    .local v0, "oldParams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v1

    .line 232
    .local v1, "originatingPage":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v3

    if-nez v3, :cond_0

    .line 233
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 236
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 237
    .local v2, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "%s %s"

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v8

    iget-object v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->skuTitle:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->skuTitle:Ljava/lang/String;

    :goto_0
    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setPurchaseTitle(Ljava/lang/String;)V

    .line 238
    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseItemConsumable(Z)V

    .line 239
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseProductId(Ljava/lang/String;)V

    .line 240
    iget-object v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->availabilityId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseAvailabilityId(Ljava/lang/String;)V

    .line 241
    iget-object v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ConsumableAvailabilityInfo;->skuId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseSkuId(Ljava/lang/String;)V

    .line 242
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseOriginatingPage(Ljava/lang/String;)V

    .line 243
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 245
    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putCodeRedeemable(Z)V

    .line 248
    const-string v3, "Purchase - Consumable"

    invoke-static {v3, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->track(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 249
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->increment()Ljava/lang/String;

    .line 250
    const-class v3, Lcom/microsoft/xbox/xle/app/activity/PurchaseWebViewActivity;

    invoke-virtual {p0, v3, v7, v2}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 252
    .end local v0    # "oldParams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v1    # "originatingPage":Ljava/lang/String;
    .end local v2    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_1
    return-void

    .line 237
    .restart local v0    # "oldParams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .restart local v1    # "originatingPage":Ljava/lang/String;
    .restart local v2    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_2
    const-string v3, ""

    goto :goto_0
.end method

.method public purchaseDurable()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 201
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->getShowPurchaseButton()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 202
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v1

    .line 203
    .local v1, "originatingPage":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 204
    .local v0, "oldParams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSourcePage()Ljava/lang/String;

    move-result-object v3

    .line 205
    .local v3, "source":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v4

    if-nez v4, :cond_0

    .line 206
    new-instance v4, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    invoke-direct {v4}, Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;-><init>()V

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 209
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 210
    .local v2, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setPurchaseTitle(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseItemConsumable(Z)V

    .line 212
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseProductId(Ljava/lang/String;)V

    .line 213
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getAvailabilityId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseAvailabilityId(Ljava/lang/String;)V

    .line 214
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameContentDetailModel;->getSkuId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseSkuId(Ljava/lang/String;)V

    .line 215
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putPurchaseOriginatingPage(Ljava/lang/String;)V

    .line 216
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getBIData()Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putBIData(Lcom/microsoft/xbox/toolkit/ui/InstrumentationParameters;)V

    .line 218
    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putCodeRedeemable(Z)V

    .line 221
    const-string v4, "Purchase - Durable"

    invoke-static {v4, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->track(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 222
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->increment()Ljava/lang/String;

    .line 224
    const-class v4, Lcom/microsoft/xbox/xle/app/activity/PurchaseWebViewActivity;

    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5, v2}, Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 226
    .end local v0    # "oldParams":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v1    # "originatingPage":Ljava/lang/String;
    .end local v2    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v3    # "source":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected shouldRefresh()Z
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x1

    return v0
.end method
