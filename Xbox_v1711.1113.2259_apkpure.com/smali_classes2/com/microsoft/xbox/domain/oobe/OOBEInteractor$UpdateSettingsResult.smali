.class public abstract Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
.super Ljava/lang/Object;
.source "OOBEInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "UpdateSettingsResult"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;
    .locals 1
    .param p0, "settings"    # Lcom/microsoft/xbox/domain/oobe/OOBESettings;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 354
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_UpdateSettingsResult;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_UpdateSettingsResult;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V

    return-object v0
.end method


# virtual methods
.method public abstract settings()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
