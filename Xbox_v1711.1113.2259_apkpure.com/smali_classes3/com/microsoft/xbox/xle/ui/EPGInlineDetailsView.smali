.class public Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;
.super Landroid/widget/LinearLayout;
.source "EPGInlineDetailsView.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;


# static fields
.field private static mCache:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

.field private mDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

.field private mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private mTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final mUpdateDescriptionTextView:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 384
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mCache:Ljava/util/Stack;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 387
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 340
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$5;-><init>(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mUpdateDescriptionTextView:Ljava/lang/Runnable;

    .line 388
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method private buildSubtitle(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Ljava/lang/String;
    .locals 4
    .param p1, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p2, "channel"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .prologue
    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentSeriesTitle()Ljava/lang/String;

    move-result-object v2

    .line 249
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowTitle()Ljava/lang/String;

    move-result-object v1

    .line 250
    .local v1, "showtitle":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 252
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 253
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;
    .locals 3
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 392
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mCache:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 394
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 395
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const v1, 0x7f0300f0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    .line 397
    .end local v0    # "inflator":Landroid/view/LayoutInflater;
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mCache:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    goto :goto_0
.end method

.method private pickTitle(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Ljava/lang/String;
    .locals 2
    .param p1, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .prologue
    .line 236
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentSeriesTitle()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "title":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 238
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowTitle()Ljava/lang/String;

    move-result-object v0

    .line 240
    :cond_1
    return-object v0
.end method

.method private static reclaimInstance(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;)V
    .locals 1
    .param p0, "view"    # Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;

    .prologue
    .line 402
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mCache:Ljava/util/Stack;

    invoke-virtual {v0, p0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    return-void
.end method

.method private setImageURI(Ljava/lang/String;)Z
    .locals 4
    .param p1, "urlString"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 350
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    if-nez v2, :cond_0

    move v0, v1

    .line 364
    :goto_0
    return v0

    .line 353
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 355
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    .line 357
    .local v0, "setAndShow":Z
    :goto_1
    if-eqz v0, :cond_2

    .line 358
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setImageSrc(Ljava/lang/String;)V

    .line 359
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setVisibility(I)V

    goto :goto_0

    .end local v0    # "setAndShow":Z
    :cond_1
    move v0, v1

    .line 355
    goto :goto_1

    .line 361
    .restart local v0    # "setAndShow":Z
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupButton(ILandroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 369
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 370
    .local v0, "button":Landroid/view/View;
    if-nez v0, :cond_0

    .line 375
    :goto_0
    return-void

    .line 373
    :cond_0
    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    if-eqz p2, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private updateAppChannelItemExtra()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 302
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->getAppChannelInformationString(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "subtitle":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 304
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 305
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v1, :cond_0

    .line 311
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 315
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    const v2, 0x7f0e057d

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->isExtraLoaded()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$4;-><init>(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;)V

    :goto_1
    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setupButton(ILandroid/view/View$OnClickListener;)V

    .line 324
    return-void

    .line 307
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0

    .line 318
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->addListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 107
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->updateAppChannelItemExtra()V

    .line 110
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 111
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->removeListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 120
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 121
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 70
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 73
    :cond_0
    const v0, 0x7f0e00df

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    .line 74
    const v0, 0x7f0e0576

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 75
    const v0, 0x7f0e0579

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 76
    const v0, 0x7f0e057a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 77
    const v0, 0x7f0e057b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 83
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setClickable(Z)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 92
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 97
    if-eqz p1, :cond_0

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mUpdateDescriptionTextView:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->post(Ljava/lang/Runnable;)Z

    .line 100
    :cond_0
    return-void
.end method

.method public onPropertyChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 332
    const-string v0, "extra"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->updateAppChannelItemExtra()V

    .line 335
    :cond_0
    return-void
.end method

.method public reclaim()V
    .locals 0

    .prologue
    .line 378
    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->reclaimInstance(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;)V

    .line 379
    return-void
.end method

.method public setAppChannelItem(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)V
    .locals 3
    .param p1, "item"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .prologue
    const/4 v1, 0x0

    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->removeListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 273
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setVisibility(I)V

    .line 279
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    const v0, 0x7f0e057c

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setupButton(ILandroid/view/View$OnClickListener;)V

    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    if-eqz v0, :cond_2

    .line 286
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->addListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mAppChannelItem:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->loadExtra()V

    .line 288
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->updateAppChannelItemExtra()V

    .line 297
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 279
    goto :goto_0

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v0, :cond_3

    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 294
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    const v0, 0x7f0e057d

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setupButton(ILandroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public setProgram(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
    .locals 8
    .param p1, "channel"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .param p2, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p3, "model"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x8

    .line 128
    const/4 v0, 0x0

    .line 130
    .local v0, "hideInfoButton":Z
    invoke-static {p1, p2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->isContentAppropriate(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 131
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070481

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070480

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v5, ""

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 135
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v3, :cond_0

    .line 136
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v5, ""

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 143
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setVisibility(I)V

    .line 145
    const/4 v0, 0x1

    .line 194
    :cond_1
    :goto_0
    const v5, 0x7f0e057c

    if-eqz p3, :cond_2

    if-eqz v0, :cond_a

    :cond_2
    move-object v3, v4

    :goto_1
    invoke-direct {p0, v5, v3}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setupButton(ILandroid/view/View$OnClickListener;)V

    .line 205
    const v3, 0x7f0e057d

    if-nez p1, :cond_b

    :goto_2
    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setupButton(ILandroid/view/View$OnClickListener;)V

    .line 217
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getHeadendId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->shouldEnableStreaming(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 218
    const v3, 0x7f0e057e

    new-instance v4, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;

    invoke-direct {v4, p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$3;-><init>(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V

    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setupButton(ILandroid/view/View$OnClickListener;)V

    .line 232
    :cond_3
    return-void

    .line 146
    :cond_4
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 148
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070459

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07045f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v3, :cond_5

    .line 151
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v5, ""

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 154
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const-string v5, ""

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 156
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mImageView:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;->setVisibility(I)V

    .line 157
    const/4 v0, 0x1

    goto :goto_0

    .line 160
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->pickTitle(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mDescriptionTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    invoke-direct {p0, p2, p1}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->buildSubtitle(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Ljava/lang/String;

    move-result-object v2

    .line 164
    .local v2, "subtitle":Ljava/lang/String;
    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 165
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 167
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v3, :cond_7

    .line 175
    :cond_7
    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v3, :cond_8

    .line 177
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->isHD()Z

    move-result v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {p2, v3, v5}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->getTVInformationString(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;ZLandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "showtime":Ljava/lang/String;
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 179
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 185
    .end local v1    # "showtime":Ljava/lang/String;
    :cond_8
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->setImageURI(Ljava/lang/String;)Z

    .line 188
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getHeadendId()Ljava/lang/String;

    move-result-object v3

    const-string v5, "ba5eba11-dea1-4bad-ba11-feddeadfab1e"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 189
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 170
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mSubtitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 171
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz v3, :cond_7

    .line 172
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;->mShowtimeTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_3

    .line 194
    .end local v2    # "subtitle":Ljava/lang/String;
    :cond_a
    new-instance v3, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$1;-><init>(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V

    goto/16 :goto_1

    .line 205
    :cond_b
    new-instance v4, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$2;

    invoke-direct {v4, p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView$2;-><init>(Lcom/microsoft/xbox/xle/ui/EPGInlineDetailsView;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)V

    goto/16 :goto_2
.end method
