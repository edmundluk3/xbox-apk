.class public Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "FeedbackViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;
    }
.end annotation


# static fields
.field public static final CRLF:Ljava/lang/String; = "\r\n"


# instance fields
.field private appVersion:Ljava/lang/String;

.field private crashLogFile:Ljava/lang/String;

.field private deviceModel:Ljava/lang/String;

.field private feedbackMessage:Ljava/lang/String;

.field private gamerTag:Ljava/lang/String;

.field private headEndId:Ljava/lang/String;

.field private internalAlias:Ljava/lang/String;

.field private isCrash:Z

.field private isProblem:Z

.field private isSuggestion:Z

.field private layout:Ljava/lang/String;

.field private logFile:Ljava/lang/String;

.field private negativeFeedbackSelected:Z

.field private oneGuideDevices:Ljava/lang/String;

.field private pageName:Ljava/lang/String;

.field private platformLogFile:Ljava/lang/String;

.field private positiveFeedbackSelected:Z

.field private referenceId:Ljava/lang/String;

.field private sandboxId:Ljava/lang/String;

.field private screenShotFile:Ljava/lang/String;

.field private version:Ljava/lang/String;

.field private videoLogFile:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 51
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->positiveFeedbackSelected:Z

    .line 52
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->negativeFeedbackSelected:Z

    .line 53
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isProblem:Z

    .line 54
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isCrash:Z

    .line 55
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isSuggestion:Z

    .line 56
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->feedbackMessage:Ljava/lang/String;

    .line 57
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->pageName:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->version:Ljava/lang/String;

    .line 59
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->appVersion:Ljava/lang/String;

    .line 60
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->deviceModel:Ljava/lang/String;

    .line 61
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->layout:Ljava/lang/String;

    .line 62
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->gamerTag:Ljava/lang/String;

    .line 63
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->referenceId:Ljava/lang/String;

    .line 64
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->sandboxId:Ljava/lang/String;

    .line 65
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->headEndId:Ljava/lang/String;

    .line 66
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->oneGuideDevices:Ljava/lang/String;

    .line 67
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->screenShotFile:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->logFile:Ljava/lang/String;

    .line 69
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->platformLogFile:Ljava/lang/String;

    .line 70
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->videoLogFile:Ljava/lang/String;

    .line 71
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->crashLogFile:Ljava/lang/String;

    .line 72
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->internalAlias:Ljava/lang/String;

    .line 76
    return-void
.end method

.method private getActivityName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 456
    const-string v2, "UNKNOWN"

    .line 458
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 459
    .local v0, "base":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    if-eqz v0, :cond_0

    .line 460
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->getActivityNameForFeedback()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 466
    .end local v0    # "base":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    .line 467
    const-string v2, "UNKNOWN"

    .line 470
    :cond_1
    return-object v2

    .line 462
    :catch_0
    move-exception v1

    .line 463
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Error getting activity name"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 452
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissFeedbackDialog()V

    .line 453
    return-void
.end method

.method public getBody()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "APP VERSION      : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->appVersion:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "OS VERSION       : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->version:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "MODEL            : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->deviceModel:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "AREA             : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->pageName:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "INSTALLER        : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "LAYOUT           : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->layout:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "SANDBOXID        : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->sandboxId:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "GAMERTAG         : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->gamerTag:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "ONEGUIDE ID      : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->headEndId:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "ONEGUIDE DEVICES : {%s}%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->oneGuideDevices:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "REF#             : %s%s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->referenceId:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, "\r\n"

    aput-object v4, v3, v7

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getCrashData()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 152
    const-string v2, "********** UNHANDLED EXCEPTION *************"

    .line 153
    .local v2, "crashHeader":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->crashLogFile:Ljava/lang/String;

    if-nez v7, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-object v4

    .line 157
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->crashLogFile:Ljava/lang/String;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 158
    .local v1, "crashFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 162
    const/4 v6, 0x0

    .line 164
    .local v6, "stream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    .end local v6    # "stream":Ljava/io/FileInputStream;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->crashLogFile:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "crashData":Ljava/lang/String;
    const-string v4, ""

    .line 170
    .local v4, "onlyCrashInfo":Ljava/lang/String;
    const-string v7, "********** UNHANDLED EXCEPTION *************"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 171
    const-string v7, "********** UNHANDLED EXCEPTION *************"

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const-string v8, "********** UNHANDLED EXCEPTION *************"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int v5, v7, v8

    .line 172
    .local v5, "start":I
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 173
    goto :goto_0

    .line 165
    .end local v0    # "crashData":Ljava/lang/String;
    .end local v4    # "onlyCrashInfo":Ljava/lang/String;
    .end local v5    # "start":I
    .end local v6    # "stream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v3

    .line 166
    .local v3, "e":Ljava/io/FileNotFoundException;
    goto :goto_0

    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .restart local v0    # "crashData":Ljava/lang/String;
    .restart local v4    # "onlyCrashInfo":Ljava/lang/String;
    .restart local v6    # "stream":Ljava/io/FileInputStream;
    :cond_2
    move-object v4, v0

    .line 175
    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 18

    .prologue
    .line 179
    const/16 v1, 0x64

    .line 180
    .local v1, "MAXLEN":I
    const-string v12, ""

    .line 181
    .local v12, "titleText":Ljava/lang/String;
    const-string v4, ""

    .line 182
    .local v4, "commentData":Ljava/lang/String;
    const-string v5, ""

    .line 184
    .local v5, "crashHashcode":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isCrash:Z

    if-eqz v13, :cond_4

    .line 185
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getCrashData()Ljava/lang/String;

    move-result-object v4

    .line 187
    invoke-static {}, Lcom/google/common/hash/Hashing;->md5()Lcom/google/common/hash/HashFunction;

    move-result-object v13

    invoke-interface {v13}, Lcom/google/common/hash/HashFunction;->newHasher()Lcom/google/common/hash/Hasher;

    move-result-object v13

    sget-object v14, Lcom/google/common/base/Charsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-interface {v13, v4, v14}, Lcom/google/common/hash/Hasher;->putString(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lcom/google/common/hash/Hasher;

    move-result-object v13

    invoke-interface {v13}, Lcom/google/common/hash/Hasher;->hash()Lcom/google/common/hash/HashCode;

    move-result-object v10

    .line 188
    .local v10, "hc":Lcom/google/common/hash/HashCode;
    invoke-virtual {v10}, Lcom/google/common/hash/HashCode;->toString()Ljava/lang/String;

    move-result-object v5

    .line 189
    move-object v6, v5

    .line 193
    .local v6, "crashHashcodeTitle":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x6

    if-le v13, v14, :cond_0

    .line 194
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    const/4 v15, 0x6

    invoke-virtual {v6, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "..."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 197
    :cond_0
    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "[Android Xbox %s] CRASH[%s]: "

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const-string v17, "External"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v6, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 203
    .end local v6    # "crashHashcodeTitle":Ljava/lang/String;
    .end local v10    # "hc":Lcom/google/common/hash/HashCode;
    :goto_0
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 204
    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 205
    .local v9, "firstNewLine":I
    if-ltz v9, :cond_6

    const/16 v13, 0x64

    if-ge v9, v13, :cond_6

    .line 206
    const/4 v13, 0x0

    invoke-virtual {v12, v13, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 212
    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v11

    .line 213
    .local v11, "len":I
    const/16 v13, 0x64

    if-le v11, v13, :cond_7

    .line 214
    const/4 v13, 0x0

    const/16 v14, 0x63

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 221
    :cond_1
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .local v2, "builder":Ljava/lang/StringBuilder;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isCrash:Z

    if-eqz v13, :cond_8

    .line 224
    const-string v13, "=========Callstack==========\r\n"

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    const-string v13, "============================\r\n"

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->positiveFeedbackSelected:Z

    if-eqz v13, :cond_9

    const-string v8, "POSITIVE"

    .line 233
    .local v8, "experience":Ljava/lang/String;
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isCrash:Z

    if-eqz v13, :cond_2

    .line 234
    const-string v8, "CRASH"

    .line 236
    :cond_2
    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "EXPERIENCE       :%s%s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v8, v15, v16

    const/16 v16, 0x1

    const-string v17, "\r\n"

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isCrash:Z

    if-eqz v13, :cond_3

    .line 239
    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "HASHCODE         :%s%s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v5, v15, v16

    const/16 v16, 0x1

    const-string v17, "\r\n"

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :cond_3
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "PROBLEM          :%s%s"

    const/4 v13, 0x2

    new-array v0, v13, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isProblem:Z

    if-eqz v13, :cond_a

    const-string v13, "YES"

    :goto_5
    aput-object v13, v16, v17

    const/4 v13, 0x1

    const-string v17, "\r\n"

    aput-object v17, v16, v13

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "SUGGESTION       :%s%s"

    const/4 v13, 0x2

    new-array v0, v13, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isSuggestion:Z

    if-eqz v13, :cond_b

    const-string v13, "YES"

    :goto_6
    aput-object v13, v16, v17

    const/4 v13, 0x1

    const-string v17, "\r\n"

    aput-object v17, v16, v13

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "PAGENAME         :%s%s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->pageName:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, "\r\n"

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    const-string v13, "\r\n"

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    const-string v13, "=======CONFIGURATION========\r\n"

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "%s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getBody()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string v13, "============================\r\n"

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    return-object v13

    .line 199
    .end local v2    # "builder":Ljava/lang/StringBuilder;
    .end local v8    # "experience":Ljava/lang/String;
    .end local v9    # "firstNewLine":I
    .end local v11    # "len":I
    :cond_4
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "[Android Xbox %s] %s: "

    const/4 v13, 0x2

    new-array v0, v13, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/4 v13, 0x0

    const-string v17, "External"

    aput-object v17, v16, v13

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->pageName:Ljava/lang/String;

    if-eqz v13, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->pageName:Ljava/lang/String;

    :goto_7
    aput-object v13, v16, v17

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 200
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getFeedbackMessage()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 199
    :cond_5
    const-string v13, "UNKNOWN"

    goto :goto_7

    .line 208
    .restart local v9    # "firstNewLine":I
    :cond_6
    const-string v13, "\r"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    .line 209
    const-string v13, "\n"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 215
    .restart local v11    # "len":I
    :cond_7
    const/16 v13, 0x64

    if-ge v11, v13, :cond_1

    .line 216
    rsub-int/lit8 v7, v11, 0x64

    .line 217
    .local v7, "diff":I
    new-array v3, v7, [C

    .line 218
    .local v3, "chars":[C
    const/16 v13, 0x20

    invoke-static {v3, v13}, Ljava/util/Arrays;->fill([CC)V

    .line 219
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_2

    .line 228
    .end local v3    # "chars":[C
    .end local v7    # "diff":I
    .restart local v2    # "builder":Ljava/lang/StringBuilder;
    :cond_8
    const-string v13, "=======Feedback Form========\r\n"

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "USER COMMENTS%s%s%s%s"

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const-string v17, "\r\n"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getFeedbackMessage()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const-string v17, "\r\n"

    aput-object v17, v15, v16

    const/16 v16, 0x3

    const-string v17, "\r\n"

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 232
    :cond_9
    const-string v8, "NEGATIVE"

    goto/16 :goto_4

    .line 242
    .restart local v8    # "experience":Ljava/lang/String;
    :cond_a
    const-string v13, "NO"

    goto/16 :goto_5

    .line 243
    :cond_b
    const-string v13, "NO"

    goto/16 :goto_6
.end method

.method public getFeedbackMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->feedbackMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getFeedbackType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 286
    const-string v0, "Rating Only"

    .line 287
    .local v0, "feedback":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getIsProblem()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 288
    const-string v0, "Problem"

    .line 293
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getIsCrash()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 294
    const-string v0, "Crash"

    .line 296
    :cond_1
    return-object v0

    .line 289
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getIsSuggestion()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 290
    const-string v0, "Suggestion"

    goto :goto_0
.end method

.method public getHasRatingBeenSelected()Z
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->positiveFeedbackSelected:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->negativeFeedbackSelected:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHasReportTypeBeenSelected()Z
    .locals 1

    .prologue
    .line 397
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isProblem:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isSuggestion:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInternalAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->internalAlias:Ljava/lang/String;

    return-object v0
.end method

.method public getIsCrash()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isCrash:Z

    return v0
.end method

.method public getIsProblem()Z
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isProblem:Z

    return v0
.end method

.method public getIsSuggestion()Z
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isSuggestion:Z

    return v0
.end method

.method public getLogFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->logFile:Ljava/lang/String;

    return-object v0
.end method

.method public getPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->pageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPlatformLogFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->platformLogFile:Ljava/lang/String;

    return-object v0
.end method

.method public getRating()Ljava/lang/String;
    .locals 2

    .prologue
    .line 278
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->positiveFeedbackSelected:Z

    if-eqz v1, :cond_1

    const-string v0, "Smile"

    .line 279
    .local v0, "experience":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getIsCrash()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    const-string v0, "Frown"

    .line 282
    :cond_0
    return-object v0

    .line 278
    .end local v0    # "experience":Ljava/lang/String;
    :cond_1
    const-string v0, "Frown"

    goto :goto_0
.end method

.method public getReferenceId()Ljava/util/UUID;
    .locals 2

    .prologue
    .line 261
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->referenceId:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    .line 262
    .local v0, "id":Ljava/util/UUID;
    return-object v0
.end method

.method public getScreenShotFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->screenShotFile:Ljava/lang/String;

    return-object v0
.end method

.method public getUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 437
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoLogFile()Ljava/lang/String;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->videoLogFile:Ljava/lang/String;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 428
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 433
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 419
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 449
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 423
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 424
    return-void
.end method

.method public saveData()V
    .locals 19

    .prologue
    .line 79
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v10

    .line 80
    .local v10, "pm":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-nez v10, :cond_0

    const/4 v5, 0x0

    .line 81
    .local v5, "gt":Ljava/lang/String;
    :goto_0
    if-eqz v5, :cond_1

    .line 82
    :goto_1
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->gamerTag:Ljava/lang/String;

    .line 83
    sget-object v11, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->version:Ljava/lang/String;

    .line 84
    sget-object v11, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "deviceManufacturer":Ljava/lang/String;
    sget-object v11, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 86
    .local v3, "deviceProduct":Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .end local v3    # "deviceProduct":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->deviceModel:Ljava/lang/String;

    .line 87
    const-string v11, "PORTRAIT"

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->layout:Ljava/lang/String;

    .line 90
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 91
    .local v9, "pkgInfo":Landroid/content/pm/PackageInfo;
    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v12, "%s (%s)"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    iget-object v15, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v11, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->appVersion:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    .end local v9    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :goto_3
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getCurrentSandboxId()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->sandboxId:Ljava/lang/String;

    .line 97
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->getActivityName()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->pageName:Ljava/lang/String;

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getDescriptor()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->headEndId:Ljava/lang/String;

    .line 100
    const/4 v8, 0x0

    .line 101
    .local v8, "oneGuideTemp":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v11

    if-eqz v11, :cond_6

    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getDevices()[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v11

    if-eqz v11, :cond_6

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getDevices()[Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v12

    array-length v13, v12

    const/4 v11, 0x0

    :goto_4
    if-ge v11, v13, :cond_6

    aget-object v6, v12, v11

    .line 103
    .local v6, "info":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    iget-object v14, v6, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_brand:Ljava/lang/String;

    if-nez v14, :cond_3

    const-string v1, "none"

    .line 104
    .local v1, "brand":Ljava/lang/String;
    :goto_5
    iget-object v14, v6, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_model:Ljava/lang/String;

    if-nez v14, :cond_4

    const-string v7, ""

    .line 105
    .local v7, "model":Ljava/lang/String;
    :goto_6
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "%s:%s%s"

    const/16 v16, 0x3

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    iget-object v0, v6, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_type:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x1

    aput-object v1, v16, v17

    const/16 v17, 0x2

    aput-object v7, v16, v17

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 106
    .local v4, "deviceTypeNamePair":Ljava/lang/String;
    if-eqz v8, :cond_5

    .line 107
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ";"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 102
    :goto_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 80
    .end local v1    # "brand":Ljava/lang/String;
    .end local v2    # "deviceManufacturer":Ljava/lang/String;
    .end local v4    # "deviceTypeNamePair":Ljava/lang/String;
    .end local v5    # "gt":Ljava/lang/String;
    .end local v6    # "info":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .end local v7    # "model":Ljava/lang/String;
    .end local v8    # "oneGuideTemp":Ljava/lang/String;
    :cond_0
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 81
    .restart local v5    # "gt":Ljava/lang/String;
    :cond_1
    const-string v5, "UNKNOWN"

    goto/16 :goto_1

    .line 86
    .restart local v2    # "deviceManufacturer":Ljava/lang/String;
    .restart local v3    # "deviceProduct":Ljava/lang/String;
    :cond_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 103
    .end local v3    # "deviceProduct":Ljava/lang/String;
    .restart local v6    # "info":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .restart local v8    # "oneGuideTemp":Ljava/lang/String;
    :cond_3
    iget-object v1, v6, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_brand:Ljava/lang/String;

    goto :goto_5

    .line 104
    .restart local v1    # "brand":Ljava/lang/String;
    :cond_4
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v6, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->device_model:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_6

    .line 109
    .restart local v4    # "deviceTypeNamePair":Ljava/lang/String;
    .restart local v7    # "model":Ljava/lang/String;
    :cond_5
    move-object v8, v4

    goto :goto_7

    .line 113
    .end local v1    # "brand":Ljava/lang/String;
    .end local v4    # "deviceTypeNamePair":Ljava/lang/String;
    .end local v6    # "info":Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;
    .end local v7    # "model":Ljava/lang/String;
    :cond_6
    if-eqz v8, :cond_7

    .line 114
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->oneGuideDevices:Ljava/lang/String;

    .line 116
    :cond_7
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->referenceId:Ljava/lang/String;

    .line 118
    const-string v11, "FEEDBACK"

    const-string v12, "==============================================="

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "APP VERSION      : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->appVersion:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "OS VERSION       : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->version:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "MODEL            : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->deviceModel:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "AREA             : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->pageName:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "INSTALLER        : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v16

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "LAYOUT           : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->layout:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "SANDBOXID        : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->sandboxId:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "GAMERTAG         : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->gamerTag:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "ONEGUIDE ID      : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->headEndId:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "ONEGUIDE DEVICES : {%s}"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->oneGuideDevices:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v11, "FEEDBACK"

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v13, "REF#             : %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->referenceId:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-static {v12, v13, v14}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v11, "FEEDBACK"

    const-string v12, "==============================================="

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    return-void

    .line 92
    .end local v8    # "oneGuideTemp":Ljava/lang/String;
    :catch_0
    move-exception v11

    goto/16 :goto_3
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "ver"    # Ljava/lang/String;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->appVersion:Ljava/lang/String;

    .line 311
    return-void
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 370
    return-void
.end method

.method public setCrashLogFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 330
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->crashLogFile:Ljava/lang/String;

    .line 331
    return-void
.end method

.method public setDeviceModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "model"    # Ljava/lang/String;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->deviceModel:Ljava/lang/String;

    .line 382
    return-void
.end method

.method public setFeedbackMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 405
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->feedbackMessage:Ljava/lang/String;

    .line 406
    return-void
.end method

.method public setFeedbackType(Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;)V
    .locals 3
    .param p1, "feedback"    # Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 300
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->positive:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->positiveFeedbackSelected:Z

    .line 301
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->negative:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->negativeFeedbackSelected:Z

    .line 302
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;->crash:Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel$feedbackType;

    if-ne p1, v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isCrash:Z

    .line 303
    return-void

    :cond_0
    move v0, v2

    .line 300
    goto :goto_0

    :cond_1
    move v0, v2

    .line 301
    goto :goto_1

    :cond_2
    move v1, v2

    .line 302
    goto :goto_2
.end method

.method public setGamerTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "gt"    # Ljava/lang/String;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->gamerTag:Ljava/lang/String;

    .line 367
    return-void
.end method

.method public setHeadEndId(Ljava/lang/String;)V
    .locals 0
    .param p1, "head"    # Ljava/lang/String;

    .prologue
    .line 354
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->headEndId:Ljava/lang/String;

    .line 355
    return-void
.end method

.method public setInternalAlias(Ljava/lang/String;)V
    .locals 0
    .param p1, "internalAlias"    # Ljava/lang/String;

    .prologue
    .line 413
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->internalAlias:Ljava/lang/String;

    .line 414
    return-void
.end method

.method public setIsProblemReport(Z)V
    .locals 0
    .param p1, "report"    # Z

    .prologue
    .line 389
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isProblem:Z

    .line 390
    return-void
.end method

.method public setIsSuggestion(Z)V
    .locals 0
    .param p1, "report"    # Z

    .prologue
    .line 393
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->isSuggestion:Z

    .line 394
    return-void
.end method

.method public setLayout(Ljava/lang/String;)V
    .locals 0
    .param p1, "orientation"    # Ljava/lang/String;

    .prologue
    .line 362
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->layout:Ljava/lang/String;

    .line 363
    return-void
.end method

.method public setLogFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 322
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->logFile:Ljava/lang/String;

    .line 323
    return-void
.end method

.method public setOneGuideDevices(Ljava/lang/String;)V
    .locals 0
    .param p1, "devices"    # Ljava/lang/String;

    .prologue
    .line 358
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->oneGuideDevices:Ljava/lang/String;

    .line 359
    return-void
.end method

.method public setPageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 385
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->pageName:Ljava/lang/String;

    .line 386
    return-void
.end method

.method public setPlatformLogFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 334
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->platformLogFile:Ljava/lang/String;

    .line 335
    return-void
.end method

.method public setReferenceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 373
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->referenceId:Ljava/lang/String;

    .line 374
    return-void
.end method

.method public setSandboxId(Ljava/lang/String;)V
    .locals 0
    .param p1, "sandbox"    # Ljava/lang/String;

    .prologue
    .line 350
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->sandboxId:Ljava/lang/String;

    .line 351
    return-void
.end method

.method public setScreenShotFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 314
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->screenShotFile:Ljava/lang/String;

    .line 315
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "ver"    # Ljava/lang/String;

    .prologue
    .line 377
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->version:Ljava/lang/String;

    .line 378
    return-void
.end method

.method public setVideoLogFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 342
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;->videoLogFile:Ljava/lang/String;

    .line 343
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 443
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    return-void
.end method
