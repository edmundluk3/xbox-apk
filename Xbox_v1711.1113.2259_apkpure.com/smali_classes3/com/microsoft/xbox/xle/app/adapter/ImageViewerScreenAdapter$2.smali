.class Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$2;
.super Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListenerBase;
.source "ImageViewerScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ImageViewerScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ImageViewerScreenAdapter;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListenerBase;-><init>()V

    return-void
.end method


# virtual methods
.method public onAfterImageSet(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "img"    # Landroid/widget/ImageView;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x1

    .line 85
    if-nez p2, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    invoke-virtual {p1}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 89
    .local v0, "parent":Landroid/widget/RelativeLayout;
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 90
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
