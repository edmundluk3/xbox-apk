.class public abstract Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;
.super Ljava/lang/Object;
.source "OOBETimeZoneSpinnerItem.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# static fields
.field private static ITEM_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;",
            ">;"
        }
    .end annotation
.end field

.field private static REVERSE_LOOKUP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static from(Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;)Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;
    .locals 3
    .param p0, "timeZone"    # Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    .prologue
    .line 29
    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBETimeZoneSpinnerItem;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v2, p0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->stringID:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->serializedName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/presentation/oobe/AutoValue_OOBETimeZoneSpinnerItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getAllItems()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    sget-object v3, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->ITEM_LIST:Ljava/util/List;

    if-nez v3, :cond_0

    .line 34
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sput-object v3, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->ITEM_LIST:Ljava/util/List;

    .line 35
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->REVERSE_LOOKUP:Ljava/util/Map;

    .line 36
    const/4 v0, 0x0

    .line 37
    .local v0, "idx":I
    invoke-static {}, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->values()[Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    move v1, v0

    .end local v0    # "idx":I
    .local v1, "idx":I
    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v2, v4, v3

    .line 38
    .local v2, "timeZone":Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;
    sget-object v6, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->ITEM_LIST:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->from(Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;)Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    sget-object v6, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->REVERSE_LOOKUP:Ljava/util/Map;

    iget-object v7, v2, Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;->serializedName:Ljava/lang/String;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "idx":I
    .restart local v0    # "idx":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    add-int/lit8 v3, v3, 0x1

    move v1, v0

    .end local v0    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_0

    .line 43
    .end local v2    # "timeZone":Lcom/microsoft/xbox/domain/oobe/OOBETimeZone;
    :cond_0
    sget-object v3, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->ITEM_LIST:Ljava/util/List;

    return-object v3
.end method

.method public static getPosition(Ljava/lang/String;)I
    .locals 2
    .param p0, "item"    # Ljava/lang/String;

    .prologue
    .line 59
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->REVERSE_LOOKUP:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 61
    invoke-static {}, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->getAllItems()Ljava/util/List;

    .line 64
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->REVERSE_LOOKUP:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method


# virtual methods
.method abstract displayName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->displayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/oobe/OOBETimeZoneSpinnerItem;->serializedName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method abstract serializedName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
