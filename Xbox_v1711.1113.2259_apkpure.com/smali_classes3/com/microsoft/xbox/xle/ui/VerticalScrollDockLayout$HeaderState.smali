.class final enum Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;
.super Ljava/lang/Enum;
.source "VerticalScrollDockLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "HeaderState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

.field public static final enum FULL_CLOSED:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

.field public static final enum FULL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

.field public static final enum PARTIAL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    const-string v1, "FULL_OPEN"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    const-string v1, "FULL_CLOSED"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_CLOSED:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    new-instance v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    const-string v1, "PARTIAL_OPEN"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->PARTIAL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_CLOSED:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->PARTIAL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->$VALUES:[Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 46
    const-class v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->$VALUES:[Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    return-object v0
.end method
