.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubRoster"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Z
    .locals 1
    .param p0, "roster"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1087
    if-eqz p0, :cond_1

    .line 1088
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->moderator()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1089
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->requestedToJoin()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1090
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->recommended()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1091
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->banned()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1087
    :goto_0
    return v0

    .line 1091
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1083
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoster$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoster$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .locals 5
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;"
        }
    .end annotation

    .prologue
    .local p0, "moderator":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .local p1, "requestedToJoin":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .local p2, "recommended":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .local p3, "banned":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    const/4 v0, 0x0

    .line 1075
    new-instance v4, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoster;

    if-eqz p0, :cond_1

    .line 1076
    invoke-static {p0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    move-object v3, v1

    :goto_0
    if-eqz p1, :cond_2

    .line 1077
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    move-object v2, v1

    :goto_1
    if-eqz p2, :cond_3

    .line 1078
    invoke-static {p2}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    :goto_2
    if-eqz p3, :cond_0

    .line 1079
    invoke-static {p3}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :cond_0
    invoke-direct {v4, v3, v2, v1, v0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoster;-><init>(Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V

    .line 1075
    return-object v4

    :cond_1
    move-object v3, v0

    .line 1076
    goto :goto_0

    :cond_2
    move-object v2, v0

    .line 1077
    goto :goto_1

    :cond_3
    move-object v1, v0

    .line 1078
    goto :goto_2
.end method


# virtual methods
.method public abstract banned()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract moderator()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract recommended()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract requestedToJoin()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;"
        }
    .end annotation
.end method
