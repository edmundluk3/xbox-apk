.class public abstract Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;
.super Ljava/lang/Object;
.source "PartySession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
.end method

.method public abstract setMultiplayerSession(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;
.end method

.method public abstract setMuted(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;
.end method

.method public abstract setRoster(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;)",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;"
        }
    .end annotation
.end method
