.class public Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;
.super Ljava/lang/Object;
.source "AdapterFactory.java"


# static fields
.field private static final instance:Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->instance:Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->instance:Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    return-object v0
.end method


# virtual methods
.method public getAboutActivityAdapter(Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 317
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 318
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 319
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 321
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AboutActivityAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/AboutActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;)V

    goto :goto_0
.end method

.method public getActivityAlertScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 866
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 867
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 868
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 870
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;)V

    goto :goto_0
.end method

.method public getActivityFeedActionsScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 906
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 907
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 908
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 910
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedActionsScreenViewModel;)V

    goto :goto_0
.end method

.method public getActivityFeedShareToFeedScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 916
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 917
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 918
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 920
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedShareToFeedScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)V

    goto :goto_0
.end method

.method public getActivityFeedStatusPostScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 896
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 897
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 898
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 900
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedStatusPostScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedStatusPostScreenViewModel;)V

    goto :goto_0
.end method

.method public getActivityGalleryAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 307
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 308
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 309
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 311
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityGalleryActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityGalleryActivityViewModel;)V

    goto :goto_0
.end method

.method public getActivityOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 297
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 298
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 299
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 301
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityOverviewActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityOverviewActivityViewModel;)V

    goto :goto_0
.end method

.method public getActivityParentItemAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 547
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 548
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 549
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 551
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityParentScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityParentScreenViewModel;)V

    goto :goto_0
.end method

.method public getAlbumDetailAdapter(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 237
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 238
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 239
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 241
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;)V

    goto :goto_0
.end method

.method public getAlbumDetailFromTrackAdapter(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 247
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 248
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 249
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 251
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenFromTrackAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenFromTrackViewModel;)V

    goto :goto_0
.end method

.method public getAppDetailHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 267
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 268
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 269
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 271
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailHeaderScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AppDetailHeaderScreenViewModel;)V

    goto :goto_0
.end method

.method public getAppDetailOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 277
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 278
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 279
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 281
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/AppDetailOverviewScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;)V

    goto :goto_0
.end method

.method public getArtistDetailAlbumsAdapter(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 727
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 728
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 729
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 731
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailAlbumsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailAlbumsScreenViewModel;)V

    goto :goto_0
.end method

.method public getArtistDetailBiographyAdapter(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 737
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 738
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 739
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 741
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailBiographyScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailBiographyScreenViewModel;)V

    goto :goto_0
.end method

.method public getArtistDetailHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 747
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 748
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 749
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 751
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailHeaderAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailHeaderViewModel;)V

    goto :goto_0
.end method

.method public getArtistDetailTopSongsAdapter(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 717
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 718
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 719
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 721
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ArtistDetailTopSongsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ArtistDetailTopSongsScreenViewModel;)V

    goto :goto_0
.end method

.method public getAttainmentDetailAdapter(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 637
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 638
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 639
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 641
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/AttainmentDetailScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)V

    goto :goto_0
.end method

.method public getBackgroundImageAdapter(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1259
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1260
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1261
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1263
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)V

    goto :goto_0
.end method

.method public getBundlesAdapter(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 886
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 887
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 888
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 890
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/BundlesScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/BundlesScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BundlesScreenViewModel;)V

    goto :goto_0
.end method

.method public getCastCrewAdapter(Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 527
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 528
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 529
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 531
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubAdminBannedScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1156
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1157
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1158
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1160
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubAdminHomeScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1116
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1117
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1118
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1120
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubAdminMembersScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1146
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1147
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1148
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1150
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminMembersScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubAdminReportsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1136
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1137
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1138
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1140
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubAdminRequestsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1126
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1127
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1128
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1130
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubAdminSettingsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1166
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1167
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1168
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1170
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubChatNotificationScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1096
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1097
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1098
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1100
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubChatScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1086
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1087
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1088
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1090
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubCreationScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1036
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1037
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1038
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1040
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)V

    goto :goto_0
.end method

.method public getClubCustomizeScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1046
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1047
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1048
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1050
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeViewModel;)V

    goto :goto_0
.end method

.method public getClubDiscoveryScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1016
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1017
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1018
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1020
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubHomeScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1066
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1067
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1068
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1070
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubInvitationsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1289
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1290
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1291
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1293
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubInviteScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1176
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1177
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1178
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1180
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubPivotScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1056
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1057
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1058
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1060
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubPlayScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1076
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1077
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1078
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1080
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubRecommendationScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1026
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1027
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1028
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1030
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubRecommendationScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubSearchScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1186
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1187
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1188
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1190
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)V

    goto :goto_0
.end method

.method public getClubWhosHereScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1106
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1107
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1108
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1110
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;)V

    goto :goto_0
.end method

.method public getCompanionDetailHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 257
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 258
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 259
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 261
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/CompanionDetailHeaderScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CompanionDetailHeaderScreenViewModel;)V

    goto :goto_0
.end method

.method public getComposeMessageAdapter(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 207
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 208
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 209
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 211
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;)V

    goto :goto_0
.end method

.method public getComposeMessageWithAttachementAdapter(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 217
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 218
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 221
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;)V

    goto :goto_0
.end method

.method public getConsoleConnectionAdapter(Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 367
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 368
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 369
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 371
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConsoleConnectionScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConsoleConnectionScreenViewModel;)V

    goto :goto_0
.end method

.method public getCreateLfgDetailsAdapter(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1206
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1207
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1208
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1210
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;-><init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)V

    goto :goto_0
.end method

.method public getCreateLfgSelectTitleAdapter(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1196
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1197
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1198
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1200
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleAdapter;-><init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleViewModel;)V

    goto :goto_0
.end method

.method public getCustomizeProfileScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 986
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 987
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 988
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 990
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/CustomizeProfileScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    goto :goto_0
.end method

.method public getDLCDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 707
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 708
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 709
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 711
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/DLCDetailHeaderAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/DLCDetailHeaderViewModel;)V

    goto :goto_0
.end method

.method public getDLCOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 697
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 698
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 699
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 701
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/DLCOverviewScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/DLCOverviewScreenViewModel;)V

    goto :goto_0
.end method

.method public getDrawerAdapter(Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 187
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 188
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 189
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 191
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/DrawerAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;)V

    goto :goto_0
.end method

.method public getEnforcementScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 996
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 997
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 998
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1000
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/EnforcementScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;)V

    goto :goto_0
.end method

.method public getExtrasAdapter(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 597
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 598
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 599
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 601
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ExtrasScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)V

    goto :goto_0
.end method

.method public getFriendsWhoPlayScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 926
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 927
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 928
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 930
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/FriendsWhoPlayScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;)V

    goto :goto_0
.end method

.method public getFutureShowtimesAdapter(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 557
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 558
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 559
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 561
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameAchievementComparisonScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 856
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 857
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 858
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 860
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V

    goto :goto_0
.end method

.method public getGameDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 687
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 688
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 689
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 691
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailHeaderAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailHeaderViewModel;)V

    goto :goto_0
.end method

.method public getGameDetailsProgressPhoneScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 677
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 678
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 679
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 681
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameDetailsProgressPhoneScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameDetailsProgressPhoneScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameGalleryAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 667
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 668
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 669
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 671
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameGalleryScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameGalleryScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 647
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 648
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 649
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 651
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameOverviewScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameOverviewScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameProfileAchievementsAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 787
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 788
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 789
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 791
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameProfileCapturesAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 507
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 508
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 509
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 511
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameProfileFriendsScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 936
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 937
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 938
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 940
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameProfileLfgScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1230
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1231
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1232
    .local v0, "adapter":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    if-eqz v0, :cond_0

    .line 1235
    .end local v0    # "adapter":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    :goto_0
    return-object v0

    .restart local v0    # "adapter":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;

    .end local v0    # "adapter":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileLfgScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameProfilePivotScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 467
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 468
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 469
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 471
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfilePivotScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfilePivotScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameProgressChallengesAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 457
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 458
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 459
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 461
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameProgressGameclipsAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 497
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 498
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 499
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 501
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)V

    goto :goto_0
.end method

.method public getGameprofileInfoAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 477
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 478
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 479
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 481
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileInfoScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)V

    goto :goto_0
.end method

.method public getGamerscoreLeaderboardAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 447
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 448
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 449
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 451
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardPageViewModel;)V

    goto :goto_0
.end method

.method public getIncludedContentAdapter(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1    # Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel",
            "<*>;)",
            "Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;"
        }
    .end annotation

    .prologue
    .line 876
    .local p1, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel<*>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 877
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 878
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 880
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/IncludedContentScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/IncludedContentScreenViewModel;)V

    goto :goto_0
.end method

.method public getLfgVettingScreenAdapter(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1240
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1241
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1242
    .local v0, "adapter":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    if-eqz v0, :cond_0

    .line 1245
    .end local v0    # "adapter":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    :goto_0
    return-object v0

    .restart local v0    # "adapter":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;

    .end local v0    # "adapter":Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;)V

    goto :goto_0
.end method

.method public getMessagesAdapter(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 197
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 198
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 201
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ConversationsActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationsActivityViewModel;)V

    goto :goto_0
.end method

.method public getMovieDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 377
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 378
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 379
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 381
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MovieDetailsHeaderAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/MovieDetailsHeaderViewModel;)V

    goto :goto_0
.end method

.method public getMovieOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 517
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 518
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 519
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 521
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MovieOverviewScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/MovieOverviewScreenViewModel;)V

    goto :goto_0
.end method

.method public getPageUserInfoScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1006
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1007
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1008
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1010
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PageUserInfoScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)V

    goto :goto_0
.end method

.method public getParentItemAdapter(Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 537
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 538
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 539
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 541
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ParentItemScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ParentItemScreenViewModel;)V

    goto :goto_0
.end method

.method public getPeopleActivityFeedAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 767
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 768
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 769
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 771
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;)V

    goto :goto_0
.end method

.method public getPeopleAdapter(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 757
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 758
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 759
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 761
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V

    goto :goto_0
.end method

.method public getPeopleHubAchievementsAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 437
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 438
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 439
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 441
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)V

    goto :goto_0
.end method

.method public getPeopleHubActivityScreenAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 337
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 338
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 339
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 341
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivityViewModel;)V

    goto :goto_0
.end method

.method public getPeopleHubCapturesScreenAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 357
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 358
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 359
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 361
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)V

    goto :goto_0
.end method

.method public getPeopleHubInfoScreenAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 347
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 348
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 349
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 351
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V

    goto :goto_0
.end method

.method public getPeopleHubSocialScreenAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 946
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 947
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 948
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 950
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubSocialScreenViewModel;)V

    goto :goto_0
.end method

.method public getPopularWithFriendsAdapter(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 427
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 428
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 429
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 431
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)V

    goto :goto_0
.end method

.method public getPurchaseWebviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 657
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 658
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 659
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 661
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PurchaseWebViewActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V

    goto :goto_0
.end method

.method public getRelatedScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/RelatedScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 327
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 328
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 329
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 331
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/RelatedScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;)V

    goto :goto_0
.end method

.method public getSearchDataResultAdapter(Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 287
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 288
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 289
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 291
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultsActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SearchResultsActivityViewModel;)V

    goto :goto_0
.end method

.method public getSettingsAdapter(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 227
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 228
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 229
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 231
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V

    goto :goto_0
.end method

.method public getSettingsNotificationsPageAdapter(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1279
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1280
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1281
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1283
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageAdapter;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsNotificationsPageViewModel;)V

    goto :goto_0
.end method

.method public getSettingsPivotScreenAdapter(Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1269
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1270
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1271
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1273
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreenViewModel;)V

    goto :goto_0
.end method

.method public getStoreGoldFullScreenItemsAdapter(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 407
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 408
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 409
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 411
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)V

    goto :goto_0
.end method

.method public getStoreGoldItemsAdapter(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 397
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 398
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 399
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 401
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreGoldItemsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StoreGoldItemsDataViewModel;)V

    goto :goto_0
.end method

.method public getStoreItemsAdapter(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 387
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 388
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 389
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 391
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StoreItemsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;)V

    goto :goto_0
.end method

.method public getStorePivotScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 417
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 418
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 419
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 421
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/StorePivotScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/StorePivotScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;)V

    goto :goto_0
.end method

.method public getSuggestionsPeopleScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 956
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 957
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 958
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 960
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)V

    goto :goto_0
.end method

.method public getSystemTagSelectorScreenAdapter(Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1216
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1217
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1218
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1223
    .end local p1    # "viewModel":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    :goto_0
    return-object v0

    .line 1220
    .restart local p1    # "viewModel":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    :cond_0
    instance-of v0, p1, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;

    if-eqz v0, :cond_1

    .line 1221
    new-instance v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenAdapter;

    check-cast p1, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;

    .end local p1    # "viewModel":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;)V

    goto :goto_0

    .line 1223
    .restart local p1    # "viewModel":Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;)V

    goto :goto_0
.end method

.method public getTVEpisodeDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 607
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 608
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 609
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 611
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeDetailsHeaderAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeDetailsHeaderViewModel;)V

    goto :goto_0
.end method

.method public getTVEpisodeOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 617
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 618
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 619
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 621
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeOverviewScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeOverviewScreenViewModel;)V

    goto :goto_0
.end method

.method public getTVEpisodeSeasonAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 627
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 628
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 629
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 631
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)V

    goto :goto_0
.end method

.method public getTVSeriesDetailsHeaderAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 567
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 568
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 569
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 571
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesDetailsHeaderAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesDetailsHeaderViewModel;)V

    goto :goto_0
.end method

.method public getTVSeriesOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 577
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 578
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 579
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 581
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesOverviewScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;)V

    goto :goto_0
.end method

.method public getTVSeriesSeasonsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 587
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 588
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 589
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 591
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TVSeriesSeasonsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesSeasonsScreenViewModel;)V

    goto :goto_0
.end method

.method public getTitleLeaderboardAdapter(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 487
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 488
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 489
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 491
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TitleLeaderboardScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)V

    goto :goto_0
.end method

.method public getTitlePickerScreenAdapter(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1250
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1251
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1252
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1254
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel;)V

    goto :goto_0
.end method

.method public getTrendingTopicScreenAdapter(Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 976
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 977
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 978
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 980
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreenViewModel;)V

    goto :goto_0
.end method

.method public getTrendingXblScreenAdapter(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 966
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 967
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 968
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 970
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;)V

    goto :goto_0
.end method

.method public getTvHubTabletAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 797
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 798
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 799
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 801
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvHubTabletScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvHubTabletScreenViewModel;)V

    goto :goto_0
.end method

.method public getTvListingsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 836
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 837
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 838
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 840
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)V

    goto :goto_0
.end method

.method public getTvMyChannelsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 826
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 827
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 828
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 830
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvMyChannelsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)V

    goto :goto_0
.end method

.method public getTvMyShowsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 816
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 817
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 818
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 820
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvMyShowsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;)V

    goto :goto_0
.end method

.method public getTvRecentChannelsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 846
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 847
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 848
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 850
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvRecentChannelsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvRecentChannelsScreenViewModel;)V

    goto :goto_0
.end method

.method public getTvTrendingAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 807
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 808
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 810
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)V

    goto :goto_0
.end method

.method public getUnsharedActivityFeedScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 777
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 778
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 779
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 781
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;)V

    goto :goto_0
.end method

.method public getUploadCustomPicScreenAdapter(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1299
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1300
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1301
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->getAdapter(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    .line 1303
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenAdapter;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;)V

    goto :goto_0
.end method
