.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PeopleHubAchievementsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRecentProgressAndAchievementAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Lcom/microsoft/xbox/service/model/ProfileModel;)V
    .locals 0
    .param p2, "profileModel"    # Lcom/microsoft/xbox/service/model/ProfileModel;

    .prologue
    .line 666
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 667
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 668
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 672
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 673
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshRecent360ProgressAndAchievements()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshRecentProgressAndAchievements()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 709
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 710
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadRecentProgressAndAchievments(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 662
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 704
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 662
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 4

    .prologue
    .line 678
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 679
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->forceLoad:Z

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/ProfileModel;Z)V

    .line 680
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 699
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->forceLoad:Z

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/ProfileModel;Z)V

    .line 700
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 662
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 684
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 685
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 686
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Z)Z

    .line 694
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)V

    .line 695
    return-void

    .line 688
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Z)Z

    goto :goto_0
.end method
