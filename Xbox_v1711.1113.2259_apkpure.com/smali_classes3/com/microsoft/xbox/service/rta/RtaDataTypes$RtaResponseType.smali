.class public final enum Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
.super Ljava/lang/Enum;
.source "RtaDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/rta/RtaDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RtaResponseType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

.field public static final enum Event:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

.field private static MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum Subscribe:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

.field public static final enum Unsubscribe:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

.field public static final enum UnsubscribeAll:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 28
    new-instance v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    const-string v3, "Unknown"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->Unknown:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    .line 29
    new-instance v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    const-string v3, "Subscribe"

    invoke-direct {v2, v3, v4, v4}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->Subscribe:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    .line 30
    new-instance v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    const-string v3, "Unsubscribe"

    invoke-direct {v2, v3, v5, v5}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->Unsubscribe:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    .line 31
    new-instance v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    const-string v3, "Event"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->Event:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    .line 32
    new-instance v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    const-string v3, "UnsubscribeAll"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->UnsubscribeAll:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    .line 27
    const/4 v2, 0x5

    new-array v2, v2, [Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    sget-object v3, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->Unknown:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->Subscribe:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    aput-object v3, v2, v4

    sget-object v3, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->Unsubscribe:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    aput-object v3, v2, v5

    sget-object v3, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->Event:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->UnsubscribeAll:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    aput-object v3, v2, v7

    sput-object v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->$VALUES:[Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    .line 34
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->MAP:Ljava/util/Map;

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->values()[Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    move-result-object v2

    array-length v3, v2

    .local v0, "responseType":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 39
    sget-object v4, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->MAP:Ljava/util/Map;

    iget v5, v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->value:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 41
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput p3, p0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->value:I

    .line 51
    return-void
.end method

.method public static valueOf(I)Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    .locals 3
    .param p0, "responseTypeCode"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 45
    sget-object v1, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->MAP:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    .line 46
    .local v0, "responseType":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    if-eqz v0, :cond_0

    .end local v0    # "responseType":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    :goto_0
    return-object v0

    .restart local v0    # "responseType":Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->Unknown:Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->$VALUES:[Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponseType;->value:I

    return v0
.end method
