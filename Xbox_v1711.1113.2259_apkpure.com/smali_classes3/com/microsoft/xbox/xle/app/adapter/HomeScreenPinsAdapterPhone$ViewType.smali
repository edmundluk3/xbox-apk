.class final enum Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;
.super Ljava/lang/Enum;
.source "HomeScreenPinsAdapterPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

.field public static final enum MORE:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

.field public static final enum PIN:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    const-string v1, "PIN"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->PIN:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    const-string v1, "MORE"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->MORE:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    .line 148
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->PIN:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->MORE:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 148
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;

    return-object v0
.end method
