.class public final Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;
.super Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
.source "TournamentDetailsPageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TournamentDetailsPageParameters"
.end annotation


# instance fields
.field public final id:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final organizer:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "organizer"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 68
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 69
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;->organizer:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;->id:Ljava/lang/String;

    .line 73
    return-void
.end method
