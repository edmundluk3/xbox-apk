.class public Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
.super Landroid/widget/EditText;
.source "CustomTypefaceEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;
    }
.end annotation


# instance fields
.field private container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

.field private keyStrokeListener:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;

.field private textOrSelectionChangedRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->textOrSelectionChangedRunnable:Ljava/lang/Runnable;

    .line 37
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->hookTextChangedEvent()V

    .line 39
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->addViewThatCausesAndroidLeaks(Landroid/view/View;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->textOrSelectionChangedRunnable:Ljava/lang/Runnable;

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->processStyledAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->textOrSelectionChangedRunnable:Ljava/lang/Runnable;

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->processStyledAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;)Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->textOrSelectionChangedEvent()V

    return-void
.end method

.method private applyCustomTypeface(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 168
    if-eqz p2, :cond_0

    .line 169
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 170
    .local v0, "tf":Landroid/graphics/Typeface;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 172
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void
.end method

.method private hookTextChangedEvent()V
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$2;-><init>(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 159
    return-void
.end method

.method private processStyledAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 128
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 129
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "typeface":Ljava/lang/String;
    invoke-direct {p0, p1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->applyCustomTypeface(Landroid/content/Context;Ljava/lang/String;)V

    .line 132
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->hookTextChangedEvent()V

    .line 134
    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 135
    .local v1, "deleteOnActivityDestroyOrTombstone":Z
    if-eqz v1, :cond_0

    .line 136
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->addViewThatCausesAndroidLeaks(Landroid/view/View;)V

    .line 139
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 140
    return-void
.end method

.method private textOrSelectionChangedEvent()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->textOrSelectionChangedRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->textOrSelectionChangedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 165
    :cond_0
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Landroid/widget/EditText;->onFinishInflate()V

    .line 110
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 118
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->keyStrokeListener:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->keyStrokeListener:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;

    invoke-interface {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 97
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSelectionChanged(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 122
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onSelectionChanged(II)V

    .line 124
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->textOrSelectionChangedEvent()V

    .line 125
    return-void
.end method

.method public setContainer(Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;)V
    .locals 2
    .param p1, "parent"    # Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    .prologue
    const/4 v1, 0x1

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    if-eqz v0, :cond_0

    .line 78
    const-string v0, "EditViewFixedLength"

    const-string v1, "container is set multiple times"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setFocusable(Z)V

    .line 81
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setFocusableInTouchMode(Z)V

    .line 83
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->container:Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;->addChild(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected setCursorPosition_internal(II)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 61
    .local v0, "textLength":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v1, v2, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 62
    return-void
.end method

.method public setKeyStrokeListener(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->keyStrokeListener:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText$KeyStrokeListener;

    .line 102
    return-void
.end method

.method protected setSpan_internal(Ljava/lang/Object;III)V
    .locals 3
    .param p1, "span"    # Ljava/lang/Object;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "flags"    # I

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v0

    .line 50
    .local v0, "textLength":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-interface {v1, p1, p2, v2, p4}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 51
    return-void
.end method

.method public setTextOrSelectionChangedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->textOrSelectionChangedRunnable:Ljava/lang/Runnable;

    .line 90
    return-void
.end method
