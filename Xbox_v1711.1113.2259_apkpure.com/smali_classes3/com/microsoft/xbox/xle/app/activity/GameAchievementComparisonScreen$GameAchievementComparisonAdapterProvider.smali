.class Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen$GameAchievementComparisonAdapterProvider;
.super Ljava/lang/Object;
.source "GameAchievementComparisonScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GameAchievementComparisonAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen$GameAchievementComparisonAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen$1;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen$GameAchievementComparisonAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;)V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 60
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .line 61
    .local v0, "gacViewModel":Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 62
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getGameAchievementComparisonScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
