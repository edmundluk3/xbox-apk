.class Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents$1;
.super Ljava/lang/Object;
.source "HomeScreenPopupScreenViewModelRecents.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
        "<",
        "Lcom/microsoft/xbox/service/model/recents/Recent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;->computeHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderData()Lcom/microsoft/xbox/service/model/recents/Recent;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;->access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;)Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getHeaderData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents$1;->getHeaderData()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v0

    return-object v0
.end method

.method public getPlaceholderRid()I
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelRecents;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;->getPlaceholderRid(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)I

    move-result v0

    return v0
.end method
