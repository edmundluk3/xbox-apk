.class public Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;
.super Lcom/microsoft/xbox/xle/ui/XLERootView;
.source "XLEHandleImeRootView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;
    }
.end annotation


# static fields
.field private static final KEY_BOARD_SHOW_HEIGHT:I


# instance fields
.field private handleImeInterface:Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f09004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sput v0, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->KEY_BOARD_SHOW_HEIGHT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/XLERootView;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/XLERootView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 39
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 40
    .local v2, "proposedHeight":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->getHeight()I

    move-result v0

    .line 42
    .local v0, "actualHeight":I
    sub-int v1, v2, v0

    .line 43
    .local v1, "diffHeight":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    sget v4, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->KEY_BOARD_SHOW_HEIGHT:I

    if-le v3, v4, :cond_0

    .line 44
    if-lez v1, :cond_1

    .line 45
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->handleImeInterface:Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;

    if-eqz v3, :cond_0

    .line 46
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->handleImeInterface:Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;->onDismissKeyboard()V

    .line 55
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/XLERootView;->onMeasure(II)V

    .line 56
    return-void

    .line 49
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->handleImeInterface:Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;

    if-eqz v3, :cond_0

    .line 50
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->handleImeInterface:Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;->onShowKeyboard()V

    goto :goto_0
.end method

.method public setHandleImeInterface(Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;)V
    .locals 1
    .param p1, "handleImeInterface"    # Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->handleImeInterface:Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->handleImeInterface:Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;->onDismissKeyboard()V

    .line 34
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView;->handleImeInterface:Lcom/microsoft/xbox/xle/ui/XLEHandleImeRootView$HandleImeInterface;

    .line 35
    return-void
.end method
