.class public final enum Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
.super Ljava/lang/Enum;
.source "ClubAdminSettingsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SettingSpinnerOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

.field public static final enum Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

.field public static final enum Moderators:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

.field public static final enum ModeratorsAndOwner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

.field public static final enum Owner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    const-string v1, "Members"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 57
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    const-string v1, "Moderators"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Moderators:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 58
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    const-string v1, "ModeratorsAndOwner"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ModeratorsAndOwner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 59
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    const-string v1, "Owner"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Owner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    .line 55
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Members:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Moderators:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ModeratorsAndOwner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->Owner:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 55
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;

    return-object v0
.end method


# virtual methods
.method public getMinimumEquivalentRole()Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .locals 2

    .prologue
    .line 78
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$admin$ClubAdminSettingsScreenViewModel$SettingSpinnerOption:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown option:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 89
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    :goto_0
    return-object v0

    .line 80
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    goto :goto_0

    .line 82
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    goto :goto_0

    .line 84
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    goto :goto_0

    .line 86
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    goto :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$clubs$admin$ClubAdminSettingsScreenViewModel$SettingSpinnerOption:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "String not set for SettingSpinnerOption."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminSettingsScreenViewModel$SettingSpinnerOption;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 73
    const-string v0, ""

    :goto_0
    return-object v0

    .line 64
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070295

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 66
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070296

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0702ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 70
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070298

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
