.class Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "AppChannelsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ToggleFavoriteAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private mChanId:Ljava/lang/String;

.field private mChanName:Ljava/lang/String;

.field private mFav:Z

.field private mProvId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

.field private wasListFull:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "fav"    # Z
    .param p3, "provId"    # Ljava/lang/String;
    .param p4, "chanId"    # Ljava/lang/String;
    .param p5, "chanName"    # Ljava/lang/String;

    .prologue
    .line 707
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 705
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->wasListFull:Z

    .line 708
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mFav:Z

    .line 709
    iput-object p3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mProvId:Ljava/lang/String;

    .line 710
    iput-object p4, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mChanId:Ljava/lang/String;

    .line 711
    iput-object p5, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mChanName:Ljava/lang/String;

    .line 712
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 716
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 761
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mFav:Z

    if-eqz v1, :cond_0

    .line 762
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mProvId:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mChanId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->addAppChannelFavorite(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 766
    .local v0, "success":Z
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_1
    return-object v1

    .line 764
    .end local v0    # "success":Z
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mProvId:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mChanId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->removeAppChannelFavorite(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .restart local v0    # "success":Z
    goto :goto_0

    .line 766
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 700
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 751
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 700
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 756
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 7
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 725
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    const/4 v6, 0x0

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->access$402(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;

    .line 726
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 727
    iget-object v6, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mFav:Z

    if-nez v3, :cond_2

    move v3, v4

    :goto_0
    invoke-static {v6, v3}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->access$502(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Z)Z

    .line 728
    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    const-string v6, "favorite"

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->notify(Ljava/lang/String;)V

    .line 730
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mFav:Z

    if-eqz v3, :cond_3

    .line 731
    const v3, 0x7f070451

    invoke-static {v5, v3, v5}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->show(IIZ)V

    .line 737
    :cond_0
    :goto_1
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->wasListFull:Z

    if-eqz v3, :cond_1

    .line 738
    const v3, 0x7f0300f8

    invoke-static {v3, v4, v4}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->show(IZZ)Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;

    .line 739
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/DialogManager;->getVisibleDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 740
    .local v0, "d":Landroid/app/Dialog;
    const v3, 0x7f0e058f

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 741
    .local v2, "textView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v2, :cond_1

    .line 742
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 743
    .local v1, "text":Ljava/lang/String;
    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->mChanName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 744
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 747
    .end local v0    # "d":Landroid/app/Dialog;
    .end local v1    # "text":Ljava/lang/String;
    .end local v2    # "textView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_1
    return-void

    :cond_2
    move v3, v5

    .line 727
    goto :goto_0

    .line 733
    :cond_3
    const v3, 0x7f070452

    invoke-static {v5, v3, v5}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->show(IIZ)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 700
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel$ToggleFavoriteAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 721
    return-void
.end method
