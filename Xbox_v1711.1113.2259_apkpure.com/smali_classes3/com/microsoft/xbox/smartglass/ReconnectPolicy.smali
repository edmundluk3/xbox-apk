.class public Lcom/microsoft/xbox/smartglass/ReconnectPolicy;
.super Ljava/lang/Object;
.source "ReconnectPolicy.java"


# instance fields
.field public final maxRetryCount:I

.field public final retryInterval:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "maxRetryCount"    # I
    .param p2, "retryInterval"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;->maxRetryCount:I

    .line 26
    iput p2, p0, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;->retryInterval:I

    .line 27
    return-void
.end method

.method public static getDefault()Lcom/microsoft/xbox/smartglass/ReconnectPolicy;
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;

    const/4 v1, 0x5

    const/16 v2, 0x1f4

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/ReconnectPolicy;-><init>(II)V

    return-object v0
.end method
