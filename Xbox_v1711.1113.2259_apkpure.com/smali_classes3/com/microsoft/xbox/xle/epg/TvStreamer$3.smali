.class Lcom/microsoft/xbox/xle/epg/TvStreamer$3;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;-><init>(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$3;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 197
    const-string v1, "TvStreamer"

    const-string v2, "MTC: OneGuide button pressed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Stream FullScreen OneGuide"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 200
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 201
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$3;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setupPipUI()V

    .line 205
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 206
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 207
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/TvHubPhoneScreen;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 209
    :cond_1
    return-void

    .line 203
    .end local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$3;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->backgroundStreamerUI()V

    goto :goto_0
.end method
