.class public abstract Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;
.super Ljava/lang/Object;
.source "PartyDataChannelTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MemberSeenMessage"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_MemberSeenMessage$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_MemberSeenMessage$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(IJ)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$MemberSeenMessage;
    .locals 3
    .param p0, "mpsdMemberIndex"    # I
    .param p1, "xuid"    # J

    .prologue
    .line 70
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_MemberSeenMessage;

    const-string v1, "mpsdMemberSeen"

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_MemberSeenMessage;-><init>(Ljava/lang/String;IJ)V

    return-object v0
.end method


# virtual methods
.method public abstract mpsdMemberIndex()I
.end method

.method public abstract type()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract xuid()J
.end method
