.class public Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "TVEpisodeSeasonScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;",
        ">;"
    }
.end annotation


# instance fields
.field private loadingSeasonDetail:Z

.field private seasonModelLoaded:Z

.field private seasonTask:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 19
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->seasonModelLoaded:Z

    .line 20
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->loadingSeasonDetail:Z

    .line 23
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVEpisodeSeasonAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 24
    return-void
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->loadingSeasonDetail:Z

    return p1
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->seasonModelLoaded:Z

    return p1
.end method


# virtual methods
.method public getEpisodes()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getParentSeason()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    move-result-object v0

    .line 68
    .local v0, "parentSeason":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;->getEpisodes()Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f070683

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->loadingSeasonDetail:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public navigateToTvEpisodeDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    .prologue
    .line 72
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 73
    return-void
.end method

.method public onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onLoadDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 45
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->seasonModelLoaded:Z

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->getParentSeason()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 47
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->seasonTask:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->seasonTask:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->load(Z)V

    .line 51
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTVEpisodeSeasonAdapter(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 29
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->onStopOverride()V

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->seasonTask:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->seasonTask:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel$LoadSeasonDetailsTask;->cancel()V

    .line 59
    :cond_0
    return-void
.end method
