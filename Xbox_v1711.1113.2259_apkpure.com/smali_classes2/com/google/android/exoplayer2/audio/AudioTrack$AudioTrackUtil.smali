.class Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;
.super Ljava/lang/Object;
.source "AudioTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/audio/AudioTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AudioTrackUtil"
.end annotation


# instance fields
.field protected audioTrack:Landroid/media/AudioTrack;

.field private endPlaybackHeadPosition:J

.field private final getLatencyMethod:Ljava/lang/reflect/Method;

.field private lastRawPlaybackHeadPosition:J

.field private final log:Lcom/google/android/exoplayer2/util/Logger;

.field private needsPassthroughWorkaround:Z

.field private passthroughWorkaroundPauseOffset:J

.field private rawPlaybackHeadWrapCount:J

.field private resumeTime:J

.field private sampleRate:I

.field private stopPlaybackHeadPosition:J

.field private stopTimestampUs:J


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Method;)V
    .locals 3
    .param p1, "getLatencyMethod"    # Ljava/lang/reflect/Method;

    .prologue
    .line 1465
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1462
    new-instance v0, Lcom/google/android/exoplayer2/util/Logger;

    sget-object v1, Lcom/google/android/exoplayer2/util/Logger$Module;->Audio:Lcom/google/android/exoplayer2/util/Logger$Module;

    invoke-static {}, Lcom/google/android/exoplayer2/audio/AudioTrack;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer2/util/Logger;-><init>(Lcom/google/android/exoplayer2/util/Logger$Module;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->log:Lcom/google/android/exoplayer2/util/Logger;

    .line 1466
    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->getLatencyMethod:Ljava/lang/reflect/Method;

    .line 1467
    return-void
.end method

.method private getAudioSWLatencies()I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1470
    iget-object v2, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->getLatencyMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    move v2, v3

    .line 1478
    :goto_0
    return v2

    .line 1475
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->getLatencyMethod:Ljava/lang/reflect/Method;

    iget-object v5, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->audioTrack:Landroid/media/AudioTrack;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1476
    .local v1, "swLatencyMs":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget v4, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->sampleRate:I

    div-int/lit16 v3, v4, 0x3e8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    mul-int/2addr v2, v3

    goto :goto_0

    .line 1477
    .end local v1    # "swLatencyMs":Ljava/lang/Integer;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move v2, v3

    .line 1478
    goto :goto_0
.end method


# virtual methods
.method public getPlaybackHeadPosition()J
    .locals 14

    .prologue
    .line 1545
    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->stopTimestampUs:J

    const-wide v12, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v9, v10, v12

    if-eqz v9, :cond_0

    .line 1547
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    iget-wide v12, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->stopTimestampUs:J

    sub-long v0, v10, v12

    .line 1548
    .local v0, "elapsedTimeSinceStopUs":J
    iget v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->sampleRate:I

    int-to-long v10, v9

    mul-long/2addr v10, v0

    const-wide/32 v12, 0xf4240

    div-long v2, v10, v12

    .line 1549
    .local v2, "framesSinceStop":J
    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->endPlaybackHeadPosition:J

    iget-wide v12, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->stopPlaybackHeadPosition:J

    add-long/2addr v12, v2

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    .line 1604
    .end local v0    # "elapsedTimeSinceStopUs":J
    .end local v2    # "framesSinceStop":J
    :goto_0
    return-wide v10

    .line 1552
    :cond_0
    iget-object v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v9}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v5

    .line 1553
    .local v5, "state":I
    const/4 v9, 0x1

    if-ne v5, v9, :cond_1

    .line 1555
    const-wide/16 v10, 0x0

    goto :goto_0

    .line 1559
    :cond_1
    const-wide/16 v6, 0x0

    .line 1560
    .local v6, "rawPlaybackHeadPosition":J
    invoke-static {}, Lcom/google/android/exoplayer2/audio/AudioTrack;->access$300()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1561
    iget-object v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v9}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v4

    .line 1565
    .local v4, "php":I
    iget-object v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v9}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v8

    .line 1566
    .local v8, "trackState":I
    const/4 v9, 0x3

    if-eq v8, v9, :cond_2

    const/4 v9, 0x2

    if-ne v8, v9, :cond_3

    if-eqz v4, :cond_3

    .line 1568
    :cond_2
    invoke-direct {p0}, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->getAudioSWLatencies()I

    move-result v9

    add-int/2addr v4, v9

    .line 1570
    :cond_3
    if-gez v4, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->resumeTime:J

    sub-long/2addr v10, v12

    const-wide/16 v12, 0x3e8

    cmp-long v9, v10, v12

    if-gez v9, :cond_4

    .line 1571
    const/4 v4, 0x0

    .line 1572
    iget-object v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v10, "php is negative during latency stabilization phase ...resetting to 0"

    invoke-virtual {v9, v10}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 1574
    :cond_4
    const-wide v10, 0xffffffffL

    int-to-long v12, v4

    and-long v6, v10, v12

    .line 1575
    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->lastRawPlaybackHeadPosition:J

    cmp-long v9, v10, v6

    if-lez v9, :cond_5

    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->lastRawPlaybackHeadPosition:J

    const-wide/32 v12, 0x7fffffff

    cmp-long v9, v10, v12

    if-lez v9, :cond_5

    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->lastRawPlaybackHeadPosition:J

    sub-long/2addr v10, v6

    const-wide/32 v12, 0x7fffffff

    cmp-long v9, v10, v12

    if-ltz v9, :cond_5

    .line 1579
    iget-object v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v10, "The playback head position wrapped around"

    invoke-virtual {v9, v10}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 1580
    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->rawPlaybackHeadWrapCount:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->rawPlaybackHeadWrapCount:J

    .line 1602
    .end local v4    # "php":I
    .end local v8    # "trackState":I
    :cond_5
    :goto_1
    iput-wide v6, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->lastRawPlaybackHeadPosition:J

    .line 1604
    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->rawPlaybackHeadWrapCount:J

    const/16 v9, 0x20

    shl-long/2addr v10, v9

    add-long/2addr v10, v6

    goto :goto_0

    .line 1584
    :cond_6
    const-wide v10, 0xffffffffL

    iget-object v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v9}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v9

    int-to-long v12, v9

    and-long v6, v10, v12

    .line 1585
    iget-object v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->log:Lcom/google/android/exoplayer2/util/Logger;

    invoke-virtual {v9}, Lcom/google/android/exoplayer2/util/Logger;->allowVerbose()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1586
    iget-object v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->log:Lcom/google/android/exoplayer2/util/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "rawPlaybackHeadPosition = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/exoplayer2/util/Logger;->v(Ljava/lang/String;)V

    .line 1588
    :cond_7
    iget-boolean v9, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->needsPassthroughWorkaround:Z

    if-eqz v9, :cond_9

    .line 1592
    const/4 v9, 0x2

    if-ne v5, v9, :cond_8

    const-wide/16 v10, 0x0

    cmp-long v9, v6, v10

    if-nez v9, :cond_8

    .line 1593
    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->lastRawPlaybackHeadPosition:J

    iput-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->passthroughWorkaroundPauseOffset:J

    .line 1595
    :cond_8
    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->passthroughWorkaroundPauseOffset:J

    add-long/2addr v6, v10

    .line 1597
    :cond_9
    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->lastRawPlaybackHeadPosition:J

    cmp-long v9, v10, v6

    if-lez v9, :cond_5

    .line 1599
    iget-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->rawPlaybackHeadWrapCount:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->rawPlaybackHeadWrapCount:J

    goto :goto_1
.end method

.method public getPlaybackHeadPositionUs()J
    .locals 4

    .prologue
    .line 1611
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->getPlaybackHeadPosition()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget v2, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->sampleRate:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getPlaybackSpeed()F
    .locals 1

    .prologue
    .line 1673
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public getTimestampFramePosition()J
    .locals 1

    .prologue
    .line 1651
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getTimestampNanoTime()J
    .locals 1

    .prologue
    .line 1635
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public handleEndOfStream(J)V
    .locals 5
    .param p1, "submittedFrames"    # J

    .prologue
    .line 1515
    invoke-virtual {p0}, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->getPlaybackHeadPosition()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->stopPlaybackHeadPosition:J

    .line 1516
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->stopTimestampUs:J

    .line 1517
    iput-wide p1, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->endPlaybackHeadPosition:J

    .line 1518
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v1, "calling stop"

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 1519
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    .line 1520
    return-void
.end method

.method public pause()V
    .locals 4

    .prologue
    .line 1527
    iget-wide v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->stopTimestampUs:J

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1533
    :goto_0
    return-void

    .line 1531
    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->log:Lcom/google/android/exoplayer2/util/Logger;

    const-string v1, "calling pause"

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer2/util/Logger;->i(Ljava/lang/String;)V

    .line 1532
    iget-object v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    goto :goto_0
.end method

.method public play()V
    .locals 2

    .prologue
    .line 1483
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->resumeTime:J

    .line 1484
    return-void
.end method

.method public reconfigure(Landroid/media/AudioTrack;Z)V
    .locals 4
    .param p1, "audioTrack"    # Landroid/media/AudioTrack;
    .param p2, "needsPassthroughWorkaround"    # Z

    .prologue
    const-wide/16 v2, 0x0

    .line 1496
    iput-object p1, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->audioTrack:Landroid/media/AudioTrack;

    .line 1497
    iput-boolean p2, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->needsPassthroughWorkaround:Z

    .line 1498
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->stopTimestampUs:J

    .line 1499
    iput-wide v2, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->lastRawPlaybackHeadPosition:J

    .line 1500
    iput-wide v2, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->rawPlaybackHeadWrapCount:J

    .line 1501
    iput-wide v2, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->passthroughWorkaroundPauseOffset:J

    .line 1502
    if-eqz p1, :cond_0

    .line 1503
    invoke-virtual {p1}, Landroid/media/AudioTrack;->getSampleRate()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer2/audio/AudioTrack$AudioTrackUtil;->sampleRate:I

    .line 1505
    :cond_0
    return-void
.end method

.method public setPlaybackParams(Landroid/media/PlaybackParams;)V
    .locals 1
    .param p1, "playbackParams"    # Landroid/media/PlaybackParams;

    .prologue
    .line 1663
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public updateTimestamp()Z
    .locals 1

    .prologue
    .line 1621
    const/4 v0, 0x0

    return v0
.end method
