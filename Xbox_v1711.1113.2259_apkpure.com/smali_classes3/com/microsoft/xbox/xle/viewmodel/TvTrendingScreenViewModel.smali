.class public Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "TvTrendingScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;
    }
.end annotation


# instance fields
.field private isLoading:Z

.field private liveTop10Items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;",
            ">;"
        }
    .end annotation
.end field

.field private loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;

.field private popularVodItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 16
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 25
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvTrendingAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 26
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->onLoadDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->isLoading:Z

    return p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->loadMockLiveTop10()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->loadMockPopularVod()V

    return-void
.end method

.method private loadMockLiveTop10()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 41
    const-string v0, "http://placehold.it/400x572"

    .line 43
    .local v0, "mockImageURI":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Justified"

    const-string v4, "S3 EP5"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Baseball Tonight"

    invoke-direct {v2, v0, v3, v5}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Portlandia"

    const-string v4, "S2 EP11"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Game of Thrones"

    const-string v4, "S3 EP6"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Sherlock"

    const-string v4, "S2 EP3"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Justified"

    const-string v4, "S3 EP5"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Baseball Tonight"

    invoke-direct {v2, v0, v3, v5}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Portlandia"

    const-string v4, "S2 EP11"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Game of Thrones"

    const-string v4, "S3 EP6"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Sherlock"

    const-string v4, "S2 EP3"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Justified"

    const-string v4, "S3 EP5"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Baseball Tonight"

    invoke-direct {v2, v0, v3, v5}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Portlandia"

    const-string v4, "S2 EP11"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Game of Thrones"

    const-string v4, "S3 EP6"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    const-string v3, "Sherlock"

    const-string v4, "S2 EP3"

    invoke-direct {v2, v0, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    return-void
.end method

.method private loadMockPopularVod()V
    .locals 3

    .prologue
    .line 62
    const-string v0, "http://placehold.it/300x400"

    .line 64
    .local v0, "mockImageURI":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    return-void
.end method

.method private onLoadDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->isLoading:Z

    .line 127
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 141
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->updateAdapter()V

    .line 142
    return-void

    .line 131
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 135
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    if-nez v0, :cond_0

    .line 136
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getLiveTop10Data()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    return-object v0
.end method

.method public getPopularVodData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->popularVodItems:Ljava/util/List;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->isLoading:Z

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->cancel()V

    .line 149
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->load(Z)V

    .line 151
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvTrendingAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 88
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->liveTop10Items:Ljava/util/List;

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->cancel()V

    .line 122
    :cond_0
    return-void
.end method
