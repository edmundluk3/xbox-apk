.class public interface abstract Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;
.super Ljava/lang/Object;
.source "WelcomeCardCompletionRepository.java"


# virtual methods
.method public abstract getStates()Lio/reactivex/Observable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;",
            ">;"
        }
    .end annotation
.end method

.method public abstract resetStates()V
.end method

.method public abstract setClubWelcomeCardCompleted(Z)V
.end method

.method public abstract setFacebookWelcomeCardCompleted(Z)V
.end method

.method public abstract setRedeemWelcomeCardCompleted(Z)V
.end method

.method public abstract setSetupWelcomeCardCompleted(Z)V
.end method

.method public abstract setShopWelcomeCardCompleted(Z)V
.end method

.method public abstract setSuggestedFriendsWelcomeCardCompleted(Z)V
.end method
