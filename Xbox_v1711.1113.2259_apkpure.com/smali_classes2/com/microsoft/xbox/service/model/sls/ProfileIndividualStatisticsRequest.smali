.class public Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest;
.super Ljava/lang/Object;
.source "ProfileIndividualStatisticsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;
    }
.end annotation


# static fields
.field private static final arrangeByTitle:Ljava/lang/String; = "scid"


# instance fields
.field public arrangebyfield:Ljava/lang/String;

.field public stats:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;",
            ">;"
        }
    .end annotation
.end field

.field public xuids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest;->stats:Ljava/util/ArrayList;

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest;->xuids:Ljava/util/ArrayList;

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "xuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "stats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest$IndividualStatistics;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest;->xuids:Ljava/util/ArrayList;

    .line 23
    const-string v0, "scid"

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest;->arrangebyfield:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest;->stats:Ljava/util/ArrayList;

    .line 25
    return-void
.end method

.method public static getProfileIndividualStatRequestBody(Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest;)Ljava/lang/String;
    .locals 4
    .param p0, "profileIndividualStatisticsRequest"    # Lcom/microsoft/xbox/service/model/sls/ProfileIndividualStatisticsRequest;

    .prologue
    .line 29
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 33
    :goto_0
    return-object v1

    .line 30
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ProfileIndividualStatisticsRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialzation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const/4 v1, 0x0

    goto :goto_0
.end method
