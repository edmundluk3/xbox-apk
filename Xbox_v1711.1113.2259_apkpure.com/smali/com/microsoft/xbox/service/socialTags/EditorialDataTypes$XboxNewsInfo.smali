.class public abstract Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;
.super Ljava/lang/Object;
.source "EditorialDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "XboxNewsInfo"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    new-instance v0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_XboxNewsInfo$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract bgColor()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "BgColor"
    .end annotation
.end method

.method public abstract bgImage()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "BgImage"
    .end annotation
.end method

.method public abstract description()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Description"
    .end annotation
.end method

.method public abstract featuredURL()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "FeaturedURL"
    .end annotation
.end method

.method public abstract iconImage()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IconImage"
    .end annotation
.end method

.method public abstract profileName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProfileName"
    .end annotation
.end method
