.class public Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;
.super Ljava/lang/Object;
.source "MyClubsNavigator.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

.field private final telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/inject/Named;
            value = "app_context"
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .param p3, "telemetryService"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;->context:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 32
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    .line 33
    return-void
.end method


# virtual methods
.method public navigateToClub(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 4
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 36
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;->telemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->inboxClubChatSelected(J)V

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsNavigator;->context:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addClubInstance(Landroid/content/Context;J)V

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    const-string v0, "Not implemented"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method
