.class public final Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ClubModeratorPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;
    }
.end annotation


# instance fields
.field private doneHandler:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    .local p3, "doneHandler":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    const v1, 0x7f080238

    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 37
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 38
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 40
    const v1, 0x7f03008e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->setContentView(I)V

    .line 42
    const v1, 0x7f0e03a7

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;

    .line 46
    const v1, 0x7f0e03a8

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 47
    .local v0, "moderatorList":Landroid/support/v7/widget/RecyclerView;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;->addAll(Ljava/util/Collection;)V

    .line 51
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->doneHandler:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 52
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->onModeratorSelected(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 71
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissClubModeratorPickerDialog()V

    .line 72
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->dismissSelf()V

    return-void
.end method

.method private onModeratorSelected(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 1
    .param p1, "moderator"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->dismissSelf()V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->doneHandler:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 68
    return-void
.end method


# virtual methods
.method public setDoneHandler(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "doneHandler":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->doneHandler:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 63
    return-void
.end method

.method public setModerators(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;->clear()V

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog$ClubModeratorListAdapter;->addAll(Ljava/util/Collection;)V

    .line 58
    return-void
.end method
