.class public final Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ActivityHubDataTypes_TrendingTopic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;",
        ">;"
    }
.end annotation


# instance fields
.field private final captionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultCaption:Ljava/lang/String;

.field private defaultDisplayImage:Ljava/lang/String;

.field private defaultName:Ljava/lang/String;

.field private defaultSourceId:Ljava/lang/String;

.field private final displayImageAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final nameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sourceIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultSourceId:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultDisplayImage:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultCaption:Ljava/lang/String;

    .line 28
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->sourceIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 29
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->displayImageAdapter:Lcom/google/gson/TypeAdapter;

    .line 31
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->captionAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    .locals 7
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_0

    .line 70
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 71
    const/4 v5, 0x0

    .line 107
    :goto_0
    return-object v5

    .line 73
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 74
    iget-object v4, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultSourceId:Ljava/lang/String;

    .line 75
    .local v4, "sourceId":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 76
    .local v3, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultDisplayImage:Ljava/lang/String;

    .line 77
    .local v2, "displayImage":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultCaption:Ljava/lang/String;

    .line 78
    .local v1, "caption":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 79
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_1

    .line 81
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 84
    :cond_1
    const/4 v5, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 102
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 84
    :sswitch_0
    const-string v6, "sourceId"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_1
    const-string v6, "name"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_2
    const-string v6, "displayImage"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x2

    goto :goto_2

    :sswitch_3
    const-string v6, "caption"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x3

    goto :goto_2

    .line 86
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->sourceIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "sourceId":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 87
    .restart local v4    # "sourceId":Ljava/lang/String;
    goto :goto_1

    .line 90
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "name":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 91
    .restart local v3    # "name":Ljava/lang/String;
    goto :goto_1

    .line 94
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->displayImageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "displayImage":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 95
    .restart local v2    # "displayImage":Ljava/lang/String;
    goto :goto_1

    .line 98
    :pswitch_3
    iget-object v5, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->captionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "caption":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 99
    .restart local v1    # "caption":Ljava/lang/String;
    goto :goto_1

    .line 106
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 107
    new-instance v5, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic;

    invoke-direct {v5, v4, v3, v2, v1}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 84
    nop

    :sswitch_data_0
    .sparse-switch
        0x337a8b -> :sswitch_1
        0x20ef99e6 -> :sswitch_3
        0x5f0dc9b9 -> :sswitch_2
        0x6816d696 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultCaption(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCaption"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultCaption:Ljava/lang/String;

    .line 47
    return-object p0
.end method

.method public setDefaultDisplayImage(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDisplayImage"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultDisplayImage:Ljava/lang/String;

    .line 43
    return-object p0
.end method

.method public setDefaultName(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultName"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 39
    return-object p0
.end method

.method public setDefaultSourceId(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSourceId"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->defaultSourceId:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    if-nez p2, :cond_0

    .line 53
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 66
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 57
    const-string v0, "sourceId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->sourceIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->sourceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 59
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 61
    const-string v0, "displayImage"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->displayImageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->displayImage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 63
    const-string v0, "caption"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->captionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;->caption()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 65
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    check-cast p2, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;)V

    return-void
.end method
