.class public Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
.super Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;
.source "MultiScreenPopupDialog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog",
        "<",
        "Lcom/microsoft/xbox/xle/app/activity/PopupScreen;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;-><init>(Landroid/content/Context;)V

    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->initialize()V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;-><init>(Landroid/content/Context;I)V

    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->initialize()V

    .line 26
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cancelable"    # Z
    .param p3, "cancelListener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->initialize()V

    .line 21
    return-void
.end method

.method private initialize()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->requestWindowFeature(I)Z

    .line 50
    return-void
.end method


# virtual methods
.method public makeFullScreen(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    .locals 4
    .param p1, "condition"    # Z

    .prologue
    const/4 v3, -0x1

    .line 29
    if-eqz p1, :cond_0

    .line 30
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 31
    .local v1, "wnd":Landroid/view/Window;
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 32
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 33
    const/16 v2, 0x77

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 34
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 35
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 36
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 38
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v1    # "wnd":Landroid/view/Window;
    :cond_0
    return-object p0
.end method

.method public makeTransparent(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    .locals 3
    .param p1, "condition"    # Z

    .prologue
    .line 42
    if-eqz p1, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 45
    :cond_0
    return-object p0
.end method
