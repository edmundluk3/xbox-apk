.class Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetCaptureSocialRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<+",
        "Lcom/microsoft/xbox/service/network/managers/ICapture;",
        ">;>;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/TitleModel;

.field private captureData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation
.end field

.field private filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/util/ArrayList;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p4, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/TitleModel;",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1625
    .local p3, "captureData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1626
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 1627
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->captureData:Ljava/util/ArrayList;

    .line 1628
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    .line 1629
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1619
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1633
    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->captureData:Ljava/util/ArrayList;

    if-eqz v9, :cond_7

    .line 1634
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1635
    .local v4, "likeCountUrls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->captureData:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v2, v9, :cond_1

    .line 1636
    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->captureData:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 1637
    .local v0, "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIsScreenshot()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1638
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getScreenshotLikeCountUrlFormat()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->xuid:Ljava/lang/String;

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-object v13, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->scid:Ljava/lang/String;

    aput-object v13, v11, v12

    const/4 v12, 0x2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1635
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1640
    :cond_0
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getLikeCountUrlFormat()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->xuid:Ljava/lang/String;

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-object v13, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->scid:Ljava/lang/String;

    aput-object v13, v11, v12

    const/4 v12, 0x2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1644
    .end local v0    # "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    :cond_1
    const/4 v7, 0x0

    .line 1645
    .local v7, "socialActionsSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    const/4 v5, 0x0

    .line 1646
    .local v5, "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_2

    .line 1647
    new-instance v9, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;

    invoke-direct {v9, v4}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;-><init>(Ljava/util/List;)V

    invoke-static {v9}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;->getCommentsServiceBatchRequestBody(Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;)Ljava/lang/String;

    move-result-object v6

    .line 1649
    .local v6, "postBody":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v9

    invoke-interface {v9, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getLikeDataInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;

    move-result-object v7

    .line 1650
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v9

    invoke-interface {v9, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getMeLikeInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 1656
    .end local v6    # "postBody":Ljava/lang/String;
    :cond_2
    :goto_2
    const/4 v2, 0x0

    :goto_3
    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->captureData:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v2, v9, :cond_7

    .line 1657
    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->captureData:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 1658
    .restart local v0    # "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 1659
    if-eqz v7, :cond_4

    iget-object v9, v7, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    if-eqz v9, :cond_4

    .line 1660
    iget-object v9, v7, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;->summaries:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 1661
    .local v8, "sum":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    iget-object v10, v8, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    if-eqz v10, :cond_3

    iget-object v10, v8, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->path:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1662
    iput-object v8, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    .line 1667
    .end local v8    # "sum":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;
    :cond_4
    if-eqz v5, :cond_6

    iget-object v9, v5, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    if-eqz v9, :cond_6

    iget-object v9, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    if-eqz v9, :cond_6

    .line 1668
    iget-object v9, v5, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    .line 1669
    .local v3, "like":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    if-eqz v3, :cond_5

    iget-object v10, v3, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;->rootPath:Ljava/lang/String;

    if-eqz v10, :cond_5

    iget-object v10, v3, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;->rootPath:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIdentifier()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1670
    iget-object v9, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    .line 1656
    .end local v3    # "like":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1651
    .end local v0    # "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .restart local v6    # "postBody":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1652
    .local v1, "e":Ljava/lang/Exception;
    const-string v9, "GetCaptureSocialRunner"

    const-string v10, "Social Info Request Failed"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1679
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "i":I
    .end local v4    # "likeCountUrls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .end local v6    # "postBody":Ljava/lang/String;
    .end local v7    # "socialActionsSummaries":Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$SocialActionsSummaries;
    :cond_7
    iget-object v9, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->captureData:Ljava/util/ArrayList;

    return-object v9
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1695
    const-wide/16 v0, 0xd1c

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1689
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<+Lcom/microsoft/xbox/service/network/managers/ICapture;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetCaptureSocialRunner;->filter:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->onGetCaptureSocialCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;)V

    .line 1690
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1685
    return-void
.end method
