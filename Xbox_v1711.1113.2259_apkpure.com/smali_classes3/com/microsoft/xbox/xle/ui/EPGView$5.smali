.class Lcom/microsoft/xbox/xle/ui/EPGView$5;
.super Ljava/lang/Object;
.source "EPGView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGView;

    .prologue
    .line 428
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$5;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 431
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$5;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->loadStartingRow()I

    move-result v1

    .line 432
    .local v1, "row":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$5;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$200(Lcom/microsoft/xbox/xle/ui/EPGView;)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->getChannelCount()I

    move-result v0

    .line 435
    .local v0, "maxChannels":I
    if-le v1, v0, :cond_0

    .line 436
    const/4 v1, 0x0

    .line 439
    :cond_0
    if-lez v1, :cond_1

    .line 440
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$5;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 442
    :cond_1
    return-void
.end method
