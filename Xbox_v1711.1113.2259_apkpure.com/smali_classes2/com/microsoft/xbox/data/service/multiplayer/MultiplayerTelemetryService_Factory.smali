.class public final Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;
.super Ljava/lang/Object;
.source "MultiplayerTelemetryService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final telemetryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p1, "telemetryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;->telemetryProvider:Ljavax/inject/Provider;

    .line 19
    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "telemetryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;
    .locals 2

    .prologue
    .line 23
    new-instance v1, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iget-object v0, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;->telemetryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService_Factory;->get()Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    move-result-object v0

    return-object v0
.end method
