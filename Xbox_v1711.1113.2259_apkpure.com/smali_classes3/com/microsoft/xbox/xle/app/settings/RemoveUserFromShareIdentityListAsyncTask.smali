.class Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "RemoveUserFromShareIdentityListAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;

.field private usersToAdd:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "caller"    # Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p2, "users":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 20
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->usersToAdd:Ljava/util/ArrayList;

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->caller:Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;

    .line 22
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 52
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 53
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_1

    .line 54
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->forceLoad:Z

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->usersToAdd:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeUserFromShareIdentity(ZLjava/util/ArrayList;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 55
    .local v1, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 64
    .end local v1    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :goto_0
    return-object v1

    .line 58
    .restart local v1    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_0
    const-string v2, "YouProfileScreenViewModel"

    const-string v3, "Failed to add user to share identity list"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 63
    .end local v1    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_1
    const-string v2, "User ProfileModel is empty"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 64
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->caller:Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;->postRemoveUserFromShareList(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 43
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/RemoveUserFromShareIdentityListAsyncTask;->caller:Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/IRealNameManager;->preRemoveUserFromShareList()V

    .line 38
    return-void
.end method
