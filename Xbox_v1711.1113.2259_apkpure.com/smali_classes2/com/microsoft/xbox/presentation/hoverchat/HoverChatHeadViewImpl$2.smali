.class Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;
.super Lcom/bumptech/glide/request/target/SimpleTarget;
.source "HoverChatHeadViewImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->renderSingleImage(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bumptech/glide/request/target/SimpleTarget",
        "<",
        "Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

.field final synthetic val$badgeOnRight:Z

.field final synthetic val$viewState:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    .prologue
    .line 278
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->this$0:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    iput-boolean p2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->val$badgeOnRight:Z

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->val$viewState:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    invoke-direct {p0}, Lcom/bumptech/glide/request/target/SimpleTarget;-><init>()V

    return-void
.end method


# virtual methods
.method public onResourceReady(Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;Lcom/bumptech/glide/request/animation/GlideAnimation;)V
    .locals 4
    .param p1, "resource"    # Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;",
            "Lcom/bumptech/glide/request/animation/GlideAnimation",
            "<-",
            "Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 281
    .local p2, "glideAnimation":Lcom/bumptech/glide/request/animation/GlideAnimation;, "Lcom/bumptech/glide/request/animation/GlideAnimation<-Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->this$0:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    .line 282
    invoke-static {v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->access$100(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    check-cast p1, Lcom/bumptech/glide/load/resource/bitmap/GlideBitmapDrawable;

    .line 283
    .end local p1    # "resource":Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;
    invoke-virtual {p1}, Lcom/bumptech/glide/load/resource/bitmap/GlideBitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->val$badgeOnRight:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Z)V

    .line 288
    .local v0, "chatBadgedGlideDrawable":Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;
    instance-of v1, p2, Lcom/bumptech/glide/request/animation/NoAnimation;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->this$0:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->access$000(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 289
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->this$0:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->access$100(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;)Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/flipkart/chatheads/ui/ChatHead;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 294
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->this$0:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->access$300(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;)Landroid/support/v4/util/LruCache;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->val$viewState:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    return-void

    .line 291
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->this$0:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;

    invoke-static {v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;->access$200(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl;)Lcom/bumptech/glide/request/animation/GlideAnimation$ViewAdapter;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lcom/bumptech/glide/request/animation/GlideAnimation;->animate(Ljava/lang/Object;Lcom/bumptech/glide/request/animation/GlideAnimation$ViewAdapter;)Z

    goto :goto_0
.end method

.method public bridge synthetic onResourceReady(Ljava/lang/Object;Lcom/bumptech/glide/request/animation/GlideAnimation;)V
    .locals 0

    .prologue
    .line 278
    check-cast p1, Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewImpl$2;->onResourceReady(Lcom/bumptech/glide/load/resource/drawable/GlideDrawable;Lcom/bumptech/glide/request/animation/GlideAnimation;)V

    return-void
.end method
