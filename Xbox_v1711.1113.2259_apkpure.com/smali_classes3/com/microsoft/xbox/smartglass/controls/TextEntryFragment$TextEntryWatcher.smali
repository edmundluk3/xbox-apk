.class Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;
.super Ljava/lang/Object;
.source "TextEntryFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextEntryWatcher"
.end annotation


# instance fields
.field private _isSuspended:Z

.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;)V
    .locals 1

    .prologue
    .line 267
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;->this$0:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;->_isSuspended:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;
    .param p2, "x1"    # Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$1;

    .prologue
    .line 267
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;-><init>(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 278
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 282
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 287
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTextManager()Lcom/microsoft/xbox/smartglass/TextManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/TextManager;->isSessionActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;->_isSuspended:Z

    if-nez v0, :cond_0

    .line 288
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTextManager()Lcom/microsoft/xbox/smartglass/TextManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;->this$0:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->access$100(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/TextManager;->updateText(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 290
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public suspend(Z)V
    .locals 1
    .param p1, "stop"    # Z

    .prologue
    .line 271
    monitor-enter p0

    .line 272
    :try_start_0
    iput-boolean p1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$TextEntryWatcher;->_isSuspended:Z

    .line 273
    monitor-exit p0

    .line 274
    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
