.class final Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;
.super Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;
.source "AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final newArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

.field private final oldArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;


# direct methods
.method constructor <init>(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)V
    .locals 0
    .param p1, "oldArrangement"    # Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "newArrangement"    # Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 17
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->oldArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    .line 19
    iput-object p2, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->newArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    .line 20
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    if-ne p1, p0, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v1

    .line 47
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 48
    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;

    .line 49
    .local v0, "that":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent<*>;"
    iget-object v3, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->oldArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;->oldArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->newArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-nez v3, :cond_4

    .line 50
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;->newArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 49
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->oldArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;->oldArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 50
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->newArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;->newArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent<*>;"
    :cond_5
    move v1, v2

    .line 52
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent<TT;>;"
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 57
    const/4 v0, 0x1

    .line 58
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->oldArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 60
    mul-int/2addr v0, v3

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->newArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    if-nez v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 62
    return v0

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->oldArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->newArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public newArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->newArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    return-object v0
.end method

.method public oldArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->oldArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;, "Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChatHeadArrangementChangedEvent{oldArrangement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->oldArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", newArrangement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadArrangementChangedEvent;->newArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
