.class public final Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubImageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Landroid/support/v4/util/Pair",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final titleNavigationListener:Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "titleNavigationListener"    # Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "pictureList":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 29
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->titleNavigationListener:Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;)Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->titleNavigationListener:Lcom/microsoft/xbox/xle/app/clubs/TitlesArrayAdapter$TitleNavigationListener;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;I)V
    .locals 1
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/util/Pair;

    invoke-virtual {p1, v0, p2}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;->bindTo(Landroid/support/v4/util/Pair;I)V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    const-string v0, "List item is null!"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 34
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 35
    .local v0, "inflater":Landroid/view/LayoutInflater;
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;

    const v2, 0x7f03007e

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter$ImageViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ClubImageAdapter;Landroid/view/View;)V

    return-object v1
.end method
