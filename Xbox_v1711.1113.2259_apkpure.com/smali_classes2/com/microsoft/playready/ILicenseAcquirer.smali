.class public interface abstract Lcom/microsoft/playready/ILicenseAcquirer;
.super Ljava/lang/Object;
.source "ILicenseAcquirer.java"

# interfaces
.implements Lcom/microsoft/playready/IExtensibleLicenseAcquirer;


# virtual methods
.method public abstract acquireLicense(Ljava/lang/String;)Lcom/microsoft/playready/ILicenseAcquisitionTask;
.end method

.method public abstract deleteLicense(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation
.end method
