.class Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;
.super Ljava/lang/Object;
.source "AppsFlyerLib.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/appsflyer/AppsFlyerLib;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendToServerRunnable"
.end annotation


# instance fields
.field private ctxReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field isLaunch:Z

.field params:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/appsflyer/AppsFlyerLib;

.field private urlString:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/appsflyer/AppsFlyerLib;Ljava/lang/String;Ljava/util/Map;Landroid/content/Context;Z)V
    .locals 1
    .param p2, "urlString"    # Ljava/lang/String;
    .param p4, "ctx"    # Landroid/content/Context;
    .param p5, "isLaunch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Context;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2002
    .local p3, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->this$0:Lcom/appsflyer/AppsFlyerLib;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1994
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->ctxReference:Ljava/lang/ref/WeakReference;

    .line 2003
    iput-object p2, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->urlString:Ljava/lang/String;

    .line 2004
    iput-object p3, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    .line 2005
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->ctxReference:Ljava/lang/ref/WeakReference;

    .line 2006
    iput-boolean p5, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->isLaunch:Z

    .line 2007
    return-void
.end method

.method synthetic constructor <init>(Lcom/appsflyer/AppsFlyerLib;Ljava/lang/String;Ljava/util/Map;Landroid/content/Context;ZLcom/appsflyer/AppsFlyerLib$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/appsflyer/AppsFlyerLib;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/util/Map;
    .param p4, "x3"    # Landroid/content/Context;
    .param p5, "x4"    # Z
    .param p6, "x5"    # Lcom/appsflyer/AppsFlyerLib$1;

    .prologue
    .line 1991
    invoke-direct/range {p0 .. p5}, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;-><init>(Lcom/appsflyer/AppsFlyerLib;Ljava/lang/String;Ljava/util/Map;Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 2010
    const/4 v2, 0x0

    .line 2013
    .local v2, "postDataString":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->ctxReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    .line 2014
    .local v7, "context":Landroid/content/Context;
    const/4 v12, 0x0

    .line 2015
    .local v12, "sentSuccessfully":Z
    if-eqz v7, :cond_1

    .line 2016
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/appsflyer/AppsFlyerProperties;->getReferrer(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 2017
    .local v11, "referrer":Ljava/lang/String;
    if-eqz v11, :cond_0

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    const-string v1, "referrer"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2019
    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    const-string v1, "referrer"

    invoke-interface {v0, v1, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021
    :cond_0
    const-string v0, "appsflyer-data"

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 2022
    .local v13, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string/jumbo v0, "true"

    const-string/jumbo v1, "sentSuccessfully"

    const-string v4, ""

    invoke-interface {v13, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    .line 2023
    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    const-string v1, "eventName"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 2024
    .local v9, "eventName":Ljava/lang/String;
    iget-object v1, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->this$0:Lcom/appsflyer/AppsFlyerLib;

    const-string v4, "appsFlyerCount"

    if-nez v9, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v7, v4, v0}, Lcom/appsflyer/AppsFlyerLib;->access$500(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v8

    .line 2025
    .local v8, "counter":I
    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    const-string v1, "counter"

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2026
    iget-object v1, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    const-string v4, "iaecounter"

    iget-object v5, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->this$0:Lcom/appsflyer/AppsFlyerLib;

    const-string v6, "appsFlyerInAppEventCount"

    if-eqz v9, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-static {v5, v7, v6, v0}, Lcom/appsflyer/AppsFlyerLib;->access$500(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2027
    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    const-string/jumbo v1, "timepassedsincelastlaunch"

    iget-object v4, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->this$0:Lcom/appsflyer/AppsFlyerLib;

    const/4 v5, 0x1

    invoke-static {v4, v7, v5}, Lcom/appsflyer/AppsFlyerLib;->access$600(Lcom/appsflyer/AppsFlyerLib;Landroid/content/Context;Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2029
    iget-boolean v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->isLaunch:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    if-ne v8, v0, :cond_1

    .line 2030
    invoke-static {}, Lcom/appsflyer/AppsFlyerProperties;->getInstance()Lcom/appsflyer/AppsFlyerProperties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/appsflyer/AppsFlyerProperties;->setFirstLaunchCalled()V

    .line 2033
    .end local v8    # "counter":I
    .end local v9    # "eventName":Ljava/lang/String;
    .end local v11    # "referrer":Ljava/lang/String;
    .end local v13    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_1
    iget-object v1, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    const-string v4, "isFirstCall"

    if-nez v12, :cond_6

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2035
    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    const-string v1, "appsflyerKey"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2036
    .local v3, "afDevKey":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_7

    .line 2037
    :cond_2
    const-string v0, "Not sending data yet, waiting for dev key"

    invoke-static {v0}, Lcom/appsflyer/AFLogger;->afDebugLog(Ljava/lang/String;)V

    .line 2055
    .end local v3    # "afDevKey":Ljava/lang/String;
    .end local v7    # "context":Landroid/content/Context;
    .end local v12    # "sentSuccessfully":Z
    :cond_3
    :goto_3
    return-void

    .line 2024
    .restart local v7    # "context":Landroid/content/Context;
    .restart local v9    # "eventName":Ljava/lang/String;
    .restart local v11    # "referrer":Ljava/lang/String;
    .restart local v12    # "sentSuccessfully":Z
    .restart local v13    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 2026
    .restart local v8    # "counter":I
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 2033
    .end local v8    # "counter":I
    .end local v9    # "eventName":Ljava/lang/String;
    .end local v11    # "referrer":Ljava/lang/String;
    .end local v13    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 2041
    .restart local v3    # "afDevKey":Ljava/lang/String;
    :cond_7
    new-instance v0, Lcom/appsflyer/HashUtils;

    invoke-direct {v0}, Lcom/appsflyer/HashUtils;-><init>()V

    iget-object v1, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/appsflyer/HashUtils;->getHashCode(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v10

    .line 2042
    .local v10, "hash":Ljava/lang/String;
    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    const-string v1, "af_v"

    invoke-interface {v0, v1, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2044
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->params:Ljava/util/Map;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2045
    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->this$0:Lcom/appsflyer/AppsFlyerLib;

    iget-object v1, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->urlString:Ljava/lang/String;

    iget-object v4, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->ctxReference:Ljava/lang/ref/WeakReference;

    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->isLaunch:Z

    if-eqz v6, :cond_8

    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->access$700()Lcom/appsflyer/AppsFlyerConversionListener;

    move-result-object v6

    if-eqz v6, :cond_8

    const/4 v6, 0x1

    :goto_4
    invoke-static/range {v0 .. v6}, Lcom/appsflyer/AppsFlyerLib;->access$800(Lcom/appsflyer/AppsFlyerLib;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ref/WeakReference;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_3

    .line 2047
    .end local v3    # "afDevKey":Ljava/lang/String;
    .end local v7    # "context":Landroid/content/Context;
    .end local v10    # "hash":Ljava/lang/String;
    .end local v12    # "sentSuccessfully":Z
    :catch_0
    move-exception v14

    .line 2048
    .local v14, "t":Ljava/io/IOException;
    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->ctxReference:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->urlString:Ljava/lang/String;

    const-string v1, "&isCachedRequest=true&timeincache="

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2049
    invoke-static {}, Lcom/appsflyer/cache/CacheManager;->getInstance()Lcom/appsflyer/cache/CacheManager;

    move-result-object v1

    new-instance v4, Lcom/appsflyer/cache/RequestCacheData;

    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->urlString:Ljava/lang/String;

    sget-object v5, Lcom/appsflyer/AppsFlyerLib;->SDK_BUILD_NUMBER:Ljava/lang/String;

    invoke-direct {v4, v0, v2, v5}, Lcom/appsflyer/cache/RequestCacheData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/appsflyer/AppsFlyerLib$SendToServerRunnable;->ctxReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v4, v0}, Lcom/appsflyer/cache/CacheManager;->cacheRequest(Lcom/appsflyer/cache/RequestCacheData;Landroid/content/Context;)V

    .line 2050
    invoke-virtual {v14}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v14}, Lcom/appsflyer/AFLogger;->afLogE(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 2045
    .end local v14    # "t":Ljava/io/IOException;
    .restart local v3    # "afDevKey":Ljava/lang/String;
    .restart local v7    # "context":Landroid/content/Context;
    .restart local v10    # "hash":Ljava/lang/String;
    .restart local v12    # "sentSuccessfully":Z
    :cond_8
    const/4 v6, 0x0

    goto :goto_4

    .line 2052
    .end local v3    # "afDevKey":Ljava/lang/String;
    .end local v7    # "context":Landroid/content/Context;
    .end local v10    # "hash":Ljava/lang/String;
    .end local v12    # "sentSuccessfully":Z
    :catch_1
    move-exception v14

    .line 2053
    .local v14, "t":Ljava/lang/Throwable;
    invoke-virtual {v14}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v14}, Lcom/appsflyer/AFLogger;->afLogE(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method
