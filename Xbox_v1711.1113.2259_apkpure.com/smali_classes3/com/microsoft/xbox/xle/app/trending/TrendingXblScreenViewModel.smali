.class public Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.source "TrendingXblScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;,
        Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;
    }
.end annotation


# static fields
.field private static final NUM_FILTERS:I = 0x5

.field private static final TAG:Ljava/lang/String;

.field private static final TrendingFeedMaxItems:I = 0x14

.field private static final UNKNOWN_STRING_ID:I


# instance fields
.field private currentFilter:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field private filteredDataCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private filters:[Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;
    .annotation build Landroid/support/annotation/Size;
        value = 0x5L
    .end annotation
.end field

.field private isLoadingTrendingTopics:Z

.field private loadTrendingTopicsAsyncTask:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;

.field private trendingPostsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

.field private trendingTopicsModel:Lcom/microsoft/xbox/service/model/TrendingModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 3
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/16 v1, 0x14

    .line 55
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 56
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Posts:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->newGlobalInstance(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;)Lcom/microsoft/xbox/service/model/TrendingModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->trendingPostsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    .line 57
    sget-object v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;->Topics:Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->newGlobalInstance(ILcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingCategoryType;)Lcom/microsoft/xbox/service/model/TrendingModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->trendingTopicsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    .line 59
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Everything:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Gameclips:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Screenshots:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->TextPosts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Achievements:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->filters:[Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 66
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Everything:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->filteredDataCache:Ljava/util/HashMap;

    .line 69
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;)Lcom/microsoft/xbox/service/model/TrendingModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->trendingTopicsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->onLoadTrendingTopicsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->isLoadingTrendingTopics:Z

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->updateAdapter()V

    return-void
.end method

.method private onLoadTrendingTopicsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v3, 0x0

    .line 205
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadTrendingTopicsCompleted Completed with status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 215
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->updateViewModelState()V

    .line 216
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->updateAdapter()V

    .line 217
    return-void

    .line 209
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->isForceRefresh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->setForceRefresh(Z)V

    .line 211
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;

    invoke-direct {v0, v3, v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->setLastSelectedPosition(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$ListPosition;)V

    goto :goto_0

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected getActivityFeedData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->trendingPostsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    .line 128
    .local v0, "responseList":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->getItemsAsProfileRecentItems()Ljava/util/ArrayList;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method protected getContinuationToken()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 140
    const/4 v0, 0x0

    return-object v0
.end method

.method public getData()Ljava/util/List;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getData()Ljava/util/List;

    move-result-object v0

    .line 102
    .local v0, "allData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    sget-object v4, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Everything:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-ne v3, v4, :cond_0

    .line 116
    .end local v0    # "allData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    :goto_0
    return-object v0

    .line 104
    .restart local v0    # "allData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->filteredDataCache:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 105
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->filteredDataCache:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    move-object v0, v3

    goto :goto_0

    .line 107
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v1, "filteredData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 110
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->isEqualToActivityItemType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 111
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 115
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->filteredDataCache:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 116
    goto :goto_0
.end method

.method public getFilterItems()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->filters:[Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNoContentText()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 171
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070cae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getScreenAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 160
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTrendingXblScreenAdapter(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    return-object v0
.end method

.method public getTopicData()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->trendingTopicsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    .line 122
    .local v0, "topicsList":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;->getItemsAsTrendingTopics()Ljava/util/ArrayList;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->isLoadingTrendingTopics:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusPostEnabled()Z
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->load(Z)V

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->loadTrendingTopicsAsyncTask:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->loadTrendingTopicsAsyncTask:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;->cancel()V

    .line 79
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->loadTrendingTopicsAsyncTask:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->loadTrendingTopicsAsyncTask:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;->load(Z)V

    .line 81
    return-void
.end method

.method protected loadActivityFeedData(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3
    .param p1, "forceLoad"    # Z
    .param p2, "continuationToken"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 146
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->trendingPostsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/TrendingModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 148
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 149
    sget-object v1, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Unable to get trending data"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_0
    :goto_0
    return-object v0

    .line 150
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_0

    .line 151
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->filteredDataCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method

.method public navigateToTopic(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;)V
    .locals 2
    .param p1, "topic"    # Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 197
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 199
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 200
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTrendingTopic(Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;)V

    .line 201
    const-class v1, Lcom/microsoft/xbox/xle/app/trending/TrendingTopicScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 202
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->onStopOverride()V

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->loadTrendingTopicsAsyncTask:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->loadTrendingTopicsAsyncTask:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$LoadTrendingTopicsAsyncTask;->cancel()V

    .line 95
    :cond_0
    return-void
.end method

.method public setFilter(I)V
    .locals 7
    .param p1, "filterPosition"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
            to = 0x5L
        .end annotation
    .end param

    .prologue
    .line 185
    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x5

    int-to-long v4, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->filters:[Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aget-object v6, v0, p1

    .line 189
    .local v6, "newFilter":Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-eq v0, v6, :cond_0

    .line 190
    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->currentFilter:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->updateViewModelState()V

    .line 192
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->updateAdapter()V

    .line 194
    :cond_0
    return-void
.end method

.method protected shouldReloadActivityFeedData()Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->trendingPostsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->trendingPostsModel:Lcom/microsoft/xbox/service/model/TrendingModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showActivityAlertNotificationBar()Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method protected updateViewModelState()V
    .locals 2

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->getData()Ljava/util/List;

    move-result-object v0

    .line 224
    .local v0, "filteredItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 225
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->isLoadingProfileRecentActivityFeedData:Z

    if-eqz v1, :cond_1

    .line 226
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 234
    :goto_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->isLoadingTrendingTopics:Z

    if-eqz v1, :cond_0

    .line 235
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 237
    :cond_0
    return-void

    .line 228
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 231
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method
