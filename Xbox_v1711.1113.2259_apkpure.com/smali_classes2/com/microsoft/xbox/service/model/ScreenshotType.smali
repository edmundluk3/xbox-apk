.class public final enum Lcom/microsoft/xbox/service/model/ScreenshotType;
.super Ljava/lang/Enum;
.source "ScreenshotType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/ScreenshotType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/ScreenshotType;

.field public static final enum Ahls:Lcom/microsoft/xbox/service/model/ScreenshotType;

.field public static final enum Download:Lcom/microsoft/xbox/service/model/ScreenshotType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/service/model/ScreenshotType;

    const-string v1, "Download"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ScreenshotType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/ScreenshotType;->Download:Lcom/microsoft/xbox/service/model/ScreenshotType;

    new-instance v0, Lcom/microsoft/xbox/service/model/ScreenshotType;

    const-string v1, "Ahls"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/ScreenshotType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/ScreenshotType;->Ahls:Lcom/microsoft/xbox/service/model/ScreenshotType;

    .line 3
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/ScreenshotType;

    sget-object v1, Lcom/microsoft/xbox/service/model/ScreenshotType;->Download:Lcom/microsoft/xbox/service/model/ScreenshotType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/ScreenshotType;->Ahls:Lcom/microsoft/xbox/service/model/ScreenshotType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/service/model/ScreenshotType;->$VALUES:[Lcom/microsoft/xbox/service/model/ScreenshotType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ScreenshotType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/model/ScreenshotType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/ScreenshotType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/ScreenshotType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/model/ScreenshotType;->$VALUES:[Lcom/microsoft/xbox/service/model/ScreenshotType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/ScreenshotType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/ScreenshotType;

    return-object v0
.end method
