.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes;
.super Ljava/lang/Object;
.source "ContentRatingsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingLocalizedProperty;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$AlternateId;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRating;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Descriptor;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$Disclaimer;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoardLocalizedProperty;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
