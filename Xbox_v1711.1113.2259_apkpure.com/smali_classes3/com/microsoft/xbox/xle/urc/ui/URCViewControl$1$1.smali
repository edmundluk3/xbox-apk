.class Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;
.super Ljava/lang/Object;
.source "URCViewControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final bottom:I

.field final synthetic this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

.field final top:I


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;)V
    .locals 1
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->bottom:I

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$scrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getTop()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->top:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->cancelFirstRunAnimation()V

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$000(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-array v1, v4, [I

    const/4 v2, 0x0

    iget v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->bottom:I

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->top:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$000(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1$1;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$000(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1$2;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$000(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$interpolator:Landroid/animation/TimeInterpolator;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$000(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->val$interpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$000(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$100(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$100(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$100(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1$3;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$100(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1$4;-><init>(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1$1;->this$1:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl$1;->this$0:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->access$100(Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 197
    return-void

    .line 168
    nop

    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method
