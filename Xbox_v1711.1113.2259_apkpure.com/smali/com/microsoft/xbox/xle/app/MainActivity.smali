.class public Lcom/microsoft/xbox/xle/app/MainActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "MainActivity.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;
.implements Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;
.implements Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;
.implements Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;
.implements Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;
.implements Lcom/facebook/react/modules/core/DefaultHardwareBackBtnHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/app/AppCompatActivity;",
        "Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;",
        "Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;",
        "Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;",
        "Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;",
        "Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;",
        "Lcom/facebook/react/modules/core/DefaultHardwareBackBtnHandler;"
    }
.end annotation


# static fields
.field public static final ACTION_FIND_PEOPLE:Ljava/lang/String; = "com.microsoft.xbox.action.ACTION_FIND_PEOPLE"

.field public static final ACTION_LAUNCH_REMOTE:Ljava/lang/String; = "ACTION_LAUNCH_REMOTE"

.field public static final ACTION_SIGNIN:Ljava/lang/String; = "com.microsoft.xbox.action.ACTION_SIGNIN"

.field public static final ACTION_VIEW_ACHIEVEMENTS:Ljava/lang/String; = "com.microsoft.xbox.action.ACTION_VIEW_ACHIEVEMENTS"

.field public static final ACTION_VIEW_GAME_PROFILE:Ljava/lang/String; = "com.microsoft.xbox.action.ACTION_VIEW_GAME_PROFILE"

.field public static final ACTION_VIEW_PURCHASE_PAGE:Ljava/lang/String; = "com.microsoft.xbox.action.ACTION_VIEW_PURCHASE_PAGE"

.field public static final ACTION_VIEW_SETTINGS:Ljava/lang/String; = "com.microsoft.xbox.action.ACTION_VIEW_SETTINGS"

.field public static final ACTION_VIEW_USER_PROFILE:Ljava/lang/String; = "com.microsoft.xbox.action.ACTION_VIEW_USER_PROFILE"

.field public static final DATA_OOBE:Ljava/lang/String; = "smartglass://oobe"

.field public static final EXTRA_BIGID:Ljava/lang/String; = "com.microsoft.xbox.extra.BIGID"

.field public static final EXTRA_INTENT_PROCESSED:Ljava/lang/String; = "EXTRA_INTENT_PROCESSED"

.field public static final EXTRA_IS_XBOX360_GAME:Ljava/lang/String; = "com.microsoft.xbox.extra.IS_XBOX360_GAME"

.field public static final EXTRA_PRODUCT_TYPE:Ljava/lang/String; = "com.microsoft.xbox.extra.PRODUCT_TYPE"

.field public static final EXTRA_RELAUNCH_INTENT:Ljava/lang/String; = "com.microsoft.xbox.extra.RELAUNCH_INTENT"

.field public static final EXTRA_TITLEID:Ljava/lang/String; = "com.microsoft.xbox.extra.TITLEID"

.field public static final EXTRA_TITLE_ID:Ljava/lang/String; = "com.microsoft.xbox.extra.TITLE_ID"

.field private static final NAVIGATION_BLOCK_TIMEOUT_MS:I = 0x1388

.field private static final PRESENCE_HEARTBEAT:Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

.field private static final START_SCREEN_HEIGHT:I

.field private static final START_SCREEN_WIDTH:I

.field public static final TAG:Ljava/lang/String;

.field private static sPlayReadyFactory:Lcom/microsoft/playready/IPlayReadyFactory;


# instance fields
.field private final APPSFLYER_DEV_KEY:Ljava/lang/String;

.field private accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private accessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

.field private alertIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;

.field private animationBlocking:Z

.field authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private contentFrame:Landroid/widget/RelativeLayout;

.field private countConversationMessagesloaded:I

.field private countConversationPagesloaded:I

.field private drawerLayout:Landroid/support/v4/widget/DrawerLayout;

.field private drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

.field private friendsIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;

.field private final gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;"
        }
    .end annotation
.end field

.field private hasRunOnReady:Z

.field homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private isNewLaunch:Z

.field private leftDrawer:Landroid/widget/ScrollView;

.field private messageIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;

.field private needToRestoreState:Z

.field private final onActivityResultCallbacks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;",
            ">;"
        }
    .end annotation
.end field

.field partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field partyEventNotifier:Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private partyLfgIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;

.field private paused:Z

.field private previousSessionState:I

.field reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private refreshIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

.field private refreshRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay",
            "<",
            "Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;",
            ">;"
        }
    .end annotation
.end field

.field private rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

.field private rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

.field private sandboxOption:Ljava/lang/String;

.field private savedInstanceState:Landroid/os/Bundle;

.field private final screens:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field

.field private showRemoteControlFromWidget:Z

.field private signOutInProgress:Z

.field private startupScreen:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field

.field private titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

.field tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 221
    const-class v0, Lcom/microsoft/xbox/xle/app/MainActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    .line 248
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenWidth()I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_WIDTH:I

    .line 249
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenHeight()I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_HEIGHT:I

    .line 250
    sget-object v0, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    sput-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->PRESENCE_HEARTBEAT:Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 212
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 252
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    .line 253
    const-string v0, "cb5Gbpxd6U2NBZCwHc2gt7"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->APPSFLYER_DEV_KEY:Ljava/lang/String;

    .line 261
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->sandboxOption:Ljava/lang/String;

    .line 267
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->startupScreen:Ljava/lang/Class;

    .line 268
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->isNewLaunch:Z

    .line 270
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->animationBlocking:Z

    .line 274
    iput v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->previousSessionState:I

    .line 275
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->showRemoteControlFromWidget:Z

    .line 278
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->signOutInProgress:Z

    .line 279
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/MainActivity;)Lcom/microsoft/xbox/toolkit/XLEObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    .line 286
    iput v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    .line 287
    iput v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    .line 288
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->refreshRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 289
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 292
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->onActivityResultCallbacks:Ljava/util/Set;

    return-void
.end method

.method private checkLaunchParameter()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2293
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 2294
    .local v0, "data":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 2295
    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->sandboxOption:Ljava/lang/String;

    .line 2296
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v3, "regular app launch"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319
    :goto_0
    return-void

    .line 2300
    :cond_0
    const-string/jumbo v2, "sandboxid"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2301
    .local v1, "sandboxId":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 2302
    const-string/jumbo v2, "sandboxId"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2305
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "requested sandbox id is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2306
    if-nez v1, :cond_2

    .line 2308
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getDefaultSandboxId()Ljava/lang/String;

    move-result-object v1

    .line 2311
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getCurrentSandboxId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2312
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sandbox changed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->sandboxOption:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 2313
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->sandboxOption:Ljava/lang/String;

    goto :goto_0

    .line 2316
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sandbox is the same as default, no action "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->sandboxOption:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 2317
    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->sandboxOption:Ljava/lang/String;

    goto :goto_0
.end method

.method private getClubIdFromExtras(Landroid/os/Bundle;)J
    .locals 4
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 1048
    :try_start_0
    const-string v1, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1051
    :goto_0
    return-wide v2

    .line 1049
    :catch_0
    move-exception v0

    .line 1050
    .local v0, "ex":Ljava/lang/NumberFormatException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get club ID from notification: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1051
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method private handleSessionState(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v3, 0x2

    .line 2649
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Handle session state"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2650
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2651
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getDisplayedSessionState()I

    move-result v0

    .line 2653
    .local v0, "sessionState":I
    if-ne v0, v3, :cond_3

    .line 2654
    invoke-static {}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getInstance()Lcom/microsoft/xbox/xle/remote/TextInputModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->onResume()V

    .line 2659
    :cond_0
    :goto_0
    if-eq v0, v3, :cond_1

    if-nez v0, :cond_6

    .line 2661
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getRunningStress()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2678
    .end local v0    # "sessionState":I
    :cond_2
    :goto_1
    return-void

    .line 2655
    .restart local v0    # "sessionState":I
    :cond_3
    if-nez v0, :cond_0

    .line 2656
    invoke-static {}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->getInstance()Lcom/microsoft/xbox/xle/remote/TextInputModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->onPause()V

    goto :goto_0

    .line 2665
    :cond_4
    iget v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->previousSessionState:I

    if-eq v0, v1, :cond_5

    .line 2666
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$33;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 2674
    :cond_5
    iput v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->previousSessionState:I

    .line 2676
    :cond_6
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->processShowRemoteControlFromWidget(I)V

    goto :goto_1
.end method

.method private isProcessed(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 817
    const-string v0, "EXTRA_INTENT_PROCESSED"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private joinAndNavigateToPartyDetails(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 977
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->removeInAppNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 978
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    .line 979
    .local v0, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 981
    .local v1, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v1, :cond_0

    .line 982
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/MainActivity;Landroid/os/Bundle;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    .line 995
    :cond_0
    return-void
.end method

.method static synthetic lambda$handleSessionState$42()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2667
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "SessionState changed, force refresh"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2668
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->loadAsync(Z)V

    .line 2669
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->loadDiscoverList(Z)V

    .line 2670
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->loadAsync(Z)V

    .line 2671
    return-void
.end method

.method static synthetic lambda$hideKeyboard$30(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 3
    .param p0, "currentScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 1617
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1618
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1619
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/microsoft/xle/test/interop/TestInterop;->setDismissSoftKeyboard(Lcom/microsoft/xle/test/interop/delegates/Action;)V

    .line 1620
    return-void
.end method

.method static synthetic lambda$joinAndNavigateToPartyDetails$9(Lcom/microsoft/xbox/xle/app/MainActivity;Landroid/os/Bundle;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 983
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    const-string v2, "com.microsoft.xbox.extra.SESSION_ID"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.microsoft.xbox.extra.HANDLE_ID"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->joinParty(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v1

    .line 984
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 985
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 986
    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    .line 989
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 993
    :goto_0
    return-void

    .line 990
    :catch_0
    move-exception v0

    .line 991
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Could not navigate to party details"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$navigateToAuthFlowWithIntent$13(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 3
    .param p0, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 1107
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    invoke-virtual {v1, v2, p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1111
    :goto_0
    return-void

    .line 1108
    :catch_0
    move-exception v0

    .line 1109
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Error trying to launch XboxAuthActivity after disabling SilentSignIn update nav"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$navigateToAuthFlowWithIntent$14(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 3
    .param p0, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 1115
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    invoke-virtual {v1, v2, p0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1119
    :goto_0
    return-void

    .line 1116
    :catch_0
    move-exception v0

    .line 1117
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Error trying to launch XboxAuthActivity"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$navigateToClub$12(Lcom/microsoft/xbox/xle/app/MainActivity;Landroid/os/Bundle;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .param p1, "extras"    # Landroid/os/Bundle;
    .param p2, "startScreen"    # Ljava/lang/Class;
    .param p3, "nm"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    .line 1061
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getClubIdFromExtras(Landroid/os/Bundle;)J

    move-result-wide v0

    .line 1062
    .local v0, "clubId":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 1063
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getClubIdFromExtras(Landroid/os/Bundle;)J

    move-result-wide v4

    invoke-direct {v2, v4, v5, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;-><init>(JLjava/lang/Class;)V

    .line 1064
    .local v2, "params":Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;
    const-class v3, Lcom/microsoft/xbox/xle/app/clubs/ClubPivotScreen;

    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 1066
    .end local v2    # "params":Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;
    :cond_0
    return-void
.end method

.method static synthetic lambda$navigateToFindPeople$24()V
    .locals 3

    .prologue
    .line 1304
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigateToHomeScreen()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1308
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :goto_0
    return-void

    .line 1305
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_0
    move-exception v0

    .line 1306
    .restart local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Error going to Find people"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$navigateToFindPeople$25(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 3
    .param p0, "nm"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p1, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 1313
    :try_start_0
    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubPivotScreen;

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPop(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/Class;Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1317
    :goto_0
    return-void

    .line 1314
    :catch_0
    move-exception v0

    .line 1315
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Error going to Find people"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$navigateToGameProfile$16(ZJLjava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 7
    .param p0, "isXbox360Game"    # Z
    .param p1, "titleId"    # J
    .param p3, "pivot"    # Ljava/lang/Class;
    .param p4, "nm"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    .line 1210
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1211
    .local v2, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>()V

    .line 1212
    .local v1, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    if-eqz p0, :cond_0

    .line 1213
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setMediaItemTypeFromInt(I)V

    .line 1217
    :goto_0
    iput-wide p1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->titleId:J

    .line 1218
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 1220
    if-eqz p3, :cond_1

    .line 1221
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    invoke-virtual {v3, v4, p3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 1228
    :goto_1
    const-class v3, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    const/4 v4, 0x0

    invoke-virtual {p4, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPop(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 1232
    .end local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    .end local v2    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_2
    return-void

    .line 1215
    .restart local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    .restart local v2    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    const/16 v3, 0x2329

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setMediaItemTypeFromInt(I)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1229
    .end local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    .end local v2    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :catch_0
    move-exception v0

    .line 1230
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v4, "Error going to GameProfilePivotScreen"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1224
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    .restart local v2    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/GameProfileInfoScreen;

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method static synthetic lambda$navigateToLfgDetails$11(Ljava/lang/String;Landroid/os/Bundle;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 5
    .param p0, "handleId"    # Ljava/lang/String;
    .param p1, "extras"    # Landroid/os/Bundle;
    .param p2, "nm"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    const/4 v4, 0x1

    .line 1028
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1029
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setForceReload(Z)V

    .line 1030
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMultiplayerHandleId(Ljava/lang/String;)V

    .line 1031
    const-string v1, "com.microsoft.xbox.extra.TITLE_ID"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 1032
    const-class v1, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsScreen;

    invoke-virtual {p2, v1, v4, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 1033
    return-void
.end method

.method static synthetic lambda$navigateToMessageScreen$8(Ljava/lang/String;Landroid/os/Bundle;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 4
    .param p0, "senderXuid"    # Ljava/lang/String;
    .param p1, "extras"    # Landroid/os/Bundle;
    .param p2, "nm"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    const/4 v3, 0x1

    .line 964
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 965
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setForceReload(Z)V

    .line 966
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSenderXuid(Ljava/lang/String;)V

    .line 967
    const-string v1, "com.microsoft.xbox.extra.THREAD_TOPIC"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putConversationTopic(Ljava/lang/String;)V

    .line 969
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedConversationSummary(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 970
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;

    invoke-virtual {p2, v1, v3, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 971
    return-void
.end method

.method static synthetic lambda$navigateToOOBE$26()V
    .locals 3

    .prologue
    .line 1331
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1335
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :goto_0
    return-void

    .line 1332
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_0
    move-exception v0

    .line 1333
    .restart local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Error going to OOBE"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$navigateToPartyDetails$10()V
    .locals 4

    .prologue
    .line 1009
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1013
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :goto_0
    return-void

    .line 1010
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_0
    move-exception v0

    .line 1011
    .restart local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Could not navigate to party details"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$navigateToPurchasePage$22(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 3
    .param p0, "bigId"    # Ljava/lang/String;
    .param p1, "nm"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    .line 1247
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$36;->lambdaFactory$(Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v0

    .line 1248
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 1249
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$37;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 1250
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$38;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 1251
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$39;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 1252
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$40;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 1253
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 1254
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Maybe;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$41;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$42;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 1255
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 1270
    return-void
.end method

.method static synthetic lambda$navigateToSettings$23(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 4
    .param p0, "nm"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    .line 1282
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1285
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const-class v2, Lcom/microsoft/xbox/xle/app/settings/SettingsPivotScreen;

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPop(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/Class;Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1289
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return-void

    .line 1286
    :catch_0
    move-exception v0

    .line 1287
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v3, "Error going to SettingsActivity"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$navigateToUserProfile$15(Ljava/lang/String;Lcom/microsoft/xbox/service/model/ProfileModel;ZLcom/microsoft/xbox/toolkit/ui/NavigationManager;)V
    .locals 7
    .param p0, "xuid"    # Ljava/lang/String;
    .param p1, "meProfile"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p2, "shouldGoBackToGame"    # Z
    .param p3, "nm"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .prologue
    const/4 v2, 0x1

    .line 1156
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1157
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 1158
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setForceReload(Z)V

    .line 1159
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1161
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToViewProfile()Z

    move-result v3

    if-eqz p1, :cond_1

    .line 1162
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->canViewOtherProfiles()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0704b5

    .line 1163
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07061c

    .line 1164
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1160
    invoke-static {v3, v2, v4, v5}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1167
    if-eqz p2, :cond_2

    .line 1168
    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const/4 v3, 0x0

    invoke-virtual {p3, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPop(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 1185
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    :goto_1
    return-void

    .line 1162
    .restart local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1170
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1182
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :catch_0
    move-exception v0

    .line 1183
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v3, "Error going to PeopleHubActivity"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1176
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_3
    if-eqz p2, :cond_4

    .line 1177
    :try_start_1
    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const/4 v3, 0x0

    invoke-virtual {p3, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPop(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/Class;Ljava/lang/Class;)V

    goto :goto_1

    .line 1179
    :cond_4
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/MainActivity;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .param p1, "asyncResult"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->signOutInProgress:Z

    if-eqz v0, :cond_0

    .line 281
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->signOutInProgress:Z

    .line 282
    sget-object v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->reset()V

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->signOutXsapi()V

    .line 285
    :cond_0
    return-void
.end method

.method static synthetic lambda$null$17(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 2
    .param p0, "bigId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1247
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreService;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreService;

    invoke-static {p0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/store/StoreService;->getProductsFromIds(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$18(Ljava/util/List;)Z
    .locals 1
    .param p0, "products"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1251
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$null$19(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .locals 1
    .param p0, "products"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1252
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    return-object v0
.end method

.method static synthetic lambda$null$20(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 6
    .param p0, "nm"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1257
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1258
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 1259
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    iget-wide v4, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->titleId:J

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedTitleId(J)V

    .line 1261
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->getDetailScreenTypeFromMediaType(I)Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;

    move-result-object v2

    .line 1262
    .local v2, "screenType":Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;
    invoke-static {v2, p1}, Lcom/microsoft/xbox/xle/viewmodel/DetailPageHelper;->getPaneConfigData(Lcom/microsoft/xbox/service/model/eds/DetailDisplayScreenType;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;

    move-result-object v0

    .line 1263
    .local v0, "data":[Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setDetailPivotData([Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/PaneConfigData;)V

    .line 1265
    if-eqz v0, :cond_0

    .line 1266
    const-class v3, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPop(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/Class;Ljava/lang/Class;)V

    .line 1268
    :cond_0
    return-void
.end method

.method static synthetic lambda$null$21(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1269
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "Error deeplinking to Purchase page"

    invoke-static {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic lambda$onCreate$1(Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "event"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 358
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignOutSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onCreate$2(Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 2
    .param p0, "ignore"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 359
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->resetModels(Z)V

    return-void
.end method

.method static synthetic lambda$onCreate$3(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 481
    const/4 v2, 0x5

    .local v2, "retryCount":I
    move v3, v2

    .line 482
    .end local v2    # "retryCount":I
    .local v3, "retryCount":I
    :goto_0
    add-int/lit8 v2, v3, -0x1

    .end local v3    # "retryCount":I
    .restart local v2    # "retryCount":I
    if-lez v3, :cond_0

    .line 484
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    move-result-object v6

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEnvironment()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->setEnvironement(Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;)V

    .line 485
    sget-object v6, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v7, "SGPlatformInstance environment initialized"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v6

    if-nez v6, :cond_1

    .line 513
    new-instance v6, Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    new-instance v7, Lcom/microsoft/xbox/xle/epg/EpgConnector;

    invoke-direct {v7}, Lcom/microsoft/xbox/xle/epg/EpgConnector;-><init>()V

    new-instance v8, Lcom/microsoft/xbox/xle/epg/URCIntegration;

    invoke-direct {v8}, Lcom/microsoft/xbox/xle/epg/URCIntegration;-><init>()V

    invoke-direct {v6, v7, v8, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;-><init>(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;Landroid/content/Context;)V

    invoke-static {v6}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->setInstance(Lcom/microsoft/xbox/xle/urc/net/BranchSession;)V

    .line 517
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$43;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 518
    return-void

    .line 487
    :catch_0
    move-exception v0

    .line 502
    .local v0, "e":Ljava/lang/LinkageError;
    const-wide/16 v4, 0x64

    .line 503
    .local v4, "sleepTime":J
    :try_start_1
    sget-object v6, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v7, "Error during running background task, sleeping for 100 milliseconds"

    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 504
    const-wide/16 v6, 0x64

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 509
    :goto_1
    sget-object v6, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Caught Linkage error "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/LinkageError;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 510
    .end local v2    # "retryCount":I
    .restart local v3    # "retryCount":I
    goto :goto_0

    .line 505
    .end local v3    # "retryCount":I
    .restart local v2    # "retryCount":I
    :catch_1
    move-exception v1

    .line 506
    .local v1, "ie":Ljava/lang/InterruptedException;
    sget-object v6, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error while sleeping"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic lambda$onCreateOptionsMenu$31()V
    .locals 3

    .prologue
    .line 1714
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCCrash;->trackExistingCrash()V

    .line 1716
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->getApplicationPaused()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1717
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->crashFileExists()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1719
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->handleCrash()V

    .line 1729
    .local v0, "ex":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 1723
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->showRateMeDialog()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1726
    :catch_0
    move-exception v0

    .line 1727
    .restart local v0    # "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Failed to handle feedback"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$onLocaleChanged$43()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2723
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->loadLiveTVSettings(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 2724
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 2725
    return-void
.end method

.method static synthetic lambda$onOptionalUpdate$39(I)V
    .locals 1
    .param p0, "latestVersion"    # I

    .prologue
    .line 2556
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setLastCheckedVersion(I)V

    return-void
.end method

.method static synthetic lambda$onOptionsItemSelected$32(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p0, "currentBehaviorTextView"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 1911
    if-eqz p2, :cond_0

    .line 1912
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    sget-object v1, Lcom/facebook/login/LoginBehavior;->WEB_ONLY:Lcom/facebook/login/LoginBehavior;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->setLoginBehavior(Lcom/facebook/login/LoginBehavior;)V

    .line 1913
    const-string v0, "Web only"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1919
    :goto_0
    return-void

    .line 1916
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    sget-object v1, Lcom/facebook/login/LoginBehavior;->NATIVE_WITH_FALLBACK:Lcom/facebook/login/LoginBehavior;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->setLoginBehavior(Lcom/facebook/login/LoginBehavior;)V

    .line 1917
    const-string v0, "Native w/ fallback"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic lambda$onOptionsItemSelected$33(Landroid/view/View;)V
    .locals 2
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 1921
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->resetFacebookFriendFinderState()V

    .line 1922
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadUpdatePhoneOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 1923
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->DeleteProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->loadShortCircuitProfileAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;)V

    .line 1924
    return-void
.end method

.method static synthetic lambda$onOptionsItemSelected$34(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "whichButton"    # I

    .prologue
    .line 2053
    return-void
.end method

.method static synthetic lambda$onOptionsItemSelected$35(Landroid/content/DialogInterface;I)V
    .locals 20
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "whichButton"    # I

    .prologue
    .line 2062
    invoke-static {}, Lcom/microsoft/xle/test/interop/TestInterop;->getCompanionDebugUrl()Ljava/lang/String;

    move-result-object v14

    .line 2063
    .local v14, "value":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xle/test/interop/TestInterop;->getCompanionDebugTitleId()Ljava/lang/String;

    move-result-object v12

    .line 2064
    .local v12, "title":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xle/test/interop/TestInterop;->getCompanionDebugAllowedUrls()Ljava/lang/String;

    move-result-object v4

    .line 2068
    .local v4, "allowed":Ljava/lang/String;
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;-><init>()V

    .line 2072
    .local v6, "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    if-eqz v14, :cond_3

    .line 2075
    :try_start_0
    new-instance v15, Ljava/net/URI;

    invoke-direct {v15, v14}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v15}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setActivityUrl(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2080
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;-><init>()V

    .line 2081
    .local v2, "act":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setActivityLaunchInfo(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V

    .line 2088
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 2089
    const-wide/16 v8, -0x1

    .line 2091
    .local v8, "numericTitleId":J
    :try_start_1
    const-string v15, "0x"

    invoke-virtual {v12, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_0

    const-string v15, "0X"

    invoke-virtual {v12, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 2092
    :cond_0
    const/4 v15, 0x2

    invoke-virtual {v12, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 2093
    .local v11, "temp":Ljava/lang/String;
    const/16 v15, 0x10

    invoke-static {v11, v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v8

    .line 2102
    .end local v11    # "temp":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2, v8, v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setNowPlayingTitleId(J)V

    .line 2103
    long-to-int v15, v8

    invoke-virtual {v2, v15}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->addAllowedTitleId(I)V

    .line 2107
    .end local v8    # "numericTitleId":J
    :cond_1
    if-eqz v4, :cond_2

    .line 2108
    const-string v15, ";"

    invoke-virtual {v4, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 2109
    .local v13, "urls":[Ljava/lang/String;
    invoke-virtual {v6, v13}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setWhitelistUrls([Ljava/lang/String;)V

    .line 2129
    .end local v13    # "urls":[Ljava/lang/String;
    :cond_2
    const/4 v3, 0x0

    .line 2131
    .local v3, "allCapabilities":I
    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setUsesCapabilities(I)V

    .line 2134
    new-instance v10, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v10}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 2135
    .local v10, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v10, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedActivityData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 2138
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v15

    const-class v16, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v0, v1, v10}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 2139
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    invoke-static {v15}, Lcom/microsoft/xle/test/interop/TestInterop;->setHasLaunchedCompanionDebug(Ljava/lang/Boolean;)V

    .line 2140
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    invoke-static {v15}, Lcom/microsoft/xle/test/interop/TestInterop;->setHasTappedLaunch(Ljava/lang/Boolean;)V

    .line 2142
    .end local v2    # "act":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .end local v3    # "allCapabilities":I
    .end local v10    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_3
    :goto_1
    return-void

    .line 2076
    :catch_0
    move-exception v5

    .line 2078
    .local v5, "e":Ljava/net/URISyntaxException;
    goto :goto_1

    .line 2095
    .end local v5    # "e":Ljava/net/URISyntaxException;
    .restart local v2    # "act":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v8    # "numericTitleId":J
    :cond_4
    :try_start_2
    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-wide v8

    goto :goto_0

    .line 2097
    :catch_1
    move-exception v7

    .line 2099
    .local v7, "ne":Ljava/lang/NumberFormatException;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v15

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v16

    const-string v17, "The title id %s is not a valid number"

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v12, v18, v19

    invoke-static/range {v16 .. v18}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method static synthetic lambda$onOptionsItemSelected$36(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p0, "dialog"    # Landroid/content/DialogInterface;
    .param p1, "whichButton"    # I

    .prologue
    const/4 v0, 0x0

    .line 2147
    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->setCompanionDebugUrl(Ljava/lang/String;)V

    .line 2148
    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->setCompanionDebugTitleId(Ljava/lang/String;)V

    .line 2149
    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->setCompanionDebugAllowedUrls(Ljava/lang/String;)V

    .line 2150
    return-void
.end method

.method static synthetic lambda$onOptionsItemSelected$37(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;Landroid/content/DialogInterface;I)V
    .locals 4
    .param p0, "launchUrl"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .param p1, "titleid"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .param p2, "allowedUrls"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .param p3, "dialog"    # Landroid/content/DialogInterface;
    .param p4, "whichButton"    # I

    .prologue
    .line 2155
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2156
    .local v1, "launch":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2157
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2159
    .local v0, "allowed":Ljava/lang/String;
    invoke-static {v1}, Lcom/microsoft/xle/test/interop/TestInterop;->setCompanionDebugUrl(Ljava/lang/String;)V

    .line 2160
    invoke-static {v2}, Lcom/microsoft/xle/test/interop/TestInterop;->setCompanionDebugTitleId(Ljava/lang/String;)V

    .line 2161
    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->setCompanionDebugAllowedUrls(Ljava/lang/String;)V

    .line 2162
    return-void
.end method

.method static synthetic lambda$onOptionsItemSelected$38(Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;Landroid/content/DialogInterface;I)V
    .locals 4
    .param p0, "launchUrl"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .param p1, "titleid"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .param p2, "allowedUrls"    # Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .param p3, "dialog"    # Landroid/content/DialogInterface;
    .param p4, "whichButton"    # I

    .prologue
    .line 2170
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2171
    .local v1, "launch":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2172
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2175
    .local v0, "allowed":Ljava/lang/String;
    invoke-static {v1}, Lcom/microsoft/xle/test/interop/TestInterop;->setCompanionDebugUrl(Ljava/lang/String;)V

    .line 2176
    invoke-static {v2}, Lcom/microsoft/xle/test/interop/TestInterop;->setCompanionDebugTitleId(Ljava/lang/String;)V

    .line 2177
    invoke-static {v0}, Lcom/microsoft/xle/test/interop/TestInterop;->setCompanionDebugAllowedUrls(Ljava/lang/String;)V

    .line 2178
    return-void
.end method

.method static synthetic lambda$onReady$27()V
    .locals 1

    .prologue
    .line 1382
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->getInstance()Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->onResume()V

    return-void
.end method

.method static synthetic lambda$onReady$28()V
    .locals 1

    .prologue
    .line 1405
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->periodcallyUploadPhoneContacts()V

    return-void
.end method

.method static synthetic lambda$onResume$5(Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 748
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onResume$6(Lcom/microsoft/xbox/xle/app/MainActivity;Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 752
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->shouldResume(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 753
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->onReady()V

    .line 754
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->PRESENCE_HEARTBEAT:Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->setUserState(Z)V

    .line 757
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->setOnUpdateExistListener(Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;)V

    .line 758
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->loadAsync(Z)V

    .line 759
    return-void
.end method

.method static synthetic lambda$onResume$7(Lcom/microsoft/xbox/xle/app/MainActivity;Ljava/lang/String;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .param p1, "newSandboxId"    # Ljava/lang/String;

    .prologue
    .line 775
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onPause()V

    .line 776
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v1}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->signOutXsapi()V

    .line 777
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setCurrentSandboxId(Ljava/lang/String;)V

    .line 779
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPop(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 783
    :goto_0
    return-void

    .line 780
    :catch_0
    move-exception v0

    .line 781
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate to auth activity after changing sandbox"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$onSignInSuccess$29(Lcom/microsoft/xbox/xle/app/MainActivity;Ljava/lang/String;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .param p1, "ignored"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1464
    sget-object v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getRegistrationTokenWithEndpoint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 1465
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    .line 1466
    iget v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    .line 1467
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "Start loading conversation list after fetching skype token"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1468
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->loadMessageListAsync(Z)V

    .line 1469
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 1470
    return-void
.end method

.method static synthetic lambda$onStart$4(Lcom/microsoft/xbox/xle/app/MainActivity;Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/MainActivity;
    .param p1, "shouldShowTutorialExperience"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 559
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "shouldShowTutorialExperience? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->setUtilityBarState(Z)V

    .line 561
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->supportInvalidateOptionsMenu()V

    .line 562
    return-void

    .line 560
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$update$40()V
    .locals 1

    .prologue
    .line 2607
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->loadSkypeConversationMessages()V

    return-void
.end method

.method static synthetic lambda$update$41(Lcom/microsoft/xbox/xle/app/MainActivity;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/MainActivity;

    .prologue
    .line 2621
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    .line 2622
    iget v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    .line 2623
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->loadMessageListAsync(Z)V

    .line 2624
    return-void
.end method

.method private markProcessed(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 826
    const-string v0, "EXTRA_INTENT_PROCESSED"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private navigateToAuthFlowWithIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    .line 1083
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    .line 1084
    .local v1, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v4

    .line 1086
    .local v4, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1087
    .local v2, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v2, v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putForceSignin(Z)V

    .line 1088
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0703d9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putWelcomeAltButtonText(Ljava/lang/String;)V

    .line 1090
    const-string v6, "com.microsoft.xbox.extra.RELAUNCH_INTENT"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    .line 1091
    .local v3, "relaunchIntent":Landroid/content/Intent;
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putAppRelaunchIntent(Landroid/content/Intent;)V

    .line 1093
    if-eqz v4, :cond_2

    .line 1094
    instance-of v6, v4, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    if-eqz v6, :cond_0

    .line 1096
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v1, v2, v6}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Z)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1128
    :goto_0
    return-void

    .line 1097
    :catch_0
    move-exception v0

    .line 1098
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v6, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v7, "Error trying to restart XboxAuthActivity"

    invoke-static {v6, v7, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1100
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_0
    instance-of v6, v4, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;

    if-eqz v6, :cond_1

    move-object v5, v4

    .line 1103
    check-cast v5, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;

    .line 1104
    .local v5, "xsapiSilentSignInActivity":Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;
    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->setDisableNavigationOnUpdate(Z)V

    .line 1105
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;->leaveScreen(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1113
    .end local v5    # "xsapiSilentSignInActivity":Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;
    :cond_1
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1123
    :cond_2
    :try_start_1
    const-class v6, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    invoke-virtual {v1, v6, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PushScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1124
    :catch_1
    move-exception v0

    .line 1125
    .restart local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v6, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v7, "Error trying to launch XboxAuthActivity"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private navigateToClub(Landroid/os/Bundle;Ljava/lang/Class;)V
    .locals 3
    .param p1, "extras"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1056
    .local p2, "startScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity;->removeClubInAppNotification(Landroid/os/Bundle;)V

    .line 1057
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    .line 1058
    .local v0, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 1059
    .local v1, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v1, :cond_0

    .line 1060
    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/MainActivity;Landroid/os/Bundle;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    .line 1068
    :cond_0
    return-void
.end method

.method private navigateToFindPeople()V
    .locals 4

    .prologue
    .line 1294
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    .line 1295
    .local v0, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    .line 1297
    .local v2, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v2, :cond_0

    .line 1298
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1300
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1302
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$21;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    .line 1320
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    :goto_0
    return-void

    .line 1311
    .restart local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_1
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$22;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private navigateToGameProfile(Landroid/os/Bundle;Ljava/lang/Class;)V
    .locals 9
    .param p1, "extras"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1192
    .local p2, "pivot":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    if-eqz p1, :cond_0

    .line 1193
    const-string v6, "com.microsoft.xbox.extra.TITLEID"

    const-string v7, "com.microsoft.xbox.extra.TITLE_ID"

    const-string v8, "0"

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1194
    .local v4, "titleIdStr":Ljava/lang/String;
    const-string v6, "com.microsoft.xbox.extra.IS_XBOX360_GAME"

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1199
    .local v0, "isXbox360Game":Z
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1201
    .local v2, "titleId":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-nez v6, :cond_1

    .line 1234
    .end local v0    # "isXbox360Game":Z
    .end local v2    # "titleId":J
    .end local v4    # "titleIdStr":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1205
    .restart local v0    # "isXbox360Game":Z
    .restart local v2    # "titleId":J
    .restart local v4    # "titleIdStr":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    .line 1206
    .local v1, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v5

    .line 1207
    .local v5, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v5, :cond_0

    .line 1208
    invoke-static {v0, v2, v3, p2, v1}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$18;->lambdaFactory$(ZJLjava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private navigateToLfgDetails(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 1019
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->removeInAppNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 1020
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    .line 1021
    .local v1, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    .line 1022
    .local v2, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    const-string v3, "com.microsoft.xbox.extra.HANDLE_ID"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1023
    .local v0, "handleId":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1024
    instance-of v3, v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsScreen;

    if-eqz v3, :cond_1

    .line 1025
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceRefresh()V

    .line 1039
    :cond_0
    :goto_0
    return-void

    .line 1026
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1027
    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$13;->lambdaFactory$(Ljava/lang/String;Landroid/os/Bundle;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1035
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v4, "Empty handleId while navigating to LFG Details"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    const-string v3, "Empty handleId while navigating to LFG Details"

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private navigateToMessageScreen(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 956
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->removeInAppNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 957
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    .line 958
    .local v0, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    .line 959
    .local v2, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    const-string v3, "com.microsoft.xbox.extra.XUID"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 960
    .local v1, "senderXuid":Ljava/lang/String;
    const-string v3, "Empty xuid from notification"

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertFalse(Ljava/lang/String;Z)V

    .line 962
    if-eqz v2, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 963
    invoke-static {v1, p1, v0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$10;->lambdaFactory$(Ljava/lang/String;Landroid/os/Bundle;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    .line 973
    :cond_0
    return-void
.end method

.method private navigateToOOBE()V
    .locals 3

    .prologue
    .line 1323
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    .line 1324
    .local v0, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 1326
    .local v1, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v2}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->temporarilyForceDisableTutorialExperience()V

    .line 1328
    if-eqz v1, :cond_0

    .line 1329
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$23;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    .line 1337
    :cond_0
    return-void
.end method

.method private navigateToPartyDetails(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 999
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->removeInAppNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 1000
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    .line 1001
    .local v0, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 1005
    .local v1, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v1, :cond_0

    .line 1006
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    instance-of v2, v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;

    if-nez v2, :cond_0

    .line 1007
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$12;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    .line 1015
    :cond_0
    return-void
.end method

.method private navigateToPurchasePage(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 1238
    if-eqz p1, :cond_0

    .line 1239
    const-string v3, "com.microsoft.xbox.extra.BIGID"

    const-string v4, "com.microsoft.xbox.extra.BIGID"

    const-string v5, ""

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1241
    .local v0, "bigId":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1242
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    .line 1243
    .local v1, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    .line 1245
    .local v2, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v2, :cond_0

    .line 1246
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$19;->lambdaFactory$(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    .line 1274
    .end local v0    # "bigId":Ljava/lang/String;
    .end local v1    # "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .end local v2    # "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method private navigateToPushTargetIfPresent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 886
    const-string v2, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->fromType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v1

    .line 887
    .local v1, "notificationType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    if-eqz v1, :cond_0

    .line 889
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    if-ne v1, v2, :cond_1

    .line 890
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v3, "navigateToPushTargetIfPresent - pushed on Chat notification"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreen;

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToClub(Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 952
    :cond_0
    :goto_0
    return-void

    .line 893
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->canSLSNotify()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 894
    invoke-static {v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications;->trackPushNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/content/Intent;)V

    .line 895
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity$3;->$SwitchMap$com$microsoft$xbox$service$model$gcm$NotificationType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 946
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "navigateToPushTargetIfPresent - un-recognized SLS notification type:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 897
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToMessageScreen(Landroid/os/Bundle;)V

    goto :goto_0

    .line 902
    :pswitch_1
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->removeInAppNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 903
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToUserProfile(Landroid/os/Bundle;)V

    goto :goto_0

    .line 906
    :pswitch_2
    const-string v2, "com.microsoft.xbox.extra.NOTIFICATION_SUBTYPE"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 908
    .local v0, "notificationSubType":Ljava/lang/String;
    const-string/jumbo v2, "scheduledLfgExpired"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "imminentLfgExpired"

    .line 909
    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 910
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/activity/PartyAndLfgScreen;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    goto :goto_0

    .line 912
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToLfgDetails(Landroid/os/Bundle;)V

    goto :goto_0

    .line 920
    .end local v0    # "notificationSubType":Ljava/lang/String;
    :pswitch_3
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToClub(Landroid/os/Bundle;Ljava/lang/Class;)V

    goto :goto_0

    .line 926
    :pswitch_4
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminHomeScreen;

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToClub(Landroid/os/Bundle;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 929
    :pswitch_5
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToGameProfile(Landroid/os/Bundle;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 937
    :pswitch_6
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToTournamentDetails(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 940
    :pswitch_7
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->joinAndNavigateToPartyDetails(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 943
    :pswitch_8
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToPartyDetails(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 949
    :cond_4
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v3, "SLS push notification was tapped, but the user probably cleared the data in Android\'s app info screen earlier"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 895
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private navigateToSettings()V
    .locals 3

    .prologue
    .line 1277
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    .line 1278
    .local v0, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 1279
    .local v1, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v1, :cond_0

    .line 1280
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$20;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    .line 1291
    :cond_0
    return-void
.end method

.method private navigateToTournamentDetails(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 1071
    const-string v3, "com.microsoft.xbox.extra.TOURNAMENT_ORGANIZER"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1072
    .local v1, "organizer":Ljava/lang/String;
    const-string v3, "com.microsoft.xbox.extra.TOURNAMENT_ID"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1074
    .local v0, "id":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1075
    new-instance v2, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;

    invoke-direct {v2, v1, v0}, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    .local v2, "parameters":Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 1080
    .end local v2    # "parameters":Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;
    :goto_0
    return-void

    .line 1078
    :cond_0
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t get tournament details from extras: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private navigateToUserProfile(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 1131
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToUserProfile(Landroid/os/Bundle;Z)V

    .line 1132
    return-void
.end method

.method private navigateToUserProfile(Landroid/os/Bundle;Z)V
    .locals 6
    .param p1, "extras"    # Landroid/os/Bundle;
    .param p2, "shouldGoBackToGame"    # Z

    .prologue
    .line 1136
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 1137
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz p1, :cond_2

    .line 1138
    const-string v5, "com.microsoft.xbox.extra.XUID"

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-virtual {p1, v5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1147
    .local v3, "xuid":Ljava/lang/String;
    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1187
    .end local v3    # "xuid":Ljava/lang/String;
    :cond_0
    :goto_2
    return-void

    .line 1138
    :cond_1
    const-string v4, ""

    goto :goto_0

    .line 1139
    :cond_2
    if-eqz v0, :cond_0

    .line 1140
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "xuid":Ljava/lang/String;
    goto :goto_1

    .line 1151
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    .line 1152
    .local v1, "nm":Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    .line 1153
    .local v2, "top":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v2, :cond_0

    .line 1154
    invoke-static {v3, v0, p2, v1}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$17;->lambdaFactory$(Ljava/lang/String;Lcom/microsoft/xbox/service/model/ProfileModel;ZLcom/microsoft/xbox/toolkit/ui/NavigationManager;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->leaveScreen(Ljava/lang/Runnable;)V

    goto :goto_2
.end method

.method private navigateToViewTarget(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 876
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 877
    .local v0, "data":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 878
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 883
    :cond_1
    :goto_1
    return-void

    .line 878
    :pswitch_0
    const-string/jumbo v2, "smartglass://oobe"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 880
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToOOBE()V

    goto :goto_1

    .line 878
    nop

    :pswitch_data_0
    .packed-switch 0x885aa0a
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method private processLoggedInIntentNavigation(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 837
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 838
    .local v0, "action":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 839
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 873
    :goto_1
    return-void

    .line 839
    :sswitch_0
    const-string v3, "com.microsoft.xbox.action.ACTION_VIEW_USER_PROFILE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "com.microsoft.xbox.action.ACTION_VIEW_GAME_PROFILE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_2
    const-string v3, "com.microsoft.xbox.action.ACTION_VIEW_PURCHASE_PAGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "com.microsoft.xbox.action.ACTION_VIEW_ACHIEVEMENTS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "com.microsoft.xbox.action.ACTION_VIEW_SETTINGS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "com.microsoft.xbox.action.ACTION_SIGNIN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "com.microsoft.xbox.action.ACTION_FIND_PEOPLE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    .line 842
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToUserProfile(Landroid/os/Bundle;Z)V

    goto :goto_1

    .line 846
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToGameProfile(Landroid/os/Bundle;Ljava/lang/Class;)V

    goto :goto_1

    .line 850
    :pswitch_2
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToPurchasePage(Landroid/os/Bundle;)V

    goto :goto_1

    .line 854
    :pswitch_3
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToGameProfile(Landroid/os/Bundle;Ljava/lang/Class;)V

    goto :goto_1

    .line 858
    :pswitch_4
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToSettings()V

    goto :goto_1

    .line 861
    :pswitch_5
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToAuthFlowWithIntent(Landroid/content/Intent;)V

    goto :goto_1

    .line 864
    :pswitch_6
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToFindPeople()V

    goto :goto_1

    .line 867
    :pswitch_7
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToViewTarget(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 871
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToPushTargetIfPresent(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 839
    :sswitch_data_0
    .sparse-switch
        -0x74971f30 -> :sswitch_0
        -0x45ed2f16 -> :sswitch_7
        -0x3d90d609 -> :sswitch_1
        -0x1c138521 -> :sswitch_3
        -0x693fa1f -> :sswitch_5
        0x14dbf0f6 -> :sswitch_6
        0x44b852b2 -> :sswitch_2
        0x5b9ffd5e -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private processShowRemoteControlFromWidget(I)V
    .locals 2
    .param p1, "sessionState"    # I

    .prologue
    .line 2681
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->showRemoteControlFromWidget:Z

    if-eqz v1, :cond_0

    .line 2682
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    .line 2683
    .local v0, "dm":Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
    if-nez p1, :cond_1

    .line 2684
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$34;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog(Ljava/lang/Runnable;)V

    .line 2688
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->showRemoteControlFromWidget:Z

    .line 2690
    .end local v0    # "dm":Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
    :cond_0
    return-void

    .line 2686
    .restart local v0    # "dm":Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showRemoteControl()V

    goto :goto_0
.end method

.method private removeClubInAppNotification(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 1042
    const-string v1, "com.microsoft.xbox.extra.NOTIFICATION_TYPE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->fromType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v0

    .line 1043
    .local v0, "notificationType":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->removeInAppNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V

    .line 1044
    return-void
.end method

.method private declared-synchronized shouldResume(Z)Z
    .locals 2
    .param p1, "isResume"    # Z

    .prologue
    const/4 v0, 0x1

    .line 1343
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->hasRunOnReady:Z

    if-nez v1, :cond_1

    if-nez p1, :cond_0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->paused:Z

    if-nez v1, :cond_1

    .line 1344
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->hasRunOnReady:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1348
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1343
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private trackLocaleInfo()V
    .locals 6

    .prologue
    .line 1411
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 1412
    .local v1, "deviceLocale":Ljava/util/Locale;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    .line 1413
    .local v0, "appLocale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v2

    .line 1415
    .local v2, "market":Ljava/lang/String;
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Device locale: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Language in use: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1417
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Market in use: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Console locale: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/SessionModel;->getConsoleLocale()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stored console locale: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getConnectedLocale()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1420
    return-void
.end method

.method private updateDrawerLockMode(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 2503
    instance-of v0, p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewImpl;

    if-eqz v0, :cond_1

    .line 2510
    :cond_0
    :goto_0
    return-void

    .line 2507
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2508
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getDesiredDrawerLockState()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0
.end method


# virtual methods
.method public addContentViewXLE(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 6
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 1625
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting content xle for screen "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1627
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1628
    invoke-virtual {p1, v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setAllEventsEnabled(Z)V

    .line 1629
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addContentViewXLE screens.size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1655
    :goto_0
    return-void

    .line 1632
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isKeepPreviousScreen()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1634
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setAllEventsEnabled(Z)V

    .line 1642
    :cond_1
    :goto_1
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1643
    .local v0, "activityParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1644
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1646
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->contentFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1647
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->bringToFront()V

    .line 1648
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getCollapsedAppBarView()Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->bringToFront()V

    .line 1649
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getPageIndicator()Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1650
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getPageIndicator()Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/PageIndicator;->bringToFront()V

    .line 1653
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v1, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1654
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addContentViewXLE screens.size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1637
    .end local v0    # "activityParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setAllEventsEnabled(Z)V

    .line 1638
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeViewFromParent(Landroid/view/View;)V

    goto :goto_1
.end method

.method public addOnActivityResultCallback(Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 2226
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 2227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->onActivityResultCallbacks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2228
    return-void
.end method

.method public closeDrawer()V
    .locals 2

    .prologue
    .line 2441
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 2442
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->leftDrawer:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 2447
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1583
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isBlocking()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1585
    const/4 v1, 0x1

    .line 1595
    :goto_0
    return v1

    .line 1588
    :cond_0
    :try_start_0
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1589
    :catch_0
    move-exception v0

    .line 1593
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "MainActivity dispatchTouchEvent failed"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1594
    const-string v1, "MainActivity dispatchTouchEvent failed"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1595
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public findDimensionIdByName(Ljava/lang/String;)I
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 2260
    const/4 v1, 0x0

    .line 2262
    .local v1, "field":Ljava/lang/reflect/Field;
    :try_start_0
    const-class v3, Lcom/microsoft/xboxone/smartglass/R$dimen;

    invoke-virtual {v3, p1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2268
    :goto_0
    const/4 v2, -0x1

    .line 2269
    .local v2, "id":I
    if-eqz v1, :cond_0

    .line 2272
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 2279
    :cond_0
    :goto_1
    return v2

    .line 2263
    .end local v2    # "id":I
    :catch_0
    move-exception v0

    .line 2264
    .local v0, "ex":Ljava/lang/NoSuchFieldException;
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to find dimension with name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2265
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to find dimension with name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 2273
    .end local v0    # "ex":Ljava/lang/NoSuchFieldException;
    .restart local v2    # "id":I
    :catch_1
    move-exception v0

    .line 2274
    .local v0, "ex":Ljava/lang/IllegalAccessException;
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to access dimension with name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2275
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to access dimension with name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public findViewByString(Ljava/lang/String;)Landroid/view/View;
    .locals 6
    .param p1, "viewName"    # Ljava/lang/String;

    .prologue
    .line 2237
    const/4 v1, 0x0

    .line 2239
    .local v1, "field":Ljava/lang/reflect/Field;
    :try_start_0
    const-class v3, Lcom/microsoft/xboxone/smartglass/R$id;

    invoke-virtual {v3, p1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2245
    :goto_0
    const/4 v2, -0x1

    .line 2246
    .local v2, "id":I
    if-eqz v1, :cond_0

    .line 2249
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 2256
    :cond_0
    :goto_1
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    return-object v3

    .line 2240
    .end local v2    # "id":I
    :catch_0
    move-exception v0

    .line 2241
    .local v0, "ex":Ljava/lang/NoSuchFieldException;
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to find view with name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2242
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to find view with name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 2250
    .end local v0    # "ex":Ljava/lang/NoSuchFieldException;
    .restart local v2    # "id":I
    :catch_1
    move-exception v0

    .line 2251
    .local v0, "ex":Ljava/lang/IllegalAccessException;
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to access view with name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2252
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to access view with name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getPlayReadyFactory()Lcom/microsoft/playready/IPlayReadyFactory;
    .locals 1

    .prologue
    .line 2741
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->sPlayReadyFactory:Lcom/microsoft/playready/IPlayReadyFactory;

    return-object v0
.end method

.method public getRefreshEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2737
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->refreshRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public getScreenHeight()I
    .locals 2

    .prologue
    .line 2364
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2365
    sget v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_WIDTH:I

    sget v1, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_HEIGHT:I

    if-le v0, v1, :cond_0

    sget v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_HEIGHT:I

    .line 2367
    :goto_0
    return v0

    .line 2365
    :cond_0
    sget v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_WIDTH:I

    goto :goto_0

    .line 2367
    :cond_1
    sget v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_HEIGHT:I

    goto :goto_0
.end method

.method public getScreenWidth()I
    .locals 3

    .prologue
    const v2, 0x7f0904f8

    .line 2356
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2357
    sget v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_WIDTH:I

    sget v1, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_HEIGHT:I

    if-le v0, v1, :cond_0

    sget v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_WIDTH:I

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2359
    :goto_0
    return v0

    .line 2357
    :cond_0
    sget v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_HEIGHT:I

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 2359
    :cond_1
    sget v0, Lcom/microsoft/xbox/xle/app/MainActivity;->START_SCREEN_WIDTH:I

    goto :goto_0
.end method

.method public getTitleBarView()Lcom/microsoft/xbox/xle/ui/TitleBarView;
    .locals 1

    .prologue
    .line 2289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    return-object v0
.end method

.method public goBack()V
    .locals 8

    .prologue
    .line 1522
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->ShouldBackCloseApp()Z

    move-result v7

    .line 1525
    .local v7, "shouldCloseApp":Z
    if-nez v7, :cond_1

    .line 1526
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GoBack()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1536
    :goto_0
    if-eqz v7, :cond_0

    .line 1537
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "Stack empty; exiting app"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1538
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->finish()V

    .line 1540
    :cond_0
    return-void

    .line 1530
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopScreensAndReplace(ILjava/lang/Class;ZZZ)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1532
    :catch_0
    move-exception v6

    .line 1533
    .local v6, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "Error attempting to goBack"

    invoke-static {v0, v1, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public hasTwoPanes()Z
    .locals 1

    .prologue
    .line 2322
    const v0, 0x7f0e0789

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideKeyboard()V
    .locals 2

    .prologue
    .line 1613
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 1615
    .local v0, "currentScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 1616
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$27;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 1622
    :cond_0
    return-void
.end method

.method public invokeDefaultOnBackPressed()V
    .locals 1

    .prologue
    .line 1558
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isBlocking()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1559
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->OnBackButtonPressed()V

    .line 1561
    :cond_0
    return-void
.end method

.method public isBlocking()Z
    .locals 1

    .prologue
    .line 1547
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getIsBlocking()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->animationBlocking:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDrawerOpen()Z
    .locals 2

    .prologue
    .line 2460
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->leftDrawer:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadingConversation()Z
    .locals 1

    .prologue
    .line 2645
    iget v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNewLaunch()Z
    .locals 1

    .prologue
    .line 1543
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->isNewLaunch:Z

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 2732
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->paused:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 2202
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2204
    const/16 v2, 0xc8

    if-ne p1, v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    .line 2205
    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreen;

    if-nez v2, :cond_3

    .line 2206
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->savedInstanceState:Landroid/os/Bundle;

    invoke-static {p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ActivityResult;->with(IILandroid/content/Intent;)Lcom/microsoft/xbox/toolkit/ActivityResult;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;->fromActivityResult(Landroid/os/Bundle;Lcom/microsoft/xbox/toolkit/ActivityResult;)Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;

    move-result-object v1

    .line 2207
    .local v1, "parameters":Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 2209
    if-eqz v1, :cond_1

    .line 2210
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreen;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 2216
    .end local v1    # "parameters":Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel$UploadCustomPicScreenParameters;
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-eqz v2, :cond_2

    .line 2217
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v2, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onActivityResult(IILandroid/content/Intent;)V

    .line 2220
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->onActivityResultCallbacks:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;

    .line 2221
    .local v0, "callback":Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;
    invoke-interface {v0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1

    .line 2212
    .end local v0    # "callback":Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2213
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v2, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2223
    :cond_4
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1553
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    invoke-virtual {v0}, Lcom/facebook/react/ReactInstanceManager;->onBackPressed()V

    .line 1554
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1448
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1449
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1450
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->contentFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestLayout()V

    .line 1451
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1565
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 1566
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 1567
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    .line 1569
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 20
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 314
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;->getInstance(Landroid/app/Activity;)Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;->setLogging()V

    .line 315
    sget-object v17, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v18, "onCreate"

    invoke-static/range {v17 .. v18}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    sget-object v17, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/app/MainActivity;)V

    .line 319
    invoke-static {}, Lcom/microsoft/xbox/toolkit/rx/RxUtils;->setUncaughtExceptionHandler()V

    .line 327
    const-string v17, "phone"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/telephony/TelephonyManager;

    .line 328
    .local v15, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v15}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v9

    .line 330
    .local v9, "deviceId":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    const-string v18, "android_id"

    invoke-static/range {v17 .. v18}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 332
    .local v4, "androidId":Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 333
    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->getInstance()Lcom/appsflyer/AppsFlyerLib;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/appsflyer/AppsFlyerLib;->setImeiData(Ljava/lang/String;)V

    .line 336
    :cond_0
    if-eqz v4, :cond_1

    .line 337
    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->getInstance()Lcom/appsflyer/AppsFlyerLib;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/appsflyer/AppsFlyerLib;->setAndroidIdData(Ljava/lang/String;)V

    .line 340
    :cond_1
    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->getInstance()Lcom/appsflyer/AppsFlyerLib;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getApplication()Landroid/app/Application;

    move-result-object v18

    const-string v19, "cb5Gbpxd6U2NBZCwHc2gt7"

    invoke-virtual/range {v17 .. v19}, Lcom/appsflyer/AppsFlyerLib;->startTracking(Landroid/app/Application;Ljava/lang/String;)V

    .line 341
    invoke-static {}, Lcom/appsflyer/AppsFlyerLib;->getInstance()Lcom/appsflyer/AppsFlyerLib;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/appsflyer/AppsFlyerLib;->getAppsFlyerUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 343
    .local v6, "appsFlyerUID":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->setAttributionId(Ljava/lang/String;)V

    .line 344
    sget-object v17, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "AppsFlyerLib Unique Id: \'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v17

    const-string v18, "Launch"

    invoke-virtual/range {v17 .. v18}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->beginTrackPerformance(Ljava/lang/String;)V

    .line 348
    invoke-static {}, Lcom/facebook/FacebookSdk;->isInitialized()Z

    move-result v17

    if-nez v17, :cond_2

    .line 349
    invoke-static/range {p0 .. p0}, Lcom/facebook/FacebookSdk;->sdkInitialize(Landroid/content/Context;)V

    .line 352
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/appevents/AppEventsLogger;->activateApp(Landroid/content/Context;)V

    .line 354
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->ensureDisplayLocale()V

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    move-object/from16 v18, v0

    .line 357
    invoke-interface/range {v18 .. v18}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v18

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v19

    .line 358
    invoke-virtual/range {v18 .. v19}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v18

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v19

    .line 359
    invoke-virtual/range {v18 .. v19}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v18

    .line 356
    invoke-virtual/range {v17 .. v18}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 362
    invoke-super/range {p0 .. p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 363
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->savedInstanceState:Landroid/os/Bundle;

    .line 365
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/XLEApplication;->setMainActivity(Lcom/microsoft/xbox/xle/app/MainActivity;)V

    .line 367
    if-nez p1, :cond_8

    const/16 v17, 0x1

    :goto_0
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->needToRestoreState:Z

    .line 368
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v17

    if-eqz v17, :cond_3

    .line 369
    sget-object v17, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v18, "pop all screens, the app restarted"

    invoke-static/range {v17 .. v18}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopAllScreens()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :cond_3
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v17

    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 381
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->paused:Z

    .line 387
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->savedInstanceState:Landroid/os/Bundle;

    .line 389
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .line 391
    new-instance v17, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->messageIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;

    .line 392
    new-instance v17, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->alertIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;

    .line 393
    new-instance v17, Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->friendsIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;

    .line 394
    new-instance v17, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->refreshIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

    .line 395
    new-instance v17, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->partyLfgIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    .line 403
    :cond_4
    const/16 v17, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->setVolumeControlStream(I)V

    .line 406
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v16

    .line 407
    .local v16, "window":Landroid/view/Window;
    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Landroid/view/Window;->setFormat(I)V

    .line 408
    const/16 v17, 0x1000

    invoke-virtual/range {v16 .. v17}, Landroid/view/Window;->addFlags(I)V

    .line 410
    const v17, 0x7f03028e

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-super {v0, v1}, Landroid/support/v7/app/AppCompatActivity;->setContentView(I)V

    .line 413
    const v17, 0x7f0e0786

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->contentFrame:Landroid/widget/RelativeLayout;

    .line 414
    const v17, 0x7f0e0785

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/support/v4/widget/DrawerLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 415
    const v17, 0x7f0e0524

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ScrollView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->leftDrawer:Landroid/widget/ScrollView;

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    move-object/from16 v17, v0

    const/high16 v18, -0x67000000

    invoke-virtual/range {v17 .. v18}, Landroid/support/v4/widget/DrawerLayout;->setScrimColor(I)V

    .line 419
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f020065

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 420
    .local v7, "background":Landroid/graphics/drawable/Drawable;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->contentFrame:Landroid/widget/RelativeLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 425
    invoke-static/range {p0 .. p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 427
    :try_start_1
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/webkit/CookieManager;->removeExpiredCookie()V

    .line 428
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 434
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    .line 435
    .local v3, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v3, :cond_5

    .line 436
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 437
    sget-object v17, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v18, 0x7f070b4a

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setHomeActionContentDescription(Ljava/lang/CharSequence;)V

    .line 438
    const v17, 0x7f02013f

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 439
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 440
    new-instance v17, Landroid/graphics/drawable/ColorDrawable;

    sget-object v18, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v19, 0x7f0c0149

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    invoke-direct/range {v17 .. v18}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 441
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setElevation(F)V

    .line 445
    :cond_5
    new-instance v17, Lcom/microsoft/xbox/xle/ui/TitleBarView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/ui/TitleBarView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    .line 446
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v17, -0x1

    sget-object v18, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v19, 0x7f09011b

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 447
    .local v14, "titleBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v17, 0xa

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->contentFrame:Landroid/widget/RelativeLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v14}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 451
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onCreate()V

    .line 452
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v17, -0x1

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 453
    .local v5, "appBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 454
    const/16 v17, 0xd

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 455
    const/16 v17, 0xe

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->contentFrame:Landroid/widget/RelativeLayout;

    move-object/from16 v17, v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getCollapsedAppBarView()Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 459
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getPageIndicator()Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    move-result-object v17

    if-eqz v17, :cond_6

    .line 460
    new-instance v12, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v17, -0x1

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 461
    .local v12, "pageParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v17, 0x2

    const v18, 0x7f0e005f

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 462
    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    .line 463
    const/16 v17, 0xe

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->contentFrame:Landroid/widget/RelativeLayout;

    move-object/from16 v17, v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getPageIndicator()Lcom/microsoft/xbox/toolkit/ui/PageIndicator;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v12}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 468
    .end local v12    # "pageParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6
    sget-object v17, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string v18, "armeabi-v7a"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 471
    :try_start_2
    invoke-static/range {p0 .. p0}, Lcom/microsoft/playready/PlayReadySuperFactory;->createFactory(Landroid/content/Context;)Lcom/microsoft/playready/IPlayReadyFactory;

    move-result-object v17

    sput-object v17, Lcom/microsoft/xbox/xle/app/MainActivity;->sPlayReadyFactory:Lcom/microsoft/playready/IPlayReadyFactory;

    .line 472
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->setCanLoadStreamingLibrary(Z)V
    :try_end_2
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_2

    .line 478
    :cond_7
    :goto_3
    move-object/from16 v8, p0

    .line 480
    .local v8, "context":Landroid/content/Context;
    new-instance v13, Lcom/microsoft/xbox/xle/app/MainActivity$1;

    sget-object v17, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$4;->lambdaFactory$(Landroid/content/Context;)Ljava/lang/Runnable;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v13, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity$1;-><init>(Lcom/microsoft/xbox/xle/app/MainActivity;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 520
    .local v13, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 521
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->addOnNavigatedListener(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$OnNavigatedListener;)V

    .line 523
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;->getInstance(Landroid/app/Activity;)Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;->checkForUpdates()V

    .line 525
    const-string v17, "accessibility"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/microsoft/xbox/xle/app/ActivityFeedViewAccessibilityDelegate;->setAccessibilityEnabled(Z)V

    .line 528
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$5;->lambdaFactory$()Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/xle/app/MainActivity;->accessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/MainActivity;->accessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 530
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v17

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->loadAsync(Z)V

    .line 533
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->initMessagingStorage()V

    .line 534
    return-void

    .line 367
    .end local v3    # "actionBar":Landroid/support/v7/app/ActionBar;
    .end local v5    # "appBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "background":Landroid/graphics/drawable/Drawable;
    .end local v8    # "context":Landroid/content/Context;
    .end local v13    # "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    .end local v14    # "titleBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v16    # "window":Landroid/view/Window;
    :cond_8
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 372
    :catch_0
    move-exception v10

    .line 374
    .local v10, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v17, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v18, "failed to pop all screens"

    invoke-static/range {v17 .. v18}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 429
    .end local v10    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v7    # "background":Landroid/graphics/drawable/Drawable;
    .restart local v16    # "window":Landroid/view/Window;
    :catch_1
    move-exception v11

    .line 430
    .local v11, "ex":Ljava/lang/Exception;
    sget-object v17, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v18, "CookieManager error"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 473
    .end local v11    # "ex":Ljava/lang/Exception;
    .restart local v3    # "actionBar":Landroid/support/v7/app/ActionBar;
    .restart local v5    # "appBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v14    # "titleBarParams":Landroid/widget/RelativeLayout$LayoutParams;
    :catch_2
    move-exception v10

    .line 474
    .local v10, "e":Ljava/lang/LinkageError;
    :goto_4
    sget-object v17, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v18, "Failed to initialize PlayReady library"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 473
    .end local v10    # "e":Ljava/lang/LinkageError;
    :catch_3
    move-exception v10

    goto :goto_4
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 1574
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1575
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 1576
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 1577
    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 1579
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 13
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v12, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v11, 0x2

    .line 1672
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1674
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 1675
    .local v0, "activityBase":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    if-nez v0, :cond_0

    .line 1789
    :goto_0
    return v7

    .line 1679
    :cond_0
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    .line 1681
    .local v3, "isAlreadyHandled":Z
    if-nez v3, :cond_2

    .line 1683
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v9

    if-nez v9, :cond_1

    .line 1684
    const v9, 0x7f0e0052

    const v10, 0x7f070d34

    invoke-interface {p1, v11, v9, v7, v10}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    .line 1685
    .local v2, "friendsItem":Landroid/view/MenuItem;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->friendsIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;

    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 1686
    invoke-interface {v2, v11}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1689
    .end local v2    # "friendsItem":Landroid/view/MenuItem;
    :cond_1
    const v9, 0x7f0e0056

    invoke-interface {p1, v11, v9, v7, v12}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v5

    .line 1690
    .local v5, "partyLfgItem":Landroid/view/MenuItem;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->partyLfgIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;

    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 1691
    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1693
    const v9, 0x7f0e0055

    invoke-interface {p1, v11, v9, v7, v12}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v4

    .line 1694
    .local v4, "messageItem":Landroid/view/MenuItem;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->messageIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;

    invoke-interface {v4, v9}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 1695
    invoke-interface {v4, v11}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1697
    const v9, 0x7f0e0035

    invoke-interface {p1, v11, v9, v7, v12}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    .line 1698
    .local v1, "alertItem":Landroid/view/MenuItem;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->alertIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;

    invoke-interface {v1, v9}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 1699
    invoke-interface {v1, v11}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1701
    const v9, 0x7f0e0057

    const v10, 0x7f070630

    invoke-interface {p1, v8, v9, v7, v10}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v6

    .line 1702
    .local v6, "refreshItem":Landroid/view/MenuItem;
    invoke-interface {v6, v8}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1703
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->refreshIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 1711
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$28;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v7

    const-wide/16 v10, 0x1388

    invoke-static {v7, v10, v11}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .end local v1    # "alertItem":Landroid/view/MenuItem;
    .end local v4    # "messageItem":Landroid/view/MenuItem;
    .end local v5    # "partyLfgItem":Landroid/view/MenuItem;
    .end local v6    # "refreshItem":Landroid/view/MenuItem;
    :cond_2
    move v7, v8

    .line 1789
    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1478
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1479
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 1481
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    invoke-virtual {v1, p0}, Lcom/facebook/react/ReactInstanceManager;->onHostDestroy(Landroid/app/Activity;)V

    .line 1487
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PopAllScreens()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1492
    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/XLEApplication;->setMainActivity(Lcom/microsoft/xbox/xle/app/MainActivity;)V

    .line 1494
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    if-eqz v1, :cond_0

    .line 1495
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onDestroy()V

    .line 1496
    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    .line 1497
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1498
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "navigation stack is not empty!"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1502
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-eqz v1, :cond_1

    .line 1503
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onDestroy()V

    .line 1506
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/TvStreamerModel;->destroy(Z)V

    .line 1507
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->onDestroy()V

    .line 1508
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->onDestroy()V

    .line 1510
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->accessibilityStateChangeListener:Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 1513
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->close()V

    .line 1514
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 1516
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->onActivityResultCallbacks:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 1517
    return-void

    .line 1488
    :catch_0
    move-exception v0

    .line 1489
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "Failed to pop an activity from the stack"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onExplicitConnectionRequired()V
    .locals 2

    .prologue
    .line 2514
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "Explicitly request to bring up console picker"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2516
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog()V

    .line 2517
    return-void
.end method

.method public onLocaleChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "oldLocale"    # Ljava/lang/String;
    .param p2, "newLocale"    # Ljava/lang/String;

    .prologue
    .line 2694
    sget-object v6, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Console locale changed from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2695
    iget-boolean v6, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->paused:Z

    if-eqz v6, :cond_1

    .line 2729
    :cond_0
    :goto_0
    return-void

    .line 2698
    :cond_1
    move-object v1, p2

    .line 2699
    .local v1, "locale":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 2700
    move-object v1, p1

    .line 2707
    :cond_2
    if-eqz p2, :cond_4

    .line 2708
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getConnectedLocale()Ljava/lang/String;

    move-result-object v3

    .line 2709
    .local v3, "oldConnectedLocale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    .line 2710
    .local v0, "deviceLocale":Ljava/lang/String;
    if-nez v3, :cond_5

    move-object v4, v0

    .line 2711
    .local v4, "oldLogicalLocale":Ljava/lang/String;
    :goto_1
    invoke-virtual {p2, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_3

    .line 2712
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 2713
    .local v2, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v6, "PreviousLocale"

    invoke-virtual {v2, v6, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2714
    const-string v6, "CurrentLocale"

    invoke-virtual {v2, v6, p2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2715
    const-string v6, "EPG Locale Changed"

    invoke-static {v6, v2}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 2717
    .end local v2    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->saveConnectedLocale(Ljava/lang/String;)V

    .line 2720
    .end local v0    # "deviceLocale":Ljava/lang/String;
    .end local v3    # "oldConnectedLocale":Ljava/lang/String;
    .end local v4    # "oldLogicalLocale":Ljava/lang/String;
    :cond_4
    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2721
    sget-object v6, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "locale is different, refresh (old: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " new: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722
    new-instance v5, Lcom/microsoft/xbox/xle/app/MainActivity$2;

    sget-object v6, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$35;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v7

    invoke-direct {v5, p0, v6, v7}, Lcom/microsoft/xbox/xle/app/MainActivity$2;-><init>(Lcom/microsoft/xbox/xle/app/MainActivity;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 2727
    .local v5, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto/16 :goto_0

    .end local v5    # "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    .restart local v0    # "deviceLocale":Ljava/lang/String;
    .restart local v3    # "oldConnectedLocale":Ljava/lang/String;
    :cond_5
    move-object v4, v3

    .line 2710
    goto :goto_1
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1
    .param p1, "featureId"    # I
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 2196
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 2197
    invoke-super {p0, p1, p2}, Landroid/support/v7/app/AppCompatActivity;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onMustUpdate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2528
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->setOnUpdateExistListener(Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;)V

    .line 2529
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "must update the app"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2530
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    if-eqz v0, :cond_0

    .line 2531
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "login screen on top of stack, ignore"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2536
    :goto_0
    return-void

    .line 2534
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->setOnUpdateExistListener(Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;)V

    .line 2535
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil;->forceUpdate()V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 831
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "onNewIntent"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 833
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity;->setIntent(Landroid/content/Intent;)V

    .line 834
    return-void
.end method

.method public onOptionalUpdate()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2540
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->setOnUpdateExistListener(Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;)V

    .line 2543
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    if-eqz v1, :cond_1

    .line 2558
    :cond_0
    :goto_0
    return-void

    .line 2548
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getLatestVersion()I

    move-result v6

    .line 2549
    .local v6, "latestVersion":I
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getLastCheckedVersion()I

    move-result v1

    if-ge v1, v6, :cond_0

    .line 2550
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070ca2

    .line 2552
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070ca5

    .line 2553
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$29;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070607

    .line 2555
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$30;->lambdaFactory$(I)Ljava/lang/Runnable;

    move-result-object v5

    .line 2550
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 14
    .param p1, "item"    # Landroid/view/MenuItem;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 1803
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 1806
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v7}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->isTutorialExperienceEnabled()Z

    move-result v2

    .line 1810
    .local v2, "isTutorialExperienceEnabled":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isDrawerOpen()Z

    move-result v7

    if-eqz v7, :cond_0

    if-nez v2, :cond_0

    .line 1811
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 1814
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 1815
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-nez v0, :cond_2

    .line 1819
    sget-object v7, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v8, "Returned activityBase is null because navigation stack is empty"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2191
    :cond_1
    :goto_0
    :sswitch_0
    return v12

    :cond_2
    move-object v7, v0

    .line 1823
    check-cast v7, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    invoke-virtual {v7, p1}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    .line 1824
    .local v1, "isAlreadyHandled":Z
    if-nez v1, :cond_1

    .line 1825
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    goto :goto_0

    .line 1841
    :sswitch_1
    if-eqz v2, :cond_4

    .line 1842
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v7, v12}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/accessibilityservice/AccessibilityServiceInfo;

    .line 1843
    .local v3, "serviceInfo":Landroid/accessibilityservice/AccessibilityServiceInfo;
    invoke-virtual {v3}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getId()Ljava/lang/String;

    move-result-object v8

    const-string v9, "TalkBackService"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1844
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v8}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->setAppToNormalState()V

    .line 1845
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->toggleDrawer()V

    goto :goto_1

    .line 1849
    .end local v3    # "serviceInfo":Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->toggleDrawer()V

    goto :goto_0

    .line 1854
    :sswitch_2
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->refreshRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v8, Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;->INSTANCE:Lcom/microsoft/xbox/toolkit/rx/RxUtils$RxNotification;

    invoke-virtual {v7, v8}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 1855
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceRefresh()V

    .line 1856
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-eqz v7, :cond_1

    .line 1857
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->forceRefresh()V

    goto :goto_0

    .line 1861
    :sswitch_3
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setScreenState(I)V

    goto :goto_0

    .line 1864
    :sswitch_4
    invoke-virtual {v0, v13}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setScreenState(I)V

    goto :goto_0

    .line 1867
    :sswitch_5
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setScreenState(I)V

    goto :goto_0

    .line 1870
    :sswitch_6
    invoke-virtual {v0, v12}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setScreenState(I)V

    goto :goto_0

    .line 1873
    :sswitch_7
    sget-boolean v7, Lcom/microsoft/xbox/toolkit/Build;->TestNetworkDisconnectivity:Z

    if-eqz v7, :cond_5

    .line 1874
    sget-object v7, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v8, "Unfreezing network state"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875
    sput-boolean v13, Lcom/microsoft/xbox/toolkit/Build;->TestNetworkDisconnectivity:Z

    goto :goto_0

    .line 1877
    :cond_5
    sget-object v7, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v8, "Freezing network state"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1878
    sput-boolean v12, Lcom/microsoft/xbox/toolkit/Build;->TestNetworkDisconnectivity:Z

    goto/16 :goto_0

    .line 1932
    :sswitch_8
    const/4 v5, 0x1

    .line 1933
    .local v5, "x":I
    const/4 v6, 0x0

    .line 1934
    .local v6, "y":I
    const-string v7, "CRASHTEST"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%s"

    new-array v10, v12, [Ljava/lang/Object;

    div-int v11, v5, v6

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v13

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2008
    .end local v5    # "x":I
    .end local v6    # "y":I
    :sswitch_9
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v7

    const-class v8, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView;

    new-instance v9, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;

    const-string/jumbo v10, "xboxlive"

    const-string v11, "915ebb23-3017-4e34-9b5b-4efbc5ca7cc2"

    invoke-direct {v9, v10, v11}, Lcom/microsoft/xbox/presentation/arena/TournamentDetailsPageView$TournamentDetailsPageParameters;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8, v12, v9}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto/16 :goto_0

    .line 2014
    :sswitch_a
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    invoke-virtual {v7}, Lcom/facebook/react/ReactInstanceManager;->showDevOptionsDialog()V

    goto/16 :goto_0

    .line 2017
    :sswitch_b
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 2018
    .local v4, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "app_first_run"

    invoke-interface {v7, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2019
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "home_screen_pref"

    invoke-interface {v7, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 1825
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f0e0038 -> :sswitch_8
        0x7f0e0039 -> :sswitch_0
        0x7f0e003a -> :sswitch_0
        0x7f0e003b -> :sswitch_0
        0x7f0e003c -> :sswitch_0
        0x7f0e003d -> :sswitch_0
        0x7f0e003e -> :sswitch_0
        0x7f0e003f -> :sswitch_0
        0x7f0e0040 -> :sswitch_0
        0x7f0e0041 -> :sswitch_0
        0x7f0e0042 -> :sswitch_9
        0x7f0e0043 -> :sswitch_0
        0x7f0e0044 -> :sswitch_0
        0x7f0e0045 -> :sswitch_0
        0x7f0e0046 -> :sswitch_0
        0x7f0e0047 -> :sswitch_a
        0x7f0e0048 -> :sswitch_b
        0x7f0e0049 -> :sswitch_7
        0x7f0e004a -> :sswitch_6
        0x7f0e004b -> :sswitch_3
        0x7f0e004c -> :sswitch_5
        0x7f0e004d -> :sswitch_4
        0x7f0e004e -> :sswitch_0
        0x7f0e0057 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPageNavigated(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 12
    .param p1, "from"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "to"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 2465
    if-nez p1, :cond_2

    const-string v0, ""

    .line 2466
    .local v0, "fromPageName":Ljava/lang/String;
    :goto_0
    if-nez p2, :cond_3

    const-string v1, ""

    .line 2467
    .local v1, "toPageName":Ljava/lang/String;
    :goto_1
    if-nez p2, :cond_4

    const-string v3, ""

    .line 2468
    .local v3, "content":Ljava/lang/String;
    :goto_2
    if-nez p2, :cond_5

    const-string v2, ""

    .line 2469
    .local v2, "contentKey":Ljava/lang/String;
    :goto_3
    if-nez p2, :cond_6

    const-string v5, ""

    .line 2470
    .local v5, "relativeId":Ljava/lang/String;
    :goto_4
    if-nez p2, :cond_7

    const-string v4, ""

    .line 2471
    .local v4, "relativeIdKey":Ljava/lang/String;
    :goto_5
    if-nez p1, :cond_8

    move v9, v10

    :goto_6
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 2472
    .local v6, "trackFromPage":Ljava/lang/Boolean;
    if-nez p2, :cond_9

    :goto_7
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 2473
    .local v7, "trackToPage":Ljava/lang/Boolean;
    if-nez p2, :cond_a

    move v9, v11

    :goto_8
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 2475
    .local v8, "useUTC":Ljava/lang/Boolean;
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 2476
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->trackPageViewTelemetry()V

    .line 2482
    :cond_0
    :goto_9
    const-string v9, "OneGuide"

    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-eq v9, v10, :cond_1

    .line 2483
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v9

    invoke-virtual {v9, v11}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->dismiss(Z)V

    .line 2486
    :cond_1
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/xle/app/MainActivity;->updateDrawerLockMode(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 2487
    return-void

    .line 2465
    .end local v0    # "fromPageName":Ljava/lang/String;
    .end local v1    # "toPageName":Ljava/lang/String;
    .end local v2    # "contentKey":Ljava/lang/String;
    .end local v3    # "content":Ljava/lang/String;
    .end local v4    # "relativeIdKey":Ljava/lang/String;
    .end local v5    # "relativeId":Ljava/lang/String;
    .end local v6    # "trackFromPage":Ljava/lang/Boolean;
    .end local v7    # "trackToPage":Ljava/lang/Boolean;
    .end local v8    # "useUTC":Ljava/lang/Boolean;
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2466
    .restart local v0    # "fromPageName":Ljava/lang/String;
    :cond_3
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 2467
    .restart local v1    # "toPageName":Ljava/lang/String;
    :cond_4
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getContent()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 2468
    .restart local v3    # "content":Ljava/lang/String;
    :cond_5
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getContentKey()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2469
    .restart local v2    # "contentKey":Ljava/lang/String;
    :cond_6
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getRelativeId()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 2470
    .restart local v5    # "relativeId":Ljava/lang/String;
    :cond_7
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getRelativeIdKey()Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 2471
    .restart local v4    # "relativeIdKey":Ljava/lang/String;
    :cond_8
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getTrackPage()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    goto :goto_6

    .line 2472
    .restart local v6    # "trackFromPage":Ljava/lang/Boolean;
    :cond_9
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getTrackPage()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    goto :goto_7

    .line 2473
    .restart local v7    # "trackToPage":Ljava/lang/Boolean;
    :cond_a
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getUseUTCTelemetry()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    goto :goto_8

    .line 2477
    .restart local v8    # "useUTC":Ljava/lang/Boolean;
    :cond_b
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2478
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->trackLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9
.end method

.method public onPageRestarted(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 2491
    if-eqz p1, :cond_0

    .line 2492
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity;->updateDrawerLockMode(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 2494
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 616
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v3, "onPause"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    invoke-virtual {v2, p0}, Lcom/facebook/react/ReactInstanceManager;->onHostPause(Landroid/app/Activity;)V

    .line 626
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;->getInstance(Landroid/app/Activity;)Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;

    move-result-object v2

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;->shutdown()V

    .line 629
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 630
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 631
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    .line 632
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    .line 635
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->onApplicationPause()V

    .line 637
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/DialogManager;->onApplicationPause()V

    .line 638
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SoundManager;->getInstance()Lcom/microsoft/xbox/toolkit/SoundManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/SoundManager;->setEnabled(Z)V

    .line 639
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->paused:Z

    .line 641
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 643
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    if-eqz v2, :cond_1

    .line 644
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onPause()V

    .line 647
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v2}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->isTutorialExperienceEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 648
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 651
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->onPause()V

    .line 652
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onPause()V

    .line 653
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->getInstance()Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->onPause()V

    .line 654
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->onPause()V

    .line 657
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;->BACKGROUND:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/SessionModel;->leaveSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    .line 658
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackAppDeactivate()V

    .line 660
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 661
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/service/model/SessionModel;->setExplicitConnectListner(Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;)V

    .line 662
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/service/model/SessionModel;->setOnSessionDroppedHandler(Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;)V

    .line 663
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/microsoft/xbox/service/model/SessionModel;->setOnLocaleChangedHandler(Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;)V

    .line 665
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 666
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_3

    .line 667
    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 672
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getStayAwakeSetting()Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->Always:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    if-eq v2, v3, :cond_4

    .line 673
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setKeepScreenOn(Z)V

    .line 676
    :cond_4
    monitor-enter p0

    .line 677
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->paused:Z

    .line 678
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->hasRunOnReady:Z

    .line 679
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 681
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->PRESENCE_HEARTBEAT:Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->setUserState(Z)V

    .line 682
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->partyEventNotifier:Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;

    invoke-virtual {v2, v5}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->setAppPaused(Z)V

    .line 683
    return-void

    .line 679
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v1, 0x1

    .line 1794
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 1796
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->isTutorialExperienceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1, v1, v0}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1797
    return v1

    :cond_0
    move v0, v1

    .line 1796
    goto :goto_0
.end method

.method public onReady()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1352
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v2, "onReady"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 1357
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->setExplicitConnectListner(Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;)V

    .line 1358
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->setOnSessionDroppedHandler(Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;)V

    .line 1359
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/SessionModel;->setOnLocaleChangedHandler(Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;)V

    .line 1361
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->trackLocaleInfo()V

    .line 1363
    invoke-static {}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->hasIcons()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1365
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->loadFriendFinderSettingsAsync(Z)V

    .line 1368
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 1369
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_1

    .line 1370
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 1371
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadAsync(Z)V

    .line 1374
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->gcmObserver:Lcom/microsoft/xbox/toolkit/XLEObserver;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 1376
    invoke-static {}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->getInstance()Lcom/microsoft/xbox/service/model/SnappableAppsModel;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/model/SnappableAppsModel;->loadSnappableAppsListAsync(Z)V

    .line 1378
    invoke-static {}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->getInstance()Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/PhoneContactFinderTicketModel;->loadAsync()V

    .line 1379
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->onResume()V

    .line 1380
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onResume()V

    .line 1382
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$24;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x5

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 1384
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    if-eqz v1, :cond_3

    .line 1385
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onResume()V

    .line 1386
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onSetActive()V

    .line 1393
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-nez v1, :cond_4

    .line 1394
    const v1, 0x7f0e0789

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    .line 1395
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-eqz v1, :cond_2

    .line 1396
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onCreate()V

    .line 1397
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onStart()V

    .line 1398
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->forceRefresh()V

    .line 1404
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->refreshIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;->setRightPaneContentLayout(Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;)V

    .line 1405
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$25;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x14

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 1407
    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getLfgLanguages()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    .line 1408
    return-void

    .line 1388
    :cond_3
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    .line 1389
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onStart()V

    .line 1390
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->load()V

    goto :goto_0

    .line 1401
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onStart()V

    .line 1402
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->forceRefresh()V

    goto :goto_1
.end method

.method protected onRestart()V
    .locals 2

    .prologue
    .line 687
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "onRestart"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    invoke-static {p0}, Lcom/microsoft/xbox/XLEApplication;->setMainActivity(Lcom/microsoft/xbox/xle/app/MainActivity;)V

    .line 689
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onRestart()V

    .line 690
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 2386
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2388
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 2389
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 2390
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2393
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    if-eqz v1, :cond_1

    .line 2394
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2396
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x0

    const/4 v3, 0x1

    .line 694
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->reactInstanceManager:Lcom/facebook/react/ReactInstanceManager;

    invoke-virtual {v0, p0, p0}, Lcom/facebook/react/ReactInstanceManager;->onHostResume(Landroid/app/Activity;Lcom/facebook/react/modules/core/DefaultHardwareBackBtnHandler;)V

    .line 698
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->appLife:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->resetMetric(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V

    .line 699
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->appLife:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->start(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V

    .line 700
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->connected:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->resetMetric(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V

    .line 701
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->companionLife:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->resetMetric(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V

    .line 702
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->companionConnected:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->resetMetric(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V

    .line 708
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/DialogManager;->setEnabled(Z)V

    .line 709
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->paused:Z

    if-eqz v0, :cond_0

    .line 711
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v8

    .line 712
    .local v8, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v8, :cond_0

    .line 713
    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    .line 714
    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    .line 722
    .end local v8    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->onApplicationResume()V

    .line 723
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->onApplicationResume()V

    .line 725
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 727
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/XLEHockeyIntegration;->getInstance(Landroid/app/Activity;)Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/IXLEHockeyIntegration;->checkForCrashes()V

    .line 730
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SoundManager;->getInstance()Lcom/microsoft/xbox/toolkit/SoundManager;

    move-result-object v0

    invoke-virtual {v0, v12}, Lcom/microsoft/xbox/toolkit/SoundManager;->setEnabled(Z)V

    .line 732
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getStayAwakeSetting()Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->Always:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    if-ne v0, v1, :cond_1

    .line 733
    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setKeepScreenOn(Z)V

    .line 736
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->shouldResume(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 739
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->onReady()V

    .line 740
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->PRESENCE_HEARTBEAT:Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/PresenceHeartbeat;->setUserState(Z)V

    .line 743
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->setOnUpdateExistListener(Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;)V

    .line 763
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->getInstance()Lcom/microsoft/xbox/xle/app/DeviceCapabilities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->onResume()V

    .line 765
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->onResume()V

    .line 768
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->checkLaunchParameter()V

    .line 770
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->sandboxOption:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 771
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->sandboxOption:Ljava/lang/String;

    .line 772
    .local v10, "newSandboxId":Ljava/lang/String;
    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->sandboxOption:Ljava/lang/String;

    .line 773
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const-string v1, "Warning"

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070434

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070738

    .line 774
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v10}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/MainActivity;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f070736

    .line 783
    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 773
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 809
    .end local v10    # "newSandboxId":Ljava/lang/String;
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->partyEventNotifier:Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;

    invoke-virtual {v0, v12}, Lcom/microsoft/xbox/presentation/party/PartyEventNotifier;->setAppPaused(Z)V

    .line 811
    monitor-enter p0

    .line 812
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->paused:Z

    .line 813
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 814
    return-void

    .line 745
    :cond_4
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "User has not signed in silently or normally"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 747
    invoke-interface {v1}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$7;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v2

    .line 748
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    .line 749
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    .line 750
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/MainActivity;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 751
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 746
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 786
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    .line 787
    .local v9, "intent":Landroid/content/Intent;
    invoke-direct {p0, v9}, Lcom/microsoft/xbox/xle/app/MainActivity;->isProcessed(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 788
    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 789
    .local v7, "action":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "ACTION_LAUNCH_REMOTE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 790
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->showRemoteControlFromWidget:Z

    .line 791
    invoke-direct {p0, v9}, Lcom/microsoft/xbox/xle/app/MainActivity;->markProcessed(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->setIntent(Landroid/content/Intent;)V

    goto :goto_1

    .line 793
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 795
    const-string v0, "com.microsoft.xbox.action.ACTION_SIGNIN"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 796
    invoke-direct {p0, v9}, Lcom/microsoft/xbox/xle/app/MainActivity;->processLoggedInIntentNavigation(Landroid/content/Intent;)V

    .line 799
    :cond_7
    invoke-direct {p0, v9}, Lcom/microsoft/xbox/xle/app/MainActivity;->markProcessed(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->setIntent(Landroid/content/Intent;)V

    goto :goto_1

    .line 800
    :cond_8
    const-string v0, "com.microsoft.xbox.action.ACTION_SIGNIN"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 801
    invoke-direct {p0, v9}, Lcom/microsoft/xbox/xle/app/MainActivity;->navigateToAuthFlowWithIntent(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 813
    .end local v7    # "action":Ljava/lang/String;
    .end local v9    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 2372
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2374
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 2375
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    .line 2376
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2379
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    if-eqz v1, :cond_1

    .line 2380
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2382
    :cond_1
    return-void
.end method

.method public onSessionDropped()V
    .locals 1

    .prologue
    .line 2521
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getRunningStress()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2522
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->getInstance()Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->autoRetryConnect()V

    .line 2524
    :cond_0
    return-void
.end method

.method public onSignInSuccess()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1454
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getDatabaseNeedsSync()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1455
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->initConversationDataFromStorage()V

    .line 1460
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    sget-object v1, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->getToken()Lio/reactivex/Single;

    move-result-object v1

    .line 1461
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    .line 1462
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$26;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/MainActivity;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 1463
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 1460
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 1472
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->setOnUpdateExistListener(Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;)V

    .line 1473
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->loadAsync(Z)V

    .line 1474
    return-void

    .line 1457
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->getInstance()Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->setResyncNeeded(Z)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 538
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStart()V

    .line 540
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->paused:Z

    .line 541
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/DialogManager;->setEnabled(Z)V

    .line 542
    sget-object v4, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v5, "onStart"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->setOnUpdateExistListener(Lcom/microsoft/xbox/xle/model/SystemSettingsModel$OnUpdateExistListener;)V

    .line 547
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v4

    if-nez v4, :cond_3

    .line 549
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->isNewLaunch:Z

    .line 552
    invoke-static {}, Lcom/microsoft/xbox/xle/app/Feedback;->trackLaunchCount()V

    .line 554
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->tutorialRepository:Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    .line 555
    invoke-interface {v5}, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;->isTutorialExperienceEnabledObservable()Lio/reactivex/Observable;

    move-result-object v5

    .line 556
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v6

    invoke-virtual {v5, v6}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v5

    .line 557
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v6

    invoke-virtual {v5, v6}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/MainActivity;)Lio/reactivex/functions/Consumer;

    move-result-object v6

    .line 558
    invoke-virtual {v5, v6}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v5

    .line 554
    invoke-virtual {v4, v5}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 565
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v4}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->isSignedIn()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 566
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->loadAsync(Z)V

    .line 568
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    if-eqz v4, :cond_0

    .line 569
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onStart()V

    .line 570
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->load()V

    .line 573
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigateToHomeScreenOrOOBETutorial()V

    .line 578
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->homeScreenRepository:Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;

    invoke-virtual {v4}, Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenRepository;->getHomeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic;->trackDefaultHomePage(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V

    .line 608
    :cond_1
    :goto_1
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->isNewLaunch:Z

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->VortexInitialize(ZLandroid/content/Intent;)V

    .line 612
    :goto_2
    return-void

    .line 575
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/XsapiSilentSignInActivity;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PushScreen(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 609
    :catch_0
    move-exception v1

    .line 610
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v4, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to start activity: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 581
    .end local v1    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_3
    :try_start_1
    sget-object v4, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "the application activity restarted for whatever reason."

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->isNewLaunch:Z

    .line 584
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->needToRestoreState:Z

    if-nez v4, :cond_4

    .line 585
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Z)V

    goto :goto_1

    .line 590
    :cond_4
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 591
    .local v2, "outState":Landroid/os/Bundle;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 592
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_5

    .line 593
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 596
    :cond_5
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v3

    .line 597
    .local v3, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->RestartCurrentScreen(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Z)V

    .line 600
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 601
    if-eqz v0, :cond_1

    .line 602
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onRestoreInstanceState(Landroid/os/Bundle;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 1424
    sget-object v0, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStop()V

    .line 1427
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    if-eqz v0, :cond_0

    .line 1428
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onSetInactive()V

    .line 1429
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerViewModel:Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->onStop()V

    .line 1432
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-eqz v0, :cond_1

    .line 1433
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onSetInactive()V

    .line 1434
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onStop()V

    .line 1441
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->VortexShutdown()V

    .line 1442
    return-void
.end method

.method public openDrawer()V
    .locals 2

    .prologue
    .line 2432
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 2436
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->leftDrawer:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(Landroid/view/View;)V

    .line 2438
    :cond_0
    return-void
.end method

.method public refreshRightPaneData()V
    .locals 1

    .prologue
    .line 2326
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-eqz v0, :cond_0

    .line 2327
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->forceRefresh()V

    .line 2329
    :cond_0
    return-void
.end method

.method public removeContentViewXLE(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 1661
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v1, p1}, Ljava/util/Stack;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1662
    .local v0, "idx":I
    if-ltz v0, :cond_0

    .line 1663
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 1664
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeViewFromParent(Landroid/view/View;)V

    goto :goto_0

    .line 1667
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "removeContentViewXLE screens.size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->screens:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1668
    return-void
.end method

.method public removeOnActivityResultCallback(Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/microsoft/xbox/xle/app/MainActivity$OnActivityResultCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 2231
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 2232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->onActivityResultCallbacks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2233
    return-void
.end method

.method public resetRightPaneData()V
    .locals 2

    .prologue
    .line 2337
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-eqz v1, :cond_0

    .line 2338
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .line 2339
    .local v0, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    if-eqz v0, :cond_0

    .line 2340
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->reset()V

    .line 2341
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onStop()V

    .line 2344
    .end local v0    # "viewModel":Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    :cond_0
    return-void
.end method

.method public setAnimationBlocking(Z)V
    .locals 3
    .param p1, "blocking"    # Z

    .prologue
    .line 1602
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->animationBlocking:Z

    if-eq v0, p1, :cond_0

    .line 1603
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->animationBlocking:Z

    .line 1604
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->animationBlocking:Z

    if-eqz v0, :cond_1

    .line 1605
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->Navigation:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x1388

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 1610
    :cond_0
    :goto_0
    return-void

    .line 1607
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->Navigation:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->clearBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;)V

    goto :goto_0
.end method

.method public setRequestedOrientation(I)V
    .locals 1
    .param p1, "requestedOrientation"    # I

    .prologue
    .line 2348
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->hasTwoPanes()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2349
    const/4 v0, 0x6

    invoke-super {p0, v0}, Landroid/support/v7/app/AppCompatActivity;->setRequestedOrientation(I)V

    .line 2353
    :goto_0
    return-void

    .line 2351
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public setShowTitleBar(Z)V
    .locals 2
    .param p1, "showTitleBar"    # Z

    .prologue
    .line 2283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    if-eqz v0, :cond_0

    .line 2284
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->titleBar:Lcom/microsoft/xbox/xle/ui/TitleBarView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/TitleBarView;->setVisibility(I)V

    .line 2286
    :cond_0
    return-void

    .line 2284
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setUtilityBarState(Z)V
    .locals 8
    .param p1, "enable"    # Z

    .prologue
    .line 2399
    sget-object v5, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Should the utility bar be enabled? "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2400
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 2402
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->friendsIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xle/ui/UtilityBarFriendsButton;->setClickable(Z)V

    .line 2403
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->partyLfgIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xle/ui/UtilityBarPartyLfgButton;->setClickable(Z)V

    .line 2404
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->messageIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;->setClickable(Z)V

    .line 2405
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->alertIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;->setClickable(Z)V

    .line 2406
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->refreshIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/xle/ui/UtilityBarRefreshButton;->setClickable(Z)V

    .line 2408
    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Integer;

    const/4 v6, 0x0

    const v7, 0x7f0e0bbf

    .line 2409
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const v7, 0x7f0e0bc2

    .line 2410
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const v7, 0x7f0e0bc0

    .line 2411
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const v7, 0x7f0e0bbd

    .line 2412
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const v7, 0x7f0e0bc3

    .line 2413
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 2408
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 2416
    .local v4, "icons":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2417
    .local v3, "customTypeFaceTextViewId":I
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 2418
    .local v2, "customTypeFaceTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0c018a

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2419
    .local v1, "color":I
    :goto_1
    if-eqz v2, :cond_0

    .line 2420
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    goto :goto_0

    .line 2418
    .end local v1    # "color":I
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0c014d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1

    .line 2424
    .end local v2    # "customTypeFaceTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .end local v3    # "customTypeFaceTextViewId":I
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 2425
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_3

    .line 2426
    if-eqz p1, :cond_4

    const v5, 0x7f02013f

    :goto_2
    invoke-virtual {v0, v5}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 2429
    :cond_3
    return-void

    .line 2426
    :cond_4
    const v5, 0x7f020140

    goto :goto_2
.end method

.method public setVisibilityRightPane(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 2332
    const v0, 0x7f0e0788

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 2333
    return-void
.end method

.method public toggleDrawer()V
    .locals 2

    .prologue
    .line 2450
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 2451
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->leftDrawer:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2452
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->closeDrawer()V

    .line 2457
    :cond_0
    :goto_0
    return-void

    .line 2454
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->openDrawer()V

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 2562
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2563
    sget-object v3, Lcom/microsoft/xbox/xle/app/MainActivity$3;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2642
    :cond_0
    :goto_0
    return-void

    .line 2565
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/MainActivity;->handleSessionState(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 2568
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    if-nez v2, :cond_0

    .line 2572
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v2}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->isSignedIn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2573
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 2574
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v1, :cond_1

    .line 2575
    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 2576
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->alertIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarAlertsButton;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 2577
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->messageIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 2578
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->messageIconActionView:Lcom/microsoft/xbox/xle/ui/UtilityBarMessageButton;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 2581
    invoke-static {}, Lcom/microsoft/xbox/service/model/ExperimentModel;->getInstance()Lcom/microsoft/xbox/service/model/ExperimentModel;

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/service/model/ExperimentModel;->load(J)V

    .line 2583
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2584
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isProcessed(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2587
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->processLoggedInIntentNavigation(Landroid/content/Intent;)V

    .line 2588
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->markProcessed(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->setIntent(Landroid/content/Intent;)V

    .line 2592
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-nez v2, :cond_3

    .line 2593
    const v2, 0x7f0e0789

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    .line 2594
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    if-eqz v2, :cond_0

    .line 2595
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onCreate()V

    .line 2596
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onStart()V

    goto :goto_0

    .line 2599
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->onStart()V

    .line 2600
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->rightPaneContentLayout:Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/activity/PeopleScreen;->forceRefresh()V

    goto :goto_0

    .line 2606
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getMostRecentConversationListCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 2607
    invoke-static {}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$31;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x1388

    invoke-static {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    .line 2609
    :cond_4
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v3, "All Conversations loaded"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2610
    iput v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    .line 2611
    iput v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    .line 2612
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    goto/16 :goto_0

    .line 2616
    :pswitch_3
    iget v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    .line 2618
    iget v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/MessageModel;->getMostRecentConversationListCount()I

    move-result v3

    if-ne v2, v3, :cond_5

    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MessageModel;->moreConversationExists()Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    if-ge v2, v5, :cond_5

    .line 2619
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/MainActivity$$Lambda$32;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/MainActivity;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x2710

    invoke-static {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    .line 2626
    :cond_5
    iget v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/MessageModel;->getMostRecentConversationListCount()I

    move-result v3

    if-lt v2, v3, :cond_6

    .line 2627
    iput v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    .line 2630
    :cond_6
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/MessageModel;->moreConversationExists()Z

    move-result v2

    if-eqz v2, :cond_7

    iget v2, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    if-lt v2, v5, :cond_0

    .line 2631
    :cond_7
    sget-object v2, Lcom/microsoft/xbox/xle/app/MainActivity;->TAG:Ljava/lang/String;

    const-string v3, "Max allowed Conversations loaded"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 2632
    iput v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationMessagesloaded:I

    .line 2633
    iput v4, p0, Lcom/microsoft/xbox/xle/app/MainActivity;->countConversationPagesloaded:I

    .line 2634
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    goto/16 :goto_0

    .line 2563
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
