.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$7;
.super Ljava/lang/Object;
.source "UTCGameHub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->trackShowFriendAction(Lcom/microsoft/xbox/service/model/FollowersData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$gamer:Lcom/microsoft/xbox/service/model/FollowersData;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$7;->val$gamer:Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 212
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$7;->val$gamer:Lcom/microsoft/xbox/service/model/FollowersData;

    if-eqz v2, :cond_0

    .line 213
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 214
    .local v1, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v2, "TitleId"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->access$000()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 215
    const-string v2, "TargetXuid"

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$7;->val$gamer:Lcom/microsoft/xbox/service/model/FollowersData;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 217
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub$7;->val$gamer:Lcom/microsoft/xbox/service/model/FollowersData;

    instance-of v0, v2, Lcom/microsoft/xbox/service/model/GameProfileVipData;

    .line 218
    .local v0, "isVip":Z
    const-string v3, "FriendType"

    if-eqz v0, :cond_1

    const-string v2, "VIP"

    :goto_0
    invoke-virtual {v1, v3, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 220
    const-string v2, "Game Hub - Show Friend Profile"

    const-string v3, "Game Hub - Friends View"

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 222
    .end local v0    # "isVip":Z
    .end local v1    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_0
    return-void

    .line 218
    .restart local v0    # "isVip":Z
    .restart local v1    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_1
    const-string v2, "Friend"

    goto :goto_0
.end method
