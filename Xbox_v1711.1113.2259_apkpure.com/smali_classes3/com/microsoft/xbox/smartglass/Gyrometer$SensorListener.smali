.class Lcom/microsoft/xbox/smartglass/Gyrometer$SensorListener;
.super Ljava/lang/Object;
.source "Gyrometer.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/Gyrometer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/Gyrometer;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/smartglass/Gyrometer;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/Gyrometer$SensorListener;->this$0:Lcom/microsoft/xbox/smartglass/Gyrometer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/smartglass/Gyrometer;Lcom/microsoft/xbox/smartglass/Gyrometer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/smartglass/Gyrometer;
    .param p2, "x1"    # Lcom/microsoft/xbox/smartglass/Gyrometer$1;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/Gyrometer$SensorListener;-><init>(Lcom/microsoft/xbox/smartglass/Gyrometer;)V

    return-void
.end method

.method private radiansToDegrees(F)D
    .locals 4
    .param p1, "radians"    # F

    .prologue
    .line 75
    float-to-double v0, p1

    const-wide v2, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 63
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 67
    new-instance v1, Lcom/microsoft/xbox/smartglass/GyrometerReading;

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/smartglass/Gyrometer$SensorListener;->radiansToDegrees(F)D

    move-result-wide v2

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/smartglass/Gyrometer$SensorListener;->radiansToDegrees(F)D

    move-result-wide v4

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/smartglass/Gyrometer$SensorListener;->radiansToDegrees(F)D

    move-result-wide v6

    iget-wide v8, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-direct/range {v1 .. v9}, Lcom/microsoft/xbox/smartglass/GyrometerReading;-><init>(DDDJ)V

    .line 69
    .local v1, "reading":Lcom/microsoft/xbox/smartglass/GyrometerReading;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/Gyrometer$SensorListener;->this$0:Lcom/microsoft/xbox/smartglass/Gyrometer;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/Gyrometer;->cloneListeners()Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/GyrometerListener;

    .line 70
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/GyrometerListener;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/Gyrometer$SensorListener;->this$0:Lcom/microsoft/xbox/smartglass/Gyrometer;

    invoke-virtual {v0, v3, v1}, Lcom/microsoft/xbox/smartglass/GyrometerListener;->onReadingChanged(Lcom/microsoft/xbox/smartglass/Gyrometer;Lcom/microsoft/xbox/smartglass/GyrometerReading;)V

    goto :goto_0

    .line 72
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/GyrometerListener;
    :cond_0
    return-void
.end method
