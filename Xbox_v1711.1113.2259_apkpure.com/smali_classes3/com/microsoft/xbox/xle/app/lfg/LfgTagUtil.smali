.class public Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;
.super Ljava/lang/Object;
.source "LfgTagUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private achievementTagsHashCode:I

.field private final mergedTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation
.end field

.field private tagsHashCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->mergedTags:Ljava/util/List;

    .line 33
    return-void
.end method


# virtual methods
.method public getMergedTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->mergedTags:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public mergeTags(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;J)Z
    .locals 22
    .param p1, "searchAttributes"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "titleId"    # J

    .prologue
    .line 44
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 46
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->tags()Ljava/util/List;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v15

    .line 47
    .local v15, "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->achievementIds()Ljava/util/List;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 48
    .local v7, "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v15}, Ljava/util/List;->hashCode()I

    move-result v14

    .line 49
    .local v14, "tagIdHashcode":I
    invoke-interface {v7}, Ljava/util/List;->hashCode()I

    move-result v6

    .line 51
    .local v6, "achievementIdHashcode":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->tagsHashCode:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v14, v0, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->achievementTagsHashCode:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v6, v0, :cond_0

    .line 53
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v19

    add-int v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->mergedTags:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_9

    .line 54
    :cond_0
    move-object/from16 v0, p0

    iput v14, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->tagsHashCode:I

    .line 55
    move-object/from16 v0, p0

    iput v6, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->achievementTagsHashCode:I

    .line 56
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v8, "achievementTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .local v11, "systemTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v10, "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 60
    .local v9, "achievementsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v18

    move-object/from16 v0, v18

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v16

    .line 62
    .local v16, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v16, :cond_5

    .line 63
    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v17

    .line 65
    .local v17, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    if-eqz v17, :cond_2

    .line 66
    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v18

    if-eqz v18, :cond_1

    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressXboxoneAchievements()Ljava/util/List;

    move-result-object v18

    if-eqz v18, :cond_1

    .line 67
    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressXboxoneAchievements()Ljava/util/List;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_3

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 68
    .local v4, "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v0, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v9, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 70
    .end local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_1
    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 71
    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_3

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 72
    .restart local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    iget-object v0, v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v9, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 76
    .end local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    :cond_2
    sget-object v18, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Could not load titleModel for titleId: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .end local v17    # "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    :cond_3
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_4
    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 83
    .local v5, "achievementId":Ljava/lang/String;
    invoke-interface {v9, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 85
    .restart local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    if-eqz v4, :cond_4

    .line 86
    invoke-static {v4}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;->with(Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 79
    .end local v4    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .end local v5    # "achievementId":Ljava/lang/String;
    :cond_5
    sget-object v18, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "TitleData not available for TitleId: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 90
    :cond_6
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 91
    .local v13, "tagId":Ljava/lang/String;
    sget-object v19, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getTag(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    move-result-object v12

    .line 93
    .local v12, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    if-eqz v12, :cond_7

    .line 94
    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 96
    :cond_7
    invoke-static {v13}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;->with(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 100
    .end local v12    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .end local v13    # "tagId":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->mergedTags:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->clear()V

    .line 101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->mergedTags:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->mergedTags:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgTagUtil;->mergedTags:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 105
    const/16 v18, 0x1

    .line 108
    .end local v8    # "achievementTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;>;"
    .end local v9    # "achievementsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;"
    .end local v10    # "customTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$CustomSocialTag;>;"
    .end local v11    # "systemTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    .end local v16    # "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :goto_5
    return v18

    :cond_9
    const/16 v18, 0x0

    goto :goto_5
.end method
