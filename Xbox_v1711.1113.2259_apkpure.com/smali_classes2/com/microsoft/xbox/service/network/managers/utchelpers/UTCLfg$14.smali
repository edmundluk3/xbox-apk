.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$14;
.super Ljava/lang/Object;
.source "UTCLfg.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackShareTo(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$action:Ljava/lang/String;

.field final synthetic val$handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$14;->val$action:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$14;->val$handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 3

    .prologue
    .line 284
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$14;->val$action:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 285
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$14;->val$handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 287
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 288
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$14;->val$handle:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v1

    .line 289
    .local v1, "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    if-eqz v1, :cond_0

    .line 290
    const-string v2, "SessionRef"

    invoke-virtual {v0, v2, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 293
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg$14;->val$action:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 294
    return-void
.end method
