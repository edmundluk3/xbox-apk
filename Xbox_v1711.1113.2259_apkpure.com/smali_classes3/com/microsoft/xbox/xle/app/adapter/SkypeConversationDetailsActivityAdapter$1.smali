.class Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;
.super Ljava/lang/Object;
.source "SkypeConversationDetailsActivityAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 590
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$onItemLongClick$0(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;Landroid/view/View;Landroid/view/MenuItem;)Z
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;
    .param p1, "partnerXuid"    # Ljava/lang/String;
    .param p2, "conversation"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .param p3, "view"    # Landroid/view/View;
    .param p4, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 612
    invoke-interface {p4}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e0c32

    if-ne v1, v2, :cond_1

    .line 613
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    move-result-object v1

    iget-object v2, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    iget-object v3, p2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->clientmessageid:Ljava/lang/String;

    invoke-virtual {v1, p1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->deleteSkypeMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 614
    :cond_1
    invoke-interface {p4}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e0c33

    if-ne v1, v2, :cond_2

    .line 615
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->onCopyMessageText(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    .line 616
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "clipboard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 617
    .local v0, "copyManager":Landroid/content/ClipboardManager;
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getContent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 618
    .end local v0    # "copyManager":Landroid/content/ClipboardManager;
    :cond_2
    invoke-interface {p4}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e0c34

    if-ne v1, v2, :cond_0

    .line 619
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v2

    invoke-virtual {v1, p2, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->navigateToEnforcement(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;Z)V

    goto :goto_0
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 592
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;

    .line 593
    .local v5, "tagData":Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;
    if-eqz v5, :cond_1

    .line 594
    iget-object v0, v5, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$SkypeConversationDetailsListItemTagData;->conversationMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .line 596
    .local v0, "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    new-instance v3, Landroid/widget/PopupMenu;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 597
    .local v3, "popup":Landroid/widget/PopupMenu;
    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v7

    const v8, 0x7f0f000a

    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 598
    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v7

    const v8, 0x7f0e0c34

    invoke-interface {v7, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 599
    .local v4, "reportMenuItem":Landroid/view/MenuItem;
    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v7

    const v8, 0x7f0e0c32

    invoke-interface {v7, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 600
    .local v1, "deleteMenuItem":Landroid/view/MenuItem;
    iget-object v7, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v6

    .line 602
    .local v6, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v7, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->TopicUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    if-eq v6, v7, :cond_0

    iget-object v7, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$000(Ljava/util/Date;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 603
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderXuid()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v7, 0x1

    :goto_0
    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 608
    :goto_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getPartnerXuid()Ljava/lang/String;

    move-result-object v2

    .line 609
    .local v2, "partnerXuid":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v7

    if-nez v7, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    const/4 v7, 0x1

    :goto_2
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 611
    invoke-static {p0, v2, v0, p2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$1;Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;Landroid/view/View;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 623
    invoke-virtual {v3}, Landroid/widget/PopupMenu;->show()V

    .line 625
    .end local v0    # "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .end local v1    # "deleteMenuItem":Landroid/view/MenuItem;
    .end local v2    # "partnerXuid":Ljava/lang/String;
    .end local v3    # "popup":Landroid/widget/PopupMenu;
    .end local v4    # "reportMenuItem":Landroid/view/MenuItem;
    .end local v6    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_1
    const/4 v7, 0x1

    return v7

    .line 603
    .restart local v0    # "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
    .restart local v1    # "deleteMenuItem":Landroid/view/MenuItem;
    .restart local v3    # "popup":Landroid/widget/PopupMenu;
    .restart local v4    # "reportMenuItem":Landroid/view/MenuItem;
    .restart local v6    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    .line 605
    :cond_3
    const/4 v7, 0x0

    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 609
    .restart local v2    # "partnerXuid":Ljava/lang/String;
    :cond_4
    const/4 v7, 0x0

    goto :goto_2
.end method
