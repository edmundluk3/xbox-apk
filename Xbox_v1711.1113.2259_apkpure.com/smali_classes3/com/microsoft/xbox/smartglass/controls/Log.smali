.class abstract Lcom/microsoft/xbox/smartglass/controls/Log;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "Log.java"


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Log"

.field private static final WriteMethod:Ljava/lang/String; = "Write"


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 18
    const-string v0, "Log"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 20
    const-string v0, "Write"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Log;->registerMethod(Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Log;->validateMethod(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/Log;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 33
    :goto_0
    return-void

    .line 32
    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/microsoft/xbox/smartglass/controls/Log;->write(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract write(ILjava/lang/String;)V
.end method

.method protected writeInternal(ILjava/lang/String;)V
    .locals 3
    .param p1, "requestId"    # I
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Activity] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Information:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Log;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V

    .line 38
    return-void
.end method
