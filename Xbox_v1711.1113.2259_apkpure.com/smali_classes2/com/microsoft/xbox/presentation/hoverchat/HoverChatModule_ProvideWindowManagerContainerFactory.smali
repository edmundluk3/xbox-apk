.class public final Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;
.super Ljava/lang/Object;
.source "HoverChatModule_ProvideWindowManagerContainerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p2, "contextProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/Context;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;->module:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    .line 24
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;->contextProvider:Ljavax/inject/Provider;

    .line 26
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;",
            "Ljavax/inject/Provider",
            "<",
            "Landroid/content/Context;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "contextProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Landroid/content/Context;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;-><init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvideWindowManagerContainer(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;Landroid/content/Context;)Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;->provideWindowManagerContainer(Landroid/content/Context;)Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;
    .locals 2

    .prologue
    .line 30
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;->module:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;->contextProvider:Ljavax/inject/Provider;

    .line 31
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule;->provideWindowManagerContainer(Landroid/content/Context;)Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 30
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatModule_ProvideWindowManagerContainerFactory;->get()Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    move-result-object v0

    return-object v0
.end method
