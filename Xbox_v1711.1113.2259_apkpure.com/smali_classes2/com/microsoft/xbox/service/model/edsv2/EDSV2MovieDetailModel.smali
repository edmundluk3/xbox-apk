.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.source "EDSV2MovieDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected bridge synthetic createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;
    .locals 1
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-object v0
.end method

.method public getLaunchType()Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->AppLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method

.method public getMediaGroup()I
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getMediaType()I

    move-result v0

    const/16 v1, 0x3f0

    if-ne v0, v1, :cond_0

    .line 45
    const/4 v0, 0x5

    .line 47
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public getMetaCriticReviewScore()F
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->getMetaCriticReviewScore()F

    move-result v0

    return v0
.end method

.method protected getRelatedMediaType()I
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getMediaType()I

    move-result v0

    const/16 v1, 0x3f0

    if-ne v0, v1, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 98
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x3e8

    goto :goto_0
.end method

.method public getRottenTomatoReviewSource()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    .locals 4

    .prologue
    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ReviewSources:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->ReviewSources:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;

    .line 72
    .local v0, "reviewSource":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;->Name:Ljava/lang/String;

    const-string v3, "Rotten Tomatoes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    .end local v0    # "reviewSource":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRottenTomatoReviews()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2CriticReview;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->getRottenTomatoReviewSource()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;

    move-result-object v0

    .line 83
    .local v0, "reviewSource":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    if-eqz v0, :cond_0

    .line 84
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;->CriticReviews:Ljava/util/ArrayList;

    .line 87
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getStudio()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MovieMediaItem;->getStudio()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleType()Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Application:Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method

.method public shouldGetProviderActivities()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method
