.class public Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;
.super Ljava/lang/Object;
.source "FeedPostPreviewDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LinkData"
.end annotation


# instance fields
.field public final embedUrl:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "EmbedUrl"
    .end annotation
.end field

.field public final youTubeId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "YouTubeId"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "embedUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "youTubeId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;->embedUrl:Ljava/lang/String;

    .line 106
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkData;->youTubeId:Ljava/lang/String;

    .line 107
    return-void
.end method
