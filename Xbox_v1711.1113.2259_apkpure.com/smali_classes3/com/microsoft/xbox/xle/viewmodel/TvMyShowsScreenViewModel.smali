.class public Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "TvMyShowsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;
    }
.end annotation


# instance fields
.field private isLoading:Z

.field private loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;

.field private moviesItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;",
            ">;"
        }
    .end annotation
.end field

.field private tvItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 15
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 24
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvMyShowsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 25
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->onLoadDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->isLoading:Z

    return p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->loadMockTVData()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->loadMockMoviesData()V

    return-void
.end method

.method private loadMockMoviesData()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "http://placehold.it/300x400"

    .line 63
    .local v0, "mockImageURI":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    return-void
.end method

.method private loadMockTVData()V
    .locals 3

    .prologue
    .line 37
    const-string v0, "http://placehold.it/300x400"

    .line 39
    .local v0, "mockImageURI":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    .line 40
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method private onLoadDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->isLoading:Z

    .line 126
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 140
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->updateAdapter()V

    .line 141
    return-void

    .line 130
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 134
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    if-nez v0, :cond_0

    .line 135
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getMockMoviesData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->moviesItems:Ljava/util/List;

    return-object v0
.end method

.method public getMockTvData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$TempMockTvMyShowsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->isLoading:Z

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;->cancel()V

    .line 148
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;->load(Z)V

    .line 150
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 86
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTvMyShowsAdapter(Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 87
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->tvItems:Ljava/util/List;

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel;->loadDataTask:Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TvMyShowsScreenViewModel$LoadMockDataAsyncTask;->cancel()V

    .line 121
    :cond_0
    return-void
.end method
