.class Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .prologue
    .line 361
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 365
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 366
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 371
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 373
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 13
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 423
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 424
    .local v12, "viewRect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildCount()I

    move-result v7

    .line 425
    .local v7, "childCount":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v7, :cond_0

    .line 426
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 427
    .local v2, "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 428
    .local v9, "left":I
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v10

    .line 429
    .local v10, "right":I
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v11

    .line 430
    .local v11, "top":I
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v6

    .line 431
    .local v6, "bottom":I
    invoke-virtual {v12, v9, v11, v10, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 432
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v12, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$700(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$700(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v8

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iget-object v4, v4, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v8

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    .line 440
    .end local v2    # "child":Landroid/view/View;
    .end local v6    # "bottom":I
    .end local v9    # "left":I
    .end local v10    # "right":I
    .end local v11    # "top":I
    :cond_0
    return-void

    .line 425
    .restart local v2    # "child":Landroid/view/View;
    .restart local v6    # "bottom":I
    .restart local v9    # "left":I
    .restart local v10    # "right":I
    .restart local v11    # "top":I
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v4, 0x1

    .line 378
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 380
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 382
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    monitor-enter v1

    .line 383
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iget v2, v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    float-to-int v3, p3

    add-int/2addr v2, v3

    iput v2, v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    .line 384
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->requestLayout()V

    .line 389
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$200(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$400(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I

    move-result v2

    invoke-interface {v0, v1, v2, p3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;->onScroll(IIF)V

    .line 394
    :cond_0
    return v4

    .line 384
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 399
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    .line 400
    .local v11, "viewRect":Landroid/graphics/Rect;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildCount()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 402
    .local v2, "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v8

    .line 403
    .local v8, "left":I
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v9

    .line 404
    .local v9, "right":I
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v10

    .line 405
    .local v10, "top":I
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v6

    .line 406
    .local v6, "bottom":I
    invoke-virtual {v11, v8, v10, v9, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 407
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 408
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$500(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$500(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iget-object v4, v4, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v7

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$600(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 412
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$600(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    iget-object v4, v4, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;->this$0:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->access$300(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v7

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 418
    .end local v2    # "child":Landroid/view/View;
    .end local v6    # "bottom":I
    .end local v8    # "left":I
    .end local v9    # "right":I
    .end local v10    # "top":I
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 400
    .restart local v2    # "child":Landroid/view/View;
    .restart local v6    # "bottom":I
    .restart local v8    # "left":I
    .restart local v9    # "right":I
    .restart local v10    # "top":I
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0
.end method
