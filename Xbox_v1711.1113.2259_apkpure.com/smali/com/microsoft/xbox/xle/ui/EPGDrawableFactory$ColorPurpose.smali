.class public final enum Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;
.super Ljava/lang/Enum;
.source "EPGDrawableFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ColorPurpose"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

.field public static final enum CellHighlight:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

.field public static final enum CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

.field public static final enum CellProgress:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

.field public static final enum ChannelHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

.field public static final enum ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

.field public static final enum InFocus:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

.field public static final enum InFocusText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

.field public static final enum TopHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

.field public static final enum TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 207
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    const-string v1, "TopHeading"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 208
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    const-string v1, "TopHeadingText"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 209
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    const-string v1, "ChannelHeading"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 210
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    const-string v1, "ChannelHeadingText"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 211
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    const-string v1, "CellHighlight"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlight:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 212
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    const-string v1, "CellHighlightText"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 215
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    const-string v1, "InFocus"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->InFocus:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 216
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    const-string v1, "InFocusText"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->InFocusText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 217
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    const-string v1, "CellProgress"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellProgress:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 206
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->ChannelHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlight:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->InFocus:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->InFocusText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellProgress:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->$VALUES:[Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 206
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 206
    const-class v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->$VALUES:[Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    return-object v0
.end method
