.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker$1;
.super Ljava/lang/Object;
.source "UTCComparePeoplePicker.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->trackCompareAction(Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$titleId:J

.field final synthetic val$xuid:Ljava/lang/String;


# direct methods
.method constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker$1;->val$titleId:J

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker$1;->val$xuid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    .prologue
    .line 36
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker$1;->val$titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->setGameTitleId(J)V

    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 38
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "TitleId"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->access$000()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 39
    const-string v1, "TargetXuid"

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker$1;->val$xuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 41
    const-string v1, "Compare People Picker - Compare"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 42
    return-void
.end method
