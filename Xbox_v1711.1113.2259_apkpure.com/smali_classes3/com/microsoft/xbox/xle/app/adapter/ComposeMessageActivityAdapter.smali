.class public Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ComposeMessageActivityAdapter.java"


# static fields
.field public static final CHARACTERLIMIT:I

.field private static final MAX_THUMBS_PHONE:I = 0x5

.field private static final STATE_NO_RECIPIENTS:I


# instance fields
.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private counterFrame:Landroid/widget/FrameLayout;

.field private messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

.field private recipientCountDrawable:Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

.field private recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private recipientsHolderView:Landroid/view/View;

.field private sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private sendButtonView:Landroid/view/View;

.field private shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private shareButtonView:Landroid/widget/FrameLayout;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0b0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->CHARACTERLIMIT:I

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;)V
    .locals 6
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 63
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 64
    const v1, 0x7f0e0414

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->screenBody:Landroid/view/View;

    .line 66
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    .line 68
    const v1, 0x7f0e0418

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    .line 71
    const v1, 0x7f0e0415

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    .line 72
    .local v0, "parent":Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setContainer(Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;)V

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setCharacterCountVisibility(I)V

    .line 74
    const v1, 0x7f0e0411

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->counterFrame:Landroid/widget/FrameLayout;

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->counterFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 76
    const v1, 0x7f0e041a

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 77
    const v1, 0x7f0e0417

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->recipientsHolderView:Landroid/view/View;

    .line 79
    new-instance v1, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->recipientCountDrawable:Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->recipientCountDrawable:Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->setMaxRecipientsDisplayedOnScreenCount(I)V

    .line 82
    const v1, 0x7f0e022a

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 83
    const v1, 0x7f0e040f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->sendButtonView:Landroid/view/View;

    .line 84
    const v1, 0x7f0e0413

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 86
    const v1, 0x7f0e040e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 87
    const v1, 0x7f0e040c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButtonView:Landroid/widget/FrameLayout;

    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButtonView:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->sendButtonView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->sendButtonView:Landroid/view/View;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setSendButtonEnabled(Z)V

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButtonView:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButtonView:Landroid/widget/FrameLayout;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setShareButtonEnabled(Z)V

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_1

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$3;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->recipientsHolderView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->recipientsHolderView:Landroid/view/View;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$4;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$4;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->recipientsHolderView:Landroid/view/View;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    if-eqz v1, :cond_3

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getMessageBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setText(Ljava/lang/String;)V

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 140
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setSendButtonEnabled(Z)V

    .line 141
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setShareButtonEnabled(Z)V

    .line 148
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter$5;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 186
    :cond_3
    return-void

    .line 143
    :cond_4
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setSendButtonEnabled(Z)V

    .line 144
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setShareButtonEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->dismissKeyboard()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->dismissKeyboard()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->dismissKeyboard()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setSendButtonEnabled(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setShareButtonEnabled(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->updateCounterVisibility(ZI)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->counterFrame:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private setSendButtonEnabled(Z)V
    .locals 1
    .param p1, "enableSendButton"    # Z

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 267
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->sendButtonView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 269
    :cond_0
    return-void
.end method

.method private setShareButtonEnabled(Z)V
    .locals 1
    .param p1, "enableShareButton"    # Z

    .prologue
    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->shareButtonView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 276
    :cond_0
    return-void
.end method

.method private updateCounterVisibility(ZI)V
    .locals 4
    .param p1, "visible"    # Z
    .param p2, "charCount"    # I

    .prologue
    .line 189
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f04000a

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 190
    .local v0, "bottom_down":Landroid/view/animation/Animation;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f04000b

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 192
    .local v1, "bottom_up":Landroid/view/animation/Animation;
    if-nez p1, :cond_0

    sget v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->CHARACTERLIMIT:I

    if-ne p2, v2, :cond_0

    .line 193
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->counterFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 195
    :cond_0
    if-eqz p1, :cond_1

    sget v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->CHARACTERLIMIT:I

    if-ne p2, v2, :cond_1

    .line 196
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->counterFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 198
    :cond_1
    return-void
.end method


# virtual methods
.method public onAnimateInCompleted()V
    .locals 2

    .prologue
    .line 218
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onAnimateInCompleted()V

    .line 219
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getShouldAutoShowKeyboard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getEditTextView()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getEditTextView()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->showKeyboard(Landroid/view/View;I)V

    .line 224
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 202
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onPause()V

    .line 203
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->dismissKeyboard()V

    .line 204
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 208
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getMessageBody()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setText(Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setSelection(I)V

    .line 214
    :cond_0
    return-void
.end method

.method public updateMessageBody()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->setMessageBody(Ljava/lang/String;)V

    .line 262
    return-void
.end method

.method public updateViewOverride()V
    .locals 7

    .prologue
    .line 235
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getRecipients()Ljava/util/ArrayList;

    move-result-object v2

    .line 236
    .local v2, "recipients":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    if-eqz v2, :cond_2

    .line 237
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 238
    .local v1, "recipientCount":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->recipientCountDrawable:Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->setRecipientCount(I)V

    .line 239
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070668

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 240
    .local v3, "recipientsHolderString":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 241
    add-int/lit8 v4, v1, -0x1

    if-ne v0, v4, :cond_0

    .line 243
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamerName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 240
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamerName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 249
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    .end local v0    # "i":I
    .end local v1    # "recipientCount":I
    .end local v3    # "recipientsHolderString":Ljava/lang/String;
    :cond_2
    const v5, 0x7f0e0413

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getIsRecipientNonEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_2
    invoke-virtual {p0, v5, v4}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setAppBarButtonEnabled(IZ)V

    .line 257
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->isBlockingBusy()Z

    move-result v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageActivityViewModel;->getBlockingStatusText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageActivityAdapter;->setBlocking(ZLjava/lang/String;)V

    .line 258
    return-void

    .line 255
    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method
