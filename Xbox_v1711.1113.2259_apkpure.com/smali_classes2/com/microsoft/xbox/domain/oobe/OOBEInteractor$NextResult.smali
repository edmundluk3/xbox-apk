.class public final enum Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;
.super Ljava/lang/Enum;
.source "OOBEInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NextResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;",
        ">;",
        "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 386
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;->INSTANCE:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    .line 385
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    sget-object v1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;->INSTANCE:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;->$VALUES:[Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    .line 388
    const-class v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 385
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 385
    const-class v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;
    .locals 1

    .prologue
    .line 385
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;->$VALUES:[Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;

    return-object v0
.end method


# virtual methods
.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 392
    sget-object v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$NextResult;->TAG:Ljava/lang/String;

    return-object v0
.end method
