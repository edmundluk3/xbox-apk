.class Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$1;
.super Ljava/lang/Object;
.source "EditRealNameDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->editUserRealName(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->access$400(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 53
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackSaveName()V

    .line 54
    return-void
.end method
