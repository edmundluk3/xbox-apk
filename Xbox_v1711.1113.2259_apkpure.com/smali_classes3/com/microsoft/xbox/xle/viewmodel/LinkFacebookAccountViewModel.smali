.class public Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "LinkFacebookAccountViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private initAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 28
    return-void
.end method

.method private createAdapter()V
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 194
    :cond_0
    return-void
.end method


# virtual methods
.method public hideLinkButtonInSuggestions()V
    .locals 1

    .prologue
    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->notInterested()V

    .line 126
    return-void
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 59
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->isLoading()Z

    move-result v0

    .line 60
    .local v0, "isBusy":Z
    return v0
.end method

.method public linkUnlink()V
    .locals 2

    .prologue
    .line 99
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookManagerReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->TAG:Ljava/lang/String;

    const-string v1, "FacebookManager not ready yet"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->setShouldNavigateToSuggestionList(Z)V

    .line 107
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$friendfinder$OptInStatus:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->nextActionRequired()Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 109
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->TAG:Ljava/lang/String;

    const-string v1, "linkUnlink - optedin"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->login()V

    goto :goto_0

    .line 114
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->TAG:Ljava/lang/String;

    const-string v1, "linkUnlink - TokenRenewalRequired"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFriendFinderConfirmDialog(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 65
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookManagerReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 67
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->loadAsync(Z)V

    .line 76
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->initAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->initAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;->cancel()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->initAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;

    .line 73
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;-><init>(Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->initAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->initAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel$FacebookManagerInitAsync;->load(Z)V

    goto :goto_0
.end method

.method public nextActionRequired()Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    .locals 4

    .prologue
    .line 135
    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 136
    .local v1, "nextAction":Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookManagerReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookFriendFinderState()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v0

    .line 138
    .local v0, "friendsFinderStateResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v2, v3, :cond_0

    .line 140
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountTokenStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;->TokenRenewalRequired:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;

    if-ne v2, v3, :cond_1

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->TokenRenewalRequired:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 144
    .end local v0    # "friendsFinderStateResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :cond_0
    :goto_0
    return-object v1

    .line 140
    .restart local v0    # "friendsFinderStateResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 32
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookManagerReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v0

    if-nez v0, :cond_1

    .line 33
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->TAG:Ljava/lang/String;

    const-string v1, "FacebookManager not ready yet"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookCallbackManager()Lcom/facebook/CallbackManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookCallbackManager()Lcom/facebook/CallbackManager;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/CallbackManager;->onActivityResult(IILandroid/content/Intent;)Z

    goto :goto_0
.end method

.method protected onFacebookManagerInitCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 80
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne p1, v0, :cond_0

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 82
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->loadAsync(Z)V

    .line 85
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->createAdapter()V

    .line 50
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->createAdapter()V

    .line 45
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 55
    return-void
.end method

.method public repairLink()V
    .locals 1

    .prologue
    .line 121
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginToRepair()V

    .line 122
    return-void
.end method

.method public showLinkFacebookAccountButtonInSettings()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 154
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToAddFriend()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 155
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookManagerReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookFriendFinderState()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v0

    .line 157
    .local v0, "friendsFinderStateResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    if-eqz v0, :cond_1

    .line 158
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v1

    .line 159
    .local v1, "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v1, v3, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 164
    .end local v0    # "friendsFinderStateResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .end local v1    # "optedInStatus":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;
    :cond_1
    return v2
.end method

.method public showLinkFacebookAccountButtonInSuggestions()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 168
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToAddFriend()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 169
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookManagerReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 170
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookFriendFinderState()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v0

    .line 171
    .local v0, "friendsFinderStateResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    if-eqz v0, :cond_1

    .line 172
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-eq v2, v3, :cond_0

    .line 173
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v2, v3, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 176
    .end local v0    # "friendsFinderStateResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :cond_1
    return v1
.end method

.method public showNotInterestedButton()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 180
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToAddFriend()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookManagerReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 182
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookFriendFinderState()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    move-result-object v0

    .line 183
    .local v0, "friendsFinderStateResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 187
    .end local v0    # "friendsFinderStateResult":Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    :cond_0
    return v1
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->FriendFinder:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Friend finder state changed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LinkFacebookAccountViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 96
    :cond_0
    return-void
.end method
