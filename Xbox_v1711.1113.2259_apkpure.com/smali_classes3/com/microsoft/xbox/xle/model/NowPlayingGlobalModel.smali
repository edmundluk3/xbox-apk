.class public Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "NowPlayingGlobalModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;


# instance fields
.field protected nowPlayingModels:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;",
            "Lcom/microsoft/xbox/xle/model/NowPlayingModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->TAG:Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->instance:Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 38
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    .line 42
    return-void
.end method

.method private createModel(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Z)V
    .locals 2
    .param p1, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .param p2, "hasConsoleFocus"    # Z

    .prologue
    .line 202
    new-instance v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-direct {v0, p1, p2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;-><init>(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Z)V

    .line 203
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->onResume()V

    .line 204
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 205
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->instance:Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/XLEObserver",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "observer":Lcom/microsoft/xbox/toolkit/XLEObserver;, "Lcom/microsoft/xbox/toolkit/XLEObserver<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "observed by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/XLEObservable;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getActiveTitleLocation(I)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 4
    .param p1, "titleId"    # I

    .prologue
    .line 126
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 128
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v2, v3, :cond_0

    .line 129
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v1

    .line 133
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    goto :goto_0
.end method

.method public getModel(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .locals 1
    .param p1, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    return-object v0
.end method

.method public getModels()[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .locals 8

    .prologue
    .line 179
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 180
    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    sget-object v6, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v5, v6}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 181
    .local v0, "hasLocationClosed":Z
    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->size()I

    move-result v6

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    :goto_0
    sub-int v5, v6, v5

    new-array v4, v5, [Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 182
    .local v4, "result":[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    const/4 v1, 0x0

    .line 183
    .local v1, "i":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 184
    .local v3, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v6, v7, :cond_0

    .line 185
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    aput-object v3, v4, v1

    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_1

    .line 181
    .end local v1    # "i":I
    .end local v3    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .end local v4    # "result":[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 188
    .restart local v1    # "i":I
    .restart local v4    # "result":[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_2
    return-object v4
.end method

.method public getNowPlayingActiveTitleLocation(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 4
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 137
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 138
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 139
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentNowPlayingIdentifier()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v2, v3, :cond_0

    .line 140
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v1

    .line 144
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    goto :goto_0
.end method

.method public isAppRunning(Ljava/lang/String;)Z
    .locals 4
    .param p1, "canonicalId"    # Ljava/lang/String;

    .prologue
    .line 164
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 165
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 166
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getAppCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 167
    const/4 v1, 0x1

    .line 170
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSnappedModeEnabled()Z
    .locals 4

    .prologue
    .line 192
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 193
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 194
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Snapped:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v2, v3, :cond_0

    .line 195
    const/4 v1, 0x1

    .line 198
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isTitleRunning(J)Z
    .locals 5
    .param p1, "titleId"    # J

    .prologue
    .line 148
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 149
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 150
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 151
    const/4 v1, 0x1

    .line 154
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 3
    .param p1, "refreshSessionData"    # Z

    .prologue
    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 51
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->load(Z)V

    goto :goto_0

    .line 53
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 68
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->TAG:Ljava/lang/String;

    const-string v2, "onPause"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 71
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->onPause()V

    goto :goto_0

    .line 73
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->removeTitleChangedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;)V

    .line 74
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->removeTitleChangeCompletedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;)V

    .line 75
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 56
    sget-object v1, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->TAG:Ljava/lang/String;

    const-string v2, "onResume"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addFirstTitleChangedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;)V

    .line 60
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addTitleChangeCompletedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;)V

    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 63
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->onResume()V

    goto :goto_0

    .line 65
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_0
    return-void
.end method

.method public onTitleChangeCompleted(Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 8
    .param p1, "newTitleState"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    .line 96
    const/4 v1, 0x0

    .line 97
    .local v1, "outOfDateModel":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 98
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v3

    iget v4, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v3

    iget-object v4, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v3, v4, :cond_0

    .line 99
    move-object v1, v0

    .line 103
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_1
    if-eqz v1, :cond_2

    .line 104
    const-string v2, "XBoxOneCompanionSession"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "removing %s(%d) model"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->onPause()V

    .line 106
    iget-object v2, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    :cond_2
    return-void
.end method

.method public onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 3
    .param p1, "oldTitleState"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    .param p2, "newTitleState"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->nowPlayingModels:Ljava/util/Hashtable;

    iget-object v2, p2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 86
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-nez v0, :cond_0

    .line 87
    iget-object v1, p2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    iget-boolean v2, p2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->hasFocus:Z

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->createModel(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Z)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-boolean v1, p2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->hasFocus:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->setHasConsoleFocus(Z)V

    goto :goto_0
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 113
    return-void
.end method
