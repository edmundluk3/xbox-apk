.class Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$1;
.super Ljava/lang/Object;
.source "SearchScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClear()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->onSearchBarClear()V

    .line 87
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "newText"    # Ljava/lang/CharSequence;

    .prologue
    .line 76
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->setSearchTag(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->loadAutoSuggestAsync(Ljava/lang/String;)V

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->onSearchBarClear()V

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "query"    # Ljava/lang/CharSequence;

    .prologue
    .line 68
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Search Return"

    const-string v2, "Search"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SearchScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/SearchScreenViewModel;->navigateToSearchResults(Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method
