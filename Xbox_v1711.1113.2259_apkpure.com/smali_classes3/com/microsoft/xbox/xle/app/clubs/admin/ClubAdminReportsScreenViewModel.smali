.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubAdminReportsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$BannedUserCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;",
        "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOAD_INCREMENT:I = 0x32

.field private static final MAX_BATCH_MESSAGES:I = 0x32

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final allReportedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;"
        }
    .end annotation
.end field

.field private final chatService:Lcom/microsoft/xbox/service/chat/IChatService;

.field private final commentsService:Lcom/microsoft/xbox/service/comments/ICommentsService;

.field private final isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isViewerOwner:Z

.field private final listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ListenerManager",
            "<",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final loadedReportedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
            ">;"
        }
    .end annotation
.end field

.field private meXuid:J

.field private final moderationService:Lcom/microsoft/xbox/service/clubs/IClubModerationService;

.field private final moderatorXuids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private ownerXuid:J

.field private final personSummaries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

.field private final slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 122
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 123
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminReportsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    .line 126
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->personSummaries:Ljava/util/Map;

    .line 127
    new-instance v0, Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ListenerManager;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    .line 128
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->moderatorXuids:Ljava/util/List;

    .line 130
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->meXuid:J

    .line 132
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    .line 133
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    .line 134
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCommentService()Lcom/microsoft/xbox/service/comments/ICommentsService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->commentsService:Lcom/microsoft/xbox/service/comments/ICommentsService;

    .line 135
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getChatService()Lcom/microsoft/xbox/service/chat/IChatService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->chatService:Lcom/microsoft/xbox/service/chat/IChatService;

    .line 136
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubModerationService()Lcom/microsoft/xbox/service/clubs/IClubModerationService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->moderationService:Lcom/microsoft/xbox/service/clubs/IClubModerationService;

    .line 137
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->onItemDeletedFailed(Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->onReportDeletedFailed(Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$lambda$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->onAllReportsDownloaded(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->onReportsLoaded(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$4(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/util/Map;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->downloadActivityItems(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$lambda$5(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/util/Map;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->downloadCommentItems(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$lambda$6(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/util/Map;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->downloadChatItems(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$lambda$7(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getMessageId(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private deleteContent(Ljava/lang/String;)Lio/reactivex/Completable;
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 176
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/lang/String;)Lio/reactivex/functions/Action;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method private deleteReport(JLcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;)Lio/reactivex/Completable;
    .locals 1
    .param p1, "clubId"    # J
    .param p3, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "reason"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    .prologue
    .line 202
    invoke-static {p0, p3, p4, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;J)Lio/reactivex/functions/Action;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method private deleteReport(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 189
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;->Deleted:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;

    invoke-direct {p0, v2, v3, p1, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->deleteReport(JLcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;)Lio/reactivex/Completable;

    move-result-object v1

    .line 190
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    .line 191
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Lio/reactivex/functions/Action;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 192
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 188
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 196
    return-void
.end method

.method private downloadActivityItems(Ljava/util/Map;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 503
    .local p1, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 504
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 537
    :cond_0
    return-object v4

    .line 507
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v6

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 509
    .local v4, "reportFeedListItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;>;"
    const/4 v1, 0x0

    .line 512
    .local v1, "failure":Z
    :try_start_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v6, v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getBatchFeedItems(Ljava/util/List;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;

    move-result-object v5

    .line 514
    .local v5, "response":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    if-eqz v5, :cond_3

    .line 515
    iget-object v6, v5, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;->activityItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 516
    .local v3, "profileRecentItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    new-instance v8, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;

    iget-object v6, v3, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->feedItemId:Ljava/lang/String;

    .line 518
    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-direct {v8, v6, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 516
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 524
    .end local v3    # "profileRecentItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .end local v5    # "response":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :catch_0
    move-exception v0

    .line 525
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const/4 v1, 0x1

    .line 528
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_2
    :goto_1
    if-eqz v1, :cond_0

    .line 529
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 530
    .local v2, "locator":Ljava/lang/String;
    new-instance v8, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;

    .line 532
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    const/4 v9, 0x0

    invoke-direct {v8, v6, v9}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 530
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 522
    .end local v2    # "locator":Ljava/lang/String;
    .restart local v5    # "response":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentsResult;
    :cond_3
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private downloadChatItems(Ljava/util/Map;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    const/4 v12, 0x0

    .line 596
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 597
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 637
    :cond_0
    return-object v5

    .line 600
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v7

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 602
    .local v5, "reportChatListItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;>;"
    const/4 v1, 0x0

    .line 605
    .local v1, "failure":Z
    :try_start_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->chatService:Lcom/microsoft/xbox/service/chat/IChatService;

    sget-object v8, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getClubId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v7, v8, v9, v10}, Lcom/microsoft/xbox/service/chat/IChatService;->getMessages(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 607
    .local v6, "response":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    if-nez v6, :cond_3

    .line 608
    const/4 v1, 0x1

    .line 628
    .end local v6    # "response":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    :cond_2
    :goto_0
    if-eqz v1, :cond_0

    .line 629
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 630
    .local v3, "locator":Ljava/lang/String;
    new-instance v9, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;

    .line 632
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-direct {v9, v7, v12}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;)V

    .line 630
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 610
    .end local v3    # "locator":Ljava/lang/String;
    .restart local v6    # "response":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    :cond_3
    :try_start_1
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->getMessages()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    .line 611
    .local v4, "message":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
    new-instance v9, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;

    iget-wide v10, v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageId:J

    .line 613
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-direct {v9, v7, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;)V

    .line 611
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 624
    .end local v4    # "message":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
    .end local v6    # "response":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    :catch_0
    move-exception v0

    .line 625
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const/4 v1, 0x1

    goto :goto_0

    .line 617
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v6    # "response":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    :cond_4
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->getNotFound()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 618
    .local v2, "id":Ljava/lang/String;
    new-instance v9, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;

    .line 620
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    const/4 v10, 0x0

    invoke-direct {v9, v7, v10}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportChatListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;)V

    .line 618
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method private downloadCommentItems(Ljava/util/Map;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541
    .local p1, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 542
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 592
    :cond_0
    return-object v8

    .line 545
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v10

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 546
    .local v8, "reportCommentListItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;>;"
    const/4 v5, 0x0

    .line 548
    .local v5, "failure":Z
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 551
    .local v7, "locators":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->commentsService:Lcom/microsoft/xbox/service/comments/ICommentsService;

    invoke-interface {v10, v7}, Lcom/microsoft/xbox/service/comments/ICommentsService;->getActionBatchResponse(Ljava/util/List;)Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;

    move-result-object v9

    .line 553
    .local v9, "response":Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;
    if-eqz v9, :cond_5

    .line 554
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 556
    .local v3, "failedCommentIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->getActions()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;

    .line 557
    .local v0, "commentAction":Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;
    if-eqz v0, :cond_2

    .line 558
    iget-object v10, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    iget-object v1, v10, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;->path:Ljava/lang/String;

    .line 559
    .local v1, "commentId":Ljava/lang/String;
    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 561
    new-instance v12, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;

    .line 563
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    iget-object v13, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;->comment:Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;

    invoke-direct {v12, v10, v13}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;)V

    .line 561
    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 579
    .end local v0    # "commentAction":Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;
    .end local v1    # "commentId":Ljava/lang/String;
    .end local v3    # "failedCommentIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "response":Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;
    :catch_0
    move-exception v2

    .line 580
    .local v2, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const/4 v5, 0x1

    .line 583
    .end local v2    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_3
    :goto_1
    if-eqz v5, :cond_0

    .line 584
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 585
    .local v6, "locator":Ljava/lang/String;
    new-instance v12, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;

    .line 587
    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    const/4 v13, 0x0

    invoke-direct {v12, v10, v13}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;)V

    .line 585
    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 568
    .end local v6    # "locator":Ljava/lang/String;
    .restart local v3    # "failedCommentIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v9    # "response":Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;
    :cond_4
    :try_start_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 569
    .local v4, "failedId":Ljava/lang/String;
    new-instance v12, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;

    .line 571
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    const/4 v13, 0x0

    invoke-direct {v12, v10, v13}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;-><init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$Comment;)V

    .line 569
    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 576
    .end local v3    # "failedCommentIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "failedId":Ljava/lang/String;
    :cond_5
    const/4 v5, 0x1

    goto :goto_1
.end method

.method private downloadReportedItems(Ljava/util/List;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;)",
            "Lio/reactivex/Maybe",
            "<",
            "Ljava/util/List;",
            ">;"
        }
    .end annotation

    .prologue
    .line 439
    .local p1, "itemsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    invoke-static {p1}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$13;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 440
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 441
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->groupBy(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 442
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Ljava/util/List;

    .line 466
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->cast(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$16;->lambdaFactory$()Lio/reactivex/functions/BiFunction;

    move-result-object v1

    .line 469
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->reduce(Lio/reactivex/functions/BiFunction;)Lio/reactivex/Maybe;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 475
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 439
    return-object v0
.end method

.method private getLastSuitableReportForItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    .locals 8
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 679
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getReports()Ljava/util/List;

    move-result-object v0

    .line 680
    .local v0, "allReports":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;>;"
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 681
    const/4 v1, 0x0

    .line 683
    .local v1, "lastReport":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    .line 684
    .local v2, "report":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->isViewerOwner:Z

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->ownerXuid:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_4

    .line 685
    move-object v1, v2

    .line 691
    :cond_1
    :goto_0
    if-eqz v1, :cond_0

    .line 697
    .end local v2    # "report":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    :cond_2
    if-nez v1, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 698
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "lastReport":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    .line 701
    .restart local v1    # "lastReport":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    :cond_3
    return-object v1

    .line 686
    .restart local v2    # "report":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->moderatorXuids:Ljava/util/List;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 687
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->meXuid:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 688
    move-object v1, v2

    goto :goto_0
.end method

.method private getMessageId(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)Ljava/lang/String;
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    .prologue
    .line 641
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentId()Ljava/lang/String;

    move-result-object v0

    .line 642
    .local v0, "contentId":Ljava/lang/String;
    const-string v2, "/"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 644
    .local v1, "sections":[Ljava/lang/String;
    array-length v2, v1

    if-lez v2, :cond_0

    .line 645
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v1, v2

    .line 648
    :goto_0
    return-object v2

    .line 647
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to extract message id from: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 648
    const-string v2, ""

    goto :goto_0
.end method

.method static synthetic lambda$banUser$4(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;JLjava/lang/String;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "xuid"    # J
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "gamertag"    # Ljava/lang/String;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$BannedUserCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p3, v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$BannedUserCallback;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/lang/String;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$1;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->ban(JLcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    return-void
.end method

.method static synthetic lambda$deleteContent$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/lang/String;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "contentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 178
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->slsServiceManager:Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->genericDeleteWithUri(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :cond_0
    return-void

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v2

    const-wide/16 v4, 0x15

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 181
    throw v0
.end method

.method static synthetic lambda$deleteItem$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->onItemDeletedSuccess(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method

.method static synthetic lambda$deleteReport$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 193
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->onReportDeletedSuccess(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method

.method static synthetic lambda$deleteReport$3(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;J)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .param p2, "reason"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;
    .param p3, "clubId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 203
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getReportId()J

    move-result-wide v2

    invoke-static {v2, v3, p2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;->with(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportReason;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;

    move-result-object v0

    .line 205
    .local v0, "deleteRequest":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubDeleteReportRequest;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->moderationService:Lcom/microsoft/xbox/service/clubs/IClubModerationService;

    .line 207
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;->with(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;

    move-result-object v2

    .line 205
    invoke-interface {v1, p3, p4, v2}, Lcom/microsoft/xbox/service/clubs/IClubModerationService;->batchDeleteItemReports(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubBatchDeleteReportRequest;)Z

    .line 208
    return-void
.end method

.method static synthetic lambda$downloadReportedItems$12(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lio/reactivex/observables/GroupedObservable;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "group"    # Lio/reactivex/observables/GroupedObservable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v2, 0x32

    .line 443
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubModerationDataTypes$ClubReportContentType:[I

    invoke-virtual {p1}, Lio/reactivex/observables/GroupedObservable;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 463
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    :goto_0
    return-object v0

    .line 446
    :pswitch_0
    invoke-virtual {p1, v2}, Lio/reactivex/observables/GroupedObservable;->buffer(I)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$20;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 447
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$21;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 448
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0

    .line 451
    :pswitch_1
    const/16 v0, 0x64

    .line 452
    invoke-virtual {p1, v0}, Lio/reactivex/observables/GroupedObservable;->buffer(I)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$22;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 453
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$23;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 454
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0

    .line 458
    :pswitch_2
    invoke-virtual {p1, v2}, Lio/reactivex/observables/GroupedObservable;->buffer(I)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$24;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 459
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$25;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 460
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0

    .line 443
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic lambda$downloadReportedItems$13(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "acc"    # Ljava/util/List;
    .param p1, "other"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 471
    invoke-interface {p0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 472
    return-object p0
.end method

.method static synthetic lambda$downloadReportedItems$14(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "items"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 477
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->populateLastReportsAndGetXuids(Ljava/util/List;)Ljava/util/Set;

    move-result-object v1

    .line 479
    .local v1, "xuidsToDownload":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 480
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->personSummaryManager:Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/network/managers/PeopleHubPersonSummaryManager;->immediateQuery(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 481
    .local v0, "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->personSummaries:Ljava/util/Map;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 486
    .end local v0    # "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->populateReporterAndCreator(Ljava/util/List;)V

    .line 488
    return-object p1
.end method

.method static synthetic lambda$downloadReportedItems$8(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)Z
    .locals 1
    .param p0, "item"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentType()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$loadAllReports$6(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Ljava/util/List;
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 363
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->moderationService:Lcom/microsoft/xbox/service/clubs/IClubModerationService;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/IClubModerationService;->getReportedItems(J)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;

    move-result-object v0

    .line 364
    .local v0, "response":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItemsResponse;->reportedItems()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic lambda$loadMoreItemsAsync$7(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 430
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to download additional reports"

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 431
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->onReportsLoaded(Ljava/util/List;)V

    .line 432
    return-void
.end method

.method static synthetic lambda$null$10(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/util/List;)Ljava/util/Map;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "items"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 453
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$27;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->toLocatorMap(Ljava/util/List;Lio/reactivex/functions/Function;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$11(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/util/List;)Ljava/util/Map;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "items"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 459
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$26;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->toLocatorMap(Ljava/util/List;Lio/reactivex/functions/Function;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/util/List;)Ljava/util/Map;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "items"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 447
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$28;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->toLocatorMap(Ljava/util/List;Lio/reactivex/functions/Function;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$onAllReportsDownloaded$15(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)I
    .locals 2
    .param p0, "lhs"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    .param p1, "rhs"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    .prologue
    .line 747
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->lastReported()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->lastReported()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    return v0
.end method

.method static synthetic lambda$onClubModelChanged$5(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 337
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to download reports"

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 338
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->onAllReportsDownloaded(Ljava/util/List;)V

    .line 339
    return-void
.end method

.method static synthetic lambda$onReportsLoaded$16(Ljava/util/List;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0
    .param p0, "nonNullReports"    # Ljava/util/List;
    .param p1, "collectionAction"    # Lcom/microsoft/xbox/toolkit/generics/Action;

    .prologue
    .line 759
    invoke-interface {p1, p0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method private loadAllReports()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 362
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized onAllReportsDownloaded(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 738
    .local p1, "allReports":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    monitor-enter p0

    if-nez p1, :cond_0

    .line 739
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 740
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 752
    :goto_0
    monitor-exit p0

    return-void

    .line 741
    :cond_0
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 742
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 743
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 738
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 745
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 746
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 747
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$18;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 749
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 750
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadMoreItemsAsync()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private onItemDeletedFailed(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 383
    const v0, 0x7f070276

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->showError(I)V

    .line 384
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->updateAdapter()V

    .line 385
    return-void
.end method

.method private onItemDeletedSuccess(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 378
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->deleteReport(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    .line 379
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->removeItemAndUpdateIfEmpty(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    .line 380
    return-void
.end method

.method private onReportDeletedFailed(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 373
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->showError(I)V

    .line 374
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->updateAdapter()V

    .line 375
    return-void
.end method

.method private onReportDeletedSuccess(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 369
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->removeItemAndUpdateIfEmpty(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    .line 370
    return-void
.end method

.method private declared-synchronized onReportsLoaded(Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 755
    .local p1, "reports":Ljava/util/List;, "Ljava/util/List<+Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;>;"
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 757
    .local v0, "nonNullReports":Ljava/util/List;, "Ljava/util/List<+Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 758
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 759
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$19;->lambdaFactory$(Ljava/util/List;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ListenerManager;->run(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 760
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 762
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 764
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 765
    monitor-exit p0

    return-void

    .line 762
    :cond_0
    :try_start_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 755
    .end local v0    # "nonNullReports":Ljava/util/List;, "Ljava/util/List<+Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private populateLastReportsAndGetXuids(Ljava/util/List;)Ljava/util/Set;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;>;"
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 653
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 655
    .local v4, "xuidsToDownload":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .line 656
    .local v0, "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getLastSuitableReportForItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    move-result-object v1

    .line 658
    .local v1, "lastReport":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    if-eqz v1, :cond_0

    .line 659
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->setLastReport(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;)V

    .line 660
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->ownerXuid:J

    cmp-long v5, v10, v12

    if-nez v5, :cond_2

    move v5, v6

    :goto_0
    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->setIsLastReporterOwner(Z)V

    .line 661
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->moderatorXuids:Ljava/util/List;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->setIsLastReporterMod(Z)V

    .line 662
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->meXuid:J

    cmp-long v5, v10, v12

    if-nez v5, :cond_3

    move v5, v6

    :goto_1
    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->setIsLastReporterMe(Z)V

    .line 664
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getRelatedPeopleXuids()Ljava/util/Set;

    move-result-object v2

    .line 666
    .local v2, "relatedXuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 667
    .local v3, "xuid":Ljava/lang/String;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-interface {v9, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 668
    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .end local v2    # "relatedXuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "xuid":Ljava/lang/String;
    :cond_2
    move v5, v7

    .line 660
    goto :goto_0

    :cond_3
    move v5, v7

    .line 662
    goto :goto_1

    .line 674
    .end local v0    # "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .end local v1    # "lastReport":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    :cond_4
    return-object v4
.end method

.method private populateReporterAndCreator(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "reportItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;>;"
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 705
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .line 706
    .local v2, "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getReporterXuid()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 708
    .local v3, "reporter":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v3, :cond_1

    .line 709
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->setReporter(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 712
    :cond_1
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorXuid()Ljava/lang/Long;

    move-result-object v1

    .line 714
    .local v1, "creatorXuid":Ljava/lang/Long;
    if-eqz v1, :cond_0

    .line 715
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorXuid()Ljava/lang/Long;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 717
    .local v0, "creator":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v0, :cond_0

    .line 718
    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->setCreator(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    .line 719
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->ownerXuid:J

    cmp-long v4, v8, v10

    if-nez v4, :cond_2

    move v4, v5

    :goto_1
    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->setIsCreatorOwner(Z)V

    .line 720
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->moderatorXuids:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->setIsCreatorMod(Z)V

    .line 721
    iget-wide v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->meXuid:J

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v4, v8, v10

    if-nez v4, :cond_3

    move v4, v5

    :goto_2
    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->setIsCreatorMe(Z)V

    goto :goto_0

    :cond_2
    move v4, v6

    .line 719
    goto :goto_1

    :cond_3
    move v4, v6

    .line 721
    goto :goto_2

    .line 725
    .end local v0    # "creator":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v1    # "creatorXuid":Ljava/lang/Long;
    .end local v2    # "item":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    .end local v3    # "reporter":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_4
    return-void
.end method

.method private declared-synchronized removeItemAndUpdateIfEmpty(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 388
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 390
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 391
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 392
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->updateAdapter()V

    .line 395
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 396
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    .line 397
    .local v1, "report":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getContentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 398
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 388
    .end local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    .end local v1    # "report":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 401
    .restart local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private toLocatorMap(Ljava/util/List;Lio/reactivex/functions/Function;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;",
            "Lio/reactivex/functions/Function",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 493
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    .local p2, "getId":Lio/reactivex/functions/Function;, "Lio/reactivex/functions/Function<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 495
    .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    .line 496
    .local v0, "item":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    invoke-interface {p2, v0}, Lio/reactivex/functions/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 499
    .end local v0    # "item":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public addOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 729
    .local p1, "listener":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/Collection<+Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ListenerManager;->addListener(Ljava/lang/Object;)V

    .line 730
    return-void
.end method

.method public banUser(JLjava/lang/String;)V
    .locals 7
    .param p1, "xuid"    # J
    .param p3, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 220
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 222
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070225

    .line 223
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070224

    .line 224
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070614

    .line 225
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;JLjava/lang/String;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07060d

    .line 227
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 222
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 229
    return-void
.end method

.method public deleteItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 6
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 157
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorXuid()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getContentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorXuid()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminReportsDelete(JLjava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->hasValidContent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 163
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getContentId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->deleteContent(Ljava/lang/String;)Lio/reactivex/Completable;

    move-result-object v1

    .line 164
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    .line 165
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)Lio/reactivex/functions/Action;

    move-result-object v2

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v3

    .line 166
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 162
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 173
    :goto_0
    return-void

    .line 171
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->deleteReport(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    goto :goto_0
.end method

.method public getClubId()J
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized getLoadedReportedItems()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNumReports()I
    .locals 1

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasMoreItemsToLoad()Z
    .locals 2

    .prologue
    .line 405
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ignoreReport(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 6
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 212
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorXuid()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getContentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorXuid()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminReportsIgnore(JLjava/lang/String;Ljava/lang/String;)V

    .line 216
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->deleteReport(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    .line 217
    return-void
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 297
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 296
    :goto_0
    return v0

    .line 297
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isViewerOwner()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->isViewerOwner:Z

    return v0
.end method

.method public declared-synchronized loadMoreItemsAsync()V
    .locals 7

    .prologue
    .line 410
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->updateAdapter()V

    .line 413
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->hasMoreItemsToLoad()Z

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 415
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->hasMoreItemsToLoad()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 416
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 417
    .local v2, "startIndexToLoad":I
    add-int/lit8 v3, v2, 0x32

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 419
    .local v0, "endIndexToLoad":I
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-interface {v3, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 421
    .local v1, "reportsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 422
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->downloadReportedItems(Ljava/util/List;)Lio/reactivex/Maybe;

    move-result-object v4

    .line 423
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/reactivex/Maybe;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Maybe;

    move-result-object v4

    .line 424
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/reactivex/Maybe;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Maybe;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v6

    .line 425
    invoke-virtual {v4, v5, v6}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v4

    .line 421
    invoke-virtual {v3, v4}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    .end local v0    # "endIndexToLoad":I
    .end local v1    # "reportsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;>;"
    .end local v2    # "startIndexToLoad":I
    :cond_0
    monitor-exit p0

    return-void

    .line 410
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method protected loadOverride(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 282
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 284
    if-eqz p1, :cond_0

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 286
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadedReportedItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 292
    return-void
.end method

.method public navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;)V
    .locals 3
    .param p1, "commentItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 258
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 262
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;->getParentLocator()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    const/4 v2, 0x0

    .line 260
    invoke-static {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 265
    return-void
.end method

.method public navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;)V
    .locals 3
    .param p1, "feedListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 248
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 252
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;->getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    const/4 v2, 0x0

    .line 250
    invoke-static {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToActionsScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Z)V

    .line 255
    return-void
.end method

.method protected declared-synchronized onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 10
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v1, 0x0

    .line 302
    monitor-enter p0

    :try_start_0
    sget-object v6, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 358
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    monitor-exit p0

    return-void

    .line 306
    :pswitch_0
    :try_start_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 307
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    .line 308
    .local v2, "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v1

    .line 310
    .local v1, "clubRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :cond_1
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 311
    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 312
    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 313
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v5

    .line 314
    .local v5, "viewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    sget-object v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 316
    .local v4, "viewerIsModerator":Z
    if-eqz v4, :cond_5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->allReportedItems:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 317
    sget-object v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->isViewerOwner:Z

    .line 318
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 319
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->ownerXuid:J

    .line 322
    :cond_2
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->moderatorXuids:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 324
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->moderator()Lcom/google/common/collect/ImmutableList;

    move-result-object v6

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 325
    .local v3, "mod":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->moderatorXuids:Ljava/util/List;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 302
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "clubRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .end local v2    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .end local v3    # "mod":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .end local v4    # "viewerIsModerator":Z
    .end local v5    # "viewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .restart local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_3
    move-object v2, v1

    .line 307
    goto :goto_1

    .line 328
    .restart local v1    # "clubRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .restart local v2    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .restart local v4    # "viewerIsModerator":Z
    .restart local v5    # "viewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    :cond_4
    :try_start_2
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 329
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->loadAllReports()Lio/reactivex/Observable;

    move-result-object v7

    .line 330
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v8

    invoke-virtual {v7, v8}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v7

    .line 331
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v8

    invoke-virtual {v7, v8}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v7

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v8

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v9

    .line 332
    invoke-virtual {v7, v8, v9}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v7

    .line 328
    invoke-virtual {v6, v7}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto/16 :goto_0

    .line 342
    :cond_5
    if-nez v4, :cond_6

    .line 343
    sget-object v6, Lcom/microsoft/xbox/toolkit/network/ListState;->InvalidState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 345
    :cond_6
    sget-object v6, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 353
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "clubRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .end local v2    # "clubSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .end local v4    # "viewerIsModerator":Z
    .end local v5    # "viewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    :pswitch_1
    sget-object v6, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 354
    sget-object v6, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v7, "Club model changed to invalid state."

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 302
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 269
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminReportsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 270
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 276
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 278
    :cond_0
    return-void
.end method

.method public removeOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 734
    .local p1, "listener":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/Collection<+Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreenViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ListenerManager;->removeListener(Ljava/lang/Object;)V

    .line 735
    return-void
.end method

.method public sendUserMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 5
    .param p1, "user"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    .line 232
    new-instance v2, Lcom/microsoft/xbox/toolkit/MultiSelection;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/MultiSelection;-><init>()V

    .line 233
    .local v2, "recipients":Lcom/microsoft/xbox/toolkit/MultiSelection;, "Lcom/microsoft/xbox/toolkit/MultiSelection<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    new-instance v4, Lcom/microsoft/xbox/service/model/FollowersData;

    invoke-direct {v4, p1}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;-><init>(Lcom/microsoft/xbox/service/model/FollowersData;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/MultiSelection;->add(Ljava/lang/Object;)V

    .line 235
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecipients(Lcom/microsoft/xbox/toolkit/MultiSelection;)V

    .line 237
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 238
    .local v1, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const-class v3, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsScreen;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMessagesOriginatingScreen(Ljava/lang/Class;)V

    .line 241
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    const-class v4, Lcom/microsoft/xbox/xle/app/activity/ComposeMessageActivity;

    invoke-virtual {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->PushScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_0
    return-void

    .line 242
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->printStackTrace()V

    goto :goto_0
.end method
