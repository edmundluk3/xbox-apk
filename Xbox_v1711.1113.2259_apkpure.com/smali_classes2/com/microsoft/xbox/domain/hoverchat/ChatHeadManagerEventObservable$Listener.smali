.class final Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;
.super Ljava/lang/Object;
.source "ChatHeadManagerEventObservable.java"

# interfaces
.implements Lio/reactivex/disposables/Disposable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Listener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/disposables/Disposable;"
    }
.end annotation


# instance fields
.field private final chatHeadListener:Lcom/flipkart/chatheads/ui/ChatHeadListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadListener",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<TT;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;

.field private final unsubscribed:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;Lcom/flipkart/chatheads/ui/ChatHeadManager;Lio/reactivex/Observer;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<TT;>;",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable<TT;>.Listener<TT;>;"
    .local p2, "chatHeadManager":Lcom/flipkart/chatheads/ui/ChatHeadManager;, "Lcom/flipkart/chatheads/ui/ChatHeadManager<TT;>;"
    .local p3, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent<TT;>;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->this$0:Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->unsubscribed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 48
    iput-object p2, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener$1;-><init>(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable;Lio/reactivex/Observer;)V

    iput-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->chatHeadListener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;)Lcom/flipkart/chatheads/ui/ChatHeadListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->chatHeadListener:Lcom/flipkart/chatheads/ui/ChatHeadListener;

    return-object v0
.end method


# virtual methods
.method public dispose()V
    .locals 3

    .prologue
    .line 96
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable<TT;>.Listener<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->unsubscribed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->chatHeadManager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->setListener(Lcom/flipkart/chatheads/ui/ChatHeadListener;)V

    .line 99
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 103
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable<TT;>.Listener<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventObservable$Listener;->unsubscribed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method
